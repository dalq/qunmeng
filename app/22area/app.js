var App = angular.module('area', []);

App.config(require('./router'));

//service
App.factory('areaservice', require('./service'));

//Controllers
App.controller('areaList', require('./controllers/arealist'));

//Directive
App.directive('arealoadover',require('./directives/arealoadover'));


module.exports = App;