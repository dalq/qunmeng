 /**
 * 子模块路由
 * DHpai
 */

var router = function($urlRouterProvider, $stateProvider){


 	$stateProvider


   //菜单列表
    .state('app.areaList', {
        url: "/area/arealist",
        views: {
            'main@' : {
                template : require('./views/area.html'),
                controller : 'areaList',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
            transData : function(utilservice){
                return utilservice.transData;
            },
            areaList : function(areaservice){
                return areaservice.areaList();
            },
            areaInfo : function(areaservice){
                return areaservice.areaInfo();
            },
            areaSave : function(areaservice){
                return areaservice.areaSave();
            },
            delArea : function(areaservice){
                return areaservice.delArea();
            }
        }
    })


	;

};

module.exports = router;