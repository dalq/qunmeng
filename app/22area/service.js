/**
 * 子模块service
 * DHpai
 */
var service = function($resource, $q, $state, $modal){
    
    //区域查询
    var areaList = '/api/as/sc/area/getAreaList';
    //ID查询
    var areaInfo = '/api/as/sc/area/getById';
    //保存区域
    var areaSave = '/api/as/sc/area/save';
    //删除
    var delArea = '/api/as/sc/area/delArea';

    return {
        areaList : function(){
            return $resource(areaList, {}, {});
        },
        areaInfo : function(){
            return $resource(areaInfo, {}, {});
        },
        areaSave : function(){
            return $resource(areaSave, {}, {});
        },
        delArea : function(){
            return $resource(delArea, {}, {});
        }
    };

};

module.exports = service;