module.exports = function($scope, transData, areaList, areaInfo, areaSave, delArea){

	$scope.obj = {};
	$scope.tabname = { 'name': '添加区域', 'active': false };
	$scope.status = { 'active': true };

	init();
	//初始化
	function init(){
		$scope.status.active = true;
		$("#treetable").hide();
		areaList.save({}, function(res){
			if(res.errcode === 0){
				$scope.status.active = true;
				$scope.area_list = res.data;
				$scope.super_area_list = res.data;
			}else{
				alert(err.errmsg);
			}
		});
	}

	//添加
	$scope.add = function(item){
		if($scope.tabname.name == '修改区域'){
			return;
		}
		$scope.tabname = { 'name': '添加区域', 'active': true };
		$scope.obj = {};
		$scope.obj.parent_id = item.id;
		$scope.obj.type = parseInt(item.type)+1+'';
		var sort = $("#treetable").find("tr[pid="+item.id+"]:last").attr("sort");
		if(sort == undefined){
			sort = '0';
		}
		$scope.obj.sort = parseInt(sort)+30;
	};

	//修改界面
	$scope.update = function(item){
		$scope.tabname = { 'name': '修改区域', 'active': true };
		areaInfo.save(item, function(res){
			if(res.errcode === 0){
				$scope.obj = res.data;
			}else{
				alert(err.errmsg);
			}
		});
	};

	//取消=重置
	$scope.cancel = function(){
		$scope.status.active = true;
		$scope.tabname = { 'name': '添加区域', 'active': false };
	};

	//删除
	$scope.delete = function(item){
		if(confirm('确认要删除该用户吗？')==true){
			delArea.save(item, function(res){
				if(res.errcode === 0){
					var index = $scope.area_list.indexOf(item);
					$scope.area_list.splice(index, 1);
				}else{
					alert(err.errmsg);
				}
			});
		}
	};

	//保存
	$scope.save = function(){
		areaSave.save($scope.obj, function(res){
			if(res.errcode === 0){
				$scope.cancel();
				init();
			}else{
				alert(err.errmsg);
			}
		});
	};


};