/**
* 子模块路由
* ml
*/

var router = function ($urlRouterProvider, $stateProvider) {

	$stateProvider

		//影院列表
		.state('app.cinemaList', {
			url: "/cinema/list",
			views: {
				'main@': {
					template: require('./views/cinemaList.html'),
					controller: 'cinemaList'
				}
			},
			resolve: {

			}
		})

		//影院房间
		.state('app.roomList', {
			url: "/filmRoom/list/:params",
			views: {
				'main@': {
					template: require('./views/roomList.html'),
					controller: 'roomList'
				}
			},
			resolve: {

			}
		})

		//影院房间布局
		.state('app.roomLayout', {
			url: "/room/roomLayout/:params",
			views: {
				'main@': {
					template: require('./views/roomLayout.html'),
					controller: 'roomLayout'
				}
			},
			resolve: {

			}
		})

		//电影产品列表
		.state('app.movieList', {
			url: "/room/movie/list",
			views: {
				'main@': {
					template: require('./views/movieList.html'),
					controller: 'movieList'
				}
			},
			resolve: {

			}
		})

		//场次列表
		.state('app.timesList', {
			url: "/room/times/list",
			views: {
				'main@': {
					template: require('./views/timesList.html'),
					controller: 'timesList'
				}
			},
			resolve: {

			}
		})

		//场次布局数据
		.state('app.timesLayout', {
			url: "/room/times/layout/:params",
			views: {
				'main@': {
					template: require('./views/timesLayout.html'),
					controller: 'timesLayout'
				}
			},
			resolve: {

			}
		})

};

module.exports = router;