/**
 * 景区系统入口
 * ml
 */
require('../998mstp/js/minicolors/jquery.minicolors');
require('../998mstp/js/minicolors/jquery.minicolors.css');
require('../998mstp/js/minicolors/jquery.minicolors.png');
require('angular-minicolors');
var App = angular.module('film', ['minicolors']);

App.config(require('./router'));

//service
// App.factory('filmService', require('./service'));

//Controllers
App.controller('cinemaList', require('./controllers/cinemaList'));
App.controller('cinemaInfo', require('./controllers/cinemaInfo'));
App.controller('roomList', require('./controllers/roomList'));
App.controller('roomInfo', require('./controllers/roomInfo'));
App.controller('roomLayout', require('./controllers/roomLayout'));
App.controller('previewLayout', require('./controllers/previewLayout'));
App.controller('movieList', require('./controllers/movieList'));
App.controller('movieInfo', require('./controllers/movieInfo'));
App.controller('timesList', require('./controllers/timesList'));
App.controller('timesInfo', require('./controllers/timesInfo'));
App.controller('timesLayout', require('./controllers/timesLayout'));
App.controller('selectMovie', require('./controllers/selectMovie'));
App.controller('previewTimesLayout', require('./controllers/previewTimesLayout'));

//directive
// App.directive('viewCell', require('./directives/viewCell'));

// module.exports = App;