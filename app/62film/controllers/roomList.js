module.exports = function ($scope, $state, $resource, $modal, toaster, $stateParams) {
	// $scope.cinema = $stateParams.params? eval('(' + $stateParams.params + ')'): {};
	$scope.cinema = $stateParams.params? angular.fromJson(decodeURIComponent($stateParams.params)): {};
	// $scope.cinema = $stateParams.params? $scope.$eval($stateParams.params): {};
	$scope.myKeyup = function (e) {
		//IE 编码包含在window.event.keyCode中，Firefox或Safari 包含在event.which中
		var keycode = window.event ? e.keyCode : e.which;
		if (keycode == 13) {
			$scope.loadlist();
		}
	};
	// $scope.currentPage = 1;		//当前页码
	// $scope.itemsPerPage = 10;	//每页显示几条
	$scope.searchform = {};
	//卡列表
	$scope.loadlist = function () {
		var para = {
			'pageNo': $scope.currentPage,
			'pageSize': $scope.itemsPerPage,
			'place_code': $scope.cinema.code
		}
		Object.assign(para, $scope.searchform);
		$resource('/api/as/fc/filmplaceroom/list', {}, {}).save(para, function (res) {
			if (res.errcode === 0) {
				$scope.roomList = res.data;
				// $scope.totalItems = res.data.totalRecord;
			} else {
				toaster.error({ title: '', body: res.errmsg });
			}
		});
	}

	$scope.loadlist();

	//房间信息
	$scope.info = function (room) {
		var modalInstance = $modal.open({
			template: require('../views/roomInfo.html'),
			controller: 'roomInfo',
			size: 'lg',
			resolve: {
				room: function () {
					return room;
				},
				place_code: function () {
					return $scope.cinema.code;
				}
			}
		});
		//关闭模态框刷新页面
		modalInstance.result.then(function () {
			$scope.loadlist();
		});
	}

	//房间信息
	$scope.layout = function (room) {
		$state.go('app.roomLayout', {params: JSON.stringify(room)})
	}

	//房间信息
	$scope.createLayout = function (room) {
		var para = {
			room_code: room.code
		}
		Object.assign(para, room);
		$resource('/api/ac/fc/filmPlaceRoomLayoutService/createLayout', {}, {}).save(para, function (res) {
			if (res.errcode === 0){
				$scope.loadlist();
			} else {
				toaster.error({title: '', body: res.errmsg});
			}
		});
	}

	//删除卡
	$scope.delete = function(room){
		if(confirm('确认删除此影院吗？') == true){
			$resource('/api/as/fc/filmplaceroom/updateDelete', {}, {}).save(room, function (res) {
				if (res.errcode === 0){
					$scope.loadlist();
				} else {
					toaster.error({title: '', body: res.errmsg});
				}
			});
		}
	}

};