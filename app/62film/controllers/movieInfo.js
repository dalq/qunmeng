module.exports = function ($scope, $resource, $modalInstance, movie, toaster, FileUploader) {

    $scope.movie;
    $scope.url = '/api/as/fc/filmproduct/insert'
    if (movie) {
        $scope.movie = movie;
        $scope.url = '/api/as/fc/filmproduct/update'
    } else {
        $scope.movie = {};
    }

    //添加影院
    $scope.ok = function () {
        $resource($scope.url, {}, {}).save($scope.movie, function (res) {
            if (res.errcode === 0) {
                $modalInstance.close();
            } else {
                toaster.error({ title: '', body: res.errmsg });
            }
        });
    }

    //取消
    $scope.close = function () {
        $modalInstance.dismiss('cancel');
    }

    $scope.uploader = new FileUploader({
        // url: 'http://cl.juyouhx.com/oss.php/oss/webuploader1?topdir=aa&selfdir=bb'
        url: 'https://dlqt.juyouhx.com/Api/Api/ObjectToOss?topdir=aa&selfdir=bb'
    });

    $scope.uploader.filters.push({
        name: 'imageFilter',
        fn: function (item /*{File|FileLikeObject}*/, options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });

    $scope.uploader.onSuccessItem = function (fileItem, response, status, headers) {
        $scope.movie.img = response.savename;
    };

};