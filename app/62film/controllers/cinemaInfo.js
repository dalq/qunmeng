module.exports = function ($scope, $resource, $modalInstance, cinema, toaster, FileUploader) {
    $scope.cinema;
    $scope.url = '/api/ac/fc/filmPlaceService/createFilmPlace'
    if (cinema) {
        $scope.cinema = cinema;
        $scope.url = '/api/as/fc/filmplace/update'
    } else {
        $scope.cinema = {};
    }

    //添加影院
    $scope.ok = function () {
        $resource($scope.url, {}, {}).save($scope.cinema, function (res) {
            if (res.errcode === 0) {
                $modalInstance.close();
            } else {
                toaster.error({ title: '', body: res.errmsg });
            }
        });
    }

    //取消
    $scope.close = function () {
        $modalInstance.dismiss('cancel');
    }

    $scope.uploader = new FileUploader({
        // url: 'http://cl.juyouhx.com/oss.php/oss/webuploader1?topdir=aa&selfdir=bb'
        url: 'https://dlqt.juyouhx.com/Api/Api/ObjectToOss?topdir=aa&selfdir=bb'
    });

    $scope.uploader.filters.push({
        name: 'imageFilter',
        fn: function (item /*{File|FileLikeObject}*/, options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });

    $scope.uploader.onSuccessItem = function (fileItem, response, status, headers) {
        $scope.cinema.img = response.savename;
    };
};