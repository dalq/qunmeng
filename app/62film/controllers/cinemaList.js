module.exports = function ($scope, $state, $resource, $modal, toaster) {
	$scope.myKeyup = function (e) {
		//IE 编码包含在window.event.keyCode中，Firefox或Safari 包含在event.which中
		var keycode = window.event ? e.keyCode : e.which;
		if (keycode == 13) {
			$scope.loadlist();
		}
	};
	$scope.currentPage = 1;		//当前页码
	$scope.itemsPerPage = 10;	//每页显示几条
	$scope.searchform = {};
	//卡列表
	$scope.loadlist = function () {
		var para = {
			'pageNo': $scope.currentPage,
			'pageSize': $scope.itemsPerPage
		}
		angular.extend(para, $scope.searchform);
		$resource('/api/as/fc/filmplace/list', {}, {}).save(para, function (res) {
			if (res.errcode === 0) {
				$scope.cinemaList = res.data;
				$scope.totalItems = res.data.totalRecord;
			} else {
				toaster.error({ title: '', body: res.errmsg });
			}
		});
	}

	$scope.loadlist();

	//房间信息
	$scope.info = function (cinema) {
		var modalInstance = $modal.open({
			template: require('../views/cinemaInfo.html'),
			controller: 'cinemaInfo',
			size: 'lg',
			resolve: {
				cinema: function () {
					return cinema;
				}
			}
		});
		//关闭模态框刷新页面
		modalInstance.result.then(function () {
			$scope.loadlist();
		});
	}

	//卡信息
	$scope.room = function (cinema) {
		$state.go('app.roomList', {params: JSON.stringify(cinema)})
	}

	//删除卡
	$scope.delete = function(cinema){
		if(confirm('确认删除此影院吗？') == true){
			$resource('/api/as/fc/filmplace/updateDelete', {}, {}).save(cinema, function (res) {
				if (res.errcode === 0){
					$scope.loadlist();
				} else {
					toaster.error({title: '', body: res.errmsg});
				}
			});
		}
	}

};