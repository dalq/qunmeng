module.exports = function ($scope, $resource, $modalInstance, room, toaster) {
    $scope.room = room;
    $scope.mapStr = [];
    $scope.unavailable = [];
    $scope.loadlist = function () {
        $scope.mapStr = [];
        $scope.unavailable = [];
        var para = {
            'room_code': $scope.room.code,
            'place_code': $scope.room.place_code
        }
        $resource('/api/as/fc/filmplaceroomlayout/getRoomLayoutList', {}, {}).save(para, function (res) {
            if (res.errcode === 0) {
                // $('#selected-seats').html('');
                $('#seat-map2').html('<div class="front">屏幕</div>');
                $('#legend').html('');
                $scope.roomList = res.data;
                for (var index = 0; index < $scope.roomList.length; index++) {
                    var element = $scope.roomList[index];
                    if (element.column_posi - 1 == 0) {
                        $scope.mapStr[element.line_posi - 1] = '';
                    }
                    $scope.mapStr[element.line_posi - 1] += 'a';
                    if (element.state == '0') {
                        $scope.unavailable.push(element.line_posi + '__' + element.column_posi)
                    }
                }
                $scope.makeData();
                $scope.applyDom();
            } else {
                toaster.error({ title: '', body: res.errmsg });
            }
        });
    }
    $scope.loadlist();

    $scope.update = function () {
        var para = {
            'room_code': $scope.room.code,
            'place_code': $scope.room.place_code
        }
        $resource('/api/ac/fc/filmPlaceRoomLayoutService/updateLayout', {}, {}).save(para, function (res) {
            if (res.errcode === 0) {
            } else {
                toaster.error({ title: '', body: res.errmsg });
            }
        });
    }

    $scope.selectMap = {};
    // var $cart = $('#selected-seats'); //座位区
    $scope.makeData = function () {
        (function (e) {
            e.fn.seatCharts = function (t) {
                if (this.data("seatCharts-2")) {
                    this.data("seatCharts-2", {})
                    // return this.data("seatCharts-2")
                }
                var n = this,
                    r = {},
                    i = [],
                    s, o = {
                        animate: false,
                        naming: {
                            top: true, 
                            left: true, 
                            getId: function (e, t, n) { return t + "__" + n }, 
                            getLabel: function (e, t, n) { return n }
                        },
                        legend: {
                            node: null, items: []
                        },
                        click: function () {
                            if (this.status() == "available") { return "selected" } else if (this.status() == "selected") { return "available" } else { return this.style() }
                        },
                        focus: function () {
                            if (this.status() == "available") { return "focused" } else { return this.style() }
                        },
                        blur: function () {
                            return this.status()
                        },
                        seats: {}
                    },
                    u = function (t, n) {
                        return function (i) {
                            var s = this;
                            s.settings = e.extend({
                                status: "available", style: "available", data: n.seats[i.character] || {}
                            }, i);
                            s.settings.$node = e("<div></div>");
                            s.settings.$node.attr({ id: s.settings.id, role: "checkbox", "aria-checked": false, focusable: true, tabIndex: -1 })
                                .text(s.settings.label)
                                .addClass(
                                ["seatCharts-seat-2", "seatCharts-cell-2", "available"]
                                    .concat(s.settings.classes, typeof n.seats[s.settings.character] == "undefined" ? [] : n.seats[s.settings.character].classes)
                                    .join(" ")
                                );
                            s.data = function () { return s.settings.data };
                            s.char = function () { return s.settings.character };
                            s.node = function () { return s.settings.$node };
                            s.style = function () {
                                return arguments.length == 1 ? function (e) {
                                    var t = s.settings.style;
                                    if (e == t) { return t }
                                    s.settings.status = e != "focused" ? e : s.settings.status;
                                    s.settings.$node.attr("aria-checked", e == "selected");
                                    n.animate ? s.settings.$node.switchClass(t, e, 200) : s.settings.$node.removeClass(t).addClass(e);
                                    return s.settings.style = e
                                }(arguments[0]) : s.settings.style
                            };
                            s.status = function () {
                                return s.settings.status = arguments.length == 1 ? s.style(arguments[0]) : s.settings.status
                            };
                            (function (i, o, u) {
                                e.each(["click", "focus", "blur"], function (e, a) {
                                    s[a] = function () {
                                        if (a == "focus") {
                                            if (t.attr("aria-activedescendant") !== undefined) {
                                                r[t.attr("aria-activedescendant")].blur()
                                            }
                                            t.attr("aria-activedescendant", u.settings.id);
                                            u.node().focus()
                                        } return s.style(typeof i[o][a] === "function" ? i[o][a].apply(u) : n[a].apply(u))
                                    }
                                })
                            })(n.seats, s.settings.character, s);
                            s.node()
                                .on("click", s.click)
                                .on("mouseenter", s.focus)
                                .on("mouseleave", s.blur)
                                .on("keydown", function (e, n) {
                                    return function (i) {
                                        var s;
                                        switch (i.which) {
                                            case 32:
                                                i.preventDefault();
                                                e.click();
                                                break;
                                            case 40:
                                            case 38:
                                                i.preventDefault();
                                                s = function o(e, t, r) {
                                                    var u;
                                                    if (!e.index(r) && i.which == 38) {
                                                        u = e.last()
                                                    } else if (e.index(r) == e.length - 1 && i.which == 40) {
                                                        u = e.first()
                                                    } else {
                                                        u = e.eq(e.index(r) + (i.which == 38 ? -1 : +1))
                                                    }
                                                    s = u.find(".seatCharts-seat-2,.seatCharts-space-2").eq(t.index(n));
                                                    return s.hasClass("seatCharts-space-2") ? o(e, t, u) : s
                                                }(
                                                    n.parents(".seatCharts-container-2").find(".seatCharts-row-2:not(.seatCharts-header)"),
                                                    n.parents(".seatCharts-row-2:first").find(".seatCharts-seat-2,.seatCharts-space-2"),
                                                    n.parents(".seatCharts-row-2:not(.seatCharts-header)")
                                                    );
                                                if (!s.length) { return }
                                                e.blur();
                                                r[s.attr("id")].focus();
                                                s.focus();
                                                t.attr("aria-activedescendant", s.attr("id"));
                                                break;
                                            case 37:
                                            case 39:
                                                i.preventDefault();
                                                s = function (e) {
                                                    if (!e.index(n) && i.which == 37) {
                                                        return e.last()
                                                    } else if (e.index(n) == e.length - 1 && i.which == 39) {
                                                        return e.first()
                                                    } else {
                                                        return e.eq(e.index(n) + (i.which == 37 ? -1 : +1))
                                                    }
                                                }(
                                                    n.parents(".seatCharts-container-2:first").find(".seatCharts-seat-2:not(.seatCharts-space-2)")
                                                    );
                                                if (!s.length) { return }
                                                e.blur();
                                                r[s.attr("id")].focus();
                                                s.focus();
                                                t.attr("aria-activedescendant", s.attr("id"));
                                                break;
                                            default:
                                                break
                                        }
                                    }
                                }(s, s.node()))
                        }
                    }(n, o);
                n.addClass("seatCharts-container-2");
                e.extend(true, o, t);
                o.naming.rows = o.naming.rows || function (e) { var t = []; for (var n = 1; n <= e; n++) { t.push(n) } return t }(o.map.length);
                o.naming.columns = o.naming.columns || function (e) { var t = []; for (var n = 1; n <= e; n++) { t.push(n) } return t }(o.map[0].split("").length);
                if (o.naming.top) {
                    var a = e("<div></div>").addClass("seatCharts-row-2 seatCharts-header");
                    if (o.naming.left) {
                        a.append(e("<div></div>").addClass("seatCharts-cell-2"))
                    }
                    e.each(o.naming.columns, function (t, n) {
                        a.append(e("<div></div>").addClass("seatCharts-cell-2").text(n))
                    })
                }
                n.append(a);
                e.each(o.map, function (t, s) {
                    var a = e("<div></div>").addClass("seatCharts-row-2");
                    if (o.naming.left) {
                        a.append(e("<div></div>").addClass("seatCharts-cell-2 seatCharts-space-2").text(o.naming.rows[t]))
                    }
                    e.each(s.match(/[a-z_]{1}(\[[0-9a-z_]{0,}(,[0-9a-z_ ]+)?\])?/gi), function (n, s) {
                        var f = s.match(/([a-z_]{1})(\[([0-9a-z_ ,]+)\])?/i),
                            l = f[1],
                            c = typeof f[3] !== "undefined" ? f[3].split(",") : [],
                            h = c.length ? c[0] : null,
                            p = c.length === 2 ? c[1] : null;
                        a.append(l != "_" ? function (e) {
                            o.seats[l] = l in o.seats ? o.seats[l] : {};
                            var s = h ? h : e.getId(l, e.rows[t], e.columns[n]);
                            r[s] = new u({ id: s, label: p ? p : e.getLabel(l, e.rows[t], e.columns[n]), row: t, column: n, character: l });
                            i.push(s);
                            return r[s].node()
                        }(o.naming) : e("<div></div>").addClass("seatCharts-cell-2 seatCharts-space-2"))
                    });
                    n.append(a)
                });
                o.legend.items.length ? function (t) {
                    var r = (t.node || e("<div></div").insertAfter(n)).addClass("seatCharts-legend");
                    var i = e("<ul></ul>").addClass("seatCharts-legendList").appendTo(r);
                    e.each(t.items, function (t, n) {
                        i.append(
                            e("<li></li>")
                                .addClass("seatCharts-legendItem")
                                .append(
                                e("<div></div>")
                                    .addClass(["seatCharts-seat-2", "seatCharts-cell-2", n[1]].concat(o.classes, typeof o.seats[n[0]] == "undefined" ? [] : o.seats[n[0]].classes).join(" "))
                                )
                                .append(e("<span></span>").addClass("seatCharts-legendDescription").text(n[2]))
                        )
                    });
                    return r
                }(o.legend) : null;
                n.attr({ tabIndex: 0 });
                n.focus(function () {
                    if (n.attr("aria-activedescendant")) {
                        r[n.attr("aria-activedescendant")].blur()
                    }
                    n.find(".seatCharts-seat-2:not(.seatCharts-space-2):first").focus();
                    r[i[0]].focus()
                });
                n.data("seatCharts-2", {
                    seats: r,
                    seatIds: i,
                    status: function () {
                        var t = this;
                        return arguments.length == 1 ? t.seats[arguments[0]].status() : function (n, r) {
                            return typeof n == "string" ? t.seats[n].status(r) : function () {
                                e.each(n, function (e, n) { t.seats[n].status(r) })
                            }()
                        }(arguments[0], arguments[1])
                    }, each: function (e) {
                        var t = this; for (var n in t.seats) { if (false === e.call(t.seats[n], n)) { return n } } return true
                    },
                    node: function () {
                        var t = this; return e("#" + t.seatIds.join(",#"))
                    },
                    find: function (e) {
                        var t = this;
                        var n = t.set();
                        return e.length == 1 ? function (e) {
                            t.each(function () { if (this.char() == e) { n.push(this.settings.id, this) } }); return n
                        }(e) : function () {
                            return e.indexOf(".") > -1 ? function () {
                                var r = e.split("."); t.each(function (e) { if (this.char() == r[0] && this.status() == r[1]) { n.push(this.settings.id, this) } }); return n
                            }() : function () {
                                t.each(function () {
                                    if (this.status() == e) {
                                        n.push(this.settings.id, this)
                                    }
                                });
                                return n
                            }()
                        }()
                    },
                    set: function f() {
                        var t = this; return {
                            seats: [],
                            seatIds: [],
                            length: 0,
                            status: function () {
                                var t = arguments,
                                    n = this;
                                return this.length == 1 && t.length == 0 ? this.seats[0].status() : function () {
                                    e.each(n.seats, function () {
                                        this.status.apply(this, t)
                                    })
                                }()
                            },
                            node: function () {
                                return t.node.call(this)
                            },
                            each: function () {
                                return t.each.call(this, arguments[0])
                            },
                            get: function () {
                                return t.get.call(this, arguments[0])
                            },
                            find: function () {
                                return t.find.call(this, arguments[0])
                            },
                            set: function () {
                                return f.call(t)
                            },
                            push: function (e, t) {
                                this.seats.push(t); this.seatIds.push(e); ++this.length
                            }
                        }
                    },
                    get: function (t) {
                        var n = this;
                        return typeof t == "string" ? n.seats[t] : function () {
                            var r = n.set();
                            e.each(t, function (e, t) {
                                if (typeof n.seats[t] === "object") { r.push(t, n.seats[t]) }
                            });
                            return r
                        }()
                    }
                });
                return n.data("seatCharts-2")
            }
        })(jQuery)
        var sc = $('#seat-map2').seatCharts({
            map: $scope.mapStr,
            // [  //座位图
            //     'aaaaaaaaaa',
            //     'aaaaaaaaaa',
            //     '__________',
            //     'aaaaaaaa__',
            //     'aaaaaaaaaa',
            //     'aaaaaaaaaa',
            //     'aaaaaaaaaa',
            //     'aaaaaaaaaa',
            //     'aaaaaaaaaa',
            //     'aa__aa__aa'
            // ],
            naming: {
                top: false,
                getLabel: function (character, row, column) {
                    return column;
                }
            },
            legend: { //定义图例
                node: $('#legend'),
                items: [
                    ['a', 'available', '可选座'],
                    ['a', 'unavailable', '不可选']
                ]
            },
            click: function () { //点击事件
                // if (this.status() == 'available') { //可选座
                //     $scope.selectMap[(this.settings.row + 1) + '_' + (this.settings.column + 1)] = 'available';
                //     return 'selected';
                // } else if (this.status() == 'selected') { //已选中
                //     var status = $scope.selectMap[(this.settings.row + 1) + '_' + (this.settings.column + 1)];
                //     delete $scope.selectMap[(this.settings.row + 1) + '_' + (this.settings.column + 1)];
                //     return status;
                // } else if (this.status() == 'unavailable') { //已售出
                //     $scope.selectMap[(this.settings.row + 1) + '_' + (this.settings.column + 1)] = 'unavailable';
                //     return 'selected';
                // } else {
                    return this.style();
                // }
            }
        });
        //已售出的座位
        // sc.get(['1_2', '4_4', '4_5', '6_6', '6_7', '8_5', '8_6', '8_7', '8_8', '10_1', '10_2']).status('unavailable');
        sc.get($scope.unavailable).status('unavailable');
    }

    $scope.applyDom = function () {
        for (var index = 0; index < $scope.roomList.length; index++) {
            var element = $scope.roomList[index];
            $('#' + element.line_posi + '__' + element.column_posi ).html(element.name.replace('排', '排<br />'));
        }
        for (var index = 0; index < $scope.unavailable.length; index++) {
            var element = $scope.unavailable[index];
            $('#' + element.line_posi + '__' + element.column_posi ).addClass('unavailable')
        }
    }

    function one() {
        console.log(111)
        $('.seatCharts-cell-2-new').addClass('seatCharts-cell-2');
        $('.seatCharts-cell-2').removeClass('seatCharts-cell-2-new');
    }
    $('#one').bind('click', function () {
    })
    
    $('#two').bind('click', function () {
        console.log(222)
        $('.seatCharts-cell-2').addClass('seatCharts-cell-2-new');
        $('.seatCharts-cell-2-new').removeClass('seatCharts-cell-2');
    })

};