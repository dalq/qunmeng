module.exports = function ($scope, $state, $resource, $modal, toaster) {
	$scope.myKeyup = function (e) {
		//IE 编码包含在window.event.keyCode中，Firefox或Safari 包含在event.which中
		var keycode = window.event ? e.keyCode : e.which;
		if (keycode == 13) {
			$scope.loadlist();
		}
	};
	function date2str(d) {
		if (d === undefined) {
			return "";
		}
		var month = (d.getMonth() + 1).toString();
		var day = d.getDate().toString();
		if (month.length == 1) month = '0' + month;
		if (day.length == 1) day = '0' + day;
		return d.getFullYear() + "-" + month + "-" + day;
	}

	$scope.dateOpen = function ($event, item) {
		$event.preventDefault();
		$event.stopPropagation();
		item.opened = true;
	};

	$scope.period_start = {
		// 'value': date2str(new Date()),
		'value': '',
		'opened': false
	}

	$scope.period_end = {
		// 'value': date2str(new Date()),
		'value': '',
		'opened': false
	}
	$scope.currentPage = 1;		//当前页码
	$scope.itemsPerPage = 10;	//每页显示几条
	$scope.searchform = {};
	//电影列表
	$scope.loadlist = function () {
		var para = {
			'pageNo': $scope.currentPage,
			'pageSize': $scope.itemsPerPage
		}
		$scope.searchform.start_time = (typeof $scope.period_start.value === 'string' ? $scope.period_start.value : date2str($scope.period_start.value));
		$scope.searchform.end_time = (typeof $scope.period_end.value === 'string' ? $scope.period_end.value : date2str($scope.period_end.value));
		if (($scope.searchform.start_time && !$scope.searchform.end_time) || (!$scope.searchform.start_time && $scope.searchform.end_time)) {
			toaster.warning({title: '', body: '开始日期和结束日期请同时选择'});
			return false;
		}
		angular.extend(para, $scope.searchform);
		$resource('/api/as/fc/filmshows/list', {}, {}).save(para, function (res) {
			if (res.errcode === 0) {
				$scope.timesList = res.data.results;
				$scope.totalItems = res.data.totalRecord;
			} else {
				toaster.error({ title: '', body: res.errmsg });
			}
		});
	}

	$scope.loadlist();

	//电影
	$scope.info = function (times) {
		var modalInstance = $modal.open({
			template: require('../views/timesInfo.html'),
			controller: 'timesInfo',
			size: 'lg',
			resolve: {
				times: function () {
					return times;
				}
			}
		});
		//关闭模态框刷新页面
		modalInstance.result.then(function () {
			$scope.loadlist();
		});
	}

	//布局
	$scope.createLayout = function (times) {
		var params = {
			shows_id: times.id
		}
		$resource('/api/ac/fc/filmShowsService/createFilmShowsLayout', {}, {}).save(params, function (res) {
			if (res.errcode === 0) {
				toaster.success({ title: '', body: '生成布局成功' });
				$scope.loadlist();
			} else {
				toaster.error({ title: '', body: res.errmsg });
			}
		});
	}

	//布局数据
	$scope.timesLayout = function (times) {
		$state.go('app.timesLayout', { params: JSON.stringify(times) });
	}

	//上架
	$scope.setUp = function (times) {
		$resource('/api/ac/fc/filmShowsService/updateShowsStateUp', {}, {}).save(times, function (res) {
			if (res.errcode === 0) {
				toaster.success({ title: '', body: '上架成功' });
				$scope.loadlist();
			} else {
				toaster.error({ title: '', body: res.errmsg });
			}
		});
	}

	//下架
	$scope.setDown = function (times) {
		$resource('/api/ac/fc/filmShowsService/updateShowsStateDown', {}, {}).save(times, function (res) {
			if (res.errcode === 0) {
				toaster.success({ title: '', body: '下架成功' });
				$scope.loadlist();
			} else {
				toaster.error({ title: '', body: res.errmsg });
			}
		});
	}

	//删除卡
	$scope.delete = function (times) {
		if (confirm('确认删除此影院吗？') == true) {
			$resource('/api/ac/fc/filmShowsService/updateDelete', {}, {}).save(times, function (res) {
				if (res.errcode === 0) {
					$scope.loadlist();
				} else {
					toaster.error({ title: '', body: res.errmsg });
				}
			});
		}
	}

};