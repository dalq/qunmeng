module.exports = function ($scope, $resource, $modalInstance, times, toaster) {

    function date2str(d) {
		if (d === undefined) {
			return "";
		}
		var month = (d.getMonth() + 1).toString();
		var day = d.getDate().toString();
		if (month.length == 1) month = '0' + month;
		if (day.length == 1) day = '0' + day;
		return d.getFullYear() + "-" + month + "-" + day;
	}

	$scope.dateOpen = function ($event, item) {
		$event.preventDefault();
		$event.stopPropagation();
		item.opened = true;
	};

	$scope.date = {
		// 'value': date2str(new Date()),
		'value': '',
		'opened': false
    }

    $scope.period_start = {
		// 'value': date2str(new Date()),
		'value': '',
        'opened': false,
        'hour': '00',
        'minute': '00'
	}

	$scope.period_end = {
		// 'value': date2str(new Date()),
		'value': '',
		'opened': false,
        'hour': '00',
        'minute': '00'
    }
    
    $scope.hourArr = [];
    for (var index = 0; index < 60; index++) {
        var element = index;
        if (index < 10) {
            $scope.hourArr.push('0' + index + '');
        } else {
            $scope.hourArr.push('' + index + '');
        }
    }

    $scope.times;
    $scope.url = '/api/ac/fc/filmShowsService/createFilmShows'
    $scope.is_new = true;
    if (times) {
        $scope.is_new = false;
        $scope.times = times;
        $scope.date.value = times.show_date;

        $scope.period_start.value = times.show_time_start.substr(0, 10);
        $scope.period_start.hour = times.show_time_start.substr(11, 2);
        $scope.period_start.minute = times.show_time_start.substr(14, 2);

        $scope.period_end.value = times.show_time_end.substr(0, 10);
        $scope.period_end.hour = times.show_time_end.substr(11, 2);
        $scope.period_end.minute = times.show_time_end.substr(14, 2);

        $scope.url = '/api/as/fc/filmshows/update'
    } else {
        $scope.times = {};
    }

    $scope.movieList= [];
    $resource('/api/as/fc/filmproduct/filmProductList', {}, {}).save({}, function (res) {
        if (res.errcode === 0) {
            $scope.movieList = res.data;
            if ($scope.is_new) {
                $scope.times.product_code = $scope.movieList[0].code
            }
        } else {
            toaster.error({ title: '', body: res.errmsg });
        }
    });

    $scope.placeList= [];
    $resource('/api/as/fc/filmplace/list', {}, {}).save({}, function (res) {
        if (res.errcode === 0) {
            $scope.placeList = res.data;
            if ($scope.is_new) {
                $scope.times.place_code = $scope.placeList[0].code
            }
        } else {
            toaster.error({ title: '', body: res.errmsg });
        }
    });

    //添加影院 = 
    $scope.ok = function () {
        if (!$scope.date.value) {
            toaster.warning({ title: '', body: '请选择场次日期' });
            return false;
        }
        if (!$scope.period_start.value || !$scope.period_start.hour || !$scope.period_start.minute) {
            toaster.warning({ title: '', body: '请选择场次开始时间' });
            return false;
        }
        if (!$scope.is_new && !$scope.period_end.value || !$scope.period_end.hour || !$scope.period_end.minute) {
            toaster.warning({ title: '', body: '请选择场次结束时间' });
            return false;
        }
        $scope.times.show_date = (typeof $scope.date.value === 'string' ? $scope.date.value : date2str($scope.date.value));
        $scope.times.show_time_start = (typeof $scope.period_start.value === 'string' ? $scope.period_start.value : date2str($scope.period_start.value)) + ' ' + $scope.period_start.hour + ':' + $scope.period_start.minute;
        $scope.times.show_time_end = (typeof $scope.period_end.value === 'string' ? $scope.period_end.value : date2str($scope.period_end.value)) + ' ' + $scope.period_end.hour + ':' + $scope.period_end.minute;
        $resource($scope.url, {}, {}).save($scope.times, function (res) {
            if (res.errcode === 0) {
                $modalInstance.close();
            } else {
                toaster.error({ title: '', body: res.errmsg });
            }
        });
    }

    //取消
    $scope.close = function () {
        $modalInstance.dismiss('cancel');
    }

    $scope.roomList= [];
    $scope.$watch('times.place_code', function (newValue, oldValue) {
        $scope.roomList= [];
        $resource('/api/as/fc/filmplaceroom/list', {}, {}).save({place_code: $scope.times.place_code}, function (res) {
            if (res.errcode === 0) {
                $scope.roomList = res.data;
                if ($scope.is_new) {
                    $scope.times.room_code = $scope.roomList[0].code
                }
            } else {
                toaster.error({ title: '', body: res.errmsg });
            }
        });
    }, false)

};