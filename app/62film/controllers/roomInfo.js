module.exports = function ($scope, $resource, $modalInstance, room, place_code, toaster, FileUploader) {

    $scope.room;
    $scope.url = '/api/as/fc/filmplaceroom/insert'
    $scope.is_new = true;
    if (room) {
        $scope.is_new = false;
        $scope.room = room;
        $scope.url = '/api/as/fc/filmplaceroom/update'
    } else {
        $scope.room = {};
    }

    //添加影院
    $scope.ok = function () {
        if (!$scope.room.line_num || !$scope.room.column_num) {
            toaster.info({ title: '', body: '信息有误' })
            return false;
        }
        $scope.room.place_code = place_code;
        $resource($scope.url, {}, {}).save($scope.room, function (res) {
            if (res.errcode === 0) {
                $modalInstance.close();
            } else {
                toaster.error({ title: '', body: res.errmsg });
            }
        });
    }

    //取消
    $scope.close = function () {
        $modalInstance.dismiss('cancel');
    }

    $scope.uploader = new FileUploader({
        // url: 'http://cl.juyouhx.com/oss.php/oss/webuploader1?topdir=aa&selfdir=bb'
        url: 'https://dlqt.juyouhx.com/Api/Api/ObjectToOss?topdir=aa&selfdir=bb'
    });

    $scope.uploader.filters.push({
        name: 'imageFilter',
        fn: function (item /*{File|FileLikeObject}*/, options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });

    $scope.uploader.onSuccessItem = function (fileItem, response, status, headers) {
        $scope.room.img = response.savename;
    };

};