/**
 * 子模块路由
 * dlq
 */

var router = function($urlRouterProvider, $stateProvider){

    $stateProvider

       //企业审核
    .state('app.company_check', {
        url: '/company_check.html',
        views: {
            'main@' : {
                controller : 'company_check',
                template: require('./views/company_check.html'),
            }
        },
        resolve:{
            formconfig : function(formservice){
                return formservice.formconfig();
            },
        }
        
      })
       //企业注册
      .state('app.register', {
        url: '/register.html',
        views: {
            'main@' : {
                controller : 'register',
                template: require('./views/register.html'),
            }
        },
        resolve:{
            formconfig : function(formservice){
                return formservice.formconfig();
            },
        }
        
      })
      //完善企业信息
      .state('app.completeinfo', {
        url: '/completeinfo.html/:company_id',
        views: {
            'main@' : {
                controller : 'completeinfo',
                template: require('./views/completeinfo.html'),
            }
        },
        resolve:{
            formconfig : function(formservice){
                return formservice.formconfig();
            },
        }
        
      })
      //通知公告
      .state('app.notice', {
        url: '/notice.html',
        views: {
            'main@' : {
                controller : 'notice',
                template: require('./views/notice.html'),
            }
        },
        resolve:{
        }
        
      })

       //审核管理
      .state('app.examine', { 
        url: '/examine.html',
        views: {
            'main@' : {
                controller : 'examine',
                template: require('./views/examine.html'),
                }
        },
        resolve:{
        }
        
      })
      //企业项目列表
      .state('app.project_list', {
        url: '/project_list.html',
        views: {
            'main@' : {
                controller : 'project_list',
                template: require('./views/project_list.html'),
            }
        },
        resolve:{
        }
        
      })

      //专家
       //项目查询列表
      .state('app.projectsearch', { 
        url: '/projectsearch.html',
        views: {
            'main@' : {
                controller : 'projectsearch',
                template: require('./views/projectsearch.html'),
                }
        },
        resolve:{
        }
        
      })
      //专家列表
      .state('app.expertlist', {
        url: '/expertlist.html',
        views: {
            'main@' : {
                controller : 'expertlist',
                template: require('./views/expertlist.html'),
            }
        },
        resolve:{
        }
        
      })

    //专家推荐
      .state('app.expertrec', {
        url: '/expertrec.html',
        views: {
            'main@' : {
                controller : 'expertrec',
                template: require('./views/expertrec.html'),
                }
        },
        resolve:{
        }
        
      })
      //项目复核
      .state('app.project_review', { 
        url: '/project_review.html',
        views: {
            'main@' : {
                controller : 'project_review',
                template: require('./views/project_review.html'),
                }
        },
        resolve:{
        }
        
      })


      //项目推荐
      .state('app.project_recommendation', { 
        url: '/project_recommendation.html',
        views: {
            'main@' : {
                controller : 'project_recommendation',
                template: require('./views/project_recommendation.html'),
            }
        },
        resolve:{
        }
        
      })

      //项目评审
      .state('app.project_appraisal', { 
        url: '/project_appraisal.html',
        views: {
            'main@' : {
                controller : 'project_appraisal',
                template: require('./views/project_appraisal.html'),
            }
        },
        resolve:{
        }
        
      })

      //专家设定
      .state('app.expert_setting', { 
        url: '/expert_setting.html/:project_code/:set',
        views: {
            'main@' : {
                controller : 'expert_setting',
                template: require('./views/expert_setting.html'),
            }
        },
        resolve:{
        }
        
      })

       //专家设定
      .state('app.project_acceptance', { 
        url: '/project_acceptance.html',
        views: {
            'main@' : {
                controller : 'project_acceptance',
                template: require('./views/project_acceptance.html'), 
            }
        },
        resolve:{
        }
        
      })

      //专家立项
      .state('app.project_confirmation', { 
        url: '/project_confirmation.html',
        views: {
            'main@' : {
                controller : 'project_confirmation',
                template: require('./views/project_confirmation.html'), 
            }
        },
        resolve:{
        }
        
      })

      //提报进度
      .state('app.reporting_progress', { 
        url: '/reporting_progress.html', 
        views: {
            'main@' : {
                controller : 'reporting_progress',
                template: require('./views/reporting_progress.html'), 
            }
        },
        resolve:{
        }
        
      })
      //专家评审
      .state('app.expert_review_list', { 
        url: '/expert_review_list.html',
        views: {
            'main@' : {
                controller : 'expert_review_list',
                template: require('./views/expert_review_list.html'), 
            }
        },
        resolve:{
        }
        
      })

      //入库专家
      .state('app.warehousing_expert', { 
        url: '/warehousing_expert.html',  
        views: {
            'main@' : {
                controller : 'warehousing_expert',
                template: require('./views/warehousing_expert.html'), 
            }
        },
        resolve:{
        }
        
      })

       //专家评审
      .state('app.expert_review', {  
        url: '/expert_review.html',  
        views: {
            'main@' : {
                controller : 'expert_review',
                template: require('./views/expert_review.html'), 
            }
        },
        resolve:{
        }
        
      })

      //专家评估
      .state('app.expert_evaluation', {  // setting_expert
        url: '/expert_evaluation.html',  
        views: {
            'main@' : {
                controller : 'expert_evaluation',
                template: require('./views/expert_evaluation.html'), 
            }
        },
        resolve:{
        }
        
      })


      //区县审核列表
      .state('app.distinct_review_list', { 
        url: '/distinct_review_list.html',
        views: {
            'main@' : {
                controller : 'distinct_review_list',
                template: require('./views/distinct_review_list.html'), 
            }
        },
        resolve:{
        }
        
      })

      //组委办上传pdf文件
      .state('app.office_file', {   //expert_first
        url: '/office_file.html',  
        views: {
            'main@' : {
                controller : 'office_file',
                template: require('./views/office_file.html'), 
            }
        },
        resolve:{
        }
        
      })

      //专家优先上传评分
      .state('app.expert_first', {  
        url: '/expert_first.html',  
        views: {
            'main@' : {
                controller : 'expert_first',
                template: require('./views/expert_first.html'), 
            }
        },
        resolve:{
        }
        
      })







     

};

module.exports = router;