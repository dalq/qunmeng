
module.exports = function ($scope, $state, $stateParams,$resource,$modal,FileUploader) {  

    $scope.employee={};
    $scope.a={};
    // $scope.a.field_type=[];
    $scope.fieldarr=[
        {name:'环保领域',value:'0'},
        {name:'城市建设领域',value:'1'},
        {name:'农药领域',value:'2'},
    ]
    $scope.employee.phoneinfo=""
    $scope.employee.cardnoinfo=""
    $scope.employee.emailinfo=""
$scope.submit = function(){
    var para = {
        expert_code:'',
        expert_name:$scope.name,
        field_type:$scope.a.field_type,
        photo:$scope.a.photo,
        mobile:$scope.mobile,
        email:$scope.email,
        profile:$scope.profile,
        cardno:$scope.cardno
     }
     console.log(para);
     $resource('/api/ac/ggc/reviewExpertService/create', {}, {}).save(para, function(res){
            // console.log(res);
            if(res.errcode === 0){            
                alert('保存成功');
                 $state.go('app.expertlist',{});
            }else{
                alert(res.errmsg);
           
            }
        });
}

            //正则判断电话号  
            $scope.checkphone=function(x){
                if(!/^1[34578]\d{9}$/.test(x)) return "您输入的电话号长度或格式错误";
            }
			$scope.showphoneinfo=function(x){
                console.log(x);
                if(x==undefined){
                    $scope.employee.phoneinfo="";
                   return;
                  }
                if(x==''){
                    $scope.employee.phoneinfo="";
                   return;
                  }
                                         
            if($scope.checkphone(x)=="您输入的电话号长度或格式错误"){                
                $scope.employee.phoneinfo="您输入的电话号长度或格式错误"
            }else{
                     $scope.employee.phoneinfo="";
                    }
            }
            //正则判断身份证号  
            $scope.checkcardno=function(x){
                if(!/^\d{17}(\d|x)$/i.test(x)) return "您输入的身份证号长度或格式错误";
            }
			$scope.showcardnoinfo=function(x){
                console.log(x);
                if(x==undefined){
                    $scope.employee.cardnoinfo="";
                   return;
                  }
                if(x==''){
                    $scope.employee.cardnoinfo="";
                   return;
                  }
                                         
            if($scope.checkcardno(x)=="您输入的身份证号长度或格式错误"){                
                $scope.employee.cardnoinfo="您输入的身份证号长度或格式错误"
            }else{
                     $scope.employee.cardnoinfo="";
                    }
            }

            //正则判断邮箱  
            $scope.checkemail=function(x){
                if(!/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/i.test(x) ) return "您输入的邮箱长度或格式错误";
            }
			$scope.showemailinfo=function(x){
                console.log(x);
                if(x==undefined){
                    $scope.employee.emailinfo="";
                   return;
                  }
                if(x==''){
                    $scope.employee.emailinfo="";
                   return;
                  }
                                         
            if($scope.checkemail(x)=="您输入的邮箱长度或格式错误"){                
                $scope.employee.emailinfo="您输入的邮箱长度或格式错误"
            }else{
                     $scope.employee.emailinfo="";
                    }
            }

    var uploader = $scope.uploader = new FileUploader({
        url: 'http://cl.juyouhx.com/oss.php/oss/webuploader1?topdir=aa&selfdir=bb'
    });

    uploader.filters.push({
        name: 'imageFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });
    
    
    uploader.onSuccessItem = function(fileItem, response, status, headers) {
        $scope.a.photo = response.savename;
    };


};
