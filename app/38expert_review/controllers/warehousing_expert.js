module.exports = function ($scope, $state, $stateParams,$resource,$modal,$http,$q, toaster) {
    /* 分页
			* ========================================= */
			$scope.maxSize = 5;             //最多显示多少个按钮
			$scope.bigCurrentPage = 1;      //当前页码
			$scope.itemsPerPage = 10         //每页显示几条
			
			$scope.getlist = function () {
				var para = {
					pageNo:$scope.bigCurrentPage, 
					pageSize:$scope.itemsPerPage
				};
				// para = angular.extend($scope.parameters, para);
				$resource('/api/ac/ggc/reviewExpertService/findStorageList', {}, {}).
				save(para,function(res) {
					console.log(para);
					if (res.errcode !== 0) {
						toaster.error({title:"",body:res.errmsg});
						return;
					}
					console.log(res);
					$scope.objs = res.data.results;
					$scope.bigTotalItems = res.data.totalRecord;

					
				})
			};
			
			$scope.getlist(); 

		$scope.search = function () {
        var para = {
            pageNo:$scope.bigCurrentPage, 
            pageSize:$scope.itemsPerPage,
            expert_name:$scope.expert_name,
            mobile:$scope.mobile
        };
        // para = angular.extend($scope.parameters, para);
        $resource('/api/ac/ggc/reviewExpertService/findStorageList', {}, {}).
        save(para,function(res) {
            console.log(para);
            if (res.errcode !== 0) {
                toaster.error({title:"",body:res.errmsg});
                return;
            }
            console.log(para);
            console.log(res);
            $scope.objs = res.data.results;
            $scope.bigTotalItems = res.data.totalRecord;

            
        })
    };
            $scope.seeinfo = function(item){
				$scope.item =item;
				console.log($scope.item);
				var modalInstance = $modal.open({
					template: require('../views/expertinfo.html'),
					controller: 'expertinfo',
					size: 'lg',
					resolve: {
						items: function () {
							return $scope.item;
						},
					}
				});
				modalInstance.result.then(function (showResult) {	
						$scope.getlist();
				});
			}
			$scope.edit = function(item){
				$scope.item = item;
		        console.log($scope.item);
		         var modalInstance = $modal.open({
		            template: require('../views/expertupdate.html'),
		            controller: 'expertupdate',
		            size: 'lg',
		            resolve: {
		                items: function () {
		                    return $scope.item;
		                },
		            }
		        });
		        modalInstance.result.then(function (showResult) {   
		             $scope.getlist();
		        });

			}

		$scope.shield = function(expert_code){
			if(confirm('确定要屏蔽吗?~~~')){
				$resource('/api/ac/ggc/reviewExpertService/updateShield  ', {}, {}).
				save({'expert_code' : expert_code},function(res) {
					if (res.errcode !== 0) {
						toaster.error({title:"",body:res.errmsg});
						return;
					}
					console.log(res);
					toaster.success({title:"",body:"屏蔽成功"});
					$scope.getlist(); 
				})
			}
			
		}

		$scope.recovery = function(expert_code){
			if(confirm('确定要恢复吗?~~~')){
				$resource('/api/ac/ggc/reviewExpertService/updateNotShield  ', {}, {}).
				save({'expert_code' : expert_code},function(res) {
					if (res.errcode !== 0) {
						toaster.error({title:"",body:res.errmsg});
						return;
					}
					console.log(res);
					toaster.success({title:"",body:"恢复成功"});
					$scope.getlist(); 

				})
			}
		}

		$scope.upup = function(expert_code,sort){
	         var modalInstance = $modal.open({
	            template: require('../views/expert_first.html'),
	            controller: 'expert_first',
	            size: 'lg',
	            resolve: {
					expert_code: function () {
	                    return expert_code;
                	},
					sort: function () {
						return sort;
	                },
	            }
	        });
	        modalInstance.result.then(function (showResult) {   
	             $scope.getlist();
	        });
		}
	
		 //页面需要的信息。
			$scope.page = {};
            $scope.info = {
                'aaa' : ''
            }

			var beforedata = {
				//项目类别列表
				'typearr':
				$http({
					'method': 'GET',
					'url': '/api/as/ggc/reviewCityResponsible/findAllProjectTypeList',
					// 'params': { 'type': 'sale_category' },
				}),
				//申报单位
				'companyarr':
				$http({
					'method': 'GET',
					'url': 'api/as/ggc/reviewCityResponsible/findAllCompanyList',
					// 'params': { 'type': 'ticket_sale_belong' },
				}),
				//项目名称
				'projectarr':
				$http({
					'method': 'GET',
					'url': '/api/as/ggc/reviewCityResponsible/findAllProjectList',
				}),
			};

			$q.all(beforedata).then(function (res) {

				//项目类别列表
				if (res.typearr.data.errcode === 0) {
				} else {
					alert('/api/as/ggc/reviewCityResponsible/findAllProjectTypeList' + res.typearr.data.errmsg);
					return;
				}
				//申报单位
				if (res.companyarr.data.errcode === 0) {
				} else {
					alert('api/as/ggc/reviewCityResponsible/findAllCompanyList' + res.companyarr.data.errmsg);
					return;
				}
				//项目名称
				if (res.projectarr.data.errcode === 0) {
				} else {
					alert('/api/as/ggc/reviewCityResponsible/findAllProjectList' + res.projectarr.data.errmsg);
					return;
				}

				$scope.page = {
					//项目类别列表
					'typearr': res.typearr.data.data,
					//申报单位
					'companyarr': res.companyarr.data.data,
					//项目名称
					'projectarr': res.projectarr.data.data,
					// 年度
					'yeararr' : [{'value' : '0', 'label' : '2017'}],
					// 区县/处室
					'areaarr' : [{'value' : '0', 'label' : '处室'},{'value' : '1', 'label' : '区县'}],
				};
				console.log($scope.page);
			});



			


};
