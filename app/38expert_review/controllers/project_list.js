/**
 * 模态框
 */
module.exports = function ($scope, $state, $stateParams,$resource,$modal) {
	/* 分页
     * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.currentPage = 1;      //当前页码
    $scope.itemsPerPage = 10         //每页显示几条
	$scope.obj={};
	$scope.d={};


	$scope.pageChanged = function () {
       var para = {
            pageNo:$scope.currentPage,
            pageSize:$scope.itemsPerPage,
            // product_id : $scope.id,
            // product_name : $scope.product_name,
            // product_type : $scope.product_type,
            // supplier_product_name : $scope.supplier_product_name,
            // start_city_arr : $scope.start_city_arr,
            // product_state : $scope.product_state,
            
        };
		

		$resource('/api/as/ggc/reviewCompany/getCompanyId', {}, {}).
			save({}, function(res){			
                 
                if(res.errcode === 0 || res.errcode === 10003){
					$scope.b=res.data;									
					console.log(res);
                    console.log('上面是企业company_id') 
					

                }else{
                    alert(res.errmsg);
                }
            });

        $resource('/api/as/ggc/reviewCompanyProject/findInfoList', {}, {}).
			save(para, function(res){			
                 
                if(res.errcode === 0 || res.errcode === 10003){
					$scope.a=res.data.results;					
					console.log($scope.a);
                    console.log('上面是企业项目列表返回值') 
					$scope.bigTotalItems = res.data.totalRecord;

                }else{
                    alert(res.errmsg);
                }
            });

    };
	$scope.pageChanged();


			$scope.search=function(){
			var dic = {
				pageNo:$scope.currentPage,
            	pageSize:$scope.itemsPerPage,
				
			}
				
				$resource('', {}, {}).save(dic, function(res){
                if(res.errcode === 0 || res.errcode === 10003){
                    // console.log(res);
					$scope.a=res.data.results;
					console.log($scope.a);
                    $scope.totalItems = res.data.totalRecord;
                }else{
                    alert(res.errmsg);
                }
            });
			}


             $scope.add = function () {
				 $scope.item = $scope.a;			
				var modalInstance = $modal.open({
					template: require('../views/addproject.html'),
					controller: 'addproject',
					size: 'lg',
					resolve: {
						items: function () {
							return $scope.item;
						}
					}
				});
				modalInstance.result.then(function (showResult) {	
						$scope.pageChanged();
				});
			}

			$scope.completeinfo = function () {
				$state.go('app.completeinfo',{'company_id':$scope.b.company_id})
			}

             $scope.toinfo= function (index) {
                 $scope.item = $scope.a[index];			
				var modalInstance = $modal.open({
					template: require('../views/projectinfo.html'),
					controller: 'projectinfo',
					size: 'lg',
					resolve: {
						items: function () {
							return $scope.item;
						}
					}
				});
				modalInstance.result.then(function (showResult) {	
						$scope.pageChanged();
				});
			}

			$scope.edit= function (index) {
                 $scope.item = $scope.a[index];			
				var modalInstance = $modal.open({
					template: require('../views/projectedit.html'),
					controller: 'projectedit',
					size: 'lg',
					resolve: {
						items: function () {
							return $scope.item;
						}
					}
				});
				modalInstance.result.then(function (showResult) {	
						$scope.pageChanged();
				});
			}

			$scope.progress= function (index) {
                 $scope.item = $scope.a[index];			
				var modalInstance = $modal.open({
					template: require('../views/project_progress.html'),
					controller: 'project_progress',
					size: 'lg',
					resolve: {
						items: function () {
							return $scope.item;
						}
					}
				});
				modalInstance.result.then(function (showResult) {	
						$scope.pageChanged();
				});
			}

			$scope.delete= function (index) {    
				console.log($scope.a[index].project_code) ;          
				 if(confirm('是否废弃该项目？')){
					$resource('/api/as/ggc/reviewCompanyProject/delete', {}, {}).save({'project_code':$scope.a[index].project_code}, function(res){			              
                if(res.errcode === 0 || res.errcode === 10003){
						alert('废弃成功')			
						$scope.pageChanged();
                }else{
                    alert(res.errmsg);
                }
            });
				
				 }		
				 
			}

			$scope.apply= function (index) {    
				console.log($scope.a[index].project_code) ;          
				 if(confirm('是否申请项目？')){
					$resource('/api/ac/ggc/reviewCompanyProjectService/updateApply', {}, {}).save({'project_code':$scope.a[index].project_code}, function(res){			              
                if(res.errcode === 0 || res.errcode === 10003){
						alert('申请成功')			
						$scope.pageChanged();
                }else{
                    alert(res.errmsg);
                }
            });
				
				 }		
				 
			}

			$scope.reapply= function (index) {    
				console.log($scope.a[index].project_code) ;          
				 if(confirm('是否重新申请项目？')){
					$resource('/api/as/ggc/reviewCompanyProject/updateApplyRe', {}, {}).save({'project_code':$scope.a[index].project_code}, function(res){			              
                if(res.errcode === 0 || res.errcode === 10003){
						alert('重新申请成功')			
						$scope.pageChanged();
                }else{
                    alert(res.errmsg);
                }
            });
				
				 }		
				 
			}

			

	







};

