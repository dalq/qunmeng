module.exports = function ($scope, $state, $stateParams,$resource,$modal,toaster) {
    /* 分页
        * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10         //每页显示几条
    
    $scope.getlist = function () {
        var para = {
            pageNo:$scope.bigCurrentPage, 
            pageSize:$scope.itemsPerPage
        };
        // para = angular.extend($scope.parameters, para);
        $resource('/api/as/ggc/review/findList', {}, {}).
        save(para,function(res) {
            console.log(para);
            if (res.errcode !== 0) {
                toaster.error({title:"",body:res.errmsg});
                return;
            }
            console.log(para);
            console.log(res);
            console.log('上面是res');
            $scope.objs = res.data.results;
            $scope.bigTotalItems = res.data.totalRecord;

            
        })
    };
     
    $scope.getlist(); 
    $scope.search = function(){
        var para = {
            pageNo:$scope.bigCurrentPage, 
            pageSize:$scope.itemsPerPage,
            project_name:$scope.project_name,
            company_name:$scope.company_name
        };
        // para = angular.extend($scope.parameters, para);
        $resource('/api/as/ggc/review/findList', {}, {}).
        save(para,function(res) {
            if (res.errcode !== 0) {
                toaster.error({title:"",body:res.errmsg});
                return;
            }
            console.log(para);
            console.log(res);
            console.log('上面是res');
            $scope.objs = res.data.results;
            $scope.bigTotalItems = res.data.totalRecord;

            
        })
    }
    $scope.flag = 0;
    var set = '';
    $scope.setexpert = function(project_code){
        set = '1'
        $state.go('app.expert_setting',{'project_code' : project_code,'set' : set});
    }
    $scope.showexpert = function(project_code){
        set = '2'
        $state.go('app.expert_setting',{'project_code' : project_code,'set' : set});
    }
};
