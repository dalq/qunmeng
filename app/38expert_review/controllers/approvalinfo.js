module.exports = function($scope, $state, $stateParams, $modalInstance, $resource,$modal,FileUploader,project_name,project_code,company_name,apply_time,toaster,btnflag){
    console.log(project_name,btnflag);
    $scope.btnflag = btnflag;
    $scope.info = {
        'project_name' : project_name,
        'company_name' : company_name,
        'apply_time' : apply_time,
        'reason' : ''
    }
    if(project_code){
        $resource('/api/as/ggc/reviewCityResponsible/findInfoApproval', {}, {}).
        save({'project_code' : project_code},function(res) {
            if (res.errcode !== 0) {
                toaster.error({title:"",body:res.errmsg});
                return;
            }
            $scope.info = res.data;
            if(res.data.back_reason){
                $scope.info.reason = res.data.back_reason;
            }
            if(res.data.eliminate_reason){
                $scope.info.reason = res.data.eliminate_reason;
            }
            console.log(res.data.project_file);
            $scope.filearr = res.data.project_file.split(",");
            console.log($scope.filearr);
            var array = res.data.project_file.split(",");
            for(var i = 0; i < array.length; i++){
                if(array[i].lastIndexOf('.pdf') == -1){
                    console.log('图片');
                } else {
                    console.log('pdf文件')
                }
            }
            
            
            
        })
    }
    // 审批通过
    $scope.pass = function(){
        $resource('/api/as/ggc/reviewCityResponsible/updateAdopt', {}, {}).
        save({'project_code' : project_code},function(res) {
            if (res.errcode !== 0) {
                toaster.error({title:"",body:res.errmsg});
                return;
            }
            toaster.success({title:"",body:"通过"});
            $modalInstance.close();
            $state.go('app.examine');
                $scope.getlist();
            
            
        })
    }
    // 审批驳回
    $scope.reject = function(id){
        var approvalflag = 'spbh';
        var modalInstance = $modal.open({
            template: require('../views/confirminfo.html'),
            controller: 'confirminfo',
            size: 'lg',
            resolve: {
                project_code: function () {
                    return project_code;
                },
                approvalflag: function () {
                    return approvalflag;
                },
            }
        });
        modalInstance.result.then(function (showResult) {	
                $modalInstance.close();
                $state.go('app.examine');
                $scope.getlist();
                
        });
        // if($scope.info.reason == ''){
        //     toaster.error({title:"",body:"驳回原因不能为空"});
        // } else {
        //     $resource('/api/as/ggc/reviewCityResponsible/updateReject', {}, {}).
        //     save({'project_code' : project_code, 'back_reason' : $scope.info.reason},function(res) {
        //         console.log(para);
        //         if (res.errcode !== 0) {
        //             toaster.error({title:"",body:res.errmsg});
        //             return;
        //         }
        //         toaster.success({title:"",body:"驳回"});
                
                
        //     })
        // }
        
    }
    // 审批淘汰
    $scope.eliminate = function(id){
        var approvalflag = 'sptt';
        var modalInstance = $modal.open({
            template: require('../views/confirminfo.html'),
            controller: 'confirminfo',
            size: 'lg',
            resolve: {
                project_code: function () {
                    return project_code;
                },
                approvalflag: function () {
                    return approvalflag;
                },
            }
        });
        modalInstance.result.then(function (showResult) {	
                $modalInstance.close();
                $state.go('app.examine');
                $scope.getlist();
        });
        // if($scope.info.reason == ''){
        //     toaster.error({title:"",body:"淘汰原因不能为空"});
        // } else {
        //     $resource('/api/as/ggc/reviewCityResponsible/updateBack', {}, {}).
        //     save({'project_code' : project_code, 'eliminate_reason' : $scope.info.reason},function(res) {
        //         console.log(para);
        //         if (res.errcode !== 0) {
        //             toaster.error({title:"",body:res.errmsg});
        //             return;
        //         }
        //         toaster.success({title:"",body:"淘汰"});
                
                
        //     })
        // }
    }
}