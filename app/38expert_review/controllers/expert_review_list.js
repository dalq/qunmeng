module.exports = function ($scope, $state, $stateParams,$resource,$modal,toaster) {
    /* 分页
        * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10         //每页显示几条
    
    $scope.getlist = function () {
        var para = {
            pageNo:$scope.bigCurrentPage, 
            pageSize:$scope.itemsPerPage
        };
        // para = angular.extend($scope.parameters, para);
        $resource('/api/ac/ggc/reviewExpertService/findInfoList', {}, {}).
        save(para,function(res) {          
            if (res.errcode !== 0) {
                toaster.error({title:"",body:res.errmsg});
                return;
            }
            console.log(res);
            $scope.objs = res.data.results;
            $scope.bigTotalItems = res.data.totalRecord;

            for(var i = 0;i<$scope.objs.length;i++){
                if($scope.objs[i].expert_state =='0'){
                    $scope.objs[i].expert_state = '草稿'
                }
                if($scope.objs[i].expert_state =='1'){
                    $scope.objs[i].expert_state = '待审核'
                }
                if($scope.objs[i].expert_state =='2'){
                    $scope.objs[i].expert_state = '已入库'
                }
                if($scope.objs[i].expert_state =='3'){
                    $scope.objs[i].expert_state = '拒绝纳入专家库'
                }
            }
            
        })
    };
     
    $scope.getlist(); 

    $scope.search = function () {
        var para = {
            pageNo:$scope.bigCurrentPage, 
            pageSize:$scope.itemsPerPage,
            expert_name:$scope.expert_name,
            mobile:$scope.mobile
        };
        // para = angular.extend($scope.parameters, para);
        $resource('/api/ac/ggc/reviewExpertService/findInfoList', {}, {}).
        save(para,function(res) {
            console.log(para);
            if (res.errcode !== 0) {
                toaster.error({title:"",body:res.errmsg});
                return;
            }
            console.log(para);
            console.log(res);
            $scope.objs = res.data.results;
            $scope.bigTotalItems = res.data.totalRecord;

            
        })
    };
    
    $scope.areaarr=[
        {name:'环境专家',value:'0'},
        {name:'城建专家',value:'1'},
        {name:'农药专家',value:'2'},
        ]

    $scope.toinfo = function(index){
        $scope.item = $scope.objs[index];
        $scope.item.flag = '0'
        console.log($scope.item);
         var modalInstance = $modal.open({
            template: require('../views/expertinfo.html'),
            controller: 'expertinfo',
            size: 'lg',
            resolve: {
                items: function () {
                    return $scope.item;
                },
            }
        });
        modalInstance.result.then(function (showResult) {	
                $scope.getlist();
        });
    } 


    $scope.naru= function (index) {    			        
				 if(confirm('是否将该专家纳入专家库？')){
					$resource('/api/ac/ggc/reviewExpertService/updateByInto', {}, {}).save({'expert_code':$scope.objs[index].expert_code}, function(res){			              
                if(res.errcode === 0 || res.errcode === 10003){
						alert('纳入成功')			
						$scope.getlist();
                }else{
                    alert(res.errmsg);
                }
            });
				
				 }		
				 
			}

    $scope.jujue= function (index) {    			        
				 if(confirm('是否拒绝该专家纳入专家库？')){
					$resource('/api/ac/ggc/reviewExpertService/updateByRefuse', {}, {}).save({'expert_code':$scope.objs[index].expert_code}, function(res){			              
                if(res.errcode === 0 || res.errcode === 10003){
						alert('操作成功')			
						$scope.getlist();
                }else{
                    alert(res.errmsg);
                }
            });
				
				 }		
				 
			}
};
