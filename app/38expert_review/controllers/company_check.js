module.exports = function ($scope, $state, $stateParams,$resource,$modal,toaster) {
    /* 分页
        * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10;         //每页显示几条
    $scope.b={};
    $scope.getlist = function () {
        var para = {
            pageNo:$scope.bigCurrentPage, 
            pageSize:$scope.itemsPerPage
        };
        // para = angular.extend($scope.parameters, para);
        $resource('/api/as/ggc/reviewCompany/findCompanyAuditList', {}, {}).
        save(para,function(res) {
            if (res.errcode !== 0) {
                toaster.error({title:"",body:res.errmsg});
                return;
            }
            console.log(res);
            $scope.objs = res.data.results;
             $scope.bigTotalItems = res.data.totalRecord;
             for(var i = 0;i<$scope.objs.length;i++){
                 if($scope.objs[i].audit_status=='0'){
                     $scope.objs[i].audit_status = '待审核'
                 }
                 if($scope.objs[i].audit_status=='1'){
                     $scope.objs[i].audit_status = '已通过'
                 }
                 if($scope.objs[i].audit_status=='2'){
                     $scope.objs[i].audit_status = '已驳回'
                 }
             }

             for(var i = 0;i<$scope.objs.length;i++){
                 if($scope.objs[i].company_code == 'LA00423'){
                    $scope.objs[i].company_code = '和平区'
                 }
                 if($scope.objs[i].company_code == 'LA00424'){
                    $scope.objs[i].company_code = '沈河区'
                 }
                 if($scope.objs[i].company_code == 'LA00425'){
                    $scope.objs[i].company_code = '皇姑区'
                 }
                 if($scope.objs[i].company_code == 'LA00426'){
                    $scope.objs[i].company_code = '铁西区'
                 }
                 if($scope.objs[i].company_code == 'LA00427'){
                    $scope.objs[i].company_code = '大东区'
                 }
                 if($scope.objs[i].company_code == 'LA00428'){
                    $scope.objs[i].company_code = '东陵区'
                 }
                 if($scope.objs[i].company_code == 'LA00429'){
                    $scope.objs[i].company_code = '于洪区'
                 }
                 if($scope.objs[i].company_code == 'LA00430'){
                    $scope.objs[i].company_code = '苏家屯区'
                 }
                 if($scope.objs[i].company_code == 'LA00431'){
                    $scope.objs[i].company_code = '沈北新区'
                 }
                 if($scope.objs[i].company_code == 'LA00432'){
                    $scope.objs[i].company_code = '浑南新区'
                 }
                 if($scope.objs[i].company_code == 'LA00433'){
                    $scope.objs[i].company_code = '辽中县'
                 }
                 if($scope.objs[i].company_code == 'LA00434'){
                    $scope.objs[i].company_code = '康平县'
                 }
                 if($scope.objs[i].company_code == 'LA00435'){
                    $scope.objs[i].company_code = '法库县'
                 }
                 if($scope.objs[i].company_code == 'LA00436'){
                    $scope.objs[i].company_code = '新民市'
                 }
             }
            
        })
    };
     
    $scope.getlist(); 

    $scope.search = function () {
        var para = {
            pageNo:$scope.bigCurrentPage, 
            pageSize:$scope.itemsPerPage,
            contact_mobile:$scope.contact.mobile,
            company_name:$scope.company_name
        };
        // para = angular.extend($scope.parameters, para);
        $resource('/api/as/ggc/reviewCompany/findCompanyAudit', {}, {}).
        save(para,function(res) {
            if (res.errcode !== 0) {
                toaster.error({title:"",body:res.errmsg});
                return;
            }

            $scope.objs = res.data.results;
            $scope.bigTotalItems = res.data.totalRecord;

            
        })
    };
    

        	$scope.access= function (index) {    
				console.log($scope.objs[index].expert_code) ;          
				 if(confirm('是否通过该公司注册请求？')){
					$resource('/api/ac/ggc/reviewCompanyService/updateStateUp', {}, {}).save({'company_code':$scope.objs[index].company_code,'company_id':$scope.objs[index].company_id}, function(res){			              
                if(res.errcode === 0 || res.errcode === 10003){
						alert('操作成功');			
						$scope.getlist();
                }else{
                    alert(res.errmsg);
                }
            });
				
				 }		
				 
			}

             	$scope.back= function (index) {    
				console.log($scope.objs[index].expert_code) ;          
				 if(confirm('是否驳回该公司注册申请？')){
					$resource('/api/as/ggc/reviewCompany/updateStateDown', {}, {}).save({'expert_code':$scope.objs[index].expert_code}, function(res){			              
                if(res.errcode === 0 || res.errcode === 10003){
						alert('操作成功');			
						$scope.getlist();
                }else{
                    alert(res.errmsg);
                }
            });
				
				 }		
				 
			}       



};
