/**
 * 模态框
 */
module.exports = function ($scope, $state, $stateParams,$resource,$modal) {
	/* 分页
     * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.currentPage = 1;      //当前页码
    $scope.itemsPerPage = 10         //每页显示几条
	$scope.obj={};
	$scope.d={};

	$scope.rsarr = [{name:'推荐',value:'1'},
							{name:'不推荐',value:'0'}]

	$scope.pageChanged = function () {
       var para = {
            pageNo:$scope.currentPage,
            pageSize:$scope.itemsPerPage,
        };
		

		$resource('/api/as/ggc/reviewDistrictTourism/findByRecommendList', {}, {}).
			save({}, function(res){			
                 
                if(res.errcode === 0 || res.errcode === 10003){
					$scope.objs=res.data.results;									
					console.log(res);
                    console.log('区县审核列表返回值') 
					$scope.totalItems = res.data.totalRecord;

					for(var i = 0;i<$scope.objs.length;i++){
						if($scope.objs[i].recommend_district=='0'){
							$scope.objs[i].recommend_district='不推荐'
						}
						if($scope.objs[i].recommend_district=='1'){
							$scope.objs[i].recommend_district='推荐'
						}
					}

                }else{
                    alert(res.errmsg);
                }
            });


    };
	$scope.pageChanged();


			$scope.search=function(){
			var dic = {
				pageNo:$scope.currentPage,
            	pageSize:$scope.itemsPerPage,
				recommend_district:$scope.recommend,
				project_name:$scope.project_name
			}
				
				$resource('/api/as/ggc/reviewDistrictTourism/findByRecommendList', {}, {}).save(dic, function(res){
                if(res.errcode === 0 || res.errcode === 10003){
                    // console.log(res);
					$scope.objs=res.data.results;
                    $scope.totalItems = res.data.totalRecord;
                }else{
                    alert(res.errmsg);
                }
            });
			}

			$scope.apply= function (index) {    
				console.log($scope.objs[index].project_code) ;          
				 if(confirm('是否推荐此项目？')){
					$resource('/api/as/ggc/reviewDistrictTourism/updateByRecommend', {}, {}).save({'project_code':$scope.objs[index].project_code}, function(res){			              
                if(res.errcode === 0 || res.errcode === 10003){
						alert('推荐成功')			
						$scope.pageChanged();
                }else{
                    alert(res.errmsg);
                }
            });
				
				 }		
				 
			}

            $scope.toinfo= function (index) {
                 $scope.item = $scope.objs[index];
                console.log($scope.objs[index]);			
				var modalInstance = $modal.open({
					template: require('../views/projectinfo.html'),
					controller: 'projectinfo',
					size: 'lg',
					resolve: {
						items: function () {
							return $scope.item;
						}
					}
				});
				modalInstance.result.then(function (showResult) {	
						$scope.pageChanged();
				});
			}

}