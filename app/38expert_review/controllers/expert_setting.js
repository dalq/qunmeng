module.exports = function ($scope, $state, $stateParams,$resource,$modal,toaster) {
    console.log($stateParams.project_code);
    $scope.project_code = $stateParams.project_code;
    $scope.set = $stateParams.set;
    console.log($scope.set);
    console.log('上面是set')

      /* 分页
        * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10         //每页显示几条
    
    $scope.getlist = function () {
        var para = {
            pageNo:$scope.bigCurrentPage, 
            pageSize:$scope.itemsPerPage,
            project_code:$scope.project_code
        };

        //抽取专家
        if($scope.set=='1'){
             $resource('/api/ac/ggc/reviewService/randomExperts', {}, {}).
                save(para,function(res) {
                    console.log(para);
                    if (res.errcode !== 0) {
                        toaster.error({title:"",body:res.errmsg});
                        return;
                    }
                    console.log(para);
                    console.log(res);
                    console.log('上面是抽取res');

                      $resource('/api/as/ggc/reviewexpertset/findList', {}, {}).
                            save(para,function(res) {
                                console.log(para);
                                if (res.errcode !== 0) {
                                    toaster.error({title:"",body:res.errmsg});
                                    return;
                                }
                                console.log(para);
                                console.log(res);
                                console.log('上面是res');
                                $scope.objs = res.data.results;
                                $scope.bigTotalItems = res.data.totalRecord;

                                
        })
                    // $scope.objs = res.data.results;
                    // $scope.bigTotalItems = res.data.totalRecord;

            
        })
        // para = angular.extend($scope.parameters, para);
      

        //项目专家确认
        $resource('/api/as/ggc/reviewexpertset/getExpertRight', {}, {}).
        save({'project_code':$scope.project_code},function(res) {
            if (res.errcode !== 0) {
                toaster.error({title:"",body:res.errmsg});
                return;
            }
            console.log(para);
            console.log(res);
            console.log('上面是确认res');
            $scope.expertnum = res.data;
            $scope.bigTotalItems = res.data.totalRecord;
            if($scope.expertnum=='5'){
                $scope.queren = 'btn btn-block btn-success'
            }else{
                $scope.queren = 'btn btn-block btn-default'
            }
            
        })

        //项目专家评审
        $resource('/api/as/ggc/reviewexpertset/getExpertReview', {}, {}).
        save({'project_code':$scope.project_code},function(res) {
            if (res.errcode !== 0) {
                toaster.error({title:"",body:res.errmsg});
                return;
            }
            console.log(para);
            console.log(res);
            console.log('上面是评审res');
            $scope.review = res.data;
            $scope.bigTotalItems = res.data.totalRecord;
            if($scope.review=='5'){
                $scope.pingshen = 'btn btn-block btn-success';
                $scope.pingjunfen = 'btn btn-block btn-success';
            }else{
                $scope.pingshen = 'btn btn-block btn-default';
                $scope.pingjunfen = 'btn btn-block btn-default';
            }
            
        })

               //项目评分
        $resource('/api/as/ggc/reviewexpertset/getAvgScore', {}, {}).
        save({'project_code':$scope.project_code},function(res) {
            if (res.errcode !== 0) {
                toaster.error({title:"",body:res.errmsg});
                return;
            }
            console.log(para);
            console.log(res);
            console.log('上面是评分res');
            $scope.score = res.data;
            $scope.bigTotalItems = res.data.totalRecord;

            
        })
    }else{
               $resource('/api/as/ggc/reviewexpertset/findList', {}, {}).
        save(para,function(res) {
            console.log(para);
            if (res.errcode !== 0) {
                toaster.error({title:"",body:res.errmsg});
                return;
            }
            console.log(para);
            console.log(res);
            console.log('上面是res');
            $scope.objs = res.data.results;
            $scope.bigTotalItems = res.data.totalRecord;

            
        })

        //项目专家确认
        $resource('/api/as/ggc/reviewexpertset/getExpertRight', {}, {}).
        save({'project_code':$scope.project_code},function(res) {
            if (res.errcode !== 0) {
                toaster.error({title:"",body:res.errmsg});
                return;
            }
            console.log(para);
            console.log(res);
            console.log('上面是确认res');
            $scope.expertnum = res.data;
            $scope.bigTotalItems = res.data.totalRecord;
            if($scope.expertnum=='5'){
                $scope.queren = 'btn btn-block btn-success'
            }else{
                $scope.queren = 'btn btn-block btn-default'
            }
            
        })

        //项目专家评审
        $resource('/api/as/ggc/reviewexpertset/getExpertReview', {}, {}).
        save({'project_code':$scope.project_code},function(res) {
            if (res.errcode !== 0) {
                toaster.error({title:"",body:res.errmsg});
                return;
            }
            console.log(para);
            console.log(res);
            console.log('上面是评审res');
            $scope.review = res.data;
            $scope.bigTotalItems = res.data.totalRecord;
            if($scope.review=='5'){
                $scope.pingshen = 'btn btn-block btn-success';
                $scope.pingjunfen = 'btn btn-block btn-success';
            }else{
                $scope.pingshen = 'btn btn-block btn-default';
                $scope.pingjunfen = 'btn btn-block btn-default';
            }
            
        })

               //项目评分
        $resource('/api/as/ggc/reviewexpertset/getAvgScore', {}, {}).
        save({'project_code':$scope.project_code},function(res) {
            if (res.errcode !== 0) {
                toaster.error({title:"",body:res.errmsg});
                return;
            }
            console.log(para);
            console.log(res);
            console.log('上面是评分res');
            $scope.score = res.data;
            $scope.bigTotalItems = res.data.totalRecord;

            
        })

    }
    
        
    };
     
    $scope.getlist(); 

    $scope.back = function(){
        $state.go('app.project_appraisal');
    }
			


};
