module.exports = function ($scope, $state, $stateParams,$resource,$modal, $http, $q) {
    /* 分页
			* ========================================= */
			$scope.maxSize = 5;             //最多显示多少个按钮
			$scope.bigCurrentPage = 1;      //当前页码
			$scope.itemsPerPage = 10         //每页显示几条

			$scope.searchform = {};

			//页面需要的信息。
			$scope.page = {};

			var beforedata = {
				//项目类别列表
				'typearr':
				$http({
					'method': 'GET',
					'url': '/api/as/ggc/reviewCityResponsible/findAllProjectTypeList',
					// 'params': { 'type': 'sale_category' },
				}),
				//申报单位
				'companyarr':
				$http({
					'method': 'GET',
					'url': 'api/as/ggc/reviewCityResponsible/findAllCompanyList',
					// 'params': { 'type': 'ticket_sale_belong' },
				}),
				//项目名称
				'projectarr':
				$http({
					'method': 'GET',
					'url': '/api/as/ggc/reviewCityResponsible/findAllProjectList',
				}),
			};

			$q.all(beforedata).then(function (res) {

				//项目类别列表
				if (res.typearr.data.errcode === 0) {
				} else {
					alert('/api/as/ggc/reviewCityResponsible/findAllProjectTypeList' + res.typearr.data.errmsg);
					return;
				}
				//申报单位
				if (res.companyarr.data.errcode === 0) {
				} else {
					alert('api/as/ggc/reviewCityResponsible/findAllCompanyList' + res.companyarr.data.errmsg);
					return;
				}
				//项目名称
				if (res.projectarr.data.errcode === 0) {
				} else {
					alert('/api/as/ggc/reviewCityResponsible/findAllProjectList' + res.projectarr.data.errmsg);
					return;
				}

				$scope.page = {
					//项目类别列表
					'typearr': res.typearr.data.data,
					//申报单位
					'companyarr': res.companyarr.data.data,
					//项目名称
					'projectarr': res.projectarr.data.data,
					// 年度
					'yeararr' : [{'value' : '0', 'label' : '2017'}],
					// 区县/处室
					'areaarr' : [{'value' : '0', 'label' : '处室'},{'value' : '1', 'label' : '区县'}],
				};
				console.log($scope.page.typearr);
			});

			$scope.getlist = function () {
				var para = {
					pageNo:$scope.bigCurrentPage, 
					pageSize:$scope.itemsPerPage
				};
				para = angular.extend($scope.searchform, para);
				$resource('/api/as/ggc/reviewCityResponsible/findInfoList', {}, {}).
				save(para,function(res) {
					console.log(para);
					if (res.errcode !== 0) {
						toaster.error({title:"",body:res.errmsg});
						return;
					}
					console.log(res);
					$scope.objs = res.data.results;
					$scope.bigTotalItems = res.data.totalRecord;

					
				})
			};
			
			$scope.getlist(); 

			$scope.search=function(){
			var dic = {
				pageNo:$scope.currentPage,
            	pageSize:$scope.itemsPerPage,
				project_name:$scope.project_name,
				company_name:$scope.company_name
			}
				
				$resource('/api/as/ggc/reviewCityResponsible/findInfoList', {}, {}).save(dic, function(res){
                if(res.errcode === 0 || res.errcode === 10003){
                    // console.log(res);
					$scope.objs=res.data.results;
                    $scope.totalItems = res.data.totalRecord;
                }else{
                   toaster.error({title:"",body:res.errmsg});
                }
            });
			}



			


};
