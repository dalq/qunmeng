module.exports = function($scope, $stateParams, $state, $modal, $modalInstance,FileUploader,$resource,project_code,approvalflag,toaster){
    console.log(approvalflag);
    $scope.approvalflag = approvalflag;
    $scope.info = {
        'project_code' : project_code,
        'money' : '',
        'reason' : ''
    }; 
    $scope.recomandpara = {
        'project_code' : project_code,
        'money' : ''
    }
    $scope.fuhepara = {
        'project_code' : project_code,
        'plan_money' : ''
    }

    $scope.tjtypara = {
        'project_code' : project_code,
        'money' : '',
        'view_reason' : ''
    }
    // 详情
    if(approvalflag == 'tuijiantg'){
        $resource('/api/as/ggc/reviewCityResponsible/findByRecommend', {}, {}).///api/as/ggc/reviewPlanning/findInfo 
        save({'project_code' : project_code},function(res) {
            if (res.errcode !== 0) {
                toaster.error({title:"",body:res.errmsg});
                return;
            }
            $scope.info = res.data;
            $scope.info.money = res.data.money;
        })
    } else if(approvalflag == 'fhinfo' || approvalflag == 'zwbtg'){
        $resource('/api/as/ggc/reviewPlanning/findInfo', {}, {}).///api/as/ggc/reviewPlanning/findInfo 
        save({'project_code' : project_code},function(res) {
            if (res.errcode !== 0) {
                toaster.error({title:"",body:res.errmsg});
                return;
            }
            $scope.info = res.data;
            $scope.info.money = res.data.plan_money;
            $scope.info.reason = res.data.plan_reason;
            $scope.imgarr = res.data.director_agree_pdf.split(",");
            console.log($scope.imgarr);
            var array = res.data.director_agree_pdf.split(",");
            for(var i = 0; i < array.length; i++){
                if(array[i].lastIndexOf('.pdf') == -1){
                    console.log('图片');
                } else {
                    console.log('pdf文件')
                }
            }
        })
    } else if(approvalflag == 'tjinfo'){
        $resource('/api/as/ggc/reviewCityResponsible/findByRecommend', {}, {}).///api/as/ggc/reviewPlanning/findInfo 
        save({'project_code' : project_code},function(res) {
            if (res.errcode !== 0) {
                toaster.error({title:"",body:res.errmsg});
                return;
            }
            console.log('推荐详情');
            $scope.info = res.data;
            $scope.info.money = res.data.money;
            $scope.info.reason = res.data.view_reason;
        })
    }

    
    // 推荐详情 
    
    var url = '';
    $scope.ok = function(){ ///api/as/ggc/reviewPlanning/updateAdopt  复核通过
        console.log('ok');
         if(approvalflag == 'fhtg'){
            $scope.fuhepara.plan_money = $scope.info.money;
            $scope.fuhepara.plan_reason = $scope.info.reason;
            url = '/api/as/ggc/reviewPlanning/updateAdopt'; //复核通过
            $resource(url, {}, {}).
            save($scope.fuhepara,function(res) {
                if (res.errcode !== 0) {
                    toaster.error({title:"",body:res.errmsg});
                    return;
                }
                toaster.success({title:"",body:"复核"});
                $modalInstance.close();
            })
        } else if(approvalflag == 'tjty') {
            url = '/api/as/ggc/reviewCityResponsible/updateByRecommend'; // 推荐通过
            $scope.tjtypara.money = $scope.info.money;
            $scope.tjtypara.view_reason = $scope.info.reason;
            $resource(url, {}, {}).
            save($scope.tjtypara,function(res) {
                if (res.errcode !== 0) {
                    toaster.error({title:"",body:res.errmsg});
                    return;
                }
                toaster.success({title:"",body:"推荐同意"});
                $modalInstance.close();
            })
        } else if(approvalflag == 'tjinfo') {
            
        }
       
    };
    $scope.cancel = function () {
        $modalInstance.close();
    };

};