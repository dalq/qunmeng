/**
 * 模态框
 */
module.exports = function ($scope, $state, $stateParams,$resource,$modal) {
	/* 分页
     * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.currentPage = 1;      //当前页码
    $scope.itemsPerPage = 10         //每页显示几条
	$scope.obj={};
	$scope.d={};


	$scope.pageChanged = function () {
       var para = {
            pageNo:$scope.currentPage,
            pageSize:$scope.itemsPerPage,
        };
		

        $resource('/api/as/ggc/reviewGuide/findInfoList', {}, {}).
			save(para, function(res){			
                 
                if(res.errcode === 0 || res.errcode === 10003){
					$scope.a=res.data.results;					
					console.log($scope.a);
                    console.log('上面是申报指南列表返回值') 
					$scope.bigTotalItems = res.data.totalRecord;

                }else{
                    alert(res.errmsg);
                }
            });

    };
	$scope.pageChanged();


			$scope.search=function(){
			var dic = {
				pageNo:$scope.currentPage,
            	pageSize:$scope.itemsPerPage,
				
			}
				
				$resource('', {}, {}).save(dic, function(res){
                if(res.errcode === 0 || res.errcode === 10003){
                    // console.log(res);
					$scope.a=res.data.results;
					console.log($scope.a);
                    $scope.bigTotalItems = res.data.totalRecord;
                }else{
                    alert(res.errmsg);
                }
            });
			}


             $scope.add = function () {
				  $scope.item = $scope.a;			
				var modalInstance = $modal.open({
					template: require('../views/addnotice.html'),
					controller: 'addnotice',
					size: 'lg',
					resolve: {
						items: function () {
							return $scope.item;
						}
					}
				});
				modalInstance.result.then(function (showResult) {	
						$scope.pageChanged();
				});
			}

			// $scope.completeinfo = function () {
			// 	$state.go('app.completeinfo',{'company_id':$scope.b.company_id})
			// }

             $scope.toinfo= function (index) {
                 $scope.item = $scope.a[index];			
				var modalInstance = $modal.open({
					template: require('../views/noticeinfo.html'),
					controller: 'noticeinfo',
					size: 'lg',
					resolve: {
						items: function () {
							return $scope.item;
						}
					}
				});
				modalInstance.result.then(function (showResult) {	
						$scope.pageChanged();
				});
			}

			$scope.edit= function (index) {
                 $scope.item = $scope.a[index];			
				var modalInstance = $modal.open({
					template: require('../views/noticeupdate.html'),
					controller: 'noticeupdate',
					size: 'lg',
					resolve: {
						items: function () {
							return $scope.item;
						}
					}
				});
				modalInstance.result.then(function (showResult) {	
						$scope.pageChanged();
				});
			}
			$scope.issue= function (index) {    
				console.log($scope.a[index].id) ;          
				 if(confirm('是否发布该条指南？')){
					$resource('/api/as/ggc/reviewGuide/updateUp', {}, {}).save({'id':$scope.a[index].id}, function(res){			              
                if(res.errcode === 0 || res.errcode === 10003){
						alert('发布成功')			
						$scope.pageChanged();
                }else{
                    alert(res.errmsg);
                }
            });
				
				 }		
				 
			}


			$scope.down= function (index) {    
				console.log($scope.a[index].id) ;          
				 if(confirm('下线？')){
					$resource('/api/as/ggc/reviewGuide/updateDown', {}, {}).save({'id':$scope.a[index].id}, function(res){			              
                if(res.errcode === 0 || res.errcode === 10003){
						alert('下线成功')			
						$scope.pageChanged();
                }else{
                    alert(res.errmsg);
                }
            });
				
				 }		
				 
			}
};

