module.exports = function($scope, $stateParams, $state, $modal, $modalInstance,FileUploader,$resource,toaster,project_code,approvalflag){  
    console.log(project_code,approvalflag);
    $scope.approvalflag = approvalflag;
    $scope.info = {
        'reason' : ''
    }
    // 详情
    if(approvalflag == 'thinfo'){
        $resource('/api/as/ggc/reviewCityResponsible/findByRecommend', {}, {}).
        save({'project_code' : project_code},function(res) {
            if (res.errcode !== 0) {
                toaster.error({title:"",body:res.errmsg});
                return;
            }
            console.log('推荐退回原因');
            $scope.info = res.data;
            $scope.info.reason = res.data.recommend_city_back;
            console.log($scope.flaginfo);
        })
    } else if(approvalflag == 'fhthinfo'){
        $resource('/api/as/ggc/reviewPlanning/findInfo', {}, {}).
        save({'project_code' : project_code},function(res) {
            if (res.errcode !== 0) {
                toaster.error({title:"",body:res.errmsg});
                return;
            }
            console.log('复核驳回详情');
            $scope.info = res.data;
            $scope.info.reason = res.data.plan_back_reason;
            console.log($scope.flaginfo);
        })
    }
    var url = '';
    $scope.ok = function(){
        console.log('ok');
        if(approvalflag == 'spbh'){
            url = '/api/as/ggc/reviewCityResponsible/updateReject';
            if($scope.info.reason == ''){
                toaster.error({title:"",body:"驳回原因不能为空"});
            } else {
                $resource(url, {}, {}).
                save({'project_code' : project_code, 'back_reason' : $scope.info.reason},function(res) {
                    if (res.errcode !== 0) {
                        toaster.error({title:"",body:res.errmsg});
                        return;
                    }
                    $modalInstance.close();
                })
            }
        } 
        else if(approvalflag == 'sptt'){
            url = '/api/as/ggc/reviewCityResponsible/updateBack';
            if($scope.info.reason == ''){
                toaster.error({title:"",body:"淘汰原因不能为空"});
            } else {
                $resource(url, {}, {}).
                save({'project_code' : project_code, 'eliminate_reason' : $scope.info.reason},function(res) {
                    if (res.errcode !== 0) {
                        toaster.error({title:"",body:res.errmsg});
                        return;
                    }
                    $modalInstance.close();
                })
            }
        } else if(approvalflag == 'queren'){
            url = ''
        } else if(approvalflag == 'buqueren'){
            url = ''
        } else if(approvalflag == 'tjth'){ // 推荐退回
            url = '/api/as/ggc/reviewCityResponsible/updateByRecommendCityBack';
            if($scope.info.reason == ''){
                toaster.error({title:"",body:"退回原因不能为空"});
            } else {
                $resource(url, {}, {}).
                save({'project_code' : project_code, 'recommend_city_back' : $scope.info.reason},function(res) {
                    if (res.errcode !== 0) {
                        toaster.error({title:"",body:res.errmsg});
                        return;
                    }
                    $modalInstance.close();
                })
            }
        } else if(approvalflag == 'fhbh'){
            url = '/api/as/ggc/reviewPlanning/updateBack';
            if($scope.info.reason == ''){
                toaster.error({title:"",body:"复核驳回原因不能为空"});
            } else {
                $resource(url, {}, {}).
                save({'project_code' : project_code, 'plan_back_reason' : $scope.info.reason},function(res) {
                    if (res.errcode !== 0) {
                        toaster.error({title:"",body:res.errmsg});
                        return;
                    }
                    $modalInstance.close();
                })
            }
        }
        
    }
    $scope.cancel = function () {
        $modalInstance.close();
    };

};