module.exports = function($scope, $state, $stateParams, $resource,FileUploader,toaster, $http, $q){
	$scope.reviewflag = 0;
    return {

		restrict: 'AE',
		template: require('../views/project_review.html'),
		replace: true,
		scope: {
			'saleobj': '=',
			'funobj': '=',
			'baseinfo': '=',
			'util': '=',
		},
		link: function (scope, elements, attrs) {
            //页面需要的信息。
			// $scope.page = {};
            // var baseinfo = [];

			var beforedata = {
				//项目类别列表
				'typearr':
				$http({
					'method': 'GET',
					'url': '/api/as/ggc/reviewCityResponsible/findAllProjectTypeList',
					// 'params': { 'type': 'sale_category' },
				}),
				//申报单位
				'companyarr':
				$http({
					'method': 'GET',
					'url': 'api/as/ggc/reviewCityResponsible/findAllCompanyList',
					// 'params': { 'type': 'ticket_sale_belong' },
				}),
				//项目名称
				'projectarr':
				$http({
					'method': 'GET',
					'url': '/api/as/ggc/reviewCityResponsible/findAllProjectList',
				}),
			};

			$q.all(beforedata).then(function (res) {

				//项目类别列表
				if (res.typearr.data.errcode === 0) {
				} else {
					alert('/api/as/ggc/reviewCityResponsible/findAllProjectTypeList' + res.typearr.data.errmsg);
					return;
				}
				//申报单位
				if (res.companyarr.data.errcode === 0) {
				} else {
					alert('api/as/ggc/reviewCityResponsible/findAllCompanyList' + res.companyarr.data.errmsg);
					return;
				}
				//项目名称
				if (res.projectarr.data.errcode === 0) {
				} else {
					alert('/api/as/ggc/reviewCityResponsible/findAllProjectList' + res.projectarr.data.errmsg);
					return;
				}

				scope.baseinfo = {
					//项目类别列表
					'typearr': res.typearr.data.data,
					//申报单位
					'companyarr': res.companyarr.data.data,
					//项目名称
					'projectarr': res.projectarr.data.data,
					// 年度
					'yeararr' : [{'value' : '0', 'label' : '2017'}],
					// 区县/处室
					'areaarr' : [{'value' : '0', 'label' : '处室'},{'value' : '1', 'label' : '区县'}],
				};
               
			});
            console.log(scope.baseinfo);

        }
    }
            
}