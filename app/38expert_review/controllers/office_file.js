module.exports = function($scope, $stateParams, $state, $modal, $modalInstance,FileUploader,$resource,toaster, project_code){  
    $scope.info = {
        'project_code' : project_code,
        'director_agree_pdf' : ''
    }
    $scope.image = function(){
        var para = $state.get('app.imageupload');
        //设置上传文件夹，以自己业务命名
        angular.extend(para, {
            resolve : {  
                'dir' : function(){
                    return 't1';
                }
            } 
        })
        console.log(para);
        var modalInstance = $modal.open(para);
        modalInstance.opened.then(function() {// 模态窗口打开之后执行的函数  
            console.log('modal is opened');  
        });  
        modalInstance.result.then(function(result) {  
            console.log(result);  
            $scope.imaarr = result;
            $scope.imagearr = JSON.stringify(result);
            console.log($scope.imagearr);
            console.log(result.join(","));
            $scope.info.director_agree_pdf = result.join(",");

        }, function(reason) {  
            console.log(reason);// 点击空白区域，总会输出backdrop  
            // click，点击取消，则会暑促cancel  
            $log.info('Modal dismissed at: ' + new Date());  
        }); 
    };
    $scope.ok = function(){
        console.log($scope.info.director_agree_pdf);
        $resource('api/as/ggc/reviewPlanning/updateAgree', {}, {}).///api/as/ggc/reviewPlanning/findInfo 
        save($scope.info,function(res) {
            if (res.errcode !== 0) {
                toaster.error({title:"",body:res.errmsg});
                return;
            }
            console.log(res);
            toaster.success({title:"",body:"上传文件成功"});
            $modalInstance.close();

        })
    }
    $scope.cancel = function () {
        $modalInstance.close();
    };

};