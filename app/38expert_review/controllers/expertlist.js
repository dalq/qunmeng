module.exports = function ($scope, $state, $stateParams,$resource,$modal,toaster) {
    /* 分页
        * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10;         //每页显示几条
    $scope.b={};
    $scope.getlist = function () {
        var para = {
            pageNo:$scope.bigCurrentPage, 
            pageSize:$scope.itemsPerPage
        };
        // para = angular.extend($scope.parameters, para);
        $resource('/api/ac/ggc/reviewExpertService/findInfoList', {}, {}).
        save(para,function(res) {
            if (res.errcode !== 0) {
                toaster.error({title:"",body:res.errmsg});
                return;
            }
            console.log(res);
            $scope.objs = res.data.results;
             $scope.bigTotalItems = res.data.totalRecord;

            
        })
    };
     
    $scope.getlist(); 

    $scope.search = function () {
        var para = {
            pageNo:$scope.bigCurrentPage, 
            pageSize:$scope.itemsPerPage,
            // field_type:$scope.b.field_type,
            expert_name:$scope.expert_name,
            mobile:$scope.mobile
        };
        // para = angular.extend($scope.parameters, para);
        $resource('/api/ac/ggc/reviewExpertService/findInfoList', {}, {}).
        save(para,function(res) {
            if (res.errcode !== 0) {
                toaster.error({title:"",body:res.errmsg});
                return;
            }

            $scope.objs = res.data.results;
            $scope.bigTotalItems = res.data.totalRecord;

            
        })
    };
    
    $scope.areaarr=[
        {name:'环境专家',value:'0'},
        {name:'城建专家',value:'1'},
        {name:'农药专家',value:'2'},
        ]

    $scope.toinfo = function(index){
        $scope.item = $scope.objs[index];
        console.log($scope.item);
         var modalInstance = $modal.open({
            template: require('../views/expertinfo.html'),
            controller: 'expertinfo',
            size: 'lg',
            resolve: {
                items: function () {
                    return $scope.item;
                },
            }
        });
        modalInstance.result.then(function (showResult) {	
                $scope.getlist();
        });
    } 

    $scope.update = function(index){
        $scope.item = $scope.objs[index];
        console.log($scope.item);
         var modalInstance = $modal.open({
            template: require('../views/expertupdate.html'),
            controller: 'expertupdate',
            size: 'lg',
            resolve: {
                items: function () {
                    return $scope.item;
                },
            }
        });
        modalInstance.result.then(function (showResult) {	
                $scope.getlist();
        });
    } 

    			$scope.delete= function (index) {    
				console.log($scope.objs[index].expert_code) ;          
				 if(confirm('是否删除专家信息？')){
					$resource('/api/ac/ggc/reviewExpertService/delete', {}, {}).save({'expert_code':$scope.objs[index].expert_code}, function(res){			              
                if(res.errcode === 0 || res.errcode === 10003){
						alert('删除成功')			
						$scope.getlist();
                }else{
                    alert(res.errmsg);
                }
            });
				
				 }		
				 
			}

        	$scope.apply= function (index) {    
				console.log($scope.objs[index].expert_code) ;          
				 if(confirm('是否申请纳入专家库？')){
					$resource('/api/ac/ggc/reviewExpertService/updateApply', {}, {}).save({'expert_code':$scope.objs[index].expert_code}, function(res){			              
                if(res.errcode === 0 || res.errcode === 10003){
						alert('申请成功');			
						$scope.getlist();
                }else{
                    alert(res.errmsg);
                }
            });
				
				 }		
				 
			}

             	$scope.giveup= function (index) {    
				console.log($scope.objs[index].expert_code) ;          
				 if(confirm('是否放弃申请？')){
					$resource('/api/ac/ggc/reviewExpertService/updateNotApply', {}, {}).save({'expert_code':$scope.objs[index].expert_code}, function(res){			              
                if(res.errcode === 0 || res.errcode === 10003){
						alert('操作成功');			
						$scope.getlist();
                }else{
                    alert(res.errmsg);
                }
            });
				
				 }		
				 
			}       


            $scope.add = function(){
                $state.go('app.expertrec');
            }

    $scope.close = function(){
        
    }


};
