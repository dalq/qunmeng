var App = angular.module('expert_review', []);

App.config(require('./router'));
App.factory('expert_reviewservice', require('./service'));
App.controller('notice',require('./controllers/notice'));//公告列表
App.controller('register',require('./controllers/register'));//企业注册
App.controller('examine',require('./controllers/examine'));
App.controller('approvalinfo',require('./controllers/approvalinfo'));
App.controller('projectsearch',require('./controllers/projectsearch'));
App.controller('project_review',require('./controllers/project_review')); // 项目复核
App.controller('project_recommendation',require('./controllers/project_recommendation')); // 项目推荐
App.controller('project_appraisal',require('./controllers/project_appraisal')); // 项目评审 expert_setting
App.controller('expert_setting',require('./controllers/expert_setting')); // 专家设定
App.controller('project_acceptance',require('./controllers/project_acceptance')); // 项目验收  
App.controller('project_confirmation',require('./controllers/project_confirmation')); // 项目立项   confirminfo
App.controller('confirminfo',require('./controllers/confirminfo')); // 确认/不确认填写信息 reporting_progress
App.controller('reporting_progress',require('./controllers/reporting_progress')); // 提报进度 warehousing_expert
App.controller('warehousing_expert',require('./controllers/warehousing_expert')); // 入库专家 expert_review
App.controller('expert_review',require('./controllers/expert_review')); // 专家评审expert_evaluation
App.controller('expert_evaluation',require('./controllers/expert_evaluation')); // 专家评估 
App.controller('recommendinfo',require('./controllers/recommendinfo')); // 推荐详情 
App.controller('office_file',require('./controllers/office_file')); //组委办上传pdf文件  expert_first
App.controller('expert_first',require('./controllers/expert_first')); //专家优先填写评分



App.directive('pendingapprovallist',require('./directives/pendingapprovallist')); // 待审批
App.directive('approvallist',require('./directives/approvallist')); // 审批通过
App.directive('rejectlist',require('./directives/rejectlist')); // 驳回 
App.directive('eliminatelist',require('./directives/eliminatelist')); // 淘汰


App.directive('recommendlist',require('./directives/recommendlist')); // 待推荐
App.directive('recommendedlist',require('./directives/recommendedlist')); // 已推荐
App.directive('returnedlist',require('./directives/returnedlist')); // 已退回

App.directive('pendingreviewlist',require('./directives/pendingreviewlist')); // 待复核
App.directive('reviewedlist',require('./directives/reviewedlist')); // 已复核
App.directive('notpasslist',require('./directives/notpasslist')); // 未通过 

App.directive('pendingexpertreview',require('./directives/pendingexpertreview')); // 待评审
App.directive('expertreviewed',require('./directives/expertreviewed')); // 已评审




App.controller('project_list',require('./controllers/project_list'));//项目列表
App.controller('addproject',require('./controllers/addproject'));//添加项目
App.controller('projectinfo',require('./controllers/projectinfo'));//项目详情
App.controller('projectinfofh',require('./controllers/projectinfofh'));//项目复核详情
App.controller('projectedit',require('./controllers/projectedit'));//项目编辑
App.controller('completeinfo',require('./controllers/completeinfo'));//完善企业信息
App.controller('project_progress',require('./controllers/project_progress'));//项目流程

App.controller('expertlist',require('./controllers/expertlist'));//专家列表
App.controller('expertrec',require('./controllers/expertrec'));//专家推荐
App.controller('expertinfo',require('./controllers/expertinfo'));//专家详情
App.controller('expertupdate',require('./controllers/expertupdate'));//专家信息修改
App.controller('expert_review_list',require('./controllers/expert_review_list'));//专家审核列表

App.controller('addnotice',require('./controllers/addnotice'));//创建新公告
App.controller('noticeinfo',require('./controllers/noticeinfo'));//公告详情
App.controller('noticeupdate',require('./controllers/noticeupdate'));//公告编辑

App.controller('distinct_review_list',require('./controllers/distinct_review_list'));//区县审核列表

App.controller('score',require('./controllers/score'));//专家评分
App.controller('scoreinfo',require('./controllers/scoreinfo'));//专家评分详情
//企业审核
App.controller('company_check',require('./controllers/company_check'));

module.exports = App;