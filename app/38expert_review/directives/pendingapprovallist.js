module.exports = function ($resource, $state, $http, $q, FileUploader, toaster,$modal) {
  
	return {

		restrict: 'AE',
		template: require('../views/pendingapprovallist.html'),
		replace: true,
		scope: {
			'saleobj': '=',
			'funobj': '=',
			'baseinfo': '=',
			'util': '=',
			'examineflag' : '='
		},
		link: function (scope, elements, attrs) {
			/* 分页
			* ========================================= */
			scope.maxSize = 5;             //最多显示多少个按钮
			scope.bigCurrentPage = 1;      //当前页码
			scope.itemsPerPage = 10         //每页显示几条
			
			scope.getlist = function () {
				var para = {
					pageNo:scope.bigCurrentPage, 
					pageSize:scope.itemsPerPage,
					'project_state' : '1'
				};
				// para = angular.extend($scope.parameters, para);
				$resource('/api/as/ggc/reviewCityResponsible/approvalList', {}, {}).
				save(para,function(res) {
					console.log(para);
					if (res.errcode !== 0) {
						toaster.error({title:"",body:res.errmsg});
						return;
					}
					console.log(res);
					scope.objs = res.data.results;
					scope.bigTotalItems = res.data.totalRecord;

					
				})
			};
			
			// scope.getlist(); 

			scope.$watch('examineflag', function(){
				scope.getlist();
			}, true)


			scope.info = function(project_name,project_code,company_name,apply_time){
				var btnflag = '0';
				var modalInstance = $modal.open({
					template: require('../views/approvalinfo.html'),
					controller: 'approvalinfo',
					size: 'lg',
					resolve: {
						project_name: function () {
							return project_name;
						},
						project_code: function () {
							return project_code;
						},
						company_name: function () {
							return company_name;
						},
						apply_time: function () {
							return apply_time;
						},
						btnflag: function () {
							return btnflag;
						}
						
					}
				});
				modalInstance.result.then(function (showResult) {	
					// scope.getlist();
					scope.examineflag++;
					console.log(cope.examineflag)
					console.log('cope.examineflag')
				});
        
			};


			scope.image = function(){
				var para = $state.get('app.imageupload');
				//设置上传文件夹，以自己业务命名
				angular.extend(para, {
					resolve : {  
						'dir' : function(){
							return 't1';
						}
					} 
				})
				console.log(para);
				var modalInstance = $modal.open(para);
				modalInstance.opened.then(function() {// 模态窗口打开之后执行的函数  
					console.log('modal is opened');  
				});  
				modalInstance.result.then(function(result) {  
					console.log(result);  
					scope.imagearr = JSON.stringify(result);
					// console.log(scope.imagearr);
					var array = result;
					scope.info.img = array[0];
					console.log(scope.info.img);
					// form.result['templete_lock_data_json'] = JSON.stringify(result.lock);
					// form.result['templete_check_data_json'] = result.unlock;

				}, function(reason) {  
					console.log(reason);// 点击空白区域，总会输出backdrop  
					// click，点击取消，则会暑促cancel  
					$log.info('Modal dismissed at: ' + new Date());  
				}); 
			};
		}
	}
}