module.exports = function ($resource, $state, $http, $q, FileUploader, $modal, toaster) {
  
	return {

		restrict: 'AE',
		template: require('../views/approvallist.html'),
		replace: true,
		scope: {
			'saleobj': '=',
			'funobj': '=',
			'baseinfo': '=',
			'util': '=',
			'examineflag' : '='
		},
		link: function (scope, elements, attrs) {
			/* 分页
			* ========================================= */
			scope.maxSize = 5;             //最多显示多少个按钮
			scope.bigCurrentPage = 1;      //当前页码
			scope.itemsPerPage = 10         //每页显示几条
			
			scope.getlist = function () {
				var para = {
					pageNo:scope.bigCurrentPage, 
					pageSize:scope.itemsPerPage,
					'project_state' : '3'
				};
				// para = angular.extend($scope.parameters, para);
				$resource('/api/as/ggc/reviewCityResponsible/approvalList', {}, {}).
				save(para,function(res) {
					console.log(para);
					if (res.errcode !== 0) {
						toaster.error({title:"",body:res.errmsg});
						return;
					}
					console.log(res);
					scope.objs = res.data.results;
					scope.bigTotalItems = res.data.totalRecord;

					
				})
			};

			scope.$watch('examineflag', function(){
				scope.getlist();
			}, true)
			
			// scope.getlist(); 

			scope.info = function(obj){
				var btnflag = '1';
				var modalInstance = $modal.open({
					template: require('../views/approvalinfo.html'),
					controller: 'approvalinfo',
					size: 'lg',
					resolve: {
						project_name: function () {
							return obj.project_name;
						},
						project_code: function () {
							return obj.project_code;
						},
						company_name: function () {
							return obj.company_name;
						},
						apply_time: function () {
							return obj.apply_time;
						},
						btnflag: function () {
							return btnflag;
						}
						
					}
				});
				modalInstance.result.then(function (showResult) {	
					scope.examineflag++;
					// console.log(cope.examineflag)
					// console.log('cope.examineflag')
				});
        
			};

        }
	}
}