module.exports = function ($resource, $state, $http, $q, FileUploader, toaster,$modal) {
  
	return {

		restrict: 'AE',
		template: require('../views/notpasslist.html'),
		replace: true,
		scope: {
			'saleobj': '=',
			'funobj': '=',
			'baseinfo': '=',
			'util': '=',
			'reviewflag' : '='
		},
		link: function (scope, elements, attrs) {
			/* 分页
			* ========================================= */
			scope.maxSize = 5;             //最多显示多少个按钮
			scope.bigCurrentPage = 1;      //当前页码
			scope.itemsPerPage = 10         //每页显示几条
			
			scope.getlist = function () {
				var para = {
					pageNo:scope.bigCurrentPage, 
					pageSize:scope.itemsPerPage,
					'project_state' : '2'
				};
				// para = angular.extend($scope.parameters, para);
				$resource('/api/as/ggc/reviewPlanning/findInfoList', {}, {}).
				save(para,function(res) {
					console.log(para);
					if (res.errcode !== 0) {
						toaster.error({title:"",body:res.errmsg});
						return;
					}
					console.log(res);
					scope.objs = res.data.results;
					scope.bigTotalItems = res.data.totalRecord;

					
				})
			};

						$scope.search = function(){
        var para = {
            		pageNo:scope.bigCurrentPage, 
					pageSize:scope.itemsPerPage,
					'project_state' : '2',
					project_name:$scope.project_name,
					company_name:$scope.company_name
        };
        // para = angular.extend($scope.parameters, para);
        $resource('/api/as/ggc/reviewPlanning/findInfoList', {}, {}).
        save(para,function(res) {
            if (res.errcode !== 0) {
                toaster.error({title:"",body:res.errmsg});
                return;
            }
            console.log(res);
            console.log('上面是res');
            $scope.objs = res.data.results;
            $scope.bigTotalItems = res.data.totalRecord;

            
        })
    }
			scope.$watch('reviewflag', function(){
				scope.getlist();
			}, true)
			// scope.getlist(); 

			scope.backinfo = function(project_code){
				var approvalflag = 'fhthinfo';
				var modalInstance = $modal.open({
					template: require('../views/confirminfo.html'),
					controller: 'confirminfo',
					size: 'lg',
					resolve: {
						project_code: function () {
							return project_code;
						},
						approvalflag: function () {
							return approvalflag;
						},
					}
				});
				modalInstance.result.then(function (showResult) {	
					scope.reviewflag++;
					// scope.getlist();
				});
			}
        }
	}
}