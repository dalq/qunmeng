module.exports = function ($resource, $state, $http, $q, FileUploader, toaster, $modal) {
  
	return {

		restrict: 'AE',
		template: require('../views/pendingreviewlist.html'),
		replace: true,
		scope: {
			'saleobj': '=',
			'funobj': '=',
			'baseinfo': '=',
			'util': '=',
			'reviewflag' : '=',
		},
		link: function (scope, elements, attrs) {
			console.log(scope.baseinfo);
			/* 分页
			* ========================================= */
			scope.maxSize = 5;             //最多显示多少个按钮
			scope.bigCurrentPage = 1;      //当前页码
			scope.itemsPerPage = 10         //每页显示几条
			
			scope.getlist = function () {
				var para = {
					pageNo:scope.bigCurrentPage, 
					pageSize:scope.itemsPerPage,
					'project_state' : '3'
				};
				// para = angular.extend($scope.parameters, para);
				$resource('/api/as/ggc/reviewPlanning/findInfoList', {}, {}).
				save(para,function(res) {
					console.log(para);
					if (res.errcode !== 0) {
						toaster.error({title:"",body:res.errmsg});
						return;
					}
					console.log(res);
					scope.objs = res.data.results;
					scope.bigTotalItems = res.data.totalRecord;

					
				})
			};

									$scope.search = function(){
        var para = {
            		pageNo:scope.bigCurrentPage, 
					pageSize:scope.itemsPerPage,
					'project_state' : '3',
					project_name:$scope.project_name,
					company_name:$scope.company_name
        };
        // para = angular.extend($scope.parameters, para);
        $resource('/api/as/ggc/reviewPlanning/findInfoList', {}, {}).
        save(para,function(res) {
            if (res.errcode !== 0) {
                toaster.error({title:"",body:res.errmsg});
                return;
            }
            console.log(res);
            console.log('上面是res');
            $scope.objs = res.data.results;
            $scope.bigTotalItems = res.data.totalRecord;

            
        })
    }

			scope.$watch('reviewflag', function(){
				scope.getlist();
			}, true)
			
			// scope.getlist(); 
			// 复核通过
			scope.agree = function(project_code){
				var approvalflag = 'fhtg';
				var modalInstance = $modal.open({
					template: require('../views/recommendinfo.html'),
					controller: 'recommendinfo',
					size: 'lg',
					resolve: {
						project_code: function () {
							return project_code;
						},
						approvalflag: function () {
							return approvalflag;
						},
					}
				});
				modalInstance.result.then(function (showResult) {
					scope.reviewflag++;	
					// scope.getlist();
				});
			}
			// 复核驳回
			scope.disagree = function(project_code){
				var approvalflag = 'fhbh';
				var modalInstance = $modal.open({
					template: require('../views/confirminfo.html'),
					controller: 'confirminfo',
					size: 'lg',
					resolve: {
						project_code: function () {
							return project_code;
						},
						approvalflag: function () {
							return approvalflag;
						},
					}
				});
				modalInstance.result.then(function (showResult) {	
					// scope.getlist();
					scope.reviewflag++;
					console.log(cope.reviewflag)
					console.log('cope.reviewflag')
				});
			}

			// 复核详情
			scope.toinfo = function(index){
				var project_code = scope.objs[index].project_code;
				var flag = '0';
				var approvalflag = 'fhtg';
				var modalInstance = $modal.open({
					template: require('../views/projectinfofh.html'),
					controller: 'projectinfofh',
					size: 'lg',
					resolve: {
						items: function () {
							return project_code;														
						},
						
					}
				});
				modalInstance.result.then(function (showResult) {
					scope.reviewflag++;	
					// scope.getlist();
				});
			}
        }
	}
}