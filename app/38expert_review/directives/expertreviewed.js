module.exports = function ($resource, $state, $http, $q, FileUploader, toaster,$modal) {
  
	return {

		restrict: 'AE',
		template: require('../views/expertreviewed.html'),
		replace: true,
		scope: {
			'saleobj': '=',
			'funobj': '=',
			'baseinfo': '=',
			'util': '=',
			'expertflag' : '='
		},
		link: function (scope, elements, attrs) {
			/* 分页
			* ========================================= */
			scope.maxSize = 5;             //最多显示多少个按钮
			scope.bigCurrentPage = 1;      //当前页码
			scope.itemsPerPage = 10         //每页显示几条
			
			scope.getlist = function () {
				var para = {				
					review_state:'1'
				};
				// para = angular.extend($scope.parameters, para);
				$resource('/api/ac/ggc/reviewProExpertService/findExpertProject', {}, {}).
				save(para,function(res) {
					console.log(para);
					if (res.errcode !== 0) {
						toaster.error({title:"",body:res.errmsg});
						return;
					}
					console.log(res);
					console.log('已评审res')
					scope.objs = res.data;
					scope.bigTotalItems = res.data.totalRecord;

					
				})
			};
			scope.$watch('expertflag', function(){
				scope.getlist();
			}, true)

		scope.info = function(index){
        scope.item = scope.objs[index];
        	console.log(scope.item);
         	var modalInstance = $modal.open({
            template: require('../views/scoreinfo.html'),
            controller: 'scoreinfo',
            size: 'lg',
            resolve: {
                items: function () {
                    return scope.item;
                },
            }
        });
        modalInstance.result.then(function (showResult) {	
                scope.getlist();
        });
    }  

        }
	}
}