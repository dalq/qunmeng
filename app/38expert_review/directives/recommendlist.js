module.exports = function ($resource, $state, $http, $q, FileUploader, $modal, toaster) {
  
	return {

		restrict: 'AE',
		template: require('../views/recommendlist.html'),
		replace: true,
		scope: {
			'saleobj': '=',
			'funobj': '=',
			'baseinfo': '=',
			'util': '=',
			'recommandflag': '='
		},
		link: function (scope, elements, attrs, $http, $q) {

            /* 分页
			* ========================================= */
			scope.maxSize = 5;             //最多显示多少个按钮
			scope.bigCurrentPage = 1;      //当前页码
			scope.itemsPerPage = 10         //每页显示几条
			
			scope.getlist = function () {
				var para = {
					pageNo:scope.bigCurrentPage, 
					pageSize:scope.itemsPerPage,
					'recommend_city' : '0',
					'project_state' : '3'
				};
				// para = angular.extend($scope.parameters, para);
				$resource('/api/as/ggc/reviewCityResponsible/findeRecommendList', {}, {}).
				save(para,function(res) {
					console.log(para);
					if (res.errcode !== 0) {
						toaster.error({title:"",body:res.errmsg});
						return;
					}
					console.log(res);
					scope.objs = res.data.results;
					scope.bigTotalItems = res.data.totalRecord;

					
				})
			};

			$scope.search = function(){
        var para = {
            		pageNo:scope.bigCurrentPage, 
					pageSize:scope.itemsPerPage,
					'recommend_city' : '0',
					'project_state' : '3',
					project_name:$scope.project_name,
					company_name:$scope.company_name
        };
        // para = angular.extend($scope.parameters, para);
        $resource('/api/as/ggc/reviewCityResponsible/findeRecommendList', {}, {}).
        save(para,function(res) {
            if (res.errcode !== 0) {
                toaster.error({title:"",body:res.errmsg});
                return;
            }
            console.log(para);
            console.log(res);
            console.log('上面是res');
            $scope.objs = res.data.results;
            $scope.bigTotalItems = res.data.totalRecord;

            
        })
    }
			scope.$watch('recommandflag', function(){
				scope.getlist();
			}, true)
			// scope.getlist(); 
			// 推荐同意
			scope.agree = function(project_code){
				var approvalflag = 'tjty';
				var modalInstance = $modal.open({
					template: require('../views/recommendinfo.html'),
					controller: 'recommendinfo',
					size: 'lg',
					resolve: {
						project_code: function () {
							return project_code;
						},
						approvalflag: function () {
							return approvalflag;
						},
					}
				});
				modalInstance.result.then(function (showResult) {
					scope.recommandflag++;
					// scope.getlist();
				});
			}
			// 推荐退回
			scope.disagree = function(project_code){
				var approvalflag = 'tjth';
				var modalInstance = $modal.open({
					template: require('../views/confirminfo.html'),
					controller: 'confirminfo',
					size: 'lg',
					resolve: {
						project_code: function () {
							return project_code;
						},
						approvalflag: function () {
							return approvalflag;
						},
					}
				});
				modalInstance.result.then(function (showResult) {	
					scope.recommandflag++;
					// scope.getlist();
				});
			}
        }
	}
}