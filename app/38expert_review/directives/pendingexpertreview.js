module.exports = function ($resource, $state, $http, $q, FileUploader, toaster,$modal) {
  
	return {

		restrict: 'AE',
		template: require('../views/pendingexpertreview.html'),
		replace: true,
		scope: {
			'saleobj': '=',
			'funobj': '=',
			'baseinfo': '=',
			'util': '=',
			'expertflag' : '='
		},
		link: function (scope, elements, attrs) {
			/* 分页
			* ========================================= */
			scope.maxSize = 5;             //最多显示多少个按钮
			scope.bigCurrentPage = 1;      //当前页码
			scope.itemsPerPage = 10         //每页显示几条
			
			scope.getlist = function () {
				var para = {
					
					review_state:'0'
					
				};
				// para = angular.extend($scope.parameters, para);
				$resource('/api/ac/ggc/reviewProExpertService/findExpertProject', {}, {}).
				save(para,function(res) {
					console.log(para);
					if (res.errcode !== 0) {
						toaster.error({title:"",body:res.errmsg});
						return;
					}
					console.log(res);
					console.log('待评审res')
					scope.objs = res.data;
					scope.bigTotalItems = res.data.totalRecord;

					
				})
			};
			scope.$watch('expertflag', function(){
				scope.getlist();
			}, true)
			
	
	scope.edit = function(index){
        scope.item = scope.objs[index];
        	console.log(scope.item);
         	var modalInstance = $modal.open({
            template: require('../views/score.html'),
            controller: 'score',
            size: 'lg',
            resolve: {
                items: function () {
                    return scope.item;
                },
            }
        });
        modalInstance.result.then(function (showResult) {	
               scope.expertflag++;
        });
    }  

	scope.confirms= function (index) {    			        
				 if(confirm('确定接受该项目？')){
					$resource('/api/as/ggc/reviewrecommendcity/updateReply', {}, {}).save({'expert_code':scope.objs[index].expert_code,'project_code':scope.objs[index].project_code}, function(res){			              
                if(res.errcode === 0 || res.errcode === 10003){
						alert('操作成功')			
						scope.$watch('expertflag', function(){
				scope.getlist();
			}, true)
                }else{
                    alert(res.errmsg);
                }
            });
				
				 }		
				 
			}

	scope.giveup= function (index) {    			        
				 if(confirm('确定放弃该项目？')){
					$resource('/api/as/ggc/reviewrecommendcity/updateNoReply', {}, {}).save({'expert_code':scope.objs[index].expert_code,'project_code':scope.objs[index].project_code}, function(res){			              
                if(res.errcode === 0 || res.errcode === 10003){
						alert('操作成功')			
						scope.$watch('expertflag', function(){
				scope.getlist();
			}, true)
                }else{
                    alert(res.errmsg);
                }
            });
				
				 }		
				 
			}

	scope.toinfo = function(index){
        scope.item = scope.objs[index];
		scope.item.flag = '1'
        	console.log(scope.item);
         	var modalInstance = $modal.open({
            template: require('../views/projectinfo.html'),
            controller: 'projectinfo',
            size: 'lg',
            resolve: {
                items: function () {
                    return scope.item;
                },
            }
        });
        modalInstance.result.then(function (showResult) {	
               scope.expertflag++;
        });
    }  

        }
	}
}