

var app = function($scope, $translate, $localStorage, $window, $resource, selectMenu, $cookies, $rootScope, $state , $urlRouter, dlq, angularPermission){

  // add 'ie' classes to html
  var isIE = !!navigator.userAgent.match(/MSIE/i);
  isIE && angular.element($window.document.body).addClass('ie');
  isSmartDevice( $window ) && angular.element($window.document.body).addClass('smart');

  // config
  $scope.app = {
    name: dlq.title,
    version: '2.0',
    // for chart colors
    color: {
      primary: '#7266ba',
      info:    '#23b7e5',
      success: '#27c24c',
      warning: '#fad733',
      danger:  '#f05050',
      light:   '#e8eff0',
      dark:    '#3a3f51',
      black:   '#1c2b36'
    },
    settings: {
      themeID: 1,
      navbarHeaderColor: 'bg-black',
      navbarCollapseColor: 'bg-white-only',
      asideColor: 'bg-black',
      headerFixed: true,
      asideFixed: false,
      asideFolded: false,
      asideDock: false,
      container: false
    }
  }

  // save settings to local storage
  if ( angular.isDefined($localStorage.settings) ) {
    $scope.app.settings = $localStorage.settings;
  } else {
    $localStorage.settings = $scope.app.settings;
  }
  $scope.$watch('app.settings', function(){
    if( $scope.app.settings.asideDock  &&  $scope.app.settings.asideFixed ){
      // aside dock and fixed must set the header fixed.
      $scope.app.settings.headerFixed = true;
    }
    // save to local storage
    $localStorage.settings = $scope.app.settings;
  }, true);

  // angular translate

  $scope.lang = { isopen: false };

  //初始化菜单项
  $scope.sys_label = {};

  $scope.sys_list = $rootScope.sys_list;

  var menuID = $cookies.get('menuId');
  if(menuID){
    var check = false;
      for(var i = 0; i < $scope.sys_list.length; i++){
        if(menuID == $scope.sys_list[i].id){
          $scope.sys_label = $scope.sys_list[i];
          check = true;
        }
      }
      if(check == false){
        $scope.sys_label = $scope.sys_list[0];
      }
  } else {
      $scope.sys_label = $scope.sys_list[0];
  }

  $resource('/api/ac/sc/menuService/newMenulist', {}, {}).get({'menu_id' : $scope.sys_label.id}, function(res){
      if(res.errcode === 0){
        $scope.menuobj = res.data;
		var menudata = res.data;
		var permissions = new Array();
		
		for (var i = 0; i < res.data.list.length; i++) {
			var tmp = res.data.list[i];
			if (angular.isArray(tmp.list)) {
				for (var j = 0; j < tmp.list.length; j++) {
					var tmp1 = tmp.list[j];
					if(tmp1.hasOwnProperty('permission') && tmp1.permission){
						var permissionArr = [];
						var permissionArr = tmp1.permission.split(',');
						for (var index = 0; index < permissionArr.length; index++) {
							var element = permissionArr[index];
							permissions.push(element)
						}
						// permissions.push(tmp1.permission)
					}
				}
			}
		}
		$rootScope.userPermissionList = permissions;
		angularPermission.setPermissions($rootScope.userPermissionList);

      } else if (res.errcode === 1001) {
        if($rootScope.info.company_sys_area == 'LW'){
          //window.location = "/qunmeng.html";
          window.location = "/login.html";
        }else{
          window.location = "/login.html";
        }
      } else if(res.errcode == 1002){
        alert('菜单无权限!');
      } else {
        alert(res.errmsg);
      }
  });

    //     $scope.sys_label = $scope.sys_list[0];
  //     if(menuID){
  //       for(var i = 0; i < $scope.sys_list.length; i++){
  //         if(menuID == $scope.sys_list[i].id){
  //           $scope.sys_label = $scope.sys_list[i];
  //         }
  //       }
  //     }


  //获取用户的拥有的菜单列表
  // $.ajax({
  //   url : '/api/as/sc/menu/userMenuList',
  //   type : "GET",
  //   dataType : 'json'
  // }).then(function (res) {
  //   if (res.errcode === 0) {
  //     $scope.sys_list = res.data;

  //     var menuID = $cookies.get('menuId');

  //     $scope.sys_label = $scope.sys_list[0];
  //     if(menuID){
  //       for(var i = 0; i < $scope.sys_list.length; i++){
  //         if(menuID == $scope.sys_list[i].id){
  //           $scope.sys_label = $scope.sys_list[i];
  //         }
  //       }
  //     }

  //     $scope.sys_list.push($scope.sys_list[0]);
  //     $scope.sys_list.splice(0,1);
  //   } else {
  //     // $scope.sys_list = [{'id': 'cf7abe1eb15f472aba8f2dd57b1908','name': '新版居游'}];
  //     // $scope.sys_label = $scope.sys_list[0];
  //     alert('获取应用列表失败!');
  //   }
  // });


  $scope.setMenu = function(item){
    var expireDate = new Date();
    //设置cookies有效时间为7天
    expireDate.setDate(expireDate.getDate() + 7); 
    $cookies.put('menuId',item.id,{'expires': expireDate});
    $scope.sys_label = item;

  }

  $scope.$watch('sys_label', function () {
    selectMenu.changeMenu($scope.sys_label);
  });


  //$scope.lang = { isopen: false };
  //$scope.langs = {en:'供应商系统', de_DE:'分销商系统', it_IT:'平台管理系统'};

  $scope.setLang = function(langKey, $event) {
    // set the current lang
    $scope.selectLang = $scope.langs[langKey];
    // You can change the language during runtime
    //$translate.use(langKey);
    $scope.lang.isopen = !$scope.lang.isopen;

    $scope.dlqmenu = $scope.langs[langKey];
  };


 //退出
  $scope.logout = function(){

    $.ajax({
        'url' : "/manager/ajaxlogout",
        'type':'GET',
        'data':{
            'username' : 'dlq',
        },
        'dataType' : 'json',
        'success':function(data,textStatus,jqXHR){
            if(data.errcode === 0){
                if($rootScope.info.company_sys_area == 'LW'){
                  //window.location = "/qunmeng.html";
                  window.location = "/login.html";
                }else{
                  window.location = "/login.html";
                }
            }else{
                alert(data.errmsg);
            }
        },
    });


    // $resource('/manager/ajaxlogout', {}, {}).get({'name' : 'dlq'}, function(res){
    //     if(res.errcode === 0){
    //         location.href='/login.html';
    //     }else{
    //         alert(res.errmsg);
    //     }
    // });
  };

  function isSmartDevice( $window )
  {
      // Adapted from http://www.detectmobilebrowsers.com
      var ua = $window['navigator']['userAgent'] || $window['navigator']['vendor'] || $window['opera'];
      // Checks for iOs, Android, Blackberry, Opera Mini, and Windows mobile devices
      return (/iPhone|iPod|iPad|Silk|Android|BlackBerry|Opera Mini|IEMobile/).test(ua);
  }

};

module.exports = app;