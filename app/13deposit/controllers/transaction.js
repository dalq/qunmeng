module.exports = function ($scope, $state, $resource, $modal, $stateParams, toaster){
    //查询城市编号别表
    $scope.search = function (pageNo){
        if($scope.date.lable){
            if(typeof $scope.date.lable === 'string'){
                $scope.tour_date=$scope.date.lable;
            }else{
                    $scope.tour_date=date2str2($scope.date.lable);
            }
        }else{
            $scope.tour_date='';
        }
        if($scope.date1.lable){
            if(typeof $scope.date1.lable === 'string'){
                $scope.tour_date_two=$scope.date1.lable;
            }else{
                    $scope.tour_date_two=date2str2($scope.date1.lable);
            }
        }else{
            $scope.tour_date_two='';
        }
        var para = {
            'pageNo': pageNo || $scope.currentPage,
            'pageSize': $scope.itemsPerPage,
            'order_code': $scope.searchform.order_code,
            'mobile': $scope.searchform.mobile,
            'name': $scope.searchform.name,
            'start': $scope.tour_date + " 00:00:00",
            'end': $scope.tour_date_two + " 23:59:59",
            'type': $scope.searchform.type,
            'seller_code': $scope.searchform.seller_code,
            'supplier_company_code': $scope.searchform.supplier_company_code,
            'deposit_system': $scope.searchform.deposit_system
        }
        $resource('/api/ac/pc/payBmAccountService/findTransactionList', {}, {}).save(para, function(res){
            if(res.errcode === 0){
                $scope.list = res.data.results;
                $scope.totalItems = res.data.totalRecord;
                $scope.currentPage = pageNo;
            }else{
                toaster.error({title: '', body: res.errmsg});
            }
        });
    }
    
    $scope.date = {
        'lable': date2str2(new Date()),
       'opened': false
    }
   
    $scope.dateOpen = function ($event, item) {
        $event.preventDefault();
        $event.stopPropagation();
        item.opened = true;
    };
    $scope.date1 = {
        'lable': date2str2(new Date()),
        'opened': false
    }
    function date2str2(d) {
        if (d === undefined) {
            return "";
        }
        var month = (d.getMonth() + 1).toString();
        var day = d.getDate().toString();
        if (month.length == 1) month = '0' + month;
        if (day.length == 1) day = '0' + day;
        return d.getFullYear() + "-" + month + "-" + day;
    }
    $scope.searchform = {
        'seltype': '1'
    }

    $scope.changetype = function () {
    	if($scope.searchform.seltype == '0'){
    		$scope.showtype = '0';
    		$scope.date.lable = new Date();
    		$scope.date1.lable = new Date();
    	}else{
    		$scope.showtype = '1';
    		$scope.date.lable =  date2str2(new Date());
    		$scope.date1.lable =  date2str2(new Date());
    	}
    }
    init();
    //初始化列表
    function init(){
        $scope.currentPage = 1;		//当前页码
        $scope.itemsPerPage = 10;	//每页显示几条
        $scope.searchform = {};
        $scope.dict = {
            'a1': '预存充值',
            'a2': '购买产品',
            'a3': '退单',
            'a4': '系统服务',
            'a5': '退服务费'
        }
        if ($stateParams.seller_code) {
            $scope.searchform.seller_code = $stateParams.seller_code;
        }
        if ($stateParams.supplier_company_code){
            $scope.searchform.supplier_company_code = $stateParams.supplier_company_code;
        }
        if ($stateParams.deposit_system){
            $scope.searchform.deposit_system = $stateParams.deposit_system;
        }

        $scope.search(1);
    }

    //查询订单信息
    $scope.getOrderInfo = function (info){
        var modalInstance = $modal.open({
            template: require('../views/transactionInfo.html'),
            controller: 'transactionInfo',
            size: '',
            resolve: {
                info : function () {
                    return info;
                }
            }
        });
        //关闭模态框刷新页面
        modalInstance.result.then(function() {
            init();
        });
    }

   

};