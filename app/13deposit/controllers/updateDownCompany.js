module.exports = function ($scope, $resource, $modalInstance, downCompanyList, toaster) {

	$scope.downCompanyList = downCompanyList;
	$scope.currentPage = 1;			//当前页码
	$scope.itemsPerPage = 10;		//每页显示几条

	$scope.getDate = function(pageNo){
		var para = {
			'pageNo' : pageNo, 
			'pageSize' : $scope.itemsPerPage
		};
		$resource('/api/as/sc/office/getDownCompanyList', {}, {}).save(para, function(res){
			if(res.errcode === 0){
				$scope.downCompanyList = res.data.results;
				$scope.totalItems = res.data.totalRecord;
			} else {
				toaster.error({title: '', body: '查询待审核分销商失败'});
			}
		});
	}

	//审核通过 or 不通过
	$scope.update = function(item, statue, index){
		var para = {
			'pass': statue,
			'down_company_code': item.down_company_code
		}
		$resource('/api/ac/sc/office/updateDownCompany', {}, {}).save(para, function(res){
			if(res.errcode === 0){
				toaster.success({title: '', body: '成功'});
				$scope.downCompanyList.splice(index, 1);
			} else {
				toaster.error({title: '', body: res.errmsg});
			}
		});
	}

	//取消
	$scope.cancel = function() {
		$modalInstance.close(); 
	};


};