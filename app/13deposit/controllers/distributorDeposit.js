module.exports = function ($scope, $resource, $modal,$state, toaster) {

	init();
	//初始化查询分销商列表
	function init() {
		$scope.item = {};
		$resource('/api/ac/pc/payBmAccountService/getChildBmAccountList', {}, {}).save({}, function(res){
			if(res.errcode === 0){
				$scope.list = res.data;
				$scope.list.forEach(function(value, index, array){
					if(value.balance_price || value.loan_limit_price || value.balance_price==0 || value.loan_limit_price==0){
						value.all_balance = value.balance_price + value.loan_limit_price;
						value.show = true;
					}
				});
			} else {
				toaster.error({title: '', body: '查询信息失败'});
			}
		});
	}

	//---------------------------
	//查询待审核分销商列表
	$scope.getDownCompany = function(){
		$resource('/api/as/sc/office/getDownCompanyList', {}, {}).save({}, function(res){
			if(res.errcode === 0){
				$scope.downCompanyList = res.data.results;
				$scope.allDownCompany = res.data.totalRecord;
			} else {
				toaster.error({title: '', body: '查询待审核分销商失败'});
			}
		});
	}
	$scope.getDownCompany();
	//-----------------------------------

	//分销商审核
	$scope.auditDown = function() {
		var modalInstance = $modal.open({
			template: require('../views/updateDownCompany.html'),
			controller: 'updateDownCompany',
			size: 'lg',
			resolve: {
				downCompanyList: function () {
					return $scope.downCompanyList;
				}
			}
		});
		modalInstance.result.then(function() {
			init();
		}, function(reason) {
			init();
		});
	}

	//搜索
	$scope.find = function() {
		$scope.item.seller_name = $scope.item.name;
	}

	//充值预存
	$scope.update = function(info, flag) {
		var modalInstance = $modal.open({
			template: require('../views/depositBalance.html'),
			controller: 'depositBalance',
			size: 'lg',
			resolve: {
				info: function () {
					return info;
				},
				flag: function () {
					return flag;
				}
			}
		});
		modalInstance.result.then(function(result) {
			info.show = true;
			info.balance_price = info.balance_price || 0;
			info.loan_limit_price = info.loan_limit_price || 0;
			info.send_sms_price = info.send_sms_price || 0;
			if(flag){
				info.balance_price += result.price;
			} else {
				info.loan_limit_price = result.price;
			}
			info.all_balance = info.balance_price + info.loan_limit_price;
		});
	}


	//查看交易记录
	$scope.findlist = function(info){
		var para = {
			'seller_code': info.seller_code
		}
		$state.go('app.transaction', para);
	};
};