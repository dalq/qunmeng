module.exports = function ($scope, $resource, $modalInstance, toaster, item) {


	$resource('/api/ac/pc/payPtAccountService/findInfoByOrdercodeList', {}, {})
		.save({'order_code': item.order_code}, function(res){
		if(res.errcode === 0){
			$scope.detailList = res.data;
		} else {
			toaster.error({title: '', body: '查询分润列表失败'});
		}
	});


	//取消
	$scope.cancel = function () {
		$modalInstance.dismiss('cancel');
	}

};