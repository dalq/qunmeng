module.exports = function ($scope, $resource, $modal, toaster) {
	
	//加载个人信息
	$scope.loadUserInfo = function() {
		$resource('/api/ac/pc/payPtAccountService/saveOrGetUserInfo', {}, {}).save({}, function(res){
			if(res.errcode === 0){
				$scope.userInfo = res.data;
			} else {
				toaster.error({title: '', body: '加载个人信息失败'});
			}
		});
	}

	//查询分润信息列表
	$scope.profitList = function() {
		$resource('/api/ac/pc/payPtAccountService/findOrderInfoList', {}, {}).save({}, function(res){
			if(res.errcode === 0){
				$scope.list = res.data.results;
				$scope.totalItems = res.data.totalRecord;
			} else {
				toaster.error({title: '', body: '查询分润列表失败'});
			}
		});
	}

	init();
	//初始化信息
	function init() {
		$scope.currentPage = 1;			//当前页码
		$scope.itemsPerPage = 10;		//每页显示几条
		$scope.loadUserInfo();
		$scope.profitList();
	}

	//查看分润明细
	$scope.detail = function(obj) {
		var modalInstance = $modal.open({
			template: require('../views/profitDetail.html'),
			controller: 'profitDetail',
			size: 'lg',
			resolve: {
				item: function () {
					return obj;
				}
			}
		});
		//关闭模态框刷新页面
		modalInstance.result.then(function(result) {
			
		});
	}



};