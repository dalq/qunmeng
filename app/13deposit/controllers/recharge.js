/**
 * 模态框
 */
module.exports = function ($scope, $state, obj, recharge, $modalInstance, balance, toaster) {

	$scope.obj = {};
	$scope.obj.company_code = obj.seller_code;
	$scope.obj.balance_price = 0;
	$scope.balance = balance;

	$scope.ok = function () {
		recharge.save($scope.obj, function (res) {
			if (res.errcode === 0) {
                toaster.success({title: '', body: '充值成功'})
				$modalInstance.close();
			}
			else {
                toaster.error({title: '', body: res.errmsg})
			}
		});
	};

	$scope.cancel = function () {
		$modalInstance.dismiss('cancel');
	};

};