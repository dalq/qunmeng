module.exports = function ($scope, $resource, $modalInstance, obj, toaster) {

	$scope.detail = {
		'supplier_company_code': obj.supplier_company_code,
		'send_sms_price': obj.send_sms_price,
		'send_sms_flag': obj.send_sms_flag,
		'deposit_system': obj.deposit_system
	};
	
	//保存
	$scope.save = function() {
		if((!$scope.detail.send_sms_price && $scope.detail.send_sms_price!=0)|| $scope.detail.send_sms_price < 0){
			toaster.warning({title: '', body: '预警金额错误'});
			return;
		}
		$resource('/api/as/pc/bmaccount/updateBmAccountInfo', {}, {}).save($scope.detail, function(res){
			if(res.errcode === 0){
				toaster.success({title: '', body: '修改成功'});
				$modalInstance.close($scope.detail);
			} else {
				toaster.error({title: '', body: '修改失败'});
			}
		});
	}

	//取消
	$scope.cancel = function() {
        $modalInstance.dismiss('cancel'); 
	};

};