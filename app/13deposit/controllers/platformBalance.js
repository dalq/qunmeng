/**
 * 平台预存充值和退回预存页面
 * flag == true : 预存充值,false 预存退还
 */
module.exports = function ($scope, $resource, $modalInstance, info, balance_price, flag, toaster) {
	
	$scope.flag = flag;
	$scope.item = {'price': balance_price};
	$scope.detail = {};

	//检验金额,true通过,false不通过
	function check(){
		if(!$scope.detail.price || $scope.detail.price < 0){
			toaster.warning({title: '', body: '金额错误'});
			return false;
		}
		if($scope.detail.price > balance_price){
			toaster.warning({title: '', body: '金额超过上限,请重新输入'});
			return false;
		}
		return true;
	}

	//保存
	$scope.save = function() {
		if(!check()) {return; }
		if(flag){
			$scope.item.url = '/api/ac/pc/payBmAccountService/addAccountBalance';
			$scope.item.msg = '充值成功';
			$scope.detail.seller_code = info.seller_code;
		} else {
			$scope.item.url = '/api/ac/pc/payBmAccountService/setAccountBack';
			$scope.item.msg = '退还成功';
		}
		$scope.detail.deposit_system = '0';
		$resource($scope.item.url, {}, {}).save($scope.detail, function(res){
			if(res.errcode === 0){
				toaster.success({title: '', body: $scope.item.msg});
				$modalInstance.close($scope.detail);
			} else {
				toaster.error({title: '', body: res.errmsg});
			}
		});
	}

	//取消
	$scope.cancel = function() {
		$modalInstance.dismiss('cancel'); 
	};


};