module.exports = function ($scope, $resource, $modal,$state, toaster) {

	//查询供应商
	$scope.getData = function() {
		$resource('/api/as/pc/bmaccount/getSupplierList', {}, {}).save({}, function(res){
			if(res.errcode === 0){
				$scope.list = res.data;
				$scope.list.forEach(function(value, index, array){
					if(value.balance_price || value.loan_limit_price || value.balance_price==0 || value.loan_limit_price==0){
						value.all_balance = value.balance_price + value.loan_limit_price;
						value.show = true;
					}
				});
			} else {
				toaster.error({title: '', body: '查询信息失败'});
			}
		});
	}

	init();
	//初始化信息
	function init() {
		$scope.item = {};
		$scope.getData();
	}

	//搜索
	$scope.find = function() {
		$scope.item.supplier_name = $scope.item.supplier_company_name;
	}

	//编辑
	$scope.edit = function(obj) {
		var modalInstance = $modal.open({
			 template: require('../views/depositEdit.html'),
			 controller: 'depositEdit',
			 size: 'lg',
			 resolve: {
				obj: function () {
					 return obj;
				}
			 }
		});
		modalInstance.result.then(function(result) {
			obj.send_sms_price = result.send_sms_price;
			obj.send_sms_flag = result.send_sms_flag;
		});
	}

	//退回预存
	$scope.backMoney = function(info) {
		var modalInstance = $modal.open({
			template: require('../views/depositBalance.html'),
			controller: 'depositBalance',
			size: 'lg',
			resolve: {
				info: function () {
					return null;
				},
				self: function () {
					return info;
				},
				flag: function () {
					return false;
				}
			}
		});
		modalInstance.result.then(function(result) {
			info.balance_price -= result.price;
		});
	}


	//查看交易记录
	$scope.findlist = function(info){
		var para = {
			'supplier_company_code': info.supplier_company_code
		}
		$state.go('app.transaction', para);
	};
};