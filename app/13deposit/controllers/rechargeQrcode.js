module.exports = function ($scope, $resource, $modalInstance, fromDate) {
    
    var intervalProcess, order_code;
    $scope.money = fromDate.money;
    $scope.context = fromDate.pay_type == '1' ? '支付宝扫一扫' : '微信扫一扫';
    function result(order_code){
        $resource('/api/ac/pc/payBmAccountService/setRechargeResult', {}, {}).save({'order_code':order_code}, function(res){
            if(res.errcode === 0 && res.data == '1'){
                clearInterval(intervalProcess);
                $scope.message = '充值成功';
                $('#price').attr('class', 'ng-hide');
                $('#msg').attr('class', 'text-center text-success ng-show');
                $('#qrcode').attr('class', 'ng-hide');
                $('#tip').attr('class', 'ng-hide');
                $('#closebtn').attr('class', 'text-center ng-show');
                //充值成功，倒计时关闭弹出框
                // successProcess = setInterval( function(){
                //     if(order_code){
                //         result(order_code);
                //     } else {
                //         toaster.warning({title: '', body: '请刷新页面!'});
                //         clearInterval(intervalProcess);
                //     }
                // }, 1500);
            }
        });
    }

    init();
    function init(){
        $('#closebtn').attr('class', 'text-center ng-show');
        $resource('/api/ac/pc/payBmAccountService/addRecharge', {}, {}).save(fromDate, function(res){
            $('#closebtn').attr('class', 'ng-hide');
            $('#wait').attr('class', 'ng-hide');
            if(res.errcode === 0){
                order_code = res.data.order_code;
                if(res.data.ret){
                    var ret = JSON.parse(res.data.ret);
                    if(ret.status == '1'){
                        $('#qrcode').qrcode({ 
                            width: 300, 
                            height: 300, 
                            text: ret.result
                        });
                        $('#qrcode').attr('class', 'ng-show');
                        $('#tip').attr('class', 'text-center ng-show');
                        $('#price').attr('class', 'text-center ng-show');
    
                        //生成二维码成功后，循环查询充值结果
                        intervalProcess = setInterval( function(){
                            if(order_code){
                                result(order_code);
                            } else {
                                toaster.warning({title: '', body: '请刷新页面!'});
                                clearInterval(intervalProcess);
                            }
                        }, 1500);
                        return;
                    }
                }
            }

            $scope.message = res.errmsg;
            $('#msg').attr('class', 'text-center ng-show');
            $('#error').attr('class', 'text-center ng-show');
        });
    }

    $scope.fresh = function(){
        $('#wait').attr('class', 'text-center ng-show');
        $('#msg').attr('class', 'ng-hide');
        $('#error').attr('class', 'ng-hide');
        $('#closebtn').attr('class', 'ng-hide');
        init();
    }

    //关闭模态框时停止计时器
    $scope.$on("$destroy", function() {
        clearInterval(intervalProcess);
    })

    //关闭交易
    $scope.closeOrder = function() {
        $resource('/api/ac/pc/payBmAccountService/deleteRecharge', {}, {}).save({'order_code':order_code}, function(res){
        });
        $modalInstance.close();
    }

    $scope.close = function () {
        $modalInstance.close();
    }
};