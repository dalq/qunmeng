module.exports = function ($scope, $modalInstance, $resource, $modal, info){
    

     //查询城市编号别表
     $scope.search = function (){
        var para = {
            'order_code': $scope.info.order_code
        }
        $resource('/api/as/tc/ticketorder/getOrderInfoForTransaction', {}, {}).save(para, function(res){
            if(res.errcode === 0){
                $scope.info = res.data;
            }else{
                toaster.error({title: '', body: res.errmsg});
            }
        });
    }


    init();
    function init(){
        $scope.info = angular.copy(info);
        $scope.search();
    }
   
   
    //取消
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    }

};