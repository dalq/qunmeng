/**
 * 供应商预存充值和授信操作
 * flag == true : 余额操作,false 授信操作
 */
module.exports = function ($scope, $resource, $modalInstance, info, flag, toaster) {

	$scope.flag = flag;
	$scope.price = flag ? info.balance_price : info.loan_limit_price;
	$scope.detail = {'seller_code': info.seller_code};

	//保存
	$scope.save = function() {
		if(!$scope.detail.price){
			toaster.warning({title: '', body: '金额错误'});
			return;
		}
		if(flag){
			if(info.all_balance + $scope.detail.price < 0){
				toaster.warning({title: '', body: '金额不合理'});
				return;
			}
			$scope.detail.deposit_system = '1';
			$scope.recharge();
		} else {
			if( $scope.detail.price < 0){
				toaster.warning({title: '', body: '授信金额错误'});
				return;
			}
			$scope.money();
		}
	}

	//账号充值预存
	$scope.recharge = function() {
		$resource('/api/ac/pc/payBmAccountService/addAccountBalance', {}, {}).save($scope.detail, function(res){
			if(res.errcode === 0){
				toaster.success({title: '', body: '充值成功'});
				$modalInstance.close($scope.detail);
			} else {
				toaster.error({title: '', body: res.errmsg});
			}
		});
	}

	//调整信用额度
	$scope.money = function() {
		$resource('/api/ac/pc/payBmAccountService/setAccountLoanLimit', {}, {}).save($scope.detail, function(res){
			if(res.errcode === 0){
				if(res.data == '1'){
					toaster.success({title: '', body: '调整信用额度成功'});
					$modalInstance.close($scope.detail);
				} else {
					toaster.error({title: '', body: '调整信用额度失败'});
				}
			} else {
				toaster.error({title: '', body: res.errmsg});
			}
		});
	}

	//取消
	$scope.cancel = function() {
		$modalInstance.dismiss('cancel'); 
	};


};