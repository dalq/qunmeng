module.exports = function ($scope, $resource, $modal, toaster) {

	$scope.fromDate = {
		'pay_type': '2',
		'money': 5000
	}
	init();
	//初始化查询分销商列表
	function init() {
		$resource('/api/as/pc/bmaccount/getPlatformPrice', {}, {}).save({}, function(res){
			if(res.errcode === 0){
				$scope.platformInfo = res.data;
			} else {
				toaster.error({title: '', body: '查询平台预存余额失败'});
			}
		});
	}

	//选择支付方式
	$scope.selectPay = function(flag){
		if(flag){
			$('#wexin').addClass('selected');
			$('#alipay').removeClass('selected');
			$scope.fromDate.pay_type = '2';
		} else {
			$('#alipay').addClass('selected');
			$('#wexin').removeClass('selected');
			$scope.fromDate.pay_type = '1';
		}
	}
	
	$scope.recharge = function(){
		if(!$scope.fromDate.money || $scope.fromDate.money < 0){
			toaster.error({title: '', body: '请填写正确的充值金额'});
			return;
		}
		if($scope.fromDate.money < 1){
			toaster.error({title: '', body: '最少充值1元'});
			return;
		}
		var modalInstance = $modal.open({
			template: require('../views/rechargeQrcode.html'),
			controller: 'rechargeQrcode',
			size: 'sm',
			backdrop: 'static',
			resolve: {
				fromDate: function () {
					return $scope.fromDate;
				}
			}
		});
		//关闭模态框刷新页面
		modalInstance.result.then(function() {
			init();
		}, function () {
			init();
		});
		
	}


};