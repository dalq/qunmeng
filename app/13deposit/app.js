var App = angular.module('deposit', []);

App.config(require('./router'));

//service
App.factory('depositservice', require('./service'));

App.controller('recharge',require('./controllers/recharge'));
App.controller('trackinfo',require('./controllers/trackinfo'));
App.controller('supplierDeposit',require('./controllers/supplierDeposit'));
App.controller('depositEdit',require('./controllers/depositEdit'));
App.controller('depositBalance',require('./controllers/depositBalance'));
App.controller('distributorDeposit',require('./controllers/distributorDeposit'));
App.controller('platformDeposit',require('./controllers/platformDeposit'));
App.controller('platformBalance',require('./controllers/platformBalance'));
App.controller('transaction',require('./controllers/transaction'));
App.controller('transactionInfo',require('./controllers/transactionInfo'));
App.controller('updateDownCompany',require('./controllers/updateDownCompany'));
App.controller('rechargeQrcode',require('./controllers/rechargeQrcode'));

//分润信息
App.controller('profitList',require('./controllers/profitList'));
App.controller('profitDetail',require('./controllers/profitDetail'));

module.exports = App;