/**
* 子模块路由
* ml
*/

var router = function ($urlRouterProvider, $stateProvider) {

	$stateProvider

		//查看供应商
		.state('app.supplier_deposit', {
			url: "/supplier/deposit",
			views: {
				'main@': {
					template: require('./views/supplierDeposit.html'),
					controller: 'supplierDeposit',
				}
			},
			resolve: {
				
			}
		})

		//管理分销商
		.state('app.distributor_deposit', {
			url: "/distributor/deposit",
			views: {
				'main@': {
					template: require('./views/distributorDeposit.html'),
					controller: 'distributorDeposit',
				}
			},
			resolve: {
				
			}
		})

		//平台预存管理
		.state('app.platform_deposit', {
			url: "/platform/deposit",
			views: {
				'main@': {
					template: require('./views/platformDeposit.html'),
					controller: 'platformDeposit',
				}
			},
			resolve: {
				
			}
		})

		//分润信息
		// .state('app.profitList', {
		// 	url: "/profit/list",
		// 	views: {
		// 		'main@': {
		// 			template: require('./views/profitList.html'),
		// 			controller: 'profitList',
		// 		}
		// 	},
		// 	resolve: {
				
		// 	}
		// })

		//交易记录
		.state('app.transaction', {
			url: "/transaction/list/:supplier_company_code/:seller_code/:deposit_system",
			views: {
				'main@': {
					template: require('./views/transaction.html'),
					controller: 'transaction',
				}
			},
			resolve: {
			}
		})

		
};

module.exports = router;