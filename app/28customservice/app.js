var App = angular.module('custs', []);
App.config(require('./router'));

App.factory('custservice', require('./service'));

App.controller('userinfo',require('./controllers/userinfo'));
App.controller('cardA',require('./controllers/cardA'));
App.controller('saleticketinfo',require('./controllers/saleticketinfo'));
App.controller('editcardA',require('./controllers/editcardA'));
App.controller('createuserinfo',require('./controllers/createuserinfo'));
App.controller('edituserinfo',require('./controllers/edituserinfo'));
App.controller('infoticket',require('./controllers/infoticket'));
App.controller('redpackage',require('./controllers/redpackage'));
App.controller('userorderlist',require('./controllers/userorderlist'));
App.controller('unicomuser',require('./controllers/unicomuser'));
App.controller('sendmessage',require('./controllers/sendmessage'));
App.controller('orderbacklist',require('./controllers/orderbacklist'));
App.controller('orderback',require('./controllers/orderback'));
App.controller('carduserinfo',require('./controllers/carduserinfo'));
App.controller('registercount',require('./controllers/registercount'));
App.controller('openlottery',require('./controllers/openlottery'));
App.controller('customTicketinfo',require('./controllers/ticketinfo'));
//查看激活码
App.controller('activecode',require('./controllers/activecode'));

// 获取亲子套票激活用户信息
App.controller('getpackageinfolist',require('./controllers/getpackageinfolist'));
App.controller('userinfopicture',require('./controllers/userinfopicture'));
// 微信用户列表
App.controller('wxuserlist',require('./controllers/wxuserlist')); 
// 用户详情
App.controller('wxuserinfo',require('./controllers/wxuserinfo')); 
//客服卡订单
App.controller('kfcardorderlist',require('./controllers/kfcardorderlist')); 


module.exports = App;