
var router = function($urlRouterProvider, $stateProvider){

 
    	$stateProvider
       //查看用户信息
 	  .state('app.userinfo', {
        url: '/user_info',
         views: {
            'main@' : {
                template : require('./views/userinfo.html'),
                controller : 'userinfo',
            }
        },
        
        resolve:{
            userinfo : function(custservice){
                return custservice.userinfo();
            },
            deleteuserinfo : function(custservice){
                return custservice.deleteuserinfo();
            },
            updateUserAuthInfo : function(custservice){
                return custservice.updateUserAuthInfo();
            },
            edituserinfo : function(custservice){
                return custservice.edituserinfo();
            },
            updateidcard : function(custservice){
                return custservice.updateidcard();
            },
            syncUserAuthInfo : function(custservice){
                return custservice.syncUserAuthInfo();
            },
            cleanRedis : function(custservice){
                return custservice.cleanRedis();
            },
            updatemobile : function(custservice){
                return custservice.updatemobile();
            }
        }
      })
      .state('app.createuserinfo', {
        url: '/createuserinfo',
        views: {
            'main@' : {
                template : require('./views/createuserinfo.html'),
                controller :'createuserinfo',
            }
        },
       
        resolve:{
            createuserinfo : function(custservice){
                return custservice.createuserinfo();
            }
        }
      })


       .state('app.cardA', {
        url: '/card_a',
        views: {
            'main@' : {
                template : require('./views/cardA.html'),
                controller :'cardA',
            }
        },
       
        resolve:{
             cardA : function(custservice){
                return custservice.cardA();
            },
            updateCardPass : function(custservice){
                return custservice.updateCardPass();
            },
            saleticketinfo : function(custservice){
                return custservice.saleticketinfo();
            }
        }
      })

     .state('app.infoticket', {
        url: '/infoticket',
           views: {
            'main@' : {
                    controller : 'infoticket',
                    template: require('./views/infoticket.html'),
                }
            },
        resolve:{
            infoticket : function(custservice){
                return custservice.infoticket();
            },
            destoryticket : function(custservice){
                return custservice.destoryticket();
            },
            getinfobyid : function(custservice){
                return custservice.getinfobyid();
            }
        }
      })
       .state('app.redpackage', {
        url: '/redpackage',
          views: {
            'main@' : {
                    controller : 'redpackage',
                    template: require('./views/redpackage.html'),
                }
            },
        resolve:{
            redpackage : function(custservice){
                return custservice.redpackage();
            }
        }
      })

    .state('app.userorderlist', {
        url: '/userorderlist',
         views: {
            'main@' : {
                    controller : 'userorderlist',
                    template: require('./views/userorderlist.html'),
                }
            },
        resolve:{
            orderlist : function(custservice){
                return custservice.orderlist();
            }
        }
      })


      /*.state('app.juyoumember', {
        url: '/unicomuser',
        views: {
            'main@' : {
                        controller : 'unicomuser',
                        template: require('./views/unicomuser.html'),
                    }
                },
        resolve:{
            getUserInfoByIdForChinaUnicom : function(custservice){
                return custservice.getUserInfoByIdForChinaUnicom();
            },
            getDate : function(utilservice){
                return utilservice.getDate;
            }
           
        }
        
      })*/

      .state('app.sendmessage', {
        url: '/sendmessage',
        views:{
            'main@':{
                controller : 'sendmessage',
                template: require('./views/sendmessage.html'),
            }
        },
        
        resolve:{
            sendmessage : function(custservice){
                return custservice.sendmessage();
            }
        }
      })


       .state('app.orderback', {
        url: '/orderbacklist',
        views:{
            'main@':{
                    controller : 'orderbacklist',
                    template: require('./views/orderbacklist.html'),
            }
        },
        
        resolve:{
            orderbacklist : function(custservice){
                return custservice.orderbacklist();
            },
            orderback : function(custservice){
                return custservice.orderback();
            },
            dictbytypelist : function(custservice){
                return custservice.dictbytypelist();
            }
        }
      })

    .state('app.carduserinfo', {
        url: '/carduserinfo',
         views:{
            'main@':{
                    controller : 'carduserinfo',
                    template: require('./views/carduserinfo.html'),
                }
            },
        resolve:{
            carduserinfo : function(custservice){
                return custservice.carduserinfo();
            }
        }
      })

      .state('app.registercount', {
        url: '/registercount',
           views:{
            'main@':{
                    controller : 'registercount',
                    template: require('./views/registercount.html'),
                }
            },
        resolve:{
            registercount : function(custservice){
                return custservice.registercount();
            }
        }
      })

    .state('app.openlottery', {
        url: '/openlottery',
         views:{
            'main@':{
                    controller : 'openlottery',
                    template: require('./views/openlottery.html'),
                }
            },
        resolve:{
           getWeekLottey : function(custservice){
                return custservice.getWeekLottey();
            },
            lotteyUserinfo : function(custservice){
                return custservice.lotteyUserinfo();
            },
            saveUserinfo : function(custservice){
                return custservice.saveUserinfo();
            }
        }
      })

   //根据电话查看激活码
       .state('app.activecode', {
         url: '/activecode',
          views:{
            'main@':{
                     controller : 'activecode',
                     template: require('./views/activecode.html'),
                 }
             },
         resolve:{
            getActiveCodeByMobile : function(custservice){
                 return custservice.getActiveCodeByMobile();
            }
         }
       })
          //根据电话查看激活码
       .state('app.getpackageinfolist', {
         url: '/getpackageinfolist',
          views:{
            'main@':{
                    controller : 'getpackageinfolist',
                    template: require('./views/getpackageinfolist.html'),
            }
          },
         resolve:{
            getUserBaseInfomobile : function(custservice){
                 return custservice.getUserBaseInfomobile();
            }
         }
       }) // 

          //微信用户列表
          .state('app.wxuserlist', {
            url: '/wxuserlist',
             views:{
               'main@':{
                       controller : 'wxuserlist',
                       template: require('./views/wxuserlist.html'),
               }
             },
            resolve:{
            //    getUserBaseInfomobile : function(custservice){
            //         return custservice.getUserBaseInfomobile();
            //    }
            }
          }) // wxuserlist

      //客服卡订单列表
       .state('app.kfcardorderlist', {
         url: '/kfcardorderlist',
         views:{
              'main@':{
                    controller : 'kfcardorderlist',
                    template: require('./views/kfcardorderlist.html'),
              }
         },
         resolve:{
            kfcardorderlist : function(custservice){
                 return custservice.kfcardorderlist();
            },
            ticketinfo : function(cardservice){
                 return cardservice.ticketinfo();
            },
            cardinfo : function(cardservice){
                 return cardservice.cardinfo();
            }

         }
       })
   


};

module.exports = router;
 	