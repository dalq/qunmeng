module.exports = function($scope, userinfo, getDate){

	$scope.msg = ['非居游卡会员', '居游卡会员'];

	$scope.searchform = {};
	$scope.searchform.mobile = '';
	$scope.usedate = '0';



	 $scope.date = {
               
                'opened': false
            }


             $scope.dateOpen = function ($event, item) {
                $event.preventDefault();
                $event.stopPropagation();
                item.opened = true;
            };

            $scope.date1 = {
                 // 'lable': date2str2(new Date()),
                //'value': date2str(new Date()),
                'opened': false
            }


             $scope.dateOpen1 = function ($event, item) {
                $event.preventDefault();
                $event.stopPropagation();
                item.opened = true;
            };
             function date2str2(d) {
                if (d === undefined) {
                    return "";
                }
                var month = (d.getMonth() + 1).toString();
                var day = d.getDate().toString();
                if (month.length == 1) month = '0' + month;
                if (day.length == 1) day = '0' + day;
                return d.getFullYear() + "-" + month + "-" + day;
            }

	//有效区间
	// $scope.section = {};
	// $scope.section.start = {};
	//$scope.section.start.date = {};

	//$scope.section.end = {};
	//$scope.section.end.date = {};

	$scope.today = function() {
		$scope.date.lable = $scope.date1.lable = date2str2(new Date());
	};
	$scope.today();
	$scope.open = function(obj) {
		obj.opened = true;
	};
	$scope.load = function()
	{
		if($scope.searchform.mobile == '' && $scope.usedate == '0'){
			return;
		}
		var para = {};
		if($scope.searchform.mobile != '')
		{
			para.mobile = $scope.searchform.mobile;
		}
        if($scope.usedate == '1')
        {
        	para['begintime'] = date2str2($scope.date.lable) + " 00:00:00";
        	para['endtime'] = date2str2($scope.date1.lable) + " 23:59:59";
        }
        console.log(para);
		userinfo.save({}, para, function(res){
			console.log(res);
			if(res.errcode === 0)
			{
				$scope.objs = res.data;
			}
			else
			{
				alert(res.errmsg);
			}
		});
	}
	$scope.load();
};