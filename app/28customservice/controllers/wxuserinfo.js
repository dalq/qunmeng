module.exports = function($scope, $state, $stateParams, $modalInstance, $resource,$modal,FileUploader,toaster,user_id){
    if(user_id){
        $resource('/api/as/uc/userwxsk/getUserWxByUserid', {}, {}).
        save({'userid' : user_id},function(res) {
            if (res.errcode !== 0) {
                toaster.error({title:"",body:res.errmsg});
                return;
            }
            console.log(res);
            $scope.obj = res.data;
            
        })
    }
    $scope.cancel = function(){
        $modalInstance.close();
    }
}    