module.exports = function($scope, $modal, cardA, updateCardPass, saleticketinfo){

	$scope.searchform = {};
    
    $scope.load = function () {
        
        cardA.save($scope.searchform, function(res){

         	console.log(res);

         	if(res.errcode !== 0)
         	{
         		alert("数据获取失败");
         		return;
         	}

         	$scope.objs = res.data;

        });

    };

    $scope.edit = function(cardnum){

        var modalInstance = $modal.open({
          template: require('../views/editcardA.html'),
          controller: 'editcardA',
           url: '/edit_card',
          size: 'lg',
          resolve: {
            cardnum : function(){
                return cardnum;
            },
            updateCardPass : function(){
                return updateCardPass;
            }
          }
        });

        modalInstance.result.then(function () {
            $scope.load();
        }, function () {
            
        });

    };

    $scope.ticketinfo = function(code){

        var modalInstance = $modal.open({
          template: require('../views/saleticketinfo.html'),
          controller: 'saleticketinfo',
          url:'ticketinfo_1',
          size: 'lg',
          resolve: {
            code : function(){
                return code;
            },
            saleticketinfo : function(){
                return saleticketinfo;
            }
          }
        });

        modalInstance.result.then(function () {
          //load();
        }, function () {
          //$log.info('Modal dismissed at: ' + new Date());
        });

    };

};