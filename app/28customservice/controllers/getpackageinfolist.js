module.exports = function($scope,getUserBaseInfomobile,$modal,toaster){

	$scope.searchform = {};
   
    $scope.search = function () {
        
        getUserBaseInfomobile.save($scope.searchform, function(res){
            console.log($scope.searchform);
         	console.log(res);

         	if(res.errcode !== 0)
         	{
         		alert(res.errmsg);
         		return;
         	}
             
         	$scope.obj = res.data;

        });

    };
    $scope.seepicture = function(pimg){
         var modalInstance = $modal.open({
          template: require('../views/userinfopicture.html'),
          controller: 'userinfopicture',
        //   size: 'lg',
          resolve: {
            pimg : function(){
                return pimg;
            }
          }
        });

        modalInstance.result.then(function () {
        //   $scope.getlist();
          
        }, function () {
          //$scope.load();
        });
    }

};