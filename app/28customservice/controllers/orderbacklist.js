module.exports = function($scope, $state, orderbacklist, orderback, 
    $modal, dictbytypelist){

  var order=[
      {
          'label':'居游',
          'value':'juyou',
      },
      {
          'label':'商客',
          'value':'shangke',
      },
      {
          'label':'不限',
          'value':'',
      }
  ];
  $scope.orderarr=order;

	$scope.searchform = {
         'from_app_id' : 'juyou'
    };
    $scope.payment_typearr = [];
    $scope.back_statearr = [];
    $scope.ticket_statearr = [];



	/* 分页
     * ========================================= */
    $scope.maxSize = 10;            //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10;         //每页显示几条

    $scope.change = function(){
        console.log('11111');
        $scope.bigCurrentPage = 1; 
    }

    $scope.load = function () {

    	var para = {
            pageNo:$scope.bigCurrentPage, 
            pageSize:$scope.itemsPerPage
        };

        para = angular.extend(para, $scope.searchform);
        console.log('入参');
        console.log(para);
        
        orderbacklist.save(para, function(res){

         	console.log(res);

         	if(res.errcode !== 0)
         	{
         		alert("数据获取失败");
         		return;
         	}

         	$scope.objs = res.data.results;
            $scope.bigTotalItems = res.data.totalRecord;

        });

    };
    $scope.load();

	$scope.back = function(obj){
		var modalInstance = $modal.open({
	          template: require('../views/orderback.html'),
	          controller: 'orderback',
	          size: 'xs',
	          resolve: {
	            id : function(){
	                return obj.id;
	            },
	            back_price : function(){
	                return obj.back_price;
	            },
	            orderback : function(){
	                return orderback;
	            }
	          }
        });

        modalInstance.result.then(function () {
            $scope.load();
        }, function () {
            
        });
	}


    dictbytypelist.save({'type' : 'ticket_payment_type'},function(res) {
        console.log(res);
        if(res.errcode === 0)
        {
            $scope.payment_typearr = res.data;
            $scope.payment_typearr.unshift({'label':'不限','value':''});
            console.log(res.data);
            console.log("++++");
        }
        else
        {
            alert(res.errmsg);
        }
    });

    dictbytypelist.save({'type' : 'ticket_back_pay_state'},function(res) {
        console.log(res);
        if(res.errcode === 0)
        {
            $scope.back_statearr = res.data;
            $scope.back_statearr.unshift({'label':'不限','value':''});
        }
        else
        {
            alert(res.errmsg);
        }
    });

    dictbytypelist.save({'type' : 'ticket_back_ticket_state'},function(res) {
        console.log(res);
        if(res.errcode === 0)
        {
              $scope.ticket_statearr = res.data;
              $scope.ticket_statearr.unshift({'label':'不限','value':''});
        }
        else
        {
            alert(res.errmsg);
        }
    });

};