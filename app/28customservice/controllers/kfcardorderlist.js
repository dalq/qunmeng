module.exports = function($scope, $modal, kfcardorderlist, ticketinfo, cardinfo){
	/*, ticketinfo, cardinfo*/

    $scope.searchform = {};

    $scope.load = function () {
        
        kfcardorderlist.save($scope.searchform, function(res){

            console.log(res);

            if(res.errcode === 0)
            {
                $scope.objs = res.data;
            }
            else
            {
                alert(res.errmsg);
            }

        });

    };

    $scope.orderinfo = function(obj){

	    var modalInstance = $modal.open({
	      template: require('../../35card/views/cardorderinfo.html'),
	      controller: require('../../35card/controllers/cardorderinfo.js'),
	      size: 'lg',
	      resolve: {
	        obj : function(){
	            return obj;
	        }
	      }
		});
	}

    $scope.ticketinfo = function(code){

        var modalInstance = $modal.open({
          template: require('../../35card/views/ticketinfo.html'),
	      controller: require('../../35card/controllers/ticketinfo.js'),
          size: 'lg',
          resolve: {
            code : function(){
                return code;
            },
            ticketinfo : function(){
                return ticketinfo;
            }
          }
        });

        modalInstance.result.then(function () {
          //load();
        }, function () {
          //$log.info('Modal dismissed at: ' + new Date());
        });

    };

    $scope.cardinfo = function(code){

        var modalInstance = $modal.open({
          template: require('../../35card/views/cardinfo.html'),
	      controller: require('../../35card/controllers/cardinfo.js'),
          size: 'lg',
          resolve: {
            code : function(){
                return code;
            },
            cardinfo : function(){
                return cardinfo;
            }
          }
        });

        modalInstance.result.then(function () {
          //load();
        }, function () {
          //$log.info('Modal dismissed at: ' + new Date());
        });

    };

};