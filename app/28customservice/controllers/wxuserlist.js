module.exports = function($scope, $state, $stateParams,$resource,$modal,toaster){   
	/* 分页
        * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10         //每页显示几条

    $scope.searchform = {};
    $scope.vm = {
        'date' : '',
        'options' : {
            // format: "YYYY-MM-DD",
            locale : 'zh-cn',
            showClear: true                        
            // clearBtn:true
        }
    }
    $scope.vm1 = {
        'date' : '',
        'options' : {
            // format: "YYYY-MM-DD",
            locale : 'zh-cn',
            showClear: true                        
            // clearBtn:true
        }
    }
    
    $scope.getlist = function () {
        if ( $scope.vm.date === null) {
            $scope.vm.date = '';
        }
        if (typeof $scope.vm.date._d === 'string') {
            $scope.vm.date._d = $scope.vm.date._d;
        } else {
            $scope.vm.date._d = time2str($scope.vm.date._d);
        }
        if ( $scope.vm1.date === null) {
            $scope.vm1.date = '';
        }
        if (typeof $scope.vm1.date._d === 'string') {
            $scope.vm1.date._d = $scope.vm1.date._d;
        } else {
            $scope.vm1.date._d = time2str($scope.vm1.date._d);
        }

        var para = {
            pageNo:$scope.bigCurrentPage, 
            pageSize:$scope.itemsPerPage,
            starttime : $scope.vm.date._d,
            endtime : $scope.vm1.date._d,
        };
        para = angular.extend($scope.searchform, para);
        $resource('api/as/uc/userwxsk/findUserWxSelectList', {}, {}).
        save(para,function(res) {
            console.log(para);
            if (res.errcode !== 0) {
                toaster.error({title:"",body:res.errmsg});
                return;
            }
            console.log(para);
            console.log(res);
            $scope.objs = res.data.results;
            $scope.bigTotalItems = res.data.totalRecord;
             $scope.objs.forEach(function(element) {
                element.isSelected = element.status=='1'?false:true;
            }, this);

            
        })
    };
     
    $scope.getlist();

    
    $scope.info = function(user_id){
        var modalInstance = $modal.open({
            template: require('../views/wxuserinfo.html'),
            controller: 'wxuserinfo',
            size: 'lg',
            resolve: {
                user_id: function () {
                    return user_id;
                }
                
            }
        });
        modalInstance.result.then(function (showResult) {	
            $scope.getlist();
        });
    }

    $scope.obj = {
        'isSelected' : ''
    }

    $scope.delmobile = function(user_id){
        if(confirm('确定解绑吗?')){
            $resource('/api/as/uc/userwxsk/updateDelMobileForUserWx', {}, {}).
            save({'userid' : user_id},function(res) {
                if (res.errcode !== 0) {
                    toaster.error({title:"",body:res.errmsg});
                    return;
                }
                console.log(res);
                toaster.success({title:"",body:"解绑成功"});
                $scope.getlist();
                
            })
        }
        
    }
   

    function time2str(objDate) {
        if(angular.isDate(objDate))
        {
            var y = objDate.getFullYear();
            var m = objDate.getMonth();
            var d = objDate.getDate();
            var h = objDate.getHours();
            var mt = objDate.getMinutes();
            // var s = objDate.getSeconds();
            return y + '-' + (m + 1) + '-' + d + ' ' + h + ':' + mt;
        }
        else
        {
            return '错误格式';
        }
    }
    $scope.b= '0';
	$scope.c = '';
	$scope.show = function(index){
        var userid = $scope.objs[index].userid;
		console.log($scope.b, $scope.c);
		if($scope.b==0){
			$scope.b='1',
			$scope.c=index
		}else if($scope.b==1){
			if($scope.c!=index){
				$scope.c=index
			}else{
				$scope.b='0',
				$scope.c=index
			}	
        }	
        // 详情
        $resource('/api/as/uc/userwxsk/getUserWxByUserid', {}, {}).
        save({'userid' : userid},function(res) {
            if (res.errcode !== 0) {
                toaster.error({title:"",body:res.errmsg});
                return;
            }
            console.log('点列表获得详情');
            console.log(res);
            $scope.infoobj = res.data;
            
        })
	};


   
 

};