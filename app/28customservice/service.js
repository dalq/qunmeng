/**
 * 子模块service
 * djp
 */
var service = function($resource){

    var userinfo = "/api/ac/uc/userService/getUserInfoByMobileForKf";

    var cleanRedis = "/api/ac/uc/userService/cleanRedis";

    var deleteuserinfo = "/api/ac/uc/userService/deleteuserinfo";

    var updateidcard = "/api/ac/uc/customService/updateUserAuthInfo";

    var updatemobile = "/api/ac/uc/userService/updatemobile";

    var syncUserAuthInfo = "/api/ac/uc/customService/syncUserAuthInfo";

    var oneuserinfo = "/api/as/uc/jyu/getjyuserinfo";

    var edituserinfo = "/api/ac/uc/userService/updateUserInfoByMobile";

    var createuserinfo = "/api/uc/uc/userService/insertUserAuthKF";

    var cardA = "/api/as/cdc/useractiveservice/getphycardlist";

    var cardB = "/api/as/uc/jyu/getdigcardlist";

    var infoticket =  "/api/as/tc/ticketorder/forKefuOrderInfoByMobilelist";

    var destoryticket =  "/tktapi/sc/destoryService/updateByCode";

    var redpackage = "/api/us/uc/jyu/getredpackagelist";

    var updateUserAuthInfo = "/api/ac/uc/userService/updateUserAuthInfo";

    var updateUserSubsidy =  "/api/ac/uc/userService/updateUserSubsidy";

    var orderlist = "/api/as/uc/jyu/getOrderListForKList";

    var sendmessage = "/getsms/service/CW0088";

    var orderbacklist =  "/api/as/tc/ticketorderback/orderbacklist";

    var orderback =  "/api/ac/tc/ticketOrderService/updatebackmoneybycode";

    var updateCardPass =  "/api/ac/cdc/userActiveService/updatecardpasswd";

    var carduserinfo = "/api/ac/uc/userService/getUserInfoUseCardP";

    var destoryticketrecord =  "/api/as/tc/ticketorder/destoryticketrecord";

    var registercount =  "/api/ac/uc/userService/countCard";


    //七星彩查uid
    var getWeekLottey =  '/api/ac/ac/weekLotteryService/getWeekLottey';
    // 个人信息查询
    var lotteyUserinfo = '/api/uc/uc/userService/getUserInfoById';
    // 保存中奖人信息
    var saveUserinfo = '/api/ac/ac/winLotteryService/saveweeklottery';


    //查看激活码
    var getActiveCodeByMobile =  '/api/ac/uc/userActiveService/getActiveCodeByMobile';

    //查看票信息
    var getinfobyid = '/api/as/tc/ticket2/getinfobyid';

    // 获取亲子套票激活用户信息
    var getUserBaseInfomobile =  '/api/ac/uc/customService/getUserBaseInfo';

     //查询票种信息
    var saleticketinfo =  '/api/as/tc/salettype/list';


    //var getUserInfoByIdForChinaUnicom =  '/api/uc/uc/userService/getUserInfoByIdForChinaUnicom';

    var getuserinfobymobile = 'api/uc/uc/userService/getUserInfoByMobile';
    //销售品类型
    var dictbytypelist = '/api/as/sc/dict/dictbytypelist';

    //客服卡订单列表
    var kfcardorderlist = '/api/as/cdc/cardproductorder/kfcardorderlist';
    
    return {

         dictbytypelist : function(){
            return $resource(dictbytypelist, {}, {});
        },

        userinfo : function(){
            return $resource(userinfo, {}, {});
        },
        cleanRedis : function(){
            return $resource(cleanRedis, {}, {});
        },
        updatemobile : function(){
            return $resource(updatemobile, {}, {});
        },
        deleteuserinfo : function(){
            return $resource(deleteuserinfo, {}, {});
        },
        updateidcard : function(){
            return $resource(updateidcard, {}, {});
        },
        syncUserAuthInfo : function(){
            return $resource(syncUserAuthInfo, {}, {});
        },
        oneuserinfo : function(){
            return $resource(oneuserinfo, {}, {});
        },
        edituserinfo : function(){
            return $resource(edituserinfo, {}, {});
        },
        createuserinfo : function(){
            return $resource(createuserinfo, {}, {});
        },
        cardA : function(){
            return $resource(cardA, {}, {});
        },
        cardB : function(){
            return $resource(cardB, {}, {});
        },
        infoticket : function(){
            return $resource(infoticket, {}, {});
        },
        destoryticket : function(){
            return $resource(destoryticket, {}, {});
        },
        redpackage : function(){
            return $resource(redpackage, {}, {});
        },
        updateUserAuthInfo : function(){
            return $resource(updateUserAuthInfo, {}, {});
        },
        updateUserSubsidy : function(){
            return $resource(updateUserSubsidy, {}, {});
        },
        orderlist : function(){
            return $resource(orderlist, {}, {});
        },
        sendmessage : function(){
            return $resource(sendmessage, {}, {});
        },
        orderbacklist : function(){
            return $resource(orderbacklist, {}, {});
        },
        orderback : function(){
            return $resource(orderback, {}, {});
        },
        updateCardPass : function(){
            return $resource(updateCardPass, {}, {});
        },
        carduserinfo : function(){
            return $resource(carduserinfo, {}, {});
        },
        destoryticketrecord : function(){
            return $resource(destoryticketrecord, {}, {});
        },
        registercount : function(){
            return $resource(registercount, {}, {});
        },
        getWeekLottey : function(){
            return $resource(getWeekLottey, {}, {});
        },

        lotteyUserinfo : function(){
            return $resource(lotteyUserinfo, {}, {});
        },

        saveUserinfo : function(){
            return $resource(saveUserinfo, {}, {});
        },
        getActiveCodeByMobile : function(){
             return $resource(getActiveCodeByMobile, {}, {});
        },
        getinfobyid : function(){
             return $resource(getinfobyid, {}, {});
        },
        getUserBaseInfomobile : function(){
             return $resource(getUserBaseInfomobile, {}, {});
        },
        saleticketinfo : function(){
             return $resource(saleticketinfo, {}, {});
        },
        getuserinfobymobile : function(){
            return $resource(getuserinfobymobile, {}, {});
        },
        kfcardorderlist : function(){
            return $resource(kfcardorderlist, {}, {});
        },

       
    };

};

module.exports = service;