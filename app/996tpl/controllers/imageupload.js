module.exports = function($scope, FileUploader, $modalInstance, dir, xxx){

	var para = {
        'url': 'https://txy.juyouhx.com/Api/Api/ObjectToOss'
    };

    //console.log(dir);
    //console.log(xxx);

    angular.extend(para, xxx);

    console.log(para);

	var uploader = $scope.uploader = new FileUploader(para);

    uploader.filters.push({
        name: 'imageFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|pdf|'.indexOf(type) !== -1;
        }
    });

    // uploader.formData.push();

    //如果没传文件夹名，默认成dlq文件夹
    if(dir == '') dir = 'dlq_' + Date.parse(new Date());

    uploader.formData.push({'dir' : dir + "_" + Date.parse(new Date())});
    
    $scope.ok = function () {  
        console.log(uploader);
        var imagearr = [];
        for(var i = 0; i < uploader.queue.length; i++){
            var tmp = uploader.queue[i];
            if(tmp.isSuccess){
                var txt = tmp._xhr.responseText;
                var json = angular.fromJson(txt); 
                imagearr.push(json.url);
            }
        }
        $modalInstance.close(imagearr);
    };  
    $scope.cancel = function () {  
        $modalInstance.dismiss('cancel');  
    };  

};
