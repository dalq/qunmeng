module.exports = function(){

	return {

		restrict : 'AE',
		template : require('../views/dlqform.html'),
		replace:true,
		// scope:{
		// 	'result' : '=',
		// 	'dateshow' : '=',
		// 	'timeshow' : '=',
		// 	'imageshow' : '=',
		// },
		link : function(scope, elements, attrs){

			//打开日历控件
		    scope.open = function($event, item){
		        $event.preventDefault();
		        $event.stopPropagation();
		        item.opened = true;
		    };
		}

	};

};

