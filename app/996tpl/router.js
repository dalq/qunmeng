 /**
 * 子模块路由
 * dlq
 */

var router = function($urlRouterProvider, $stateProvider){


 	$stateProvider
 
	
 	//通用列表
    .state('app.tpl_table', {
        url: "/table/:id",
        views: {
			'main@' : {
				template : require('../996tpl/views/table.html'),
				controller : 'tpltable',
			}
		},
		resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
            urllist : function(sysservice){
                return sysservice.urllist();
            }
        }
    })




    //通用表单
    .state('app.tpl_form', {
        url: "/form/:id",
        views: {
            'main@' : {
                template : require('./views/form.html'),
                controller : 'tplform',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return formservice.formconfig();
            }
        }
    })
    
 	

        // //上传文件
    // .state('app.imageupload', {
    //     url: "/directive/file_upload.html",
    //     views: {
    //         'main@' : {
    //             template : require('./views/fileupload.html'),
    //             controller : 'fileupload',
    //         }
    //     },
    //     // resolve:{
    //     //     tableconfig : function(tableservice){
    //     //         return tableservice.tableconfig();
    //     //     },
    //     // }
    // })

    
    //图片上传
    .state('app.imageupload', {
        url: "/common/imageupload.html",
        template : require('./views/imageupload.html'),
        'size' : 'lg',
        controller : 'imageupload',
        resolve : {  
            'dir' : function(){
                return '';
            },
            'xxx' : function(){
                return {};
            }
        }  
    })

	;

};

module.exports = router;