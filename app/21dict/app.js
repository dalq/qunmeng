var App = angular.module('dict', []);

App.config(require('./router'));

//service
App.factory('dictservice', require('./service'));

//Controllers
App.controller('dictList', require('./controllers/list'));


module.exports = App;