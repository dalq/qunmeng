module.exports = function ($scope, $resource) {

	$scope.maxSize = '5';			//最多显示多少个按钮
    $scope.currentPage = '1';		//当前页码
    $scope.itemsPerPage = '20'		//每页显示几条

	//取消==重置
	$scope.cancel = function () {
		$scope.obj = {};
		$scope.status = {'active': true};
		$scope.tabname = {'name': '添加字典', 'active': false};
	};

	//搜索
	$scope.search = function (pageNo) {
		$scope.cancel();
		var para = {
            'pageNo': pageNo,
			'pageSize': $scope.itemsPerPage,
			'label' : $scope.search.label,
			'description' : $scope.search.description,
        };
		if($scope.search.item){
			para.type = $scope.search.item.value
		}
		$resource('/api/as/sc/dict/getDictList', {}, {}).save(para, function(res){
			if(res.errcode === 0){
				$scope.dict_list = res.data.results;
				$scope.totalItems = res.data.totalRecord;
			}else{
				alert(res.errmsg);
			}
		});
	};
	$scope.search('1');

	//加载搜索栏的数据
	$scope.loadSearch = function() {
		$resource('/api/as/sc/dict/getList', {}, {}).save({}, function(res){
			if(res.errcode === 0){
				$scope.dict_sel = res.data;
			}else{
				alert(res.errmsg);
			}
		});
	};
	$scope.loadSearch();

	//点击页数查询
	$scope.change = function(pageNo){
		$scope.search(pageNo);
	};

	//添加字典页面
	$scope.add = function (item) {
		if($scope.tabname.name == '修改字典'){
			return;
		}
		$scope.tabname.active = true;
		if(item){
			$scope.obj.type = item.type;
			$scope.obj.description = item.description;
			$scope.obj.dict_type = item.dict_type;
			$scope.obj.sort = item.sort + 10;
		} else {
			$scope.obj.dict_type = true;
			$scope.obj.sort = 10;
		}
	};

	//修改字典页面
	$scope.update = function (item) {
		$scope.tabname = {'name': '修改字典', 'active': true};
		$scope.obj = item;
	};

	//删除字典
	$scope.delete = function (index) {
		if (confirm('确认要删除字典吗？')==true){
			$resource('/api/as/sc/dict/delDict', {}, {}).save({'id': $scope.dict_list[index].id}, function(res){
				if(res.errcode === 0){
					$scope.dict_list.splice(index,1);
				}else{
					alert(res.errmsg);
				}
			});
		}
	};

	//保存
	$scope.save = function () {
		$resource('/api/as/sc/dict/save', {}, {}).save($scope.obj, function(res){
			if(res.errcode === 0){
				$scope.search('1');
			}else{
				alert(res.errmsg);
			}
		});
	};


};