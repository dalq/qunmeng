var service = function($resource, $q, $state, $modal){
     // 用户信息
    var userinfo = BASEURL38985 + "/api/as/info";
    // 查看全部说明
    var findExplainList = BASEURL38985 + '/api/as/mc/merexplaindao/findExplainList';
    // 查看单条说明
    var getAdminExplain = BASEURL38985 + '/api/as/mc/merexplaindao/getAdminExplain';
    // 保存说明
    var saveExplain = BASEURL38985 + '/api/as/mc/merexplaindao/saveExplain';
    // 修改说明
    var updateExplain = BASEURL38985 + '/api/as/mc/merexplaindao/updateExplain';
    // 删除说明
    var updateDelIns = BASEURL38985 + '/api/as/mc/merexplaindao/updateDel';
    //销售品类型
    var dictbytypelist = BASEURL38985 + '/api/as/sc/dict/dictbytypelist';
    
    
    return {
        userinfo : function(){
            return $resource(userinfo, {}, {});
        },
        findExplainList : function(){
             return $resource(findExplainList, {}, {});
        },
        saveExplain : function(){
             return $resource(saveExplain, {}, {});
        },
        updateExplain : function(){
             return $resource(updateExplain, {}, {});
        },
        updateDelIns : function(){
             return $resource(updateDelIns, {}, {});
        },
        getAdminExplain : function(){
             return $resource(getAdminExplain, {}, {});
        },
        dictbytypelist : function(){
             return $resource(dictbytypelist, {}, {});
        }
    };
};
module.exports = service;