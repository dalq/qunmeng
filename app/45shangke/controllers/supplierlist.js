module.exports = function($scope, $state, $stateParams,$resource,$modal,toaster){
    $scope.statearr = [{'label' : '申请中', 'value' : '0'},{'label' : '已联系', 'value' : '1'}];
    /* 分页
        * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10         //每页显示几条
    $scope.searchform = {};
    // 获取申请人列表信息
    $scope.getlist = function(){
        var para = {
            pageNo:$scope.bigCurrentPage, 
            pageSize:$scope.itemsPerPage
        };
        para = angular.extend($scope.searchform,para);
        $resource('/api/as/mc/mersupplierapply/findSupplierApplyList', {}, {}).
        save(para, function(res){
            if (res.errcode !== 0) {
                toaster.error({title:"",body:res.errmsg});
                return; 
            }
            console.log(res);
            $scope.objs = res.data.results;
            $scope.bigTotalItems = res.data.totalRecord;
        });
    };
    $scope.getlist();
    // 申请人确认
    $scope.supplierconfirm = function(item){
        var modalInstance = $modal.open({
        template: require('../views/supplyremark.html'),
        controller: 'supplyremark',
        size: 'lg',
        resolve: {
            item: function () {
                return item;
            }

         }
        });
        modalInstance.result.then(function (showResult) {	
            $scope.getlist();
        });
        
    };

};