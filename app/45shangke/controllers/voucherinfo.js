module.exports = function($scope, $state, $stateParams, $modalInstance, code, $resource,$modal){
    $resource('/api/as/tc/voucherorder/voucherlist', {}, {}).
    save({'order_code' : code}, function(res){
        console.log(res);
        if(res.errcode === 0)
        {
            $scope.objs = res.data;
            console.log(res.data);
        }
        else
        {
            alert(res.errmsg);
            //toaster.success({title:"",body:res.errmsg});
        }
    });
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
    $scope.usedinfo = function(code) {
	  	$state.go('app.usedorderlist', {'code' : code});
	  	$modalInstance.dismiss('cancel');
	}

};