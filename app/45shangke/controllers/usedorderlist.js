module.exports = function($scope, $state, $stateParams,$resource,$modal){
    /* 分页
     * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.currentPage = 1;      //当前页码
    $scope.itemsPerPage = 10         //每页显示几条

    $scope.searchform = {};

    $scope.searchform.verification_code = $stateParams.code;
    $scope.pay_statearr = [{'label' : '未支付', 'value' : '0'},{'label' : '已支付', 'value' : '1'}];

    

    $scope.date = {
        // 'lable': date2str2(new Date()),
        'value': date2str(new Date()),
        'opened': false
    }
     $scope.date1 = {
        // 'lable': date2str2(new Date()),
        'value': date2str(new Date()),
        'opened': false
    }
    $scope.load = function () {
        $scope.total = {
	        'total' : 0
	    };
        if($scope.date.lable){
            $scope.tour_date=date2str2($scope.date.lable);
        }else{
            $scope.tour_date=''
        }
        if($scope.date1.lable){
            $scope.tour_date_two=date2str2($scope.date1.lable);
        }else{
            $scope.tour_date_two=''
        }

        if($scope.date.lable!=null && $scope.date1.lable!=null){
            if($scope.tour_date>$scope.tour_date_two){
                alert('开始日期不能大于结束日期');
                return;
            }
        }
        if($scope.date.lable){
            if(!$scope.date1.lable){
                alert('开始和结束日期必须同时输入进行搜索')
                return;
            }
        }
        if($scope.date1.lable){
            if(!$scope.date.lable){
                alert('开始和结束日期必须同时输入进行搜索')
                return;
            }
        }
        var para = {
           // pageNo:$scope.bigCurrentPage, 
            //pageSize:$scope.itemsPerPage,
            start_time : $scope.tour_date,
            end_time : $scope.tour_date_two,
        };

        para = angular.extend($scope.searchform, para);
        console.log(para);
        $resource('/api/as/tc/voucherorder/usedorderlist', {}, {}).
        save(para, function(res){

            console.log(res);

            if(res.errcode === 0)
            {
                $scope.objs=res.data;
                for(var i = 0, j = res.data.length; i < j; i++)
		        {
		            $scope.total.total += res.data[i].pay_price;
	            }
            }
            else
            {   
                aler(res.errmsg);
                // toaster.success({title:"",body:res.errmsg});
            }

        });

    };
    $scope.load();

    $scope.usedorderinfo = function(obj){
       $scope.item = obj;
        console.log($scope.item);
        var modalInstance = $modal.open({
            template: require('../views/usedorderinfo.html'),
            controller: 'usedorderinfo',
            size: 'lg',
            resolve: {
                items: function () {
                    return $scope.item;
                }
            }
        });
        modalInstance.result.then(function (showResult) {	
                $scope.load();
        });
    	
    }

    $scope.b= '0';
    $scope.c = '';
    $scope.show = function(index){
        if($scope.b==0){
            $scope.b='1',
            $scope.c=index
        }else if($scope.b==1){
            if($scope.c!=index){
                $scope.c=index
            }else{
                $scope.b='0',
                $scope.c=index
            }	
        }	
    };

    


    $scope.dateOpen = function ($event, item) {
        console.log('open');
        $event.preventDefault();
        $event.stopPropagation();
        item.opened = true;
    };

   


        $scope.dateOpen1 = function ($event, item) {
        $event.preventDefault();
        $event.stopPropagation();
        item.opened = true;
    };

    function date2str2(d) {
        if (d === undefined) {
            return "";
        }
        var month = (d.getMonth() + 1).toString();
        var day = d.getDate().toString();
        if (month.length == 1) month = '0' + month;
        if (day.length == 1) day = '0' + day;
        return d.getFullYear() + "-" + month + "-" + day;
    }

    function date2str(d) {
        if (d === undefined) {
            return "";
        }
        var month = (d.getMonth() + 1).toString();
        var day = d.getDate().toString();
        if (month.length == 1) month = '0' + month;
        if (day.length == 1) day = '0' + day;
        return d.getFullYear() + month + day;
    }


};