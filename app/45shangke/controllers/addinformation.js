module.exports = function($scope, $state, $stateParams,$resource,$modal,FileUploader,str2date,date2str,toaster){  
    var id = $stateParams.id;
    console.log(id);   
     $scope.date = {
        'lable': date2str(new Date()),
        'opened': false
    }
     $scope.info = {
        'title' : '',
        'img' : '',
        'create_by' : '',
        'num' :'',
        'content' : '',
        'asort' : '',
        'time' : $scope.date.lable +  " 00:00:00"
    }

    
    if(id){
        $resource('/api/as/mc/merinformation/getInformation', {}, {}).
        save({'id' : id},function(res){
            if(res.errcode != 0){
                toaster.success({title:"",body:res.errmsg});
                return;
            }
            console.log(res);
            $scope.info = res.data;
            $scope.date.lable = date2str(str2date(res.data.time));
        })
    }
    
    // 主图
    var uploader = $scope.uploader = new FileUploader({
        url: 'http://cl.juyouhx.com/oss.php/oss/webuploader1?topdir=aa&selfdir=bb'
    });

    uploader.filters.push({
        name: 'imageFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });
    
    
    uploader.onSuccessItem = function(fileItem, response, status, headers) {
        $scope.info.img = response.savename;
    };
    

    $scope.ok = function () {
        console.log('ok');
        if (typeof $scope.date.lable=== 'string') {
            $scope.date.lable = $scope.date.lable
        } else {
            $scope.date.lable = date2str($scope.date.lable);
        }
        if(id){           
            var para = {
                'id' : id,
                'time' : $scope.date.lable
            }; 
            para = angular.extend($scope.info,para); 
            console.log(para);
            $resource('/api/as/mc/merinformation/updateinformation', {}, {}).     
            save(para,function(res){
                if(res.errcode != 0){
                    toaster.error({title:"",body:res.errmsg});
                    return;
                }
                console.log(res);
                toaster.success({title:"",body:"修改成功"});
                // $uibModalInstance.close();
                $state.go('app.shangkeinformationlist');

            })
            
        } else {
            var para = {
                'time' : $scope.date.lable
            }; 
            para = angular.extend($scope.info,para);             
            $resource('/api/as/mc/merinformation/saveInformation', {}, {}).                 
            save(para,function(res){
                if(res.errcode != 0){
                    toaster.error({title:"",body:res.errmsg});
                    return;
                }
                console.log(res);
                toaster.success({title:"",body:"添加成功"});                
                $state.go('app.shangkeinformationlist');
                
                // $uibModalInstance.close();
            })
            
        }
    };

    // $scope.cancel = function () {
    //     $uibModalInstance.dismiss('cancel');
    // };

      $scope.b= '0';
    $scope.c = '';
    $scope.show = function(index){
        if($scope.b==0){
            $scope.b='1',
            $scope.c=index
        }else if($scope.b==1){
            if($scope.c!=index){
                $scope.c=index
            }else{
                $scope.b='0',
                $scope.c=index
            }	
        }	
    };

    
    $scope.dateOpen = function ($event, item) {
        console.log('open');
        $event.preventDefault();
        $event.stopPropagation();
        item.opened = true;
    };

    $scope.dateOpen1 = function ($event, item) {
        $event.preventDefault();
        $event.stopPropagation();
        item.opened = true;
    };

    // function date2str2(d) {
    //     if (d === undefined) {
    //         return "";
    //     }
    //     var month = (d.getMonth() + 1).toString();
    //     var day = d.getDate().toString();
    //     if (month.length == 1) month = '0' + month;
    //     if (day.length == 1) day = '0' + day;
    //     return d.getFullYear() + "-" + month + "-" + day;
    // }

    // function date2str(d) {
    //     if (d === undefined) {
    //         return "";
    //     }
    //     var month = (d.getMonth() + 1).toString();
    //     var day = d.getDate().toString();
    //     if (month.length == 1) month = '0' + month;
    //     if (day.length == 1) day = '0' + day;
    //     return d.getFullYear() + month + day;
    // }
 

};