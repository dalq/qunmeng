module.exports = function($scope, $state, $stateParams,$resource,$modal,date2str,str2date,toaster){
    /* 分页
        * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10         //每页显示几条
    $scope.info = {};
    $scope.date = {
        'lable': '',
        'opened': false
    }
     $scope.date1 = {
        'lable': '',
        'opened': false
    }
   
    
    

  $scope.createactivity = function (item) {
      console.log('1111');
       var modalInstance = $modal.open({
        template: require('../views/creatbargain.html'),
        controller: 'creatbargain',
        size: 'lg',
        resolve: {
            item: function () {
                return item;
            },
            date2str: function () {
                return date2str;
            },
            str2date : function() {
                return str2date;
            }
         }
        });
        modalInstance.result.then(function (showResult) {	
            $scope.getlist();
        });
  };
  
    $scope.getlist = function () {
        var para = {
            pageSize : $scope.itemsPerPage,
            pageNo : $scope.bigCurrentPage,
        };
        para = angular.extend(para,$scope.info); 
        $resource('/api/as/ac/aactactive/findManageActiveList', {}, {}).
        save(para, function (res) {
            console.log(para);
            if (res.errcode != 0) {
                    toaster.error({title: "", body:res.errmsg});
                    return;
            }                
            $scope.objs = res.data.results;
            $scope.bigTotalItems = res.data.totalRecord;
            $scope.objs.forEach(function(element) {
                element.isSelected = element.status=='1'?false:true;
            }, this);
            console.log($scope.objs);
        })
    };
    $scope.getlist();
    $scope.search = function(){
        if (typeof $scope.date.lable === 'string') {
            $scope.date.lable = $scope.date.lable;
        } else {
            $scope.date.lable = date2str($scope.date.lable);
        }
        if (typeof $scope.date1.lable === 'string') {
            $scope.date1.lable = $scope.date1.lable;
        } else {
            $scope.date1.lable = date2str($scope.date1.lable);
        }
        
        var para = {
            'startTime' : ($scope.date.lable),
            'endTime' : ($scope.date1.lable)
        };
        para = angular.extend($scope.info,para);
        $resource('/api/as/ac/aactactive/findManageActiveList', {}, {}).
        save(para, function (res) {
            console.log(para);
            if (res.errcode != 0) {
                    toaster.error({title: "", body:res.errmsg});
                    return;
            }                
            $scope.objs = res.data.results;
            $scope.totalItems = res.data.totalRecord;
            $scope.objs.forEach(function(element) {
                element.isSelected = element.status=='1'?false:true;
            }, this);
            console.log($scope.objs);
        })
    }

   	$scope.seeprizelist = function (id) {
        // $scope.item = obj;
        console.log(id);
   	    $state.go('app.prizelist',{'id' : id});
   	};
   	$scope.edit = function (item) {
        var modalInstance = $modal.open({
        template: require('../views/creatbargain.html'),
        controller: 'creatbargain',
        size: 'lg',
        resolve: {
            item: function () {
                return item;
            },
            date2str: function () {
                return date2str;
            },
            str2date : function() {
                return str2date;
            }
         }
        });
        modalInstance.result.then(function (showResult) {	
            $scope.getlist();
        });
   	};
   	$scope.seepbargainuser = function (id) {
   		 $state.go('app.bargainuser',{'id' : id});
   	};
   	$scope.seewinuser = function (id) {
   		 $state.go('app.winuser',{'id' : id});
   	};

    
    // 开关状态
    $scope.onChange = function(isSelected,id,status){  
        console.log(isSelected);    
       if(isSelected==false){
        $resource('/api/as/ac/aactactive/updateActiveEnd', {}, {}).           
            save({'id':id},function(res){
                if(res.errcode!=0){
                        toaster.error({title: "", body:res.errmsg});
                        return;
                }
                console.log(res);
                $scope.getlist();
                return;
            });
       } else {
        $resource('/api/as/ac/aactactive/updateActiveStart', {}, {}).                      
            save({'id':id},function(res){
                if(res.errcode!=0){
                    toaster.success({title: "", body:res.errmsg});
                    return;
                }
                console.log(res);
                $scope.getlist();
                return;
            });                           
            
       }
    
     };

     $scope.dateOpen = function ($event, item) {
        console.log(item);
        $event.preventDefault();
        $event.stopPropagation();
        item.opened = true;
    };

    $scope.dateOpen1 = function ($event, item) {
        $event.preventDefault();
        $event.stopPropagation();
        item.opened = true;
    };

    
    


};