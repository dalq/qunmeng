module.exports = function($scope,$state,$modal,$resource, toaster){
   /* 分页
        * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10         //每页显示几条
    $scope.getlist = function () {
        var para = {
            pageNo:$scope.bigCurrentPage, 
            pageSize:$scope.itemsPerPage,
        };
        //para = angular.extend($scope.info,para);
        $resource('/api/as/mc/merwithdraw/findMcOrdertTXList', {}, {}). 
        save(para,function (res) {
        if (res.errcode != 0) {
            toaster.error({title:"",body:res.errmsg});
            return;
        }
        console.log(res);
        $scope.objs = res.data.results;
        $scope.bigTotalItems = res.data.totalRecord;
        });
        
    };
    $scope.getlist();

    $scope.change = function(id){
        if(confirm('确认更改状态吗?')){
            $resource('/api/as/mc/merwithdraw/updateMcTiXian', {}, {}).         
            save({'id' : id},function(res){
                if (res.errcode != 0) {
                    toaster.success({title:"",body:res.errmsg});
                    return;
                }
                toaster.success({title:"",body:"更改状态成功"});
                $scope.getlist();
    
            })
        }
        
    }
    

};