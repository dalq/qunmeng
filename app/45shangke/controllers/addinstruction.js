module.exports = function($scope, $state, $stateParams, $modalInstance, items,$resource,$modal,FileUploader,toaster){ 
     $scope.editinfo = {
        'flag' : ''
    }
    if(items){
        $scope.obj = items;  
        id = $stateParams.id; 
        var title_identifier = $stateParams.title_identifier; 
        $scope.editinfo.flag  = 0;             
    } 
    console.log(items.id);
    if (title_identifier) {
        $scope.title_identifier = title_identifier;
    }
    $scope.saveinstruction = function(){
        if (items.id) {
            // 修改
            var para = {
                id : items.id
            };
            para = angular.extend($scope.obj, para);
            if ($scope.obj.title_identifier!==''&&$scope.obj.title!=='') {
                $resource('/api/as/mc/merexplaindao/updateExplain', {}, {}).
                save(para, function(res){
                console.log(para);
                if (res.errcode !== 0) {
                    toaster.error({title: "", body:res.errmsg});
                    return;
                }
                console.log(res);
                toaster.success({title: "", body:"修改成功!"});
                $modalInstance.close();
                });
            } else {
                toaster.error({title: "", body:"信息填写不完全!"});
            }
            
        } else {
            if ($scope.obj.title_identifier!==''&&$scope.obj.title!=='') {
                $resource('/api/as/mc/merexplaindao/saveExplain', {}, {}).                
                save($scope.obj, function(res){
                console.log($scope.saveinfo);
                if (res.errcode !== 0) {
                    toaster.success({title: "", body:res.errmsg});
                    return;
                }
                console.log(res);
                toaster.success({title: "", body:"添加成功!"});                
                $modalInstance.close();
                });
            } else {
                toaster.error({title: "", body:"信息填写不完全!"});
                
        }
            
        }
        
    };
   
};