module.exports = function($scope, $state, $stateParams,$resource,$modal){  
    var id =  $stateParams.id;
    console.log(id);
    $scope.info = {
        'id' : id
    };
    /* 分页
     * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10         //每页显示几条

    $scope.getlist = function () {
        var para = {
            pageNo:$scope.bigCurrentPage, 
            pageSize:$scope.itemsPerPage,
        };
        para = angular.extend($scope.info,para); ///api/as/ac/aactactive/findPrizeList\
        $resource('/api/as/ac/aactactive/findPrizeList', {}, {}).
        save(para,function (res) {
            if (res.errcode != 0) {
                toaster.success({title:"",body:res.errmsg});
                return;
            }
            console.log(res);
            $scope.objs = res.data.results;
            $scope.bigTotalItems = res.data.totalRecord;
        });   
    };
    $scope.getlist();

    // 添加奖品
    $scope.add = function(prizeId){
        // $scope.item = obj;
        var modalInstance = $modal.open({
            template: require('../views/addprize.html'),
            controller: 'addprize',
            size: 'lg',
            resolve: {
                id: function () {
                    return id;
                },
                prizeId : function(){
                    return prizeId;
                },
                // item: function () {
                //     return $scope.item;
                // }
            }
        });
        modalInstance.result.then(function (showResult) {	
                $scope.getlist();
        });
        
    };

    // 编辑奖品
    $scope.edit = function (prizeId,id) {
        var modalInstance = $modal.open({
          template: require('../views/addprize.html'),
          controller: 'addprize',
          size: 'lg',
          resolve: {
            prizeId : function(){
                return prizeId;
            },
            id : function(){
                return id;
            },
            // savePrize : function(){
            //     return savePrize;
            // },
            
          }
        });

        modalInstance.result.then(function () {
          $scope.getlist();
          
        }, function () {
          //$scope.load();
        });
    };

    // 删除奖品
    $scope.delete = function (prizeId) {
      if (confirm('确定删除该奖品吗？')) {
        $resource('/api/as/ac/aactactive/updateDel', {}, {}).          
          save({'id' : prizeId},function (res) {
            if (res.errcode != 0) {
              toaster.success({title:"",body:res.errmsg});
              return;
            }
            console.log(res);
            $scope.getlist();
        })
      }
        
    };
};