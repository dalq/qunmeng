module.exports = function($scope, $state, $stateParams, $modalInstance, $resource,$modal,item,FileUploader,date2str,str2date,toaster){
    $scope.date = {
        'lable': date2str(new Date()),
        'opened': false
    }
     $scope.date1 = {
        'lable': date2str(new Date()),
        'opened': false
    }
    console.log(item);
    if(item != undefined){
        var id = item.id;        
    }
    console.log(id);
    if(id != undefined){
        $resource('/api/as/ac/aactactive/getActiveInfo', {}, {}).
        save({'id' : id},function (res){
            // console.log(para);
            if (res.errcode != 0) {
                toaster.success({title:"",body : res.errmsg});
                return;
            }
            console.log(res);
            $scope.info = res.data;
            $scope.date.lable = date2str(str2date(res.data.startTime));
            $scope.date1.lable = date2str(str2date(res.data.endTime));
        });

    }
    // 活动图片
    var uploader = $scope.uploader = new FileUploader({
        url: 'http://cl.juyouhx.com/oss.php/oss/webuploader1?topdir=aa&selfdir=bb'
    });

    uploader.filters.push({
        name: 'imageFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });
    
    
    uploader.onSuccessItem = function(fileItem, response, status, headers) {
        $scope.info.img = response.savename;
    };
    // $scope.vm = {
    //     'date' : '',
    //     'options' : {
    //         format: "YYYY-MM-DD",
    //         locale : 'zh-cn'
    //     }
    // }
    $scope.save = function () {
        // console.log(time2str($scope.vm.date._d));
       if (typeof $scope.date.lable === 'string') {
            $scope.date.lable = $scope.date.lable;
        } else {
            $scope.date.lable = date2str($scope.date.lable);
        }
        if (typeof $scope.date1.lable === 'string') {
            $scope.date1.lable = $scope.date1.lable;
        } else {
            $scope.date1.lable = date2str($scope.date1.lable);
        }
        if (id){
            var para = {
                'id' : id,
                'startTime' : ($scope.date.lable),
                'endTime' : ($scope.date1.lable),
            };
            console.log(para);
            para = angular.extend($scope.info,para);
            if ($scope.info.title!=''&&$scope.info.everyoneDayTimes!=''&&$scope.info.activeTimes!=''&&$scope.info.img!=''
                &&$scope.info.description!=''&&$scope.info.startTime!=''&&$scope.info.endTime!='') { 
                $resource('/api/as/ac/aactactive/updateActiveInfo', {}, {}).             
                save(para,function (res) {
                    console.log(para);
                    if (res.errcode != 0) {
                        toaster.success({title:"",body : res.errmsg});
                        return;
                    }
                    toaster.success({title:"",body : "💐你，修改成功！"});
                    $modalInstance.close();
                })
            } else {
                toaster.success({title:"",body : "💐你，请将活动数据补充完整！"});
            }     
        // 添加活动
    } else {    
            var para = {
                'startTime' : ($scope.date.lable),
                'endTime' : ($scope.date.lable),
            };
            para = angular.extend($scope.info,para);
            if ($scope.info.title!=''&&$scope.info.everyoneDayTimes!=''&&$scope.info.activeTimes!=''&&$scope.info.img!=''
                &&$scope.info.description!=''&&$scope.info.startTime!=''&&$scope.info.endTime!='') {
                $resource('/api/as/ac/aactactive/saveActive', {}, {}).                             
                save(para,function (res) {
                    console.log(para);
                    if (res.errcode != 0) {
                        toaster.success({title:"",body : res.errmsg});
                            return;
                    }
                    toaster.success({title:"",body : "💐你创建成功！"});
                    $modalInstance.close();
                })

            } else {
                toaster.success({title:"",body : "💐你，请将活动数据补充完整！"});
            }
        
        }
        
    };   
   
    $scope.dateOpen = function ($event, item) {
        console.log(item);
        $event.preventDefault();
        $event.stopPropagation();
        item.opened = true;
    };

    $scope.dateOpen1 = function ($event, item) {
        $event.preventDefault();
        $event.stopPropagation();
        item.opened = true;
    };

    $scope.dateOpen2 = function ($event, item) {
        $event.preventDefault();
        $event.stopPropagation();
        item.opened = true;
    };

    function time2str(objDate) {
        if(angular.isDate(objDate))
        {
            var y = objDate.getFullYear();
            var m = objDate.getMonth();
            var d = objDate.getDate();
            var h = objDate.getHours();
            var mt = objDate.getMinutes();
            // var s = objDate.getSeconds();
            return y + '-' + (m + 1) + '-' + d + ' ' + h + ':' + mt;
        }
        else
        {
            return '错误格式';
        }
    }

    

};