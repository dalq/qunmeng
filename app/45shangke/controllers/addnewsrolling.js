module.exports = function($scope, $state, $stateParams, $modalInstance, item,$resource,$modal,FileUploader,toaster){  
    if(item != undefined){
        var id = item.id;
        console.log(id);
    }
    $scope.info = item;
    var uploader = $scope.uploader = new FileUploader({
        url: 'http://cl.juyouhx.com/oss.php/oss/webuploader1?topdir=aa&selfdir=bb'
    });

    uploader.filters.push({
        name: 'imageFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });
    
    
    uploader.onSuccessItem = function(fileItem, response, status, headers) {
        console.log(response.savename);
        $scope.info.photo = response.savename;
    };
	$scope.save = function() {
        if (id && $scope.info.title!=''&&$scope.info.type!=''&&$scope.info.photo!=''&&$scope.info.url!='') {
            var para = {
                'id' : id
            }
            para = angular.extend($scope.info,para);
            $resource('/api/as/gc/rollingpicture/updateRolling', {}, {}).
            save(para,function(res){
                console.log(para);
                if (res.errcode !== 0) {
                    toaster.error({title: "", body:res.errmsg});
                    return;
                }
                console.log(res.data);
                toaster.success({title: "", body:"修改成功"});
                $modalInstance.close();
                // $state.go('app.newsrollinglist');
            });


        } else if ($scope.info.title!=''&&$scope.info.type!=''&&$scope.info.photo!=''&&$scope.info.url!='') {
            $resource('/api/as/gc/rollingpicture/saveNewsPhoto', {}, {}).            
            save($scope.info,function(res){
                console.log($scope.info);
                if (res.errcode !== 0) {
                    toaster.error({title: "", body:res.errmsg});
                    return;
                }
                console.log(res.data);
                toaster.success({title: "", body:"添加成功"});
                $modalInstance.close();                
            });

        } else {
            toaster.error({title: "", body:"请将数据补充完整"});
        }
            
        
        
    };
        

};