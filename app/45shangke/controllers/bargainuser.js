module.exports = function($scope, $stateParams, $state, $modal,$resource,toaster){
  	var id = $stateParams.id;
  	$scope.info = {
		'id' : id,
		'userName' : '',
		'mobile' : ''
	}

  /* 分页
     * ========================================= */
    $scope.maxSize = 5;            //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10;         //每页显示几条

		$scope.search = function () {
			// var para = {
			// 		pageNo:$scope.currentPage, 
			// 		pageSize:$scope.itemsPerPage,
			// };
			// para = angular.extend($scope.info,para); 
			$resource('/api/as/ac/aactactive/findJoinUserList', {}, {}).
			save($scope.info, function (res) {
					console.log($scope.info);
					if (res.errcode != 0) {
						alert(res.errmsg);
							// toaster.success({title:"",body : res.errmsg});
						return;
					}
					console.log(res);
					$scope.objs = res.data.results;
					$scope.bigTotalItems = res.data.totalRecord;
			});
		};
		// $scope.search();		


		$scope.getlist = function () {
			var para = {
					pageNo:$scope.bigCurrentPage, 
					pageSize:$scope.itemsPerPage
			};
				para = angular.extend({'id' : id},para); 
				$resource('/api/as/ac/aactactive/findJoinUserList', {}, {}).				
				save(para, function (res) {
					console.log(para);
					if (res.errcode != 0) {
						toaster.success({title:"",body : res.errmsg});
						return;
					}
					console.log(res);
					$scope.objs = res.data.results;
					$scope.totalItems = res.data.totalRecord;

				})
		};
		$scope.getlist();

};