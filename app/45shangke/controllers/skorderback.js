module.exports = function($scope, $state, $stateParams, $modalInstance, id,back_price,actual_back_price,order_code,rebate_price,total_integral,$resource,$modal,FileUploader,toaster){
	console.log(id);
	console.log(back_price,actual_back_price);
	$scope.obj = {
		'remark' : '',
		'rebate_price' : rebate_price
	};
	$scope.info = {
		'back_price' : back_price * 0.01,
		'total_integral' : total_integral,
		'actual_back_price' : actual_back_price * 0.01
	}
	$scope.obj.id = id;
	$scope.obj.back_price = (back_price - actual_back_price)* 0.01;

	$scope.cancel = function(){

		$modalInstance.close();

	}

	$scope.gogo = function(){
		console.log($scope.obj.remark);
		if($scope.obj.back_price === undefined || $scope.obj.back_price == '')
		{
			toaster.error({title:"", body:"退款金额不能为空"});
		} else if($scope.obj.remark == ''){
			toaster.error({title:"", body:"退款说明不能为空"});
		} else {
			$resource('/api/ac/mc/merorderserviceimpl/updateBackOrder', {}, {}).
			save({'id' : id, 'back_price' : $scope.obj.back_price*100, 'remark' : $scope.obj.remark, 'order_code' : order_code}, function(res){
				console.log({'id' : id, 'back_price' : $scope.obj.back_price*100, 'remark' : $scope.obj.remark, 'order_code' : order_code});
				if(res.errcode === 0)
				{
					toaster.success({title:"", body:"退款成功"});
					$modalInstance.close();
				}
				else
				{
					toaster.error({title:"", body:res.errmsg});
				}
	
			});

		}
		
		
	}



};