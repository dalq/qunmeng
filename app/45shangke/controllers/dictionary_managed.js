module.exports = function($scope, tableconfig,$state,$modal,$resource,toaster){
    $scope.searchform = {};
    /* 分页
        * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10         //每页显示几条

    $scope.search = function () {
        var para = {
                pageNo:$scope.bigCurrentPage, 
                pageSize:$scope.itemsPerPage,
            };
        para = angular.extend($scope.searchform,para); 
        $resource('/api/as/mc/mertradetypedao/findtradetypelist', {}, {}).
        save(para, function (res) {
            if (res.errcode != 0) {
                toaster.error({title:"",body : res.errmsg});
                return;
            }
            $scope.objs = res.data.results;
            $scope.bigTotalItems = res.data.totalRecord;
        });
      };

    $scope.gettypelist = function (){
        $resource('/api/us/mc/mertradetypedao/findTypeList', {}, {}).        
        save($scope.searchform,function(res){
            if (res.errcode!=0) {
                toaster.error({title:"",body : res.errmsg});
                return;
            }
            console.log(res);
            $scope.datas = res.data;

        })
    };
    $scope.gettypelist();

    $scope.getlist = function(){
        var para = {
            pageNo:$scope.bigCurrentPage, 
            pageSize:$scope.itemsPerPage
        };
        $resource('/api/as/mc/mertradetypedao/findtradetypelist', {}, {}).        
        save(para,function(res){
        if (res.errcode !== 0) {
            toaster.error({title:"",body : res.errmsg});
            return;
        }
        console.log(res);
        $scope.objs = res.data.results;
        $scope.bigTotalItems = res.data.totalRecord;
        });
    };
    $scope.getlist();
    $scope.adddic = function(){
        $state.go('app.shangkedictionaryinfo');
    };
    $scope.change = function(id){
        $state.go('app.shangkedictionaryinfo',{'id':id});

    };
    $scope.delete = function(id){
        if (confirm('确定删除吗?')) {
        $resource('/api/as/mc/mertradetypedao/del', {}, {}).            
            save({'id' : id},function(res){
                if (res.errcode != 0) {
                    toaster.error({title:"",body : res.errmsg});
                    return;
                }
                console.log(res);
                toaster.success({title: "",body : "删除成功!"});
                $scope.getlist();
                $scope.gettypelist();
                
            });
            return;
        }
    };

};