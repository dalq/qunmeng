module.exports = function($scope, $state, $stateParams, $modal,$resource,toaster){
  var id = $stateParams.id;
  $scope.searchform = {
    'id' : id,
    'userName' : '',
    'mobile' : ''
  }
  /* 分页
     * ========================================= */
    $scope.maxSize = 5;            //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10;         //每页显示几条       //每页显示几条
    $scope.search = function () {
    var para = {
        pageNo:$scope.bigCurrentPage, 
        pageSize:$scope.itemsPerPage,
    };
    para = angular.extend($scope.searchform,para); 
    $resource('/api/as/ac/aactactive/findWinPrizeUserList', {}, {}).
    save(para,function (res) {
      if (res.errcode != 0) {
          toaster.error({title:"",body:res.errmsg});
          return;
      }
      console.log(res);
      $scope.objs = res.data.results;
      $scope.bigTotalItems = res.data.totalRecord;
    });
    
  };
  $scope.search();

  
};