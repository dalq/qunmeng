module.exports = function($scope, $state, $resource,$modal,toaster){
    $scope.order_statearr = [{'label' : '未处理', 'value' : '0'}, {'label' : '已通过', 'value' : '1'},{'label' : '未通过', 'value' : '2'}];
    $scope.smsStateArr = [{'label' : '未发送', 'value' : '0'},{'label' : '已发送', 'value' : '1'}];

    $scope.searchform = {};
    // 获取用户信息
    var company_id = '';
    var company_code = '';
    var office_id = '';
	$resource('/api/as/info', {}, {}).    
    save({}, function(res){ 
    	console.log(res);
    	$scope.loginuser = res;
        company_id = res.company_id;
        company_code = res.company_code;
        office_id = res.office_id;

    });
    console.log($scope.loginuser);

    $scope.getlist = function(){
		$resource('/api/as/tc/tktdealerapplydao/finddealerapplylist', {}, {}).
        save($scope.searchform,function(res){
        if (res.errcode !== 0) {
            toaster.error({tltle: "" , body:res.errmsg});
            return;
        }
        console.log(res);
        $scope.objs = res.data;
        });
	}
	$scope.getlist();
    $scope.seepicture = function(pimg){
        console.log('seee');
        var modalInstance = $modal.open({
            template: require('../views/skacountpicture.html'),
            controller: 'skacountpicture',
            size: 'lg',
            resolve: {
                pimg: function () {
                    return pimg;
                }
            }
        });
        modalInstance.result.then(function (showResult) {	
                $scope.getlist();
        });
    }

	// $scope.search = function(obj){
    //     var para = {
	// 		'title': obj.title,
	// 		'mobile': obj.mobile,
    //         'idcard' : obj.idcard,
	// 	};
    //     console.log(para);
	// 	$resource('/api/as/tc/tktdealerapplydao/finddealerapplylist', {}, {}).save(para, function(res){
	// 		if(res.errcode === 0){            
	// 			tableconfig.search();
	// 		}else{
    //             toaster.error({title:"",body:res.errmsg});                
	// 		}
	// 	});
	// };

    
	
	// 通过
	$scope.pass = function(obj,ticket_id){ 
        //利用对话框返回的值 （true 或者 false）  
        if (confirm("你确定要通过吗?")) {
		$resource('/api/as/tc/tktdealerapplydao/updatestate', {}, {}).save({'id' : obj.id}, function(res){  
            console.log({'id' : id});          
	                if (res.errcode !== 0) { 
                        toaster.error({title:"",body:res.errmsg});
    	            } else { 
                        $scope.objss = res.data;
                        console.log($scope.objss);
                        toaster.success({title:"",body:'通过'});                        
                        $scope.getlist();     
    	            }
	            });
            
        } else {  
            console.log("点击了取消");  
        }  	
	};

	// 拒绝
	$scope.refuse = function(obj){ 
      if (confirm("你确定要拒绝吗?")) {
        $resource('/api/as/tc/tktdealerapplydao/updatestateno', {}, {}).
        save({'id' : obj.id}, function(res){ 
        if (res.errcode !== 0) { 
            toaster.success({title:"",body:res.errmsg});
            return;
        }  
        $scope.getlist();
        }); 
	  } else { 
	  	 console.log("点击了取消");
	  }
      
	};

	//分配权限
	$scope.assignauthority = function(obj){
        $resource('/api/as/tc/tktdealerapplydao/updatestate', {}, {}).save({'id' : obj.id}, function(res){
            if(res.errcode != 0){
                toaster.error({title:"",body:res.errmsg});
                return;
            }            
	        toaster.success({title:"",body:"💐你,获取到权限!"});
        });
	};

	// 创建账号
	$scope.creataccount = function(obj){ 	
        console.log(obj);	
		var modalInstance = $modal.open({
          template: require('../views/creataccount.html'),
          controller: 'creataccount',
          //size: 'lg',
          resolve: {
            // role : function(){
            //     return role;
            // },
            // create : function(){
            //     return create;
            // },
            // message : function(){
            //     return message;
            // },
            id : function(){
                return obj.id;
            },
            company_id : function(){
                return $scope.loginuser.company_id;
            },
            company_code : function(){
                return $scope.loginuser.company_code;
            },
            office_id : function(){
                return $scope.loginuser.office_id;
            },
            // insertnops : function(){
            //     return insertnops;
            // }
            
          }
        });

        modalInstance.result.then(function () {
          toaster.success({title:"",body:"账号创建成功!"});
         $scope.getlist();
        }, function () {
        
        });
	};
    
    // 发送短信
    $scope.sendmessage = function(obj){ 
    	console.log(obj.id);
        if(confirm('确定发信息吗?')){
            $resource('/api/ac/tc/tktdealerapplyservice/updatesms', {}, {}).save({'id' : obj.id}, function(res){
                if(res.errcode === 0){
                    toaster.error({title:"",body:res.errmsg});
                    return;
                }else{
                    toaster.success({title:"",body:"发送短信验证码成功!"});
                }
            });
        }
    	
    };


    
    
};