module.exports = function($scope, $state, $stateParams,$resource,$modal,toaster){ 
    /* 分页
        * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10         //每页显示几条
    
    $scope.getlist = function () {
        var para = {
            pageNo:$scope.bigCurrentPage, 
            pageSize:$scope.itemsPerPage,
        };
        $resource('/api/as/gc/viewad/findViewList', {}, {}).
        save(para,function(res) {
            if (res.errcode!=0) {
                toaster.error({title : "", body:res.errmsg});
                return;
            }
            console.log(res);
            $scope.objs = res.data.results;
            $scope.bigTotalItems = res.data.totalRecord;
        })
    };
    $scope.getlist();
    $scope.delete = function(ad_id) {
        if (confirm('确定要删除吗')) {
            $resource('/api/as/gc/viewad/del', {}, {}).            
            save({'ad_id':ad_id},function (res) {
                console.log(ad_id);
                if (res.errcode!=0) {
                    toaster.error({title : "", body:res.errmsg});                    
                    return;
                }
                console.log(res);
                toaster.success({title: "", body:"删除成功"});
                $scope.getlist();

            });
        } else {

        }
        
        
    };
    $scope.creatad = function(item) {
        var modalInstance = $modal.open({
        template: require('../views/addad.html'),
        controller: 'addad',
        size: 'lg',
        resolve: {
            item: function () {
                return item;
            },
         }
        });
        modalInstance.result.then(function (showResult) {	
            $scope.getlist();
        });
    };
    $scope.edit = function(item) {
        var modalInstance = $modal.open({
        template: require('../views/addad.html'),
        controller: 'addad',
        size: 'lg',
        resolve: {
            item: function () {
                return item;
            },
         }
        });
        modalInstance.result.then(function (showResult) {	
            $scope.getlist();
        });
    }

    

};