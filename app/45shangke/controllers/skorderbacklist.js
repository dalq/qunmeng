module.exports = function($scope, tableconfig,$state,$modal,$resource,toaster){

	$scope.searchform = {
         'from_app_id' : 'shangke'
    };
    $scope.payment_typearr = [];
    $scope.back_statearr = [];
    $scope.ticket_statearr = [];
    $scope.from_orderarr = [{'label' : '商客H5', 'value' : 'shangke'}, {'label' : '商客APP', 'value' : 'shangke_app'}];



	/* 分页
        * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10         //每页显示几条
    $scope.change = function(){
        $scope.currentPage = 1; 
    }

    $scope.load = function () {

    	var para = {
            pageNo:$scope.bigCurrentPage, 
            pageSize:$scope.itemsPerPage
        };

        para = angular.extend(para, $scope.searchform);
        console.log(para);
        $resource('/api/ac/mc/merorderserviceimpl/orderbacklist', {}, {}).
        save(para, function(res){

         	console.log(res);

         	if(res.errcode !== 0)
         	{
                toaster.error({title:"", body:res.errmsg});
         		return;
         	}

         	$scope.objs = res.data.results;
            $scope.bigTotalItems = res.data.totalRecord;

        });

    };
    $scope.load();

	$scope.back = function(obj){
        console.log(obj);
        var modalInstance = $modal.open({
        template: require('../views/skorderback.html'),
        controller: 'skorderback',
        size: 'lg',
        resolve: {
            id : function(){
                return obj.id;
            },
            back_price : function(){
                return obj.back_price;
            },
            actual_back_price : function(){
                return obj.actual_back_price;
            },
            order_code : function(){
                return obj.order_code;
            },
            rebate_price : function(){
                return obj.rebate_price;
            },
            total_integral : function(){
                return obj.total_integral;
            }


         }
        });
        modalInstance.result.then(function (showResult) {	
            $scope.load();
        });
		// var modalInstance = $uibModal.open({
	    //       template: require('../views/skorderback.html'),
	    //       controller: 'skorderback',
	    //       size: 'xs',
	    //       resolve: {
	    //         id : function(){
	    //             return obj.id;
	    //         },
	    //         back_price : function(){
	    //             return obj.back_price;
	    //         },
	    //         orderback : function(){
	    //             return orderback;
	    //         }
	    //       }
        // });

        // modalInstance.result.then(function () {
        //     $scope.load();
        // }, function () {
            
        // });
	}



    $resource('/api/as/sc/dict/dictbytypelist', {}, {}). 
    save({'type' : 'ticket_payment_type'},function(res) {
        console.log(res);
        if(res.errcode === 0)
        {
            $scope.payment_typearr = res.data;
        }
        else
        {
            toaster.error({title:"", body:res.errmsg});
        }
    });

    $resource('/api/as/sc/dict/dictbytypelist', {}, {}). 
    save({'type' : 'ticket_back_pay_state'},function(res) {
        console.log(res);
        if(res.errcode === 0)
        {
            $scope.back_statearr = res.data;
        }
        else
        {
            toaster.error({title:"", body:res.errmsg});
        }
    });

    $resource('/api/as/sc/dict/dictbytypelist', {}, {}). 
    save({'type' : 'ticket_back_ticket_state'},function(res) {
        console.log(res);
        if(res.errcode === 0)
        {
            $scope.ticket_statearr = res.data;
        }
        else
        {
            toaster.error({title:"", body:res.errmsg});
        }
    });

    
};