module.exports = function($scope,$state,$modal,$resource){ 
	 /* 分页
        * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10         //每页显示几条
	$scope.somepara = {};
    $scope.getlist = function(){
		 var para = {
			pageNo:$scope.bigCurrentPage, 
			pageSize:$scope.itemsPerPage
		};
		para = angular.extend($scope.somepara,para);
		$resource('/api/as/mc/merexplaindao/findExplainList', {}, {}).
        save(para,function(res){
        if (res.errcode !== 0) {
            toaster.error({title:"",body:res.errmsg});
            return;
        }
        console.log(res);
        $scope.objs = res.data.results;
        $scope.bigTotalItems = res.data.totalRecord;
        });
	}
	$scope.getlist();
   
    $scope.addinstruction = function(item){
        $scope.item = '';
		console.log($scope.item);
		var modalInstance = $modal.open({
			template: require('../views/addinstruction.html'),
			controller: 'addinstruction',
			size: 'lg',
			resolve: {
				items: function () {
					return $scope.item;
				}
			}
		});
		modalInstance.result.then(function (showResult) {	
            $scope.getlist();
        });
		
    };

    $scope.edit = function(item){
		$scope.item = item;
        console.log(item);
		var modalInstance = $modal.open({
			template: require('../views/addinstruction.html'),
			controller: 'addinstruction',
			size: 'lg',
			resolve: {
				items: function () {
					return $scope.item;
				}
			}
		});
		modalInstance.result.then(function (showResult) {	
            $scope.getlist();
        });

		// modalInstance.opened.then(function () {// 模态窗口打开之后执行的函数  
		// 	console.log('modal is opened');
		// });
		// modalInstance.result.then(
		// 	function (result) {
		// 		console.log(result);
		// 	},
		// 	function (reason) {
		// 		console.log("点击空白区域了");// 点击空白区域，总会输出backdrop  
		// 		// click，点击取消，则会暑促cancel  
		// 		// $log.info('Modal dismissed at: ' + new Date());  
		// 	}
		// );
	

    };
    $scope.delete = function(id){
        if (confirm('确定删除吗?')) {
		$resource('/api/as/mc/merexplaindao/updateDel', {}, {}).			
            save({'id' : id},function(res){
                if (res.errcode != 0) {
                    toaster.error({title:"",body:res.errmsg});
                    return;
                }
                console.log(res);
                toaster.success({title:"",body:"删除成功"});
                $scope.getlist();
            });
            return;
        }
        
    };
	// $scope.search = function(){

	// }

    // $scope.detail = function(id,title_identifier){
    //     $state.go('app.addinstruction',{'id':id, 'title_identifier':title_identifier});
    // };
     
   


   
   
};