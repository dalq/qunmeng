module.exports = function($scope, $state, $stateParams,$resource,$modal,toaster){   
	 /* 分页
     * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10         //每页显示几条

    // $scope.info = {
    // 	'state' : '0'
    // };
    $scope.getlist = function () {
    	var para = {
            pageNo:$scope.bigCurrentPage, 
            pageSize:$scope.itemsPerPage
        };
       // para = angular.extend($scope.info, para);
        $resource('/api/as/mc/merintegralmal/findSaleList', {}, {}).
        save(para,function(res){
    		console.log(para);
    		if (res.errcode !== 0) {
    			toaster.error({title:"",body:res.errmsg});
    			return;
    		}
 			console.log(res);
 			$scope.objs = res.data.results;
            $scope.bigTotalItems = res.data.totalRecord;
            // $scope.bigTotalItems = res.data.totalRecord;
    	});
    };
    $scope.getlist();

    
    // $scope.delete = function() {

    //     $state.go('app.addintegralgoods');
    // };

    $scope.delete = function(id){
        if(confirm('确定要删除吗??')){
            $resource('/api/as/mc/merintegralmal/updateDel', {}, {}).
            save({'id': id},function(res){
                if (res.errcode !== 0) {
                    toaster.error({title:"",body:res.errmsg});
                    return;
                }
                toaster.success({title:"",body:"删除成功"});
                $scope.getlist();
            });
        }
    };

    $scope.update = function(item){
        console.log(item);
        var modalInstance = $modal.open({
            template: require('../views/addintegralgoods.html'),
            controller: 'addintegralgoods',
            size: 'lg',
            resolve: {
                item: function () {
                    return item;
                }
            }
        });
        modalInstance.result.then(function (showResult) {	
                $scope.getlist();
        });
    };
    $scope.addgoods = function(item){
        var modalInstance = $modal.open({
            template: require('../views/addintegralgoods.html'),
            controller: 'addintegralgoods',
            size: 'lg',
            resolve: {
                item: function () {
                    return item;
                }
            }
        });
        modalInstance.result.then(function (showResult) {	
            $scope.getlist();
        });
    }


};