module.exports = function($scope, $stateParams, $state,FileUploader,$resource,toaster){ 
   var id = $stateParams.id;
   console.log(id);
   $scope.info={
        'id':'',
        'code':'',
        'label':'',
        'type':'',
        'info':'',
        'url' : '',
        'asort':'',
        'img': '',
        'remark':'',
        'app_url' : '',
        'display_status' : '1'
   }
    $scope.searchform = {
        'selected' :{
            'type' : ''
        }
    };

    // 主图
    var uploader = $scope.uploader = new FileUploader({
        url: 'http://cl.juyouhx.com/oss.php/oss/webuploader1?topdir=aa&selfdir=bb'
    });
    uploader.filters.push({
        name: 'imageFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });    
    uploader.onSuccessItem = function(fileItem, response, status, headers) {
        $scope.info.img = response.savename;
    };
    $scope.showinfo = {
        'isSelected' : true
    }

    //详情
    if (id) {
        $resource('/api/as/mc/mertradetypedao/getInfoById', {}, {}).            
        save({'id':id},function(res){
            if (res.errcode != 0) {
                toaster.error({title:"",body : res.errmsg});
                return;
            }
            console.log(res.data);
            $scope.info = res.data;
            if(res.data.display_status == '0'){
                $scope.showinfo.isSelected = false;
            } else {
                $scope.showinfo.isSelected = true;
            }


        })
    }
    $scope.changeState = function(state){
        console.log(state);
        if(state == true){
            $scope.info.display_status = '1';
        } else {
            $scope.info.display_status = '0';
        }
    }
    $scope.gettypelist = function (){
        $resource('/api/us/mc/mertradetypedao/findTypeList', {}, {}).        
        save($scope.searchform,function(res){
            if (res.errcode!=0) {
                toaster.error({title:"",body : res.errmsg});
            }
            console.log(res);
            $scope.datas = res.data;

        })
    };
    $scope.gettypelist();
    $scope.save = function () {
        console.log('save');
        $resource('/api/as/mc/mertradetypedao/save', {}, {}).                
        save($scope.info, function (res) {
            console.log($scope.info);
            $scope.info.type=$scope.searchform.selected.type;
            if (res.errcode != 0) {
                toaster.error({title:"",body : res.errmsg});
                return;
            }
            toaster.success({title:"",body : "操作成功"});
            $state.go('app.shangkedictionary_managed');
    });
  };

};