module.exports = function($scope, $state, $stateParams,$resource,$modal,toaster){   
	/* 分页
        * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10         //每页显示几条
    
    $scope.getlist = function () {
        var para = {
            pageNo:$scope.bigCurrentPage, 
            pageSize:$scope.itemsPerPage
        };
        // para = angular.extend($scope.parameters, para);
        $resource('/api/as/gc/rollingpicture/findNewsRollinginfolist', {}, {}).
        save(para,function(res) {
            console.log(para);
            if (res.errcode !== 0) {
                toaster.error({title:"",body:res.errmsg});
                return;
            }
            console.log(para);
            console.log(res);
            $scope.objs = res.data.results;
            $scope.bigTotalItems = res.data.totalRecord;

            
        })
    };
     
    $scope.getlist();

    $scope.addnewsrolling = function(item) {
        var modalInstance = $modal.open({
        template: require('../views/addnewsrolling.html'),
        controller: 'addnewsrolling',
        size: 'lg',
        resolve: {
            item: function () {
                return item;
            },
         }
        });
        modalInstance.result.then(function (showResult) {	
            $scope.getlist();
        });
        // $state.go('app.addnewsrolling');
    };
    $scope.delete = function(id) {
        if (confirm('确定要删除吗?')) {
            $resource('/api/as/gc/rollingpicture/del', {}, {}).            
            save({'id':id},function(res) {
                console.log({'id':id});
                if (res.errcode !== 0) {
                    toaster.error({title:"",body:res.errmsg});
                    return;
                } 
                toaster.success({title:"",body:"删除成功!"});
                $scope.getlist();
            })
            return;
        } 
        
    };

    $scope.edit = function(item) {
        var modalInstance = $modal.open({
        template: require('../views/addnewsrolling.html'),
        controller: 'addnewsrolling',
        size: 'lg',
        resolve: {
            item: function () {
                return item;
            },
         }
        });
        modalInstance.result.then(function (showResult) {	
            $scope.getlist();
        });
        // $state.go('app.addnewsrolling',{'id':id});
    };
    

 

};