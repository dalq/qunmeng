 /**
 * 子模块路由
 * DHpai
 */

var router = function($urlRouterProvider, $stateProvider){


 	$stateProvider
     //字典;列表
    .state('app.shangkedictionary_managed', {
        url: "/shangke/dictionary_managed.html",
        views: {
            'main@' : {
                template : require('../45shangke/views/dictionary_managed.html'),
                controller : 'shangkedictionary_managed',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
        }
    })
     //字典详情
    .state('app.shangkedictionaryinfo', {
        url: "/shangke/dictionaryinfo.html/:id",
        views: {
            'main@' : {
                template : require('../45shangke/views/dictionaryinfo.html'),
                controller : 'shangkedictionaryinfo',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
        }
    })
    //退款列表
    .state('app.shangkeskorderbacklist', {
        url: "/shangke/skorderbacklist.html",
        views: {
            'main@' : {
                template : require('../45shangke/views/skorderbacklist.html'),
                controller : 'shangkeskorderbacklist',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
            
        }
    })
    //申请提现列表
    .state('app.shangkewithdrawalslist', {
        url: "/shangke/withdrawalslist.html",
        views: {
            'main@' : {
                template : require('../45shangke/views/withdrawalslist.html'),
                controller : 'shangkewithdrawalslist',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
            
        }
    })
    //资讯列表
    .state('app.shangkeinformationlist', {
        url: "/shangke/informationlist.html",
        views: {
            'main@' : {
                template : require('../45shangke/views/informationlist.html'),
                controller : 'shangkeinformationlist',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
            
        }
    })
    //添加资讯
    .state('app.addinformation', {
        url: "/shangke/addinformation.html/:id",
        views: {
            'main@' : {
                template : require('../45shangke/views/addinformation.html'),
                controller : 'addinformation',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
            getDate : function(utilservice){
		             return utilservice.getDate;
		    },
            date2str: function (utilservice) {
                    return utilservice.getDate;
            },
            str2date: function (utilservice) {
                return utilservice.str2date;
            },
            
        }
    })
    //热搜列表
    .state('app.shangkehotsearch', {
        url: "/shangke/hotsearch.html",
        views: {
            'main@' : {
                template : require('../45shangke/views/hotsearch.html'),
                controller : 'shangkehotsearch',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
        }
    })
    //轮播图列表
    .state('app.shangkenewsrollinglist', {
        url: "/shangke/newsrollinglist.html",
        views: {
            'main@' : {
                template : require('../45shangke/views/newsrollinglist.html'),
                controller : 'shangkenewsrollinglist',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
        }
    })
    

    //申请列表管理
    .state('app.skacountlist', {
        url: "/shangke/skacountlist",
        views: {
            'main@' : {
                template : require('../45shangke/views/skacountlist.html'),
                controller : 'skacountlist',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
            // userinfo : function(shangkeservice){
            //     return shangkeservice.userinfo();
            // }
        }
    })

    //添加分类
    .state('app.instructionlist', {
        url: "/shangke/instructionlist",
        views: {
            'main@' : {
                template : require('../45shangke/views/instructionlist.html'),
                controller : 'instructionlist',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
        }
    })

     // 添加说明
    .state('app.addinstruction', {
        url: '/addinstruction/:id/:title_identifier',
        controller : 'addinstruction',
        template: require('./views/addinstruction.html'),
        resolve:{
        findExplainList : function(shangkeservice){
                return shangkeservice.findExplainList();
        },
        saveExplain : function(shangkeservice){
                return shangkeservice.saveExplain();
        },
        updateDelIns : function(shangkeservice){
                return shangkeservice.updateDelIns();
        },
        getAdminExplain : function(shangkeservice){
            return shangkeservice.getAdminExplain();
        },
        updateExplain : function(shangkeservice){
                return shangkeservice.updateExplain();
        }

        }
    })
        //申请列表管理
    .state('app.headline', {
        url: "/shangke/headline",
        views: {
            'main@' : {
                template : require('../996tpl/views/table.html'),
                controller : 'headline',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
            // userinfo : function(shangkeservice){
            //     return shangkeservice.userinfo();
            // }
        }
    })
    // // 添加说明
    // .state('app.get2', {
    //     url: '/get2/:id/:title_identifier',
    //     controller : 'get2',
    //     template: require('./views/get2.html'),
    //     resolve:{
    //         userinfo : function(shangkeservice){
    //                 return shangkeservice.userinfo();
    //         }

    //     }
    // })

    //二级链接
    .state('app.shangkeget2', {
        url: "/shangke/get.html",
        views: {
            'main@' : {
                template : require('../45shangke/views/get2.html'),
                controller : 'shangkeget2',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
        }
    })
     //一元券销售品列表
    .state('app.shangkevouchersalelist', {
        url: "/shangke/vouchersalelist.html",
        views: {
            'main@' : {
                template : require('../45shangke/views/vouchersalelist.html'),
                controller : 'shangkevouchersalelist',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
            date2str: function (utilservice) {
                    return utilservice.getDate;
            },
            str2date: function (utilservice) {
                return utilservice.str2date;
            },

        }
    })
     //一元券订单列表
    .state('app.shangkevoucherorderlist', {
        url: "/shangke/voucherorderlist.html",
        views: {
            'main@' : {
                template : require('../45shangke/views/voucherorderlist.html'),
                controller : 'shangkevoucherorderlist',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
            getDate : function(utilservice){
		             return utilservice.getDate;
		    }
        }
    })
    //在线支付订单列表
    .state('app.shangkeusedorderlist', {
        url: "/shangke/usedorderlist.html",
        views: {
            'main@' : {
                template : require('../45shangke/views/usedorderlist.html'),
                controller : 'shangkeusedorderlist',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
        }
    })
    //积分商城商品列表
    .state('app.shangkeintegral', {
        url: "/shangke/integral.html",
        views: {
            'main@' : {
                template : require('../45shangke/views/integral.html'),
                controller : 'shangkeintegral',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
        }
    })
    //杀价帮活动列表
    .state('app.shangkebargainlist', {
        url: "/shangke/bargainlist/:id",
        views: {
            'main@' : {
                template : require('../45shangke/views/bargainlist.html'),
                controller : 'shangkebargainlist',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
            date2str: function (utilservice) {
                    return utilservice.date2str;
            },
            str2date: function (utilservice) {
                return utilservice.str2date;
            },
        }
    })
    // //杀价帮奖品列表
    // .state('app.shangkeprizelist', {
    //     url: "/shangke/bargainlist/:id",
    //     views: {
    //         'main@' : {
    //             template : require('../28shangke/views/bargainlist.html'),
    //             controller : 'shangkeprizelist',
    //         }
    //     },
    //     resolve:{
    //         tableconfig : function(tableservice){
    //             return tableservice.tableconfig();
    //         },
    //     }
    // })
    //供应商列表
    .state('app.shangkesupplierlist', {
        url: "/shangke/supplierlist.html",
        views: {
            'main@' : {
                template : require('../45shangke/views/supplierlist.html'),
                controller : 'shangkesupplierlist',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
        }
    })
    //一级权限申请列表
    .state('app.shangkeassignauthority', {
        url: "/shangke/assignauthority.html",
        views: {
            'main@' : {
                template : require('../45shangke/views/assignauthority.html'),
                controller : 'shangkeassignauthority',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
        }
    })
    //头条列表
    .state('app.shangkeheadline', {
        url: "/shangke/headline.html",
        views: {
            'main@' : {
                template : require('../45shangke/views/headline.html'),
                controller : 'shangkeheadline',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
        }
    })
    //看看赚列表
    .state('app.shangkeadlist', {
        url: "/shangke/adlist.html",
        views: {
            'main@' : {
                template : require('../45shangke/views/adlist.html'),
                controller : 'shangkeadlist',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
        }
    })

    //看看赚列表
    .state('app.prizelist', {
        url: "/shangke/prizelist.html/:id",
        views: {
            'main@' : {
                template : require('../45shangke/views/prizelist.html'),
                controller : 'prizelist',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
        }
    })
     //参与用户列表
    .state('app.bargainuser', {
        url: "/shangke/bargainuser.html/:id",
        views: {
            'main@' : {
                template : require('../45shangke/views/bargainuser.html'),
                controller : 'bargainuser',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
        }
    })
     //中奖用户列表
    .state('app.winuser', {
        url: "/shangke/winuser.html/:id",
        views: {
            'main@' : {
                template : require('../45shangke/views/winuser.html'),
                controller : 'winuser',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
        }
    })
    // //字典列表
    // .state('app.shangkedictionaryinfo', {
    //     url: "/shangke/dictionaryinfo.html",
    //     views: {
    //         'main@' : {
    //             template : require('../28shangke/views/dictionaryinfo.html'),
    //             controller : 'shangkedictionaryinfo',
    //         }
    //     },
    //     resolve:{
    //         tableconfig : function(tableservice){
    //             return tableservice.tableconfig();
    //         },
    //     }
    // })
    

    






};

module.exports = router;