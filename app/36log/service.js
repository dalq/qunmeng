/**
 * 子模块service
 * DHpai
 */
var service = function($resource, $q, $state, $modal){
    
    //接口的日志
    var logList = '/api/ac/sc/logService/getList';
    //数据库内集合
    var collName = '/api/ac/sc/logService/getCollNameList';
    //删除接口日志
    var delColl = '/api/ac/sc/logService/delLog';
    //构建记录
    var buildHistorys = '/api/ac/sc/buildHistoryService/findBuildHistorys';
    //部署记录
    var deployHistorys = '/api/ac/sc/buildHistoryService/findDeployHistorys';
    //修改构建内容
    var updateContent = '/api/uc/sc/buildHistoryService/addBuildHistory';
    

    return {
        logList : function(){
            return $resource(logList, {}, {});
        },
        collName : function(){
            return $resource(collName, {}, {});
        },
        delColl : function(){
            return $resource(delColl, {}, {});
        },
        buildHistorys : function(){
            return $resource(buildHistorys, {}, {});
        },
        deployHistorys : function(){
            return $resource(deployHistorys, {}, {});
        },
        updateContent : function(){
            return $resource(updateContent, {}, {});
        }
    };

};

module.exports = service;