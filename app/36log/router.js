 /**
 * 子模块路由
 * DHpai
 */

var router = function($urlRouterProvider, $stateProvider){


 	$stateProvider


    //接口日志列表
    .state('app.api_log', {
        url: "/log/loglist/:url",
        views: {
            'main@' : {
                template : require('./views/log.html'),
                controller : 'logList',
            }
        },
        resolve:{
            logList : function(logservice){
                return logservice.logList();
            },
            collName : function(logservice){
                return logservice.collName();
            },
            flag : function(){
                return true;
            }
        }
    })
    
    //接口名列表
    .state('app.api_name', {
        url: "/log/apiname",
        views: {
            'main@' : {
                template : require('./views/apiname.html'),
                controller : 'apiname',
            }
        },
        resolve:{
            collName : function(logservice){
                return logservice.collName();
            },
            delColl : function(logservice){
                return logservice.delColl();
            },
            flag : function(){
                return true;
            }
        }
    })

    //定时任务日志列表
    .state('app.task_log', {
        url: "/log/tasklog/:url",
        views: {
            'main@' : {
                template : require('./views/log.html'),
                controller : 'logList',
            }
        },
        resolve:{
            logList : function(logservice){
                return logservice.logList();
            },
            collName : function(logservice){
                return logservice.collName();
            },
            flag : function(){
                return false;
            }
        }
    })

    //定时任务名称列表
    .state('app.task_name', {
        url: "/log/taskname",
        views: {
            'main@' : {
                template : require('./views/apiname.html'),
                controller : 'apiname',
            }
        },
        resolve:{
            collName : function(logservice){
                return logservice.collName();
            },
            delColl : function(logservice){
                return logservice.delColl();
            },
            flag : function(){
                return false;
            }
        }
    })


    //build模块记录
    .state('app.build_history', {
        url: "/build/history",
        views: {
            'main@' : {
                template : require('./views/build_history.html'),
                controller : 'buildHistory',
            }
        },
        resolve:{
            buildHistorys : function(logservice){
                return logservice.buildHistorys();
            }
        }
    })

    //部署记录
    .state('app.deploy_history', {
        url: "/deploy/history",
        views: {
            'main@' : {
                template : require('./views/deploy_history.html'),
                controller : 'deployHistory',
            }
        },
        resolve:{
            deployHistorys : function(logservice){
                return logservice.deployHistorys();
            }
        }
    })

    //事件列表
    .state('app.event_list', {
        url: "/event/list",
        views: {
            'main@' : {
                template : require('./views/event.html'),
                controller : 'event',
            }
        },
        resolve:{
            // buildHistorys : function(logservice){
            //     return logservice.buildHistorys();
            // }
        }
    })

    //对接日志
    .state('app.docking_log', {
        url: "/docking/log",
        views: {
            'main@' : {
                template : require('./views/docking.html'),
                controller : 'docking',
            }
        },
        resolve:{
        }
    })
    
    ;

};

module.exports = router;