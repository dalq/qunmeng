var App = angular.module('log', []);

App.config(require('./router'));

//service
App.factory('logservice', require('./service'));

//Controllers
App.controller('logList', require('./controllers/loglist'));
App.controller('info', require('./controllers/info'));
App.controller('apiname', require('./controllers/apiname'));
App.controller('buildHistory', require('./controllers/build_history'));
App.controller('deployHistory', require('./controllers/deploy_history'));
App.controller('event', require('./controllers/event'));
App.controller('taskParam', require('./controllers/task_param'));
App.controller('docking', require('./controllers/docking'));



module.exports = App;