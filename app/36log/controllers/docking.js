module.exports = function ($scope, $state, $modal, $resource) {
    $scope.searchform = {};
    $scope.date = {
        'lable': date2str(new Date()),
        'value': date2str(new Date()),
        'opened': false
    }
    $scope.dateOpen = function ($event, item) {
        $event.preventDefault();
        $event.stopPropagation();
        item.opened = true;
    };
    $scope.date1 = {
        'lable': date2str(new Date()),
        'value': date2str(new Date()),
        'opened': false
    }
    $scope.dateOpen1 = function ($event, item) {
        $event.preventDefault();
        $event.stopPropagation();
        item.opened = true;
    };
    function date2str(d) {
        if (d === undefined) {
            return "";
        }
        var month = (d.getMonth() + 1).toString();
        var day = d.getDate().toString();
        if (month.length == 1) month = '0' + month;
        if (day.length == 1) day = '0' + day;
        return d.getFullYear() + "-" + month + "-" + day;
    }

    /* 分页
     * ========================================= */
    $scope.maxSize = 5;            //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10;         //每页显示几条
    $scope.sale_belong_arr = [];
    $scope.function_code_arr = [];
    $scope.change = function () {
        if (!$scope.searchform.sale_belong) {
            return false;
        }
        $resource('/api/uc/dc/dockingLogService/findSaleBelongByFunctionCode', {}, {}).save({ 'sale_belong': $scope.searchform.sale_belong }, function (res) {
            if (res.errcode === 0) {
                $scope.function_code_arr = res.data;
            } else {
                alert(res.errmsg);
            }
        });
    }
    $scope.myKeyup = function (e) {
        //IE 编码包含在window.event.keyCode中，Firefox或Safari 包含在event.which中
        var keycode = window.event ? e.keyCode : e.which;
        if (keycode == 13) {
            $scope.load();
        }
    };
    $scope.load = function () {
        if ($scope.date.lable) {
            if (typeof $scope.date.lable === 'string') {
                $scope.tour_date = $scope.date.lable
            } else {
                $scope.tour_date = date2str($scope.date.lable);
            }
        } else {
            $scope.tour_date = ''
        }
        if ($scope.date1.lable) {
            if (typeof $scope.date1.lable === 'string') {
                $scope.tour_date_two = $scope.date1.lable
            } else {
                $scope.tour_date_two = date2str($scope.date1.lable);
            }
        } else {
            $scope.tour_date_two = ''
        }
        var para = {
            pageNo: $scope.bigCurrentPage,
            pageSize: $scope.itemsPerPage,
            start_time: $scope.tour_date,
            end_time: $scope.tour_date_two
        };
        para = angular.extend($scope.searchform, para);
        $resource('/api/uc/dc/dockingLogService/findDockingLog', {}, {}).save(para, function (res) {
            if (res.errcode === 0) {
                $scope.bigTotalItems = res.data.totalRecord;
                $scope.logList = res.data.results;
            } else {
                alert(res.errmsg);
            }
        });
    };
    $scope.load();
    $resource('/api/as/sc/dict/dictbytypelist', {}, {}).save({ 'type': 'ticket_sale_belong' }, function (res) {
        if (res.errcode === 0) {
            $scope.sale_belong_arr = res.data;
        } else {
            alert(res.errmsg);
        }
    });
};