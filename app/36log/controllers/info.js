module.exports = function($scope, $modalInstance, item, flag){

    if(item.x_result){
        var reg = /^[\u4E00-\u9FA5]+/;
        if (!reg.test(item.x_result)){
            var x_result = JSON.parse(item.x_result);
            item.x_result = JSON.stringify(x_result,null,2);
        }
    }
    if(item.header){
        var header = JSON.parse(item.header);
        item.header = JSON.stringify(header,null,2);
    }

	$scope.flag = flag;
	$scope.obj = item;

	//关闭
    $scope.close = function () {
        $modalInstance.dismiss('cancel');
    };

	
};