module.exports = function($scope, $resource, $modal){

    $scope.status = {'event': true, 'task': false};

    init();
	//初始化事件列表
	function init(enviro){
		$resource('/api/ac/sc/eventTaskService/eventList', {}, {}).save({}, function (res) {
            if (res.errcode === 0) {
                $scope.event_list = res.data;
            } else {
                alert(res.errmsg);
            }
        });
    }

    //查看事件绑定的任务列表
    $scope.seeInfo = function(item){
        $scope.task_list = [];
        $scope.status.task = true;
        $scope.status.taskid = item.id;
        if(item.execSum != 0){
            $resource('/api/ac/sc/eventTaskService/taskList', {}, {}).save({'event_id': item.id}, function (res){
                if (res.errcode === 0) {
                    $scope.task_list = res.data;
                    angular.forEach($scope.task_list, function(item){
                        item.date = new Date(parseInt(item.createTime)).toLocaleString();
                        item.errorCount = item.errorCount || '0';
                    });
                } else {
                    alert(res.errmsg);
                }
            });
        }
    };

    //搜索
    $scope.search = function(){
        var item = {
            'id': $scope.status.taskid,
            'execSum': 1
        };
		$scope.seeInfo(item);
    };
    
    //取消=重置
    $scope.cancel = function(){
		$scope.status.event = true;
    };

    //取消=重置
    $scope.taskinfo = function(item){
        var modalInstance = $modal.open({
			template: require('../views/task_param.html'),
			controller: 'taskParam',
			size: 'lg',
			resolve: {
				item: function () {
					return item;
				}
			}
		});
		//关闭模态框刷新页面
		modalInstance.result.then(function() {
  			
		});
    };

	
};