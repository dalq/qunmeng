module.exports = function($scope, $modal, $state, $stateParams, collName, delColl, flag){

	init();
	//初始化日志列表
	function init(enviro){
		$scope.enviro = flag?'undb':'taskdb';
		collName.save({'dbname' : $scope.enviro}, function(res){
			if (res.errcode === 0) {
				$scope.apiname_list = res.data;
			} else {
				alert(res.errmsg);
			}
		});
	}

	//接口日志的详细信息
	$scope.watchLog = function (item) {
		var router = flag?'app.api_log':'app.task_log';
		$state.go(router, {'url': item});
	}

	//删除日志集合
	$scope.delLog = function (index) {
		if(confirm('确认要删除日志吗？')==true){
			var para = {
				'dbname' : $scope.enviro,
				'collname' : $scope.apiname_list[index]
			}
			delColl.save(para, function(res){
				if (res.errcode === 0) {
					$scope.apiname_list.splice(index,1);
				} else {
					alert(res.errmsg);
				}
			});
		}
	}

};