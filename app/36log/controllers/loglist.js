module.exports = function($scope, $modal, $state, $interval, $stateParams, flag, logList, collName){

	$scope.maxSize = '5';			//最多显示多少个按钮
    $scope.currentPage = '1';		//当前页码
    $scope.itemsPerPage = '10';		//每页显示几条
	$scope.searchData = {};
	$scope.flag = flag;
	$scope.enviro = flag?'undb':'taskdb';
	$scope.searchData.url = $stateParams.url;
	
	//查询数据
	$scope.getData = function(fresh) {
		var para = {
			'dbname' : $scope.enviro,
			'collname' : $scope.searchData.url,
			'id' : $scope.searchData.id,
            'pageNo' : $scope.currentPage,
            'pageSize' : $scope.itemsPerPage
        };
		if(fresh){
			para.id = '';
			$scope.currentPage = '1';
		}
		logList.save(para, function(res){
			if (res.errcode === 0) {
				$scope.log_list = res.data.results;
				$scope.totalItems = res.data.totalRecord;
				angular.forEach($scope.log_list, function(temp){
					temp.date = new Date(parseInt(temp.timestamp.$date)).toLocaleString();
					temp.spend_c = parseInt(temp.spend.$numberLong);
					if(temp.y_parent && !angular.isString(temp.y_parent)){
						var y_parent = JSON.parse(temp.y_parent);
						temp.y_parent = JSON.stringify(y_parent,null,2);
					}
					if (temp.params){
						if(temp.params.indexOf('[') == 0){
							temp.params_list = temp.params;
						} else {
							var params = JSON.parse(temp.params);
							var params_list = {};
							for(x in params){
								if(x == '_user' || x == '_system'){
									// console.log(params[x]);
								} else {
									params_list[x] = params[x];
								}
							}
							temp.params_list = params_list;
							temp.params = JSON.stringify(params,null,2);
						}
					}
				});
			} else {
				// alert(res.errmsg);
				console.log(res.errmsg);
			}
		});
	}

	//搜索按钮
	$scope.search = function () {
		$scope.time = false;
		$interval.cancel($scope.timer);
		$scope.getData();
	}

	init();
	//初始化页面
	function init(){
		collName.save({'dbname' : $scope.enviro}, function(res){
			if (res.errcode === 0) {
				$scope.apiname_list = res.data;
			} else {
				alert(res.errmsg);
			}
		});
		$scope.getData();
	}

	//接口日志的详细信息
	$scope.getInfo = function (item) {
		var modalInstance = $modal.open({
			template: require('../views/info.html'),
			controller: 'info',
			size: 'lg',
			resolve: {
				item: function () {
					return item;
				},
				flag: function () {
					return flag;
				}
			}
		});
		//关闭模态框刷新页面
		modalInstance.result.then(function() {
  			
		});
	}

	//自动刷新
	$scope.refresh = function () {
		if($scope.time){
			$interval.cancel($scope.timer);
		} else {
			$scope.timeout = 5;
			$scope.timer = $interval(function(){
				$scope.timeout -= 1;
				if($scope.timeout < 0){
					$scope.getData(true);
					$scope.timeout = 5;
				}
			},1000);
		}
	}

	//后退
	$scope.back = function () {
		var router = flag?'app.api_name':'app.task_name';
		$state.go(router, {});
	}

	$scope.$on('$destroy',function(){
		$interval.cancel($scope.timer);
	});

};