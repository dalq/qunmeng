module.exports = function($scope, $modal, buildHistorys){

	$scope.maxSize = '5';			//最多显示多少个按钮
    $scope.currentPage = '1';		//当前页码
	$scope.itemsPerPage = '10';		//每页显示几条
	


	init();
	//初始化日志列表
	function init(){
		buildHistorys.save({}, function(res){
			if (res.errcode === 0) {
				$scope.build_historys = res.data.results;
				$scope.totalItems = res.data.totalRecord;
			} else {
				alert(res.errmsg);
			}
		});
	}

	//查找
	$scope.search = function (item) {
		var para = {
			'pageNo' : $scope.currentPage,
            'pageSize' : $scope.itemsPerPage,
			'app_name': $scope.search.app_name,
			'build_user': $scope.search.build_user
		};
		buildHistorys.save(para, function(res){
			if (res.errcode === 0) {
				$scope.build_historys = res.data.results;
				$scope.totalItems = res.data.totalRecord;
			} else {
				alert(res.errmsg);
			}
		});
	}



};