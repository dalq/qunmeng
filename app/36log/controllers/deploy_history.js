module.exports = function($scope, $modal, deployHistorys){

	$scope.maxSize = 5;				//最多显示多少个按钮
    $scope.currentPage = 1;			//当前页码
	$scope.itemsPerPage = 10;		//每页显示几条
	


	init();
	//初始化日志列表
	function init(){
		deployHistorys.save({}, function(res){
			if (res.errcode === 0) {
				$scope.deploy_historys = res.data.results;
				$scope.totalItems = res.data.totalRecord;
			} else {
				alert(res.errmsg);
			}
		});
	}

	//查找
	$scope.search = function (item) {
		var para = {
			'pageNo' : $scope.currentPage,
            'pageSize' : $scope.itemsPerPage,
			'app_name': $scope.search.app_name,
			'user': $scope.search.user
		};
		deployHistorys.save(para, function(res){
			if (res.errcode === 0) {
				$scope.deploy_historys = res.data.results;
				$scope.totalItems = res.data.totalRecord;
			} else {
				alert(res.errmsg);
			}
		});
	}

	//查看内容信息
	// $scope.seeInfo = function (item) {
	// 	var modalInstance = $modal.open({
	// 		template: require('../views/build_content.html'),
	// 		controller: 'buildContent',
	// 		size: 'lg',
	// 		resolve: {
	// 			item: function () {
	// 				return item;
	// 			},
	// 			updateContent : function(logservice){
	// 				return logservice.updateContent();
	// 			}
	// 		}
	// 	});
	// 	modalInstance.result.then(function() {
  	// 		$scope.search();
	// 	});
	// }



};