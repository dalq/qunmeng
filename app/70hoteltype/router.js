/**
* 子模块路由
* dlq
*/

var router = function ($urlRouterProvider, $stateProvider) {

    $stateProvider

            //待审核产品列表
            .state('app.hotelApplyProductList', {
                url: '/hotel/hotelApplyProductList',
                views: {
                    'main@': {
                        template: require('./views/hotelApplyProductList.html'),
                        controller: 'hotelApplyProductList',
                    }
                },
                resolve: {
                    date2str: function (utilservice) {
                        return utilservice.getDate;
                    },
                    str2date: function (utilservice) {
                        return utilservice.str2date;
                    },
                    saleupdate: function (productservice) {
                        return productservice.saleupdate();
                    },
                    saleup: function (productservice) {
                        return productservice.saleup();
                    },
                    saledown: function (productservice) {
                        return productservice.saledown();
                    },
                    talist: function (depositservice) {
                        return depositservice.talist;
                    },
    
                    sellerListno: function (productservice) {
                        return productservice.sellerListno();
                    },
                    tstcreateno: function (productservice) {
                        return productservice.tstcreateno();
                    },
                    tststartno: function (productservice) {
                        return productservice.tststartno();
                    },
                    tststopno: function (productservice) {
                        return productservice.tststopno();
                    },
    
    
                }
            })
                //酒店产品列表
                .state('app.hotelSupplyProductList', {
                    url: "/hotel/hotelSupplyList",
                    views: {
                        'main@': {
                            template: require('./views/hotelSupplyProductList.html'),
                            controller: 'hotelSupplyProductList',
                        }
                    },
                    resolve: {
                        date2str: function (utilservice) {
                            return utilservice.getDate;
                        },
                        str2date: function (utilservice) {
                            return utilservice.str2date;
                        },
                        saleupdate: function (productservice) {
                            return productservice.saleupdate();
                        },
                        saleup: function (productservice) {
                            return productservice.saleup();
                        },
                        saledown: function (productservice) {
                            return productservice.saledown();
                        },
                        talist: function (productservice) {
                            return productservice.talist;
                        },
                        sellerListno: function (productservice) {
                            return productservice.sellerListno();
                        },
                        sellerList: function (productservice) {
                            return productservice.sellerList();
                        },
                        tstcreateno: function (productservice) {
                            return productservice.tstcreateno();
                        },
                        tstcreate: function (productservice) {
                            return productservice.tstcreate();
                        },
                        tststartno: function (productservice) {
                            return productservice.tststartno();
                        },
                        tststart: function (productservice) {
                            return productservice.tststart();
                        },
                        tststopno: function (productservice) {
                            return productservice.tststopno();
                        },
                        tststop: function (productservice) {
                            return productservice.tststop();
                        }
                    }
                })

        //票种列表
        .state('app.hoteltypeList', {
            url: "/hoteltype/hoteltypeList:placeid",
            views: {
                'main@': {
                    template: require('./views/hoteltypeList.html'),
                    controller: 'hoteltypeList',
                }
            },
            resolve: {
                viewlist: function (tickettypeservice) {
                    return tickettypeservice.viewlist;
                },
                typepagelist: function (tickettypeservice) {
                    return tickettypeservice.typepagelist();
                },
                tktlist: function (tickettypeservice) {
                    return tickettypeservice.tktlist();
                },
                tktupdate: function (tickettypeservice) {
                    return tickettypeservice.tktupdate();
                },
                updateTicketPeriod: function (tickettypeservice) {
                    return tickettypeservice.updateTicketPeriod();
                },
                str2date: function (utilservice) {
                    return utilservice.str2date;
                }
            }
        })

        //创建票种
        .state('app.hoteltypeCreate', {
            url: "/hoteltype/hoteltypeCreate",
            views: {
                'main@': {
                    template: require('./views/hoteltypeCreate.html'),
                    controller: 'hoteltypeCreate',
                }
            },
            resolve: {
                viewlist: function (tickettypeservice) {
					return tickettypeservice.slist;
				},
				tktcreate: function (tickettypeservice) {
					return tickettypeservice.tktcreate();
				},
				placeinfo: function (tickettypeservice) {
					return tickettypeservice.info();
				},
				makeArr: function (tickettypeservice) {
					return tickettypeservice.makeArr;
				},
				makeStr: function (tickettypeservice) {
					return tickettypeservice.makeStr;
				},
				getDate: function (utilservice) {
					return utilservice.getDate;
				},
				str2date: function (utilservice) {
					return utilservice.str2date;
				}
            }
        })

        //票种详情
        // .state('app.hoteltypeInfo', {
        //     url: "/hoteltype/hoteltypeInfo/:id",
        //     views: {
        //         'main@': {
        //             template: require('./views/tickettypeInfo.html'),
        //             controller: 'tickettypeInfo',
        //         }
        //     },
        //     resolve: {
                
        //     }
        // })

        //票种编辑
        .state('app.hoteltypeEdit', {
            url: "/hoteltype/hoteltypeEdit/:id/:placeid/:hotel_info_json",
            views: {
                'main@': {
                    template: require('./views/hoteltypeEdit.html'),
                    controller: 'hoteltypeEdit',
                }
            },
            resolve: {
                tktinfo: function (tickettypeservice) {
					return tickettypeservice.tktinfo();
				},
				tktupdate: function (tickettypeservice) {
					return tickettypeservice.tktupdate();
				},
				viewlist: function (tickettypeservice) {
					return tickettypeservice.viewlist;
				},
				placeinfo: function (tickettypeservice) {
					return tickettypeservice.info();
				},
				makeArr: function (tickettypeservice) {
					return tickettypeservice.makeArr;
				},
				makeStr: function (tickettypeservice) {
					return tickettypeservice.makeStr;
				},
				getDate: function (utilservice) {
					return utilservice.getDate;
				},
				str2date: function (utilservice) {
					return utilservice.str2date;
				}
            }
        })

        //票种编辑
        .state('app.hoteltypeattr', {
            url: "/hoteltype/hoteltypeattr",
            views: {
                'main@': {
                    template: require('./views/tkttypeattr.html'),
                    controller: 'ttkttypeattr',
                }
            },
            resolve: {
            }
        })

        //票种编辑
        .state('app.thoteltypeattrcreate', {
            url: "/hoteltype/thoteltypeattrcreate",
            views: {
                'main@': {
                    template: require('./views/ttkttypeattrcreate.html'),
                    controller: 'ttkttypeattrcreate',
                }
            },
            resolve: {
            }
        })

        //票种编辑
        .state('app.thoteltypeattredit', {
            url: "/hoteltype/thoteltypeattredit/:type_attr",
            views: {
                'main@': {
                    template: require('./views/ttkttypeattredit.html'),
                    controller: 'ttkttypeattredit',
                }
            },
            resolve: {
            }
        })

         //酒店列表
         .state('app.hotellist', {
            url: "/hotel/hotellist",
            views: {
                'main@': {
                    template: require('./views/hotellist.html'),
                    controller: 'hotellist',
                }
            },
            resolve: {
                date2str: function (utilservice) {
                    return utilservice.date2str;
                },
                str2date: function (utilservice) {
                    return utilservice.str2date;
                },
            }
        })
        // 创建酒店
        .state('app.creatHotel', {
            url: "/hotel/creatHotel/:id",
            views: {
                'main@': {
                    template: require('./views/creatHotel.html'),
                    controller: 'creatHotel',
                }
            },
            resolve: {
                date2str: function (utilservice) {
                    return utilservice.date2str;
                },
                str2date: function (utilservice) {
                    return utilservice.str2date;
                },
            }
        })
        // 酒店订单列表
        .state('app.hotelOrderList', {
            url: "/hotel/hotelOrderList/:id",
            views: {
                'main@': {
                    template: require('./views/hotelOrderList.html'),
                    controller: 'hotelOrderList',
                }
            },
            resolve: {
                date2str: function (utilservice) {
                    return utilservice.date2str;
                },
                str2date: function (utilservice) {
                    return utilservice.str2date;
                },
                DateDiff : function(utilservice) {
                    return utilservice.DateDiff;
                }
            }
        })




        // //选择票种
        // .state('app.tickettypeChoice', {
        //     url: "/tickettype/tickettypeChoice",
        //     template: require('./views/tickettypeChoice.html'),
        //     controller: 'tickettypechoice',
        //     resolve: {

        //     }
        // })



        // //弹出票种详情
        // .state('app.tickettype_info1', {
        //     url: "/tickettype/info1/:id",
        //     template : require('../996tpl/views/popform.html'),
        //     controller : 'poptickettypeinfo',
        //     resolve:{
        //         formconfig : function(formservice){
        //             return formservice.formconfig();
        //         },
        //         model : function(tickettypeservice){
        //             return tickettypeservice.model;
        //         }
        //     }
        // })



        ;

};

module.exports = router;