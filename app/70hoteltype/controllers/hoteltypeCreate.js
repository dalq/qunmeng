module.exports = function ($scope, viewlist, tktcreate, getDate, $stateParams,FileUploader,
	placeinfo, $state, makeArr, makeStr) {

	var placeid = $stateParams.placeid;
	//  alert(placeid);
	$scope.placeid = $stateParams.placeid;
	$scope.id = $stateParams.placeid;
	$scope.searchform = {
		'selected': {
			'name': ''
		}
	}
	$scope.objt = {
		'name': '',
		'placename': '',
		'place_code': '',
		'used_state': '0',
		'state': '1',
		'book_info': '',
		'img': ''
	};

	$scope.midstart = new Date();
	$scope.midend = new Date();

	//有效区间
	$scope.section = {};
	$scope.section.start = {};
	$scope.section.start.date = {};

	$scope.section.end = {};
	$scope.section.end.date = {};

	$scope.today = function () {
		$scope.section.start.date = $scope.section.end.date = new Date();
	};
	$scope.today();
	$scope.open = function (obj) {
		obj.opened = true;
	};

	$scope.objs = [];

	// $scope.change = function (code) {
	// 	$scope.objt.place_code = code;
	// }

	viewlist({type:'H'}).then(function (res) {
		if (res.errcode === 0) {
			$scope.viewarr = res.data;
			var array = res.data;
			for (var i = 0; i < array.length; i++) {
				if (placeid == array[i].code) {
					$scope.objt.place_code = array[i].code;
					$scope.searchform.selected.name = array[i].name;
				}
			}
		}
		else {
			alert(res.errmsg);
		}
	});


	//保存按钮
	$scope.gogo = function () {
		
		$scope.hotel_info_json = {
			max_adult:$scope.max_adult,
			max_child:$scope.max_child,
			area:$scope.area,
			floor:$scope.floor,
			room_num:$scope.room_num,
			add_bed:$scope.add_bed,
			add_bed_price:$scope.add_bed_price,
			room_style:$scope.room_style,
			smoke_room:$scope.smoke_room,
			window:$scope.window,
			describe:$scope.describe,
			bed_else:$scope.bed_else,
			web_else:$scope.web_else,
			web:$scope.web

		}
		$scope.hotel_info_json  = angular.toJson($scope.hotel_info_json );


		if (!check()) return;
		var para = {
			'print_setup': makeStr($scope.objs)
		}
		para.hotel_info_json =$scope. hotel_info_json;
		para = angular.extend($scope.objt, para);
		para.start_date = getDate($scope.midstart);
		para.end_date = getDate($scope.midend);
		if (parseInt(para.start_date.substring(0, 4) + para.start_date.substring(5, 7) + para.start_date.substring(8, 10)) > parseInt(para.end_date.substring(0, 4) + para.end_date.substring(5, 7) + para.end_date.substring(8, 10))) {
			alert('有效时间的初始日期应早于末尾日期');
			return;
		}
		tktcreate.save(para, function (res) {

			if (res.errcode === 0) {
				alert('保存成功');
				$state.go('app.hoteltypeList');
				// $state.go('app.edittkttype', {'id' : res.data.uuid});
			}
			else {
				alert(res.errmsg);
			}

		});

	};

	function check() {

		if ($scope.objt.name === '') {
			alert('请输入酒店房型名称');
			return false;
		}

		if ($scope.objt.code === '') {
			alert('请输入票种编号');
			return false;
		}

		return true;
	}



	$scope.printadd = function () {
		var obj = {
			'name': ''
		};
		$scope.objs.push(obj);
	};

	$scope.printdel = function (index) {
		$scope.objs.splice(index, 1);
	};

	var uploader1 = $scope.uploader1 = new FileUploader({
		url: 'http://cl.juyouhx.com/oss.php/oss/webuploader1?topdir=aa&selfdir=bb'
	});
	uploader1.filters.push({
		name: 'imageFilter',
		fn: function (item /*{File|FileLikeObject}*/, options) {
			var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
			return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
		}
	});
	uploader1.onSuccessItem = function (fileItem, response, status, headers) {
		$scope.objt.img = response.savename;
	};

	$scope.uploader1 = uploader1;


};
