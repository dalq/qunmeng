module.exports = function ($scope, $state, $resource, toaster, $modal, date2str, str2date, DateDiff) {
    $scope.vm = {
        'date' : '',
        'options' : {
            format: "YYYY-MM-DD",
            locale : 'zh-cn',
            showClear: true                        
            
        }
    }
    $scope.vm1 = {
        'date' : '',
        'options' : {
            format: "YYYY-MM-DD",
            locale : 'zh-cn',
            showClear: true                        
            
        }
    }
    $scope.searchform = {};
     /* 分页
     * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10         //每页显示几条
    $scope.searchform = {
        'place_name' : ''
    };

    $scope.addview = {'name' : '-----全部-----', 'value' : ''};
    
    // 酒店列表
    $resource('/api/as/tc/salehotel/findHotelNameList', {}, {}).
    save({},function (res) {
        if (res.errcode != 0) {
            toaster.success({title:"",body:res.errmsg});
            return;
        }
        console.log(res);
        $scope.hotelarr = res.data;
        $scope.hotelarr.splice(0,0,$scope.addview);
        
        
    });  
    // 订单状态
    $scope.cancelstatenamearr = [{'value' : '', 'name' : '-----全部----'},{'value' : '0', 'name' : '正常'}, {'value' : '1', 'name' : '取消'}];
    $scope.getlist = function () {
        if ( $scope.vm.date === null) {
            $scope.vm.date = '';
        }
        if ( $scope.vm1.date === null) {
            $scope.vm1.date = '';
        }
        if (typeof $scope.vm.date._d === 'string') {
            $scope.vm.date._d = $scope.vm.date._d;
        } else {
            $scope.vm.date._d = date2str($scope.vm.date._d);
        }
        if (typeof $scope.vm1.date._d === 'string') {
            $scope.vm1.date._d = $scope.vm1.date._d;
        } else {
            $scope.vm1.date._d= date2str($scope.vm1.date._d);
        }
        
        var para = {
            pageNo:$scope.bigCurrentPage, 
            pageSize:$scope.itemsPerPage,
            start_time : ($scope.vm.date._d),
            end_time : ($scope.vm1.date._d)
        };
        para = angular.extend($scope.searchform,para);
        $resource('/api/ac/tc/hotelOrderService/findHotelOrderList', {}, {}).
        save(para,function (res) {
            if (res.errcode != 0) {
                toaster.success({title:"",body:res.errmsg});
                return;
            }
            console.log(res);
            $scope.objs = res.data.results;
            $scope.bigTotalItems = res.data.totalRecord;
        });   
    };
    $scope.getlist();
    $scope.orderdetail = function(item){
        $scope.item = item;
        console.log('产看详情');
        var modalInstance = $modal.open({
            template: require('../views/hotelOrderInfo.html'),
            controller: 'hotelOrderInfo',
            size: 'lg',
            resolve: {
                item: function () {
                    return $scope.item;
                },
                DateDiff : function () {
                    return DateDiff;
                }
                
            }
        });
        modalInstance.result.then(function (showResult) {	
                // $scope.pageChanged();
        });
    }
    $scope.changehotel = function(placename){
        if(placename === '-----全部-----'){
            $scope.searchform.place_name = '';
            // $scope.getlist();
        }
    }
    
    
}    