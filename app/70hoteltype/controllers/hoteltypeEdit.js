module.exports = function ($scope, $stateParams, viewlist, $state, tktinfo,FileUploader,
	tktupdate, placeinfo, makeArr, makeStr, getDate, str2date) {
	var placeid = $stateParams.placeid;
	$scope.placeid = '';
	$scope.id = $stateParams.id;
	// $scope.hotel_info_json = $stateParams.hotel_info_json;
	

	//有效区间
	// $scope.section = {};
	// $scope.section.start = {};
	// $scope.section.start.date = {};

	// $scope.section.end = {};
	// $scope.section.end.date = {};

	// $scope.today = function() {
	// 	$scope.section.start.date = $scope.section.end.date = new Date();
	// };
	// $scope.today();
	// $scope.open = function(obj) {
	// 	obj.opened = true;
	// };

	$scope.objs = [];
	//$scope.midstart = new Date();
	//$scope.midend = new Date();

	$scope.flag = true;

	$scope.objt = {
		'name': '',
		//	'code' : '',
		'place_code': '',	
		'used_state': '0',
		'state': '1',
		'book_info': '',
		'img': ''
	};



	tktinfo.get({ 'id': $stateParams.id }, function (res) {

		if (res.errcode === 0) {		
			$scope.objt = res.data;
			console.log(res.data);
			console.log('上面是res.data');		
			$scope.objt.place_code = res.data.place_code;
			//$scope.midstart = str2date( $scope.objt.start_date);
			//$scope.midend = str2date($scope.objt.end_date);
			$scope.objs = makeArr(res.data.print_setup);
			//getplaceinfo();
				$scope.hotel_info_json = angular.fromJson($scope.objt.hotel_info_json);
				$scope.max_adult = $scope.hotel_info_json.max_adult;
				$scope.max_child = $scope.hotel_info_json.max_child;
				$scope.area = $scope.hotel_info_json.area;
				$scope.floor = $scope.hotel_info_json.floor;
				$scope.room_num = $scope.hotel_info_json.room_num;
				$scope.add_bed = $scope.hotel_info_json.add_bed;
				$scope.add_bed_price = $scope.hotel_info_json.add_bed_price;
				$scope.room_style = $scope.hotel_info_json.room_style;
				$scope.smoke_room = $scope.hotel_info_json.smoke_room;
				$scope.window = $scope.hotel_info_json.window;
				$scope.describe = $scope.hotel_info_json.describe;
				$scope.bed_else = $scope.hotel_info_json.bed_else;
				$scope.web_else = $scope.hotel_info_json.web_else;
				$scope.web = $scope.hotel_info_json.web;
			
		}

	});

	//景区下拉
	viewlist({type:'H'}).then(function (res) {

		if (res.errcode === 0) {
			$scope.viewarr = res.data;
			console.log($scope.viewarr)
			//$scope.objt.place_code = res.data[0].code;
		}
		else {
			alert(res.errmsg);
		}
	});

	// function getplaceinfo(){
	// 	placeinfo.get({'id' : $scope.placeid}, function(res){


	//     	if(res.errcode === 0)
	//         {
	//         	$scope.objt.placename = res.data.name;
	//         	$scope.objt.placecode = res.data.code;
	//         }
	//         else
	//         {
	//             alert(res.errmsg);
	//         }

	//     });
	// }

	$scope.gogo = function () {

		$scope.hotel_info_json = {
			max_adult:$scope.max_adult,
			max_child:$scope.max_child,
			area:$scope.area,
			floor:$scope.floor,
			room_num:$scope.room_num,
			add_bed:$scope.add_bed,
			add_bed_price:$scope.add_bed_price,
			room_style:$scope.room_style,
			smoke_room:$scope.smoke_room,
			window:$scope.window,
			describe:$scope.describe,
			bed_else:$scope.bed_else,
			web_else:$scope.web_else,
			web:$scope.web
		}
		$scope.hotel_info_json  = angular.toJson($scope.hotel_info_json );

		$scope.objt['print_setup'] = makeStr($scope.objs);
		$scope.objt.hotel_info_json = $scope.hotel_info_json;
		console.log($scope.objt);
		tktupdate.save($scope.objt, function (res) {
			if (res.errcode === 0) {
				alert('修改成功');
				// $state.go('app.tkttype',{'placeid' : placeid});
				// $state.go('app.tickettypeEdit', { 'placeid': placeid });

			}
			else {
				alert(res.errmsg);
			}
		});
	}


	// $scope.objs = [
	// 	{
	// 		'name' : '电瓶车'
	// 	},
	// 	{
	// 		'name' : '博物馆'
	// 	},
	// 	{
	// 		'name' : '门票'
	// 	}
	// ];

	$scope.printadd = function () {
		var obj = {
			'name': ''
		};
		$scope.objs.push(obj);
	};

	$scope.printdel = function (index) {
		$scope.objs.splice(index, 1);
	};


	// function makeArr(str){
	// 	var obj = [];

	// 	if(str === undefined || str.length === 0) return obj;

	// 	var arr = str.split(',');
	// 	for(var i = 0; i < arr.length; i++)
	// 	{
	// 		obj.push({'name' : arr[i]});
	// 	}
	// 	return obj;
	// }

	// function makeStr(arr){

	// 	if(!angular.isArray(arr)) return '';

	// 	var arr1 = [];
	// 	for(var i = 0; i < arr.length; i++)
	// 	{
	// 		arr1.push(arr[i].name);
	// 	}
	// 	return arr1.join(',');
	// }
	
	var uploader1 = $scope.uploader1 = new FileUploader({
		url: 'http://cl.juyouhx.com/oss.php/oss/webuploader1?topdir=aa&selfdir=bb'
	});
	uploader1.filters.push({
		name: 'imageFilter',
		fn: function (item /*{File|FileLikeObject}*/, options) {
			var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
			return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
		}
	});
	uploader1.onSuccessItem = function (fileItem, response, status, headers) {
		$scope.objt.img = response.savename;
	};

	$scope.uploader1 = uploader1;

};
