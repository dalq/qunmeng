module.exports = function ($scope, $state, $resource, toaster, date2str, str2date, $stateParams, $http, $q) {
    var id = $stateParams.id;
    console.log(id);
    $scope.model = {
        'startTime' : '',
        'endTime' : ''
    }
    $scope.timeinfo = {
        'startTime' : '',
        'endTime' : ''
    }

    $scope.changestart = function(startTime){
        console.log(startTime);
        $scope.hotelinfo.earliest_arrival_time = time2str(startTime);
        console.log(time2str(startTime));
    }
    $scope.changeend = function(endTime){
        $scope.hotelinfo.latest_departure_time = time2str(endTime);
    }
    $scope.hotelinfo = {
        'type' : 'H',
        'facilities' : '',
        'unionPay' : '',
        'breakfast_type' : '',
        'foreign_credit_card' : '',
        'opening_time' : '',
        'recently_renovated' : '',
        'earliest_arrival_time' : '',
        'latest_departure_time' : '',
        'pay_by_card' : '',
        'id' : id
    };
    
    // 早餐类型
    $scope.breakfirsttype = [
        {
            'label': '中餐',
            'value1': '0',
        },
        {
            'label': '西式早餐',
            'value1': '1',
        },
        {
            'label': '中西式自助餐',
            'value1': '2',
        },
        {
            'label': '早餐',
            'value1': '3',
        },
    ]
    // 卡类型
    $scope.cardtype = [
        {
            'label': '长城卡',
            'value1': '0',
            'checked': false,
        },
        {
            'label': '龙卡',
            'value1': '1',
            'checked': false,
            
        },
        {
            'label': '太平洋卡',
            'value1': '2',
            'checked': false
        },
        {
            'label': '东方卡',
            'value1': '3',
            'checked': false
        },
        {
            'label': '牡丹卡',
            'value1': '4',
            'checked': false
        },
        {
            'label': '金穗卡',
            'value1': '5',
            'checked': false
        },
        {
            'label': '国内发行银联卡',
            'value1': '6',
            'checked': false
        },
    ]
    // 国外信用卡类型
    $scope.creditcardtype = [
        {
            'label': '万事达(Master)',
            'value1': '0',
        },
        {
            'label': '威士(Visa)',
            'value1': '1',
        },
        {
            'label': '运通(AMEX)',
            'value1': '2',
        },
        {
            'label': '大来(Diners Club)',
            'value1': '3',
        },
        {
            'label': 'JCB',
            'value1': '4',
        },
    ]
    
    $scope.facilitiesarr = [
        {
            'label': '免费wifi',
            'value1': '0',
            'checked': false
        },
        {
            'label': '收费wifi',
            'value1': '1',
            'checked': false
        },
        {
            'label': '免费宽带',
            'value1': '2',
            'checked': false
        },
        {
            'label': '收费宽带',
            'value1': '3',
            'checked': false
        },
        {
            'label': '免费停车场',
            'value1': '4',
            'checked': false
        },
        {
            'label': '收费停车场',
            'value1': '5',
            'checked': false
        },
        {
            'label': '免费接机服务',
            'value1': '6',
            'checked': false
        },
        {
            'label': '收费接机服务',
            'value1': '7',
            'checked': false
        },
        {
            'label': '室内游泳池',
            'value1': '8',
            'checked': false
        },
        {
            'label': '室外游泳池',
            'value1': '9',
            'checked': false
        },
        {
            'label': '健身房',
            'value1': '10',
            'checked': false
        },
        {
            'label': '商务中心',
            'value1': '11',
            'checked': false
        },
        {
            'label': '会议室',
            'value1': '12',
            'checked': false
        },
        {
            'label': '酒店餐厅',
            'value1': '13',
            'checked': false
        }
    ];
    $scope.supportCard = {
        'cardtype' : false
    }
   
    $scope.vm = {
        'date' : '',
        'options' : {
            format: "YYYY-MM-DD",
            locale : 'zh-cn',
            showClear : true
        }
    }
    $scope.vm1 = {
        'date' : '',
        'options' : {
            format: "YYYY-MM-DD",
            locale : 'zh-cn',
            showClear : true
        }
    }
    
    $scope.change = function(cardtype){
        console.log(cardtype);
        if(cardtype == false){
            $scope.hotelinfo.pay_by_card = '1'; 
        } else {
            $scope.hotelinfo.pay_by_card = '0'; // 不支持刷卡
            // 把银联卡之前选的都清空
            for(var i = 0; i < $scope.cardtype.length; i++){
                $scope.cardtype[i].value = false;
            }
            // 把信用卡之前选的都清空
            for(var i = 0; i < $scope.creditcardtype.length; i++){
                $scope.creditcardtype[i].value = false;
            }
            $scope.hotelinfo.unionPay = '';
            $scope.hotelinfo.foreign_credit_card = '';


        }
    }

    var facilitiesarray = []; // 设施
    var facilitiesstr = '';
    var cardtypearray = []; // 卡类型
    var cardtypestr = '';
    var creditcardarray = []; // 信用卡
    var credditcardstr = '';
    var breakfastarray = []; //早餐
    var breakfaststr = '';

    // //地区

   


    // 详情
    if(id){
        $resource('/api/as/tc/place/hotelInfo', {}, {}).
        save({'id' : id},function(res){
            if(res.errcode != 0){
                toaster.error({title: "", body:res.errmsg});
                return;
            }
            console.log('详情~~~~');
            console.log(res);
            $scope.hotelinfo = res.data;
            $scope.vm.date = res.data.opening_times; // 开业时间
            $scope.vm1.date = res.data.recently_renovateds; // 最近装修
            // $scope.vm2.date = res.data.earliest_arrival_times; // 最早到店时间
            // $scope.vm3.date = res.data.latest_departure_times; // 最晚离店时间
            $scope.model.startTime = res.data.earliest_arrival_times;
            $scope.model.endTime = res.data.latest_departure_times;
            // 银联卡
            var unionPayArr = [];
            unionPayArr = res.data.unionPay.split(',');
            for(var i = 0; i < $scope.cardtype.length; i++){
                for(var j = 0; j < unionPayArr.length; j++){
                    if($scope.cardtype[i].value1 == unionPayArr[j]){
                        console.log($scope.cardtype[i]);
                        $scope.cardtype[i].value = true;
                    }
                }

            }
            // 早餐
            var breakfastArr = [];
            breakfastArr = res.data.breakfast_type.split(',');
            for(var i = 0; i < $scope.breakfirsttype.length; i++){
                for(var j = 0; j < breakfastArr.length; j++){
                    if($scope.breakfirsttype[i].value1 == breakfastArr[j]){
                        console.log($scope.breakfirsttype[i]);
                        $scope.breakfirsttype[i].value = true;
                    }
                }

            }
            // 国外信用卡
            var creditArr = [];
            creditArr = res.data.foreign_credit_card.split(',');
            for(var i = 0; i < $scope.creditcardtype.length; i++){
                for(var j = 0; j < creditArr.length; j++){
                    if($scope.creditcardtype[i].value1 == creditArr[j]){
                        console.log($scope.creditcardtype[i]);
                        $scope.creditcardtype[i].value = true;
                    }
                }

            }
            // 设施
            var facilitiesArr = [];
            facilitiesArr = res.data.facilities.split(',');
            for(var i = 0; i < $scope.facilitiesarr.length; i++){
                for(var j = 0; j < facilitiesArr.length; j++){
                    if($scope.facilitiesarr[i].value1 == facilitiesArr[j]){
                        console.log($scope.facilitiesarr[i]);
                        $scope.facilitiesarr[i].value = true;
                    }
                }

            }
            if(res.data.pay_by_card == '0'){
                $scope.supportCard.cardtype = true;
                for(var i = 0; i < $scope.cardtype.length; i++){
                    $scope.cardtype[i].value = false;
                }
                // 把信用卡之前选的都清空
                for(var i = 0; i < $scope.creditcardtype.length; i++){
                    $scope.creditcardtype[i].value = false;
                }
                $scope.hotelinfo.unionPay = '';
                $scope.hotelinfo.foreign_credit_card = '';
            } else {
                $scope.supportCard.cardtype = false;
            }
        
        })
    };
    
    //地区

    $scope.page = {};
    
    var beforedata = {
        //省份列表
        'provincelist':
        $http({
            'method': 'GET',
            'url': '/api/us/sc/city/arealist',
        }),
        //城市列表
        'citylist':
        $http({
            'method': 'GET',
            'url': '/api/us/sc/city/arealist',
            'params': { 'code': '210000' },
        }),
        //区
        'districtlist':
        $http({
            'method': 'GET',
            'url': '/api/us/sc/city/arealist',
            'params': { 'code': '210100' },
        }),
        //类型
        'typelist':
        $http({
            'method': 'GET',
            'url': '/api/us/mc/mertradetypedao/findByTypeList',
            'params': { 'type': 'cheap_menu' },
        }),
        //主题
        'themelist':
        $http({
            'method' : 'GET', 
            'url': '/api/as/sc/dict/dictbytypelist',
            'params' : {'type' : 'place_theme'},
        }),
    };

    if($scope.hotelinfo.id != ''){
        beforedata['placeinfo'] = $http({
            'method' : 'GET', 
            'url': '/api/as/tc/place/hotelInfo',
            'params' : {
                'id' : $scope.hotelinfo.id
            }
        });
    }
    $q.all(beforedata).then(function(res){
        // 地址信息
        //分类信息
        if(res.provincelist.data.errcode === 0){

        }else{
            alert(res.provincelist.data.errmsg);
            return ;
        }

        if(res.citylist.data.errcode === 0){

        }else{
            alert(res.citylist.data.errmsg);
            return ;
        }

        if(res.districtlist.data.errcode === 0){

        }else{
            alert(res.districtlist.data.errmsg);
            return ;
        }
            
        if(res.themelist.data.errcode === 0){
        } else{
            alert(res.themelist.data.errmsg);
            return;					
        }

        if(angular.isDefined(res.placeinfo)){
            //地点信息
            if(res.placeinfo.data.errcode === 0){
                angular.extend($scope.hotelinfo, res.placeinfo.data.data);
                console.log('闲情____');
                $scope.vm.date = $scope.hotelinfo.opening_time;
                $scope.vm1.date = $scope.hotelinfo.recently_renovated;
                
            }else{
                alert(res.placeinfo.data.errmsg);
                return
            }
            
        }

       

        $scope.page = {
            'provincelist' : res.provincelist.data.data,
            'citylist' : res.citylist.data.data,
            'districtlist' : res.districtlist.data.data,
            'typelist' : res.typelist.data.data,
            'business_districtlist' : [],
            'themelist' : res.themelist.data.data
        }
        console.log('省');
        console.log($scope.page.provincelist);
        console.log($scope.hotelinfo.province,$scope.hotelinfo.city);
        if($scope.hotelinfo.province == ''){
            $scope.hotelinfo.province = '210000';	//辽宁省
            $scope.hotelinfo.city = '210100';		//沈阳市
            $scope.hotelinfo.area = '';		//铁西区
            $scope.hotelinfo.business_district = ''	//xxx商圈
        } else {
            getarea('province', $scope.hotelinfo.province);

            if(!$scope.hotelinfo.city == ''){
                getarea('city', $scope.hotelinfo.city);
            }

            if(!$scope.hotelinfo.area == ''){
                getarea('area', $scope.hotelinfo.area);
            }
            
        }
        

    });


    //省
    $scope.changeprovince = function(code){
        $scope.hotelinfo.city = '';
        $scope.hotelinfo.area = '';
        $scope.hotelinfo.business_district = '';

        $scope.page.citylist = [];
        $scope.page.districtlist = [];
        $scope.page.business_districtlist = [];

        getarea('province', code);
    };
    //市
    $scope.changecity = function(code){
        $scope.hotelinfo.area = '';
        $scope.hotelinfo.business_district = '';

        $scope.page.districtlist = [];
        $scope.page.business_districtlist = [];

        getarea('city', code);
    };
    //区
    $scope.changedistrict = function(code){
        console.log('ququuququ', code);
        $scope.hotelinfo.business_district = '';
        $scope.page.business_districtlist = [];
        getarea('area', code);
    };
    


    function getarea(what, code){
        $resource('/api/us/sc/city/arealist', {}, {})
        .get({'code' : code}, function(res){
            if(res.errcode !== 0){
                alert(res.errmsg);
                return;
            }

            if(what === 'province'){
                $scope.page.citylist = res.data;
            }else if(what === 'city'){
                $scope.page.districtlist = res.data;
            }else if(what === 'area'){
                $scope.page.business_districtlist = res.data;
            }
        });
    }

        // })   
    

    
    
    $scope.save = function(){
        // 设施
        console.log($scope.facilitiesarr);
        for(var i = 0; i < $scope.facilitiesarr.length; i++){
            var facilities = $scope.facilitiesarr[i];
            if(facilities.value == true){
                facilitiesarray.push(facilities.value1);
            }
        }
        facilitiesstr = facilitiesarray.join(',');
        $scope.hotelinfo.facilities = facilitiesstr;
        // 卡
        console.log($scope.cardtype);
        for(var i = 0; i < $scope.cardtype.length; i++){
            var card = $scope.cardtype[i];
            if(card.value == true){
                cardtypearray.push(card.value1);
            }
        }
        cardtypestr = cardtypearray.join(',');
        $scope.hotelinfo.unionPay = cardtypestr;
        // 早餐
        for(var i = 0;i < $scope.breakfirsttype.length; i++){
            var breakfast = $scope.breakfirsttype[i];
            if(breakfast.value == true){
                breakfastarray.push(breakfast.value1);
            }
        }
        breakfaststr = breakfastarray.join(',');
        $scope.hotelinfo.breakfast_type = breakfaststr;
        // 国外信用卡
        for(var i = 0; i < $scope.creditcardtype.length; i++){
            var creditcard = $scope.creditcardtype[i];
            if(creditcard.value == true){
                creditcardarray.push(creditcard.value1);
            }
        }
        credditcardstr = creditcardarray.join(',');
        $scope.hotelinfo.foreign_credit_card = credditcardstr;
        console.log($scope.vm.date);
        
        if($scope.vm.date === null){
            $scope.vm.date = '';
            $scope.hotelinfo.opening_time = '';        
        }
        if($scope.vm1.date === null){
            $scope.vm1.date = '';
            $scope.hotelinfo.recently_renovated = '';
        } 

        if (typeof $scope.vm.date._d === 'string') {
            $scope.vm.date._d = $scope.vm.date._d;
            $scope.hotelinfo.opening_time = ($scope.vm.date._d);
        } else {
            $scope.vm.date._d = date2str($scope.vm.date._d);
            $scope.hotelinfo.opening_time = ($scope.vm.date._d);
        }
        if (typeof $scope.vm1.date._d === 'string') {
            $scope.vm1.date._d = $scope.vm1.date._d;
            $scope.hotelinfo.recently_renovated = ($scope.vm1.date._d);
        } else {
            $scope.vm1.date._d = date2str($scope.vm1.date._d);
            $scope.hotelinfo.recently_renovated = ($scope.vm1.date._d);
        }
       
        if($scope.vm.date._d === undefined){
            $scope.vm.date = '';
            $scope.hotelinfo.opening_time = '';
        }
        if($scope.vm1.date._d === undefined){
            $scope.vm1.date = '';
            $scope.hotelinfo.recently_renovated = '';
        }
       
       

        // 开业时间
        // $scope.hotelinfo.opening_time = ($scope.vm.date._d);
        // 最近装修
        // $scope.hotelinfo.recently_renovated = ($scope.vm1.date._d);
        // // 最早到店时间
        // $scope.hotelinfo.earliest_arrival_time =($scope.vm2.date._d);
        // // 最晚离店时间
        // $scope.hotelinfo.latest_departure_time = ($scope.vm3.date._d);
        if($scope.hotelinfo.name !== '' && $scope.hotelinfo.hotel_Pinyin !== '' && $scope.hotelinfo.address !== '' && 
            $scope.hotelinfo.mobile !== '' && $scope.hotelinfo.star !== '' && $scope.hotelinfo.content !== '' &&
            $scope.hotelinfo.characteristic_info !== ''){
            if(id){
                var para = {
                    'id' : id
                }
                para = angular.extend($scope.hotelinfo, para);
                console.log(para);
                $resource('/api/as/tc/place/updateHotel', {}, {}).
                save(para,function(res){
                    if(res.errcode != 0){
                        toaster.error({title: "", body:res.errmsg});
                        return;
                    }
                    console.log(res);
                    toaster.success({title: "", body:'修改成功'});   
                    $state.go('app.hotellist');                 
                    
                })
            } else {
                $resource('/api/ac/tc/ticketPlaceService/createhotel', {}, {}).
                save($scope.hotelinfo,function(res){
                    console.log($scope.hotelinfo);
                    if(res.errcode != 0){
                        toaster.error({title: "", body:res.errmsg});
                        return;
                    }
                    console.log(res);
                    toaster.success({title: "", body:'创建成功'});   
                    $state.go('app.hotellist');                 
                })
            }
        } else {
            toaster.error({title: "", body:'信息填写不完全'});   
        }
        
        
    }

    function time2str(objDate) {
        if(angular.isDate(objDate))
        {
            
            var h = objDate.getHours();
            var mt = objDate.getMinutes();
            h = h < 10? '0' + h: h;
            mt = mt < 10? '0' + mt: mt;
           
            return h + ':' + mt;
            
        }
        else
        {
            return '错误格式';
        }
    }

}    