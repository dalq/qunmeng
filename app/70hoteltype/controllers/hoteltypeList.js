module.exports = function ($scope, $state, $stateParams
	, viewlist, tktlist, typepagelist, tktupdate, updateTicketPeriod, str2date
) {

	/* 分页
     * ========================================= */
	$scope.pageFlag = true;
    $scope.maxSize = 5;            //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10;         //每页显示几条

	$scope.searchform = {};

	//景区id
	console.log($stateParams);
	var placeid = $stateParams.placeid;

	if (placeid === "") {

	} else {
		$scope.searchform.place_code = placeid;
		console.log($scope.searchform.place_code);
	}

	viewlist({type:'H'}).then(function (res) {
		if (res.errcode === 0) {
			$scope.viewarr = res.data;
			$scope.viewarr.unshift({ name: '----全部----', code: '' });
		}
		else {
			alert(res.data.errmsg);
		}
	});

	$scope.load = function () {
		console.log($scope.searchform);
		console.log('上面是load传参')
		 var para = {
            pageNo: $scope.bigCurrentPage,
			pageSize: $scope.itemsPerPage,
			type:'H'
        };

        para = angular.extend($scope.searchform, para);
		if($scope.searchform.place_code){
			tktlist.save($scope.searchform, function (res) {

				/* 门票存储结构
				* ========================================= */
				var tkt = new Object();
				var restkt = new Array();

				if (res.errcode !== 0) {
					alert(res.errmsg);
					return;
				}

				//用景区编号作为存储结构的属性，值是数组
				for (var i = 0, j = res.data.length; i < j; i++) {
					var tt = res.data[i];
					var v = tt.place_code;

					if (!tkt.hasOwnProperty(v)) {
						tkt[v] = new Object();
						tkt[v].ticketarr = new Array();
						tkt[v].viewname = tt.view_name;
					}
					tkt[v].ticketarr.push(tt);
				}

				for (var key in tkt) {
					var o = tkt[key];
					restkt.push(o);
				}

				$scope.objs = restkt;
				$scope.pageFlag = false;

			});
		}else{
			typepagelist.save($scope.searchform, function (res) {

				/* 门票存储结构
				* ========================================= */
				var tkt = new Object();
				var restkt = new Array();

				if (res.errcode !== 0) {
					alert(res.errmsg);
					return;
				}

				//用景区编号作为存储结构的属性，值是数组
				for (var i = 0, j = res.data.results.length; i < j; i++) {
					var tt = res.data.results[i];
					var v = tt.place_code;

					if (!tkt.hasOwnProperty(v)) {
						tkt[v] = new Object();
						tkt[v].ticketarr = new Array();
						tkt[v].viewname = tt.view_name;
					}
					tkt[v].ticketarr.push(tt);
				}

				for (var key in tkt) {
					var o = tkt[key];
					restkt.push(o);
				}

				$scope.objs = restkt;
				$scope.pageFlag = true;
				$scope.bigTotalItems = res.data.totalRecord;

			});
		}

	};
	$scope.load();

	// 发布新票种
	$scope.create = function () {
		$state.go('app.hoteltypeCreate', { 'placeid': placeid });
	};

	$scope.edit = function (id) {
		$state.go('app.hoteltypeEdit', { 'id': id, 'placeid': placeid });
	};

	$scope.start = function (id) {
		tktupdate.get({ 'id': id, 'state': '0' }, function (res) {
			if (res.errcode === 0) {
				$scope.load();
			} else {
				alert(res.errmsg);
			}
		});
	}

	$scope.stop = function (id) {
		tktupdate.get({ 'id': id, 'state': '1' }, function (res) {
			if (res.errcode === 0) {
				$scope.load();
			} else {
				alert(res.errmsg);
			}
		});
	}

	$scope.usedstart = function (id) {
		tktupdate.get({ 'id': id, 'used_state': '0' }, function (res) {
			if (res.errcode === 0) {
				$scope.load();
			} else {
				alert(res.errmsg);
			}
		});
	}

	$scope.usedstop = function (id) {
		tktupdate.get({ 'id': id, 'used_state': '1' }, function (res) {
			if (res.errcode === 0) {
				$scope.load();
			} else {
				alert(res.errmsg);
			}
		});
	}
	$scope.updateTicket = function (code) {
		updateTicketPeriod.get({ 'code': code }, function (res) {
			if (res.errcode === 0) {
				alert(res.data.num);
			} else {
				alert(res.errmsg);
			}
		});
	}

	$scope.auth = function (code) {
		// alert('销票权限');
		$state.go('app.configurationticket', { 'tktcode': code });
	}

};
