module.exports = function ($scope, $state, $resource, toaster) {
        /* 分页
     * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10         //每页显示几条
    $scope.searchform = {};
    $scope.getlist = function () {
        var para = {
            pageNo:$scope.bigCurrentPage, 
            pageSize:$scope.itemsPerPage,
        };
        para = angular.extend($scope.searchform,para);
        $resource('/api/as/tc/placeview/hlist', {}, {}).
        save(para,function (res) {
            if (res.errcode != 0) {
                toaster.success({title:"",body:res.errmsg});
                return;
            }
            console.log(res);
            $scope.objs = res.data.results;
            $scope.bigTotalItems = res.data.totalRecord;
            $scope.objs.forEach(function(element) {
                element.isSelected = element.state=='1'?true:false;
            }, this);
        });   
    };
    $scope.getlist();
    $scope.edit = function(id){
        $state.go('app.creatHotel',{'id' : id});
    }
    
    $scope.create = function(){
        $state.go('app.creatHotel');
    }
    $scope.asort = function(code, asort){
        $resource('/api/as/tc/place/updateHotel', {}, {}).
        save({'code' : code, 'asort' : asort},function (res) {
            if (res.errcode != 0) {
                toaster.error({title:"",body:res.errmsg});
                return;
            }
            console.log(res);
            $scope.getlist();
    

        }); 
    }
    
    };