module.exports = function($scope, $state, $stateParams, $modalInstance, $resource,$modal,toaster, item, DateDiff){
    console.log(item);
    $scope.obj = item;

    $scope.nightNum = DateDiff(item.max_time, item.min_time);
    // 入住人数
    $resource('/api/ac/tc/hotelOrderService/findRoomNameList', {}, {}).
    save({'code' : item.code},function (res) {
        if (res.errcode != 0) {
            toaster.success({title:"",body:res.errmsg});
            return;
        }
        $scope.array = res.data.list;
        $scope.namearr = [];
        for(var i = 0; i < $scope.array.length; i++){
            $scope.namearr.push($scope.array[i].name);

        }
    });  

    // 可用房间数
    $scope.total = 0;
    for(var i = 0; i<item.roomList.length; i++){
        ($scope.total) = parseInt($scope.total) + parseInt(item.roomList[i].num);
    }
    // // 已退房间数
    // $scope.totalback = 0;
    // for(var i = 0; i<item.roomBackList.length; i++){
    //     ($scope.totalback) = parseInt($scope.totalback) + parseInt(item.roomBackList[i].back_num);
    // }
    console.log($scope.total);

    $scope.cancel = function(){
        $modalInstance.close();
    }

   

}   