var App = angular.module('hoteltype', []);

App.config(require('./router'));
App.factory('hoteltypeservice', require('./service'));

App.controller('hoteltypeList',require('./controllers/hoteltypeList'));
App.controller('hoteltypeCreate',require('./controllers/hoteltypeCreate'));
App.controller('tickettypeInfo',require('./controllers/tickettypeInfo'));
App.controller('hoteltypeEdit',require('./controllers/hoteltypeEdit'));

App.controller('tickettypeChoice',require('./controllers/tickettypeChoice'));
// App.controller('poptickettypeinfo',require('./controllers/poptickettypeinfo'));
App.controller('ttkttypeattr',require('./controllers/tkttypeattr'));
App.controller('ttkttypeattrcreate',require('./controllers/ttkttypeattrcreate'));
App.controller('ttkttypeattredit',require('./controllers/ttkttypeattredit'));

// 酒店管理
App.controller('hotellist',require('./controllers/hotellist')); // 酒店列表creatHotel
App.controller('creatHotel',require('./controllers/creatHotel')); // 创建酒店 hotelOrderList
// 酒店订单列表
App.controller('hotelOrderList',require('./controllers/hotelOrderList')); 
// 酒店详情
App.controller('hotelOrderInfo',require('./controllers/hotelOrderInfo')); 
//酒店产品列表
App.controller('hotelSupplyProductList',require('./controllers/hotelSupplyProductList')); 
//酒店待审核产品列表
App.controller('hotelApplyProductList',require('./controllers/hotelApplyProductList')); 


module.exports = App;