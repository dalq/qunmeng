var App = angular.module('equipment', []);

App.config(require('./router'));

//service
App.factory('equipmentservice', require('./service'));

//Controllers
App.controller('checkrecordinformation', require('./controllers/checkrecordinformation'));
App.controller('getinformation', require('./controllers/getinformation'));
App.controller('hotelinspectionrecord', require('./controllers/hotelinspectionrecord'));
App.controller('scenicinspectionrecord', require('./controllers/scenicinspectionrecord'));
App.controller('inforcount', require('./controllers/inforcount'));
App.controller('hotelmessage', require('./controllers/hotelmessage'));
App.controller('hoteldetailmessage', require('./controllers/hoteldetailmessage'));
App.controller('sysaccountmanager', require('./controllers/sysaccountmanager'));
App.controller('updateinformation', require('./controllers/updateinformation1'));
App.controller('updateinformation11', require('./controllers/updateinformation11'));
App.controller('equipmentmessageDetailInformation', require('./controllers/equipmentmessageDetailInformation'));
App.controller('user_manager', require('./controllers/user_manager'));
App.controller('information', require('./controllers/information'));

App.controller('equipmentBacklog', require('./controllers/equipmentBacklog'));
App.controller('equipmentManage', require('./controllers/equipmentManage'));
App.controller('equipmentMessage', require('./controllers/equipmentMessage'));
App.controller('equipmentParametere', require('./controllers/equipmentParametere'));
App.controller('equipmentUserManage',require('./controllers/equipmentUserManage'));
App.controller('equipmentAdd',require('./controllers/equipmentAdd'));
App.controller('equipmentPersonManage',require('./controllers/equipmentPersonManage'));
App.controller('equipmentAddPerson', require('./controllers/equipmentAddPerson'));
App.controller('equipmentUpdatePerson', require('./controllers/equipmentUpdatePerson'));
App.controller('equipmentUpdate', require('./controllers/equipmentUpdate'));
App.controller('reuipmentDetails', require('./controllers/reuipmentDetails'));
App.controller('equipmentBacklogJ', require('./controllers/equipmentBacklogJ'));
App.controller('equipmentchart1', require('./controllers/equipmentchart1'));
App.controller('euipmentBacklogDetails', require('./controllers/euipmentBacklogDetails'));
App.controller('changepassword',require('./controllers/changepassword'));
App.controller('reuipmentStatus',require('./controllers/reuipmentStatus'));
App.controller('reuipmentMainrec',require('./controllers/mainRec'));
App.controller('reuipmentShowpic',require('./controllers/reuipmentShowpic'));
module.exports = App;