 /**
 * 子模块路由
 * DHpai
 */

var router = function($urlRouterProvider, $stateProvider){
 	$stateProvider
   //用户管理
    .state('app.user_manager', {
        url: "/equipment/usermanager.html/:officeid",
        views: {
            'main@' : {
                template : require('../27equipment/views/user_manager.html'),
                controller : 'user_manager',
            }
        }
    })
    //系统账号管理
     .state('app.sys_account_manager', {
        url: "/equipment/sys_account_manager.html",
        views: {
            'main@' : {
                template : require('../27equipment/views/sysaccountmanager.html'),
                controller : 'sysaccountmanager',
            }
        }
    })
    //待办事项列表
     .state('app.equipmentBacklog', {
        url: "/equipment/equipmentbacklog.html",
        views: {
            'main@' : {
                template : require('../27equipment/views/EquipmentBacklog.html'),
                controller : 'equipmentBacklog',
            }
        }
    })
    //景区端待办事项列表
    .state('app.equipmentBacklogJ', {
        url: "/equipment/equipmentbacklogJ.html",
        views: {
            'main@' : {
                template : require('../27equipment/views/EquipmentBacklogJ.html'),
                controller : 'equipmentBacklogJ',
            }
        }
    }) 
    //图形
    .state('app.equipmentchart1', {
        url: "/equipment/equipmentchart1.html",
        views: {
            'main@' : {
                template : require('../27equipment/views/equipmentchart1.html'),
                controller : 'equipmentchart1',
            }
        }
    })
    //修改密码
    .state('app.changepassword', {
        url: "/equipment/changepassword.html",
        views: {
            'main@' : {
                template : require('../27equipment/views/changepassword.html'),
                controller : 'changepassword',
            }
        }
    })  
    //设备管理
    .state('app.equipmentManage', {
        url: "/equipment/equipmentmanage.html",
        views: {
            'main@' : {
                template : require('../27equipment/views/EquipmentManage1.html'),
                controller : 'equipmentManage',
            }
        },
        resolve:{
        }
    })
    //设备责任人管理
    .state('app.equipmentPersonManage', {
        url: "/equipment/equipmentpersonmanage.html",
        views: {
            'main@' : {
                template : require('../27equipment/views/EquipmentPersonManage.html'),
                controller : 'equipmentPersonManage',
            }
        },
        resolve:{
        }
    })
    //景区信息
    .state('app.equipmentScenicMessage', {
        url: "/equipment/equipmentmessage.html",
        views: {
            'main@' : {
                template : require('../27equipment/views/EquipmentMessage.html'),
                controller : 'equipmentMessage',
            }
        },
        resolve:{
           
        }
    })
    //酒店信息
    .state('app.equipmentHotelMessage', {
        url: "/equipment/equipmenthotelmessage.html",
        views: {
            'main@' : {
                template : require('../27equipment/views/HotelMessage.html'),
                controller : 'hotelmessage',
            }
        },
        resolve:{
             
        }
    })

      //景区详细信息
    .state('app.equipmentMessageDetailInformation', {
        url: "/equipment/equipmentmessageDetailInformation.html/:geid/:show",
        views: {
            'main@' : {
                template : require('../27equipment/views/equipmentmessageDetailInformation.html'),
                controller : 'equipmentmessageDetailInformation',
            }
        },
        resolve:{
            
        }
    })
        //酒店详细信息
    .state('app.equipmentMessageHotelDetailInformation', {
        url: "/equipment/equipmentmessageHotelDetailInformation.html/:geid",
        views: {
            'main@' : {
                template : require('../27equipment/views/hoteldetailmessage.html'),
                controller : 'hoteldetailmessage',
            }
        },
        resolve:{
            
        }
    })
    //参数
    .state('app.equipmentParameter', {
        url: "/equipment/equipmentparameter.html",
        views: {
            'main@' : {
                template : require('../27equipment/views/EquipmentParameter.html'),
                controller : 'equipmentParametere',
            }
        },
        resolve:{
        }
    })
    //用户管理
    .state('app.equipmentUserManage', {
        url: "/equipment/equipmentusermanage.html",
        views: {
            'main@' : {
                template : require('../27equipment/views/EquipmentUserManage.html'),
                controller : 'equipmentUserManage',
            }
        },
        resolve:{
        }
    })
    //添加设备
    .state('app.equipmentAdd', {
        url: "/equipment/equipmentadd.html",
        views: {
            'main@' : {
                template : require('../27equipment/views/EquipmentAdd.html'),
                controller : 'equipmentAdd',
            }
        },
        resolve:{
        }
    })
    //修改设备
    .state('app.equipmentUpdate', {
        url: "/equipment/equipmentUpdate.html/:id",
        views: {
            'main@' : {
                template : require('../27equipment/views/EquipmentUpdate.html'),
                controller : 'equipmentUpdate',
            }
        },
        resolve:{
        }
    })

      //信息统计
    .state('app.informationcount', {
        url: "/equipment/informationcount.html",
        views: {
            'main@' : {
                template : require('../27equipment/views/inforcount.html'),
                controller : 'inforcount',
            }
        },
        resolve:{
        }
    })
         //景区检验记录
    .state('app.scenicinspectionrecord', {
        url: "/equipment/scenicinspectionrecord.html",
        views: {
            'main@' : {
                template : require('../27equipment/views/scenicinspectionrecord.html'),
                controller : 'scenicinspectionrecord',
            }
        },
        resolve:{
        }
    })
        //酒店检验记录
    .state('app.hotelinspectionrecord', {
        url: "/equipment/hotelinspectionrecord.html",
        views: {
            'main@' : {
                template : require('../27equipment/views/hotelinspectionrecord.html'),
                controller : 'hotelinspectionrecord',
            }
        },
        resolve:{
        }
    })
};

module.exports = router;