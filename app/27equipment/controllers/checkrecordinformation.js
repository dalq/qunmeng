module.exports = function($scope,item, $state, $stateParams,$resource,$modal,$modalInstance){
    
        $scope.info={};
        $scope.info1={};
        var geid=item.gmeid;
       $scope.info1=item;

        if(item.gmpic !== undefined){
		        var imgjson=[];
		        
			    var imgStr=item.gmpic.substr(2,item.gmpic.length-4);
			    var imgArr=imgStr.split("\",\"");
			    for(var j=0;j<imgArr.length;j++){
				    var json={};
				    json.img=imgArr[j];
				    imgjson.push(json);
			    }
			    $scope.imagearr=imgjson;

        }
        
	$resource('/api/ac/ggc/equipmentManageDetailInformationService/findOneEquipmentDetailInformation', 
		{}, {}).save({'geid':geid}, function(res){
	                if(res.errcode === 0 || res.errcode === 10003){
						$scope.info=res.data;

						   if($scope.info.guplace==0){
       $scope.info.guplace='和平区';
    }
    if($scope.info.guplace==1){
       $scope.info.guplace='沈河区';
    }
    if($scope.info.guplace==2){
       $scope.info.guplace='皇姑区';
    }
      if($scope.info.guplace==3){
       $scope.info.guplace='铁西区';
    }
    if($scope.info.guplace==4){
       $scope.info.guplace='大东区';
    }
    if($scope.info.guplace==5){
       $scope.info.guplace='东陵区';
    }
    if($scope.info.guplace==6){
       $scope.info.guplace='于洪区';
    }
      if($scope.info.guplace==7){
       $scope.info.guplace='苏家屯区';
    }
    if($scope.info.guplace==8){
       $scope.info.guplace='沈北新区';
    }
    if($scope.info.guplace==9){
       $scope.info.guplace='浑南新区';
    }
    if($scope.info.guplace==10){
       $scope.info.guplace='辽中县';
    }
    if($scope.info.guplace==11){
       $scope.info.guplace='康平县';
    }
      if($scope.info.guplace==12){
       $scope.info.guplace='法库县';
    }
    if($scope.info.guplace==13){
       $scope.info.guplace='新民市';
    }
						if(res.data.gecertificate=='0'){
                             $scope.info.gecertificate_f='不齐全';

						}
					    if(res.data.gecertificate=='1'){
                             $scope.info.gecertificate_f='齐全';
						}
						 if(res.data.gemaintenance_cycle!=undefined || res.data.gemaintenance_cycle !=''){
                             $scope.info.gemaintenance_cycle=res.data.gemaintenance_cycle+'天';
						}
					 
	                	
	                }else{
	                    alert(res.errmsg);
	                }
               	 });
	$scope.cancel = function () {
            $modalInstance.dismiss('cancel');
    };


}