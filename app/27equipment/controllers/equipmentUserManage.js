module.exports = function($scope, $state, $log,$stateParams,$resource,$modal){

     
     $scope.usermanager=function(res){
                $resource('/api/ac/ggc/equipmentUserManageService/findOneUserCompanyCode', {}, {}).
                           save({}, function(resss){
                                       var lname=resss.data.getloginname;
                                       for(var i=0;i<res.length;i++){
                                           if(lname==res[i].code){
                                               
                                              res[i].flag=true;

                                           }else{
                                             res[i].flag=false;
                                           }

                                       }   
                                  })
     }
    //获得用户（机构）列表
     $scope.getuserlist=function(){
                 $resource('/api/as/sc/office/myofficeList', {}, {}).
                  save({}, function(res){
                    console.log(res);
                     $scope.totalItems=res.data.length
                     $scope.currentPage=1;
                     $scope.itemsPerPage=10;
                     var arr=[];                    
                     for(var i=0;i<10;i++){
                          if(res.data[i]){
                             arr.push(res.data[i]);

                          }
                        
                     }
                     $scope.objs=arr;
                     $scope.pageChanged=function(currentPage){
                        var arr=[];
                        $scope.objs=[];
                        for(j=currentPage*10-10;j<=(currentPage)*10-1;j++){
                                                   
                            if(j==res.data.length){
                                break;
                            }
                            if(res.data[j]!==undefined){
                               arr.push(res.data[j]);

                            }
                            
                        }
                        $scope.objs=arr;
                     }

                    //  $scope.usermanager(res);
                    
                  });
             };

    $scope.getuserlist();

   //详情
  $scope.information = function (item) {
       $resource('/api/ac/ggc/equipmentUserManageService/findOneEquipmentUser', {}, {}).
      save({'code':item.code}, function(res){
          if(res.errcode === 0 || res.errcode === 10003){
                 var modalInstance = $modal.open({
                  template: require('../views/information.html'),
                    controller:'information',
                  url: '/get_information',
                  size: 'lg',
                  resolve: {
                    'itemparam': function () {
                      return res.data;
                    },
                     item: function () {
                            return item;
                    }
                  }
                
                });

                modalInstance.opened.then(function() {// 模态窗口打开之后执行的函数
                });
                modalInstance.result.then(function(result) {
                 }, function(reason) {
                       $scope.getuserlist();
                   // $state.go('app.equipmentUserManage',{},{reload: true});// 点击空白区域，总会输出backdrop
                     // click，点击取消，则会暑促cancel
                     // $log.info('Modal dismissed at: ' + new Date());
                 });
          }else{
              alert(res.errmsg)
            }
      })
		
  }; 

	

	  // 分配角色
  $scope.updateinformation = function (item) {

      $resource('/api/ac/ggc/equipmentUserManageService/findOneEquipmentUser', {}, {}).
      save({'code':item.code}, function(res){
            if(res.errcode === 0 || res.errcode === 10003){
                  res.data.zid=item.id;
                   var modalInstance = $modal.open({
                    template: require('../views/updateinformation.html'),
                      controller:'updateinformation',
                    url: '/updateinformation11',
                    size: 'lg',
                    resolve: {
                            itemparam: function () {
                            return res.data;
                          },
                        
                            item: function () {
                            return item;
                          },

                            mydate2str: function (utilservice) {
                            return utilservice.getDate;
                          }

                    
                    }
                  
                  });

                   modalInstance.opened.then(function() {// 模态窗口打开之后执行的函数
                });
                modalInstance.result.then(function(result) {
                 }, function(reason) {
                       location.reload(); 
                       $scope.getuserlist();
                 });
            }
        })
      
	};


  //编辑信息
$scope.updateinfor=function(item){


  $resource('/api/ac/ggc/equipmentUserManageService/findOneEquipmentUser', {}, {}).
      save({'code':item.code}, function(res){
            if(res.errcode === 0 || res.errcode === 10003){
                   var modalInstance = $modal.open({
                    template: require('../views/updateinformation11.html'),
                      controller:'updateinformation11',
                    url: '/updateinformation112',
                    size: 'lg',
                    resolve: {
                            itemparam: function () {
                            return res.data;
                          }
                            
                    
                    }
                  
                  });

                modalInstance.opened.then(function() {// 模态窗口打开之后执行的函数
                });
                modalInstance.result.then(function(result) {
                 }, function(reason) {
                     
                     $scope.getuserlist();// 点击空白区域，总会输出backdrop
                    
                 });
            }else{
              alert(res.errmsg)
            }
        })


}

    // $scope.createuserlist=function(item){
   
    //      $state.go('app.user_manager',{'officeid':item.id});
    // }

    $scope.addUser = function(item){
    	  
      
      if(item == undefined){
          alert("请填写用户信息");
        
      }else{
         if(item.name==undefined || item.name==""){
            alert("请填写单位名称");
              return false;
         }
          if(item.place==undefined || item.place==""){
             alert("请填写所在区县");
              return false;
         }
             
       if(item.type==undefined || item.type==""){
              alert("请选择用户类别");
              return false;
         }
          if(item.place==undefined || item.place==""){
              alert("请填写区县信息");
              return false;
         }
         for(var i = 0;i<$scope.objs.length;i++){
           if(item.name == $scope.objs[i].name){
             alert('该用户名已存在！');
             return;
           }
         }
         if(confirm("提示：请核准用户名称、类别等信息，用户创建成功后无法删除！")){
                //创建机构用户
                $resource('/api/ac/sc/office/addOfficeForMonitor', {}, {}).save({'name' : item.name}, function(res){
                  item.loginname=res.data.login_name;
                  if(res.errcode === 0)
                  {
                   
                      $resource('/api/ac/ggc/equipmentUserManageService/createEquipmentUser', {}, {}).
                      save(item, function(res){
                        console.log(item);
                            if(res.errcode === 0 || res.errcode === 10003){
                                  alert("创建成功，用户的登录账号为："+item.loginname);
                                 if(confirm("是否继续创建用户？")){
                                      $scope.obj={};
                                     $scope.getuserlist();
                                 }else{
                                      //$scope.getuserlist();
                                      location.reload(); 
                                      $scope.getuserlist();

                                     //$state.go('app.equipmentUserManage',{},{reload: true});
                                 }
                              
                            }else{
                                alert(res.errmsg);
                            }
                        });

                     
                  }
                  else
                  {
                      alert(res.errmsg);
                  }
              });
        
         }
    
      }

    };

    $scope.cancel=function(){
     
      location.reload(); 
    };
  
    $scope.pass = {};
    
   $scope.gogo = function(){


        if($scope.pass.newPassword == undefined || $scope.pass.newPassword =="" )
        {
          alert('请输入新密码');
          return;
        }
       
        if($scope.pass.newPassword !== $scope.pass.newPassword2)
        {
          alert('两次输入的密码不一致。');
          return;
        }

       $resource('/a/sys/user/ajaxmodifyPwd', {}, {}).
       save($scope.pass, {}, function(res){

          alert(res.message);

          $scope.pass={};
        });

  };

    $resource('/a/sys/user/ajaxform', {}, {}).get({}, function(res){

        $scope.objss = res.allRoles;
        $scope.company = res.company;

    });


}