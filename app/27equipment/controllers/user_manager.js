module.exports = function($scope, $state, $stateParams,$resource,$modal){

	var number=$stateParams.officeid;
	   /* 分页
     * ========================================= */
    $scope.maxSize = 5;            //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 30;         //每页显示几条
    
    $scope.load = function () {
        
        var para = {
            pageNo:$scope.bigCurrentPage, 
            pageSize:$scope.itemsPerPage,
            'company.id' : number
        };

        // para = angular.extend({'office.id' : officeid, 'office.name' : officename}, para);
        
        $resource('/a/sys/user/ajaxlist',{},{}).get(para, function(res){
            $scope.objs = res.list;
            $scope.bigTotalItems = res.count;
        });

    };
    $scope.load();

}