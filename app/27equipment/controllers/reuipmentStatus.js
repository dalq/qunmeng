module.exports = function($scope, $modal, $state,$resource,$modalInstance,items){
    // $resource('/api/ac/ggc/equipmentBacklogService/updateReject', {}, {}).save({ id: '3df48265-3134-11e7-9bac-a0d3c1f10268'}, function (res) {
    //                         if (res.errcode === 0) {
    //                             alert('维护成功');
    //                             $scope.cancel();
    //                         } else {
    //                             alert(res.errmsg);
    //                         }
    //                     });
    if(items.pic_url!=undefined){
        var imgjson=[];
        var imgStr=items.pic_url.substr(2,items.pic_url.length-4);
        var imgArr=imgStr.split("\",\"");
        for(var j=0;j<imgArr.length;j++){
            var json={};
            json.img=imgArr[j];
            imgjson.push(json);
        }
        $scope.imagearr=imgjson;
    }
    
    //$scope.imagearr=imgtojson(imgArr);
    $scope.saveimg;
    function date2str2(d) {
				if (d === undefined) {
					return "";
				}
				var month = (d.getMonth() + 1).toString();
				var day = d.getDate().toString();
				if (month.length == 1) month = '0' + month;
				if (day.length == 1) day = '0' + day;
				return d.getFullYear() + "-" + month + "-" + day;
				// return d.getFullYear() + month + day;
	}
    $scope.image = function(){
		var para = $state.get('app.imageupload');
        var modalInstance = $modal.open(para);
        modalInstance.opened.then(function() {// 模态窗口打开之后执行的函数  
        });  
        modalInstance.result.then(function(result) {  
            $scope.saveimg = JSON.stringify(result);
            $scope.imagearr=imgtojson(result);
            // form.result['templete_lock_data_json'] = JSON.stringify(result.lock);
            // form.result['templete_check_data_json'] = result.unlock;
        }, function(reason) {  
            // click，点击取消，则会暑促cancel  
            $log.info('Modal dismissed at: ' + new Date());  
        }); 
	};
    var imgtojson=function(result){
        var imgs=[];
        for(var i=0;i<result.length;i++){
            var json={};
            json.img=result[i];
            imgs.push(json);
        }
        return imgs;       
    };
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
    $scope.ok=function(){
        if($scope.imagearr==undefined){
            alert("请先上传图片");
            return;
        }
        if($scope.saveimg==undefined){
            alert("请重新上传图片");
            return;
        }
                   var j = new Date();
                   var today=date2str2(j);
                 
                   var nextdate=items.reinspection_data;

                  if(today<=nextdate){
                      if(!confirm("是否确认维护")){
                            return ;
                        }
                        //alert($scope.itemw.code);
                        $resource('/api/ac/ggc/equipmentManageService/updateStatus', {}, {}).save({ id: items.id,pic_url:$scope.saveimg}, function (res) {
                            if (res.errcode === 0) {
                                alert('维护成功');
                                $scope.cancel();
                            } else {
                                alert(res.errmsg);
                            }
                        });

                  }
                   if(today>nextdate){
                      if(!confirm("是否确认维护")){
                            return ;
                        }
                        //alert($scope.itemw.code);
                        $resource('/api/ac/ggc/equipmentManageService/updateStatu', {}, {}).save({ id: items.id,pic_url:$scope.saveimg }, function (res) {
                            if (res.errcode === 0) {
                                alert('维护成功');
                                $scope.cancel();
                            } else {
                                alert(res.errmsg);
                            }
                        });
                  }
    }
                    
}