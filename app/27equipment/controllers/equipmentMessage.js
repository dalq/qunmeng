
module.exports = function($scope,$http,$q, $state, $stateParams,$resource,$modal){
    $scope.searchform = {};
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.currentPage = 1;      //当前页码
    $scope.itemsPerPage = 10         //每页显示几条
 
   
   $scope.pageChanged = function () {
       //获取place
       $resource('api/ac/ggc/equipmentUserManageService/getPlaceByCompanyCode', {}, {}).
      save({}, function(res){

                if(res.errcode === 0 || res.errcode === 10003){                  
                var  resstate=res.data.place;              
                var para = {
                        place: resstate,
                        pageNo:$scope.currentPage,
                        pageSize:$scope.itemsPerPage
                    };
        para = angular.extend($scope.searchform, para);
    // 风控设备总概况信息查询
      $resource('/api/ac/ggc/equipmentManageDetailInformationService/findEquipmentTotalInformationList', {}, {}).
      save(para, function(res){

                if(res.errcode === 0 || res.errcode === 10003){
                     //console.log(res);
                     
                     for(var i=0;i<res.data.results.length;i++){
                           if(res.data.results[i].type==3){
                                  res.data.results.splice(i, 1);
                            }
                     }
                      $scope.userList=res.data.results;
                      $scope.totalItems = res.data.totalRecord;

                }else{
                    alert(res.errmsg);
                }
            });                
                }else{
                    alert(res.errmsg);
                }


            });

 

    };
     $scope.pageChanged();

     $scope.information=function(info){
             $state.go('app.equipmentMessageDetailInformation',{'geid':info.code1});
     }

}