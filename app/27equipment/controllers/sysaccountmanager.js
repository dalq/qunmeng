module.exports = function($scope, $state, $log,$stateParams,$resource,$modal){
 $scope.searchform = {};
   $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.currentPage = 1;      //当前页码
    $scope.itemsPerPage = 10       //每页显示几条


  $scope.pageChanged = function () {
       var para = {
        //    ta:1,
          pageNo:$scope.currentPage,
            pageSize:$scope.itemsPerPage,
          
           
        };
        para = angular.extend($scope.searchform, para);

        $resource('/api/ac/ggc/equipmentUserManageService/findEquipmentUserList', {}, {}).
      save(para, function(res){
                if(res.errcode === 0 || res.errcode === 10003){
                      $scope.userList={};
                      $scope.userList=res.data.results;
                      $scope.totalItems = res.data.totalRecord;
                         for(var i = 0;i < res.data.results.length; i++) {
                              if(res.data.results[i].type=='1'){

                                 res.data.results[i].typename='景区';
                              }else if(res.data.results[i].type=='2'){
                                  res.data.results[i].typename='酒店';
                              }
                            
                          }

                }else{
                    alert(res.errmsg);
                }
            });

    };
      $scope.pageChanged();

    //获得机构列表，看到各自的，L0002可以看到全部
     //当前的id
    // $scope.currentid = '';
    // $scope.currentcode = '';
    // $scope.currentname = '';

    // $scope.editshow = false;
      // function mechanismtree(){
    //获得机构列表
           $resource('/a/sys/office/treeData', {}, {}).
            query({}, function(res){
                // alert("ppppp");
                  $scope.objs=res;
                   $resource('/api/ac/ggc/equipmentUserManageService/findOneUserCompanyCode', {}, {}).
                  save({}, function(resss){
                       var lname=resss.data.getloginname;
                       for(var i=0;i<res.length;i++){
                           if(lname==res[i].code){
                              // alert("fffff"); 
                              res[i].flag=true;

                           }else{
                             res[i].flag=false;
                           }

                       }

                  })

             
         });


     

  //  $scope.createstb = function () {

  //   var modalInstance = $modal.open({
  //      template: require('../views/createjg.html'),
  //      controller:'createjg',
  //      url: '/cratejg',
  //      size: 'lg'
    
  //  });
  // }
   

	 // $scope.searchform = {};

    // $scope.obj={};
	// $scope.load = function () {
 //       var para = {
 //        //    ta:1, 
 //            pageNo:$scope.currentPage, 
 //            pageSize:$scope.itemsPerPage,
	// 		// deptid : $scope.deptid,
	// 		// deptname : $scope.deptname
		
 //        };
 //        para = angular.extend($scope.searchform, para);
 //        $resource('/api/ac/ggc/equipmentUserManageService/findEquipmentUserList', {}, {}).
	// 		save(para, function(res){
				
 //                if(res.errcode === 0 || res.errcode === 10003){
 //            					$scope.userList=res.data.results;
 //            					$scope.totalItems = res.data.totalRecord;
                	
 //                }else{
 //                    alert(res.errmsg);
 //                }
 //            });

 //    };
	//  $scope.load();
  
 

   //详情
  $scope.information = function (item) {
       $resource('/api/ac/ggc/equipmentUserManageService/findOneEquipmentUser', {}, {}).
      save({'code':item.code}, function(res){
          if(res.errcode === 0 || res.errcode === 10003){
                 var modalInstance = $modal.open({
                  template: require('../views/information.html'),
                    controller:'information',
                  url: '/getinformation',
                  size: 'lg',
                  resolve: {
                    'itemparam': function () {
                      return res.data;
                    },
                     item: function () {
                            return item;
                    }
                  }
                
                });

                  modalInstance.opened.then(function() {// 模态窗口打开之后执行的函数
                });
                modalInstance.result.then(function(result) {
                 }, function(reason) {
                    $state.go('app.equipmentUserManage',{},{reload: true});// 点击空白区域，总会输出backdrop
                     // click，点击取消，则会暑促cancel
                     // $log.info('Modal dismissed at: ' + new Date());
                 });
          }
      })
		
  }; 

	

	  // 编辑
  $scope.updateinformation = function (item) {

		// $state.go('app.productInfo', {'id' : id});

         
      
                   var modalInstance = $modal.open({
                    template: require('../views/updateinformation11.html'),
                      controller:'updateinformation11',
                    url: '/updateinformation111',
                    size: 'lg',
                    resolve: {
                            
                        
                            item: function () {
                            return item;
                          },

                            mydate2str: function (utilservice) {
                            return utilservice.getDate;
                          }

                    
                    }
                  
                  });

                   modalInstance.opened.then(function() {// 模态窗口打开之后执行的函数
                });
                modalInstance.result.then(function(result) {
                 }, function(reason) {
                    $state.go('app.sys_account_manager',{},{reload: true});// 点击空白区域，总会输出backdrop
                     // click，点击取消，则会暑促cancel
                     // $log.info('Modal dismissed at: ' + new Date());
                 });
            
        
		
	};

    $scope.addUser = function(item){
    	  
      
      if(item == undefined){
          alert("请填写用户信息");
        
      }else{
         if(item.name==undefined || item.name==""){
            alert("请填写单位名称");
              return false;
         }
          if(item.place==undefined || item.place==""){
             alert("请填写所在区县");
              return false;
         }
         //  if(item.loginname==undefined || item.name==""){
         //    alert("请填写用户的登录账号");


         //      return false;
         // }
      
       if(item.type==undefined || item.type==""){
              alert("请选择用户类别");
              return false;
         }
          if(item.responsible_person==undefined || item.responsible_person==""){
              alert("请填写主要负责人（法人）");
              return false;
         }
          if(item.responsible_person_phone==undefined || item.responsible_person_phone==""){
              alert("请填写联系电话");
              return false;
         }
         if(confirm("提示：请核准信息，用户创建成功后无法删除！")){
                //创建机构用户
          $resource('/a/sys/office/ajaxsave', {}, {}).save({'name' : item.name}, {}, function(res){
            item.loginname=res.errmsg;
            if(res.errcode === 0)
            {
               $resource('/api/ac/ggc/equipmentUserManageService/createEquipmentUser', {}, {}).
                save(item, function(res){
                if(res.errcode === 0 || res.errcode === 10003){
                      alert("创建成功，用户的登录账号为："+item.loginname);
                     if(confirm("是否继续创建用户？")){
                          $scope.obj={};
                          $scope.pageChanged();
                     }else{
                         $state.go('app.equipmentUserManage',{},{reload: true});
                     }
                  
                }else{
                    alert(res.errmsg);
                }
            });
                // alert("机构添加成功");
               
                // $state.go('app.equipmentUserManage',{},{reload:true});
               
            }
            else
            {
                alert(res.errmsg);
            }
        });
        
         }
        
      

    
      }
      

    };
   //  $scope.deleteUserInfo=function(item){
   //  	$resource('/api/ac/ggc/equipmentUserManageService/deleteEquipmentUser', {}, {}).
			// save({id:item.id,flag:'1'}, function(res){
				
   //              if(res.errcode === 0 || res.errcode === 10003){
			// 		               alert("删除用户成功");
   //              	  $state.go('app.equipmentUserManage',{},{reload: true});
   //              }else{
   //                  alert(res.errmsg);
   //              }
   //          });


   //  };
     $scope.cancel=function(){
    	
       $state.go('app.equipmentUserManage',{},{reload: true});

    };
  
    $scope.pass = {};
    
  $scope.gogo = function(){


    if($scope.pass.newPassword == undefined || $scope.pass.newPassword =="" )
    {
      alert('请输入新密码');
      return;
    }
   
    if($scope.pass.newPassword !== $scope.pass.newPassword2)
    {
      alert('两次输入的密码不一致。');
      return;
    }

   $resource('/a/sys/user/ajaxmodifyPwd', {}, {}).
   save($scope.pass, {}, function(res){


      alert(res.message);

      $scope.pass={};
    });

  };

    $resource('/a/sys/user/ajaxform', {}, {}).get({}, function(res){


        $scope.objss = res.allRoles;
        $scope.company = res.company;

    });


}