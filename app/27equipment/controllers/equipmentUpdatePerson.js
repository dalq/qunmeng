module.exports = function($scope,$resource,$state,$modalInstance,items){
	 $scope.equipmentperson=items;
	//用户名非空验证
	$scope.equipmentperson.nameadd=function(){
    $scope.equipmentperson.namecheck=false;
	}
	$scope.equipmentperson.nameblur=function(){
		if ($scope.equipmentperson.name==undefined || $scope.equipmentperson.name==""){
					$scope.equipmentperson.namecheck=true;
				}
	}
	//维护周期非空验证
	$scope.equipmentperson.mobileadd=function(){	
    $scope.equipmentperson.mobilecheck=false;
	}
	$scope.equipmentperson.mobileblur=function(){
		if ($scope.equipmentperson.mobile==undefined || $scope.equipmentperson.mobile==""){
					$scope.equipmentperson.mobilecheck=true;
				}
	}
	$scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
	$scope.ok=function(){
		//提交校验
	   if($scope.equipmentperson.name==undefined || $scope.equipmentperson.name==""){
			$scope.equipmentperson.namecheck=true;
			return;
	   }
	   if($scope.equipmentperson.mobile==undefined || $scope.equipmentperson.mobile==""){
			$scope.equipmentperson.mobilecheck=true;
			return;
	   }	   								
		var savedata={};
		savedata.id=items.id;
		savedata.name=$scope.equipmentperson.name;
		savedata.mobile=$scope.equipmentperson.mobile;
        savedata.remark=$scope.equipmentperson.remark;
		$resource('/api/ac/ggc/equipmentPerManageService/update', {}, {}).save(savedata, function(res){
			if(res.errcode==0){
				$scope.cancel();				
			}else{
				alert(res.errmsg);
			}
    	});			   
		};	
};