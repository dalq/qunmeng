module.exports = function($scope,$state,$resource,$modal){
   
    /* 分页
     * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.currentPage = 1;      //当前页码
    $scope.itemsPerPage = 10 ;           //每页显示几条
    $scope.reuipmentbl={};
    $scope.reuipmentbl1={};
    $scope.reuipmentbl1.name="";
    $scope.reuipmentbl1.r_name=""
    $scope.reuipmentbl2={};
    $scope.reuipmentbl2.name="";
    $scope.reuipmentbl2.r_name=""
    function date2str2(d) {
				if (d === undefined) {
					return "";
				}
				var month = (d.getMonth() + 1).toString();
				var day = d.getDate().toString();
				if (month.length == 1) month = '0' + month;
				if (day.length == 1) day = '0' + day;
				return d.getFullYear() + "-" + month + "-" + day;
				// return d.getFullYear() + month + day;
	}   
	$scope.pageChanged = function () {
       $scope.reuipmentbl.r_names=[{ id:"",name:"未选择"}];	 
		$resource('/api/ac/ggc/equipmentPerManageService/findAllEquPer', {}, {}).save({}, function(res){
			if(res.errcode==0){	
				for(var i = 0;i < res.data.list.length; i++){
					$scope.reuipmentbl.r_names.push(res.data.list[i]);
				}	
			}else{
				alert(res.errmsg);
			}
		}); 
       var para = {
            pageNo:$scope.acurrentPage,
            pageSize:$scope.aitemsPerPage,
            name:$scope.reuipmentbl1.name,
            r_name:$scope.reuipmentbl1.r_name,
        };
        $resource('/api/ac/ggc/equipmentBacklogService/findBlList', {}, {}).
			save(para, function(res){
                if(res.errcode === 0 || res.errcode === 10003){
                     $scope.a=res.data.results;
                     for(var i=0;i<res.data.results.length;i++){
                        if(res.data.results[i].b_submit_person=="旅游局"){
                            $scope.a[i].b_last_main=res.data.results[i].inspection_data;
                        }
                     };
                     $scope.atotalItems=res.data.totalRecord;
                     for(var i=0;i<$scope.a.length;i++){
                         $scope.a[i].last_data=date2str2(new Date((new Date($scope.a[i].b_last_main)/1000+86400*$scope.a[i].b_maintenance_cycle)*1000));
                         var str=$scope.a[i].b_name;
                         var arr = str.split("");
                         if(arr[str.length-4]=='1'){
                            $scope.a[i].next_remind=date2str2(new Date((new Date($scope.a[i].b_last_main)/1000+86400*($scope.a[i].b_maintenance_cycle-15))*1000));
                         }
                         if(arr[str.length-4]=='2'){
                            $scope.a[i].next_remind=date2str2(new Date((new Date($scope.a[i].b_last_main)/1000+86400*($scope.a[i].b_maintenance_cycle-0))*1000));
                         }
                         if(arr[str.length-4]>='3'){
                             var day3 = new Date();
                             day3.setTime(day3.getTime()+24*60*60*1000);
                             var s3 = day3.getFullYear()+"-" + (day3.getMonth()+1) + "-" + day3.getDate();
                            $scope.a[i].next_remind=s3;
                         }                                                                 
                                // $scope.a[i].frist_remind=date2str2(new Date((new Date($scope.a[i].inspection_data)/1000+86400*($scope.a[i].maintenance_cycle-30))*1000));      
                                // $scope.a[i].third_remind=date2str2(new Date((new Date($scope.a[i].inspection_data)/1000+86400*$scope.a[i].maintenance_cycle)*1000));
                        }
                }else{
                    alert(res.errmsg);
                }
            });

    };
    $scope.pageChanged1 = function () {
       var para = {
            pageNo:$scope.bcurrentPage,
            pageSize:$scope.bitemsPerPage,
            name:$scope.reuipmentbl2.name,
            r_name:$scope.reuipmentbl2.r_name,
        };
        $resource('/api/ac/ggc/equipmentBacklogService/findBlDoneList', {}, {}).
			save(para, function(res){
                if(res.errcode === 0 || res.errcode === 10003){
                    $scope.b=res.data.results
                    $scope.btotalItems=res.data.totalRecord;
                    for(var i=0;i<$scope.a.length;i++){
                         $scope.a[i].last_data=date2str2(new Date((new Date($scope.a[i].inspection_data)/1000+86400*$scope.a[i].maintenance_cycle)*1000));
                         var str=$scope.a[i].b_name;
                         var arr = str.split("");
                         if(arr[str.length-4]=='1'){
                            $scope.a[i].next_remind=date2str2(new Date((new Date($scope.a[i].inspection_data)/1000+86400*($scope.a[i].maintenance_cycle-15))*1000));
                         }
                         if(arr[str.length-4]=='2'){
                            $scope.a[i].next_remind=date2str2(new Date((new Date($scope.a[i].inspection_data)/1000+86400*($scope.a[i].maintenance_cycle-0))*1000));
                         }
                         if(arr[str.length-4]>='3'){
                             var day3 = new Date();
                             day3.setTime(day3.getTime()+24*60*60*1000);
                             var s3 = day3.getFullYear()+"-" + (day3.getMonth()+1) + "-" + day3.getDate();
                            $scope.a[i].next_remind=s3;
                         }                                                                 
                                // $scope.a[i].frist_remind=date2str2(new Date((new Date($scope.a[i].inspection_data)/1000+86400*($scope.a[i].maintenance_cycle-30))*1000));      
                                // $scope.a[i].third_remind=date2str2(new Date((new Date($scope.a[i].inspection_data)/1000+86400*$scope.a[i].maintenance_cycle)*1000));
                        }
                }else{
                    alert(res.errmsg);
                }
            });

    };
	 $scope.pageChanged();
     $scope.pageChanged1();
			$scope.search1=function(){
            var dic = {
                pageNo:$scope.acurrentPage,
                pageSize:$scope.aitemsPerPage,
                name:$scope.reuipmentbl1.name,
                r_name:$scope.reuipmentbl1.r_name,
			}
		    $resource('/api/ac/ggc/equipmentBacklogService/findBlList', {}, {}).
			save(dic, function(res){
                if(res.errcode === 0 || res.errcode === 10003){
                    $scope.a=res.data.results
                    $scope.atotalItems=res.data.totalRecord;
                    for(var i=0;i<$scope.a.length;i++){
                         $scope.a[i].last_data=date2str2(new Date((new Date($scope.a[i].inspection_data)/1000+86400*$scope.a[i].maintenance_cycle)*1000));
                         var str=$scope.a[i].b_name;
                         var arr = str.split("");
                         if(arr[str.length-4]=='1'){
                            $scope.a[i].next_remind=date2str2(new Date((new Date($scope.a[i].inspection_data)/1000+86400*($scope.a[i].maintenance_cycle-15))*1000));
                         }
                         if(arr[str.length-4]=='2'){
                            $scope.a[i].next_remind=date2str2(new Date((new Date($scope.a[i].inspection_data)/1000+86400*($scope.a[i].maintenance_cycle-0))*1000));
                         }
                         if(arr[str.length-4]>='3'){
                             var day3 = new Date();
                             day3.setTime(day3.getTime()+24*60*60*1000);
                             var s3 = day3.getFullYear()+"-" + (day3.getMonth()+1) + "-" + day3.getDate();
                            $scope.a[i].next_remind=s3;
                         }                                                                 
                                // $scope.a[i].frist_remind=date2str2(new Date((new Date($scope.a[i].inspection_data)/1000+86400*($scope.a[i].maintenance_cycle-30))*1000));      
                                // $scope.a[i].third_remind=date2str2(new Date((new Date($scope.a[i].inspection_data)/1000+86400*$scope.a[i].maintenance_cycle)*1000));
                        }
                }else{
                    alert(res.errmsg);
                }
            });
			}
            $scope.search2=function(){
            var dic = {
                pageNo:$scope.bcurrentPage,
                pageSize:$scope.bitemsPerPage,
                name:$scope.reuipmentbl2.name,
                r_name:$scope.reuipmentbl2.r_name,
			}
				 $resource('/api/ac/ggc/equipmentBacklogService/findBlDoneList', {}, {}).
			save(dic, function(res){
                if(res.errcode === 0 || res.errcode === 10003){
                    $scope.b=res.data.results
                    $scope.btotalItems=res.data.totalRecord;
                    // for(var i=0;i<$scope.a.length;i++){
                    //      $scope.b[i].last_data=date2str2(new Date((new Date($scope.b[i].inspection_data)/1000+86400*$scope.b[i].maintenance_cycle)*1000));
                    //      var str=$scope.b[i].b_name;
                    //      var arr = str.split("");
                    //      if(arr[str.length-4]=='1'){
                    //         $scope.b[i].next_remind=date2str2(new Date((new Date($scope.b[i].inspection_data)/1000+86400*($scope.b[i].maintenance_cycle-15))*1000));
                    //      }
                    //      if(arr[str.length-4]=='2'){
                    //         $scope.b[i].next_remind=date2str2(new Date((new Date($scope.b[i].inspection_data)/1000+86400*($scope.b[i].maintenance_cycle-0))*1000));
                    //      }
                    //      if(arr[str.length-4]>='3'){
                    //          var day3 = new Date();
                    //          day3.setTime(day3.getTime()+24*60*60*1000);
                    //          var s3 = day3.getFullYear()+"-" + (day3.getMonth()+1) + "-" + day3.getDate();
                    //         $scope.b[i].next_remind=s3;
                    //      }                                                                 
                    //             // $scope.a[i].frist_remind=date2str2(new Date((new Date($scope.a[i].inspection_data)/1000+86400*($scope.a[i].maintenance_cycle-30))*1000));      
                    //             // $scope.a[i].third_remind=date2str2(new Date((new Date($scope.a[i].inspection_data)/1000+86400*$scope.a[i].maintenance_cycle)*1000));
                    //     }
                }else{
                    alert(res.errmsg);
                }
            });
			}
    //维护
		$scope.ado= function (info) {          
            if(!confirm("是否确处理")){
                return ;
            }
            //alert(info.id);
            $resource('/api/ac/ggc/equipmentBacklogService/updateBlJStatus', {}, {}).save({id:info.b_id}, function (res) {
                 if (res.errcode === 0) {
                     alert('处理成功');
                    $scope.pageChanged();
                    $scope.pageChanged1();
                } else {
                    alert(res.errmsg);
                }
            });
        };        
	//待办详情
    $scope.adetails=function(info){
        var modalInstance = $modal.open({
                    template: require('../views/reuipmentDetails.html'),
                    controller: 'euipmentBacklogDetails',
                    size: 'lg',
                    resolve: {
                        items: function () {
                            return info;
                        }
                    }
                });
                modalInstance.opened.then(function () {
                });
                modalInstance.result.then(function (showResult) {
                }, function (reason) {
                    // click，点击取消，则会暑促cancel
                    $log.info('Modal dismissed at: ' + new Date());
                });
    }
    //已办详情
    $scope.bdetails=function(info){
        var modalInstance = $modal.open({
                    template: require('../views/reuipmentDetails.html'),
                    controller: 'euipmentBacklogDetails',
                    size: 'lg',
                    resolve: {
                        items: function () {
                            return info;
                        }
                    }
                });
                modalInstance.opened.then(function () {
                });
                modalInstance.result.then(function (showResult) {
                }, function (reason) {
                    // click，点击取消，则会暑促cancel
                    $log.info('Modal dismissed at: ' + new Date());
                });
    }	
}