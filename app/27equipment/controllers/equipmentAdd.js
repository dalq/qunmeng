module.exports = function($scope,$resource,$state){
	 $scope.equipment={};
	 $scope.equipment.responsible_person="00";
	 $scope.equipment.certificate="0";
	 $scope.equipment.types=[{ id:"00",name:"未选择"}];	 
		$resource('/api/ac/ggc/equipmentPerManageService/findAllEquPer', {}, {}).save({}, function(res){
			if(res.errcode==0){	
				for(var i = 0;i < res.data.list.length; i++){
					$scope.equipment.types.push(res.data.list[i]);
				}	
			}else{
				alert(res.errmsg);
			}
		});
		$scope.equipment.perphone=function(){
			$resource('/api/ac/ggc/equipmentPerManageService/findAllEquPer', {}, {}).save({id:$scope.equipment.responsible_person}, function(res){
			if(res.errcode==0){
				$scope.equipment.showmobi=true;	
				$scope.equipment.showphone=res.data.list[0].mobile;
			}else{
				alert(res.errmsg);
			}
		});
		}	
	//用户名非空验证
	$scope.equipment.nameadd=function(){
    $scope.equipment.namecheck=false;
	}
	$scope.equipment.nameblur=function(){
		if ($scope.equipment.name==undefined || $scope.equipment.name==""){
					$scope.equipment.namecheck=true;
				}
	}
	//风险点详细位置非空验证
	$scope.equipment.locationadd=function(){
    $scope.equipment.locationcheck=false;
	}
	$scope.equipment.locationblur=function(){
		if ($scope.equipment.location==undefined || $scope.equipment.location==""){
					$scope.equipment.locationcheck=true;
				}
	}
	//生产厂家非空验证
	$scope.equipment.manufactureradd=function(){
    $scope.equipment.manufacturercheck=false;
	}
	$scope.equipment.manufacturerblur=function(){
		if ($scope.equipment.manufacturer==undefined || $scope.equipment.manufacturer==""){
					$scope.equipment.manufacturercheck=true;
				}
	}
	//生产日期非空验证
	$scope.equipment.production_dataadd=function(){
    $scope.equipment.production_datacheck=false;
	}
	$scope.equipment.production_datablur=function(){
		if ($scope.equipment.production_data==undefined || $scope.equipment.production_data==""){
					$scope.equipment.production_datacheck=true;
				}
	}
	//检验单位非空验证
	$scope.equipment.inspection_unitadd=function(){
    $scope.equipment.inspection_unitcheck=false;
	}
	$scope.equipment.inspection_unitblur=function(){
		if ($scope.equipment.inspection_unit==undefined || $scope.equipment.inspection_unit==""){
					$scope.equipment.inspection_unitcheck=true;
				}
	}
	//检验日期非空验证
	$scope.equipment.inspection_dataadd=function(){
    $scope.equipment.inspection_datacheck=false;
	}
	$scope.equipment.inspection_datablur=function(){
		if ($scope.equipment.inspection_data==undefined || $scope.equipment.inspection_data==""){
					$scope.equipment.inspection_datacheck=true;
				}
	}
	//检查周期非空验证
	$scope.equipment.cycleadd=function(){	
    $scope.equipment.cyclecheck=false;
	}
	$scope.equipment.cycleblur=function(){
		if ($scope.equipment.maintenance_cycle==undefined || $scope.equipment.maintenance_cycle==""){
					$scope.equipment.cyclecheck=true;
				}
	}
	//责任部门非空验证
	$scope.equipment.control_departmentadd=function(){
    $scope.equipment.control_departmentcheck=false;
	}
	$scope.equipment.control_departmentblur=function(){
		if ($scope.equipment.control_department==undefined || $scope.equipment.control_department==""){
					$scope.equipment.inspecontrol_department=true;
				}
	}
	//安全非空验证
	$scope.equipment.safe_departmentadd=function(){	
    $scope.equipment.safe_departmentcheck=false;
	}
	$scope.equipment.safe_departmentblur=function(){
		if ($scope.equipment.safe_department==undefined || $scope.equipment.safe_department==""){
					$scope.equipment.safe_departmentcheck=true;
				}
	}
	//主管非空验证
	$scope.equipment.director_departmentadd=function(){	
    $scope.equipment.director_departmentcheck=false;
	}
	$scope.equipment.director_departmentblur=function(){
		if ($scope.equipment.director_department==undefined || $scope.equipment.director_department==""){
					$scope.equipment.director_departmentcheck=true;
				}
	}
			$scope.date = {
						// 'lable': date2str2(new Date()),
						'value': date2str(new Date()),
						'opened': false
					}
             $scope.dateOpen = function ($event, item) {
                $event.preventDefault();
                $event.stopPropagation();
                item.opened = true;
            };

            $scope.date1 = {
                // 'lable': date2str2(new Date()),
                'value': date2str(new Date()),
                'opened': false
            }


             $scope.dateOpen1 = function ($event, item) {
                $event.preventDefault();
                $event.stopPropagation();
                item.opened = true;
            };
			function date2str(d) {
                if (d === undefined) {
                    return "";
                }
                var month = (d.getMonth() + 1).toString();
                var day = d.getDate().toString();
                if (month.length == 1) month = '0' + month;
                if (day.length == 1) day = '0' + day;
                return d.getFullYear() + month + day;
            }
			function date2str2(d) {
				if (d === undefined) {
					return "";
				}
				var month = (d.getMonth() + 1).toString();
				var day = d.getDate().toString();
				if (month.length == 1) month = '0' + month;
				if (day.length == 1) day = '0' + day;
				return d.getFullYear() + "-" + month + "-" + day;
				// return d.getFullYear() + month + day;
			}	
	$scope.submit=function(){
		$scope.equipment.production_data=date2str2($scope.date.lable);
		$scope.equipment.inspection_data=date2str2($scope.date1.lable);
		//提交校验
	   if($scope.equipment.name==undefined || $scope.equipment.name==""){
			$scope.equipment.namecheck=true;
			return;
	   }
	   if($scope.equipment.maintenance_cycle==undefined || $scope.equipment.maintenance_cycle==""){
			$scope.equipment.cyclecheck=true;
			return;
	   }
	   if($scope.equipment.manufacturer==undefined || $scope.equipment.manufacturer==""){
			$scope.equipment.manufacturercheck=true;
			return;
	   }
	   if($scope.equipment.production_data==undefined || $scope.equipment.production_data==""){
			$scope.equipment.production_datacheck=true;
			return;
	   }
	   if($scope.equipment.inspection_unit==undefined || $scope.equipment.inspection_unit==""){
			$scope.equipment.inspection_unitcheck=true;
			return;
	   }
	   if($scope.equipment.inspection_data==undefined || $scope.equipment.inspection_data==""){
			$scope.equipment.inspection_datacheck=true;
			return;
	   }
	   if($scope.equipment.control_department==undefined || $scope.equipment.control_department==""){
			$scope.equipment.control_departmentcheck=true;
			return;
	   }
	   if($scope.equipment.safe_department==undefined || $scope.equipment.safe_department==""){
			$scope.equipment.safe_departmentcheck=true;
			return;
	   }
	   if($scope.equipment.director_department==undefined || $scope.equipment.director_department==""){
			$scope.equipment.director_departmentcheck=true;
			return;
	   }		
		
		
		// var savedata={};
		// savedata.name=$scope.equipment.name;
		// savedata.maintenance_cycle=$scope.equipment.maintenance_cycle;
		// savedata.submit_person=$scope.equipment.submit_person;

		$resource('/api/ac/ggc/equipmentManageService/creat', {}, {}).save($scope.equipment, function(res){
			if(res.errcode==0){
				if(confirm("是否继续添加")){
					$state.go("app.equipmentAdd",{}, {reload: true});
				}else{
					$state.go("app.equipmentManage",{}, {reload: true});
				}				
			}else{
				alert(res.errmsg);
			}
    	})

		};	
};