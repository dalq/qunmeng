


// module.exports = function ($scope, $state, $stateParams,$resource,$modal) {
//     $scope.create=function(){
//         var modalInstance = $modal.open({
// 					template: require('../views/createaccount.html'),
// 					controller: 'createaccount',
// 					size: 'lg',
// 					resolve: {
// 					}
// 				});
//     }
// }










/**
 * 模态框
 */
module.exports = function ($scope, $state, $stateParams,$resource,$modal) {
	/* 分页
     * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.currentPage = 1;      //当前页码
    $scope.itemsPerPage = 10 ;        //每页显示几条
    $scope.reuipment_person_manage={};
    $scope.reuipment_person_manage.name="";   
	$scope.pageChanged = function () {
       var para = {
            pageNo:$scope.currentPage,
            pageSize:$scope.itemsPerPage,
            name:$scope.reuipment_person_manage.name
        };
        $resource('/api/ac/ggc/equipmentPerManageService/findEquPerList', {}, {}).
			save(para, function(res){				
                if(res.errcode === 0 || res.errcode === 10003){
					$scope.a=res.data.results;
					$scope.totalItems = res.data.totalRecord;
                    //alert(res.data.results.length);
                   
                }else{
                    alert(res.errmsg);
                }
            });

    };
	 $scope.pageChanged();

			$scope.reuipment_person_manage.search=function(){
            var dic = {
                pageNo:$scope.currentPage,
                pageSize:$scope.itemsPerPage,
                name:$scope.reuipment_person_manage.name
			}
				 $resource('/api/ac/ggc/equipmentPerManageService/findEquPerList', {}, {}).
				save(dic, function(res){				
                if(res.errcode === 0 || res.errcode === 10003){
					$scope.a=res.data.results;
					$scope.totalItems = res.data.totalRecord;                  
                }else{
                    alert(res.errmsg);
                }
            });
			}
			//添加
			$scope.reuipment_person_manage.add = function () {
				var modalInstance = $modal.open({
					template: require('../views/EquipmentAddPerson.html'),
					controller: 'equipmentAddPerson',
					size: 'lg',
					resolve: {
					}
				});
				modalInstance.result.then(function (showResult) {
						$scope.pageChanged();
				}, function (reason) {
					$scope.pageChanged();
				});
			}
			//删除
			// $scope.reuipment_person_manage.delete= function (info) {
            //     if(!confirm("是否确认删除")){
            //         return ;
            //     }
            //     //alert($scope.itemw.code);
            //     $resource('/api/ac/ggc/equipmentPerManageService/delete', {}, {}).save({ id: info.id }, function (res) {
            //         if (res.errcode === 0) {
            //             alert('删除成功');
            //             $scope.pageChanged();
            //         } else {
            //             alert(res.errmsg);
            //         }
            //     });

            // };
			//修改
    		$scope.reuipment_person_manage.edit = function (info) {
				var modalInstance = $modal.open({
					template: require('../views/EquipmentUpdatePerson.html'),
					controller: 'equipmentUpdatePerson',
					size: 'lg',
					resolve: {
						items: function () {
                            return info;
                        }
					}
				});
				modalInstance.result.then(function (showResult) {
						$scope.pageChanged();
				});
			}                   
};
    
