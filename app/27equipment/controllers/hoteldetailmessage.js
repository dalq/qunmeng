module.exports = function($scope, $state, $stateParams,$resource,$modal){
    $scope.searchform = {};
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.currentPage = 1;      //当前页码
    $scope.itemsPerPage = 10        //每页显示几条

      

        var geid=$stateParams.geid;
    
          
	 	$scope.pageChanged = function () {
	       var para = {
	        //    ta:1,
	            pageNo:$scope.currentPage,
	            pageSize:$scope.itemsPerPage,
	            gucode:geid

	        };
	        para = angular.extend($scope.searchform, para);
	        $resource('/api/ac/ggc/equipmentManageDetailInformationService/findCompanyInformationList', {}, {}).
	        save(para, function(res){
	                if(res.errcode === 0 || res.errcode === 10003){
	                	 $scope.objlist={};
	                	    $scope.objlist=res.data.results;
	                	    $scope.totalItems = res.data.totalRecord;

   	                	    if($scope.totalItems===0){
   	                	    	$scope.tishi='温馨提示：该单位还没有风险点检验的数据';
   	                	    }
   	                	     if(res.data.results.length!=0 || res.data.results !=undefined ||res.data.results !=''){
	                	    	  $scope.cname=res.data.results[0].guname;
	                	    	   $scope.fr=res.data.results[0].gurp;
	                	    	    $scope.frn=res.data.results[0].gurpp;
	                	    	    $scope.frts='（法人：';
	                	    	    $scope.dh='；电话：';
	                	    	    $scope.kh='）';
	                	    	    for(var i=0;i<res.data.results.length;i++){
	                	    	    	 if(res.data.results[i].num>=0){
	                	    	    	 	res.data.results[i].bz='在检验期内';

	                	    	    	 }else{
	                	    	    	 	
	                	    	    	 	res.data.results[i].bz1='已超期,请到待办事项中完成电话确认';
	                	    	    	 }
   
	                	    	    }
   	                	    }
	                     
	                }else{
	                    alert(res.errmsg);
	                }
	            });

	    };
     	$scope.pageChanged();
	
	$scope.searchform1 = {};
    $scope.maxSize1 = 5;             //最多显示多少个按钮
    $scope.currentPage1 = 1;      //当前页码
    $scope.itemsPerPage1 = 10        //每页显示几条
        $scope.pageChanged1 = function () {
	       var para = {
	        //    ta:1,
	            pageNo:$scope.currentPage1,
	            pageSize:$scope.itemsPerPage1,
	            companycode:geid

	        };
	        para = angular.extend($scope.searchform1, para);
	        $resource('/api/ac/ggc/equipmentManageDetailInformationService/findYanqiInformationList', {}, {}).
	        save(para, function(res){
	                if(res.errcode === 0 || res.errcode === 10003){
	                	 
	                	    $scope.yanqilist=res.data.results;
	                	    $scope.totalItems1 = res.data.totalRecord;
	                     
                          
	                }else{
	                    alert(res.errmsg);
	                }
	            });

	    };
     	$scope.pageChanged1();
     $scope.returnHome=function(){
     	// $state.go('app.equipmentMessage',{},{reload: true });
     	$state.go('app.equipmentHotelMessage');
    }

      $scope.infor=function(item){
           var modalInstance = $modal.open({
                  template: require('../views/getinformation.html'),
                  controller:'getinformation',
                  url: '/getinformation',
                  size: 'lg',
                  resolve: {
                   
                     item: function () {
                            return item;
                    }
                  }
                
                });

                  modalInstance.opened.then(function() {// 模态窗口打开之后执行的函数
                });
                modalInstance.result.then(function(result) {
                 }, function(reason) {
                    //$state.go('app.equipmentUserManage',{},{reload: true});// 点击空白区域，总会输出backdrop
                     // click，点击取消，则会暑促cancel
                     // $log.info('Modal dismissed at: ' + new Date());
                 });

                
                     
           
       }


     $scope.nopass=function(info){
         if(confirm("确定要驳回此次检验记录吗?")){
         	$resource('/api/ac/ggc/equipmentBacklogService/updateReject', {}, {}).
		      save({'id':info.geid}, function(res){

		      	    if(res.errcode === 0 || res.errcode === 10003){
	                                   
	                      alert("驳回成功!");
	                      $scope.pageChanged();
	                       // $state.go('app.scenicinspectionrecord',{},{reload: true});

	                }else{
	                    alert(res.errmsg);


	                }

		      })
	       }
	      
 
       }
}
