

module.exports = function ($scope,$state, $resource,$stateParams,itemparam,item,$http,$modalInstance,mydate2str) {


    $scope.user1 = itemparam;
    $scope.obj = {};
    
    $scope.objs = [];
    var idobj = {};

    $scope.maxSize = 5;            //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 200000;         //每页显示几条
    var para = {
            pageNo:$scope.bigCurrentPage, 
            pageSize:$scope.itemsPerPage,
            'company.id' : item.id
        };

    $resource('/a/sys/user/ajaxlist', {}, {}).
      get(para, function(res){ 
         
            
            if(res.list.length!==0 || res.list!==''){
                for(var i=0;i<res.list.length;i++){
                      if(res.list[i].loginName==item.code){
                            
                              item.id=res.list[i].id;

                                 $resource('/a/sys/user/ajaxform', {'id':item.id}, {}).get({}, {}, function(res){
                                       
                                        $scope.objs = res.allRoles;
                                        $scope.getroles=res.allRoles;
                                       
                                          $scope.obj = res.user;
                                            
                                            $scope.obj.oldLoginName = $scope.obj.loginName;
                                            $scope.obj.roleIdList = [];
                                            $scope.obj['company.id'] = res.company.id;
                                            $scope.obj['office.id'] = res.office.id; 

                                          

                                            //角色选择
                                            for(var i = 0; i < $scope.objs.length; i++)
                                            {
                                                var tmp = $scope.objs[i];
                                                tmp.iselected = 0;
                                                for(var j = 0; j < res.roleIdList.length; j++)
                                                {
                                                    if(res.roleIdList[j] == tmp.id)
                                                    {
                                                        tmp.iselected = 1;
                                                        idobj[tmp.id] = '';
                                                        break;
                                                    }
                                                }

                                            }


                                })
                      }
                }
            }
      })

              

       $scope.selection = function($event, obj){
           
            var checkbox = $event.target;
            
            if(checkbox.checked)
            {
                idobj[obj.id] = '';
            }
            else
            {
               delete idobj[obj.id];
            }

    };

      $scope.ok = function(){

       if(confirm("提示：请最后确认勾选的角色，是否重新分配角色？")){

                    $scope.obj.roleIdList = [];

                    angular.forEach(idobj, function (value, key) {
                        $scope.obj.roleIdList.push(key);
                    });

                    if($scope.obj.roleIdList.length === 0)
                    {
                        alert('请选择角色');
                        return;
                    }
                    $scope.obj.remark='';
                    $scope.obj.phone='';
                    $scope.obj.loginName=$scope.user1.loginname;
                     $scope.obj.name=$scope.user1.loginname;
                     $scope.obj.id = item.id;
                    //  $scope.obj.phone=$scope.user1.responsible_person_phone;
                    // $scope.obj.loginName=$scope.user1.loginname;
                    //  $scope.obj.name=$scope.user1.responsible_person;
                    //  $scope.obj.id = item.id;


                   

                     $resource('/a/sys/user/ajaxsave', {}, {}).save($scope.obj, {}, function(res){
                       // alert("9999");
                       
                        if(res.errcode===0){
                            alert("角色分配成功！");
                            var bh="5b49479a7cd34283abcad19bcb2f203d";
                            for(var i=0; i<$scope.obj.roleIdList.length; i++){
                                if($scope.obj.roleIdList[i]===bh){
                               
                                    $state.go('app.equipmentUserManage',{},{reload: true});
                                    $modalInstance.dismiss('cancel');
                                    return;   

                                }
                            }
 
                                     $state.go('app.equipmentPersonManage',{},{reload: true});
                                     $modalInstance.dismiss('cacnel');
                                
                     
                        }else{
                          alert("修改失败");
                            $modalInstance.dismiss('cacnel');

                        }
                      
                    });
            }


    };


        
}
