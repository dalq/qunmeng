module.exports = function($scope,$resource){
    var d = new Date();
    var dy=d.getFullYear();
    $scope.years=[];
    $scope.yearT=dy;
    $scope.yearS=dy;
    $scope.yearH=dy;
    for(var i=0;i<=50;i++){
                var cy=dy+i;               
                $scope.years.push({code:cy,value:cy})
    }
     $resource('/api/ac/ggc/equipmentMainRecoService/selectOneToTwelve', {}, {}).save({year:dy}, function (res) {
                 if (res.errcode === 0) {
                    // 总维护数
                    $scope.labelsT = ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'];
                    $scope.seriesT = ['维护次数'];
                    $scope.dataT = [
                        [res.data.h1[0]+res.data.s1[0],res.data.h2[0]+res.data.s2[0],res.data.h3[0]+res.data.s3[0],res.data.h4[0]+res.data.s4[0],
                         res.data.h5[0]+res.data.s5[0], res.data.h6[0]+res.data.s6[0], res.data.h7[0]+res.data.s7[0],res.data.h8[0]+res.data.s8[0]
                         , res.data.h9[0]+res.data.s9[0], res.data.h10[0]+res.data.s10[0],res.data.h11[0]+res.data.s11[0], res.data.h12[0]+res.data.s12[0] ]
                         ];
                   $scope.equidonenumT=res.data.h1[0]+res.data.s1[0]+res.data.h2[0]+res.data.s2[0]+res.data.h3[0]+res.data.s3[0]+res.data.h4[0]+res.data.s4[0]+
                         res.data.h5[0]+res.data.s5[0]+ res.data.h6[0]+res.data.s6[0]+ res.data.h7[0]+res.data.s7[0]+res.data.h8[0]+res.data.s8[0]
                         + res.data.h9[0]+res.data.s9[0]+ res.data.h10[0]+res.data.s10[0]+res.data.h11[0]+res.data.s11[0]+ res.data.h12[0]+res.data.s12[0]      
                   // 酒店维护数
                    $scope.labelsH = ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'];
                    $scope.seriesH = ['维护次数'];
                    $scope.dataH = [
                        [res.data.h1[0],res.data.h2[0],res.data.h3[0],res.data.h4[0],
                         res.data.h5[0], res.data.h6[0], res.data.h7[0],res.data.h8[0]
                         , res.data.h9[0], res.data.h10[0],res.data.h11[0], res.data.h12[0] ]
                         ];
                   $scope.equidonenumH=res.data.h1[0]+res.data.h2[0]+res.data.h3[0]+res.data.h4[0]+
                         res.data.h5[0]+ res.data.h6[0]+ res.data.h7[0]+res.data.h8[0]
                         + res.data.h9[0]+ res.data.h10[0]+res.data.h11[0]+ res.data.h12[0] 
                    // 景区维护数
                    $scope.labelsS = ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'];
                    $scope.seriesS = ['维护次数'];
                    $scope.dataS = [
                        [res.data.s1[0],res.data.s2[0],res.data.s3[0],res.data.s4[0],
                         res.data.s5[0], res.data.s6[0], res.data.s7[0],res.data.s8[0]
                         , res.data.s9[0], res.data.s10[0],res.data.s11[0], res.data.s12[0] ]
                         ];
                   $scope.equidonenumS=res.data.s1[0]+res.data.s2[0]+res.data.s3[0]+res.data.s4[0]+
                         res.data.s5[0]+ res.data.s6[0]+ res.data.s7[0]+res.data.s8[0]
                         + res.data.s9[0]+ res.data.s10[0]+res.data.s11[0]+ res.data.s12[0];
                          
            } else {
                    alert(res.errmsg);
                }
            });
 
        $scope.searchT=function(){
            $resource('/api/ac/ggc/equipmentMainRecoService/selectOneToTwelve', {}, {}).save({year:$scope.yearT}, function (res) {            
                 if (res.errcode === 0) {
                    // 总维护数
                    $scope.labelsT = ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'];
                    $scope.seriesT = ['维护次数'];
                    $scope.dataT = [
                        [res.data.h1[0]+res.data.s1[0],res.data.h2[0]+res.data.s2[0],res.data.h3[0]+res.data.s3[0],res.data.h4[0]+res.data.s4[0],
                         res.data.h5[0]+res.data.s5[0], res.data.h6[0]+res.data.s6[0], res.data.h7[0]+res.data.s7[0],res.data.h8[0]+res.data.s8[0]
                         , res.data.h9[0]+res.data.s9[0], res.data.h10[0]+res.data.s10[0],res.data.h11[0]+res.data.s11[0], res.data.h12[0]+res.data.s12[0] ]
                         ];
                   $scope.equidonenumT=res.data.h1[0]+res.data.s1[0]+res.data.h2[0]+res.data.s2[0]+res.data.h3[0]+res.data.s3[0]+res.data.h4[0]+res.data.s4[0]+
                         res.data.h5[0]+res.data.s5[0]+ res.data.h6[0]+res.data.s6[0]+ res.data.h7[0]+res.data.s7[0]+res.data.h8[0]+res.data.s8[0]
                         + res.data.h9[0]+res.data.s9[0]+ res.data.h10[0]+res.data.s10[0]+res.data.h11[0]+res.data.s11[0]+ res.data.h12[0]+res.data.s12[0]            
            } else {
                    alert(res.errmsg);
                };
            });
        };
        $scope.searchH=function(){
            $resource('/api/ac/ggc/equipmentMainRecoService/selectOneToTwelve', {}, {}).save({year:$scope.yearH}, function (res) {            
                 if (res.errcode === 0) {
                    // 酒店维护数
                    $scope.labelsH = ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'];
                    $scope.seriesH = ['维护次数'];
                    $scope.dataH = [
                        [res.data.h1[0],res.data.h2[0],res.data.h3[0],res.data.h4[0],
                         res.data.h5[0], res.data.h6[0], res.data.h7[0],res.data.h8[0]
                         , res.data.h9[0], res.data.h10[0],res.data.h11[0], res.data.h12[0] ]
                         ];
                   $scope.equidonenumH=res.data.h1[0]+res.data.h2[0]+res.data.h3[0]+res.data.h4[0]+
                         res.data.h5[0]+ res.data.h6[0]+ res.data.h7[0]+res.data.h8[0]
                         + res.data.h9[0]+ res.data.h10[0]+res.data.h11[0]+ res.data.h12[0]            
            } else {
                    alert(res.errmsg);
                };
            });
        };
        $scope.searchS=function(){
            $resource('/api/ac/ggc/equipmentMainRecoService/selectOneToTwelve', {}, {}).save({year:$scope.yearS}, function (res) {            
                 if (res.errcode === 0) {
                    // 景区维护数
                    $scope.labelsS = ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'];
                    $scope.seriesS = ['维护次数'];
                    $scope.dataS = [
                        [res.data.s1[0],res.data.s2[0],res.data.s3[0],res.data.s4[0],
                         res.data.s5[0], res.data.s6[0], res.data.s7[0],res.data.s8[0]
                         , res.data.s9[0], res.data.s10[0],res.data.s11[0], res.data.s12[0] ]
                         ];
                   $scope.equidonenumS=res.data.s1[0]+res.data.s2[0]+res.data.s3[0]+res.data.s4[0]+
                         res.data.s5[0]+ res.data.s6[0]+res.data.s7[0]+res.data.s8[0]
                         +res.data.s9[0]+ res.data.s10[0]+res.data.s11[0]+ res.data.s12[0]
                                
            } else {
                    alert(res.errmsg);
                };
            }); 
        };
        //市单位总数
        $resource('/api/ac/ggc/equipmentUserManageService/findCountInformation', {}, {}).save({}, function (res) {
                if (res.errcode === 0) {               
                 $scope.counts=res.data;
                 $scope.counts.ScenicUsercount1=parseInt(res.data.ScenicUsercount1);
                 $scope.counts.HotelUsercount1=parseInt(res.data.HotelUsercount1); 
                 $scope.counts.ScenicEquipmentCount1=parseInt(res.data.ScenicEquipmentCount1); 
                 $scope.counts.HotelEquipmentCount1=parseInt(res.data.HotelEquipmentCount1);                 
                } else {
                    alert(res.errmsg);
                };
            });
        //市设备总数
        // $resource('/api/ac/ggc/equipmentMainRecoService/selectequinum', {}, {}).save({}, function (res) {
        //          if (res.errcode === 0) {
        //             $scope.equinum=res.data.num[0];
        //         } else {
        //             alert(res.errmsg);
        //         }
        //     });
            
        //饼图
        $resource('/api/ac/ggc/equipmentMainRecoService/selectbigtable', {}, {}).save({}, function (res) {
                 if (res.errcode === 0) {
                     //总
                      $scope.labeltst = ["景区准时维护","酒店准时维护", "景区未维护","酒店未维护", "景区延期维护", "酒店延期维护"];
                      $scope.datat = [res.data.donenums,res.data.donenumh,(res.data.equinums-res.data.donenums-res.data.latenums),(res.data.equinumh-res.data.donenumh-res.data.latenumh), 
                                      res.data.latenums,res.data.latenumh];
                      $scope.weihulvt=Math.round((res.data.donenums+res.data.donenumh+res.data.latenumh+res.data.latenums) /(res.data.equinums+res.data.equinumh) * 10000) / 100.00 + "%";
                      $scope.lateweihulvt=Math.round((res.data.latenumh+res.data.latenums) /(res.data.donenums+res.data.donenumh+res.data.latenumh+res.data.latenums)  * 10000) / 100.00 + "%";
                      //酒店
                       $scope.labeltsh = ["准时维护", "未维护", "延期维护"];
                      $scope.datah = [res.data.donenumh,res.data.equinumh-res.data.donenumh-res.data.latenumh, 
                                      res.data.latenumh];
                      $scope.weihulvh=Math.round((res.data.donenumh+res.data.latenumh) /(res.data.equinumh) * 10000) / 100.00 + "%";
                      $scope.lateweihulvh=Math.round((res.data.latenumh) /(res.data.donenumh+res.data.latenumh)  * 10000) / 100.00 + "%";
                      //景区
                      $scope.labeltss = ["准时维护", "未维护", "延期维护"];
                      $scope.datas = [res.data.donenums,res.data.equinums-res.data.donenums-res.data.latenums, 
                                      res.data.latenums];
                      $scope.weihulvs=Math.round((res.data.donenums+res.data.latenums) /(res.data.equinums) * 10000) / 100.00 + "%";
                      $scope.lateweihulvs=Math.round((res.data.latenums) /(res.data.donenums+res.data.latenums)  * 10000) / 100.00 + "%";
            } else {
                    alert(res.errmsg);
                };
            });

};