module.exports = function($scope,$resource,$modalInstance,$modal,items){
 /* 分页
     * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.currentPage = 1;      //当前页码
    $scope.itemsPerPage = 10 ;        //每页显示几条
    $scope.name=items.name;
    $scope.pageChanged = function () {
       var para = {
            pageNo:$scope.currentPage,
            pageSize:$scope.itemsPerPage,
            equipment_id:items.id
        };
        $resource('/api/ac/ggc/equipmentMainRecoService/findMainRecByIdList', {}, {}).
			save(para, function(res){				
                if(res.errcode === 0 || res.errcode === 10003){
					$scope.a=res.data.results;
					$scope.totalItems = res.data.totalRecord;                                          
                }else{
                    alert(res.errmsg);
                }
            });

    };
	 $scope.pageChanged(); 
     $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
    };
    $scope.showpic = function (info) {
                //alert("nihao");
               //$scope.item = $scope.a[index];
                var modalInstance = $modal.open({
                    template: require('../views/reuipmentmainpic.html'),
                    controller: 'reuipmentShowpic',
                    size: 'lg',
                    resolve: {
                        items: function () {
                            return info;
                        }
                    }
                });
                modalInstance.opened.then(function () {
                });
                modalInstance.result.then(function (showResult) {
                }, function (reason) {
                    // click，点击取消，则会暑促cancel
                    $log.info('Modal dismissed at: ' + new Date());
                });
    }   
}