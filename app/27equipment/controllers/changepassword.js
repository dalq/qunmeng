module.exports = function($scope,$resource){

	$scope.obj = {};
    
	$scope.gogo = function(){

		if($scope.obj.newPassword !== $scope.obj.newPassword2)
		{
			alert('两次输入的密码不一致。');
			return;
		}

		$resource('/a/sys/user/ajaxmodifyPwd', {}, {}).save($scope.obj, {}, function(res){
			if(res.errcode==0){				
				alert(修改成功);
				$scope.obj = {};
			}else{
				alert(res.message);
			}			
		});

	};

};