module.exports = function($scope,$state,$resource,$modal){
   
    /* 分页
     * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.currentPage = 1;      //当前页码
    $scope.itemsPerPage = 10 ;        //每页显示几条
    $scope.reuipmentmanage={};
    $scope.reuipmentmanage.name="";
    $scope.reuipmentmanage.maintenance_status="";
    $scope.reuipmentmanage.r_name="";
    function date2str2(d) {
				if (d === undefined) {
					return "";
				}
				var month = (d.getMonth() + 1).toString();
				var day = d.getDate().toString();
				if (month.length == 1) month = '0' + month;
				if (day.length == 1) day = '0' + day;
				return d.getFullYear() + "-" + month + "-" + day;
				// return d.getFullYear() + month + day;
	}  
	$scope.pageChanged = function () {
        $scope.reuipmentmanage.r_names=[{ id:"",name:"未选择"}];	 
		$resource('/api/ac/ggc/equipmentPerManageService/findAllEquPer', {}, {}).save({}, function(res){
			if(res.errcode==0){	
				for(var i = 0;i < res.data.list.length; i++){
					$scope.reuipmentmanage.r_names.push(res.data.list[i]);
				}	
			}else{
				alert(res.errmsg);
			}
		});
       var para = {
            pageNo:$scope.currentPage,
            pageSize:$scope.itemsPerPage,
            name:$scope.reuipmentmanage.name,
            maintenance_status:$scope.reuipmentmanage.maintenance_status,
            r_name:$scope.reuipmentmanage.r_name,
        };
        $resource('/api/ac/ggc/equipmentManageService/findEquipmentList', {}, {}).
			save(para, function(res){				
                if(res.errcode === 0 || res.errcode === 10003){
					$scope.a=res.data.results;
					$scope.totalItems = res.data.totalRecord;
                    //alert(res.data.results.length);                   
                        for(var i=0;i<$scope.a.length;i++){                          
                                if($scope.a[i].maintenance_status=="0"){
                                    $scope.a[i].maintenance_status="未维护"
                                    //$scope.a[i].staustt=true;
                                }                                
                                if($scope.a[i].maintenance_status=="1"){
                                    $scope.a[i].maintenance_status="已维护"
                                    //$scope.a[i].staustn=true;
                                }
         
                                $scope.a[i].reinspection_data=date2str2(new Date((new Date($scope.a[i].inspection_data)/1000+86400*$scope.a[i].maintenance_cycle)*1000));
                                $scope.a[i].frist_remind=date2str2(new Date((new Date($scope.a[i].inspection_data)/1000+86400*($scope.a[i].maintenance_cycle-30))*1000));
                                $scope.a[i].second_remind=date2str2(new Date((new Date($scope.a[i].inspection_data)/1000+86400*($scope.a[i].maintenance_cycle-15))*1000));
                                $scope.a[i].third_remind=date2str2(new Date((new Date($scope.a[i].inspection_data)/1000+86400*$scope.a[i].maintenance_cycle)*1000));
                        }                       
                }else{
                    alert(res.errmsg);
                }
            });

    };
	 $scope.pageChanged();

			$scope.reuipmentmanage.search=function(){
            var dic = {
                pageNo:$scope.currentPage,
                pageSize:$scope.itemsPerPage,
                name:$scope.reuipmentmanage.name,
                maintenance_status:$scope.reuipmentmanage.maintenance_status,
                r_name:$scope.reuipmentmanage.r_name,
			}
				 $resource('/api/ac/ggc/equipmentManageService/findEquipmentList', {}, {}).
				save(dic, function(res){				
                if(res.errcode === 0 || res.errcode === 10003){
					$scope.a=res.data.results;
					$scope.totalItems = res.data.totalRecord;
                    for(var i=0;i<$scope.a.length;i++){                          
                                if($scope.a[i].maintenance_status=="0"){
                                    $scope.a[i].maintenance_status="未维护"
                                     $scope.a[i].staustt=true;
                                }
                                if($scope.a[i].maintenance_status=="1"){
                                    $scope.a[i].maintenance_status="已维护"
                                    $scope.a[i].staustn=true;
                                }
                    }                   
                }else{
                    alert(res.errmsg);
                }
            });
			}
            
			//添加
			    $scope.reuipmentmanage.add=function(){                   
                    $state.go('app.equipmentAdd', { });
                }
			//删除
			$scope.reuipmentmanage.delete= function (info) {
                if(!confirm("是否确认删除")){
                    return ;
                }
                //alert($scope.itemw.code);
                $resource('/api/ac/ggc/equipmentManageService/delete', {}, {}).save({ id: info.id }, function (res) {
                    if (res.errcode === 0) {
                        alert('删除成功');
                        $scope.pageChanged();
                    } else {
                        alert(res.errmsg);
                    }
                });

            };
            //维护
			$scope.reuipmentmanage.status= function (info) {
                var modalInstance = $modal.open({
                    template: require('../views/reuipmentStatus.html'),
                    controller: 'reuipmentStatus',
                    size: 'lg',
                    resolve: {
                        items: function () {
                            return info;
                        }
                    }
                });
                modalInstance.opened.then(function () {
                });
                modalInstance.result.then(function (showResult) {
                }, function (reason) {
                    // click，点击取消，则会暑促cancel
                    $scope.pageChanged();
                    $log.info('Modal dismissed at: ' + new Date());
                });  
                //    var j = new Date();
                //    var today=date2str2(j);
                 
                //    var nextdate=info.reinspection_data;

                //   if(today<=nextdate){
                //       if(!confirm("是否确认维护")){
                //             return ;
                //         }
                //         //alert($scope.itemw.code);
                //         $resource('/api/ac/ggc/equipmentManageService/updateStatus', {}, {}).save({ id: info.id }, function (res) {
                //             if (res.errcode === 0) {
                //                 alert('维护成功');
                //                 $scope.pageChanged();
                //             } else {
                //                 alert(res.errmsg);
                //             }
                //         });

                //   }
                //    if(today>nextdate){
                //       if(!confirm("是否确认维护")){
                //             return ;
                //         }
                //         //alert($scope.itemw.code);
                //         $resource('/api/ac/ggc/equipmentManageService/updateStatu', {}, {}).save({ id: info.id }, function (res) {
                //             if (res.errcode === 0) {
                //                 alert('维护成功');
                //                 $scope.pageChanged();
                //             } else {
                //                 alert(res.errmsg);
                //             }
                //         });
                //   }
              
            };
            //详情
            $scope.reuipmentmanage.details = function (info) {
                //alert("nihao");
               //$scope.item = $scope.a[index];
                var modalInstance = $modal.open({
                    template: require('../views/reuipmentDetails.html'),
                    controller: 'reuipmentDetails',
                    size: 'lg',
                    resolve: {
                        items: function () {
                            return info;
                        }
                    }
                });
                modalInstance.opened.then(function () {
                });
                modalInstance.result.then(function (showResult) {
                }, function (reason) {
                    // click，点击取消，则会暑促cancel
                    $scope.pageChanged();
                    $log.info('Modal dismissed at: ' + new Date());
                });
            }
            //维护记录表
            $scope.reuipmentmanage.mainRec = function (info) {
                //alert("nihao");
               //$scope.item = $scope.a[index];
                var modalInstance = $modal.open({
                    template: require('../views/reuipmentMainRec.html'),
                    controller: 'reuipmentMainrec',
                    size: 'lg',
                    resolve: {
                        items: function () {
                            return info;
                        }
                    }
                });
                modalInstance.opened.then(function () {
                });
                modalInstance.result.then(function (showResult) {
                }, function (reason) {
                    // click，点击取消，则会暑促cancel
                    $scope.pageChanged();
                    $log.info('Modal dismissed at: ' + new Date());
                });
            }
			//修改
    		$scope.reuipmentmanage.edit = function (info) {
				 $state.go('app.equipmentUpdate', {                   
                     id: info.id
                    // product_state_name: $scope.item.product_state_name
                });
			} 
}