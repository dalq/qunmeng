var App = angular.module('productOld', [
     // 'ui.router',
     // 'ngResource',
     // 'datatables',
     // "oc.lazyLoad"
    // 'constant'
]);

App.config(require('./router'));
App.factory('productservice', require('./service'));

App.controller('template',require('./controllers/template'));
App.controller('producttemp',require('./controllers/temp'));
App.controller('create',require('./controllers/create'));
App.controller('list',require('./controllers/list'));
App.controller('productApplyingProductList',require('./controllers/applyingProductList'));
App.controller('productExaminedProductList',require('./controllers/examinedProductList'));
App.controller('productSalingProductList',require('./controllers/salingProductList'));
App.controller('productNoSaleProductList',require('./controllers/noSaleProductList'));
App.controller('productBanSaleProductList',require('./controllers/banSaleProductList'));


module.exports = App;