module.exports = function($scope, tableconfig){

	tableconfig.start($scope, {

		url : '/api/ac/tc/product/getApplyProductList',
		col : [
			{'title' : '产品编号', 'col' : 'product_code'},
			{'title' : '产品名称', 'col' : 'product_name'},
			{'title' : '产品状态', 'col' : 'product_state_name'},
			{'title' : '创建人', 'col' : 'create_by'},
			{'title' : '创建时间', 'col' : 'create_time'},
			// {'title' : '使用', 'col' : 'used'},
			// {'title' : '退票', 'col' : 'back'},
			// {'title' : '操作', 'col' : 'btn'},
		],
		btn : [
			{'title' : '编辑', 'onclick' : 'edit'},
			{'title' : '删除', 'onclick' : 'delete'},
			{'title' : '上架', 'onclick' : 'start'}
		],
		search : [
			// {'title' : '订单名称', 'type' : 'txt', 'name' : 'keyword', 'show' : true},
			// {'title' : '创建时间', 'type' : 'date1', 'name' : 'date'},
			// {'title' : '下单时间', 'type' : 'date2', 'name1' : 'start_time', 'name2' : 'end_time'},
			// {'title' : '状态', 'type' : 'select', 'name' : 'duan', 'show' : true, 
			// 	'sel' : [
			// 		{'label' : '全部', value : ''},
			// 		{'label' : '成功', value : '0'},
			// 		{'label' : '失败', value : '1'}
			// 	]
			// }
		]

	});
	$scope.table = tableconfig;






    $scope.edit = function(person) {
        
        alert('编辑' + JSON.stringify(person.code));
       
    }

    $scope.delete = function(person) {
        
        alert('删除' + JSON.stringify(person.code));
    }

    $scope.start = function(person) {
        
        alert('上架' + JSON.stringify(person.code));
        
    }





    $scope.people = [
        { name: 'Adam',      email: 'adam@email.com',      age: 12, country: 'United States' },
        { name: 'Amalie',    email: 'amalie@email.com',    age: 12, country: 'Argentina' },
        { name: 'Estefanía', email: 'estefania@email.com', age: 21, country: 'Argentina' },
        { name: 'Adrian',    email: 'adrian@email.com',    age: 21, country: 'Ecuador' },
        { name: 'Wladimir',  email: 'wladimir@email.com',  age: 30, country: 'Ecuador' },
        { name: 'Samantha',  email: 'samantha@email.com',  age: 30, country: 'United States' },
        { name: 'Nicole',    email: 'nicole@email.com',    age: 43, country: 'Colombia' },
        { name: 'Natasha',   email: 'natasha@email.com',   age: 54, country: 'Ecuador' },
        { name: 'Michael',   email: 'michael@email.com',   age: 15, country: 'Colombia' },
        { name: 'Nicolás',   email: 'nicolas@email.com',    age: 43, country: 'Colombia' }
        ];



};