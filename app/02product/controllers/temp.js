module.exports = function($scope, $state, $resource, $modalInstance, data){

	$scope.res = {};
	$scope.checkres = {};

	console.log(data);

	if(data.lock != '')
	{
		angular.forEach(angular.fromJson(data.lock), function(value, key){
			$scope.checkres[key] = 'yes';
			$scope.res[key] = value;
		});
	}
	var arr = data.unlock.split('#');
	console.log(arr);
	for(var i = 1; i < arr.length - 1; i++){
		$scope.checkres[arr[i]] = 'yes';
	}

	console.log($scope.res);
	console.log($scope.checkres);


	$resource('/api/ac/tc/ticketProductSubAttrService/getTicketProductSubAttrList', {}, {}).save({}, function(res){
        console.log(res);

        if(res.errcode === 0){
            $scope.arr = res.data;
        }else{
            alert(res.errmsg);
        }
    });

	$scope.ok = function () {  
		// console.log($scope.res);
		// console.log($scope.checkres);

		var result = {
			'lock' : {},
			'unlock' : '#',
		}

		angular.forEach($scope.checkres, function(value, key){
			if(value === 'yes')
			{
				var v = $scope.res[key];
				if(angular.isUndefined(v) || v == ''){
					result.unlock += key + '#';
				} else {
					result.lock[key] = v;
				}
			}
		});

        $modalInstance.close(result);  
    };  
    $scope.cancel = function () {  
        $modalInstance.dismiss('cancel');  
    };  

	
};