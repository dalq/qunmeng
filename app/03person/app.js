var App = angular.module('person', []);

App.config(require('./router'));
App.factory('personservice', require('./service'));

App.controller('personInfo',require('./controllers/person'));
App.controller('modifyPwd',require('./controllers/modifypwd'));

module.exports = App;