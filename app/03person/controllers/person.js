module.exports = function ($scope, $resource) {

	init();

	function init(){
		$scope.set = true;
		$resource('/api/ac/sc/systemUserService/getPersonInfo', {}, {}).save({}, function (res) {
            if (res.errcode === 0) {
				$scope.obj = res.data;
				$scope.roleList = res.data.roleList;
            } else {
                alert(res.errmsg);
            }
        });
	}

	//保存信息
	$scope.save = function(){
		if (confirm('修改个人信息后,需要重新登录!')==true){
			//个人信息页面不可修改角色信息,保存时删除角色信息
			delete $scope.obj.roleList;
			$scope.obj.user_type = '0';
			$resource('/api/ac/sc/systemUserService/setUserInfo', {}, {}).save($scope.obj, function (res) {
				if (res.errcode === 0) {
					alert('个人信息修改成功');
					$scope.set = true;
				} else {
					alert(res.errmsg);
				}
			});
		}
	}

	//修改用户信息
	$scope.update = function(){
		$scope.set = false;
	}


};