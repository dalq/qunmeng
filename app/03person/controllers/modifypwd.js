module.exports = function ($scope, $resource) {

	$scope.obj = {};

	//修改密码
	$scope.modifypwd = function(){
		if(!$scope.obj.oldpassword || !$scope.obj.newpassword || !$scope.obj.repassword){
			console.log('信息不全,过不去');
			return;
		}
		$resource('/api/ac/sc/systemUserService/updatePwd', {}, {}).save($scope.obj, function (res) {
			if (res.errcode === 0 && res.data.message == 1) {
				$scope.obj = {};
				alert('密码修改成功,请重新登录!');
				location.href = '/login.html';
			} else {
				alert(res.errmsg);
			}
		});
	}



};