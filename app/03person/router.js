 /**
 * 子模块路由
 * dlq
 */

var router = function($urlRouterProvider, $stateProvider){

 	$stateProvider

    //个人信息
    .state('app.person_info', {
        url: "/person/personinfo.html",
        views: {
            'main@' : {
                template : require('./views/person.html'),
                controller : 'personInfo',
            }
        },
        resolve:{
            
        }
    })

    //系统列表
    .state('app.modify_pwd', {
        url: "/modify/password.html",
        views: {
            'main@' : {
                template : require('./views/modifypwd.html'),
                controller : 'modifyPwd',
            }
        },
        resolve:{
            
        }
    })


	;

};

module.exports = router;