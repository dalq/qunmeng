/**
 * 子模块service
 * djp
 */
var service = function($resource){

    //模型
    var model = [
        {
            'title' : '标题',
            'id' : 'title',
            'type' : 'text',
            'required' : true,
            'placeholder' : '必填',
        },
        {
            'title' : '内容',
            'id' : 'content',
            'type' : 'textarea',
            //'state' : 'normal',
        },

        {
            'title' : '排序',
            'id' : 'asort',
            'type' : 'text',
            'value' : '0',
        },

        
    ];


   
    return {

        model : function(){
            return model;
        },
       
    };

};

module.exports = service;