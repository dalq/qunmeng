module.exports = function($scope, tableconfig, $state){

	tableconfig.start($scope, {
		'url' : '/api/as/wc/notice/noticelist',
		'col' : [
			{'title' : '标题', 'col' : 'title'},
			{'title' : '更新时间', 'col' : 'update_time'},
			{'title' : '操作', 'col' : 'btn'},
		],
		'btn' : [
			{'title' : '查看详情', 'onclick' : 'info'},
			{'title' : '编辑', 'onclick' : 'edit'},
			{'title' : '删除', 'onclick' : 'delete'},
		],
		'search' : [
			{'title' : '标题', 'type' : 'txt', 'name' : 'title', 'show' : true},
		],
		'title' : '公告列表',
		'info' : {
			'to' : 'app.qmnoticeinfo',
		},
		'delete' : {
			'url' : '/api/as/wc/notice/delete',
		},
		'edit' : {
			'to' : 'app.qmnoticeedit'
		}
	});
	$scope.table = tableconfig;

};