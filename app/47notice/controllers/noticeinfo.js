module.exports = function($scope, formconfig, $stateParams, model){

	var id = $stateParams.id;

	formconfig.start({
		'title' : '公告详情',
		'formtitle' : '公告基本信息',
		'elements' : model(),
		'info' : {
			'url' : '/api/as/wc/notice/info',
			'para' : {'id' : id}
		},
	}, $scope);

	$scope.form = formconfig;

};