module.exports = function($scope, formconfig, model){

	formconfig.start({
		'title' : '创建公告',
		'formtitle' : '公告基本信息',
		'elements' : model(),
		'save' : {
			'url' : '/api/as/wc/notice/create',
			'to' : 'app.qmnoticelist',
		}
	}, $scope);

};