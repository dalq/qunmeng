module.exports = function($scope, $resource){

    console.log('aaaaaaaa');

    $scope.p = {
        'type' : '1',
    };

    var d = new Date();

    $scope.ddd = (d.getMonth() + 1) + '月' + d.getDate() + '日';


    $scope.send = function(){

        if(!confirm("你即将发送公众号通知，请慎重！！！！！")) {
            return;
        }

        $resource('/api/as/uc/userwxsk/createnotice', {}, {}).save($scope.p, function(res){

            console.log(res);
            if (res.errcode !== 0) {
              alert(res.errmsg);
              return;
            }
            
            alert('发送成功');
            $scope.load();

        });

    };


    /* 分页
     * ========================================= */
    $scope.maxSize = 5;            //最多显示多少个按钮
    $scope.bigCurrentPage = 1;     //当前页码
    $scope.itemsPerPage = 5;      //每页显示几条

    var type_label = ['普通会员', '分销商', '全部'];
    var isnotice_label = ['未发送', '已发送'];

    $scope.load = function(){

        var para = {
            pageNo: $scope.bigCurrentPage,
            pageSize: $scope.itemsPerPage,
        };

        $resource('/api/as/uc/userwxsk/noticerecordlist', {}, {}).save(para, function(res){

            console.log(res);
            if (res.errcode !== 0) {
              alert(res.errmsg);
              return;
            }

            for(var i = 0; i < res.data.results.length; i++){
                var tmp = res.data.results[i];
                tmp['type_label'] = type_label[parseInt(tmp.type)];
                tmp['isnotice_label'] = isnotice_label[parseInt(tmp.isnotice)];

            }
            
            $scope.objs = res.data.results;
            $scope.bigTotalItems = res.data.totalRecord;
        });
    };
    $scope.load();

};
