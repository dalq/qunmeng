/**
 * 子模块路由
 * djp
 */

var router = function($urlRouterProvider, $stateProvider){

 	$stateProvider

    // .state('app.qmnoticelist', {
    //     url: '/notice/list.html',
    //      views: {
    //         'main@' : {
    //                 controller : 'noticelist',
    //                 template: require('./views/noticelist.html'),
    //             }
    //         },
    //     resolve:{
    //         mylist : function(noticeservice){
    //             return noticeservice.mylist();
    //         },
    //         update : function(noticeservice){
    //             return noticeservice.update();
    //         }
    //     }
    //   })

    //公告列表
    .state('app.qmnoticelist', {
        url: "/notice/list.html",
        views: {
            'main@' : {
                template : require('../996tpl/views/table.html'),
                controller : 'noticelist',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
        }
    })


    //创建公告
    .state('app.qmnoticecreate', {
        url: "/notice/create.html",
        views: {
            'main@' : {
                template : require('../996tpl/views/form.html'),
                controller : 'noticecreate',
            }
        },
        resolve:{
            formconfig : function(formservice){
                return formservice.formconfig();
            },
            model : function(noticeservice){
                return noticeservice.model;
            }
        }
    })
    //渠道详情
    .state('app.qmnoticeinfo', {
        url: "/notice/info/:id",
        views: {
            'main@' : {
                template : require('../996tpl/views/form.html'),
                controller : 'noticeinfo',
            }
        },
        resolve:{
            formconfig : function(formservice){
                return formservice.formconfig();
            },
            model : function(noticeservice){
                return noticeservice.model;
            }
        }
    })

    //公告编辑
    .state('app.qmnoticeedit', {
        url: "/notice/edit/:id",
        views: {
            'main@' : {
                template : require('../996tpl/views/form.html'),
                controller : 'noticeedit',
            }
        },
        resolve:{
            formconfig : function(formservice){
                return formservice.formconfig();
            },
            model : function(noticeservice){
                return noticeservice.model;
            }
        }
    })


    //创建公告
    .state('app.sendnotice', {
        url: "/notice/send.html",
        views: {
            'main@' : {
                template : require('./views/sendnotice.html'),
                controller : 'sendnotice',
            }
        },
        resolve:{
            // formconfig : function(formservice){
            //     return formservice.formconfig();
            // },
            // model : function(noticeservice){
            //     return noticeservice.model;
            // }
        }
    })

    // .state('app.qmcreatenotice', {
    //     url: '/notice/create.html',
    //      views: {
    //         'main@' : {
    //                 controller : 'noticelist',
    //                 template: require('./views/noticelist.html'),
    //             }
    //         },
    //     resolve:{
    //         mylist : function(noticeservice){
    //             return noticeservice.mylist();
    //         },
    //         update : function(noticeservice){
    //             return noticeservice.update();
    //         }
    //     }
    //   })

 	  // .state('app.createnotice', {
    //     url: '/noticelist',
    //      views: {
    //         'main@' : {
    //                 controller : 'noticelist',
    //                 template: require('./views/noticelist.html'),
    //             }
    //         },
    //     resolve:{
    //         mylist : function(noticeservice){
    //             return noticeservice.mylist();
    //         },
    //         update : function(noticeservice){
    //             return noticeservice.update();
    //         }
    //     }
    //   })

    //   .state('app.noticecreate', {
    //     url: '/notice',
    //     views: {
    //         'main@' : {
    //                 controller : 'noticecreate',
    //                 template: require('./views/noticemodel.html'),
    //             }
    //         },
    //     resolve:{
    //         create : function(noticeservice){
    //             return noticeservice.create();
    //         },
    //         place_code : function(noticeservice){
    //             return noticeservice.place_code();
    //         }
    //     }
    //   })


    //   .state('app.noticeedit', {
    //     url: '/notice/:id',
    //      views: {
    //         'main@' : {
    //                 controller : 'noticeedit',
    //                 template: require('./views/noticemodel.html'),
    //             }
    //         },

    //     resolve:{
    //         myinfo : function(noticeservice){
    //             return noticeservice.myinfo();
    //         },
    //         update : function(noticeservice){
    //             return noticeservice.update();
    //         },
    //         place_code : function(noticeservice){
    //             return noticeservice.place_code();
    //         }
    //     }
    //   })

    //   // .state('app.messagetemplet', {
    //   //   url: '/messagetempletlist',
    //   //   controller : 'messagetempletlist',
    //   //   template: require('./notice/messagetempletlist.html'),
    //   //   resolve:{
        	
    //   //   }
    //   // })


};

module.exports = router;