/**
* 子模块路由
* ml
*/

var router = function ($urlRouterProvider, $stateProvider) {

	$stateProvider
		//设备列表
		.state('app.deviceList', {
			url: "/device/list",
			views: {
				'main@': {
					template: require('./views/deviceList.html'),
					controller: 'deviceList',
				}
			},
			resolve: {
				devicelist: function (deviceservice) {
					return deviceservice.devicelist();
				},
				typelist: function (deviceservice) {
					return deviceservice.typelist();
				},
				add: function (deviceservice) {
					return deviceservice.add();
				},
				del: function (deviceservice) {
					return deviceservice.del();
				},
				slist: function (viewservice) {
					return viewservice.slist;
				},
				editurl : function(){
					return 'app.deviceEdit';
				}
			}
		})

		//创建设备
		.state('app.deviceCreate', {
			url: "/device/deviceCreate",
			views: {
				'main@': {
					template: require('./views/deviceCreate.html'),
					controller: 'deviceCreate',
				}
			},
			resolve: {
				create: function (deviceservice) {
					return deviceservice.create();
				},
				slist: function (viewservice) {
					return viewservice.slist;
				},
				devicetype: function (deviceservice) {
					return deviceservice.devicetype;
				},
				what : function(){
					return 'create';
				},
				editurl : function(){
					return 'app.deviceEdit';
				}
			}
		})

		



		//景区编辑
		.state('app.deviceEdit', {
			url: "/device/deviceEdit/:id",
			views: {
				'main@': {
					template: require('./views/deviceCreate.html'),
					controller: 'deviceEdit',
				}
			},
			resolve: {
				info: function (deviceservice) {
					return deviceservice.info();
				},
				slist: function (viewservice) {
					return viewservice.slist;
				},
				devicetype: function (deviceservice) {
					return deviceservice.devicetype;
				},
				update: function (deviceservice) {
					return deviceservice.update();
				},
				remove: function (deviceservice) {
					return deviceservice.remove();
				},
				what : function(){
					return 'edit';
				}
			}
		})





		//===============  商户的设备 =====================//

		//创建设备
		.state('app.bussinessdeviceCreate', {
			url: "/device/bussinessdeviceCreate",
			views: {
				'main@': {
					template: require('./views/deviceCreate.html'),
					controller: 'deviceCreate',
				}
			},
			resolve: {
				create: function (deviceservice) {
					return deviceservice.create();
				},
				// slist : function(ticketservice){
    //             	return ticketservice.nbussinesslist;
    //         	},
				devicetype: function (deviceservice) {
					return deviceservice.devicetype;
				},
				what : function(){
					return 'create';
				},
				editurl : function(){
					return 'app.bussinessdeviceEdit';
				}
			}
		})

		//编辑设备
		.state('app.bussinessdeviceEdit', {
			url: "/device/bussinessdeviceEdit/:id",
			views: {
				'main@': {
					template: require('./views/deviceCreate.html'),
					controller: 'deviceEdit',
				}
			},
			resolve: {
				info: function (deviceservice) {
					return deviceservice.info();
				},
				// slist : function(ticketservice){
    //             	return ticketservice.nbussinesslist;
    //         	},
				devicetype: function (deviceservice) {
					return deviceservice.devicetype;
				},
				update: function (deviceservice) {
					return deviceservice.update();
				},
				remove: function (deviceservice) {
					return deviceservice.remove();
				},
				what : function(){
					return 'edit';
				}
			}
		})


		//设备列表
		.state('app.bussinessdeviceList', {
			url: "/device/bussinesslist",
			views: {
				'main@': {
					template: require('./views/deviceList.html'),
					controller: 'deviceList',
				}
			},
			resolve: {
				devicelist: function (deviceservice) {
					return deviceservice.wddevicelist();
				},
				typelist: function (deviceservice) {
					return deviceservice.typelist();
				},
				add: function (deviceservice) {
					return deviceservice.add();
				},
				del: function (deviceservice) {
					return deviceservice.del();
				},
				// slist : function(ticketservice){
    //             	return ticketservice.nbussinesslist;
    //         	},
				editurl : function(){
					return 'app.bussinessdeviceEdit';
				}
			}
		})


		//===============  商户的设备 =====================//

};

module.exports = router;