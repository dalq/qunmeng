/**
 * 子模块service
 * ml
 */
module.exports = function ($resource, $q, $http) {

	var typelist = '/api/as/tc/device/watchdevicetickettypelist';

	//var devicelist = '/api/us/tc/device/list';

	var devicelist = '/api/as/tc/device/watchdevicepagelist';

	//商户的设备列表，能能看到当前机构下的设备
	var wddevicelist = '/api/as/tc/device/wdwatchdevicepagelist';

	//var setdevicetkttype = '/api/as/tc/type/adminList';

	//添加票种权限
	var add = '/api/as/tc/deviceauth/create';
	//删除票种权限
	var del = '/api/as/tc/deviceauth/delete';

	var info = '/api/as/tc/device/info';

	var update = '/api/as/tc/device/update';

	var create = '/api/as/tc/device/create';


	var remove = '/api/as/tc/deviceauth/deleteall';

	//景区简表
	//var slist = "/api/as/sa/placeview/jlist";

	//列表
	var tktlist = '/api/as/tc/type2/settypelist';

	//票种设置-权限详情显示
	var typeauthinfo = '/api/as/tc/typeauth/info';

	//票种设置-权限详情修改
	var typeauthupdate = '/api/as/tc/typeauth/update';

	//票种设置-权限详情编辑节日列表
	var viewfestivallist = '/api/as/tc/viewfestival/list';

	//票种设置-权限详情编辑节日新增
	var viewfestivalcreate = '/api/as/tc/viewfestival/create';

	//票种设置-权限详情编辑节日删除
	var viewfestivaldel = '/api/as/tc/viewfestival/delete';

	var devicenamelist = '/api/as/tc/device/devicenamelist';


	return {

		typelist: function () {
			return $resource(typelist, {}, {});
		},
		devicelist: function () {
			return $resource(devicelist, {}, {});
		},
		setdevicetkttype: function () {
			return $resource(typelist, {}, {});
		},
		add: function () {
			return $resource(add, {}, {});
		},
		del: function () {
			return $resource(del, {}, {});
		},
		info: function () {
			return $resource(info, {}, {});
		},
		update: function () {
			return $resource(update, {}, {});
		},
		create: function () {
			return $resource(create, {}, {});
		},
		remove: function () {
			return $resource(remove, {}, {});
		},
		slist: function (obj) {
			var deferred = $q.defer(); // 声明延后执行，表示要去监控后面的执行  
			$http({ method: 'GET', params: obj, url: slist }).then(
				function (data) {
					deferred.resolve(data.data);
				},
				function (data) {
					deferred.reject(data.data);
				});
			// success(function(data, status, headers, config) {  
			//     deferred.resolve(data);  // 声明执行成功，即http请求数据成功，可以返回数据了  
			// }).  
			// error(function(data, status, headers, config) {  
			//     deferred.reject(data);   // 声明执行失败，即服务器返回错误  
			// });  
			return deferred.promise;   // 返回承诺，这里并不是最终数据，而是访问最终数据的API 
		},
		devicetype: [
			{
				'name': '检票机',
				'code': 1
			},
			{
				'name': '闸机',
				'code': 2
			},
			{
				'name': '手持查消票机',
				'code': 3
			},
			{
				'name': '办卡机',
				'code': 4
			},
			{
				'name': '自助售票机',
				'code': 5
			},
			{
				'name': '双屏pos机',
				'code': 6
			}
		],
		tktlist: function () {
			return $resource(tktlist, {}, {});
		},
		typeauthinfo: function () {
			return $resource(typeauthinfo, {}, {});
		},
		typeauthupdate: function () {
			return $resource(typeauthupdate, {}, {});
		},
		viewfestivallist: function () {
			return $resource(viewfestivallist, {}, {});
		},
		viewfestivalcreate: function () {
			return $resource(viewfestivalcreate, {}, {});
		},
		viewfestivaldel: function () {
			return $resource(viewfestivaldel, {}, {});
		},
		devicenamelist: function () {
			return $resource(devicenamelist, {}, {});
		},
		wddevicelist: function () {
			return $resource(wddevicelist, {}, {});
		}

	};

};