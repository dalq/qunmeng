module.exports = function ($scope, $modal, typelist, devicelist, add, del, $state, $stateParams, editurl, $resource) {

	$scope.searchform = {};
    /* 分页
     * ========================================= */
    $scope.maxSize = 5;            //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10;         //每页显示几条
	$scope.addview = {'name' : '-----全部----', 'value' : ''};
	//景区下拉
	// slist().then(function (res) {
	// 	if (res.errcode === 0) {
	// 		$scope.viewarr = res.data;
	// 		$scope.viewarr.splice(0,0,$scope.addview);
	// 		console.log($scope.viewarr);
	// 	}
	// 	else {
	// 		alert(res.errmsg);
	// 	}
	// });

	$resource('/api/as/wc/productbussiness/nbussinesslist', {}, {}).save({}, function (res) {
		if (res.errcode === 0) {
			$scope.viewarr = res.data;
			$scope.viewarr.splice(0,0,$scope.addview);
			console.log($scope.viewarr);
		}
		else {
			alert(res.errmsg);
		}
	});


	$scope.load = function() {
		var para = {
			pageNo:$scope.bigCurrentPage, 
			pageSize:$scope.itemsPerPage
		};
	   	para = angular.extend($scope.searchform, para); 
		devicelist.save(para, function (res) {

			if (res.errcode === 0) {
				$scope.objs = res.data.results;
				$scope.bigTotalItems = res.data.totalRecord;
			}
			else {
				alert(res.errmsg);
			}

		});
	}
	$scope.load();

	$scope.myKeyup = function (e) {
		//IE 编码包含在window.event.keyCode中，Firefox或Safari 包含在event.which中
		var keycode = window.event ? e.keyCode : e.which;
		if (keycode == 13) {
			$scope.load();
		}
	};


	//打开模态框
	$scope.configurationticket = function (device_code, place_code) {

		var modalInstance = $modal.open({
			template: require('../views/configureTicketType.html'),
			controller: 'deviceConfigureTicketType',
			size: 'lg',
			resolve: {
				view: function () {
					return place_code;
				},
				device_code: function () {
					return device_code;
				},
				typelist: function () {
					return typelist;
				},
				add: function () {
					return add;
				},
				del: function () {
					return del;
				},
			}
		});

		modalInstance.result.then(function () {

			$scope.load();

		}, function () {
			//$log.info('Modal dismissed at: ' + new Date());
		});
	}

	$scope.edit = function (id) {

		$state.go(editurl, { 'id': id });

	};

	$scope.create = function () {
		$state.go('app.deviceCreate', {});
	}


};