module.exports = function ($scope, toaster, $modalInstance, view, typelist, device_code, add, del) {


	//alert(view);

	$scope.ok = function () {
		$modalInstance.close();
	};

	$scope.cancel = function () {
		$modalInstance.dismiss('cancel');
	};

	typelist.get({ 'view': view, 'device_code': device_code }, function (res) {

		if (res.errcode === 0) {
			$scope.objs = [];
			$scope.objsKeyTemp = {};
			for (var index = 0; index < res.data.length; index++) {
				$scope.objsKeyTemp[res.data[index].CODE] = 1;
			}
			var count = 0;
			for (var key in $scope.objsKeyTemp) {
				$scope.objs.push({ name: '', arr: [] })
				for (var indexResData = 0; indexResData < res.data.length; indexResData++) {
					if (key == res.data[indexResData].CODE) {
						if ($scope.objs[count].arr.length < 1) {
							$scope.objs[count].name = res.data[indexResData].NAME;
							$scope.objs[count].arr.push({ name: res.data[indexResData] })
						} else {
							$scope.objs[count].arr.push({ name: res.data[indexResData] })
						}

					}
				}
				count++;
			}
		}
		else {
			alert(res.errmsg);
		}

	});

	$scope.selection = function ($event, obj) {

		var para = {
			'device_code': device_code,
			'ticket_type': obj.CODE,
			'ticket_type_attr': obj.type_attr

		};

		var checkbox = $event.target;

		if (checkbox.checked) {

			add.save(para, function (res) {

				if (res.errcode != 0) {
					alert(res.errmsg);
				}
				toaster.success({ title: '', body: '添加成功！' })

			});

		}
		else {
			del.save(para, function (res) {

				if (res.errcode != 0) {
					alert(res.errmsg);
				}
				toaster.info({ title: '', body: '删除成功！' })

			});
		}



	};

};