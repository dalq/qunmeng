module.exports = function ($scope, $stateParams, devicetype, create, $state, what, editurl, $resource) {

	//机器id
	var placecode = $stateParams.placecode;

	$scope.what = what;

	$scope.lock = false;

	$scope.obj = {};
	$scope.obj.view = placecode;
	//机器类型
	$scope.typearr = devicetype;
	$scope.obj.type = $scope.typearr[0].code;

	//状态
	$scope.obj.state = 1;
	$scope.obj.del_flg = 0;
	$scope.obj.auth_state = 0;
	$scope.obj.many_state = 1;
	$scope.obj.group_auth_state = 1;

	$resource('/api/as/wc/productbussiness/nbussinesslist', {}, {}).save({}, function (res) {
		if (res.errcode === 0) {
			$scope.viewarr = res.data;
			$scope.obj.view = res.data[0].code;
		}
		else {
			alert(res.errmsg);
		}
	});

	function trim(str) {
		return str.replace(/(^\s+)|(\s+$)/g, "");
	}

	$scope.gogo = function () {

		if ($scope.obj.type == 5 || $scope.obj.type == 6) {
			if ($scope.obj.seller_login_code == undefined || trim($scope.obj.seller_login_code) == '') {
				alert('请填写登录用户')
				return;
			}
		} else {
			delete $scope.obj.print_ticket_length;
			delete $scope.obj.print_ticket_width;
			delete $scope.obj.print_ticket_x_axis;
			delete $scope.obj.print_ticket_y_axis;
		}

		create.save($scope.obj, function (res) {

			if (res.errcode !== 0) {
				alert(res.errmsg);
			}
			else {
				$state.go('app.bussinessdeviceList');
			}

		});

	};

};