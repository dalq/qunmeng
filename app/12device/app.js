/**
 * 子模块入口
 * ml
 */
var App = angular.module('device', []);

App.config(require('./router'));

//service
App.factory('deviceservice', require('./service'));

//Controllers
App.controller('deviceList', require('./controllers/deviceList'));
App.controller('deviceEdit', require('./controllers/deviceEdit'));
App.controller('deviceCreate', require('./controllers/deviceCreate'));
App.controller('deviceConfigureTicketType', require('./controllers/configureTicketType'));

module.exports = App;