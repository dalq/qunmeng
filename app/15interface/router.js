 /**
 * 子模块路由
 * DHpai
 */

var router = function($urlRouterProvider, $stateProvider){

 	$stateProvider

    //同步列表
    .state('app.interfaceSyncList', {
        url: "/api/synclist",
        views: {
            'main@' : {
                template : require('./views/syncApiList.html'),
                controller : 'syncApiList',
            }
        }
    })

    //接口列表
    .state('app.interfaceList', {
        url: "/api/list",
        views: {
            'main@' : {
                template : require('./views/apiList.html'),
                controller : 'apiList',
            }
        }
    })

    //用户侧接口列表
    .state('app.userInterfaceList', {
        url: "/api/userlist",
        views: {
            'main@' : {
                template : require('./views/userApiList.html'),
                controller : 'userApiList',
            }
        }
    })
   
    //编辑接口
    .state('app.interfaceEdit', {
        url: "/api/edit/:id",
        views: {
            'main@' : {
                template : require('./views/apiEdit.html'),
                controller : 'apiEdit',
            }
        }
    })

    //接口参数列表
    .state('app.interfaceParam', {
        url: "/api/param/:id",
        views: {
            'main@' : {
                template : require('./views/paramlist.html'),
                controller : 'interfaceParam',
            }
        }
    })



	;

};

module.exports = router;