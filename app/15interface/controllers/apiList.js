module.exports = function ($scope, $state, $resource, $modal, toaster) {

	//加载模块搜索栏的数据
	$scope.loadSearchBox = function (){
		$resource('/api/as/sc/dict/getDictInfoList', {}, {}).get({'type': 'sys_center_type'}, function(res){
			if(res.errcode === 0){
				$scope.searchitem.center_list = res.data;
				$scope.searchitem.center_list.unshift({'label': '全部', 'center_code': '', 'value': ''});
				$scope.searchfrom.center = $scope.searchitem.center_list[0];
			} else {
				toaster.error({title: '', body: '加载搜索信息失败'});
			}
		});
	}

	//搜索
	$scope.search = function (firstLoad){
		var para = {
            'pageNo': $scope.currentPage,
            'pageSize': $scope.itemsPerPage
		};
		if(!firstLoad){
			para.center_code = $scope.searchfrom.center.center_code;
			para.auth_type = $scope.searchfrom.auth_type.value;
			para.type = $scope.searchfrom.type.value;
			para.url = $scope.searchfrom.url;
		}
		$resource('/api/as/sc/sysfun/getList', {}, {}).save(para, function(res){
			if(res.errcode === 0){
				$scope.apiList = res.data.results;
				$scope.totalItems = res.data.totalRecord;
			}else{
				alert(res.errmsg);
			}
		});
	};

	//初始化搜索栏信息
	init();
	function init(){
		$scope.currentPage = 1;			//当前页码
		$scope.itemsPerPage = 10;		//每页显示几条
		$scope.searchitem = {
			//权限类型下拉菜单
			'auth_type': [
				{ 'label': '全部', value: '' },
				{ 'label': '管理侧', value: 'a' },
				{ 'label': '用户侧', value: 'u' }
			],
			//接口类型下拉菜单
			'type': [
				{ 'label': '全部', value: '' },
				{ 'label': '方法类接口', value: 'c' },
				{ 'label': 'XML类接口', value: 's' }
			]
		};
		$scope.searchfrom = {
			'auth_type': $scope.searchitem.auth_type[0],
			'type': $scope.searchitem.type[0]
		};
		$scope.loadSearchBox();
		$scope.search(true);
	}

	//编辑接口
	$scope.edit = function (item) {
		var modalInstance = $modal.open({
			template: require('../views/apiEdit.html'),
			controller: 'apiEdit',
			size: 'lg',
			resolve: {
				item: function () {
					return item;
				}
			}
		});
		modalInstance.result.then(function() {
  			$scope.search();
		});

	};

	//接口参数-----功能暂未开启
	// $scope.param = function (item) {
	// 	$state.go('app.interfaceParam', { 'id': item.id });
	// };

	//设置权限
	$scope.authType = function (item) {
		var modalInstance = $modal.open({
			template: require('../views/authType.html'),
			controller: 'authType',
			size: 'lg',
			resolve: {
				item: function () {
					return item;
				}
			}
		});
		modalInstance.result.then(function(result){
			toaster.success({title: "", body: '接口修改权限成功'});
			item.auth_type = result.auth_type;
			item.url = result.url
		});
	};

	//查看接口权限范围
	$scope.getRole = function (item) {
		var modalInstance = $modal.open({
			template: require('../views/authScope.html'),
			controller: 'authScope',
			size: 'lg',
			resolve: {
				item: function () {
					return item;
				}
			}
		});
		modalInstance.result.then(function() {
  			
		});
	};

	//设置接口绑定信息
	$scope.setApi = function (item) {
		var modalInstance = $modal.open({
			template: require('../views/setBindApi.html'),
			controller: 'setBindApi',
			size: 'lg',
			resolve: {
				item: function () {
					return item;
				},
				center_list : function(){
					return $scope.searchitem.center_list;
				}
			}
		});
		modalInstance.result.then(function() {
  			
		});
	};

};