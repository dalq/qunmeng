/**
 * 模态框
 */
module.exports = function($scope, $modalInstance, $resource, item){

    //初始化信息
    init();
    function init(){
        $scope.authType = {};
        if(item.auth_type == 'a,u'){
            $scope.authType.admin = true;
            $scope.authType.user = true;
        } else if(item.auth_type == 'a'){
            $scope.authType.admin = true;
        } else if(item.auth_type == 'u'){
            $scope.authType.user = true;
        }
    }

    $scope.ok = function () {
        $scope.authType.id = item.id;
        $resource('/api/ac/sc/systemFunction/setAuthType', {}, {}).save($scope.authType, function (res) {
            if (res.errcode === 0){
                $modalInstance.close(res.data);
            } else {
                alert(res.errmsg);
            }
		});
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
};