module.exports = function($scope, $resource, $modalInstance, $modal, item, toaster){

	init();
	function init(){
		$resource('/api/as/sc/sysfun/getById', {}, {}).save({'id': item.id}, function(res){
			if(res.errcode === 0){
				$scope.obj = res.data;
				$scope.obj.gain_header = $scope.obj.gain_header || '0';
			} else {
				toaster.error({title: '', body: res.errmsg});
			}
		});
	};

	//保存
	$scope.save = function () {
		$resource('/api/as/sc/sysfun/save', {}, {}).save($scope.obj, function(res){
			if(res.errcode === 0){
				toaster.success({title: "", body: '保存成功'});
				$modalInstance.close();
			} else {
				toaster.error({title: '', body: res.errmsg});
			}
		});
	};

	//取消
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

};