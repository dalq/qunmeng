module.exports = function ($scope, $resource, $modal, toaster) {

	//加载模块搜索栏的数据
	$scope.loadSearchBox = function (){
		$resource('/api/as/sc/dict/getDictInfoList', {}, {}).get({'type': 'sys_center_type'}, function(res){
			if(res.errcode === 0){
				$scope.item_set.center_list = res.data;
				$scope.item_set.center_list.unshift({'label': '全部', 'center_code': '', 'value': ''});
				$scope.searchform.center = $scope.item_set.center_list[0];
				$scope.search(1);
			} else {
				toaster.error({title: '', body: '加载搜索信息失败'});
			}
		});
	}

	//搜索
	$scope.search = function (pageNo){
		var para = {
            'pageNo': pageNo,
            'pageSize': $scope.itemsPerPage,
			'center_code': $scope.searchform.center.center_code,
			'type': $scope.searchform.type.value,
			'url': $scope.searchform.url,
		};
		$resource('/api/ac/sc/systemFunction/getUserVisitList', {}, {}).save(para, function(res){
			if(res.errcode === 0){
				$scope.apiList = res.data.results;
				$scope.totalItems = res.data.totalRecord;
				$scope.item_set.btn = para.type == '1' ? true : false;
				$scope.currentPage = pageNo;
			} else {
				toaster.error({title: '', body: res.errmsg});
			}
		});
	};

	$scope.changePage = function (){
		$scope.search($scope.currentPage);
	}

	init();
	//初始化页面信息
	function init(){
		$scope.currentPage = 1;			//当前页码
		$scope.itemsPerPage = 20;		//每页显示几条
		$scope.item_set = {
			//接口类型下拉菜单
			'type': [
				{ 'label': '新增', 'value': '1' },
				{ 'label': '已有', 'value': '0' }
			]
		};
		$scope.searchform = {'type': $scope.item_set.type[0]};
		$scope.loadSearchBox();
	};

	//flag=true:开启用户侧接口; flag=false:修改用户侧接口
	$scope.update = function (index, flag){
		var apiInfo = $scope.apiList[index];
		var modalInstance = $modal.open({
			template: require('../views/userApiEdit.html'),
			controller: 'userApiEdit',
			size: 'lg',
			resolve: {
				item : function () {
					return apiInfo;
				},
				flag : function () {
					return flag;
				}
			}
		});
		modalInstance.result.then(function(result){
			toaster.success({title: '', body: '保存成功'});
			if(flag){
				$scope.apiList.splice(index, 1);
			} else {
				apiInfo.state = result.state;
				apiInfo.type = result.type;
				apiInfo.name = result.name;
				apiInfo.remarks = result.remarks;
			}
		});
	};

	//删除
	$scope.delete = function (index) {
		if(confirm('确认要删除此用户侧接口吗?') == true){
			$resource('/api/as/sc/uservisit/delUserVisit', {}, {}).save({'id': $scope.apiList[index].id}, function(res){
				if(res.errcode === 0){
					toaster.success({title: '', body: '删除成功'});
					$scope.apiList.splice(index, 1);
				} else {
					toaster.error({title: '', body: res.errmsg});
				}
			});
		}
    };



};