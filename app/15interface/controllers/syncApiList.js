module.exports = function ($scope, $resource, toaster) {

	//初始化搜索栏信息
	init();
	function init(){
		$scope.searchfrom = {};
		$resource('/api/as/sc/dict/getDictInfoList', {}, {}).get({'type': 'sys_center_type'}, function(res){
			if(res.errcode === 0){
				$scope.search_box = res.data;
			} else {
				toaster.error({title: '', body: '加载搜索信息失败'});
			}
        });
	}
	
	//搜索列表
	$scope.search = function () {
		$scope.all = false;
		var para = {
			'center_code': $scope.searchfrom.item.center_code,
			'center_key': $scope.searchfrom.item.value
		};
		$resource('/api/ac/sc/systemFunction/isSync', {}, {}).get(para, function(res){
			if(res.errcode === 0){
				$scope.apiList = res.data;
			}else{
				toaster.error({title: '', body: res.errmsg});
			}
        });
	};

	//全部选择框
	$scope.setSelect = function (all) {
		if(all){
			angular.forEach($scope.apiList, function(item){
				item.check = true;
			});
		}else{
			angular.forEach($scope.apiList, function(item){
				item.check = false;
			});
		}
	};

	//同步
	$scope.sync = function (item) {
		var list = [];
		if(item){
			list.push(item);
		} else {
			angular.forEach($scope.apiList, function(item){
				if(item.check){
					list.push(item);
				}
			});
		}
		$resource('/api/ac/sc/systemFunction/updateSysFun', {}, {}).save({'list': list}, function (res) {
			if (res.errcode === 0) {
				toaster.success({title: "", body: '成功更新' + res.data.sucess + '个接口'});
				$scope.search();
			} else {
				toaster.error({title: '', body: res.errmsg});
			}
		});
	}


};