/**
 * 模态框
 */
module.exports = function($scope, $modalInstance, $resource, item){
    
    init();
    function init(){
         $resource('/api/ac/sc/systemFunction/getRoleList', {}, {}).save({'id': item.id}, function (res) {
            if (res.errcode === 0) {
                $scope.role_list = res.data.roleList;
                $scope.menu_list = res.data.menuList;
            } else {
                alert(res.errmsg);
            }
		});
    }

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
};