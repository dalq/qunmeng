module.exports = function ($scope, $state, $resource, $stateParams) {

	var para = {
		'id': $stateParams.id
	};

	//获得入参列表
	$resource('/api/as/sc/sysfunreq/getReqList', {}, {}).get(para, function(res){
		if(res.errcode === 0){
			$scope.paraInList = res.data;
		}else{
			alert(res.errmsg);
		}
	});

	//获得出参列表
	$resource('/api/as/sc/sysfunres/getResList', {}, {}).get(para, function(res){
		if(res.errcode === 0){
			$scope.paraOutList = res.data;
		}else{
			alert(res.errmsg);
		}
	});

	//后退
	$scope.back = function () {
		$state.go('app.interfaceList', {});
	};

};