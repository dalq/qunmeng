/**
 * 模态框
 */
module.exports = function($scope, $modalInstance, $resource, item, flag){
    
    init();
    function init(){
        $scope.login_type = [{'label': '需要登录', 'value': '1'}, { 'label': '无需登录', 'value': '0'}];
        $scope.obj = {
            'id': item.id,
            'url': item.url,
            'logintype': $scope.login_type[0],
            'name': item.name,
            'remarks': item.remarks
        };
        $scope.obj.state = flag ? '0' : item.state;
        if(item.type == 0){
            $scope.obj.logintype = $scope.login_type[1];
        }
    }

    $scope.save = function () {
        $scope.obj.type = $scope.obj.logintype.value;
        $resource('/api/as/sc/uservisit/save', {}, {}).save($scope.obj, function(res){
			if(res.errcode === 0){
                $modalInstance.close($scope.obj);
			}
		});
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
};