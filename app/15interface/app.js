var App = angular.module('interface', []);

App.config(require('./router'));

//service
App.factory('interfaceservice', require('./service'));

//Controllers
App.controller('apiList', require('./controllers/apiList'));
App.controller('syncApiList', require('./controllers/syncApiList'));
App.controller('apiEdit', require('./controllers/apiEdit'));
App.controller('authType', require('./controllers/authType'));
App.controller('authScope', require('./controllers/authScope'));
App.controller('setBindApi', require('./controllers/setBindApi'));
App.controller('interfaceParam', require('./controllers/paramlist'));
App.controller('userApiList', require('./controllers/userApiList'));
App.controller('userApiEdit', require('./controllers/userApiEdit'));



module.exports = App;