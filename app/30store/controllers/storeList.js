module.exports = function($scope, $state, list, viewupdate, updateplacemerchant,merchantinfo,gogosort, toaster){

	$scope.searchform = {};


	$scope.create = function(){

		$state.go('app.storeCreat');
	};

	/* 分页
     * ========================================= */
    $scope.maxSize = 5;            //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10;         //每页显示几条
    
    $scope.load = function () {     
        var para = {
            pageNo:$scope.bigCurrentPage,
            pageSize:$scope.itemsPerPage,
            'type' : 'M',
        };

        para = angular.extend($scope.searchform, para);
        
        list.save(para, function(res){

         	if(res.errcode !== 0)
         	{
         		alert("数据获取失败");
         		return;
         	}

         	$scope.objs = res.data.results;
            $scope.bigTotalItems = res.data.totalRecord;

        });

        

    };
    $scope.load();

	$scope.myKeyup = function (e) {

		//IE 编码包含在window.event.keyCode中，Firefox或Safari 包含在event.which中
		var keycode = window.event ? e.keyCode : e.which;
		if (keycode == 13) {
			$scope.load();
		}
	};


    $scope.edit = function(id){

    	$state.go('app.storeEdit', {'id' : id});

    };


    $scope.asort = function(id, asort){

        merchantinfo.save({'merchant_id' : id},function(res){
            if(res.errcode == 10003){
                toaster.warning({title:"",body:"未设置商户信息,不可进行排序操作"});
                
            } else {
                gogosort.save({'id' : id, 'asort' : asort}, function(res){

                    if(res.errcode === 0)
                    {
                        $scope.load();
                    }
                    else
                    {
                        alert(res.errmsg);
                    }

                });
            }
        

        });
        

    	// updateplacemerchant.save({'merchant_id' : id, 'merchant_asort' : asort}, function(res){


		// 	if(res.errcode === 0)
		// 	{
		// 		$scope.load();
		// 	}
		// 	else
		// 	{
		// 		alert(res.errmsg);
		// 	}

		// });

    };

    $scope.type = function(id){
    	$state.go('app.tkttype', {'placeid' : id});
    };


    $scope.createtkttype = function(id){
        $state.go('app.tkttypecreate', {'placeid' : id});
    }

    $scope.device = function(code){
        $state.go('app.devicelist', {'placecode' : code});
    }

    
};