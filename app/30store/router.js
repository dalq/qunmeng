/**
* 子模块路由
* dlq
*/

var router = function ($urlRouterProvider, $stateProvider) {

    $stateProvider

        //商户列表
        .state('app.storeList', {
            url: '/storelist.html',
            views: {
                'main@': {
                    template: require('./views/storeList.html'),
                    controller: 'storeList',
                }
            },
            resolve: {
                viewupdate: function (storeservice) {
                    return storeservice.update();
                },
                list: function (storeservice) {
                    return storeservice.list();
                },
                updateplacemerchant: function (storeservice) {
                    return storeservice.updateplacemerchant();
                },
                merchantinfo: function (storeservice) {
                    return storeservice.merchantinfo();
                },
                gogosort: function (storeservice) {
                    return storeservice.gogosort();
                }
            }
        })

        //商户编辑
        .state('app.storeEdit', {
            url: '/storeEdit:id',
            views: {
                'main@': {
                    template: require('./views/storeEdit.html'),
                    controller: 'storeEdit',
                }
            },
            resolve: {
                'type': function () {
                    return 'M';
                },
                'id': function () {
                    return '';
                }
            }
        })

        //商户创建
        .state('app.storeCreat', {
            url: '/storeCreat',
            views: {
                'main@': {
                    template: require('./views/storeCreat.html'),
                    controller: 'storeCreat',
                }
            },
            resolve: {
                'type': function () {
                    return 'M';
                },
                'id': function () {
                    return '';
                }
            }
        })

        ;

};

module.exports = router;