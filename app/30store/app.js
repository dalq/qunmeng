var App = angular.module('store', []);

App.config(require('./router'));
App.factory('storeservice', require('./service'));

App.controller('storeList',require('./controllers/storeList'));
App.controller('storeEdit',require('./controllers/storeEdit'));
App.controller('storeCreat',require('./controllers/storeCreat'));

App.directive('storeBaseInfo',require('./directives/storeBaseInfo'));
App.directive('storeView',require('./directives/storeView'));
App.directive('storeCustom',require('./directives/storeCustom'));

module.exports = App;