module.exports = function ($resource, $state, $http, $q, toaster) {

	return {

		restrict: 'AE',
		template: require('../views/storeBaseInfo.html'),
		replace: true,
		scope: {
			'placeobj': '=',
			'baseinfo': '=',
			// 'util' : '=',
		},
		link: function (scope, elements, attrs) {

			var obj = {
				'name': '',
				'subname': '',
				'address': '',
				'content': '',
				'province': '',	//省
				'city': '',		//市
				'district': '',	//区
				'business_district': '', 	//商圈
				'open_date': '',	//开放日期
				'open_time': '',	//营业时间
				'longitude': '0',	//经度
				'latitude': '0',	//纬度
				'asort': 0,
				'theme': '',	//主题
				'star': '5',
				'img': '',		//顶图
				'logo': '',
				'view_type': '',	//分类
			};

			scope.info = {
				'themearr' : []
			}

			scope.page = {};

			var beforedata = {
				//省份列表
				'provincelist':
				$http({
					'method': 'GET',
					'url': '/api/us/sc/city/arealist',
				}),
				//城市列表
				'citylist':
				$http({
					'method': 'GET',
					'url': '/api/us/sc/city/arealist',
					'params': { 'code': '210000' },
				}),
				//区
				'districtlist':
				$http({
					'method': 'GET',
					'url': '/api/us/sc/city/arealist',
					'params': { 'code': '210100' },
				}),
				//类型
				'typelist':
				$http({
					'method': 'GET',
					'url': '/api/us/mc/mertradetypedao/findByTypeList',
					'params': { 'type': 'cheap_menu' },
				}),
				//主题
				'themelist':
				$http({
					'method' : 'GET', 
					'url': '/api/as/sc/dict/dictbytypelist',
					'params' : {'type' : 'place_theme'},
				}),
			};

			if (scope.placeobj.id !== '') {

				beforedata['placeinfo'] = $http({
					'method': 'GET',
					'url': '/api/as/tc/place/info',
					'params': {
						'id': scope.placeobj.id
					}
				});
			}


			$q.all(beforedata).then(function (res) {
				// 地址信息
				//分类信息
				if (res.provincelist.data.errcode === 0) {
				} else {
					alert(res.provincelist.data.errmsg);
					return;
				}

				if (res.citylist.data.errcode === 0) {
				} else {
					alert(res.citylist.data.errmsg);
					return;
				}

				if (res.districtlist.data.errcode === 0) {
				} else {
					alert(res.districtlist.data.errmsg);
					return;
				}

				if (angular.isDefined(res.placeinfo)) {
					//地点信息
					if (res.placeinfo.data.errcode === 0) {
						angular.extend(scope.placeobj, res.placeinfo.data.data);


						var themeArr = scope.placeobj.theme.split(",");
						var array = res.themelist.data.data;
						console.log(array);
						for(var i = 0; i < array.length; i++){
							for(var j = 0; j < themeArr.length; j++){
								if(themeArr[j] == array[i].label){
									console.log(themeArr[j]);
									scope.info.themearr.push(themeArr[j]);
								} else {
									console.log('不匹配');
								}
							}
						}

						// scope.info.themearr = scope.placeobj.theme.split(",");
						
					} else {
						alert(res.placeinfo.data.errmsg);
						return
					}
				}

				if (res.typelist.data.errcode === 0) {
				} else {
					alert(res.districtlist.data.errmsg);
					return;
				}

				if(res.themelist.data.errcode === 0){
				} else {
					alert(res.themelist.data.errmsg);
					return;
				}


				scope.page = {
					'provincelist': res.provincelist.data.data,
					'citylist': res.citylist.data.data,
					'districtlist': res.districtlist.data.data,
					'typelist': res.typelist.data.data,
					'business_districtlist': [],
					'themelist' : res.themelist.data.data,
				}

				if (scope.placeobj.province == '') {
					scope.placeobj.province = '210000';	//辽宁省
					scope.placeobj.city = '210100';		//沈阳市
					scope.placeobj.district = '';		//铁西区
					scope.placeobj.business_district = ''	//xxx商圈
				}

			});

			scope.save = function () {

				var url = '';
				if (scope.placeobj.id == '') {
					url = '/api/as/tc/place/create';
				} else {
					url = '/api/as/tc/place/update';
				}
				if (!scope.placeobj.longitude) {
					scope.placeobj.longitude = 0;
				}
				if (!scope.placeobj.latitude) {
					scope.placeobj.latitude = 0;
				}
				scope.placeobj.theme = scope.info.themearr.join(",");
				$resource(url, {}, {}).save(scope.placeobj, function (res) {
					if (res.errcode != 0) {
						alert(res.errmsg);
						return;
					}
					if (angular.isDefined(res.data.uuid)) {
						$state.go('app.storeEdit', { 'id': res.data.uuid });
					} else {
						toaster.success({ title: "", body: "修改成功" });
					}
				});
			};

			//省
			scope.changeprovince = function (code) {
				scope.placeobj.city = '';
				scope.placeobj.district = '';
				scope.placeobj.business_district = '';

				scope.page.citylist = [];
				scope.page.districtlist = [];
				scope.page.business_districtlist = [];

				getarea('province', code);
			};
			//市
			scope.changecity = function (code) {
				scope.placeobj.district = '';
				scope.placeobj.business_district = '';

				scope.page.districtlist = [];
				scope.page.business_districtlist = [];

				getarea('city', code);
			};
			//区
			scope.changedistrict = function (code) {
				scope.placeobj.business_district = '';
				scope.page.business_districtlist = [];
				getarea('district', code);
			};



			function getarea(what, code) {
				$resource('/api/us/sc/city/arealist', {}, {})
					.get({ 'code': code }, function (res) {
						if (res.errcode !== 0) {
							alert(res.errmsg);
							return;
						}

						if (what === 'province') {
							scope.page.citylist = res.data;
						} else if (what === 'city') {
							scope.page.districtlist = res.data;
						} else if (what === 'district') {
							scope.page.business_districtlist = res.data;
						}
					});
			}

		}
	};
};

