module.exports = function ($resource, $state, $http, $q) {

	return {

		restrict: 'AE',
		template: require('../views/storeView.html'),
		replace: true,
		scope: {
			'placeobj': '=',
		},
		link: function (scope, elements, attrs) {

			scope.viewobj = {
				'tel': '',
				//'view_type' : '',
				'promise': '',
				'ev_good': 0,
				'ev_general': 0,
				'ev_bad': 0,
				'view720_url': '',
				'ebook_url': '',
				'fav_policy': '',
				'bef_hour': 0,
			}

			scope.hb = {
				'isSelected': false
			}


			$resource('/api/as/tc/placeview/info', {}, {})
				.get({ 'place_id': scope.placeobj.id }, function (res) {

					if (res.errcode === 0) {
						angular.extend(scope.viewobj, res.data);
					} else if (res.errcode === 10003) {

					} else {
						alert(res.errmsg);
					}

					if (scope.viewobj.rebate_type == 0) {
						scope.hb.isSelected = true;
					} else if (scope.viewobj.rebate_type == 1) {
						scope.hb.isSelected = false;
					}

				});

			scope.onHbChange = function (isSelected) {
				if (isSelected == true) {
					scope.viewobj.rebate_type = '0';
				} else {
					scope.viewobj.rebate_type = '1';
				}
			}

			// 获取类型列表
			// 获取类型列表
			// $resource('/api/us/mc/mertradetypedao/findByTypeList', {}, {})
			// .get({'type':'cheap_menu'},function(res){				
			// 	if(res.errcode === 0){
			// 		scope.typedatas = res.data;
			// 		//ScopedCredential.storeobj.view_type = res.data
			// 	}else if(res.errcode === 10003){

			// 	}else{
			// 		alert(res.errmsg);
			// 	}
			// });




			scope.save = function () {

				var url = '/api/as/tc/placeview/create';
				// if(scope.placeobj.id == ''){
				// 	url = '/api/as/tc/placeview/create';
				// }else{
				// 	url = '/api/as/tc/placeview/update';
				// }

				scope.viewobj['place_id'] = scope.placeobj.id;

				$resource(url, {}, {}).save(scope.viewobj, function (res) {
					if (res.errcode != 0) {
						alert(res.errmsg);
						return;
					}

					alert('操作成功');
				});
			};

		}

	};
};

