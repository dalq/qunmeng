var App = angular.module('report', []);

App.config(require('./router'));
//App.factory('shopservice', require('./service'));

App.controller('report',require('./controllers/report'));
// App.controller('qmnewproduct',require('./controllers/product'));
// App.controller('qmplace',require('./controllers/place'));
// App.controller('qmdevice',require('./controllers/device'));
// App.controller('qmaddplace',require('./controllers/addplace'));
// App.controller('viewinfo',require('./controllers/info'));
// App.controller('viewedit',require('./controllers/edit'));

App.directive('income',require('./directives/income'));
App.directive('book',require('./directives/book'));
App.directive('cancellation',require('./directives/cancellation'));
App.directive('seller',require('./directives/seller'));





// App.controller('list',require('./controllers/list'));

module.exports = App;