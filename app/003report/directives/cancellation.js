module.exports = function ($resource, $state, $http, $q, $modal) {
	return {
		restrict: 'AE',
		template: require('../views/d_cancellation.html'),
		replace: true,
		scope: {
			//'p' : '=',
			//'other': '=',
			//'state' : '=',
			'str2date' : '=',
			'date2str' : '=',
		},
		link: function (scope, elements, attrs) {

			console.log('income');

    scope.searchState = false;

    //保存的数据
    var results = [];

    //统计
    var statistics = {};

    scope.objs = [];

    var filename = '';

	scope.vm = {
        'date' : new Date(),
        'options' : {
            format: "YYYY-MM-DD",
            locale : 'zh-cn',
            ignoreReadonly : true,
        }
    };

	scope.searchform = {
		'start_time_date' : scope.date2str(new Date()),
		'end_time_date' : scope.date2str(new Date()),
	};

    scope.load = function () {

        statistics = {};

        filename = '';

        results = [];

        scope.objs = [];

        scope.total = {};


        var s = '';
        var e = '';

        if (typeof scope.searchform.start_time_date === 'string') {
            s = scope.searchform.start_time_date;
        } else {
            s = scope.searchform.start_time_date.format('YYYY-MM-DD');
        }

        if (typeof scope.searchform.end_time_date === 'string') {
            e = scope.searchform.end_time_date;
        } else {
            e = scope.searchform.end_time_date.format('YYYY-MM-DD');
        }

        var para = {
            //'order_code' : scope.searchform.order_code,
            'start_time' : s + ' 00:00:00',
            'end_time' : e + ' 23:59:59',
        };

        filename = '核销报表' + '[' + s + '~' + e + ']';

        scope.searchState = true;
        $resource('/api/as/tc/ticketreceipt/receiptlist', {}, {}).save(para, function(res){
        //$resource('/api/as/wc/ticketreceipt/receiptlist', {}, {}).save(para, function(res){
            console.log(res);
            scope.searchState = false;
            if (res.errcode !== 0) {
                alert(res.errmsg);
                return;
            }

            results = res.data;

            for(var i  = 0; i < results.length; i++){
                var tmp = results[i];

                if(angular.isUndefined(statistics[tmp.sale_code])){
                    statistics[tmp.sale_code] = {
                        'name' : tmp.title,
                        'sale_code' : tmp.sale_code,
                        'count' : 1,
                        'des_num' : parseInt(tmp.des_num),

                        
                    };
                }else{
                    statistics[tmp.sale_code].count += 1;
                    statistics[tmp.sale_code].des_num += parseInt(tmp.des_num);
                }
            }

            var total = {
                'count' : 0,
                'des_num' : 0,
            };

            angular.forEach(statistics, function (value, key) {
                scope.objs.push(value);
            });

            for(var i = 0; i < scope.objs.length; i++){
                var tmp = scope.objs[i];
                angular.forEach(tmp, function (v, k) {
                    total[k] += parseInt(v);
                });
            }

            scope.total = total;
        });
	}
	scope.load();

	scope.toExcel = function(){
			
		var labels = ['title', 'order_unique_code', 'hexiao_time', 'des_num', 
		'name', 'mobile', 'cardno', 'view_name', 'device', 'out_order_id'];
        var arr = [];
        for(var i = 0; i< results.length; i++){
            var tmp = results[i];
            var haha = [];
            for(var j = 0; j < labels.length; j++){
                haha.push(tmp[labels[j]]);
            }
            arr.push(haha);
        }
        arr.splice(0, 0, ['产品名称', '票码', '核销时间' , '核销数量', 
        	'姓名', '电活', '身份证', '商家', '核销设备码' , '订单编号'
        	]);

        ExportUtil.toExcel(arr, filename);

	}
	

		}
	};
};

