module.exports = function ($resource, $state, $http, $q, $modal, dlq) {
	return {
		restrict: 'AE',
		template: require('../views/d_income.html'),
		replace: true,
		scope: {
			//'p' : '=',
			//'other': '=',
			//'state' : '=',
			'str2date' : '=',
			'date2str' : '=',
		},
		link: function (scope, elements, attrs) {

			console.log('income');

            console.log(dlq);

    scope.searchState = false;

    //保存的数据
    var results = [];

    //统计
    var statistics = {};

    scope.objs = [];

    var filename = '';

	scope.vm = {
        'date' : new Date(),
        'options' : {
            format: "YYYY-MM-DD",
            locale : 'zh-cn',
            ignoreReadonly : true,
        }
    };

	scope.searchform = {
		'start_time_date' : scope.date2str(new Date()),
		'end_time_date' : scope.date2str(new Date()),
	};

    scope.load = function () {

        statistics = {};

        filename = '';

        results = [];

        scope.objs = [];

        scope.total = {};


        var s = '';
        var e = '';

        if (typeof scope.searchform.start_time_date === 'string') {
            s = scope.searchform.start_time_date;
        } else {
            s = scope.searchform.start_time_date.format('YYYY-MM-DD');
        }

        if (typeof scope.searchform.end_time_date === 'string') {
            e = scope.searchform.end_time_date;
        } else {
            e = scope.searchform.end_time_date.format('YYYY-MM-DD');
        }

        var para = {
            //'order_code' : scope.searchform.order_code,
            'start_time' : s + ' 00:00:00',
            'end_time' : e + ' 23:59:59',
        };

        filename = '收入报表' + '[' + s + '~' + e + ']';

        scope.searchState = true;
        $resource('/api/as/wc/usedrecord/nopcincomelist', {}, {}).save(para, function(res){
            console.log(res);

            scope.searchState = false;

            if (res.errcode !== 0) {
                alert(res.errmsg);
                return;
            }

            results = res.data;

            for(var i  = 0; i < results.length; i++){
                var tmp = results[i];

                //卖其他机构的产品，不显示微信手续费。
                // if(tmp.myproduct == '1'){
                //     tmp.sxf_price = 0;
                // }

                if(angular.isUndefined(statistics[tmp.sale_code])){
                    statistics[tmp.sale_code] = {
                        'name' : tmp.title,
                        'sale_code' : tmp.sale_code,
                        'total_price' : tmp.total_price,    //实际收入
                        //'pay_fee' : tmp.pay_fee,            //支付金额 
                        'one_price' : tmp.one_price,        //一级分佣
                        'two_price' : tmp.two_price,        //二级分佣
                        'profit_wb' : tmp.profit_wb,        //分销他人产品成本
                        'sxf_price' : tmp.sxf_price,        //微信手续费
                        'sys_price' : tmp.sys_price,        //系统分佣
                        'voucher_price' : tmp.voucher_price,    //优惠券金额
                    };
                }else{
                    statistics[tmp.sale_code].total_price += tmp.total_price;
                    statistics[tmp.sale_code].one_price += tmp.one_price;
                    statistics[tmp.sale_code].two_price += tmp.two_price;
                    statistics[tmp.sale_code].profit_wb += tmp.profit_wb;
                    statistics[tmp.sale_code].sxf_price += tmp.sxf_price;
                    statistics[tmp.sale_code].sys_price += tmp.sys_price;
                    statistics[tmp.sale_code].voucher_price += tmp.voucher_price;
                }
            }

            var total = {
                'total_price' : 0,
                'one_price' : 0,
                'two_price' : 0,
                'profit_wb' : 0,
                'sxf_price' : 0,
                'sys_price' : 0,
                'voucher_price' : 0,
            };

            angular.forEach(statistics, function (value, key) {
                angular.forEach(value, function (v, k) {
                    if(angular.isNumber(v)){
                        value[k] = v.toFixed(2);
                    }
                });
                scope.objs.push(value);
            });

            for(var i = 0; i < scope.objs.length; i++){
                var tmp = scope.objs[i];
                angular.forEach(tmp, function (v, k) {
                    total[k] += parseFloat(v);
                });
            }

            angular.forEach(total, function (v, k) {
                if(angular.isNumber(v)){
                    total[k] = v.toFixed(2);
                }
            });

            scope.total = total;
        });
	}
	scope.load();

	scope.toExcel = function(){
			
		var labels = ['code', 'title', 'create_time', 'hexiao_time', 
		'name', 'mobile', 'cardno', 'uid_nickname', 'seller_remark', 
		'guide_price', 'num', 'des_num', 'total_guide',
		'pay_fee', 'total_price', 'nickname', 'one_price', 'nickname1', 'two_price', 'profit_wb', 
		'sxf_price', 'sys_price', 'voucher_price'];
        var arr = [];
        for(var i = 0; i< results.length; i++){
            var tmp = results[i];
            var haha = [];
            for(var j = 0; j < labels.length; j++){
                haha.push(tmp[labels[j]]);
            }

            if(tmp.myproduct == 1 && tmp.sale_belong == dlq.obj.company_code){
                haha.push('群盟');
            }else{
                haha.push('自己');
            }

            arr.push(haha);
        }
        arr.splice(0, 0, ['订单编号', '订单名称', '下单时间' , '分佣时间', 
        	'姓名', '电活', '身份证', '下单人微信昵称', '卖家备注', 
        	'单价', '购买数量', '核销数量', '总价', 
        	'支付金额', '实际收入', '一级昵称', '一级分佣', '二级昵称', '二级分佣', '分销他人产品成本' ,
        	'微信手续费', '系统分佣', '优惠券金额', '收入来自']);

        ExportUtil.toExcel(arr, filename);

	}
	

		}
	};
};

