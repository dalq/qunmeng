module.exports = function ($resource, $state, $http, $q, $modal) {
	return {
		restrict: 'AE',
		template: require('../views/d_book.html'),
		replace: true,
		scope: {
			//'p' : '=',
			//'other': '=',
			//'state' : '=',
			'str2date' : '=',
			'date2str' : '=',
		},
		link: function (scope, elements, attrs) {

			console.log('income');

    scope.searchState = false;

    //保存的数据
    var results = [];

    //统计
    var statistics = {};

    scope.objs = [];

    var filename = '';


	scope.vm = {
        //'date' : new Date(),
        'options' : {
            format: "YYYY-MM-DD",
            locale : 'zh-cn',
            ignoreReadonly : true,
        }
    };

	scope.searchform = {
		'start_time_date' : scope.date2str(new Date()),
		'end_time_date' : scope.date2str(new Date()),
	};

    /* 分页
       * ========================================= */
    scope.maxSize = 5;            //最多显示多少个按钮
    scope.bigCurrentPage = 1;      //当前页码
    scope.itemsPerPage = 10;         //每页显示几条

    scope.load = function () {

        statistics = {};

        filename = '';

        results = [];

        scope.objs = [];

        scope.total = {};

    	var s = '';
        var e = '';

        if (typeof scope.searchform.start_time_date === 'string') {
            s = scope.searchform.start_time_date;
        } else {
            s = scope.searchform.start_time_date.format('YYYY-MM-DD');
        }


        if (typeof scope.searchform.end_time_date === 'string') {
            e = scope.searchform.end_time_date;
        } else {
            e = scope.searchform.end_time_date.format('YYYY-MM-DD');
        }

        var para = {
            //'order_code' : scope.searchform.order_code,
            'start_time' : s + ' 00:00:00',
            'end_time' : e + ' 23:59:59',
        };

        filename = '预订报表' + '[' + s + '~' + e + ']';

        scope.searchState = true;
        $resource('/api/as/wc/productorder/noorderlist', {}, {}).save(para, function(res){
            console.log(res);
            scope.searchState = false;
            if (res.errcode !== 0) {
                alert(res.errmsg);
                return;
            }

            results = res.data;

            for(var i  = 0; i < results.length; i++){
                var tmp = results[i];

                if(angular.isUndefined(statistics[tmp.product_code])){
                    statistics[tmp.product_code] = {
                        'name' : tmp.title,
                        'product_code' : tmp.product_code,
                        'count' : 1,
                        'num' : tmp.num,
                        'used' : tmp.used,
                        'back' : tmp.back,
                    };
                }else{
                    statistics[tmp.product_code].count += 1;
                    statistics[tmp.product_code].num += tmp.num;
                    statistics[tmp.product_code].used += tmp.used;
                    statistics[tmp.product_code].back += tmp.back;
                }
            }

            var total = {
                'count' : 0,
                'num' : 0,
                'used' : 0,
                'back' : 0,
            };

            angular.forEach(statistics, function (value, key) {
                scope.objs.push(value);
            });

            for(var i = 0; i < scope.objs.length; i++){
                var tmp = scope.objs[i];
                angular.forEach(tmp, function (v, k) {
                    total[k] += parseInt(v);
                });
            }

            scope.total = total;

            

        });

	}
	scope.load();




	scope.toExcel = function(){

		var labels = ['code', 'title', 'create_time', 'pay_time',
		'name', 'mobile', 'cardno', 'group', 'uid_nickname', 'remark', 'seller_remark', 
		'guide_price', 'pay_fee', 'num', 'used', 'back',
		'pay_trade_no', 
		'nickname', 'profit', 'nickname1', 'profit1', 'voucher_price'];
        var arr = [];
        for(var i = 0; i< results.length; i++){
            var tmp = results[i];
            var haha = [];
            for(var j = 0; j < labels.length; j++){
                haha.push(tmp[labels[j]]);
            }
            arr.push(haha);
        }
        arr.splice(0, 0, ['订单编号', '订单名称', '下单时间' , '支付时间', 
        	'姓名', '电活', '身份证', '团期', '下单人微信昵称', '买家备注', '卖家备注', 
        	'单价', '支付金额', '购买数量', '核销数量', '退票数量', 
        	'支付流水号', 
            '一级昵称', '一级分佣', '二级昵称', '二级分佣', '优惠券金额'
            ]);

        ExportUtil.toExcel(arr, filename);

	}
	

		}
	};
};

