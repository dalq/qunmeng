 /**
 * 子模块路由 此模块有时间需要重构
 * DHpai
 */

var router = function($urlRouterProvider, $stateProvider){


 	$stateProvider


    //机构列表(树表格结构)
    .state('app.officeList', {
        url: "/office/officelist",
        views: {
            'main@' : {
                template : require('./views/office.html'),
                controller : 'officeList',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
            officeList : function(officeservice){
                return officeservice.officeList();
            },
            officeInfo : function(officeservice){
                return officeservice.officeInfo();
            },
            officeSave : function(officeservice){
                return officeservice.officeSave();
            },
            delOffice : function(officeservice){
                return officeservice.delOffice();
            },
            areaList : function(officeservice){
                return officeservice.areaList();
            },
            addUser : function(officeservice){
                return officeservice.addUser();
            },
            roleList : function(officeservice){
                return officeservice.roleList();
            },
            sysAreaList : function(officeservice){
                return officeservice.sysAreaList();
            },
            tree : function (){
                return true;
            }
        }
    })

    //机构列表2(非树表格结构)
    .state('app.officeListNoTree', {
        url: "/officelist/notree",
        views: {
            'main@' : {
                template : require('./views/office.html'),
                controller : 'officeList',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
            officeList : function(officeservice){
                return officeservice.officeNoTreeList();
            },
            officeInfo : function(officeservice){
                return officeservice.officeInfo();
            },
            officeSave : function(officeservice){
                return officeservice.officeSave();
            },
            delOffice : function(officeservice){
                return officeservice.delOffice();
            },
            areaList : function(officeservice){
                return officeservice.areaList();
            },
            addUser : function(officeservice){
                return officeservice.addUser();
            },
            roleList : function(officeservice){
                return officeservice.roleList();
            },
            sysAreaList : function(officeservice){
                return officeservice.sysAreaList();
            },
            tree : function (){
                return false;
            }
        }
    })

    //菜单列表
    .state('app.officeInfoList', {
        url: "/office/officeinfo/:code",
        views: {
            'main@' : {
                template : require('./views/officeinfo.html'),
                controller : 'officeInfoList',
            }
        },
        resolve:{
            areaList : function(officeservice){
                return officeservice.areaList();
            },
            officeInfo : function(officeservice){
                return officeservice.officeInfo();
            },
            roleList : function(officeservice){
                return officeservice.roleList();
            },
            officeSave : function(officeservice){
                return officeservice.officeSave();
            }
        }
    })

    //菜单列表
    .state('app.dockingSystemList', {
        url: "/dockingSystem/list",
        views: {
            'main@' : {
                template : require('./views/dockingSystemList.html'),
                controller : 'dockingSystemList',
            }
        },
        resolve:{
        }
    })

    //本账号对接系统列表
    .state('app.dockingParamsList', {
        url: "/dockingParams/list",
        views: {
            'main@' : {
                template : require('./views/dockingParamsList.html'),
                controller : 'dockingParamsList',
            }
        },
        resolve:{
        }
    })


	;

};

module.exports = router;