/**
 * 子模块service
 * DHpai
 */
var service = function($resource, $q, $state, $modal){
    
    //机构列表(树表格结构)
    var officeList = '/api/ac/sc/office/getList';
    //机构列表(非树表格结构)
    var officeNoTreeList = '/api/ac/sc/office/getNoTreeList';
    //ID查询机构
    var officeInfo = '/api/as/sc/office/getById';
    //保存机构
    var officeSave = '/api/ac/sc/office/saveOffice';
    //删除机构
    var delOffice = '/api/ac/sc/office/deleteOffice';
    //城市列表
    var areaList = '/api/as/sc/area/getList';
    //加载运营商代码
    var sysAreaList = '/api/as/sc/dict/dictbytypelist';
    //添加机构同名用户
    var addUser = '/api/ac/sc/systemUserService/addUser';
    //角色列表-创建机构同名用户时选择角色
    var roleList = '/api/as/sc/role/getForUserList';

    return {
        officeList : function(){
            return $resource(officeList, {}, {});
        },
        officeNoTreeList : function(){
            return $resource(officeNoTreeList, {}, {});
        },
        officeInfo : function(){
            return $resource(officeInfo, {}, {});
        },
        officeSave : function(){
            return $resource(officeSave, {}, {});
        },
        delOffice : function(){
            return $resource(delOffice, {}, {});
        },
        areaList : function(){
            return $resource(areaList, {}, {});
        },
        addUser : function(){
            return $resource(addUser, {}, {});
        },
        roleList : function(){
            return $resource(roleList, {}, {});
        },
        sysAreaList : function(){
            return $resource(sysAreaList, {}, {});
        },
    };

};

module.exports = service;