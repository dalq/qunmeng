var App = angular.module('office', []);

App.config(require('./router'));

//service
App.factory('officeservice', require('./service'));

//Controllers
App.controller('officeList', require('./controllers/officelist'));
App.controller('officeInfoList', require('./controllers/officeinfo'));
App.controller('officeBindInfo', require('./controllers/officeBindInfo'));
App.controller('dockingSystemList',require('./controllers/dockingSystemList'));
App.controller('dockingSystemAddCompany',require('./controllers/dockingSystemAddCompany'));
App.controller('dockingParamsList',require('./controllers/dockingParamsList'));
App.controller('dockingParamsInfo',require('./controllers/dockingParamsInfo'));
App.controller('dockingCompanyList',require('./controllers/dockingCompanyList'));
App.controller('officeRelation',require('./controllers/officeRelation'));

//Directive
App.directive('officeloadover',require('./directives/officeloadover'));


module.exports = App;