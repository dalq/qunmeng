module.exports = function ($scope, $state, $resource, $modal, toaster){
    
        $scope.currentPage = 1;		//当前页码
        $scope.itemsPerPage = 10;	//每页显示几条
        $scope.searchform = {};
        //卡列表
        $scope.loadlist = function (){
            var para = {
                'pageNo': $scope.currentPage,
                'pageSize': $scope.itemsPerPage
            }
            angular.extend(para, $scope.searchform);
            $resource('/api/as/sc/sysdocking/companyUserList', {}, {}).save(para, function (res) {
                if (res.errcode === 0){
                    $scope.sysList = res.data;
                    for (var index = 0; index < $scope.sysList.length; index++) {
                        if ($scope.sysList[index].docking_system_params) {
                            var element = eval('(' + $scope.sysList[index].docking_system_params + ')');
                            // $scope.sysList[index].paramsMap = [];
                            // for (var key in element) {
                            //     if (object.hasOwnProperty(key)) {
                            //         var child = element[key];
                            //         $scope.sysList[index].paramsMap.push({name: key, value: element[key]})
                            //     }
                            // }
                            $scope.sysList[index].paramsMap = element;
                            console.log($scope.sysList)
                        } else {
                            $scope.sysList[index].paramsMap = {};
                        }
                    }
                    console.log($scope.sysList)
                    $scope.totalItems = res.data.totalRecord;
                } else {
                    toaster.error({title: '', body: res.errmsg});
                }
            });
        }
        $scope.loadlist();
    
        $scope.setParams = function (sysInfo){
            var modalInstance = $modal.open({
                template: require('../views/dockingParamsInfo.html'),
                controller: 'dockingParamsInfo',
                size: 'lg',
                resolve: {
                    sysInfo : function () {
                        return sysInfo;
                    }
                }
            });
            //关闭模态框刷新页面
            modalInstance.result.then(function() {
                $scope.loadlist();
            });
        }
    
    };