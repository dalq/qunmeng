module.exports = function ($scope, $resource, $modalInstance, sysInfo, toaster){
    
    $scope.sysInfo = angular.copy(sysInfo);
    $scope.companyInfo = {};
    
    //添加机构
    $scope.ok = function (){
        if(!$scope.companyInfo.company_code){
            toaster.warning({title: '', body: '请填写机构编号'});
            return false;
        }
        var para = {
            company_code: $scope.companyInfo.company_code
        };
        angular.extend(para, $scope.sysInfo);
        $resource('/api/as/sc/sysdocking/insertCompanyUser', {}, {}).save(para, function (res) {
            if (res.errcode === 0) {
                toaster.success({title: '', body: '操作成功'});
                $modalInstance.close();
            } else {
                toaster.error({title: '', body: res.errmsg});
            }
        });
    }

	//取消
    $scope.close = function () {
        $modalInstance.dismiss('cancel');
    }

};