module.exports = function($scope, $modal, $resource, $stateParams, areaList, officeInfo, roleList, officeSave, toaster){


	//取消=重置
    $scope.cancel = function(){
		$scope.tabname = {'name': '添加机构', 'active': false };
		$scope.status = {'active' : true, 'addSub': false};
		$scope.obj = {};
    };

	init();
	//初始化机构列表
	function init(){
		$scope.cancel();
		$("#treetable").hide();
		$resource('/api/ac/sc/office/getOfficeList', {}, {}).save({}, function(res){
			if (res.errcode === 0) {
				$scope.office_list = res.data;
				$scope.loadOfficeMsg();
			} else {
				toaster.error({title: '', body: res.errmsg});
			}
		});
	}

	//加载区域列表
	$scope.loadOfficeMsg = function (){
		areaList.save({}, function(res){
			if (res.errcode === 0) {
				$scope.cityList = res.data;
			} else {
				toaster.error({title: '', body: res.errmsg});
			}
		});
	}

    //保存机构
    $scope.save = function(){
		//----------表单验证---------
		if(!checkInfo()) return;
		//---------------------------
		if($scope.status.addSub){
			$scope.obj.rolelist = [];
			angular.forEach($scope.role_list, function(item){
				$scope.obj.rolelist.push(item.id);
			});
		}
		$scope.obj.type = '1';
		officeSave.save($scope.obj, function(res){
			if(res.errcode === 0){
				init();
			}else{
				toaster.error({title: '', body: res.errmsg});
			}
		});
    };

	//开启或停用机构
    // $scope.openANDstop = function(item){
	// 	if(confirm('确认对机构进行此操作吗?\r\n此操作会停用机构下全部用户')==true){
	// 		var para = {
	// 			'id': item.id,
	// 			'useable': item.USEABLE
	// 		}
	// 		$resource('/api/ac/sc/office/officeUseable', {}, {}).save(para, function(res){
	// 			if(res.errcode === 0){
	// 				item.USEABLE = item.USEABLE == '1' ? '0' : '1';
	// 			}else{
	// 				toaster.error({title: '', body: res.errmsg});
	// 			}
	// 		});
	// 	}
	// };

	//修改界面
	$scope.update = function (item) {
		if(item.flag == 1){
			$scope.tabname = {'name': '修改机构', 'active': true};
			officeInfo.save(item, function (res) {
				if (res.errcode === 0) {
					$scope.obj = res.data;
				} else {
					toaster.error({title: '', body: res.errmsg});
				}
			});
		}
	};

	//添加界面
	$scope.add = function (item) {
		if($scope.tabname.name == '修改机构'){
			return;
		}
		$scope.status.addSub = true;
		var sort = $("#treetable").find("tr[pid=" + item.id + "]:last").attr("sort") || '0';
		$scope.obj = {
			'set_user': true,
			'super_name': item.name,
			'parent_id': item.id,
			'area_id': item.area_id,
			'sys_area': item.sys_area,
			'sort': parseInt(sort) + 30
		};
		$scope.selected_role = $stateParams.code;
		//点击添加机构时加载角色列表
		$scope.loadrolelist(item.id);
	};

	//加载角色列表-创建机构同名用户时选择角色
	$scope.loadrolelist = function () {
		$resource('/api/as/sc/role/getUserByIdsList', {}, {}).save({'ids': $stateParams.code}, function (res) {
            if (res.errcode === 0) {
				$scope.role_list = res.data;
			} else {
				toaster.error({title: '', body: res.errmsg});
			}
        });
	};
	
	//验证提交的用户信息---暂时只校验手机号
	function checkInfo(){
		if(!$scope.obj.name){
			toaster.warning({title: '', body: '机构名称不能为空'});
			return false;
		}
		if($scope.obj.mobile && !/^1\d{10}$/.test($scope.obj.mobile)){
			toaster.warning({title: '', body: '手机号格式不正确'});
			return false;
		}
		if($scope.role_list.length == 0){
			toaster.warning({title: '', body: '无默认角色不可添加分销商'});
			return false;
		}
		return true;
	}

};