module.exports = function($scope, $modal, $resource, officeList, officeInfo, officeSave, sysAreaList, tree, 
						  delOffice, areaList, addUser, roleList){

	$scope.obj = {};			//机构对象
	$scope.status = {
		'active' : true,
		'code_flag': false,
		'flag': false,
		'tree': tree,
		'office_flag': '0'
	};

	init();
	loadOfficeMsg();
	//初始化机构列表
	function init(){
		$scope.status.active = true;
		$scope.tabname = {'name': '添加机构', 'active': false };
		$("#treetable").hide();
		officeList.save({}, function(res){
			if (res.errcode === 0) {
				if(tree){
					$scope.office_list = res.data;
				} else {
					$scope.office_list = res.data.results;
					$scope.status.office_flag = res.data.params.authority;
					$scope.searchform = {};
					$scope.parentOffice = [
						{'code': res.data.params.ocode,'id': res.data.params.oid, 'name': res.data.params.oname}
					];
					$scope.parentsLength = 0;
					$scope.page = {
						'currentPage': 1,
						'pageSize': 10,
						'totalItems': res.data.totalRecord
					}
				}
				if($scope.office_list[0].uid == '1' || res.data.params.uid == '1'){
					$scope.tabname.name = '添加运营商';
				}
			} else {
				alert(res.errmsg);
			}
		});
	}

	//加载区域列表和运营商编码
	function loadOfficeMsg(){
		areaList.save({}, function(res){
			if (res.errcode === 0) {
				$scope.cityList = res.data;
			} else {
				alert(res.errmsg);
			}
		});
		sysAreaList.save({'type': 'sys_company_sys_area'}, function(res){
			if (res.errcode === 0) {
				$scope.sys_area_list = res.data;
			} else {
				alert(res.errmsg);
			}
		});
	}

	//修改界面
	$scope.update = function (item) {
		item.flag = tree ? item.flag : $scope.status.office_flag;
		if(item.flag != 1){
			return;
		}
		$scope.tabname = { 'name': '修改机构', 'active': true };
		officeInfo.save(item, function (res) {
			if (res.errcode === 0) {
				$scope.obj = res.data;
				if($scope.obj.sys_area){
					$scope.status.flag = true;
				}else{
					$scope.status.flag = false;
				}
			} else {
				alert(res.errmsg);
			}
		});
	};

    //保存机构
    $scope.save = function(){
		if($scope.obj.addSub){
			$scope.obj.rolelist = []
			for(var i = 0; i < $scope.role_list.length; i++){
				if($scope.role_list[i].active){
					$scope.obj.rolelist.push($scope.role_list[i].id);
				}
			}
		}
		officeSave.save($scope.obj, function(res){
			if(res.errcode === 0){
				if(tree){
					init();
				} else {
					$scope.cancel();
					$scope.pageDown($scope.currentPage);
				}
			}else{
				alert(res.errmsg);
			}
		});
    };

    //取消=重置
    $scope.cancel = function(){
		if($scope.office_list[0].uid == '1'){
			$scope.tabname = { 'name': '添加运营商', 'active': false };
		} else {
			$scope.tabname = { 'name': '添加机构', 'active': false };
		}
		$scope.status.active = true;
		$scope.obj = {};
    };

	//删除
    $scope.delete = function(item){
		if(confirm('确认要删除此机构吗？\r\n此操作会删除机构下全部用户')==true){
			delOffice.save(item, function(res){
				if(res.errcode === 0){
					if(tree){
						init();
					} else {
						$scope.cancel();
						$scope.pageDown($scope.currentPage);
					}
				}else{
					alert(err.errmsg);
				}
			});
		}
    };

	//添加界面---index=0时表示点击的时页面标签进入此方法,否则是点击添加下级进入
	$scope.add = function (item,index) {
		if($scope.tabname.name == '修改机构'){
			return;
		}
		$scope.obj = {};
		var sort = '';
		if(tree){
			if(item.uid == '1' && index == 0){
				//一定是点击标签页的添加运营商才会进入这里
				$scope.status.code_flag = true;
				$scope.status.flag = false;
				$scope.obj.parent_id = '0';
				sort = $("#treetable").find("tr[pid='0']:last").attr("sort");
			} else {
				//点击机构后面的添加下级,进入这里
				if(item.sys_area){
					$scope.obj.sys_area = item.sys_area;
					$scope.status.flag = true;
				} else {
					$scope.status.flag = false;
				}
				$scope.obj.addSub = true;
				$scope.obj.super_name = item.name;
				$scope.obj.parent_id = item.id;
				$scope.obj.area_id = item.area_id;
				$scope.obj.grade = parseInt(item.grade)+1+'';
				sort = $("#treetable").find("tr[pid="+item.id+"]:last").attr("sort");
				
			}
		} else {
			$scope.obj.addSub = true;
			if(index == 0){
				$scope.obj.super_name = $scope.parentOffice[$scope.parentsLength].name;
				$scope.obj.parent_id = $scope.parentOffice[$scope.parentsLength].id;
				officeInfo.save({'id': $scope.parentOffice[$scope.parentsLength].id}, function (res) {
					if (res.errcode === 0) {
						$scope.obj.sys_area = res.data.sys_area;
						$scope.obj.area_id = res.data.area_id;
						$scope.obj.grade = parseInt(res.data.grade)+1+'';
					} else {
						alert(res.errmsg);
					}
				});
			} else {
				$scope.obj.parent_id = item.id;
				$scope.obj.super_name = item.name;
				$scope.obj.sys_area = item.sys_area;
				$scope.obj.area_id = item.area_id;
				$scope.obj.grade = parseInt(item.grade)+1+'';
			}
		}
		sort = sort || '0';
		$scope.tabname.active = true;
		$scope.obj.sort = parseInt(sort)+30;
		$scope.obj.type = '1';
		$scope.obj.USEABLE = '1';
		//点击添加机构时加载角色列表
		$scope.loadrolelist(item.id);
	};

	//设置机构绑定信息
    $scope.bindInfo = function(office){
		var modalInstance = $modal.open({
			template: require('../views/officeBindInfo.html'),
			controller: 'officeBindInfo',
			size: 'lg',
			resolve: {
				item: function () {
					return office;
				}
			}
		});
		//关闭模态框刷新页面
		modalInstance.result.then(function() {
			
		});
	};

	//设置机构绑定信息
    $scope.createRelation = function(office){
		var modalInstance = $modal.open({
			template: require('../views/officeRelation.html'),
			controller: 'officeRelation',
			size: 'lg',
			resolve: {
				item: function () {
					return office;
				}
			}
		});
		//关闭模态框刷新页面
		modalInstance.result.then(function() {
			
		});
	};


	//加载角色列表-创建机构同名用户时选择角色
	$scope.loadrolelist = function (id) {
		roleList.save({'company_id': id}, function (res) {
			if (res.errcode === 0) {
				$scope.role_list = res.data;
			} else {
				alert(res.errmsg);
			}
		});
	};

	//非树表格查看下级
	$scope.selectDown = function (item){
		$scope.parentOffice.push({'code': item.code, 'id': item.id, 'name': item.name});
		$scope.parentsLength = $scope.parentOffice.length - 1;
		var para = {
			'pageNo': 1,
			'pageSize': $scope.page.pageSize,
			'parent_id': item.id
		}
		$scope.loadData(para);
	};

	//非树表格查询
	$scope.searchData = function (){
		var para = {
			'pageNo': $scope.page.currentPage,
			'pageSize': $scope.page.pageSize,
			'name': $scope.searchform.name,
			'parent_id': $scope.parentOffice[$scope.parentsLength].id
		}
		$scope.loadData(para);
	};

	//非树表格点击父节点链查询机构
	$scope.selectParent = function (index){
		$scope.parentOffice.splice(index+1);
		$scope.parentsLength = $scope.parentOffice.length - 1;
		var para = {
			'pageNo': 1,
			'pageSize': 10,
			'parent_id': $scope.parentOffice[index].id
		}
		$scope.loadData(para);
	};

	//非树表格查询
	$scope.loadData = function (para){
		officeList.save(para, function(res){
			if (res.errcode === 0) {
				$scope.office_list = res.data.results;
				$scope.status.office_flag = res.data.params.authority;
				$scope.page.totalItems = res.data.totalRecord;
				if(res.data.params.uid == '1'){
					$scope.tabname.name = '添加运营商';
				}
				$scope.page.currentPage = para.pageNo;
			} else {
				alert(res.errmsg);
			}
		});
	};


};