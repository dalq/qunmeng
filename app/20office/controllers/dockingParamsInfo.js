module.exports = function ($scope, $resource, $modalInstance, sysInfo, toaster){
    
    $scope.sysInfo = angular.copy(sysInfo);
    $scope.paramsMapStr = eval('(' + $scope.sysInfo.docking_system_params + ')');
    $scope.paramsArray = $scope.sysInfo.docking_params.split(',');
    $scope.paramsMap = {};
    if ($scope.paramsMapStr) {
        for (var i = 0; i < $scope.paramsArray.length; i++) {
            $scope.paramsMap[$scope.paramsArray[i]] = $scope.paramsMapStr[$scope.paramsArray[i]];
        }
    } else {
        for (var i = 0; i < $scope.paramsArray.length; i++) {
            $scope.paramsMap[$scope.paramsArray[i]] = '';
        }
    }
    //2017年10月30日15:59:30 添加参数说明
    $scope.paramsGuide = {'show': false};
    if(sysInfo.docking_system_params_remark){
        $scope.paramsGuide.show = true;
        $scope.paramsGuide.text = sysInfo.docking_system_params_remark;
    }

    //添加机构
    $scope.ok = function (){
        var para = {};
        angular.extend(para, $scope.sysInfo);
        para.docking_system_params = JSON.stringify($scope.paramsMap);
        $resource('/api/as/sc/sysdocking/updateCompanyUser', {}, {}).save(para, function (res) {
            if (res.errcode === 0) {
                toaster.success({title: '', body: '操作成功'});
                $modalInstance.close();
            } else {
                toaster.error({title: '', body: res.errmsg});
            }
        });
    }

	//取消
    $scope.close = function () {
        $modalInstance.dismiss('cancel');
    }

};