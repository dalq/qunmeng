module.exports = function ($scope, $state, $resource, $modal, toaster){
    
        $scope.currentPage = 1;		//当前页码
        $scope.itemsPerPage = 10;	//每页显示几条
        $scope.searchform = {};
        //卡列表
        $scope.loadlist = function (){
            var para = {
                'pageNo': $scope.currentPage,
                'pageSize': $scope.itemsPerPage
            }
            angular.extend(para, $scope.searchform);
            $resource('/api/as/sc/sysdocking/dockingList', {}, {}).save(para, function (res) {
                if (res.errcode === 0){
                    $scope.sysList = res.data;
                    $scope.totalItems = res.data.totalRecord;
                    $scope.currentPage = pageNo;
                } else {
                    toaster.error({title: '', body: res.errmsg});
                }
            });
        }
        $scope.loadlist();
    
        //添加机构
        $scope.addCompany = function (sysInfo){
            var modalInstance = $modal.open({
                template: require('../views/dockingSystemAddCompany.html'),
                controller: 'dockingSystemAddCompany',
                size: 'lg',
                resolve: {
                    sysInfo : function () {
                        return sysInfo;
                    }
                }
            });
            //关闭模态框刷新页面
            modalInstance.result.then(function() {
                $scope.loadlist();
            });
        }
    
        //机构列表
        $scope.companyList = function (sysInfo){
            var modalInstance = $modal.open({
                template: require('../views/dockingCompanyList.html'),
                controller: 'dockingCompanyList',
                size: 'lg',
                resolve: {
                    sysInfo : function () {
                        return sysInfo;
                    }
                }
            });
            //关闭模态框刷新页面
            modalInstance.result.then(function() {
                $scope.loadlist();
            });
        }

        $scope.setUp = function (sysInfo){
            var para = {
                'id': sysInfo.id
            }
            $resource('/api/as/sc/sysdocking/updateDockingOn', {}, {}).save(para, function (res) {
                if (res.errcode === 0){
                    toaster.success({title: '', body: "操作成功"});
                    $scope.loadlist();
                } else {
                    toaster.error({title: '', body: res.errmsg});
                }
            });
        }
        
        $scope.setDown = function (sysInfo){
            var para = {
                'id': sysInfo.id
            }
            $resource('/api/as/sc/sysdocking/updateDockingOff', {}, {}).save(para, function (res) {
                if (res.errcode === 0){
                    toaster.success({title: '', body: "操作成功"});
                    $scope.loadlist();
                } else {
                    toaster.error({title: '', body: res.errmsg});
                }
            });
        }
    
    };