/**
 * 模态框
 */
module.exports = function($scope, $modalInstance, $resource, item, toaster){

    init();
    //查询机构可被绑定的全部信息
    function init(){
        $scope.searchfrom = {};         //搜索栏显示项
        $resource('/api/ac/tc/officeBindService/getAllList', {}, {}).save({}, function (res) {
            if (res.errcode === 0) {
                $scope.place_list = res.data.placeList;
                $scope.channel_list = res.data.channelList || [];
                $scope.officeBindInfo();
            } else {
                toaster.error({title: '', body: res.errmsg});
            }
        });
    }

    //查询机构已绑定的全部信息
    $scope.officeBindInfo = function() {
        $resource('/api/ac/tc/officeBindService/getBindInfoList', {}, {}).save({'company_code': item.code}, function(res){
            if (res.errcode === 0) {
                $scope.selected_place = res.data.placeList || [];
                $scope.selected_channel = res.data.channelList || [];
                $scope.selected_product = res.data.productPlace || [];
            } else {
                toaster.error({title: '', body: res.errmsg});
            }
        });
    }

    //添加,num=1：添加景区、num=2：添加渠道、num=3：添加可创建景区产品
    $scope.add = function(num){
        var list = [], temp = {};
        if(num == 1){
            temp = $scope.searchfrom.place;
            list = $scope.selected_place;
        } else if(num == 2){
            temp = $scope.searchfrom.channel;
            list = $scope.selected_channel;
        } else if(num == 3){
            temp = $scope.searchfrom.product;
            list = $scope.selected_product;
        }
        if(temp){
            for(var i = 0; i < list.length; i++){
                if(temp.code == list[i].code){
                    toaster.warning({title: '', body: '无法重复添加'});
                    return;
                }
            }
            list.push(temp);
        }
    };

    //删除,num=1：删除景区、num=2：删除渠道、num=3：删除可创建景区产品
    $scope.remove = function(index, num) {
        var list = [];
        if(num == 1){
            list = $scope.selected_place;
        } else if(num == 2){
            list = $scope.selected_channel;
        } else if(num == 3){
            list = $scope.selected_product;
        }
        list.splice(index,1);
    };

    //确认
    $scope.save = function(num){
        var url = '/api/as/sc/office/setOffBindChannel';
        var para = {'id': item.id};
        
        if(num == 1){
            url = '/api/as/sc/office/setOffBindPlace';
            para.code = '';
            angular.forEach($scope.selected_place, function(item){
                para.code += item.code + ',';
            });
            para.code = para.code.substring(0, para.code.length-1);
        } else if(num == 2){
            para.channel_code = '';
            angular.forEach($scope.selected_channel, function(item){
                para.channel_code += item.code + ',';
            });
            para.channel_code = para.channel_code.substring(0, para.channel_code.length-1);
        } else if(num == 3){
            para.product_place_code = '';
            angular.forEach($scope.selected_product, function(item){
                para.product_place_code += item.code + ',';
            });
            para.product_place_code = para.product_place_code.substring(0, para.product_place_code.length-1);
        }

        $resource(url, {}, {}).save(para, function (res) {
            if (res.errcode === 0) {
                toaster.success({title: "", body: '保存成功'});
            } else {
                toaster.error({title: '', body: res.errmsg});
            }
        });
    }

    //取消
    $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
    };

};