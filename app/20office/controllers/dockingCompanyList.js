module.exports = function ($scope, $resource, $modalInstance, sysInfo, toaster){
    
    $scope.sysInfo = angular.copy(sysInfo);
    $scope.companyInfo = {};

    $scope.load = function(){
        $resource('/api/as/sc/sysdocking/dockingSystemCompanyList', {}, {}).save($scope.sysInfo, function (res) {
            if (res.errcode === 0){
                $scope.companyList = res.data;
            } else {
                toaster.error({title: '', body: res.errmsg});
            }
        });
    }
    $scope.load();

    $scope.setUp = function (companyInfo){
        $resource('/api/as/sc/sysdocking/updateDockingCompanyOn', {}, {}).save(companyInfo, function (res) {
            if (res.errcode === 0){
                toaster.success({title: '', body: "操作成功"});
                $scope.load();
            } else {
                toaster.error({title: '', body: res.errmsg});
            }
        });
    }
    
    $scope.setDown = function (companyInfo){
        $resource('/api/as/sc/sysdocking/updateDockingCompanyOff', {}, {}).save(companyInfo, function (res) {
            if (res.errcode === 0){
                toaster.success({title: '', body: "操作成功"});
                $scope.load();
            } else {
                toaster.error({title: '', body: res.errmsg});
            }
        });
    }
    
    //添加机构
    // $scope.ok = function (){
    //     $modalInstance.close();
    // }

	//取消
    $scope.close = function () {
        $modalInstance.close();
        // $modalInstance.dismiss('cancel');
    }

};