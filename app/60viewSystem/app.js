/**
 * 景区系统入口
 * ml
 */
var App = angular.module('viewSystem', []);

App.config(require('./router'));

//service
App.factory('viewSystemService', require('./service'));

//Controllers
App.controller('duplScrPosSign', require('./controllers/duplScrPosSign'));
App.controller('viewSystemMenuList', require('./controllers/viewSystemMenuList'));
App.controller('viewSystemSaleList', require('./controllers/viewSystemSaleList'));
App.controller('viewSystemBulkBuying', require('./controllers/viewSystemBulkBuying'));
App.controller('viewSystemChangeTicket', require('./controllers/viewSystemChangeTicket'));
App.controller('viewSystemMarketingStatistics', require('./controllers/viewSystemMarketingStatistics'));
App.controller('viewSystemHistory', require('./controllers/viewSystemHistory'));
App.controller('viewSystemMember', require('./controllers/viewSystemMember'));
App.controller('viewSystemMemberProduct', require('./controllers/viewSystemMemberProduct'));
App.controller('viewSystemMemberCardMaintain', require('./controllers/viewSystemMemberCardMaintain'));
App.controller('viewSystemMemberCardCancellation', require('./controllers/viewSystemMemberCardCancellation'));
App.controller('viewSystemSetUp', require('./controllers/viewSystemSetUp'));
App.controller('viewBackTicket', require('./controllers/viewBackTicket'));

App.controller('addViewCard', require('./controllers/addViewCard'));
App.controller('addCardBatch', require('./controllers/addCardBatch'));
App.controller('viewCardList', require('./controllers/viewCardList'));
App.controller('viewGoodsList', require('./controllers/viewGoodsList'));
App.controller('viewGoodsInfo', require('./controllers/viewGoodsInfo'));
App.controller('viewOrderList', require('./controllers/viewOrderList'));
App.controller('viewOrderComplexList', require('./controllers/viewOrderComplexList'));
App.controller('orderInfo', require('./controllers/orderInfo'));
App.controller('cardHistoryList', require('./controllers/cardHistoryList'));
App.controller('typeSelectGoods', require('./controllers/typeSelectGoods'));
App.controller('typeAddGoods', require('./controllers/typeAddGoods'));
App.controller('goodsAddType', require('./controllers/goodsAddType'));
App.controller('viewStoreGoods', require('./controllers/viewStoreGoods'));
App.controller('viewSaveGoods', require('./controllers/viewSaveGoods'));
App.controller('viewGoodsCategoryList', require('./controllers/viewGoodsCategoryList'));
App.controller('viewGoodsCategoryInfo', require('./controllers/viewGoodsCategoryInfo'));
App.controller('viewCreateOrderInfo', require('./controllers/viewCreateOrderInfo'));
App.controller('updateCard', require('./controllers/updateCard'));
App.controller('cardDetails', require('./controllers/cardDetails'));
App.controller('bindDevice', require('./controllers/bindDevice'));
App.controller('bindAccount', require('./controllers/bindAccount'));
App.controller('statistics', require('./controllers/statistics'));

App.controller('memberBind', require('./controllers/memberBind'));
App.controller('memberManager', require('./controllers/memberManager'));
App.controller('memberInfo', require('./controllers/memberInfo'));
App.controller('cardPeiod', require('./controllers/cardPeiod'));
App.controller('updateCardPassword', require('./controllers/updateCardPassword'));
App.controller('viewCardCategoryList', require('./controllers/viewCardCategoryList'));
App.controller('viewCardCategoryInfo', require('./controllers/viewCardCategoryInfo'));
App.controller('viewFunctionCardTypeList', require('./controllers/viewFunctionCardTypeList'));
App.controller('viewFunctionCardTypeInfo', require('./controllers/viewFunctionCardTypeInfo'));
App.controller('updateDeposit', require('./controllers/updateDeposit'));
App.controller('orderBack', require('./controllers/orderBack'));
App.controller('viewBackOrderList', require('./controllers/viewBackOrderList'));
App.controller('backInfo', require('./controllers/backInfo'));
App.controller('statisticsCardGoods', require('./controllers/statisticsCardGoods'));

//directive
App.directive('viewCell', require('./directives/viewCell'));

module.exports = App;