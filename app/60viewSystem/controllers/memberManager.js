module.exports = function ($scope, $resource, $modal, toaster, $state) {

    $scope.loadlist = function(pageNo) {
        $scope.searchform.pageNo = pageNo;
		$scope.searchform.pageSize = $scope.itemsPerPage;
        if(ageToDay()){
            $resource('/api/as/vc/viewuser/getAllUserList', {}, {}).save($scope.searchform, function (res) {
                if (res.errcode === 0) {
                    $scope.memberList = res.data.results;
                } else {
                    toaster.error({title: '', body: res.errmsg});
                }
            });
        }
    }

    //年龄转换成时间字符串
    function ageToDay(){
        var min_age = $scope.searchform.min_age;
        var max_age = $scope.searchform.max_age;
        if((min_age || min_age == 0) && (max_age || max_age == 0) && min_age > max_age){
            toaster.warning({title: '', body: '年龄上限不能小于年龄下限'});
            return false;
        }
        if(min_age < 0 || min_age > 200){
            toaster.warning({title: '', body: '最小年龄错误'});
            return false;
        }
        if(max_age < 0 || max_age > 200){
            toaster.warning({title: '', body: '最大年龄错误'});
            return false;
        }
        
        var temp, today = new Date();
        if(min_age || min_age == 0){
            min_age = min_age > 0 ? min_age : 0;
            //最小年龄转换成日期上限
            temp = today.getFullYear() - min_age;
            $scope.searchform.max_day = temp + '-' + (today.getMonth() + 1) + '-' + today.getDate();
        } else {
            delete $scope.searchform.max_day;
        }
        if(max_age || max_age == 0){
            max_age = max_age > 0 ? max_age : 0;
            //最大年龄转换成时间下限
            temp = today.getFullYear() - max_age;
            $scope.searchform.min_day = temp + '-' + (today.getMonth() + 1) + '-' + today.getDate();
        } else {
            delete $scope.searchform.min_day;
        }
        return true;
    }

    init();
    //初始化列表
    function init(){
        $scope.currentPage = 1;		//当前页码
        $scope.itemsPerPage = 10;	//每页显示几条
        $scope.sexList = [
            {'label': '全部', 'value': undefined},
            {'label': '男', 'value': '1'},
            {'label': '女', 'value': '0'}
        ];
		$scope.searchform = {'sex': undefined};
		$scope.loadlist(1);
    }

    //添加用户
    $scope.userInfo = function(flag, member){
        var modalInstance = $modal.open({
            template: require('../views/memberInfo.html'),
            controller: 'memberInfo',
            size: 'lg',
            resolve: {
                member : function () {
                    return member;
                },
                flag : function () {
                    return flag;
                },
                cardInfo : function () {
                    return null;
                }
            }
        });
        //关闭模态框刷新页面
        modalInstance.result.then(function(result) {
            if(member){
                member.name = result.name;
                member.sex = result.sex;
                member.mobile = result.mobile;
                member.smart_card_no = result.smart_card_no;
            } else {
                $scope.loadlist(1);
            }
        });
    }
    
    $scope.sellInfo = function(member){
        $state.go('app.cardDetails', {smart_card_no: member.smart_card_no})
    }

    //删除用户---暂不启用
    // $scope.delete = function(index){
    //     if(confirm('确认删除此用户吗?') == true){
    //         var id = $scope.memberList[index].id;
    //         $resource('/api/as/vc/viewuser/deleteUser', {}, {}).save({'id': id}, function (res) {
    //             if (res.errcode === 0) {
    //                 $scope.memberList.splice(index, 1);
    //                 toaster.success({title: '', body: '删除成功'});
    //             } else {
    //                 toaster.error({title: '', body: res.errmsg});
    //             }
    //         });
    //     }
    // }


};