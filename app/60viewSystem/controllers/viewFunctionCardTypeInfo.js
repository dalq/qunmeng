module.exports = function ($scope, $resource, $modalInstance, FileUploader, item, toaster) {

    //初始化列表
    $scope.status_info = {};
    $scope.searchitem = {};
    $scope.is_new = true;
    $scope.obj = {};
    if (item) {
        $scope.is_new = false;
        $scope.obj = angular.copy(item);
        $scope.status_info.url = '/api/ac/vc/viewCardService/updateFunctionCardType';
        $scope.status_info.msg = '修改分类信息成功';
    } else {
        $scope.obj = {
            'name': '',
            // 'code': '',
            'remark': ''
        };
        $scope.status_info.url = '/api/ac/vc/viewCardService/createFunctionCardType';
        $scope.status_info.msg = '成功添加一条分类信息';
    }

    $scope.searchform = {};
    $scope.change = function () {
        if ($scope.obj.view_code == undefined) {
            return false;
        }
        $resource('/api/ac/vc/viewCardService/cardCategoryListNoPage', {}, {}).save({view_code: $scope.obj.view_code}, function (res) {
            if (res.errcode === 0) {
                $scope.category_list = res.data;
            } else {
                toaster.error({ title: '', body: res.errmsg });
            }
        });
    }

    $resource('/api/ac/vc/viewCommonService/getPlaceByCode', {}, {}).save({}, function (result) {
        if (result.errcode === 0) {
            $scope.viewList = result.data;
            if ($scope.is_new) {
                $scope.obj.view_code = $scope.viewList[0].code;
            }
        } else {
            toaster.error({ title: '', body: result.errmsg });
        }
    });

    //添加景区
    $scope.ok = function () {
        if (!$scope.obj.view_code || !$scope.obj.card_type || !$scope.obj.day_num) {
            toaster.warning({ title: '', body: '分类信息不完整' });
            return;
        }
        $resource($scope.status_info.url, {}, {}).save($scope.obj, function (res) {
            if (res.errcode === 0) {
                toaster.success({ title: '', body: $scope.status_info.msg });
                $modalInstance.close();
            } else {
                toaster.error({ title: '', body: res.errmsg });
            }
        });
    }

    //取消
    $scope.close = function () {
        $modalInstance.dismiss('cancel');
    }

    $scope.$watch('obj.view_code', function (newValue, oldValue) {
        console.log(1,$scope.obj.view_code)
        console.log(newValue, oldValue)
        $scope.change();
    }, true)

};