module.exports = function ($scope, $resource, $modal, toaster) {
    $scope.myKeyup = function (e) {
        //IE 编码包含在window.event.keyCode中，Firefox或Safari 包含在event.which中
        var keycode = window.event ? e.keyCode : e.which;
        if (keycode == 13) {
            $scope.loadlist();
        }
    };
    //景区卡分类列表
    $scope.currentPage = 1;		//当前页码
	$scope.itemsPerPage = 10;	//每页显示几条
	$scope.searchform = {};
	$scope.loadlist = function () {
		$scope.searchform.pageNo = $scope.currentPage;
		$scope.searchform.pageSize = $scope.itemsPerPage;
        $resource('/api/ac/vc/viewCardService/cardCategoryList', {}, {}).save($scope.searchform, function (res) {
            if (res.errcode === 0) {
                $scope.cardCategoryList = res.data.results;
				$scope.totalItems = res.data.totalRecord;
            } else {
                toaster.error({ title: '', body: res.errmsg });
            }
        });
    }
    $scope.loadlist();

    //编辑卡分类
    $scope.updateCardCategory = function (obj) {
        var modalInstance = $modal.open({
            template: require('../views/viewCardCategoryInfo.html'),
            controller: 'viewCardCategoryInfo',
            size: 'lg',
            resolve: {
                item: function () {
                    return obj;
                }
            }
        });
        //关闭模态框刷新页面
        modalInstance.result.then(function () {
            $scope.loadlist();
        });
    }

    //删除卡分类
    $scope.deleteCardCategory = function (id) {
        if (confirm('确认删除本卡类型吗？')) {
            $resource('/api/ac/vc/viewCardService/deleteCardCategory', {}, {}).save({ 'id': id }, function (res) {
                if (res.errcode === 0) {
                    toaster.success({ title: '', body: '卡类型删除成功' });
                    $scope.loadlist();
                } else {
                    toaster.error({ title: '', body: res.errmsg });
                }
            });
        }
    }

};