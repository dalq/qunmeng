module.exports = function ($scope, $resource, $modalInstance, FileUploader, item, toaster) {

    //查找可以被当做赠品的产品
    $scope.getGiftList = function () {
        $resource('/api/ac/vc/viewGoodsService/findGiftGoodsList', {}, {}).save({}, function (res) {
            if (res.errcode === 0) {
                $scope.gift_list = res.data;
                $scope.findGiftGoods();
            } else {
                toaster.error({ title: '', body: res.errmsg });
            }
        });
    }

    init();
    //初始化列表
    function init() {
        $scope.status_info = {};
        $scope.searchitem = {};
        $scope.is_new = true;
        if (item) {
            $scope.is_new = false;
            $scope.obj = angular.copy(item);
            $scope.status_info.msg = '修改产品信息成功';
            $scope.status_info.url = '/api/ac/vc/viewGoodsService/updateGoods';
        } else {
            $scope.obj = {
                'is_pick': '0',
                'is_restore': '0',
                'back_type': '1',
                'type': '1',
                'is_define_price': '0',
                'pay_type': '1',
                'recharge': 0,
                'stock': -1,
                'is_gift': '1'
            };
            $scope.status_info.url = '/api/ac/vc/viewGoodsService/createGoods';
            $scope.status_info.msg = '成功添加一条产品信息';
        }
        $scope.gifts = [];
        $scope.getGiftList();
    }

    $scope.searchform = {};
    $resource('/api/ac/vc/viewGoodsService/findGoodsCategoryList', {}, {}).save({}, function (res) {
        if (res.errcode === 0) {
            $scope.category_list = res.data;
            if ($scope.is_new) {
                $scope.obj.sale_category = $scope.category_list[0].code;
            }
        } else {
            toaster.error({ title: '', body: res.errmsg });
        }
    });
    $resource('/api/ac/vc/viewCommonService/getPlaceByCode', {}, {}).save({}, function (result) {
        if (result.errcode === 0) {
            $scope.viewList = result.data;
            if($scope.is_new){
                $scope.obj.view_code = $scope.viewList[0].code;
            }
        } else {
            toaster.error({ title: '', body: result.errmsg });
        }
    });

    $scope.findGiftGoods = function () {
        $resource('/api/ac/vc/viewGoodsService/findGiftGoods', {}, {}).save({ goods_code: $scope.obj.code }, function (res) {
            if (res.errcode === 0) {
                angular.forEach(res.data, function (info, index) {
                    $scope.gifts.push({
                        'code': info.gift_goods_code,
                        'name': info.name + '    *    ' + info.num,
                        'real_name': info.name,
                        'num': info.num
                    });
                });
            } else {
                toaster.error({ title: '', body: res.errmsg });
            }
        });
    }


    //添加景区产品
    $scope.ok = function () {
        if (!$scope.obj.name) {
            toaster.warning({ title: '', body: '产品信息不完整' });
            return;
        }
        if ($scope.obj.is_define_price == '0' && !$scope.obj.price) {
            toaster.warning({ title: '', body: '产品信息不完整' });
            return;
        }
        if ($scope.obj.price < 0) {
            toaster.warning({ title: '', body: '产品价格不能小于零' });
            return;
        }
        if ($scope.obj.stock == -1 || $scope.obj.stock > 0) {
        } else {
            toaster.warning({ title: '', body: '总库存数要大于零张' });
            return;
        }
        if ($scope.obj.asort < 0) {
            toaster.warning({ title: '', body: '产品排序数不能小于零' });
            return;
        }
        if($scope.is_new){
            $scope.obj.remaining_stock = $scope.obj.stock;
        }
        $scope.obj.gift_sale_code = [];
        angular.forEach($scope.gifts, function (info) {
            $scope.obj.gift_sale_code.push({
                code: info.code,
                num: info.num,
                name: info.real_name
            });
        });
        $resource($scope.status_info.url, {}, {}).save($scope.obj, function (res) {
            if (res.errcode === 0) {
                toaster.success({ title: '', body: $scope.status_info.msg });
                $modalInstance.close();
            } else {
                toaster.error({ title: '', body: res.errmsg });
            }
        });
    }

    //编辑产品时添加的附属产品
    $scope.add = function () {
        if (!$scope.searchitem.gift) {
            toaster.error({ title: '', body: "请选择产品" });
            return false;
        }
        if (!$scope.searchitem.num) {
            toaster.error({ title: '', body: "请填写数量" });
            return false;
        }
        for (var i = 0; i < $scope.gifts.length; i++) {
            if ($scope.gifts[i].code == $scope.searchitem.gift.code) {
                toaster.error({ title: '', body: '无法重复添加相同的附属产品' });
                return;
            }
        }
        $scope.gifts.push({
            'code': $scope.searchitem.gift.code,
            'name': $scope.searchitem.gift.name + '    *    ' + $scope.searchitem.num,
            'real_name': $scope.searchitem.gift.name,
            'num': $scope.searchitem.num
        });
    }

    //删除附属产品
    $scope.remove = function (index) {
        $scope.gifts.splice(index, 1);
    }

    //取消
    $scope.close = function () {
        $modalInstance.dismiss('cancel');
    }

    $scope.$watch('obj.type', function (newValue, oldValue) {
        $scope.obj.pay_type = $scope.obj.type;
        if ($scope.obj.type == '2') {
            $scope.obj.is_pick = '0';
            $scope.obj.is_restore = '0';
            // $scope.obj.is_define_price = '0';
            $scope.obj.is_gift = '0';
        }
    }, true);

    $scope.$watch('obj.is_define_price', function (newValue, oldValue) {
        if ($scope.obj.is_define_price == '1') {
            $scope.obj.recharge = 0;
        }
    }, true);

    $scope.$watch('obj.is_define_price', function (newValue, oldValue) {
        if ($scope.obj.is_define_price == '1') {
            $scope.obj.price = 0;
        }
    }, true);

    $scope.uploader = new FileUploader({
        url: 'http://cl.juyouhx.com/oss.php/oss/webuploader1?topdir=aa&selfdir=bb'
    });

    $scope.uploader.filters.push({
        name: 'imageFilter',
        fn: function (item /*{File|FileLikeObject}*/, options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });

    $scope.uploader.onSuccessItem = function (fileItem, response, status, headers) {
        $scope.obj.img = response.savename;
    };

};