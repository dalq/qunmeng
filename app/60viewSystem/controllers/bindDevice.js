module.exports = function ($scope, $resource, $modalInstance, FileUploader, item, toaster) {

    $scope.device_code_list = [];
    $resource('/api/ac/vc/viewGoodsService/findDeviceList', {}, {}).save({ view_code: item.view_code }, function (res) {
        if (res.errcode === 0) {
            $scope.devicelist = res.data;
            for (let index = 0; index < $scope.devicelist.length; index++) {
                const element = $scope.devicelist[index];
                if (item.device_code.indexOf(element.code) > -1) {
                    element.isSelected = true;
                }
            }
        } else {
            toaster.error({ title: '', body: res.errmsg });
        }
    });

    $scope.isAllSelected = false;
    $scope.$watch('isAllSelected', function (newVal, oldVal) {
        if ($scope.devicelist) {
            for (var index = 0; index < $scope.devicelist.length; index++) {
                var element = $scope.devicelist[index];
                element.isSelected = $scope.isAllSelected;
            }
        }
    }, false);

    $scope.$watch('devicelist', function (newVal, oldVal) {
        if ($scope.devicelist) {
            $scope.device_code_list = [];
            for (var index = 0; index < $scope.devicelist.length; index++) {
                var element = $scope.devicelist[index];
                if (element.isSelected) {
                    $scope.device_code_list.push(element.code)
                }
            }
        }
    }, true);

    //添加景区产品
    $scope.ok = function () {
        if (!$scope.devicelist || !$scope.devicelist.length) {
            toaster.error({ title: '', body: "请选择设备" });
            return false;
        }
        var para = {
            device_code: $scope.device_code_list,
            id: item.id
        }
        $resource('/api/ac/vc/viewGoodsService/updateGoodsCategory', {}, {}).save(para, function (res) {
            if (res.errcode === 0) {
                toaster.success({ title: '', body: '修改成功' });
                $modalInstance.close();
            } else {
                toaster.error({ title: '', body: res.errmsg });
            }
        });
    }

    //取消
    $scope.close = function () {
        $modalInstance.dismiss('cancel');
    }

};