module.exports = function ($scope, $resource, $modal, toaster, $state) {

    $scope.searchform = {};

    //搜索卡面号查询数据
    $scope.loadData = function () {
        if (!$scope.searchform.smart_card_no) {
            toaster.warning({ title: '', body: '请输入要查询的卡面号' });
            return;
        }
        $resource('/api/ac/vc/viewCardService/getCardByCodeList', {}, {}).save($scope.searchform, function (res) {
            if (res.errcode === 0) {
                $scope.cardInfo = res.data[0];
                $scope.cardInfo.len = res.data.length;
            } else {
                toaster.error({ title: '', body: '查不到卡信息' });
            }
        });
    }

    //绑定用户
    $scope.bindUser = function () {
        var member;
        if ($scope.cardInfo.userId) {
            member = { 'id': $scope.cardInfo.userId }
        }
        var modalInstance = $modal.open({
            template: require('../views/memberInfo.html'),
            controller: 'memberInfo',
            size: 'lg',
            resolve: {
                member: function () {
                    return member;
                },
                flag: function () {
                    return true;
                },
                cardInfo: function () {
                    return $scope.cardInfo;
                }
            }
        });
        //关闭模态框刷新页面
        modalInstance.result.then(function (result) {

        });
    }

    //修改卡状态
    $scope.updateStatus = function () {
        var tips = $scope.cardInfo.status == 0 ? '使用状态' : '初始状态';
        if (confirm('确认将卡修改为' + tips + '吗?') == true) {
            var para = {
                'smart_card': $scope.cardInfo.smart_card,
                'status': $scope.cardInfo.status == 0 ? '1' : '0'
            }
            $resource('/api/ac/vc/viewCardService/updateCard', {}, {}).save(para, function (res) {
                if (res.errcode === 0) {
                    $scope.cardInfo.status = para.status;
                    toaster.success({ title: '', body: '卡状态修改成功' });
                } else {
                    toaster.error({ title: '', body: res.errmsg });
                }
            });
        }
    }

    //修改卡有效期
    $scope.updatePeriod = function () {
        var modalInstance = $modal.open({
            template: require('../views/cardPeiod.html'),
            controller: 'cardPeiod',
            size: 'lg',
            resolve: {
                cardInfo: function () {
                    return $scope.cardInfo;
                }
            }
        });
        //关闭模态框刷新页面
        modalInstance.result.then(function (result) {
            $scope.cardInfo.period_start = result.period_start;
            $scope.cardInfo.period_end = result.period_end;
        });
    }

    //修改卡密码
    $scope.updateCardPassword = function (card) {
        var modalInstance = $modal.open({
            template: require('../views/updateCardPassword.html'),
            controller: 'updateCardPassword',
            size: 'md',
            resolve: {
                card: function () {
                    return card;
                },
                type: function () {
                    return '1'
                }
            }
        });
        //关闭模态框刷新页面
        modalInstance.result.then(function () {
            $scope.loadData();
        });
    }

    //修改卡密码
    $scope.updateDeposit = function (card) {
        var modalInstance = $modal.open({
            template: require('../views/updateDeposit.html'),
            controller: 'updateDeposit',
            size: 'md',
            resolve: {
                card: function () {
                    return card;
                }
            }
        });
        //关闭模态框刷新页面
        modalInstance.result.then(function () {
            $scope.loadData();
        });
    }

    $scope.sellInfo = function (card) {
        $state.go('app.cardDetails', { smart_card_no: card.smart_card_no })
    }

};