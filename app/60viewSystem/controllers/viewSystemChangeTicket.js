/**
 * 双屏pos机-产品销售(列表)
 * ml
 */
module.exports = function ($scope, $modal, $state, $resource, toaster, $timeout) {

    $scope.params = {
        device: 'J0101'
    };
    $scope.ticketInfo = null;
    $scope.backMenu = function () {
        $state.go('app.viewSystemMenuList', {});
    }

    // $scope.$watch('params.code', function (value) {
    //     if (!$scope.params.code) {
    //         return false;
    //     }
    //     if ($scope.timeout) $timeout.cancel($scope.timeout); //如果当前时间已经有一个timeout在开启，那么先取消掉这个开启的timeout  
    //     $scope.timeout = $timeout(function () {
    //         /**
    //          * 获取产品
    //          */
    //         $resource('/tktapi/sc/queryService/byCode', {}, {}).save($scope.params, function (res) {
    //             if (res.errcode !== 0) {
    //                 toaster.error({ title: '', body: res.errmsg })
    //                 return false;
    //             }
    //             $scope.ticketInfo = res.data.ticketInfo;
    //         });
    //     }, 800);
    // }, true);

    $scope.search = function () {
        $resource('/tktapi/sc/queryService/byCode', {}, {}).save($scope.params, function (res) {
            if (res.errcode !== 0) {
                toaster.error({ title: '', body: res.errmsg })
                return false;
            }
            $scope.ticketInfo = res.data.ticketInfo;
            $scope.ticketInfo.reqkey = res.data.reqkey;
        });
    }

    $scope.useTicket = function () {
        var map = { "num": $scope.sale.num, "type": $scope.ticketInfo.type, "type_attr": $scope.ticketInfo.type_attr, "reqkey": $scope.ticketInfo.reqkey, 'goods_code':$scope.ticketInfo.goods_code };
        angular.extend(map, $scope.params);
        $resource('/tktapi/sc/destoryService/updateByCode', {}, {}).save(map, function (res) {
            if (res.errcode !== 0) {
                toaster.error({ title: '', body: res.errmsg })
                return false;
            }
            toaster.success({ title: '恭喜(*^__^*)', body: '销票成功' })
        });
    }

    $scope.add = function (index) {
        $scope.saleList[index].num++;
    }
    $scope.remove = function (index) {
        $scope.saleList[index].num--;
    }
};