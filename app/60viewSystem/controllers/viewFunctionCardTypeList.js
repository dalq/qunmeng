module.exports = function ($scope, $resource, $modal, toaster) {
    $scope.myKeyup = function (e) {
        //IE 编码包含在window.event.keyCode中，Firefox或Safari 包含在event.which中
        var keycode = window.event ? e.keyCode : e.which;
        if (keycode == 13) {
            $scope.loadlist();
        }
    };
    $scope.currentPage = 1;		//当前页码
    $scope.itemsPerPage = 10;	//每页显示几条
    //景区产品列表
    $scope.searchform = {};
    $scope.loadlist = function () {
        $resource('/api/ac/vc/viewCardService/functionCardTypeList', {}, {}).save($scope.searchform, function (res) {
            if (res.errcode === 0) {
                $scope.functionCardTypeList = res.data.results;
				$scope.totalItems = res.data.totalRecord;
            } else {
                toaster.error({ title: '', body: res.errmsg });
            }
        });
    }
    $scope.loadlist();

    //创建or编辑景区产品
    $scope.updateFunctionCardType = function (obj) {
        var modalInstance = $modal.open({
            template: require('../views/viewFunctionCardTypeInfo.html'),
            controller: 'viewFunctionCardTypeInfo',
            size: 'lg',
            resolve: {
                item: function () {
                    return obj;
                }
            }
        });
        //关闭模态框刷新页面
        modalInstance.result.then(function () {
            $scope.loadlist();
        });
    }

    //启用
    $scope.start = function (obj) {
        let para = {
            id: obj.id,
            state: '1'
        }
        $resource('/api/ac/vc/viewCardService/updateFunctionCardType', {}, {}).save(para, function (res) {
            if (res.errcode === 0) {
                toaster.success({ title: '', body: '操作成功' });
                $scope.loadlist();
            } else {
                toaster.error({ title: '', body: res.errmsg });
            }
        });
    }

    //停用
    $scope.end = function (obj) {
        let para = {
            id: obj.id,
            state: '0'
        }
        $resource('/api/ac/vc/viewCardService/updateFunctionCardType', {}, {}).save(para, function (res) {
            if (res.errcode === 0) {
                toaster.success({ title: '', body: '操作成功' });
                $scope.loadlist();
            } else {
                toaster.error({ title: '', body: res.errmsg });
            }
        });
    }

    //删除产品
    $scope.deleteFunctionCardType = function (id) {
        if (confirm('确认删除本分类吗？')) {
            $resource('/api/ac/vc/viewCardService/deleteFunctionCardType', {}, {}).save({ 'id': id }, function (res) {
                if (res.errcode === 0) {
                    toaster.success({ title: '', body: '分类删除成功' });
                    $scope.loadlist();
                } else {
                    toaster.error({ title: '', body: res.errmsg });
                }
            });
        }
    }

};