module.exports = function ($scope, $state, $resource, $modal, toaster) {
	$scope.myKeyup = function (e) {
		//IE 编码包含在window.event.keyCode中，Firefox或Safari 包含在event.which中
		var keycode = window.event ? e.keyCode : e.which;
		if (keycode == 13) {
			$scope.loadlist();
		}
	};
	
	$scope.currentPage = 1;		//当前页码
	$scope.itemsPerPage = 10;	//每页显示几条
	//卡列表
	$scope.loadlist = function () {
		var para = {
			'pageNo': $scope.currentPage,
			'pageSize': $scope.itemsPerPage
		}
		angular.extend(para, $scope.searchform);
		$resource('/api/ac/vc/viewBackOrderService/findBackOrderList', {}, {}).save(para, function (res) {
			if (res.errcode === 0) {
				$scope.orderList = res.data.results;
				$scope.totalItems = res.data.totalRecord;
			} else {
				toaster.error({ title: '', body: res.errmsg });
			}
		});
	}
	$scope.loadlist();

	//订单信息
	$scope.orderInfo = function (order) {
		var modalInstance = $modal.open({
			template: require('../views/backInfo.html'),
			controller: 'backInfo',
			size: 'lg',
			resolve: {
				order: function () {
					return order;
				}
			}
		});
		//关闭模态框刷新页面
		modalInstance.result.then(function () {
			$scope.loadlist();
		});
	}

};