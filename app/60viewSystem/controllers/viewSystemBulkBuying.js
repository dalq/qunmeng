/**
 * 双屏pos机-批量购票
 * ml
 */
module.exports = function ($scope, $modal, $state, $resource, toaster) {

    $scope.backMenu = function(){
        $state.go('app.viewSystemMenuList', {});
    }
    $scope.totalPrice = 0;
     $scope.order = {              //订单信息
        pay_type: '0'
    };
    $scope.saleList = [];
    $scope.params = {               //票机获取产品参数
        code: 'J0101',
        sale_category: 'F10'
    }

    /**
     * 获取产品
     */
    $resource('/api/ac/tc/equipmentProductService/getProductListByCode', {}, {}).save($scope.params, function (res) {
        if (res.errcode !== 0) {
            toaster.error({ title: '', body: res.errmsg})
            return false;
        }
        res.data.forEach(function(element) {
            element.num = 0;
        }, this);
        $scope.saleList = res.data;
    });

    /**
     * 更改数量同步订单信息
     */
    $scope.$watch('saleList', function(value){
        $scope.totalPrice = 0;
        $scope.saleList.forEach(function(element) {
            try {
                var num = parseInt(element.num);
                var price = parseInt(element.cost_price);
            } catch (error) {
                toaster.error({title: '(*^__^*) ……', body: '请填入正确数量'});
                return false;
            }
            if ( isNaN(num) || isNaN(price) ) {
                toaster.error({title: '(*^__^*) ……', body: '请填入正确数量'});
                return false;
            }
            $scope.totalPrice += num * price;
        }, this);
    }, true);

    /**
     * 增加产品数量
     */
    $scope.add = function(index){
        $scope.saleList[index].num++;
    }

    /**
     * 减少产品数量
     */
    $scope.remove = function(index){
        $scope.saleList[index].num--;
    }

    /**
     * 重置
     */
    $scope.reset = function(){
        var tempArray = angular.copy($scope.saleList)
        tempArray.forEach(function(element) {
            element.num = 0;
        }, this);
        $scope.saleList = angular.copy(tempArray);
        tempArray = null;
    }

    /**
     * 下单
     */
    $scope.createOrder = function(){
         $scope.order.pay_fee = $scope.totalPrice;
        $scope.order.num = 0;
        $scope.order.sale_list = [];
        $scope.order.device_code = $scope.params.code;
        for (var index = 0; index < $scope.saleList.length; index++) {
			var element = $scope.saleList[index];
			if ( parseInt(element.num) && parseInt(element.num) > 0 ) {
				$scope.order.sale_list.push({ sale_code: element.code, num: parseInt(element.num), pay_fee: parseInt(element.cost_price?element.cost_price:0) * parseInt(element.num) });
				$scope.order.num += parseInt(element.num);
			}
		}
        if ($scope.order.sale_list.length < 1 || !$scope.order.num ) {
			toaster.warning({ title: "提示", body: '您还未选择产品' });
			return false;
		}
		$scope.order.sale_num = $scope.order.sale_list.length;
        $resource('/api/ac/tc/equipmentProductService/createOrder', {}, {}).save($scope.order, function (res) {
            if (res.errcode !== 0) {
                toaster.error({ title: '', body: res.errmsg});
                return false;
            }
            toaster.success({ title: '', body: '下单成功, 请支付!'});
        });
    }

};