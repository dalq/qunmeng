module.exports = function ($scope, $resource, $modalInstance, card, toaster) {

    init();
    //初始化列表
    function init() {
        $scope.card = {};
        $scope.addStatus = true;
        if (card) {
            $scope.addStatus = false;
            $scope.card = card;
        } else {
            $scope.card = {
                is_need_password: '0',
                status: '0',
                deposit: 0,
            }
        }
        $resource('/api/ac/vc/viewCommonService/getPlaceByCode', {}, {}).save({}, function (result) {
            if (result.errcode === 0) {
                $scope.viewList = result.data;
                if (!$scope.addStatus) {
                    for (let index = 0; index < $scope.viewList.length; index++) {
                        const element = $scope.viewList[index];
                        if ($scope.card.view_code == element.code) {
                            $scope.card.view_name = element.name;
                        }
                    }
                }
            } else {
                toaster.error({ title: '', body: result.errmsg });
            }
        });
        $resource('/api/ac/vc/viewCardService/cardCategoryListNoPage', {}, {}).save({}, function (res) {
            if (res.errcode === 0) {
                $scope.category_list = res.data;
                $scope.card.card_type = $scope.category_list[0].code;
            } else {
                toaster.error({ title: '', body: res.errmsg });
            }
        });
    }

    function date2str(d) {
        if (d === undefined) {
            return "";
        }
        var month = (d.getMonth() + 1).toString();
        var day = d.getDate().toString();
        if (month.length == 1) month = '0' + month;
        if (day.length == 1) day = '0' + day;
        return d.getFullYear() + "-" + month + "-" + day;
    }

    $scope.dateOpen = function ($event, item) {
        $event.preventDefault();
        $event.stopPropagation();
        item.opened = true;
    };

    $scope.period_start = {
        'value': date2str(new Date()),
        'opened': false
    }

    $scope.period_end = {
        'value': date2str(new Date()),
        'opened': false
    }


    //添加一卡通
    $scope.ok = function () {
        if ($scope.card.is_need_password != '1') {
            delete $scope.card.password;
        } else {
            if (isNaN($scope.card.password) || $scope.card.password.length != 6) {
                toaster.error({ title: '', body: '请输入六位纯数字密码' });
                return false;
            }
        }
        $scope.card.period_start = (typeof $scope.period_start.value === 'string' ? $scope.period_start.value : date2str($scope.period_start.value));
        $scope.card.period_end = (typeof $scope.period_end.value === 'string' ? $scope.period_end.value : date2str($scope.period_end.value));
        if ($scope.card.smart_card_no && $scope.card.smart_card) {
            $resource('/api/ac/vc/viewCardService/createCard', {}, {}).save($scope.card, function (res) {
                if (res.errcode === 0) {
                    $modalInstance.close();
                } else {
                    toaster.error({ title: '', body: res.errmsg });
                }
            });
        } else {
            toaster.error({ title: '', body: '一卡通信息不完整' });
        }
    }

    //取消
    $scope.close = function () {
        $modalInstance.dismiss('cancel');
    }

};