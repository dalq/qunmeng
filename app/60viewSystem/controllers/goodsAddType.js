module.exports = function ($scope, $resource, $modalInstance, goods, toaster) {
    
        $scope.goods = goods;
        $scope.tempObj = {};
        $scope.typeCodeArr = [];
        $scope.typeCodeMap = {};
        $scope.dataMap = {};
        $resource('/api/ac/vc/viewGoodsService/findTypeListNoPage', {}, {}).save({}, function (res) {
            if (res.errcode === 0) {
                $scope.typeList = res.data;
                for (var index = 0; index < $scope.typeList.length; index++) {
                    var element = $scope.typeList[index];
                    $scope.dataMap[element.code] = element;
                }
                var codeList = $scope.goods.type_code.split(',');
                for (var i = 0; i < codeList.length; i++) {
                    var element = codeList[i];
                    $scope.typeCodeArr.push(angular.copy($scope.dataMap[element]));
                    $scope.typeCodeMap[element] = true;
                }
            } else {
                toaster.error({ title: '', body: res.errmsg });
            }
        });
    
        $scope.add = function(){
            if (!$scope.tempObj.type) {
                toaster.warning({title: "", body: "请选择一个票种"});
                return false;
            }
            if ($scope.typeCodeMap[$scope.tempObj.type.code]) {
                toaster.warning({title: "", body: "请不要选择重复票种"});
                return false;
            }
            $scope.typeCodeArr.push(angular.copy($scope.tempObj.type));
            $scope.typeCodeMap[$scope.tempObj.type.code] = true;
        }
    
        $scope.remove = function(index){
            delete $scope.typeCodeMap[$scope.typeCodeArr[index]];
            $scope.typeCodeArr.splice(index, 1);
        }
    
        //添加一卡通
        $scope.ok = function () {
            if ($scope.typeCodeArr.length < 1) {
                toaster.error({ title: '', body: '请至少选择一个票种' });
                return false;
            }
            var para = {};
            var typeCodeArray = [];
            for (var index = 0; index < $scope.typeCodeArr.length; index++) {
                var element = $scope.typeCodeArr[index];
                typeCodeArray[index] = element.code;
            }
            para = {
                list: typeCodeArray,
                goods_code: $scope.goods.code
            }
            para.list = typeCodeArray;
            $resource('/api/ac/vc/viewGoodsService/updateGoodsType', {}, {}).save(para, function (res) {
                if (res.errcode === 0) {
                    $modalInstance.close();
                } else {
                    toaster.error({ title: '', body: res.errmsg });
                }
            });
        }
    
        //取消
        $scope.close = function () {
            $modalInstance.dismiss('cancel');
        }
    
    };