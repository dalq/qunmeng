module.exports = function ($scope, $resource, $modal, toaster) {
	$scope.myKeyup = function (e) {
		//IE 编码包含在window.event.keyCode中，Firefox或Safari 包含在event.which中
		var keycode = window.event ? e.keyCode : e.which;
		if (keycode == 13) {
			$scope.loadlist();
		}
	};
	//景区产品列表
	$scope.currentPage = 1;		//当前页码
	$scope.itemsPerPage = 10;	//每页显示几条
	$scope.searchform = {};
	$scope.loadlist = function () {
		$scope.searchform.pageNo = $scope.currentPage;
		$scope.searchform.pageSize = $scope.itemsPerPage;
		$resource('/api/ac/vc/viewGoodsService/pcList', {}, {}).save($scope.searchform, function (res) {
			if (res.errcode === 0) {
				$scope.goodslist = res.data.results;
				$scope.totalItems = res.data.totalRecord;
			} else {
				toaster.error({ title: '', body: res.errmsg });
			}
		});
	}
	$scope.loadlist();

	$scope.item_sel = {
		'sale_category': [
			// {'name': '全部', 'value': null},
			// {'name': '雪具', 'value': '1'},
			// {'name': '零食', 'value': '2'}
		],
		'state': [
			{ 'name': '全部', 'value': null },
			{ 'name': '未上架', 'value': '0' },
			{ 'name': '已上架', 'value': '1' },
			{ 'name': '下架', 'value': '2' }
		]
	};
	$resource('/api/ac/vc/viewGoodsService/findGoodsCategoryList', {}, {}).save({}, function (res) {
		if (res.errcode === 0) {
			$scope.item_sel.sale_category = res.data;
			$scope.item_sel.sale_category.unshift({
				code: null,
				name: "-- 全部 --"
			})
		} else {
			toaster.error({ title: '', body: res.errmsg });
		}
	});

	//创建or编辑景区产品
	$scope.updateGoods = function (obj) {
		var modalInstance = $modal.open({
			template: require('../views/viewGoodsInfo.html'),
			controller: 'viewGoodsInfo',
			size: 'lg',
			resolve: {
				item: function () {
					return obj;
				}
			}
		});
		//关闭模态框刷新页面
		modalInstance.result.then(function () {
			$scope.loadlist();
		});
	}

	//创建or编辑景区产品
	$scope.updateType = function (goods) {
		var modalInstance = $modal.open({
			template: require('../views/goodsAddType.html'),
			controller: 'goodsAddType',
			size: 'lg',
			resolve: {
				goods: function () {
					return goods;
				}
			}
		});
		//关闭模态框刷新页面
		modalInstance.result.then(function () {
			$scope.loadlist();
		});
	}

	//删除产品
	$scope.deleteGoods = function (index) {
		if (confirm('确认删除本产品吗？') == true) {
			var id = $scope.goodslist[index].id;
			$resource('/api/ac/vc/viewGoodsService/deleteGoods', {}, {}).save({ 'id': id }, function (res) {
				if (res.errcode === 0) {
					toaster.success({ title: '', body: '产品删除成功' });
					$scope.goodslist.splice(index, 1);
				} else {
					toaster.error({ title: '', body: res.errmsg });
				}
			});
		}
	}

	//景区产品上架
	$scope.setGoodsUp = function (obj) {
		$resource('/api/ac/vc/viewGoodsService/setGoodsUp', {}, {}).save({ 'id': obj.id }, function (res) {
			if (res.errcode === 0) {
				toaster.success({ title: '', body: '产品上架成功' });
				$scope.loadlist($scope.currentPage);
			} else {
				toaster.error({ title: '', body: res.errmsg });
			}
		});
	}

	//景区产品下架
	$scope.setGoodsDown = function (obj) {
		$resource('/api/ac/vc/viewGoodsService/setGoodsDown', {}, {}).save({ 'id': obj.id }, function (res) {
			if (res.errcode === 0) {
				toaster.success({ title: '', body: '产品下架成功' });
				$scope.loadlist();
			} else {
				toaster.error({ title: '', body: res.errmsg });
			}
		});
	}


};