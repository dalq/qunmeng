module.exports = function ($scope, $state, $resource, $modal, toaster) {
    $scope.myKeyup = function (e) {
        //IE 编码包含在window.event.keyCode中，Firefox或Safari 包含在event.which中
        var keycode = window.event ? e.keyCode : e.which;
        if (keycode == 13) {
            $scope.loadlist();
        }
    };
    //卡列表
    $scope.loadlist = function (pageNo) {
        var para = {
            'pageNo': pageNo,
            'pageSize': $scope.itemsPerPage,
            'smart_card_no': $scope.searchform.smart_card_no,
            'batch': $scope.searchform.batch
        }
        $resource('/api/ac/vc/viewCardService/cardHistoryList', {}, {}).save(para, function (res) {
            if (res.errcode === 0) {
                $scope.cardlist = res.data.results;
                $scope.totalItems = res.data.totalRecord;
                $scope.currentPage = pageNo;
            } else {
                toaster.error({ title: '', body: res.errmsg });
            }
        });
    }

    init();
    //初始化列表
    function init() {
        $scope.currentPage = 1;		//当前页码
        $scope.itemsPerPage = 20;	//每页显示几条
        $scope.searchform = {};
        $scope.loadlist(1);
    }

    //卡信息
    $scope.cardInfo = function (card) {
        var modalInstance = $modal.open({
            template: require('../views/addViewCard.html'),
            controller: 'addViewCard',
            size: 'lg',
            resolve: {
                card: function () {
                    return card;
                }
            }
        });
        //关闭模态框刷新页面
        modalInstance.result.then(function () {
            if (!card) {
                $scope.loadlist(1);
            }
        });
    }

    //删除卡
    $scope.delete = function (index) {
        if (confirm('确认删除此卡吗？') == true) {
            var code = $scope.cardlist[index].smart_card
            $resource('/api/ac/vc/viewCardService/deleteCard', {}, {}).save({ 'smart_card': code }, function (res) {
                if (res.errcode === 0) {
                    $scope.cardlist.splice(index, 1);
                } else {
                    toaster.error({ title: '', body: res.errmsg });
                }
            });
        }
    }

};