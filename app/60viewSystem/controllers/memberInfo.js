module.exports = function ($scope, $resource, $modalInstance, toaster, member, flag, cardInfo) {

    //获取用户信息
    $scope.userInfo = function() {
        $resource('/api/as/vc/viewuser/getUserInfo', {}, {}).save({'id': member.id}, function (res) {
            if (res.errcode === 0) {
                $scope.member = res.data;
            } else {
                toaster.error({title: '', body: res.errmsg});
            }
        });
    }

    init();
    //初始化列表
    function init(){
        $scope.date = {
            'lable': date2str2(new Date()),
            'opened': false
        };
        $scope.status = flag;
        if(member){
            $scope.userInfo();
        } else {
            $scope.member = {
                'smart_card_no': cardInfo.smart_card_no,
                'card_code': cardInfo.code,
                'sex': '1'
            };
        }
    }


    //确认
    $scope.ok = function() {
        $scope.member.birthday = date2str2($scope.date.lable);
        $resource('/api/as/vc/viewuser/saveUser', {}, {}).save($scope.member, function (res) {
            if (res.errcode === 0) {
                $modalInstance.close($scope.member);
            } else {
                toaster.error({title: '', body: res.errmsg});
            }
        });
    }

    //取消
    $scope.close = function () {
        $modalInstance.dismiss('cancel');
    }



    //打开日历空间,选择时间
    $scope.dateOpen = function ($event, item) {
        $event.preventDefault();
        $event.stopPropagation();
        item.opened = true;
    };

    //日期转字符串
    function date2str2(d){
        if (d === undefined) {
            return "";
        } else if(typeof d === 'string'){
            return d;
        }
        var month = (d.getMonth() + 1).toString();
        var day = d.getDate().toString();
        if (month.length == 1) month = '0' + month;
        if (day.length == 1) day = '0' + day;
        return d.getFullYear() + "-" + month + "-" + day;
    }

};