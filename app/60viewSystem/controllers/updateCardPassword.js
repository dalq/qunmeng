module.exports = function ($scope, $resource, $modalInstance, card, type, toaster) {
    $scope.cardInfo = card;

    //添加一卡通
    $scope.ok = function () {
        if (isNaN($scope.cardInfo.password_old) || $scope.cardInfo.password_old.length != 6) {
            toaster.error({ title: '', body: '请输入六位纯数字密码' });
            return false;
        }
        if (isNaN($scope.cardInfo.password_new) || $scope.cardInfo.password_new.length != 6) {
            toaster.error({ title: '', body: '请输入六位纯数字密码' });
            return false;
        }
        if (isNaN($scope.cardInfo.password_new_twice) || $scope.cardInfo.password_new_twice.length != 6) {
            toaster.error({ title: '', body: '请输入六位纯数字密码' });
            return false;
        }
        let para = {
            smart_card: $scope.cardInfo.smart_card,
            password_old: $scope.cardInfo.password_old,
            password_new: $scope.cardInfo.password_new
        }
        $resource('/api/ac/vc/viewCardService/updateCardPassword', {}, {}).save(para, function (res) {
            if (res.errcode === 0) {
                toaster.success({ title: '', body: '修改成功' });
                $modalInstance.close();
            } else {
                toaster.error({ title: '', body: res.errmsg });
            }
        });
    }

    //取消
    $scope.close = function () {
        $modalInstance.dismiss('cancel');
    }

    $scope.is_invail = false;
    $scope.is_same = true;
    $scope.$watch('cardInfo', function (newvalue, oldvalue) {
        if ($scope.cardInfo.password_new != $scope.cardInfo.password_new_twice) {
            $scope.is_same = false;
            $scope.is_invail = false;
        } else {
            $scope.is_same = true;
            if ($scope.cardInfo.password_new && $scope.cardInfo.password_new_twice) {
                $scope.is_invail = true;
            }
        }
    }, true)

};