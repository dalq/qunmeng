module.exports = function ($scope, $resource, $modal, toaster) {
    $scope.myKeyup = function (e) {
        //IE 编码包含在window.event.keyCode中，Firefox或Safari 包含在event.which中
        var keycode = window.event ? e.keyCode : e.which;
        if (keycode == 13) {
            $scope.loadlist();
        }
    };
    //景区产品列表
    $scope.searchform = {};
    $scope.loadlist = function () {
        $resource('/api/ac/vc/viewGoodsService/findGoodsCategoryList', {}, {}).save($scope.searchform, function (res) {
            if (res.errcode === 0) {
                $scope.goodsCategoryList = res.data;
            } else {
                toaster.error({ title: '', body: res.errmsg });
            }
        });
    }
    $scope.loadlist();

    //创建or编辑景区产品
    $scope.updateGoodsCategory = function (obj) {
        var modalInstance = $modal.open({
            template: require('../views/viewGoodsCategoryInfo.html'),
            controller: 'viewGoodsCategoryInfo',
            size: 'lg',
            resolve: {
                item: function () {
                    return obj;
                }
            }
        });
        //关闭模态框刷新页面
        modalInstance.result.then(function () {
            $scope.loadlist();
        });
    }

    //创建or编辑景区产品
    $scope.bindAccount = function (obj) {
        var modalInstance = $modal.open({
            template: require('../views/bindAccount.html'),
            controller: 'bindAccount',
            size: 'lg',
            resolve: {
                item: function () {
                    return obj;
                }
            }
        });
        //关闭模态框刷新页面
        modalInstance.result.then(function () {
            $scope.loadlist();
        });
    }

    //创建or编辑景区产品
    $scope.bindDevice = function (obj) {
        var modalInstance = $modal.open({
            template: require('../views/bindDevice.html'),
            controller: 'bindDevice',
            size: 'lg',
            resolve: {
                item: function () {
                    return obj;
                }
            }
        });
        //关闭模态框刷新页面
        modalInstance.result.then(function () {
            $scope.loadlist();
        });
    }

    //删除产品
    $scope.deleteGoodsCategory = function (id) {
        if (confirm('确认删除本产品吗？')) {
            $resource('/api/ac/vc/viewGoodsService/deleteGoodsCategory', {}, {}).save({ 'id': id }, function (res) {
                if (res.errcode === 0) {
                    toaster.success({ title: '', body: '产品删除成功' });
                    $scope.loadlist();
                } else {
                    toaster.error({ title: '', body: res.errmsg });
                }
            });
        }
    }

};