/**
 * 双屏pos机登录
 * ml
 */
module.exports = function ($scope, $state, $resource, toaster) {

	$scope.user = {};

	$scope.sign = function () {
		if (!$scope.user.name || !$scope.user.password) {
			toaster.error({ title: '(*^__^*) ……', body: '请填写登录信息!' })
			return false;
		}
		$state.go('app.viewSystemMenuList',{})
		$resource('http://www.weather.com.cn/data/sk/101070101.html', {}, {}).save($scope.user, function (res) {
			if (res.errcode === 0) {
			}
		});
	}

	$scope.checkSign = function($event){
		//IE 编码包含在window.event.keyCode中，Firefox或Safari 包含在event.which中
		var keycode = window.event ? $event.keyCode : $event.which;
		if (keycode == 13) {
			$scope.sign();
		}
	}

	/**
	 * 天气信息	跨域	待解决
	 */
	// $.ajax({
	// 	method: "GET",
	// 	url: "http://www.weather.com.cn/data/sk/101070101.html",
	// 	dataType: 'JSONP',
	// 	data: {},
	// 	// jsonpCallback: "jsonpcallback",
	// 	beforeSend: function (xhr) {
	// 	},
	// 	success: function (response) {
	// 	},
	// 	error: function (jqXHR, textStatus, errorThrown) {
	// 	}
	// });

	// $resource('http://www.weather.com.cn/data/sk/101070101.html', {}, {}).save($scope.user, function (res) {
	// 	alert(515)
	// 	if (res.errcode === 0) {

	// 	}
	// });

};