/**
 * 双屏pos机-产品销售(列表)
 * ml
 */
module.exports = function ($scope, $modal, $state, $resource, toaster) {

    $scope.user = {};
    $scope.backMenu = function(){
        $state.go('app.viewSystemMenuList', {});
    }
    $scope.totalPrice = 0;
    $scope.ticketFlag = false;
    $scope.saleList = [];
    $scope.saleList = [
        {
            num: 0,
            code: '12465',
            name: '沈阳怪坡虎园成人票',
            price: '163'
        },
        {
            num: 0,
            code: '6357',
            name: '沈阳怪坡虎园成人票',
            price: '163'
        },
        {
            num: 0,
            code: '6345',
            name: '沈阳怪坡虎园成人票',
            price: '163'
        },
        {
            num: 0,
            code: '6345',
            name: '沈阳怪坡虎园成人票',
            price: '163'
        },
        {
            num: 0,
            code: '35345',
            name: '沈阳怪坡虎园成人票',
            price: '163'
        },
        {
            num: 0,
            code: '412',
            name: '沈阳怪坡虎园成人票',
            price: '163'
        },
        {
            num: 0,
            code: '12341234465',
            name: '沈阳怪坡虎园成人票',
            price: '163'
        },
        {
            num: 0,
            code: '143412',
            name: '沈阳怪坡虎园成人票',
            price: '163'
        }
    ];
    $scope.$watch('saleList', function(value){
        $scope.totalPrice = 0;
        $scope.saleList.forEach(function(element) {
            try {
                var num = parseInt(element.num);
                var price = parseInt(element.price);
            } catch (error) {
                toaster.error({title: '(*^__^*) ……', body: '请填入正确数量'});
                return false;
            }
            if ( isNaN(num) || isNaN(price) ) {
                toaster.error({title: '(*^__^*) ……', body: '请填入正确数量'});
                return false;
            }
            $scope.totalPrice += num * price;
        }, this);
        if ($scope.totalPrice) {
            $scope.ticketFlag = 'disabled';
        } else {
            $scope.ticketFlag = false;
        }
    }, true);
    $scope.add = function(index){
        $scope.saleList[index].num++;
    }
    $scope.remove = function(index){
        $scope.saleList[index].num--;
    }
};