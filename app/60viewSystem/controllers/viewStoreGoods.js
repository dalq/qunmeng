module.exports = function ($scope, $resource, $modal, toaster) {
    $scope.myKeyup = function (e) {
        //IE 编码包含在window.event.keyCode中，Firefox或Safari 包含在event.which中
        var keycode = window.event ? e.keyCode : e.which;
        if (keycode == 13) {
            $scope.loadlist();
        }
    };
    $scope.currentPage = 1;		//当前页码
    $scope.itemsPerPage = 9999;	//每页显示几条
    $scope.searchform = {
        type: '1'   //消费产品 必传
    };
    $scope.order = {
        total_price: 0,
        total_num: 0,
        list: []
    };
    //景区产品列表
    $scope.goodslist = [];
    $scope.loadlist = function () {
        $scope.searchform.pageNo = $scope.currentPage;
        $scope.searchform.pageSize = $scope.itemsPerPage;
        $resource('/api/ac/vc/viewGoodsService/sellList', {}, {}).save($scope.searchform, function (res) {
            if (res.errcode === 0) {
                $scope.goodslist = res.data.results;
                $scope.totalItems = res.data.totalRecord;
                for (let index = 0; index < $scope.goodslist.length; index++) {
                    const element = $scope.goodslist[index];
                    if (element.is_define_price == '1') {
                        $scope.goodslist[index].number = 1;
                    }
                }
            } else {
                toaster.error({ title: '', body: res.errmsg });
            }
        });
    }
    $scope.loadlist();

    //产品类型
    $resource('/api/ac/vc/viewGoodsService/findGoodsCategoryListForSell', {}, {}).save($scope.searchform, function (res) {
        if (res.errcode === 0) {
            $scope.goodslCategoryist = res.data;
            $scope.goodslCategoryist.unshift({
                code: "",
                name: "-- 全部 --"
            })
            $scope.searchform.sale_category = "";
        } else {
            toaster.error({ title: '', body: res.errmsg });
        }
    });

    //创建or编辑景区产品
    $scope.createOrder = function () {
        var modalInstance = $modal.open({
            template: require('../views/viewCreateOrderInfo.html'),
            controller: 'viewCreateOrderInfo',
            size: 'md',
            resolve: {
                item: function () {
                    return $scope.order;
                },
                type: function () {
                    return 1;
                }
            }
        });
        //关闭模态框刷新页面
        modalInstance.opened.then(function () {// 模态窗口打开之后执行的函数  
        });
        modalInstance.result.then(function (result) {
            toaster.success({ title: '购买成功【' + result.data.code + '】', body: result.data.pay_fee + "元" });
            $scope.loadlist();
            // $scope.order.smart_card_no = result.smart_card_no;
            // $resource('/api/ac/vc/viewOrderService/createComplexOrderForPc', {}, {}).save($scope.order, function (res) {
            //     if (res.errcode === 0) {
            //         toaster.success({ title: '', body: "购买成功" });
            //         $scope.loadlist();
            //     } else {
            //         toaster.error({ title: '', body: res.errmsg });
            //     }
            // });
        }, function (reason) {
        });
    }

    $scope.$watch('isAllSelected', function (newVal, oldVal) {
        for (var index = 0; index < $scope.goodslist.length; index++) {
            var element = $scope.goodslist[index];
            element.isSelected = $scope.isAllSelected;
        }
    }, false);

    $scope.$watch('goodslist', function (newVal, oldVal) {
        $scope.order.total_price = 0;
        $scope.order.total_num = 0;
        $scope.order.list = [];
        for (var index = 0; index < $scope.goodslist.length; index++) {
            var element = $scope.goodslist[index];
            if ((element.is_define_price != '1' && element.number > 0) || (element.is_define_price == '1' && element.cus_price > 0)) {
                if (element.is_define_price == '1') {
                    $scope.order.total_price += (parseFloat(element.cus_price) || 0);
                    $scope.order.list.push({
                        name: element.name,
                        sale_code: element.code,
                        num: 1,
                        cus_price: element.cus_price,
                        recharge: element.recharge
                    })
                    $scope.order.total_num++;
                } else if (element.number) {
                    $scope.order.total_price += parseFloat(parseFloat((parseFloat(element.price) || 0) * (parseFloat(element.number) || 0)).toFixed(2));
                    $scope.order.list.push({
                        name: element.name,
                        sale_code: element.code,
                        num: parseInt(element.number) || 0,
                        price: element.price,
                        recharge: element.recharge
                    })
                    $scope.order.total_num += (parseInt(element.number) || 0);
                }
            }
        }
    }, true);

};