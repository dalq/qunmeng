module.exports = function ($scope, $resource, $modalInstance, card, toaster) {
    $scope.cardInfo = card;
    $scope.deposit = $scope.cardInfo.deposit;

    //添加一卡通
    $scope.ok = function () {
        if (isNaN($scope.cardInfo.deposit)) {
            toaster.error({ title: '', body: '请输入纯数字押金' });
            return false;
        }
        let para = {
            smart_card: $scope.cardInfo.smart_card,
            deposit: $scope.cardInfo.deposit
        }
        $resource('/api/ac/vc/viewCardService/updateCard', {}, {}).save(para, function (res) {
            if (res.errcode === 0) {
                toaster.success({ title: '', body: '修改成功' });
                $modalInstance.close();
            } else {
                toaster.error({ title: '', body: res.errmsg });
            }
        });
    }

    //取消
    $scope.close = function () {
        $modalInstance.dismiss('cancel');
    }

};