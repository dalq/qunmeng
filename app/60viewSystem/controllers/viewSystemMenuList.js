/**
 * 双屏pos机菜单
 * ml
 */
module.exports = function ($scope, $modal, $state, $resource, toaster) {

    $scope.functionArray = [
        {
            url:'app.viewSystemSaleList',
            params: {},
            img: 'http://oss.juyouhx.com/tickets/sale.png',
            titleCn: '景区产品售票',
            titleEn: 'Scenic products sales',
            description: '测试用的'
        },
        {
            url:'app.viewSystemBulkBuying',
            params: {},
            img: 'http://oss.juyouhx.com/tickets/batch_ticket.png',
            titleCn: '批量购票',
            titleEn: 'Scenic products sales',
            description: '测试用的'
        },
        {
            url:'app.viewSystemChangeTicket',
            params: {},
            img: 'http://oss.juyouhx.com/tickets/for_ticket.png',
            titleCn: '景区换票',
            titleEn: 'Scenic products sales',
            description: '测试用的'
        },
        {
            url:'app.viewSystemMarketingStatistics',
            params: {},
            img: 'http://oss.juyouhx.com/tickets/sales_system.png',
            titleCn: '营销统计',
            titleEn: 'Scenic products sales',
            description: '测试用的'
        },
        {
            url:'app.viewSystemHistory',
            params: {},
            img: 'http://oss.juyouhx.com/tickets/history_query.png',
            titleCn: '历史查询',
            titleEn: 'Scenic products sales',
            description: '测试用的'
        }
    ];

    $scope.systemArray = [
        {
            url:'app.viewSystemChangeTicket',
            params: {a:'bcd'},
            img: 'http://oss.juyouhx.com/tickets/inspect_ticket.png',
            titleCn: '景区验票',
            titleEn: 'Scenic sales',
            description: '测试用的'
        },
        {
            url:'app.viewSystemMember',
            params: {},
            img: 'http://oss.juyouhx.com/tickets/member_zone.png',
            titleCn: '会员专区',
            titleEn: 'Scenic sales',
            description: '测试用的'
        },
        {
            url:'app.viewSystemSetUp',
            params: {},
            img: 'http://oss.juyouhx.com/tickets/system_settings.png',
            titleCn: '系统设置',
            titleEn: 'Scenic sales',
            description: '测试用的'
        },
        {
            url:'app.duplScrPosSign',
            params: {},
            img: 'http://oss.juyouhx.com/tickets/exit.png',
            titleCn: '退出',
            titleEn: 'Scenic sales',
            description: '测试用的'
        }
    ];

    $scope.menuTail = function(url, params){
        $state.go(url, params)
    }

};