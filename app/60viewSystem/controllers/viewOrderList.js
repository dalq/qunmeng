module.exports = function ($scope, $state, $resource, $modal, toaster) {
	$scope.myKeyup = function (e) {
		//IE 编码包含在window.event.keyCode中，Firefox或Safari 包含在event.which中
		var keycode = window.event ? e.keyCode : e.which;
		if (keycode == 13) {
			$scope.loadlist();
		}
	};
	
	$scope.currentPage = 1;		//当前页码
	$scope.itemsPerPage = 10;	//每页显示几条
	$scope.searchform = {};
	//卡列表
	$scope.loadlist = function () {
		var para = {
			'pageNo': $scope.currentPage,
			'pageSize': $scope.itemsPerPage
		}
		angular.extend(para, $scope.searchform);
		$resource('/api/ac/vc/viewOrderService/list', {}, {}).save(para, function (res) {
			if (res.errcode === 0) {
				$scope.orderList = res.data.results;
				$scope.totalItems = res.data.totalRecord;
			} else {
				toaster.error({ title: '', body: res.errmsg });
			}
		});
	}

	$scope.loadlist();

	//卡信息
	$scope.orderInfo = function (order) {
		var modalInstance = $modal.open({
			template: require('../views/orderInfo.html'),
			controller: 'orderInfo',
			size: 'lg',
			resolve: {
				order: function () {
					return order;
				}
			}
		});
		//关闭模态框刷新页面
		modalInstance.result.then(function () {
			if (!card) {
				$scope.loadlist(1);
			}
		});
	}

	//卡信息
	$scope.orderBack = function (order) {
		var modalInstance = $modal.open({
			template: require('../views/orderBack.html'),
			controller: 'orderBack',
			size: 'lg',
			resolve: {
				order: function () {
					return order;
				}
			}
		});
		//关闭模态框刷新页面
		modalInstance.result.then(function () {
			$scope.loadlist();
		});
	}

	// //删除卡
	// $scope.delete = function(index){
	// 	if(confirm('确认删除此卡吗？') == true){
	// 		var code = $scope.cardlist[index].code
	// 		$resource('/api/ac/vc/viewCardService/deleteCard', {}, {}).save({'card_code': code}, function (res) {
	// 			if (res.errcode === 0){
	// 				$scope.cardlist.splice(index, 1);
	// 			} else {
	// 				toaster.error({title: '', body: res.errmsg});
	// 			}
	// 		});
	// 	}
	// }

};