module.exports = function ($scope, $resource, $modalInstance, toaster, cardInfo) {
    
    //开始时间
    $scope.date = {
        'lable': cardInfo.period_start,
        'opened': false
    };
    //结束时间
    $scope.date1 = {
        'lable': cardInfo.period_end,
        'opened': false
    };


    //确认
    $scope.ok = function() {
        var para = {
            'smart_card': cardInfo.smart_card,
            'period_start': date2str2($scope.date.lable),
            'period_end': date2str2($scope.date1.lable)
        }
        //---------时间校验-----
        var d1 = parseInt(para.period_start.replace(/-/g, ""));
        var d2 = parseInt(para.period_end.replace(/-/g, ""));
        if(d1 > d2){
            toaster.error({title: '', body: '开始时间不能大于结束时间'});
            return;
        }
        //----------------------

        $resource('/api/ac/vc/viewCardService/updateCard', {}, {}).save(para, function (res) {
            if (res.errcode === 0) {
                toaster.success({title: '', body: '成功修改卡的有效期'});
                $modalInstance.close(para);
            } else {
                toaster.error({title: '', body: res.errmsg});
            }
        });
    }

    //取消
    $scope.close = function () {
        $modalInstance.dismiss('cancel');
    }



    //打开日历空间,选择时间
    $scope.dateOpen = function ($event, item) {
        $event.preventDefault();
        $event.stopPropagation();
        item.opened = true;
    };

    //日期转字符串
    function date2str2(d){
        if (d === undefined) {
            return "";
        } else if(typeof d === 'string'){
            return d;
        }
        var month = (d.getMonth() + 1).toString();
        var day = d.getDate().toString();
        if (month.length == 1) month = '0' + month;
        if (day.length == 1) day = '0' + day;
        return d.getFullYear() + "-" + month + "-" + day;
    }

};