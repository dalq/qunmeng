module.exports = function ($scope, $resource, $modalInstance, FileUploader, item, toaster){
    
    //初始化列表
    function init(){
        $scope.status_info = {};
        $scope.searchitem = {};
        $scope.is_new = true;
        console.log(item)
        if(item){
            $scope.is_new = false;
            $scope.obj = angular.copy(item);
            $scope.status_info.url = '/api/ac/vc/viewGoodsService/updateGoodsCategory';
            $scope.status_info.msg = '修改产品分类信息成功';
        } else {
            $scope.obj = {
                'name': '',
                // 'code': '',
                'remark': ''
            };
            $scope.status_info.url = '/api/ac/vc/viewGoodsService/createGoodsCategory';
            $scope.status_info.msg = '成功添加一条产品分类信息';
        }
    }
    init();
    
    $scope.searchform = {};
    $resource('/api/ac/vc/viewGoodsService/findGoodsCategoryList', {}, {}).save({}, function (res) {
        if (res.errcode === 0) {
            $scope.category_list = res.data;
        } else {
            toaster.error({ title: '', body: res.errmsg });
        }
    });
   
    $resource('/api/ac/vc/viewCommonService/getPlaceByCode', {}, {}).save({}, function (result) {
        if (result.errcode === 0) {
            $scope.viewList = result.data;
            if ($scope.is_new) {
                $scope.obj.view_code = $scope.viewList[0].code;
            }
        } else {
            toaster.error({ title: '', body: result.errmsg });
        }
    });
    
    //添加景区产品
    $scope.ok = function (){
        if(!$scope.obj.name || !$scope.obj.view_code){
            toaster.warning({title: '', body: '产品分类信息不完整'});
            return;
        }
        let para = {
            id: $scope.obj.id,
            name: $scope.obj.name,
            remark: $scope.obj.remark,
            view_code: $scope.obj.view_code
        }
        $resource($scope.status_info.url, {}, {}).save(para, function (res) {
            if (res.errcode === 0) {
                toaster.success({title: '', body: $scope.status_info.msg});
                $modalInstance.close();
            } else {
                toaster.error({title: '', body: res.errmsg});
            }
        });
    }

	//取消
    $scope.close = function () {
        $modalInstance.dismiss('cancel');
    }

};