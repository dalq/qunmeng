/**
 * 双屏pos机-产品销售(列表)
 * ml
 */
module.exports = function ($scope, $modal, $state, $resource, toaster) {

    $scope.backMenu = function(){
        $state.go('app.viewSystemMenuList', {});
    }
    $scope.view = {
        id:'a8db8792594c43b5b2067495558a3820'
    }
    $resource('/api/as/tc/device/info', {}, {}).save($scope.view, function (res) {
        if (res.errcode !== 0) {
            toaster.error({ title: '', body: res.errmsg})
            return false;
        }
        $scope.view = res.data;
    });
    $scope.save = function(){
        $resource('/api/as/tc/device/update', {}, {}).save($scope.view, function (res) {
            if (res.errcode !== 0) {
                toaster.error({ title: '', body: res.errmsg})
                return false;
            }
            toaster.success({ title: '', body: '修改成功'})
        });

    }
};