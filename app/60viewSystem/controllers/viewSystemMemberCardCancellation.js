/**
 * 双屏pos机-产品销售(列表)
 * ml
 */
module.exports = function ($scope, $modal, $state, $resource, toaster) {

    $scope.user = {};
    $scope.backMenu = function(){
        $state.go('app.viewSystemMenuList', {});
    }
    $scope.totalPrice = 0;
    $scope.ticketFlag = false;
    $scope.ticketTypeList = [];
    $scope.ticketTypeList = [
        {
            num: 0,
            code: '12465',
            name: '沈阳怪坡虎园成人票',
            price: '163'
        },
        {
            num: 0,
            code: '6357',
            name: '沈阳怪坡虎园成人票',
            price: '163'
        },
        {
            num: 0,
            code: '6345',
            name: '沈阳怪坡虎园成人票',
            price: '163'
        },
        {
            num: 0,
            code: '143412',
            name: '沈阳怪坡虎园成人票',
            price: '163'
        }
    ];
    $scope.$watch('saleList', function(value){
    }, true);
    $scope.add = function(index){
        $scope.saleList[index].num++;
    }
    $scope.remove = function(index){
        $scope.saleList[index].num--;
    }
};