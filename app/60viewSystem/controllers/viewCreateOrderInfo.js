module.exports = function ($scope, $resource, $modalInstance, FileUploader, item, type, toaster) {

    $scope.order = item;
    $scope.type = type;
    //添加景区产品
    $scope.ok = function () {
        if (!$scope.order.smart_card_no) {
            toaster.warning({ title: "", body: "请输入卡号" })
            return false;
        }
        $scope.is_post = true;
        $resource('/api/ac/vc/viewOrderService/createComplexOrderForPc', {}, {}).save($scope.order, function (res) {
            $scope.is_post = false;
            if (res.errcode === 0) {
                $modalInstance.close(res);
            } else {
                toaster.error({ title: '', body: res.errmsg });
            }
        });
    }

    //取消
    $scope.close = function () {
        $modalInstance.dismiss('cancel');
    }

};