module.exports = function ($scope, $state, $resource, $modal, toaster, Excel, $timeout) {

	$scope.myKeyup = function (e) {
		//IE 编码包含在window.event.keyCode中，Firefox或Safari 包含在event.which中
		var keycode = window.event ? e.keyCode : e.which;
		if (keycode == 13) {
			$scope.loadlist();
		}
	};
	//卡列表
	$scope.loadlist = function (pageNo) {
		let para = {
			'pageNo': pageNo,
			'pageSize': $scope.itemsPerPage
		}
		angular.extend(para, $scope.searchform)
		$resource('/api/ac/vc/viewCardService/list', {}, {}).save(para, function (res) {
			if (res.errcode === 0) {
				$scope.cardlist = res.data.results;
				$scope.totalItems = res.data.totalRecord;
				$scope.currentPage = pageNo;
				$resource('/api/ac/vc/viewCommonService/getPlaceByCode', {}, {}).save(para, function (result) {
					if (result.errcode === 0) {
						for (let index = 0; index < result.data.length; index++) {
							const element = result.data[index];
							$scope.viewMap[element.code] = element.name;
						}
						for (let index = 0; index < $scope.cardlist.length; index++) {
							const element = $scope.cardlist[index];
							element.view_name = $scope.viewMap[element.view_code] || '未找到';
						}
					} else {
						toaster.error({ title: '', body: res.errmsg });
					}
				});
			} else {
				toaster.error({ title: '', body: res.errmsg });
			}
		});
	}
	$resource('/api/ac/vc/viewCardService/cardCategoryListNoPage', {}, {}).save({}, function (res) {
        if (res.errcode === 0) {
            $scope.category_list = res.data;
        } else {
            toaster.error({ title: '', body: res.errmsg });
        }
    });

	init();
	//初始化列表
	function init() {
		$scope.currentPage = 1;		//当前页码
		$scope.itemsPerPage = 10;	//每页显示几条
		$scope.searchform = {};
		$scope.viewMap = {};
		$scope.loadlist(1);
	}

	//卡信息
	$scope.cardInfo = function (card) {
		var modalInstance = $modal.open({
			template: require('../views/addViewCard.html'),
			controller: 'addViewCard',
			size: 'lg',
			resolve: {
				card: function () {
					return card;
				}
			}
		});
		//关闭模态框刷新页面
		modalInstance.result.then(function () {
			if (!card) {
				$scope.loadlist(1);
			}
		});
	}

	//修改
	$scope.updateCard = function (card) {
		var modalInstance = $modal.open({
			template: require('../views/updateCard.html'),
			controller: 'updateCard',
			size: 'lg',
			resolve: {
				card: function () {
					return card;
				},
				type: function () {
					return '1'
				}
			}
		});
		//关闭模态框刷新页面
		modalInstance.result.then(function () {
			if (!card) {
				$scope.loadlist(1);
			}
		});
	}

	//修改
	$scope.detail = function (card) {
		var modalInstance = $modal.open({
			template: require('../views/cardDetails.html'),
			controller: 'cardDetails',
			size: 'lg',
			resolve: {
				card: function () {
					return card;
				}
			}
		});
		//关闭模态框刷新页面
		modalInstance.result.then(function () {
			if (!card) {
				$scope.loadlist(1);
			}
		});
	}

	//挂失
	$scope.updateCardInfo = function (card) {
		var modalInstance = $modal.open({
			template: require('../views/updateCard.html'),
			controller: 'updateCard',
			size: 'lg',
			resolve: {
				card: function () {
					return card;
				},
				type: function () {
					return '2'
				}
			}
		});
		//关闭模态框刷新页面
		modalInstance.result.then(function () {
			if (!card) {
				$scope.loadlist(1);
			}
		});
	}

	//删除卡
	$scope.delete = function (index) {
		if (confirm('确认删除此卡吗？') == true) {
			var code = $scope.cardlist[index].smart_card
			$resource('/api/ac/vc/viewCardService/deleteCard', {}, {}).save({ 'smart_card': code }, function (res) {
				if (res.errcode === 0) {
					$scope.cardlist.splice(index, 1);
				} else {
					toaster.error({ title: '', body: res.errmsg });
				}
			});
		}
	}

	//导入excel
	$scope.excel = function () {
		var modalInstance = $modal.open({
			template: require('../views/addCardBatch.html'),
			controller: 'addCardBatch',
			size: 'lg',
			resolve: {

			}
		});
		//关闭模态框刷新页面
		modalInstance.result.then(function () {

		});
	}

	function date2str(d) {
        if (d === undefined) {
            return "";
        }
        var month = (d.getMonth() + 1).toString();
        var day = d.getDate().toString();
        if (month.length == 1) month = '0' + month;
        if (day.length == 1) day = '0' + day;
        return d.getFullYear() + "-" + month + "-" + day;
    }
	$scope.ExcelName = date2str(new Date());
    $scope.exportToExcel = function (tableId) {
        let paraForExcel = {
            pageNo: 1,
            pageSize: 200000
        }
        angular.extend(paraForExcel, $scope.searchform);
        $resource('/api/ac/vc/viewCardService/list', {}, {}).save(paraForExcel, function (res) {
            if (res.errcode === 0) {
				$scope.detaillistForExcel = res.data.results;
				for (let index = 0; index < $scope.detaillistForExcel.length; index++) {
					const element = $scope.detaillistForExcel[index];
					element.view_name = $scope.viewMap[element.view_code] || '未找到';
				}
                $timeout(function () {
                    document.getElementById("dlink").href = Excel.tableToExcel(tableId, 'sheet1');
                    //document.getElementById("dlink").download = "1122b.xls";//这里是关键所在,当点击之后,设置a标签的属性,这样就可以更改标签的标题了
                    document.getElementById("dlink").click();
                }, 100); // trigger download
            } else {
                toaster.error({ title: '查询Excel数据失败', body: res.errmsg });
            }
        });
    }

};