/**
 * 双屏pos机-产品销售(列表)
 * ml
 */
module.exports = function ($scope, $modal, $state, $resource, toaster) {

    $scope.user = {};
    $scope.backMenu = function(){
        $state.go('app.viewSystemMenuList', {});
    }
    $scope.product = function(){
        $state.go('app.viewSystemMemberProduct', {});
    }
    $scope.maintain = function(){
        $state.go('app.viewSystemMemberCardMaintain', {});
    }
    $scope.cncellation = function(){
        $state.go('app.viewSystemMemberCardCancellation', {});
    }
};