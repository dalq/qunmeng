module.exports = function ($scope, $state, $resource, $modal, toaster) {
    $scope.myKeyup = function (e) {
        //IE 编码包含在window.event.keyCode中，Firefox或Safari 包含在event.which中
        var keycode = window.event ? e.keyCode : e.which;
        if (keycode == 13) {
            $scope.loadlist();
        }
    };
    function date2str(d) {
        if (d === undefined) {
            return "";
        }
        var month = (d.getMonth() + 1).toString();
        var day = d.getDate().toString();
        if (month.length == 1) month = '0' + month;
        if (day.length == 1) day = '0' + day;
        return d.getFullYear() + "-" + month + "-" + day;
    }

    $scope.dateOpen = function ($event, item) {
        $event.preventDefault();
        $event.stopPropagation();
        item.opened = true;
    };

    $scope.period_start = {
        // 'value': date2str(new Date()),
        'value': '',
        'opened': false
    }

    $scope.period_end = {
        // 'value': date2str(new Date()),
        'value': '',
        'opened': false
    }

    $scope.searchform = {};
    $scope.currentPage = 1;		//当前页码
    $scope.itemsPerPage = 10;	//每页显示几条
    //卡列表
    $scope.loadlist = function () {
        var para = {
            'pageNo': $scope.currentPage,
            'pageSize': $scope.itemsPerPage
        }
        $scope.searchform.start_time = (typeof $scope.period_start.value === 'string' ? $scope.period_start.value : date2str($scope.period_start.value));
        $scope.searchform.end_time = (typeof $scope.period_end.value === 'string' ? $scope.period_end.value : date2str($scope.period_end.value));
        angular.extend(para, $scope.searchform);
        $resource('/api/ac/vc/viewOrderService/findOrderGroupList', {}, {}).save(para, function (res) {
            if (res.errcode === 0) {
                $scope.cardList = res.data.results;
                $scope.totalItems = res.data.totalRecord;
            } else {
                toaster.error({ title: '', body: res.errmsg });
            }
        });
    }
    $scope.loadlist();

    
	$resource('/api/ac/vc/viewGoodsService/findGoodsCategoryList', {}, {}).save({}, function (res) {
		if (res.errcode === 0) {
			$scope.sale_category_list = res.data;
			$scope.sale_category_list.unshift({
				code: null,
				name: "-- 全部 --"
			})
		} else {
			toaster.error({ title: '', body: res.errmsg });
		}
	});

};