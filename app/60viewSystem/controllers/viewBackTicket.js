/**
 * 双屏pos机-产品销售(列表)
 * ml
 */
module.exports = function ($scope, $modal, $state, $resource, toaster, $timeout) {

    $scope.params = {
        device: 'J0101'
    };
    $scope.ticketInfo = null;
    $scope.backMenu = function () {
        $state.go('app.viewSystemMenuList', {});
    }

    $scope.search = function () {
        $resource('/tktapi/sc/queryService/byCode', {}, {}).save($scope.params, function (res) {
            if (res.errcode !== 0) {
                toaster.error({ title: '', body: res.errmsg })
                return false;
            }
            $scope.ticketInfo = res.data.ticketInfo;
            $scope.ticketInfo.reqkey = res.data.reqkey;
        });
    }

    $scope.backTicket = function () {
        var map = { 
            "order_unique_code": $scope.params.code, 
            "back_count": $scope.sale.num, 
            "device_code": $scope.params.device
        };
        $resource('/api/ac/tc/equipmentProductService/createBackOrder', {}, {}).save(map, function (res) {
            if (res.errcode !== 0) {
                toaster.error({ title: '', body: res.errmsg })
                return false;
            }
            var message = '';
            if (res.data.pay_type == '1') {
                message = '现金支付';
            }else{
                message = '其他方式支付';
            }
            toaster.success({ title: '退票成功', body: '退票金额: ' + res.data.back_fee * 0.01 + '元 (' + message + ')'})
            return false;
        });
    }

    $scope.add = function (index) {
        $scope.saleList[index].num++;
    }

    $scope.remove = function (index) {
        $scope.saleList[index].num--;
    }
};