/**
 * 双屏pos机-产品销售(列表)
 * ml
 */
module.exports = function ($scope, $modal, $state, $resource, toaster) {

    $scope.user = {};
    $scope.backMenu = function(){
        $state.go('app.viewSystemMenuList', {});
    }
    $scope.totalMoney = 0;
    $scope.backMoney = 0;
    $scope.saleNum = 0;
    $scope.backNum = 0;
    $scope.validateNum = 0;
    $scope.saleList = [];
    $scope.saleList = [
        {
            validate: 0,
            sale: '12465',
            name: '沈阳怪坡虎园成人票',
            back: '163',
            price: 651
        },
        {
            validate: 0,
            sale: '6357',
            name: '沈阳怪坡虎园成人票',
            back: '163',
            price: 651
        },
        {
            validate: 0,
            sale: '6345',
            name: '沈阳怪坡虎园成人票',
            back: '163',
            price: 651
        },
        {
            validate: 0,
            sale: '6345',
            name: '沈阳怪坡虎园成人票',
            back: '163',
            price: 651
        },
        {
            validate: 0,
            sale: '35345',
            name: '沈阳怪坡虎园成人票',
            back: '163',
            price: 651
        },
        {
            validate: 4,
            sale: '412',
            name: '沈阳怪坡虎园成人票',
            back: '163',
            price: 651
        },
        {
            validate: 0,
            sale: '12341234465',
            name: '沈阳怪坡虎园成人票',
            back: '163',
            price: 651
        },
        {
            validate: 0,
            sale: '143412',
            name: '沈阳怪坡虎园成人票',
            back: '163',
            price: 651
        }
    ];
    $scope.$watch('saleList', function(value){

        $scope.totalMoney = 0;
        $scope.backMoney = 0;
        $scope.saleNum = 0;
        $scope.backNum = 0;
        $scope.validateNum = 0;
        $scope.saleList.forEach(function(element) {
            try {
                var validate = parseInt(element.validate);
                var back = parseInt(element.back);
                var sale = parseInt(element.sale);
                var price = parseInt(element.price);
            } catch (error) {
                toaster.error({title: '(*^__^*) ……', body: '数量错误'});
                return false;
            }
            if ( isNaN(validate) || isNaN(back) || isNaN(sale) ) {
                toaster.error({title: '(*^__^*) ……', body: '数量错误'});
                return false;
            }
            $scope.saleNum += sale;
            $scope.backNum += back;
            $scope.validateNum += validate;
            $scope.totalMoney += sale * price;
            $scope.backMoney += back * price;
        }, this);
    }, true);
    
};