module.exports = function ($scope, $resource, toaster, Excel, $timeout, $stateParams) {
    $scope.myKeyup = function (e) {
        //IE 编码包含在window.event.keyCode中，Firefox或Safari 包含在event.which中
        var keycode = window.event ? e.keyCode : e.which;
        if (keycode == 13) {
            $scope.loadList();
        }
    };

    function date2str(d) {
        if (d === undefined) {
            return "";
        }
        var month = (d.getMonth() + 1).toString();
        var day = d.getDate().toString();
        if (month.length == 1) month = '0' + month;
        if (day.length == 1) day = '0' + day;
        return d.getFullYear() + "-" + month + "-" + day;
    }

    $scope.dateOpen = function ($event, item) {
        $event.preventDefault();
        $event.stopPropagation();
        item.opened = true;
    };

    $scope.period_start = {
        // 'value': date2str(new Date()),
        'value': '',
        'opened': false
    }

    $scope.period_end = {
        // 'value': date2str(new Date()),
        'value': '',
        'opened': false
    }

    $resource('/api/ac/vc/viewCardService/cardCategoryListNoPage', {}, {}).save({}, function (res) {
        if (res.errcode === 0) {
            $scope.category_list = res.data;
        } else {
            toaster.error({ title: '', body: res.errmsg });
        }
    });

    $scope.currentPage = 1;		//当前页码
    $scope.itemsPerPage = 10;	//每页显示几条
    $scope.searchform = {};
    if ($stateParams.smart_card_no !== undefined) {
        $scope.searchform.smart_card_no = $stateParams.smart_card_no;
    }
    $scope.loadList = function () {
        let para = {
            pageNo: $scope.currentPage,
            pageSize: $scope.itemsPerPage
        }
        $scope.searchform.create_time_start = (typeof $scope.period_start.value === 'string' ? $scope.period_start.value : date2str($scope.period_start.value));
        $scope.searchform.create_time_end = (typeof $scope.period_end.value === 'string' ? $scope.period_end.value : date2str($scope.period_end.value));
        angular.extend(para, $scope.searchform);
        $resource('/api/ac/vc/viewCardService/detailList', {}, {}).save(para, function (res) {
            if (res.errcode === 0) {
                $scope.detaillist = res.data.results;
                $scope.totalItems = res.data.totalRecord;
            } else {
                toaster.error({ title: '', body: res.errmsg });
            }
        });
    }
    $scope.loadList();
    
    $scope.ExcelName = date2str(new Date());
    $scope.exportToExcel = function (tableId) {
        let paraForExcel = {
            pageNo: 1,
            pageSize: 200000
        }
        angular.extend(paraForExcel, $scope.searchform);
        $resource('/api/ac/vc/viewCardService/detailList', {}, {}).save(paraForExcel, function (res) {
            if (res.errcode === 0) {
                $scope.detaillistForExcel = res.data.results;
                $timeout(function () {
                    document.getElementById("dlink").href = Excel.tableToExcel(tableId, 'sheet1');
                    //document.getElementById("dlink").download = "1122b.xls";//这里是关键所在,当点击之后,设置a标签的属性,这样就可以更改标签的标题了
                    document.getElementById("dlink").click();
                }, 100); // trigger download
            } else {
                toaster.error({ title: '查询Excel数据失败', body: res.errmsg });
            }
        });
    }

};