module.exports = function ($scope, $resource, $modalInstance, toaster){

	init();
    //初始化列表
    function init(){
        $scope.list = [];
        // var input = $('.fileup label');
        // input.attr('class', 'btn btn-info');
    }
    
    $scope.ok = function () {
    }

	//取消
    $scope.close = function () {
        $modalInstance.dismiss('cancel');
    }

    //读取本地EXCEL文件
    $scope.read = function () {
        var event = document.getElementById('file');
        if (event.files[0].name.indexOf(".xls") < 0){
            toaster.error({title: '', body: '文件格式不正确'});
            return;
        }
        
        var reader = new FileReader();
        reader.onload = function (e){
            var data = e.target.result;
            data = String.fromCharCode.apply(null, new Uint8Array(data));
            $scope.parseToJson(XLSX.read(btoa(data), {type: 'base64'}));
        };
        reader.readAsArrayBuffer(event.files[0]);
    }
    
    //excel读取之后,解析内容
    $scope.parseToJson = function (workbook){
        var title = {'smart_card_no': workbook.Strings[0].h, 'smart_card':workbook.Strings[1].h};
        // 暂时只能读取第一个sheet页的数据
        var sheet1Date = workbook.Sheets[workbook.SheetNames[0]];
        var card_list = XLSX.utils.sheet_to_row_object_array(sheet1Date);
        
        
        $scope.list = card_list;
        
        
        // 循环读取每个sheet页的数据,此功能不需要
        // var result = {};
        // workbook.SheetNames.forEach(function (sheetName) {
        //     var roa = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
        //     if (roa.length > 0) {
        //         result[sheetName] = roa;
        //     }
        // });
    }
    

};