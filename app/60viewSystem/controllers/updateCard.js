module.exports = function ($scope, $resource, $modalInstance, card, type, toaster) {

    $scope.type = type;
    $scope.card = card;
    $scope.is_password = false;
    $resource('/api/ac/vc/viewCommonService/getPlaceByCode', {}, {}).save({}, function (result) {
        if (result.errcode === 0) {
            $scope.viewList = result.data;
        } else {
            toaster.error({ title: '', body: result.errmsg });
        }
    });
    $resource('/api/ac/vc/viewCardService/cardCategoryListNoPage', {}, {}).save({}, function (res) {
        if (res.errcode === 0) {
            $scope.category_list = res.data;
        } else {
            toaster.error({ title: '', body: res.errmsg });
        }
    });

    function date2str(d) {
        if (d === undefined) {
            return "";
        }
        var month = (d.getMonth() + 1).toString();
        var day = d.getDate().toString();
        if (month.length == 1) month = '0' + month;
        if (day.length == 1) day = '0' + day;
        return d.getFullYear() + "-" + month + "-" + day;
    }

    $scope.dateOpen = function ($event, item) {
        $event.preventDefault();
        $event.stopPropagation();
        item.opened = true;
    };

    $scope.period_start = {
        'value': $scope.card.period_start || '',
        'opened': false
    }

    $scope.period_end = {
        'value': $scope.card.period_end || '',
        'opened': false
    }

    //添加一卡通
    $scope.ok = function () {
        if ($scope.card.is_need_password != '1') {
            delete $scope.card.password;
        } else {
            if (($scope.card.password) && (isNaN($scope.card.password) || $scope.card.password.length != 6)) {
                toaster.error({ title: '', body: '请输入六位纯数字密码' });
                return false;
            }
        }
        $scope.card.period_start = (typeof $scope.period_start.value === 'string'? $scope.period_start.value: date2str($scope.period_start.value));
        $scope.card.period_end = (typeof $scope.period_end.value === 'string'? $scope.period_end.value: date2str($scope.period_end.value));
        let url = '/api/ac/vc/viewCardService/updateCard';
        //挂失
        if ($scope.type == '2') {
            url = '/api/ac/vc/viewCardService/updateCardInfo';
            // $scope.card.smart_card = $scope.card.smart_card_new;
            // $scope.card.smart_card_no = $scope.card.smart_card_no_new;
        }
        $resource(url, {}, {}).save($scope.card, function (res) {
            if (res.errcode === 0) {
                $modalInstance.close();
            } else {
                toaster.error({ title: '', body: res.errmsg });
            }
        });
    }

    //取消
    $scope.close = function () {
        $modalInstance.dismiss('cancel');
    }

};