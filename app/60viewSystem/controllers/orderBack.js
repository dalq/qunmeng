module.exports = function ($scope, $resource, $modalInstance, order, toaster) {
    $scope.order = order;
    //取消
    $scope.ok = function () {
        let para = {order_code: $scope.order.code}
        $resource('/api/ac/vc/viewBackOrderService/createBackOrder', {}, {}).save(para, function (res) {
            if (res.errcode === 0) {
                toaster.success({ title: '', body: '申请成功' });
                $modalInstance.close($scope.member);
            } else {
                toaster.error({ title: '', body: res.errmsg });
            }
        });
    }
    //取消
    $scope.close = function () {
        $modalInstance.dismiss('cancel');
    }
};