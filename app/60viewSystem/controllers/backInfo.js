module.exports = function ($scope, $resource, $modalInstance, order, toaster) {
    $scope.order = order;
    //取消
    $scope.ok = function (state) {
        if(!$scope.order.description){
            toaster.warning({title: '', body: '请填写审核意见'})
            return false;
        }
        let para = {
            id: $scope.order.id,
            state: state,
            description: $scope.order.description
        }
        $resource('/api/ac/vc/viewBackOrderService/updateBackOrder', {}, {}).save(para, function (res) {
            if (res.errcode === 0) {
                toaster.success({ title: '', body: res.data.msg });
                $modalInstance.close($scope.order);
            } else {
                toaster.error({ title: '', body: res.errmsg });
            }
        });
    }
    //取消
    $scope.close = function () {
        $modalInstance.dismiss('cancel');
    }

};