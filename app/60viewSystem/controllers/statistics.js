module.exports = function ($scope, $state, $resource, $modal, toaster, Excel, $timeout) {
	$scope.myKeyup = function (e) {
		//IE 编码包含在window.event.keyCode中，Firefox或Safari 包含在event.which中
		var keycode = window.event ? e.keyCode : e.which;
		if (keycode == 13) {
			$scope.loadlist();
		}
	};

	function date2str(d) {
		if (d === undefined) {
			return "";
		}
		var month = (d.getMonth() + 1).toString();
		var day = d.getDate().toString();
		if (month.length == 1) month = '0' + month;
		if (day.length == 1) day = '0' + day;
		return d.getFullYear() + "-" + month + "-" + day;
	}

	$scope.dateOpen = function ($event, item) {
		$event.preventDefault();
		$event.stopPropagation();
		item.opened = true;
	};

	$scope.period_start = {
		'value': date2str(new Date()),
		// 'value': '',
		'opened': false
	}

	$scope.period_end = {
		'value': date2str(new Date()),
		// 'value': '',
		'opened': false
	}

	$scope.searchform = {};
	$resource('/api/ac/sc/systemUserService/getCompanyUserList', {}, {}).save({}, function (res) {
		if (res.errcode === 0) {
			$scope.accountList = res.data;
			$scope.accountList.unshift({
				name: '---下单账号---',
				login_name: ''
			})
			$scope.searchform.loginName = '';
		} else {
			toaster.error({ title: '', body: res.errmsg });
		}
	});

	//卡列表
	$scope.loadlist = function () {
		var para = {
			'pageNo': $scope.currentPage,
			'pageSize': $scope.itemsPerPage
		}
		$scope.searchform.create_time_start = (typeof $scope.period_start.value === 'string' ? $scope.period_start.value : date2str($scope.period_start.value));
		$scope.searchform.create_time_end = (typeof $scope.period_end.value === 'string' ? $scope.period_end.value : date2str($scope.period_end.value));
		if ($scope.searchform._user) {
			$scope.searchform.loginName = $scope.searchform._user.login_name;
		}
		angular.extend(para, $scope.searchform);
		$resource('/api/ac/vc/viewStatisticsService/findSaleStatistics', {}, {}).save(para, function (res) {
			if (res.errcode === 0) {
				$scope.statisticsList = res.data;
				$scope.totalItems = res.data.totalRecord;
				$scope.currentPage = pageNo;
			} else {
				toaster.error({ title: '', body: res.errmsg });
			}
		});
	}

	init();
	//初始化列表
	function init() {
		$scope.currentPage = 1;		//当前页码
		$scope.itemsPerPage = 10;	//每页显示几条
		$scope.searchform = {};
		$scope.loadlist();
	}

	//卡信息
	$scope.orderInfo = function (order) {
		var modalInstance = $modal.open({
			template: require('../views/orderInfo.html'),
			controller: 'orderInfo',
			size: 'lg',
			resolve: {
				order: function () {
					return order;
				}
			}
		});
		//关闭模态框刷新页面
		modalInstance.result.then(function () {
			if (!card) {
				$scope.loadlist(1);
			}
		});
	}

	$scope.ExcelName = date2str(new Date());
	$scope.exportToExcel = function (tableId) {
		$timeout(function () {
			document.getElementById("dlink").href = Excel.tableToExcel(tableId, 'sheet1');
			//document.getElementById("dlink").download = "1122b.xls";//这里是关键所在,当点击之后,设置a标签的属性,这样就可以更改标签的标题了
			document.getElementById("dlink").click();
		}, 100); // trigger download
	}

};