/**
* 子模块路由
* ml
*/

var router = function ($urlRouterProvider, $stateProvider) {

	$stateProvider

		//双屏pos机登录页
		.state('app.duplScrPosSign', {
			url: "/viewSystem/duplScrPosSign",
			views: {
				'main@': {
					template: require('./views/duplScrPosSign.html'),
					controller: 'duplScrPosSign'
				}
			},
			resolve: {

			}
		})

		//双屏pos机菜单页
		.state('app.viewSystemMenuList', {
			url: "/viewSystem/menuList",
			views: {
				'main@': {
					template: require('./views/viewSystemMenuList.html'),
					controller: 'viewSystemMenuList'
				}
			},
			resolve: {

			}
		})

		//双屏pos机-产品销售(列表)
		.state('app.viewSystemSaleList', {
			url: "/viewSystem/saleList",
			views: {
				'main@': {
					template: require('./views/viewSystemSaleList.html'),
					controller: 'viewSystemSaleList'
				}
			},
			resolve: {

			}
		})

		//双屏pos机-产品批量购买
		.state('app.viewSystemBulkBuying', {
			url: "/viewSystem/bulkBuying",
			views: {
				'main@': {
					template: require('./views/viewSystemBulkBuying.html'),
					controller: 'viewSystemBulkBuying'
				}
			},
			resolve: {

			}
		})

		//双屏pos机-产品批量购买
		.state('app.viewSystemChangeTicket', {
			url: "/viewSystem/changeTicket",
			views: {
				'main@': {
					template: require('./views/viewSystemChangeTicket.html'),
					controller: 'viewSystemChangeTicket'
				}
			},
			resolve: {

			}
		})

		//双屏pos机-营销统计
		.state('app.viewSystemMarketingStatistics', {
			url: "/viewSystem/marketingStatistics",
			views: {
				'main@': {
					template: require('./views/viewSystemMarketingStatistics.html'),
					controller: 'viewSystemMarketingStatistics'
				}
			},
			resolve: {

			}
		})

		//双屏pos机-历史查询
		.state('app.viewSystemHistory', {
			url: "/viewSystem/history",
			views: {
				'main@': {
					template: require('./views/viewSystemHistory.html'),
					controller: 'viewSystemHistory'
				}
			},
			resolve: {

			}
		})

		//双屏pos机-会员专区
		.state('app.viewSystemMember', {
			url: "/viewSystem/member",
			views: {
				'main@': {
					template: require('./views/viewSystemMember.html'),
					controller: 'viewSystemMember'
				}
			},
			resolve: {

			}
		})

		//双屏pos机-会员产品专区
		.state('app.viewSystemMemberProduct', {
			url: "/viewSystem/memberProduct",
			views: {
				'main@': {
					template: require('./views/viewSystemMemberProduct.html'),
					controller: 'viewSystemMemberProduct'
				}
			},
			resolve: {

			}
		})

		//双屏pos机-会员卡片维护
		.state('app.viewSystemMemberCardMaintain', {
			url: "/viewSystem/memberCardMaintain",
			views: {
				'main@': {
					template: require('./views/viewSystemMemberCardMaintain.html'),
					controller: 'viewSystemMemberCardMaintain'
				}
			},
			resolve: {

			}
		})

		//双屏pos机-会员卡片注销
		.state('app.viewSystemMemberCardCancellation', {
			url: "/viewSystem/memberCardCancellation",
			views: {
				'main@': {
					template: require('./views/viewSystemMemberCardCancellation.html'),
					controller: 'viewSystemMemberCardCancellation'
				}
			},
			resolve: {

			}
		})

		//双屏pos机-会员卡片注销
		.state('app.viewSystemSetUp', {
			url: "/viewSystem/setUp",
			views: {
				'main@': {
					template: require('./views/viewSystemSetUp.html'),
					controller: 'viewSystemSetUp'
				}
			},
			resolve: {

			}
		})

		//双屏pos机-退票
		.state('app.viewBackTicket', {
			url: "/viewSystem/viewBackTicket",
			views: {
				'main@': {
					template: require('./views/viewBackTicket.html'),
					controller: 'viewBackTicket'
				}
			},
			resolve: {

			}
		})

		//景区系统-卡列表
		.state('app.viewCardList', {
			url: "/viewcard/list",
			views: {
				'main@': {
					template: require('./views/viewCardList.html'),
					controller: 'viewCardList'
				}
			},
			resolve: {

			}
		})

		//景区系统-产品列表
		.state('app.viewGoodsList', {
			url: "/viewgoods/list",
			views: {
				'main@': {
					template: require('./views/viewGoodsList.html'),
					controller: 'viewGoodsList'
				}
			},
			resolve: {

			}
		})

		//景区系统-订单列表
		.state('app.viewOrderList', {
			url: "/vieworder/list",
			views: {
				'main@': {
					template: require('./views/viewOrderList.html'),
					controller: 'viewOrderList'
				}
			},
			resolve: {

			}
		})

		//景区系统-多商品订单列表
		.state('app.viewOrderComplexList', {
			url: "/viewOrderComplex/list",
			views: {
				'main@': {
					template: require('./views/viewOrderComplexList.html'),
					controller: 'viewOrderComplexList'
				}
			},
			resolve: {

			}
		})

		//景区系统-卡信息历史
		.state('app.cardHistoryList', {
			url: "/cardHistory/list",
			views: {
				'main@': {
					template: require('./views/cardHistoryList.html'),
					controller: 'cardHistoryList'
				}
			},
			resolve: {

			}
		})

		//景区系统-票种选景区产品
		.state('app.typeSelectGoods', {
			url: "/typeSelectGoods/list",
			views: {
				'main@': {
					template: require('./views/typeSelectGoods.html'),
					controller: 'typeSelectGoods'
				}
			},
			resolve: {

			}
		})

		//景区系统-储值产品
		.state('app.saveGoods', {
			url: "/viewSaveGoods/list",
			views: {
				'main@': {
					template: require('./views/viewSaveGoods.html'),
					controller: 'viewSaveGoods'
				}
			},
			resolve: {

			}
		})

		//景区系统-消费产品
		.state('app.storeGoods', {
			url: "/viewStoreGoods/list",
			views: {
				'main@': {
					template: require('./views/viewStoreGoods.html'),
					controller: 'viewStoreGoods'
				}
			},
			resolve: {

			}
		})

		//景区系统-产品分类列表
		.state('app.viewGoodsCategoryList', {
			url: "/viewGoodsCategory/list",
			views: {
				'main@': {
					template: require('./views/viewGoodsCategoryList.html'),
					controller: 'viewGoodsCategoryList'
				}
			},
			resolve: {

			}
		})

		//绑定会员
		.state('app.memberBind', {
			url: "/memberbind/list",
			views: {
				'main@': {
					template: require('./views/memberBind.html'),
					controller: 'memberBind'
				}
			},
			resolve: {

			}
		})

		//绑定会员
		.state('app.memberManager', {
			url: "/membermanager/list",
			views: {
				'main@': {
					template: require('./views/memberManager.html'),
					controller: 'memberManager'
				}
			},
			resolve: {

			}
		})

		//绑定会员
		.state('app.cardDetails', {
			url: "/cardDetails/list/:smart_card_no",
			views: {
				'main@': {
					template: require('./views/cardDetails.html'),
					controller: 'cardDetails'
				}
			},
			resolve: {
			}
		})

		//绑定会员
		.state('app.viewStatistics', {
			url: "/statistics/list",
			views: {
				'main@': {
					template: require('./views/statistics.html'),
					controller: 'statistics'
				}
			},
			resolve: {

			}
		})

		//景区系统-卡分类列表
		.state('app.viewCardCategoryList', {
			url: "/viewCardCategory/list",
			views: {
				'main@': {
					template: require('./views/viewCardCategoryList.html'),
					controller: 'viewCardCategoryList'
				}
			},
			resolve: {

			}
		})

		//景区系统-需要修改卡押金的配置列表
		.state('app.viewFunctionCardTypeList', {
			url: "/viewFunctionCardType/list",
			views: {
				'main@': {
					template: require('./views/viewFunctionCardTypeList.html'),
					controller: 'viewFunctionCardTypeList'
				}
			},
			resolve: {

			}
		})

		//景区系统-退单审核列表
		.state('app.viewBackOrderList', {
			url: "/viewBackOrder/list",
			views: {
				'main@': {
					template: require('./views/viewBackOrderList.html'),
					controller: 'viewBackOrderList'
				}
			},
			resolve: {

			}
		})

		//景区系统-按卡、产品统计购买次数
		.state('app.statisticsCardGoods', {
			url: "/statisticsCardGoods/list",
			views: {
				'main@': {
					template: require('./views/statisticsCardGoods.html'),
					controller: 'statisticsCardGoods'
				}
			},
			resolve: {

			}
		})

};

module.exports = router;