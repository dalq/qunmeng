module.exports = function ($scope, $stateParams, saleChannelList, wdnamelist, info,
	createorder, IdentityCodeValid, getuserinfobymobile, createSubsidyOrder, date2str,$modal) {

	init();
	//初始化页面参数
	function init(){
		$scope.orderInfoShow = false;	//显示购票界面控制器
		$scope.saleobj = {};
		$scope.product = {};
		$scope.obj = {
			'name': '',
			//最大购票数量
			'max': 99
		};
		//查询景区列表,并且将结果转换成树结构
		saleChannelList.get({'channel_list_code': $stateParams.code}, function (res) {
			if (res.errcode !== 0){
				alert(res.errmsg);
				return;
			}
			$scope.treeData = {};
			res.data.forEach(function(item) {
				item.show = true;
				if (!$scope.treeData.hasOwnProperty(item.place_code)) {
					$scope.treeData[item.place_code] = {
						'sale_name': item.place_name,
						'nodes': [],
						'show': true
					};
				}
				$scope.treeData[item.place_code].nodes.push(item);
			});
		});
	}

	//树结构列表选择,显示订单信息
	$scope.getInfo = function(obj) {
		if(!obj.id) return;		//不包含id为展开项,无法查看详细
		$scope.orderInfoShow = true;
		$scope.saleobj = obj;
		$scope.order = {
			'sale_code': obj.sale_code,
			'num': 0
		};

		$scope.obj.id = obj.id;
		$scope.obj.name = obj.sale_name;
		$scope.obj.price_type = obj.price_type;
		$scope.obj.govsubsidy_price = obj.govsubsidy_price;
		$scope.obj.tour_date_type = obj.tour_date_type;
		//更新出游时间
		$scope.section_date = {
			'label': date2str(new Date()),
			'value': date2str(new Date()),
			'opened': false
		};
		info.get({'id': obj.id}, function(res){
			if (res.errcode === 0){
				$scope.obj.detail = res.data.detail;
				$scope.obj.bookingnotes = res.data.bookingnotes;
			} else {
				$scope.orderInfoShow = false;
				alert(res.errmsg);
			}
		});
	};

	//确认购买
	$scope.buy = function () {
		//按钮设置为不可用,防止连续点击下多单
		$scope.btnstate = true;
		if($scope.section_date.label == null){

		}else if (typeof $scope.section_date.label === 'string') {
			$scope.order.tour_date = $scope.section_date.label;
		} else {
			$scope.order.tour_date = date2str($scope.section_date.label);
		}
		//-------------- 参数验证 -----------------------//
		if (!check()) {
			$scope.btnstate = false;
			return;
		}
		//-------------- 参数验证 -----------------------//
		createorder.save($scope.order, function (res) {
			if (res.errcode === 0) {
				alert('下单成功，请注意查收短信');
				$scope.order = {};
				$scope.obj.id = '';
				$scope.obj.name = '';
				$scope.obj.sale_code = '';
				$scope.orderInfoShow = false;
			} else {
				alert(res.errmsg);
			}
			$scope.btnstate = false;
		});

	};

	function check() {
		if (!$scope.order.cardno) {
			if (!confirm("未填写身份证，要强制下单吗?")) {
				return false;
			}
		} else if (!IdentityCodeValid($scope.order.cardno)) {
			return false;
		}
		if (!$scope.order.name) {
			alert("请填写姓名");
			return false;
		}
		if (!$scope.order.mobile) {
			alert("请填写购电话");
			return false;
		}
		if ($scope.order.num <= 0 || $scope.order.num > 99) {
			alert('请填写购票数量');
			$scope.order.num = 0;
			return false;
		}
		if ($scope.obj.tour_date_type == '1' && $scope.order.tour_date == '') {
			alert("请填写出游时间，该票种只能在填写的出游时间使用。");
			return false;
		}
		return true;
	}

	//选择出游时间
	$scope.open = function ($event, item) {
		$event.preventDefault();
		$event.stopPropagation();
		item.opened = true;
	}

	$scope.selectDate = function(){
		var modalInstance = $modal.open({
		    template: require('../views/sellPriceSetting.html'),
		    controller: 'sellPriceSetting',
		    size: 'lg',
		    resolve: {
		        items: function () {
		            return {
		                saleobj: $scope.saleobj
		            };
		        }
		    }
		});
		modalInstance.result.then(function (result) {
			$scope.section_date.label = date2str(new Date(result.date));
			$scope.product = result;
		});
	}

	$scope.visible = function(item){
		return !($scope.query && $scope.query.length > 0 && item.sale_name.indexOf($scope.query) == -1);
	};

	//票数量增加或减少
	$scope.numModify = function(num){
		$scope.order.num += num;
		if($scope.order.num < 0){
			$scope.order.num = 0;
		} else if($scope.order.num > 99){
			$scope.order.num = 99;
		}
	}

};