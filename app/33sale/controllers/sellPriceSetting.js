module.exports = function ($scope, $state, $stateParams, toaster, items, $resource, $modalInstance) {


	//日历要显示的数据
	$scope.data = [];

	$scope.result = [];

	$scope.dateArray = {};

	$scope.day = '';

	$scope.product_state_name = '';

	//日历显示信息的顺序和样式
	$scope.showattrarr = [
		{
			key: 'market_price_display',
			position: 'left',
			before: '<font color=red >市场价格: ¥</font>'
		},
		{
			key: 'guide_price_display',
			position: 'left',
			before: '<font color=green >居游价格: ¥</font>'
		},
		{
			key: 'cost_price_display',
			position: 'left',
			before: '<font color=red >成本价格: ¥</font>'
		}
	];

	//每一天的点击事件
	$scope.clickday = function (item) {
		

	};

	//日期转换
	function date2str(d) {
		if (d === undefined) {
			return "";
		}
		var month = (d.getMonth() + 1).toString();
		var day = d.getDate().toString();
		if (month.length == 1) month = '0' + month;
		if (day.length == 1) day = '0' + day;
		return d.getFullYear() + '-' + month + '-' + day;
	}

	//查询数据
	$scope.findinfoList = function () {
		var para = {
			sale_code: items.saleobj.code
		}
		$resource('/api/ac/tc/ticketSaleDayPriceService/getDayPriceBySaleCodeForSale', {}, {}).save(para, function (res) {
			if (res.errcode === 0) {
				$scope.dateArray
				for (var index = 0; index < res.data.daylist.length; index++) {
					var date = res.data.daylist[index].date;
					var id = res.data.daylist[index].id;
					$scope.dateArray[date] = id;
					var element = {};
					element.d = res.data.daylist[index].date;
					element.market_price_display = '<font color=red >' + res.data.daylist[index].market_price + '</font>';
					element.guide_price_display = '<font color=green >' + res.data.daylist[index].guide_price + '</font>';
					element.cost_price_display = '<font color=red >' + res.data.daylist[index].cost_price + '</font>';
					element.market_price = res.data.daylist[index].market_price;
					element.guide_price = res.data.daylist[index].guide_price;
					element.cost_price = res.data.daylist[index].cost_price;
					$scope.data[index] = element;
				}
				$scope.loadupdate();
			} else {
				toaster.warning({ title: '', body: res.errmsg });
			}
		});
	}
	$scope.findinfoList();

    $scope.options = {
		btn_flag: false,//是否显示按钮
		leftClick: function () {//左按钮函数
		},
		rightClick: function(){//右按钮函数
		},
		click: function (item) {//每天的点击事件
			if(item.d == '0' || item.d == 0){
				toaster.info({ title: '', body: '请选择当月日期' });
				return false;
			}
			var today = date2str(new Date());
			if (item.d < today) {
				toaster.warning({ title: item.d, body: '出游日期不能小于当天日期' });
				return false;
			}
			if (item.labelarr.length < 1) {
				toaster.warning({ title: item.d, body: '未设置价格日期无法选择' });
				return false;
			}
			item.data.date = item.data.d;
			$modalInstance.close(item.data);

		},
		state: 'state',
		checkbox: false
	}
}