module.exports = function($scope, $state, $modal, $modalInstance, groupsalelist, groupsale, createOrder, getDate, userinfo){

	$scope.groupobj = {};
	// $scope.section = {};
	// $scope.section.start = {};
	// $scope.section.start.date = new Date((new Date()/1000+86400)*1000);
	$scope.groupobjstate = 1;

	var name;

	// $scope.open = function(obj) {
	// 	obj.opened = true;
	// };

	 $scope.date = {
                'lable': date2str2(new Date()),
                //'lable': new Date(),
                //'value': date2str(new Date()),
                'opened': false
            }


             $scope.dateOpen = function ($event, item) {
                $event.preventDefault();
                $event.stopPropagation();
                item.opened = true;
            };
                function date2str2(d) {
                if (d === undefined) {
                    return "";
                }
                var month = (d.getMonth() + 1).toString();
                var day = d.getDate().toString();
                if (month.length == 1) month = '0' + month;
                if (day.length == 1) day = '0' + day;
                return d.getFullYear() + "-" + month + "-" + day;
            }

	groupsalelist().then(function(res) {
        if(res.errcode === 0)
        {
        	$scope.salearr = res.data;
        	$scope.groupobj.sale_code = $scope.salearr[0].code;
        }
        else
        {
            alert(res.errmsg);
        }
    });

    userinfo().then(function(res) {
		name = res.name;
    });

    $scope.detail = function(){

        detailmodal($scope.groupobj.sale_code);
        
    }

    //打开模态框
    function detailmodal(code)
    {
        var modalInstance = $modal.open({
          template: require('../views/saledetail.html'),
          controller: 'saledetail',
          resolve: {
            code : function(){
                return code;
            },
            groupsale : function(){
                return groupsale;
            }
          }
        });
    }

    //保存
	$scope.gogo = function(){

		if($scope.date.lable === undefined || $scope.date.lable == ''|| $scope.date.lable === null )
		{
			alert('出游时间不能为空');
			return;
		}
			 
		
		if($scope.groupobj.sale_code === undefined || $scope.groupobj.sale_code == '')
		{
			alert('预定团票不能为空');
			return;
		}

		if($scope.groupobj.guide_name === undefined || $scope.groupobj.guide_name == '')
		{
			alert('导游姓名不能为空');
			return;
		}

		if($scope.groupobj.guide_mobile === undefined || $scope.groupobj.guide_mobile == '')
		{
			alert('导游电话不能为空');
			return;
		}

		if($scope.groupobj.vehicle_number === undefined || $scope.groupobj.vehicle_number == '')
		{
			alert('车牌号不能为空');
			return;
		}
		if(typeof $scope.date.lable === 'string'){
                        $scope.tour_date=$scope.date.lable;
         }else{
                            $scope.tour_date=date2str2($scope.date.lable);
         }


		$scope.groupobj.arrival_date = $scope.tour_date;
		$scope.groupobj.username = name;
		createOrder.save($scope.groupobj, function(res){


			if(res.errcode === 0)
			{
				//$state.go('app.sellinggroup');
				alert('创建成功');
				$modalInstance.close();
			}
			else
			{
				alert(res.errmsg);
			}

		});
	};

	$scope.cancel = function(){

		$modalInstance.close();
		
	}

};
