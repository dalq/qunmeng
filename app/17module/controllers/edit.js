module.exports = function($scope,formconfig , $stateParams, model){
	var id = $stateParams.id;

	formconfig.start({
		'title' : '模块详情',
		'formtitle' : '模块基本信息',
		'elements' : model(),
		'info' : {
			'url' : '/api/as/sc/sysmodule/getById',
			'para' : {
				'id' : id
			}
		},
		'save' : {
			'url' : '/api/as/sc/sysmodule/save',
			'para' : {
				'id' : id
			},
			'to' : 'app.moduleList',
		}
	}, $scope);

	$scope.form = formconfig;

};