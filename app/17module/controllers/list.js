module.exports = function ($scope, tableconfig, $state, $resource) {
	
	tableconfig.start($scope, {
		'url': '/api/as/sc/sysmodule/getModuleList',
		'col': [
			{ 'title': '系统编号', 'col': 'system_code' },
			{ 'title': '模块编码', 'col': 'system_module_code' },
			{ 'title': '模块名称', 'col': 'system_module_name'},
			{ 'title': '模块别名', 'col': 'system_module_alias' },
			{ 'title': '启用标志', 'col': 'system_module_start_flag' },
			{ 'title': '排序', 'col' : 'system_module_sort'},
			{ 'title': '操作', 'col': 'btn' },
		],
		'btn': [
			{ 'title': '编辑', 'onclick': 'edit' },
			{ 'title': '删除', 'onclick': 'delete' },
		],
		'search': [
			{'title': '选择系统', 'type': 'select', 'name': 'system_code', 'show': true,
			 	'url' : '/api/as/sc/syssystem/getSystemList',			 
			},
		],
		'title': '模块列表',
		'delete': {
			'url': '/api/as/sc/sysmodule/delete',
		},
		'edit': {
			'to': 'app.moduleEdit',
		},
		
	});
	$scope.table = tableconfig;

	

};