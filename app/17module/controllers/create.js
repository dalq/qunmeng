module.exports = function($scope, formconfig, model){

	formconfig.start({
		'title' : '创建模块',
		'formtitle' : '模块基本信息',
		'elements' : model(),
		'save' : {
			'url' : '/api/as/sc/sysmodule/save',
			'to' : 'app.moduleList'
		}
	}, $scope);

};