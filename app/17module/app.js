var App = angular.module('module', []);

App.config(require('./router'));

//service
App.factory('moduleservice', require('./service'));

//Controllers
App.controller('moduleList', require('./controllers/list'));
App.controller('tempModuleList', require('./controllers/listbak'));
App.controller('moduleEdit', require('./controllers/edit'));
App.controller('moduleCreate',require('./controllers/create'));


module.exports = App;