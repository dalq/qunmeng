/**
 * 子模块service
 * DHpai
 */
var service = function($resource, $q, $state, $modal){
    
    var model = [
        {
            'title' : '系统编号',
            'id' : 'system_code',
            'type' : 'text',
        },
        {
            'title' : '模块编码',
            'id' : 'system_module_code',
            'type' : 'text',
        },
        {
            'title' : '模块名称',
            'id' : 'system_module_name',
            'type' : 'text',
        },
        {
            'title' : '模块别名',
            'id' : 'system_module_alias',
            'type' : 'text',
        },
        {
            'title' : '启用标志',
            'id' : 'system_module_start_flag',
            'type' : 'switch',
            'open': 1,
            'close':0,
        },
        {
            'title' : '排序',
            'id' : 'system_module_sort',
            'type' : 'number',
        }
        
    ];

    return {
        model : function(){
            return model;
        }
    };

};

module.exports = service;