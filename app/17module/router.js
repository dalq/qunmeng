 /**
 * 子模块路由
 * DHpai
 */

var router = function($urlRouterProvider, $stateProvider){


 	$stateProvider

     //临时
    .state('app.tempModuleList', {
        url: "/temp/templist",
        views: {
            'main@' : {
                template : require('./views/moduleList.html'),
                controller : 'tempModuleList',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
        }
    })

   //模块列表
    .state('app.moduleList', {
        url: "/module/modulelist",
        views: {
            'main@' : {
                template : require('../996tpl/views/table.html'),
                controller : 'moduleList',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
        }
    })
   
    //编辑模块信息
    .state('app.moduleEdit', {
        url: "/module/moduleedit/:id",
        views: {
            'main@' : {
                template : require('../996tpl/views/form.html'),
                controller : 'moduleEdit',
            }
        },
        resolve:{
            formconfig : function(formservice){
                return formservice.formconfig();
            },
            model : function(moduleservice){
                return moduleservice.model;
            }
        }
    })

    //创建模块
    .state('app.moduleCreate', {
        url: "/module/modulecreate",
        views: {
            'main@' : {
                template : require('../996tpl/views/form.html'),
                controller : 'moduleCreate',
            }
        },
        resolve:{
            formconfig : function(formservice){
                return formservice.formconfig();
            },
            model : function(moduleservice){
                return moduleservice.model;
            }
        }
    })



	;

};

module.exports = router;