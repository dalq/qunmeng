var App = angular.module('view', []);

App.config(require('./router'));
App.factory('viewservice', require('./service'));

App.controller('viewlist',require('./controllers/list'));
App.controller('viewsimplelist',require('./controllers/simplelist'));
App.controller('viewcreate',require('./controllers/create'));
App.controller('viewinfo',require('./controllers/info'));
App.controller('viewedit',require('./controllers/edit'));

App.directive('placebaseinfo',require('./directives/baseinfo'));
App.directive('placeview',require('./directives/view'));
App.directive('placestore',require('./directives/store'));




// App.controller('list',require('./controllers/list'));

module.exports = App;