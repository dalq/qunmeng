module.exports = function ($scope, $state, $resource, $timeout, $stateParams) {

	$scope.searchform = history.state ? history.state.searchform : {};

	/* 分页
     * ========================================= */
	$scope.maxSize = 5;            //最多显示多少个按钮
	$scope.bigCurrentPage = history.state ? history.state.bigCurrentPage : null;      //当前页码
	$scope.itemsPerPage = 10;         //每页显示几条

	$scope.conut = 0;
	$scope.load = function (numb) {
		if(numb == 1 && $scope.conut == 0 && history.state){
			$scope.conut = 1;
			return false;
		}
		history.replaceState({
			bigCurrentPage: $scope.bigCurrentPage,
			searchform: $scope.searchform
		}, '');
		 var para = {
			name:$scope.searchform.name,
			code:$scope.searchform.code,
			 //pageNo: history.state ? history.state.bigCurrentPage : 1,
			pageNo: $scope.bigCurrentPage,
		 	pageSize: $scope.itemsPerPage,
			type : $stateParams.type,
		 };

		
		//console.log($scope.type);
		//para = angular.extend($scope.searchform, para);

		$resource('/api/as/tc/placeview/list', {}, {}).save(para,function (res) {

			if (res.errcode !== 0) {
				alert("数据获取失败");
				return;
			}

			$scope.objs = res.data.results;
			$scope.bigTotalItems = res.data.totalRecord;
			$scope.bigCurrentPage = res.data.pageNo;

		});

	};
	$scope.load();

	$scope.myKeyup = function (e) {

		//IE 编码包含在window.event.keyCode中，Firefox或Safari 包含在event.which中
		var keycode = window.event ? e.keyCode : e.which;
		if (keycode == 13) {
			$scope.load();
		}
	};

	$scope.create = function () {
		if($stateParams.type == 'H'){
			$state.go('app.creatHotel');
		}else{
			$state.go('app.view_create');
		}
		
	};

	$scope.edit = function (id) {
		if($stateParams.type == 'H'){
			$state.go('app.creatHotel', { 'id': id });
		}else{
			$state.go('app.view_edit', { 'id': id });
		}
		
	};


	$scope.asort = function (id, asort) {


		$resource('/api/as/tc/place/updateAsort', {}, {}).save({ 'place_id': id, 'asort': asort }, function (res) {


			if (res.errcode === 0) {
				$scope.load();
			}
			else {
				alert(res.errmsg);
			}

		});

	};

	$scope.type = function (id) {


		$state.go('app.tkttype', { 'placeid': id });

	};


	$scope.createtkttype = function (id) {
		$state.go('app.tkttypecreate', { 'placeid': id });
	}

	$scope.device = function (code) {
		$state.go('app.devicelist', { 'placecode': code });
	}


};