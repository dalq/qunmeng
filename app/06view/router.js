/**
* 子模块路由
* dlq
*/

var router = function ($urlRouterProvider, $stateProvider) {


    $stateProvider


        //景区列表
        .state('app.view_list', {
            url: "/view/viewlist.html?type",
            views: {
                'main@': {
                    template: require('./views/viewList.html'),
                    controller: 'viewlist',
                }
            },
            resolve: {
            }
        })

        //创建景区
        .state('app.view_create', {
            url: "/view/viewcreate.html",
            views: {
                'main@': {
                    template: require('./views/viewCreate.html'),
                    controller: 'viewcreate',
                }
            },
            resolve: {
                'type': function () {
                    return 'J';
                },
                'placeid': function () {
                    return '';
                }
            }
        })




        //景区详情
        .state('app.view_info', {
            url: "/sys/viewinfo/:id",
            views: {
                'main@': {
                    template: require('../996tpl/views/form.html'),
                    controller: 'viewinfo',
                }
            },
            resolve: {
                formconfig: function (formservice) {
                    return formservice.formconfig();
                },
                viewmodel: function (viewservice) {
                    return viewservice.viewmodel;
                }
            }
        })



        //景区编辑
        .state('app.view_edit', {
            url: "/sys/viewedit/:id",
            views: {
                'main@': {
                    template: require('./views/viewEdit.html'),
                    controller: 'viewedit',
                }
            },
            resolve: {
                'type': function () {
                    return 'J';
                },
                'placeid': function () {
                    return '';
                }
            }
        })




        //景区简表
        .state('app.view_simplelist', {
            url: "/view/simplelist",
            template: require('./views/list.html'),
            controller: 'viewsimplelist',
            resolve: {

            }
        })





        ;

};

module.exports = router;