// /**
//  * 子模块service
//  * dlq
//  */
// var service = function($resource, $q){

//     //view模型
//     var viewmodel = [
//         {
//             'title' : '名称',
//             'id' : 'name',
//             'type' : 'text',
//             'required' : true,
//             'placeholder' : '必填',
//         },
//         {
//             'title' : '景区编号',
//             'id' : 'code',
//             'type' : 'text',
//             'required' : true,
//             'placeholder' : '必填',
//         },
//         {
//             'title' : '经度',
//             'id' : 'longitude',
//             'type' : 'number',
//         },
//         {
//             'title' : '纬度',
//             'id' : 'latitude',
//             'type' : 'number',
//         },
//         {
//             'title' : '地址',
//             'id' : 'address',
//             'type' : 'textarea',
//             'state' : 'normal'
//         },
//         {
//             'title' : '图片',
//             'id' : 'img',
//             'type' : 'image',
//         },
//         {
//             'title' : '简介',
//             'id' : 'content',
//             'type' : 'textarea',
//         },
//         {
//             'title' : '营业时间',
//             'id' : 'open_time',
//             'type' : 'text',
//         },
        
//         {
//             'title' : '省份',
//             'id' : 'province',
//             'type' : 'number',
//         },
//         {
//             'title' : '城市',
//             'id' : 'city',
//             'type' : 'select',
//             'url' : '/api/us/sc/city/citylist',
//             'n' : 'NAME',
//             'v' : 'CODE',
//         },
//         {
//             'title' : '区域',
//             'id' : 'area',
//             'type' : 'number',
//         },
//         {
//             'title' : 'logo',
//             'id' : 'logo',
//             'type' : 'image',
//         },
//         {
//             'title' : '主题',
//             'id' : 'theme',
//             'type' : 'text',
//         },
//         {
//             'title' : '子名称',
//             'id' : 'subname',
//             'type' : 'text',
//         },
//         {
//             'title' : '开放日期',
//             'id' : 'open_date',
//             'type' : 'text',
//         },
//         {
//             'title' : '服务承诺',
//             'id' : 'promise',
//             'type' : 'text',
//         },
//         {
//             'title' : '预定须知',
//             'id' : 'book_info',
//             'type' : 'textarea',
//         },
//         {
//             'title' : '好评',
//             'id' : 'ev_good',
//             'type' : 'number',
//         },
//         {
//             'title' : '一般评价',
//             'id' : 'ev_general',
//             'type' : 'number',
//         },
//         {
//             'title' : '差评',
//             'id' : 'ev_bad',
//             'type' : 'number',
//         },
//         {
//             'title' : '星级',
//             'id' : 'star',
//             'type' : 'text',
//         },
//         {
//             'title' : '排序',
//             'id' : 'asort',
//             'type' : 'number',
//         },
//         {
//             'title' : '全景地址',
//             'id' : 'view720_url',
//             'type' : 'text',
//         },
//         {
//             'title' : '电子杂志地址',
//             'id' : 'ebook_url',
//             'type' : 'text',
//         },
//         {
//             'title' : '优惠政策',
//             'id' : 'fav_policy',
//             'type' : 'textarea',
//             'state' : 'normal',
//         },
//         {
//             'title' : '提前入园时间（小时）',
//             'id' : 'bef_hour',
//             'type' : 'number',
//         },
        

//     ];

   
//     return {

//         viewmodel : function(){
//             return viewmodel;
//         },
       
//     };

// };

// module.exports = service;

/**
 * 子模块service
 * dlq
 */
var service = function($resource,  $q, $http){

    var list =  "/api/as/tc/placeview/list";

    var create =  "/api/as/tc/placeview/create";
    
    var info =  "/api/as/tc/placeview/info";

    var update =  "/api/as/tc/placeview/update";

    var slist =  "/api/as/tc/placeview/jlist";

    //带景区判断的
    var sslist =  "/api/as/tc/placeview/jjlist";

    var city =  "/api/us/sc/city/citylist";

    var updateplacemerchant =  "/api/as/tc/placemerchant/update";
    // 商户信息详情
    var merchantinfo =  '/api/as/tc/placemerchant/info';
    // 更新商户表首页排序
    var gogosort =  '/api/as/tc/place/update';

    
    return {

        list : function(){
            return $resource(list, {}, {});
        },
    	create : function(){
            return $resource(create, {}, {});
        },
        info : function(){
            return $resource(info, {}, {});
        },
        update : function(){
            return $resource(update, {}, {});
        },
        updateplacemerchant : function(){
            return $resource(updateplacemerchant, {}, {});
        },
        merchantinfo : function(){
            return $resource(merchantinfo, {}, {});
        },
        gogosort : function(){
            return $resource(gogosort, {}, {});
        },
        slist : function(obj){
            var deferred = $q.defer(); // 声明延后执行，表示要去监控后面的执行  
            $http({method: 'GET', params: obj, url: slist}).then(
                function(data){
                    deferred.resolve(data.data);
                },
                function(data){
                    deferred.reject(data.data);
                });
            // success(function(data, status, headers, config) {  
            //     deferred.resolve(data);  // 声明执行成功，即http请求数据成功，可以返回数据了  
            // }).  
            // error(function(data, status, headers, config) {  
            //     deferred.reject(data);   // 声明执行失败，即服务器返回错误  
            // });  
            return deferred.promise;   // 返回承诺，这里并不是最终数据，而是访问最终数据的API 
        },
        sslist : function(obj){
            var deferred = $q.defer(); // 声明延后执行，表示要去监控后面的执行  
            $http({method: 'GET', params: obj, url: sslist}).then(
                function(data){
                    deferred.resolve(data.data);
                },
                function(data){
                    deferred.reject(data.data);
                });
            // success(function(data, status, headers, config) {  
            //     deferred.resolve(data);  // 声明执行成功，即http请求数据成功，可以返回数据了  
            // }).  
            // error(function(data, status, headers, config) {  
            //     deferred.reject(data);   // 声明执行失败，即服务器返回错误  
            // });  
            return deferred.promise;   // 返回承诺，这里并不是最终数据，而是访问最终数据的API 
        },
        city : function(obj){
            var deferred = $q.defer(); // 声明延后执行，表示要去监控后面的执行  
            $http({method: 'GET', params: obj, url: city}).then(
                function(data){
                    deferred.resolve(data.data);
                },
                function(data){
                    deferred.reject(data.data);
                });
            // success(function(data, status, headers, config) {  
            //     deferred.resolve(data);  // 声明执行成功，即http请求数据成功，可以返回数据了  
            // }).  
            // error(function(data, status, headers, config) {  
            //     deferred.reject(data);   // 声明执行失败，即服务器返回错误  
            // });  
            return deferred.promise;   // 返回承诺，这里并不是最终数据，而是访问最终数据的API 
        }
       
    };

};

module.exports = service;