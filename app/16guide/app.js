/**
 * 子模块入口
 * dlq
 */

var App = angular.module('guide', []);

App.config(require('./router'));
App.factory('guideservice', require('./service'));

//设备管理
App.controller('shakedevicelist',require('./controllers/shakedevicelist'));
App.controller('devicecreate',require('./controllers/devicecreate'));
App.controller('deviceedit',require('./controllers/deviceedit'));
App.controller('deviceinfo',require('./controllers/deviceinfo'));

//问题管理
App.controller('shakequestionlist',require('./controllers/shakequestionlist'));
App.controller('shakequestioncreate',require('./controllers/shakequestioncreate'));
App.controller('shakequestionedit',require('./controllers/shakequestionedit'));
App.controller('shakequestioninfo',require('./controllers/shakequestioninfo'));

//评价管理
App.controller('shakeevaluategrouplist',require('./controllers/shakeevaluategrouplist'));
App.controller('shakeevaluatetourist',require('./controllers/shakeevaluatetourist'));
App.controller('shakeevaluatetouristinfo',require('./controllers/shakeevaluatetouristinfo'));

//评价系统图表
App.controller('evaluatechart',require('./controllers/evaluatechart'));
App.controller('evaluatequestioninfo',require('./controllers/evaluatequestioninfo'));
App.controller('evaluatechart1',require('./controllers/evaluatechart1'));
App.controller('evaluatechart2',require('./controllers/evaluatechart2'));
App.controller('evaluatechart3',require('./controllers/evaluatechart3'));

module.exports = App;