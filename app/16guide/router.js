/**
 * 子模块路由
 * djp
 */

var router = function($urlRouterProvider, $stateProvider){

 	$stateProvider

	//设备列表
    .state('app.shakedevicelist', {
 	  	url: "/guide/shakedevicelist.html",
        views: {
            'main@' : {
                template : require('../16guide/views/shakedevicelist.html'),
                controller : 'shakedevicelist',
            }
        },
        resolve:{
          shakedevicelist: function (guideservice) {
              return guideservice.shakedevicelist;
          },
          shakedevicedel: function (guideservice) {
              return guideservice.shakedevicedel;
          },
          userinfo: function (guideservice) {
              return guideservice.userinfo;
          },
          date2str : function(utilservice){
            return utilservice.date2str;
          },
          getDate : function(utilservice){
              return utilservice.getDate;
          },
        }
    })

    //创建景区
    .state('app.shakedevicecreate', {
        url: "/guide/shakedevicecreate.html/:id",
        views: {
            'main@' : {
                template : require('../16guide/views/shakedevice.html'),
                controller : 'devicecreate',
            }
        },
        resolve:{
          shakedevice : function(guideservice){
            return guideservice.shakedevice();
        },
        shakedeviceinfo : function(guideservice){
            return guideservice.shakedeviceinfo();
        },
        dictbytypelist : function(guideservice){
            return guideservice.dictbytypelist;
        },
        shakecompanyinfolist : function(guideservice){
            return guideservice.shakecompanyinfolist();
        }
        ,
        shakegroupinfolist : function(guideservice){
            return guideservice.shakegroupinfolist();
        }
        ,
        getDate : function(utilservice){
            return utilservice.getDate;
        }
        ,
        date2str : function(utilservice){
            return utilservice.date2str;
        },
        str2date : function(utilservice){
            return utilservice.str2date;
        },
        savedevicerecode : function(guideservice){
            return guideservice.savedevicerecode();
        },
        userinfo : function(guideservice){
            return guideservice.userinfo;
        }
        }
    })

	//设备详情
    .state('app.shakedevice_info', {
        url: "/guide/shakedeviceinfo.html/:id",
        views: {
            'main@' : {
                template : require('../996tpl/views/form.html'),
                controller : 'deviceinfo',
            }
        },
        resolve:{
            formconfig : function(formservice){
                return formservice.formconfig();
            },
            viewmodel : function(guideservice){
                return guideservice.viewmodel;
            }
        }
    })

    //设备编辑
    .state('app.shakedevice_edit', {
        url: "/guide/shakedeviceedit.html/:id",
        views: {
            'main@' : {
                template : require('../996tpl/views/form.html'),
                controller : 'deviceedit',
            }
        },
        resolve:{
            formconfig : function(formservice){
                return formservice.formconfig();
            },
            viewmodel : function(guideservice){
                return guideservice.viewmodel;
            }
        }
    })

    //问题列表
    .state('app.shakequestionlist', {
      	url: "/guide/shakequestionlist.html",
        views: {
            'main@' : {
                template : require('../16guide/views/shakequestionlist.html'),
                controller : 'shakequestionlist',
            }
        },
        resolve:{
        }
      })

    //创建问题
    .state('app.shakequestioncreate', {
        url: "/guide/shakequestioncreate.html",
        views: {
            'main@' : {
                template : require('../996tpl/views/form.html'),
                controller : 'shakequestioncreate',
            }
        },
        resolve:{
            formconfig : function(formservice){
                return formservice.formconfig();
            },
            viewmodel : function(guideservice){
                return guideservice.viewmodel1;
            }
        }
    })

	//问题详情
    .state('app.shakequestioninfo', {
        url: "/guide/shakequestioninfo.html/:id",
        views: {
            'main@' : {
                template : require('../16guide/views/shakequestioninfo.html'),
                controller : 'shakequestioninfo',
            }
        },
        resolve:{
        }
    })

    //问题编辑
    .state('app.shakequestion_edit', {
        url: "/guide/shakequestionedit.html/:id",
        views: {
            'main@' : {
                template : require('../996tpl/views/form.html'),
                controller : 'shakequestionedit',
            }
        },
        resolve:{
            formconfig : function(formservice){
                return formservice.formconfig();
            },
            viewmodel : function(guideservice){
                return guideservice.viewmodel1;
            }
        }
    })

    //评价列表
    .state('app.shakeevaluategrouplist', {
      	url: "/guide/shakeevaluategrouplist.html",
        views: {
            'main@' : {
                template : require('../996tpl/views/table.html'),
                controller : 'shakeevaluategrouplist',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            }
        }
      })

	//游客列表
    .state('app.shakeevaluatetourist', {
      	url: "/guide/shakeevaluatetourist.html/:id",
        views: {
            'main@' : {
                template : require('../996tpl/views/table.html'),
                controller : 'shakeevaluatetourist',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
            shakeanswerslist : function(guideservice){
                return guideservice.shakeanswerslist();
            }
        }
      })

    

      .state('app.evaluatechart', {
      	url: "/guide/evaluatechart.html",
        views: {
            'main@' : {
                template : require('./views/evaluatechart.html'),
                controller : 'evaluatechart',
            }
        },
        resolve:{
        	dictbytypelist : function(guideservice){
                return guideservice.dictbytypelist;
            },
            shakecompanyinfolist : function(guideservice){
                return guideservice.shakecompanyinfolist();
            },
            shakegroupinfolist : function(guideservice){
                return guideservice.shakegroupinfolist();
            },
            date2str : function(utilservice){
                return utilservice.date2str;
            },
            getDate : function(utilservice){
                return utilservice.getDate;
            },
            shakeanswer : function(guideservice){
                return guideservice.shakeanswer();
            },
            questionstatisticlist : function(guideservice){
                return guideservice.questionstatisticlist();
            },
            peoplerebatelist : function(guideservice){
                return guideservice.peoplerebatelist();
            },
            shakeanswerslist : function(guideservice){
                return guideservice.shakeanswerslist();
            }
        }
      })

      .state('app.evaluatechart1', {
      	url: "/guide/evaluatechart1.html",
        views: {
            'main@' : {
                template : require('./views/evaluatechart1.html'),
                controller : 'evaluatechart1',
            }
        },
        resolve:{
        	shakegroupinfolist : function(guideservice){
                return guideservice.shakegroupinfolist();
            },
            shakecompanyinfolist : function(guideservice){
                return guideservice.shakecompanyinfolist();
            },
            totalevaluatelist : function(guideservice){
                return guideservice.totalevaluatelist();
            },
            getDate : function(utilservice){
                return utilservice.getDate;
            }
        }
      })

      .state('app.evaluatechart2', {
      	url: "/guide/evaluatechart2.html",
        views: {
            'main@' : {
                template : require('./views/evaluatechart2.html'),
                controller : 'evaluatechart2',
            }
        },
        resolve:{
            shakecompanyinfolist : function(guideservice){
                return guideservice.shakecompanyinfolist();
            },
            totalevaluatelist : function(guideservice){
                return guideservice.totalevaluatelist();
            },
            getDate : function(utilservice){
                return utilservice.getDate;
            }
        }
      })
      
      .state('app.evaluatechart3', {
      	url: "/guide/evaluatechart3.html",
        views: {
            'main@' : {
                template : require('./views/evaluatechart3.html'),
                controller : 'evaluatechart3',
            }
        },
        resolve:{
        	shakecompanyinfolist : function(guideservice){
                return guideservice.shakecompanyinfolist();
            },
            questionhpllist : function(guideservice){
                return guideservice.questionhpllist();
            }
        }
      })
      .state('app.evaluatequestioninfo', {
      	url: "/guide/evaluatequestioninfo.html",
        views: {
            'main@' : {
                template : require('./views/evaluatequestioninfo.html'),
                controller : 'evaluatequestioninfo',
            }
        },
        resolve:{
        	shakeanswerslist : function(guideservice){
              return guideservice.shakeanswerslist();
          }
        }
      })

};

module.exports = router;