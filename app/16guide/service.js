/**
 * 子模块service
 * dlq
 */
var service = function($resource, $q, $http){

	//设备view模型
    var viewmodel = [
        {
            'title' : '机器码',
            'id' : 'code',
            'type' : 'text',
            'required' : true,
            'placeholder' : '必填',
        },
        {
            'title' : '设备名称',
            'id' : 'name',
            'type' : 'text',
            'required' : true,
            'placeholder' : '必填',
        },
        {
            'title' : '设备绑定类型',
            'id' : 'binding_type',
            'type' : 'select',
            'url' : '/api/as/tc/place/findPlaceCodeTypeList?type=general_evaluate_type',
            'n' : 'label',
            'v' : 'value',
        },
        {
            'title' : '设备所属机构',
            'id' : 'binding_company_code',
            'type' : 'select',
            'url' : '/api/as/gc/shakecompany/companyInfoList',
            'n' : 'name',
            'v' : 'binding_company_code',
        },
        {
            'title' : '绑定团名',
            'id' : 'binding_code_new',
            'type' : 'select',
            'url' : '/api/as/gc/shakecompany/groupInfoList',
            'n' : 'group_name',
            'v' : 'group_id',
        },
        {
            'title' : '绑定时间',
            'id' : 'binding_time',
            'type' : 'date1',
            'value' : 'now',
        },
        {
            'title' : '最大报名人数',
            'id' : 'binding_maxsignup',
            'type' : 'number',
        },
        {
            'title' : '备注',
            'id' : 'remarks',
            'type' : 'text',
        },
    ];

    //问题view模型
    var viewmodel1 = [
        {
            'title' : '问题类型',
            'id' : 'category',
            'type' : 'select',
            'info' : [
				{'name' : '旅行社评价', 'value' : '1'},
				{'name' : '景区评价', 'value' : '2'}
			],
        },
        {
            'title' : '问题',
            'id' : 'question',
            'type' : 'text',
            'required' : true,
            'placeholder' : '必填',
        },
        {
            'title' : '排序',
            'id' : 'sort',
            'type' : 'text',
            'value' : 0,
        },
    ];



	var userinfo = "/api/as/info";

	//销售品类型
    var dictbytypelist = '/api/as/tc/place/findPlaceCodeTypeList'

    //导游评价系统摇一摇设备
    var shakedevicelist = '/api/as/gc/shakedevice/findlist';

    var shakedevice = '/api/as/gc/shakedevice/save';

    var shakedeviceinfo = '/api/as/gc/shakedevice/getone';

    var shakedevicedel = '/api/as/gc/shakedevice/del';

    var savedevicerecode = '/api/as/gc/shakedevice/saverecode';

    var shakecompanyinfolist = '/api/as/gc/shakecompany/companyInfoList';

    var shakegroupinfolist = '/api/as/gc/shakecompany/groupInfoList';

    var userinfo = "/api/as/info";

    //导游评价系统问题
    var shakeevaluatequestion = '/api/as/gc/shakeevaluatequestion/save';
  
    var shakeevaluatequestionlist = '/api/as/gc/shakeevaluatequestion/list';

    var shakeevaluatequestioninfo = '/api/as/gc/shakeevaluatequestion/sel';
    
    var shakeevaluatequestiondel = '/api/as/gc/shakeevaluatequestion/del';


    //导游评价系统
    var shakeevaluategroup = '/api/as/gc/shakeevaluateanswer/coldlist';

    var shakeevaluatetouristlist = '/api/as/gc/shakeevaluateanswer/openidByCodeList';

    var shakeevaluateanswerlist = '/api/as/gc/shakeevaluateanswer/answerslist';

    var shakeevaluatecountlist = '/api/as/gc/shakeevaluateanswer/answertypecountlist';

    var shakeanswers = '/api/ac/gc/shakeEvaluateAnswerService/save';

    var shakeanswer = '/api/as/gc/shakeevaluateanswer/getanswerlist';

    var shakegetquestion = '/api/as/gc/shakeevaluatequestion/findquerybydevicelist';

    //评价系统图标分析
    var questionstatisticlist = '/api/as/gc/shakestatistics/questionstatisticlist';

    var peoplerebatelist = '/api/ac/gc/ShakeEvaluateStatisticsService/rebatelist';

    var shakeanswerslist = '/api/as/gc/shakeevaluateanswer/shakeanswerslist';

    //评价系统图标分析时间轴
    var totalevaluatelist = '/api/ac/gc/ShakeEvaluateStatisticsService/evaluatechart1';

    //评价系统图标分析景区综合评价
    var questionhpllist = '/api/ac/gc/ShakeEvaluateStatisticsService/evaluatechart3';


    return {

    	userinfo : function(){
          var deferred = $q.defer(); // 声明延后执行，表示要去监控后面的执行  
          // $http({method: 'GET', url: userinfo}).
          // success(function(data, status, headers, config) {  
          //   deferred.resolve(data);  // 声明执行成功，即http请求数据成功，可以返回数据了  
          // }).  
          // error(function(data, status, headers, config) {  
          //   deferred.reject(data);   // 声明执行失败，即服务器返回错误  
          // });  
          $http({method: 'GET', url: userinfo}).then(
            function(data){
                deferred.resolve(data.data);
            },
            function(data){
                deferred.reject(data.data); 
            }
          );
          return deferred.promise;   // 返回承诺，这里并不是最终数据，而是访问最终数据的API
        },
    	dictbytypelist : function(obj){
            var deferred = $q.defer(); // 声明延后执行，表示要去监控后面的执行  
            $http({method: 'GET', params: obj, url: dictbytypelist}).then(
                function(data){
                    deferred.resolve(data.data);
                },
                function(data){
                    deferred.reject(data.data);
                });
            // success(function(data, status, headers, config) {  
            //     deferred.resolve(data);  // 声明执行成功，即http请求数据成功，可以返回数据了  
            // }).  
            // error(function(data, status, headers, config) {  
            //     deferred.reject(data);   // 声明执行失败，即服务器返回错误  
            // });  
            return deferred.promise;   // 返回承诺，这里并不是最终数据，而是访问最终数据的API 
        },

        

        shakedevicelist : function(){
            return $resource(shakedevicelist, {}, {});
        },
        shakedevice : function(){
            return $resource(shakedevice, {}, {});
        },
        shakedeviceinfo : function(){
            return $resource(shakedeviceinfo, {}, {});
        },
        shakedevicedel : function(){
            return $resource(shakedevicedel, {}, {});
        },
        savedevicerecode : function(){
            return $resource(savedevicerecode, {}, {});
        },
        shakecompanyinfolist : function(){
            return $resource(shakecompanyinfolist, {}, {});
        },
        shakegroupinfolist : function(){
            return $resource(shakegroupinfolist, {}, {});
        },



        shakeevaluatequestion : function(){
            return $resource(shakeevaluatequestion, {}, {});
        },
        shakeevaluatequestionlist : function(){
            return $resource(shakeevaluatequestionlist, {}, {});
        },
        shakeevaluatequestioninfo : function(){
            return $resource(shakeevaluatequestioninfo, {}, {});
        },
        shakeevaluatequestiondel : function(){
            return $resource(shakeevaluatequestiondel, {}, {});
        },
        
        
        
        shakeevaluategroup : function(){
            return $resource(shakeevaluategroup, {}, {});
        },
        shakeevaluatetouristlist : function(){
            return $resource(shakeevaluatetouristlist, {}, {});
        },
        shakeevaluateanswerlist : function(){
            return $resource(shakeevaluateanswerlist, {}, {});
        },
        shakeevaluatecountlist : function(){
            return $resource(shakeevaluatecountlist, {}, {});
        },
        shakeanswers : function(){
            return $resource(shakeanswers, {}, {});
        },
        shakeanswer : function(){
            return $resource(shakeanswer, {}, {});
        },
        shakegetquestion : function(){
            return $resource(shakegetquestion, {}, {});
        },


        questionstatisticlist : function(){
            return $resource(questionstatisticlist, {}, {});
        },
        peoplerebatelist : function(){
            return $resource(peoplerebatelist, {}, {});
        },
        shakeanswerslist : function(){
            return $resource(shakeanswerslist, {}, {});
        },

        totalevaluatelist : function(){
            return $resource(totalevaluatelist, {}, {});
        },

        questionhpllist : function(){
            return $resource(questionhpllist, {}, {});
        },

        viewmodel : function(){
            return viewmodel;
        },
        viewmodel1 : function(){
            return viewmodel1;
        },

    };

};

module.exports = service;