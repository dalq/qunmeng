module.exports = function($scope, $resource, $state, $stateParams, shakedevice, shakedeviceinfo, dictbytypelist,
  shakecompanyinfolist, shakegroupinfolist, getDate, savedevicerecode, userinfo, toaster, date2str, str2date){
  $scope.obj = {};
  $scope.isshow = '1';
  
  $scope.vm = {
    'date' : '',
    'options' : {
        format: "YYYY-MM-DD",
        locale : 'zh-cn'
    }
  }
  $scope.search = {
    'isSelected' : {
      
    }
  }


  //用户信息
	function isable()
	{
		userinfo().then(function(res) {
			$scope.company_code = res.company_code;
	        if(res.company_code != 'L0002')
	    	{
	    		$scope.isable = '1';
	    	}
      });
      console.log('xiangqing');
      console.log(res);
	}
    

    //机器id
    var id = $stateParams.id;
    console.log(id);
    dictbytypelist({}).then(function(res) {
        console.log(res);
        console.log('~~~~~~~~~');
        if(res.errcode === 0)
        {
          
            $scope.typearr = res.data;
            console.log($scope.typearr);
            if(id === '')   //新建
            {
            console.log(id);
            $scope.isable = '0';
            console.log($scope.isable);
            
                $scope.obj.binding_type = '1';
                getCompany($scope.obj.binding_type);
                getGroup($scope.obj.binding_company_code);
            }
            else    
            {
                isable();
                $resource('/api/as/gc/shakedevice/getone', {}, {}).
                save({'id' : id},function (res) {
                    if (res.errcode != 0) {
                        toaster.success({title:"",body:res.errmsg});
                        return;
                    }
                    $scope.vm.date = res.data.binding_time;
                    // $scope.section.start.date = new Date(info.data.binding_time);
                    $scope.obj = res.data;
                    $scope.obj.binding_code = $scope.obj.binding_code.substr(0,$scope.obj.binding_code.indexOf('_'));
                    if($scope.obj.binding_type != '1'){
                      $scope.isshow = '0';
                    }
                        console.log('!!!走道这');
                        getCompany(res.data.binding_type)
                        getGroup(res.data.binding_company_code);
                });
               
            }
        }
        else
        {
            alert(res.errmsg);
        }
    });

    
    function getCompany(type){
        $scope.companyarr = [];
        $resource('/api/as/tc/place/findPlaceCodeNameList', {}, {}).
        save({'binding_type' : type},function (res) {
            if (res.errcode != 0) {
                toaster.error({title:"",body:res.errmsg});
                return;
            }
            $scope.companyarr = res.data;

        });
        
    }

    function getGroup(type){
        $scope.grouparr = [];
        $resource('/api/as/gc/shakecompany/groupInfoList', {}, {}).
        save({'company_code' : type},function (res) {
            if (res.errcode != 0) {
                toaster.error({title:"",body:res.errmsg});
                return;
            }
            $scope.grouparr = res.data;

        });
        
    }

    $scope.flag = {
      'binding_type' : ''
    }
    $scope.changeCompany = function(type){
    	if(type == '1'){
    		$scope.isshow = '1';
    		$scope.obj.binding_time = new Date();
    	}else{
    		$scope.isshow = '0';
    		$scope.obj.binding_time = '';
      }
      if(type == 'J'){
        alert('eeee');
        $scope.obj.binding_type = '2';
        console.log($scope.obj.binding_type);
      }
        getCompany(type);
    }

    $scope.changeGroup = function(type){

        getGroup(type);
    }

    $scope.gogo = function(){

        console.log($scope.search);
        $scope.obj.binding_company_code = $scope.search.isSelected.code;
        console.log($scope.obj);

        if($scope.obj.code === undefined)
        {
            alert('设备编号必填');
            return;
        }

        if($scope.obj.name === undefined)
        {
            alert('设备名称必填');
            return;
        }

        if($scope.obj.binding_company_code === undefined || $scope.obj.binding_company_code == null)
        {
            alert('设备所属机构必填');
            return;
        }

        if($scope.obj.binding_type == '1')
    	{
        	if($scope.obj.binding_code === undefined || $scope.obj.binding_code == null)
	        {
	            alert('设备绑定团名必填');
	            return;
	        }	
	        if($scope.obj.binding_maxsignup == null)
	        {
	            alert('最大报名人数必填');
	            return;
	        }
      }
        console.log('~~~~~~~' + $scope.obj.subsidy_id);
        if ($scope.obj.subsidy_id === undefined){
            alert('活动补贴id必填');
            return;
        }



    	if($scope.obj.binding_code == null || $scope.obj.binding_code == ''){
    		$scope.obj.binding_code = $scope.obj.binding_company_code;
    		$scope.obj.binding_time = null;
    	}else{
    		$scope.obj.binding_code = $scope.obj.binding_code + '_' + date2str($scope.vm.date._d);
        $scope.obj.binding_time = date2str($scope.vm.date._d);
      }
      $resource('/api/as/gc/shakedevice/save', {}, {}).
      save($scope.obj, function(res){

            console.log(res);
            if(res.errcode !== 0)
            {
                alert(res.errmsg);
                return;
            }
            alert('创建成功');
            $state.go('app.shakedevicelist');
            

            if(id != '' && $scope.company_code != 'L0002')
        	{
            $resource('/api/as/gc/shakedevice/saverecode', {}, {}).
        		save($scope.obj, function(res){

		            console.log(res);
		            if(res.errcode !== 0)
		            {
		                alert("插入记录表失败");
		                return;
		            }

            }); 
            

            }

            $state.go('app.shakedevicelist');

        });

    };

};


