module.exports = function($scope, formconfig, $stateParams, viewmodel){

	var id = $stateParams.id;

	formconfig.start({
		'title' : '问题详情',
		'formtitle' : '问题基本信息',
		'elements' : viewmodel(),
		'info' : {
			'url' : '/api/as/gc/shakeevaluatequestion/sel',
			'para' : {'id' : id}
		},
		'save' : {
			'url' : '/api/as/gc/shakeevaluatequestion/save',
			'to' : 'app.shakequestionlist',
			'para' : {'id' : id}
		}
	}, $scope);

	$scope.form = formconfig;

};

