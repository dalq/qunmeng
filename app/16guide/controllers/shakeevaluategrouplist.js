module.exports = function($scope, tableconfig){

	tableconfig.start($scope, {
		'url' : '/api/as/gc/shakeevaluateanswer/coldlist',
		'col' : [
			{'title' : '团名', 'col' : 'binding_code'},
			{'title' : '团人数', 'col' : 'openid_count'},
			{'title' : '操作', 'col' : 'btn'},
		],
		'btn' : [
			{'title' : '游客列表', 'onclick' : 'info'},
		],
		'search' : [],
		'page' : 'no',
		'title' : '评价列表',
		'info' : {
			'to' : 'app.shakeevaluatetourist',
		}
	});
	$scope.table = tableconfig;

};
/*module.exports = function($scope, $state, shakeevaluategrouplist){

	shakeevaluategrouplist.get({}, function(res){
            console.log(res.data);
            if(res.errcode !== 0)
            {
                alert(res.errmsg);
                return;
            }
            $scope.objs = res.data;

        });

	$scope.edit = function(code)
    {
        $state.go('app.shakeevaluatetourist', {'code' : code});
    }






};*/