module.exports = function($scope, $stateParams, $modal, tableconfig, shakeanswerslist){

	var id = $stateParams.id;

	tableconfig.start($scope, {
		'url' : '/api/as/gc/shakeevaluateanswer/openidByCodeList',
		'para' : {'id' : id},
		'col' : [
			{'title' : '游客名', 'col' : 'openid'},
			{'title' : '操作', 'col' : 'btn'},
		],
		'btn' : [
			{'title' : '问题详情', 'onclick' : 'info'},
		],
		'search' : [],
		'page' : 'no',
		'title' : '游客列表'/*,
		'info' : {
			'to' : 'app.shakeevaluatetourist',
		}*/
	});
	$scope.table = tableconfig;

	$scope.info = function(item){

        var modalInstance = $modal.open({
          template: require('../views/shakeevaluatetouristinfo.html'),
          controller: 'shakeevaluatetouristinfo',
          size: 'lg',
          resolve: {
            openid : function(){
                return item.openid;
            },
            shakeanswerslist : function(){
                return shakeanswerslist;
            } 
          }
        });

        modalInstance.opened.then(function() {// 模态窗口打开之后执行的函数  
			console.log('modal is opened');  
		});  
		modalInstance.result.then(
			function(result) {  
				console.log(result);  
			}, 
			function(reason) {  
				console.log("点击空白区域了");// 点击空白区域，总会输出backdrop
			}
		);
    }


};