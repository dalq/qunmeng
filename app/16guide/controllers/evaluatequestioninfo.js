module.exports = function($scope, shakeanswerslist, openid, $modalInstance){
    $scope.info = {
      'category' : '2'
    };
    function getlist(type){
      shakeanswerslist.get({'openid':openid, 'binding_type': type}, function(res){
        var tkt = new Object();
        var restkt = new Array();

        if(res.errcode !== 0)
        {
            alert("数据获取失败");
            return;
        }
        $scope.questionarr = res.data;
        for(var i = 0, j = res.data.length; i < j; i++)
        {
            var tt = res.data[i];
            var v = tt.binding_code;
            if(!tkt.hasOwnProperty(v))
            {
                tkt[v] = new Object();
                tkt[v].answerarr = new Array();
                tkt[v].groupid = tt.binding_code;
                tkt[v].groupname = tt.group_name;
                tkt[v].grouptime = tt.binding_time;
            }
            
        	tkt[v].answerarr.push(tt);
            
        }

        for(var key in tkt)
        {
            var o = tkt[key];
            restkt.push(o);
        }

        console.log("------------");
        console.log(restkt);
        console.log("------------");

        $scope.objs = restkt;

      });
    }
    getlist(2);
    $scope.change = function(category){
      console.log(category);
      getlist(category);
    }
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

};