module.exports = function($scope, $state,shakedevicelist, shakedevicedel, userinfo, $resource, date2str, toaster){

	
    $scope.searchform = {};
    $scope.searchform.binding_type = '';

    $scope.usedate = '0';
    $scope.company_code = '';

    $scope.vm = {
      'date' : '',
      'options' : {
          format: "YYYY-MM-DD",
          locale : 'zh-cn',
          showClear: true                        
          // clearBtn:true
      }
    }

	//用户信息
    userinfo().then(function(res) {
        $scope.company_code = res.company_code;
    });



    $scope.maxSize = 5;            //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10;         //每页显示几条

    $scope.load = function () {
      if ( $scope.vm.date._d === null) {
        $scope.vm.date._d = '';
      }
      if (typeof $scope.vm.date._d === 'string') {
          $scope.vm.date._d = $scope.vm.date._d;
      } else {
          $scope.vm.date._d = date2str($scope.vm.date._d);
      }
    	var para = {
            pageNo:$scope.bigCurrentPage, 
            pageSize:$scope.itemsPerPage
        };

    	if($scope.usedate == '1')
        {
            $scope.searchform.binding_time = ($scope.vm.date._d);
        }else{
        	$scope.searchform.binding_time = '';
        }

        para = angular.extend($scope.searchform, para);
        console.log(para);
        $resource('/api/as/gc/shakedevice/findlist', {}, {}).
        save(para,function (res) {
            if (res.errcode != 0) {
                toaster.error({title:"",body:res.errmsg});
                return;
            }
            console.log(res);
            $scope.objs = res.data.results;
            $scope.bigTotalItems = res.data.totalRecord;
            
            
        });
        
    }
    $scope.load();

    $scope.create = function()
    {
        $state.go('app.shakedevicecreate');
    }

    $scope.edit = function(id)
    {
        $state.go('app.shakedevicecreate', {'id' : id});
    }

    $scope.del = function(id)
    {
        if(!confirm("确定要删除数据吗？")){
            return;
        }
        $resource('/api/as/gc/shakedevice/del', {}, {}).
        save({'id' : id}, function(res){
            console.log(res);
            if(res.errcode !== 0)
            {
                alert(res.errmsg);
                return;
            }
            alert('删除成功');
            $scope.load();
        });
    }
};