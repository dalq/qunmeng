module.exports = function($scope, $state, $stateParams,$resource,$modal,toaster){
  $scope.searchform = {};
  $scope.getlist = function () {
      $resource('/api/as/gc/shakeevaluatequestion/list', {}, {}).
      save($scope.searchform,function (res) {
          if (res.errcode != 0) {
              toaster.success({title:"",body:res.errmsg});
              return;
          }
          console.log(res);
          $scope.objs = res.data;
          
      });   
  };
  $scope.getlist();

  $scope.create = function(id)
  {
      var modalInstance = $modal.open({
        template: require('../views/shakequestioninfo.html'),
        controller: 'shakequestioninfo',
        size: 'lg',
        resolve: {
          id : function(){
            return id;
          },
        }
    });
    modalInstance.result.then(function (showResult) {	
        $scope.getlist();
    });
  }

    $scope.edit = function(id){
        var modalInstance = $modal.open({
          template: require('../views/shakequestioninfo.html'),
          controller: 'shakequestioninfo',
          size: 'lg',
          resolve: {
              id: function () {
                  return id;
              },
          }
        });
        modalInstance.result.then(function (showResult) {	
            $scope.getlist();
        });
    }

  $scope.del = function(id)
  {
      if(!confirm("确定要删除数据吗？")){
          return;
      }
      $resource('/api/as/gc/shakeevaluatequestion/del', {}, {}).
      save({'id' : id},function (res) {
          if (res.errcode != 0) {
              toaster.error({title:"",body:res.errmsg});
              return;
          }
          toaster.success({title:"",body:"删除成功"});
          $scope.getlist();
          
      }); 
  }
};