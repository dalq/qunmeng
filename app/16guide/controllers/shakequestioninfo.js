module.exports = function($scope, $state, $stateParams, $modalInstance,$resource,$modal,FileUploader,toaster,id){
    //问题id
    $scope.obj = {
      'category' : '',
      'sort' : ''
    }
        if(id === undefined)   //新建
        {
            $scope.obj.category = '1';
            $scope.obj.sort = 0;
        }
        else    
        {
            angular.extend($scope.obj, {'id' : id});
            $resource('/api/as/gc/shakeevaluatequestion/sel', {}, {}).
            save({'id' : id},function (res) {
                if (res.errcode != 0) {
                    toaster.error({title:"",body:res.errmsg});
                    return;
                }
                console.log(res);
                $scope.obj = res.data;
                
            });   
        }
    
    
        $scope.gogo = function(){
            console.log($scope.obj);
            if($scope.obj.question === undefined)
            {
                alert('问题必填');
                return;
            }
            $resource('/api/as/gc/shakeevaluatequestion/findquerybydevicelist', {}, {}).
            save($scope.obj, function (res) {
                if (res.errcode != 0) {
                    toaster.error({title:"",body:res.errmsg});
                    return;
                }
                $modalInstance.close()
            });
    
        };
        $scope.cancel = function(){
          $modalInstance.close();
        }

}  