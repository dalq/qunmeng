module.exports = function($scope, formconfig, viewmodel){

	formconfig.start({
		'title' : '问题详情',
		'formtitle' : '问题基本信息',
		'elements' : viewmodel(),
		'save' : {
			'url' : '/api/as/gc/shakeevaluatequestion/save',
			'to' : 'app.shakequestionlist',
			'before' : function(form){
				//console.log(form);return;	
				form.result['binding_code'] = form.result['binding_code_new'] + '_' + form.result['binding_time'];
			}
		},
	}, $scope);

	$scope.form = formconfig;

};