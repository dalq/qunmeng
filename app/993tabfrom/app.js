/**
 * 子模块入口
 * DHpai
 */

var App = angular.module('tabfrom', []);


App.factory('tabfromservice', require('./service'));


module.exports = App;