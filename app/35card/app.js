var App = angular.module('card', []);

App.config(require('./router'));
App.factory('cardservice', require('./service'));

//添加卡池
App.controller('insertcard',require('./controllers/insertcard'));
//卡基本信息列表
App.controller('basecardlist',require('./controllers/basecardlist'));
// 设置批次号
App.controller('batchnumber',require('./controllers/batchnumber'));
//卡池列表
App.controller('cardpoollist',require('./controllers/cardpoollist'));
// 添加卡
App.controller('addcard',require('./controllers/addcard'));
//添加卡池信息、修改卡池信息
App.controller('addcardpool',require('./controllers/addcardpool'));

//卡池详情
App.controller('releasecard',require('./controllers/releasecard'));

// 释放卡
App.controller('relief',require('./controllers/relief'));
//卡产品列表
App.controller('cardproductlist',require('./controllers/cardproductlist'));
// 下架弹出模态框
App.controller('offsale',require('./controllers/offsale'));
//添加，修改卡产品
App.controller('cardproduct',require('./controllers/cardproduct'));
// 查看卡产品信息
App.controller('cardproductinfo',require('./controllers/cardproductinfo'));
//激活卡
App.controller('activationcard',require('./controllers/activationcard'));
// 记录发卡信息
App.controller('issuecard',require('./controllers/issuecard'));
//卡订单列表
App.controller('cardorderlist',require('./controllers/cardorderlist'));
//卡订单详情
App.controller('cardorderinfo',require('./controllers/cardorderinfo'));
//票信息
App.controller('ticketinfo',require('./controllers/ticketinfo'));
//卡信息
App.controller('cardinfo',require('./controllers/cardinfo'));
// 拿卡人管理
App.controller('takecard',require('./controllers/takecard'));
// 修改领卡人信息
App.controller('changecarduser',require('./controllers/changecarduser'));

// 修改卡信息
App.controller('resivecardinfo',require('./controllers/resivecardinfo'));
// 发卡统计
App.controller('cardreleaselist',require('./controllers/cardreleaselist'));
//景区激活卡
App.controller('jqactivecard',require('./controllers/jqactivecard'));
//景区激活卡订单
App.controller('jqactivecardorder',require('./controllers/jqactivecardorder'));
//换卡
App.controller('changecard',require('./controllers/changecard'));
App.controller('jqticketlist',require('./controllers/jqticketlist'));
module.exports = App;