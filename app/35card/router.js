

var router = function($urlRouterProvider, $stateProvider){

 	$stateProvider

       //卡基本信息
       .state('app.basecardlist', {
         url: '/basecardlist/',
          views: {
            'main@' : {
                controller : 'basecardlist',
                template: require('./views/basecardlist.html'),
              }
         },
         resolve:{
         	insertCard : function(cardservice){
                 return cardservice.insertCard();
            },
            exExcel : function(cardservice){
                 return cardservice.exExcel();
            },
            cardbaselist : function(cardservice){
                 return cardservice.cardbaselist();
            },
            cardproductlist:function(cardservice){
                 return cardservice.cardproductlist();
            },
            searchcard : function(cardservice){
                 return cardservice.searchcard();
             }    
         }
       })

       //设置批次号 
       .state('app.batchnumber', {
         url: '/batchnumber/:mincard/:maxcard',
           views: {
            'main@' : {
                    controller : 'batchnumber',
                    template: require('./views/batchnumber.html'),
            }
        },
         resolve:{
            // 释放卡（置为未发放）
            batchnumber : function(cardservice){
                 return cardservice.batchnumber();
             }
         }
       })

        // 卡池列表
 	   .state('app.cardpool', {
         url: '/cardpoollist',
         views: {
            'main@' : {
                controller : 'cardpoollist',
                template: require('./views/cardpoollist.html'),
            }
         },
         resolve:{
            cardpoollist : function(cardservice){
                 return cardservice.cardpoollist();
            },
            insertActiveCard : function(cardservice){
                 return cardservice.insertActiveCard();
            },
            getcardno : function(cardservice){
                 return cardservice.getcardno();
            },
            addcard : function(cardservice){
                 return cardservice.addcard();
            },
            unusedcard : function(cardservice){
                 return cardservice.unusedcard();
            },
            releasecard : function(cardservice){
                 return cardservice.releasecard();
            },
            canrelease : function(cardservice){
                 return cardservice.canrelease();
            },
            addcardpool : function(cardservice){
                 return cardservice.addcardpool();
            },
            dictbytypelist : function(productservice){
                return productservice.dictbytypelist;
            }
         }
       })

         // (卡池详情)
       .state('app.releasecard', {
         url: '/releasecard/:poolcode',
         views: {
            'main@' : {
                    controller : 'releasecard',
                    template: require('./views/releasecard.html'),
            }
         },
         resolve:{
            /*releasecard : function(cardservice){
                 return cardservice.releasecard();
            },*/
            getcardlist : function(cardservice){
                 return cardservice.getcardlist();
            },
            cardinpool : function(cardservice){
                 return cardservice.cardinpool();
            },
            listinpool : function(cardservice){
                 return cardservice.listinpool();
            },
           
            statename : function(cardservice){
                 return cardservice.statename;
            },
            targetcard : function(cardservice){
                 return cardservice.targetcard();
            },
            operationrecordlist : function(cardservice){
                 return cardservice.operationrecordlist();
            }
            
         }
       })
        //卡产品管理
       .state('app.cardproductlist', {
         url: '/cardproductlist',
         views:{
             'main@':{
                controller : 'cardproductlist',
                template: require('./views/cardproductlist.html'),
             }
         },
       
         resolve:{
            cardproductlist : function(cardservice){
                 return cardservice.cardproductlist();
            },
            onsale : function(cardservice){
                 return cardservice.onsale();
            },
            goodoffsale : function(cardservice){
                 return cardservice.goodoffsale();
            },
            dictbytypelist : function(productservice){
                return productservice.dictbytypelist;
            }
         }
       })
          //卡产品信息
       .state('app.cardproduct', {
         url: '/cardproduct/:code/:editstate',
           views:{
             'main@':{
                    controller : 'cardproduct',
                    template: require('./views/cardproduct.html'),
             }
           },
         resolve:{
            cardproduct : function(cardservice){
                 return cardservice.cardproduct();
            },
            cardproductinfo : function(cardservice){
                 return cardservice.cardproductinfo();
            },
            dictbytypelist : function(productservice){
                return productservice.dictbytypelist;
            },
            cardresources : function(cardservice){
                return cardservice.cardresources();
            },
            cardresourcesinsert : function(cardservice){
                return cardservice.cardresourcesinsert();
            },
            cardresourcesdel : function(cardservice){
                return cardservice.cardresourcesdel();
            },
            cardpoollists : function(cardservice){
                return cardservice.cardpoollists();
            },
            cardpoollist : function(cardservice){
                return cardservice.cardpoollist();
            },
            cardproduct_cardpoollist : function(cardservice){
                return cardservice.cardproduct_cardpoollist();
            },
            cardproductpoolinsert : function(cardservice){
                return cardservice.cardproductpoolinsert();
            },
            cardproductpooldel : function(cardservice){
                return cardservice.cardproductpooldel();
            },
            cardproduct_ticketlist : function(cardservice){
                return cardservice.cardproduct_ticketlist();
            },
            cardproductticketinsert : function(cardservice){
                return cardservice.cardproductticketinsert();
            },
            cardproductticketdel : function(cardservice){
                return cardservice.cardproductticketdel();
            },
            saleticketinfo : function(cardservice){
                return cardservice.saleticketinfo();
            },
            getRedPacketProductlist : function(cardservice){
                return cardservice.getRedPacketProductlist();
            },
            findCouProductDsNoPageList : function(cardservice){
                return cardservice.findCouProductDsNoPageList();
            }
         }
       })

        // 查看卡产品信息
        .state('app.cardproductinfo', {
	         url: '/cardproductinfo/:id',
             views:{
              'main@':{
                    controller : 'cardproductinfo',
                    template: require('./views/cardproductinfo.html'),
               }
             },
	         resolve:{
	           cardproductinfo : function(cardservice){
	                 return cardservice.cardproductinfo();
	           },
	           cardresources : function(cardservice){
	                 return cardservice.cardresources();
	           },
	           cardproduct_cardpoollist : function(cardservice){
	                 return cardservice.cardproduct_cardpoollist();
	           },
	           cardproduct_ticketlist : function(cardservice){
	                 return cardservice.cardproduct_ticketlist();
	           },
		       saleticketinfo : function(cardservice){
		           return cardservice.saleticketinfo();
		       },
		       dictbytypelist : function(productservice){
	                return productservice.dictbytypelist;
	            },
	            getRedPacketProductlist : function(cardservice){
	                return cardservice.getRedPacketProductlist();
	            },
	            findCouProductDsNoPageList : function(cardservice){
	                return cardservice.findCouProductDsNoPageList();
	            }

	         }
       })

         //激活卡
       .state('app.activationcard', {
         url: '/activationcard',
          views:{
              'main@':{
                    controller : 'activationcard',
                    template: require('./views/activationcard.html'),
              }
          },
         resolve:{
            getUserInfoByMobile : function(cardservice){
                 return cardservice.getUserInfoByMobile();
            },
            getProductByCardNoList : function(cardservice){
                 return cardservice.getProductByCardNoList();
            },
            createProductOrderByCardNo : function(cardservice){
                 return cardservice.createProductOrderByCardNo();
            }
          }   
        })

        // 记录发卡信息
       .state('app.issuecard', {
         url: '/issuecard',
          views:{
              'main@':{
                    controller : 'issuecard',
                    template: require('./views/issuecard.html'),
              }
          },
         resolve:{
            issuecard : function(cardservice){
                 return cardservice.issuecard();
            },
            takecardlist : function(cardservice){
                 return cardservice.takecardlist();
            },
            takecardlists : function(cardservice){
                 return cardservice.takecardlists();
            }

             
            
         }
       })

       //卡订单列表
       .state('app.cardorderlist', {
         url: '/cardorderlist',
         views:{
              'main@':{
                    controller : 'cardorderlist',
                    template: require('./views/cardorderlist.html'),
              }
         },
         resolve:{
            cardproductorderlist : function(cardservice){
                 return cardservice.cardproductorderlist();
            },
            ticketinfo : function(cardservice){
                 return cardservice.ticketinfo();
            },
            cardinfo : function(cardservice){
                 return cardservice.cardinfo();
            },
            getDate : function(utilservice){
                return utilservice.getDate;
            }

         }
       })

       //拿卡人管理
        .state('app.takecard', {
         url: '/takecard',
           views:{
              'main@':{
                    controller : 'takecard',
                    template: require('./views/takecard.html'),
              }
           },
         resolve:{
           takecardlist : function(cardservice){
                 return cardservice.takecardlist();
           },
           savetakecarduser : function(cardservice){
                 return cardservice.savetakecarduser();
           },
           deletetakecarduser : function(cardservice){
                 return cardservice.deletetakecarduser();
           },

         }
       })

         // 领卡人修改
        .state('app.changecarduser', {
         url: '/changecarduser/:id/:name/:travelagency/:mobile/:remarks',
           views:{
              'main@':{
                    controller : 'changecarduser',
                    template: require('./views/changecarduser.html'),
              }
           },
         resolve:{
           savetakecarduser : function(cardservice){
                 return cardservice.savetakecarduser();
           }

         }
       })

       // 修改卡信息
       .state('app.resivecardinfo', {
         url: '/resivecardinfo/:poolcode',
          views:{
              'main@':{
                        controller : 'resivecardinfo',
                        template: require('./views/resivecardinfo.html'),
              }
          },
         resolve:{
            used : function(cardservice){
                 return cardservice.used();
            },
            lost : function(cardservice){
                 return cardservice.lost();
            },
            cardnumuser : function(cardservice){
                 return cardservice.cardnumuser();
            }

         }
       })

       
        //发卡统计
       .state('app.cardstatistics', {
         url: '/cardreleaselist',
          views:{
              'main@':{
                    controller : 'cardreleaselist',
                    template: require('./views/cardreleaselist.html'),
              }
          },
         resolve:{
            cardreleaselist : function(cardservice){
                 return cardservice.cardreleaselist();
            },
            getDate : function(utilservice){
                return utilservice.getDate;
            }

         }
       })

       //景区激活卡
       .state('app.jqactivecard', {
         url: '/jqactivecard',
          views:{
              'main@':{
                    controller : 'jqactivecard',
                    template: require('./views/jqactivecard.html'),
              }
          },
         resolve:{

          }   
        })

       //景区激活卡订单
       .state('app.jqactivecardorder', {
         url: '/jqactivecardorder',
          views:{
              'main@':{
                    controller : 'jqactivecardorder',
                    template: require('./views/jqactivecardorder.html'),
              }
          },
         resolve:{
         	list: function (orderservice) {
                    return orderservice.list();
                },
                getDate: function (utilservice) {
                    return utilservice.getDate;
                },
                ticketlist: function (orderservice) {
                    return orderservice.ticketlist();
                },
                createBackOrder: function (orderservice) {
                    return orderservice.createBackOrder();
                },
                resend: function (orderservice) {
                    return orderservice.resend();
                },
                getRedCorridorOrderList: function (orderservice) {
                    return orderservice.getRedCorridorOrderList();
                },
                getRedCorridorResentMsg: function (orderservice) {
                    return orderservice.getRedCorridorResentMsg();
                },
                orderbacklist: function (orderservice) {
                    return orderservice.orderbacklist();
                },
                relay: function (orderservice) {
                    return orderservice.relay();
                },
                getRedCorridorTrSendSms: function (orderservice) {
                    return orderservice.getRedCorridorTrSendSms();
                },
                getOrderSimInfo: function (orderservice) {
                    return orderservice.getOrderSimInfo();
                },
                agencyOrderRepeatECode: function (orderservice) {
                    return orderservice.agencyOrderRepeatECode();
                },
                updateTicketEffectTime: function (orderservice) {
                    return orderservice.updateTicketEffectTime();
                },
                getroyalocOrdersState: function (orderservice) {
                    return orderservice.getroyalocOrdersState();
                },
                testCreateBackOrder: function (orderservice) {
                    return orderservice.testCreateBackOrder();
                },
                str2date: function (utilservice) {
                    return utilservice.str2date;
                },
                categorylist: function (orderservice) {
                    return orderservice.categorylist();
                },
                date2str: function (utilservice) {
                    return utilservice.date2str;
                }
          }   
        })

  

};

module.exports = router;