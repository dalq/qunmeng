module.exports = function($scope,$resource){

	$scope.obj = {};
	$scope.obj.num = 0;

	$resource('/api/as/tc/sale/jqsaleNameBySubsidyList', {}, {}).save({'sale_category':'J11'}, function(res){
    	console.log(res);
    	if(res.errcode !== 0)
        {
    		alert(res.errmsg);
        	
        }
        $scope.levelarr = res.data;
    });

	$scope.active = function(){

		if($scope.obj.username == '') { 
        	alert('请输入姓名');
        	return;
        }
        if($scope.obj.mobile == '') { 
        	alert('请输入电话');
        	return;
        }
        if($scope.obj.papersno == '') { 
        	alert('请输入身份证号');
        	return;
        }
        if($scope.obj.cardnum == '') { 
        	alert('请输入卡号');
        	return;
        }
        if($scope.obj.num == '' || $scope.obj.num == 0) { 
        	alert('请选择购买数量');
        	return;
        }
		//验证手机号
		if(!(/^1[34578]\d{9}$/.test($scope.obj.mobile))){ 
	        alert("手机号码有误，请重填");  
	        return; 
   		};

   		$resource('/api/ac/cdc/userActiveService/createActiveCard', {}, {}).save($scope.obj, function(res){
    		if (res.errcode === 0) {
    			alert("激活成功");
				$scope.obj = {};
				$scope.obj.num == 0;
			}else {
				alert(res.errmsg);
				return;
			}
    	})

	};

	$scope.jian = function () {
		if (isNaN($scope.obj.num)) {
			$scope.obj.num = 0;
			return;
		}
		$scope.obj.num -= 1;
		if ($scope.obj.num - 1 < 0) {
			$scope.obj.num = 0;
		}
	};

	$scope.jia = function () {
		if (isNaN($scope.obj.num)) {
			$scope.obj.num = 0;
			return;
		}
		$scope.obj.num += 1;
	};
	
};