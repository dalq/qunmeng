module.exports = function($scope, $modal, ticketinfo, cardinfo, cardproductorderlist, 
	 getDate, $state, $stateParams){


    var ordertype_arr=[
        {
        'label':'查询全部',
        'value':'',
       },
       {
        'label':'激活',
        'value':'1',
       }, 
       {
        'label':'购买',
        'value':'2',
       },

   ];
   $scope.ordertype=ordertype_arr;

    var pay_arr=[
        {
        'label':'查询全部',
        'value':'',
       },
       {
        'label':'未支付',
        'value':'0',
       }, 
       {
        'label':'已支付',
        'value':'1',
       },
 
   ];
   $scope.paystate=pay_arr;

    $scope.searchform = {};
  
      $scope.date = {
                'lable': date2str2(new Date()),
                
                'opened': false
            }


             $scope.dateOpen = function ($event, item) {
                $event.preventDefault();
                $event.stopPropagation();
                item.opened = true;
            };

            $scope.date1 = {
                 'lable': date2str2(new Date()),
                //'value': date2str(new Date()),
                'opened': false
            }


             $scope.dateOpen1 = function ($event, item) {
                $event.preventDefault();
                $event.stopPropagation();
                item.opened = true;
            };
             function date2str2(d) {
                if (d === undefined) {
                    return "";
                }
                var month = (d.getMonth() + 1).toString();
                var day = d.getDate().toString();
                if (month.length == 1) month = '0' + month;
                if (day.length == 1) day = '0' + day;
                return d.getFullYear() + "-" + month + "-" + day;
            }


    /* 分页
     * ========================================= */
    $scope.maxSize = 5;            //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10;         //每页显示几条

    $scope.load = function () {

               if($scope.date.lable){
                    if(typeof $scope.date.lable ==='string'){
                         $scope.tour_date=$scope.date.lable;
                    }else{
                        $scope.tour_date=date2str2($scope.date.lable);
                    }
                   
                }else{
                    $scope.tour_date=''
                }
                if($scope.date1.lable){
                     if(typeof $scope.date1.lable ==='string'){
                         $scope.tour_date_two=$scope.date1.lable;
                    }else{
                        $scope.tour_date_two=date2str2($scope.date1.lable);
                    }
                  
                }else{
                    $scope.tour_date_two=''
                }
                if($scope.tour_date === '' &&  $scope.tour_date_two === ''){
                     var para = {
                        pageNo:$scope.bigCurrentPage, 
                        pageSize:$scope.itemsPerPage,
                        start_time : '',
                        end_time : ''
                    };
                }
                 if($scope.tour_date !== '' &&  $scope.tour_date_two === ''){
                            var para = {
                                pageNo:$scope.bigCurrentPage, 
                                pageSize:$scope.itemsPerPage,
                                start_time : $scope.tour_date + " 00:00:00",
                                end_time : ''
                            };
                 }

                    if($scope.tour_date === '' &&  $scope.tour_date_two !== ''){
                        var para = {
                            pageNo:$scope.bigCurrentPage, 
                            pageSize:$scope.itemsPerPage,
                            start_time : '',
                            end_time :  $scope.tour_date_two + " 23:59:59"
                        };
                    }
                    
                    if($scope.tour_date !== '' &&  $scope.tour_date_two !== ''){
                        var para = {
                            pageNo:$scope.bigCurrentPage, 
                            pageSize:$scope.itemsPerPage,
                            start_time :$scope.tour_date + " 00:00:00",
                            end_time :  $scope.tour_date_two + " 23:59:59"
                        };
                    }
        
      
        para = angular.extend($scope.searchform, para);

        console.log(para);
        
        cardproductorderlist.save(para, function(res){

            console.log(res);

            if(res.errcode === 0)
            {
                $scope.objs = res.data.results;
                $scope.bigTotalItems = res.data.totalRecord;
            }
            else
            {
                alert(res.errmsg);
            }

        });

    };
    $scope.load();

    $scope.orderinfo = function(obj){

	    var modalInstance = $modal.open({
	      template: require('../views/cardorderinfo.html'),
	      controller: 'cardorderinfo',
	      size: 'lg',
	      resolve: {
	        obj : function(){
	            return obj;
	        }
	      }
		});
	}

    $scope.ticketinfo = function(code){

        var modalInstance = $modal.open({
          template: require('../views/ticketinfo.html'),
          controller: 'ticketinfo',
          size: 'lg',
          resolve: {
            code : function(){
                return code;
            },
            ticketinfo : function(){
                return ticketinfo;
            }
          }
        });

        modalInstance.result.then(function () {
          //load();
        }, function () {
          //$log.info('Modal dismissed at: ' + new Date());
        });

    };

    $scope.cardinfo = function(code){

        var modalInstance = $modal.open({
          template: require('../views/cardinfo.html'),
          controller: 'cardinfo',
          size: 'lg',
          resolve: {
            code : function(){
                return code;
            },
            cardinfo : function(){
                return cardinfo;
            }
          }
        });

        modalInstance.result.then(function () {
          //load();
        }, function () {
          //$log.info('Modal dismissed at: ' + new Date());
        });

    };

};