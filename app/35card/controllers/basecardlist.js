module.exports = function($scope, $state, cardbaselist, cardproductlist, insertCard, $modal, exExcel){

	$scope.searchform = {
		startcard : '',
		endcard : '',
		cardbatch : '',
		company_code : '',
		update_by : ''
	};

    // 设置批次号
    $scope.batchnumber = function(mincard,maxcard){ 
    	$state.go('app.batchnumber',{'mincard' : mincard, 'maxcard' : maxcard});
    };
    
    // 制卡完成
    $scope.cardcomplete = function(cardmakebatch,mincard,maxcard){ 
    	$state.go('app.cardcomplete', {'cardmakebatch' : cardmakebatch, 'mincard' : mincard, 'maxcard' : maxcard});
    	
    };
    // 创建卡产品
    $scope.create = function(){
		$state.go('app.cardproduct');
	};

    $scope.exExcel = function(obj){
    	exExcel.save(obj, function(res){
			if(res.errcode !== 0)
			{
				alert(res.errmsg);
				return;
			}else{
				alert("导出成功,请查看：" + res.data);
			}
        })
    }
	
    
	$scope.search = function(card_status){
		if(card_status == undefined || card_status == ''){
			card_status = '0';
		}
		if (($scope.searchform.startcard !== '' && $scope.searchform.endcard === '') || ($scope.searchform.startcard === '' && $scope.searchform.endcard !== '') ) { 
			alert('卡号输入不完全');			
		} else if ($scope.searchform.endcard < $scope.searchform.startcard) { 
			alert('结束卡号不能小于起始卡号');
		}else { 
			$scope.searchform.card_status = card_status;
		    cardbaselist.save($scope.searchform, function(res){
		    	console.log($scope.searchform);
				console.log(res.data);
				if(res.errcode !== 0)
				{
					alert(res.errmsg);
					return;
				}
				$scope.objs = res.data;
	        })
	    }
			
	};
    $scope.search();

    $scope.add = function(obj){
        
    	var modalInstance = $modal.open({
          template: require('../views/insertcard.html'),
          controller: 'insertcard',
          size: 'xs',
          resolve: {
            insertCard : function(){
                return insertCard;
            }
          }
        });

        modalInstance.result.then(function () {
            $scope.search();
        }, function () {
            
        });
    }

};