module.exports = function($scope, $resource, $modalInstance, obj){

	$scope.obj = obj;


	$scope.cancel = function(){

		$modalInstance.close();

	}

	$scope.gogo = function(){

		if($scope.obj.xin_smart_card_no === undefined || $scope.obj.xin_smart_card_no == '')
		{
			alert('请输入新卡号');
			return;
		}

		 $resource('/api/ac/cdc/userActiveService/updatecardno', {}, {}).save($scope.obj, function (res) {

			if(res.errcode === 0)
			{
				alert('修改成功');
				$modalInstance.close();
			}
			else
			{
				alert(res.errmsg);
			}

		});

	}



};