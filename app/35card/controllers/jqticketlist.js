module.exports = function($scope,  ticketlist, createBackOrder, obj,$modalInstance,
    getRedCorridorOrderList, getDate, $modal, orderbacklist, getOrderSimInfo, updateTicketEffectTime, str2date,$resource,
    //皇家海洋馆订单状态详情
    getroyalocOrdersState){
  
    function date2str2(d) {
        if (d === undefined) {
            return "";
        }
        var month = (d.getMonth() + 1).toString();
        var day = d.getDate().toString();
        if (month.length == 1) month = '0' + month;
        if (day.length == 1) day = '0' + day;
        return d.getFullYear() + "-" + month + "-" + day;
    }
    function str2date2 (strDate){

        if(angular.isString(strDate))
        {
            var objDate = new Date(Date.parse(strDate.replace(/-/g, "/")));

            return objDate;
        }
        else
        {
            return '错误格式';
        }
        
    }

    $scope.authority = true;

    $scope.load = function () {
        ticketlist.get({'order_code' : obj.order_code}, function(res){

            for (var i = res.data.length - 1; i >= 0; i--) {
                res.data[i].take_effect_time = str2date2(res.data[i].take_effect_time); 
                res.data[i].take_effect_time = date2str2(res.data[i].take_effect_time); 
            }

            // obj = res;
            // $scope.newdate = str2date(res.ticket_out_time);

            /* 门票存储结构
             * ========================================= */
            var tkt = new Object();
            var restkt = new Array();


            if(res.errcode !== 0)
            {
                alert("数据获取失败");
                return;
            }

            var arr = res.data;


            //用景区编号作为存储结构的属性，值是数组
            for(var i = 0, j = arr.length; i < j; i++)
            {
                var tt = arr[i];
                var v = tt.sequence;

                if(!tkt.hasOwnProperty(v))
                {
                    tkt[v] = new Object();
                    tkt[v].ticketarr = new Array();
                    tkt[v].sequence = tt.sequence;
                    tkt[v].name = tt.order_name;
                    tkt[v].newdate = tt.take_effect_time;
                    tkt[v].destory_view = tt.destory_view;
                }
                tkt[v].ticketarr.push(tt);
            }

            for(var key in tkt)
            {
                var o = tkt[key];
                restkt.push(o);
            }

            // for (var i = restkt.length - 1; i >= 0; i--) {
            //     restkt[i]
            // }
            var viewMap = {};
            $resource('/api/as/tc/placeview/jlist', {}, {})
				.save({}, function (res) {
                    res.data.forEach(function(element) {
                        viewMap[element.code] = element.name;
                    }, this);
                    for (var index = 0; index < restkt.length; index++) {
                        restkt[index].destory_view = viewMap[restkt[index].destory_view] || '无';
                        
                    }
                    $scope.objs = restkt;
				});
        });

    };
    $scope.load();
    


    /*$scope.back = function(obj1){

        if(obj.sale_belong === 'juyou' || obj.sale_belong.indexOf('supply_piaofutong') == 0 ||  obj.sale_belong.indexOf('supply_tstc') == 0 ||  obj.sale_belong === 'supply_tongchenglvyou' || obj.sale_belong === 'supply_zhiyoubao' || obj.sale_belong === 'supply_xiaojing' || obj.sale_belong === 'supply_ziwoyou')
        {
            juyouback(obj1);
        }
        else
        {
            getbacknum(obj1);
        }
    };


    $scope.cancel = function () {
       $modalInstance.dismiss('cancel');
    };



    function change(obj)
    {
        var arr = [];

        var msg = ' (总人数' + obj.inCount + 
                  ', 已使用人数' + obj.usedCount +
                  ', 退票人数' + obj.backCount + ') ';


        var newobj = {
            'back1' : obj.backCount,
            'code' : obj.credence,
            'goods_code' : obj.goodsId,
            //'id':
            'order_code' : obj.orderId,
            'order_name' : obj.goodsName  + msg,
            //'otime' : otime,
            // 'place_code' :
            'place_name' : viewname,
            'sequence' : 1,
            'newdate' : obj.take_effect_time,
            //'state' : '1',
            // 'type' :
            // 'type_attr' :
            'type_name' : obj.goodsName,
            'used1' : obj.usedCount,
            'inCount' : obj.inCount 
        };

        arr.push(newobj);
        return arr;
    }


    function change_huaxiapiaolian(obj){

        var arr = [];

        var msg = ' (总人数' + obj.firstNum + 
                  ', 已使用人数' + obj.usedNum +
                  ', 退票人数' + obj.cancelNum + ') ';


        var newobj = {
            'back1' : obj.cancelNum,
            'code' : obj.ecode,
            'goods_code' : obj.productCode,
            //'id':
            'order_code' : obj.platOrderNo,
            'order_name' : obj.productName  + msg,
            //'otime' : otime,
            // 'place_code' :
            'place_name' : viewname,
            'sequence' : 1,
            //'state' : '1',
            // 'type' :
            // 'type_attr' :
            'type_name' : obj.productName,
            'used1' : obj.usedNum,
            'inCount' : obj.firstNum 
        };

        arr.push(newobj);
        return arr;

    }

    function change_royaloc(obj)
    {

        var usedNum = 0;
        var backNum = 0;
        var buyNum = 1;
        

        //0  未消费
        if(obj.state == 0)
        {
            usedNum = 0;
            backNum = 0;
        }
        //1  已消费
        else if(obj.state == 1)
        {
            usedNum = 1;
            backNum = 0;
        }
        //2  已作废,退票
        else if(obj.state == 2)
        {
            usedNum = 0;
            backNum = 1;
        }
        //3  订单错误
        else if(obj.state == 3)
        {
            return [];
        }


        var arr = [];

        var msg = ' (总人数' + 1 + 
                  ', 已使用人数' + usedNum +
                  ', 退票人数' + backNum + ') ';


        var newobj = {
            'back1' : backNum,
            'code' : '',
            'goods_code' : '',
            //'id':
            'order_code' : '',
            'order_name' : '皇家极地海洋馆门票' + msg,
            //'otime' : otime,
            // 'place_code' :
            'place_name' : viewname,
            'sequence' : 1,
            //'state' : '1',
            // 'type' :
            // 'type_attr' :
            'type_name' : '皇家极地海洋馆门票',
            'used1' : usedNum,
            'inCount' : 1 
        };

        arr.push(newobj);
        return arr;
    }

    //居游退票
    function juyouback(obj)
    {
        var flag = true;
        for(var i = 0; i < obj.ticketarr.length; i++)
        {
            var tmp = obj.ticketarr[i];
            if(tmp.state !== '1')
            {
                flag = false;
            }
        }

        if(!flag)
        {
            alert('销售品中有已经使用的商品。');
            return;
        }

        var para = {
            'order_code' : code,
            'sequence' : obj.sequence
        };


        if (!confirm("确定要退 " + obj.name + ' 中的第 ' + obj.sequence + ' 个吗？')) {
            return false;
        }

        createBackOrder.save(para, function(res){

            if(res.errcode === 0)
            {
                if(res.data.result == '1'){
					alert('退票成功');
				} else if(res.data.result == '2') {
					alert('退票申请已提交，待审核');
				} else if(res.data.result == '3') {
					alert(res.data.remark_err);
				}
                $scope.load();
            }
            else
            {
                alert(res.errmsg);
            }

        });
    }

    $scope.timechange = function(id,take_effect_time){

        updateTicketEffectTime.save({'id' : id, 'take_effect_time' : take_effect_time} ,function(res){

            if(res.errcode === 0)
            {
                alert('修改成功');
                $scope.authority = true;
                // $scope.load();
            }
            else
            {
                alert(res.errmsg);
            }
        });
    }

    // $scope.open = function(obj) {
    //     obj.opened = true;
    // };
     $scope.obj = {
                // 'lable': date2str2(new Date()),
                //'lable': new Date(),
                //'value': date2str(new Date()),
                'opened': false
            }


             $scope.dateOpen = function ($event, item) {
                $event.preventDefault();
                $event.stopPropagation();
                item.opened = true;
            };

    $scope.authchange = function(){
        $scope.authority = false;
    }

    

    $scope.mosaic = function(id,take_effect_time,date){

        $scope.front = getDate(take_effect_time);
        take_effect_time = $scope.front + ' ' + date.substring(11,19);
        $scope.timechange(id,take_effect_time);
    };*/


};