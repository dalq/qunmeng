/**
*卡订单列表
*ml
*/

module.exports = function($scope, cardreleaselist, getDate){

 var type_arr=[
        {
        'label':'查询全部',
        'value':'',
       },
       {
        'label':'拿卡',
        'value':'1',
       }, 
       {
        'label':'退卡',
        'value':'2',
       },

   ];
   $scope.typearr=type_arr;

    var pay_arr=[
        {
        'label':'查询全部',
        'value':'',
       },
       {
        'label':'已结算',
        'value':'0',
       }, 
       {
        'label':'未结算',
        'value':'1',
       },
 
   ];
   $scope.settlementarr=pay_arr;


    $scope.searchform = {};

    $scope.total = {
        'num' : 0,
        'total' : 0
    };

    
     $scope.date = {
                'lable': date2str2(new Date()),
                //'lable': new Date(),
                //'value': date2str(new Date()),
                'opened': false
            }


             $scope.dateOpen = function ($event, item) {
                $event.preventDefault();
                $event.stopPropagation();
                item.opened = true;
            };

            $scope.date1 = {
                 'lable': date2str2(new Date()),
                //'value': date2str(new Date()),
                'opened': false
            }


             $scope.dateOpen1 = function ($event, item) {
                $event.preventDefault();
                $event.stopPropagation();
                item.opened = true;
            };
             function date2str2(d) {
                if (d === undefined) {
                    return "";
                }
                var month = (d.getMonth() + 1).toString();
                var day = d.getDate().toString();
                if (month.length == 1) month = '0' + month;
                if (day.length == 1) day = '0' + day;
                return d.getFullYear() + "-" + month + "-" + day;
            }

    $scope.load = function () {

    	$scope.total = {
	        'num' : 0,
	        'total' : 0
	    };
        
          if($scope.date.lable){
                    if(typeof $scope.date.lable ==='string'){
                         $scope.tour_date=$scope.date.lable;
                    }else{
                        $scope.tour_date=date2str2($scope.date.lable);
                    }
                   
                }else{
                    $scope.tour_date=''
                }
                if($scope.date1.lable){
                     if(typeof $scope.date1.lable ==='string'){
                         $scope.tour_date_two=$scope.date1.lable;
                    }else{
                        $scope.tour_date_two=date2str2($scope.date1.lable);
                    }
                  
                }else{
                    $scope.tour_date_two=''
                }
                if($scope.tour_date === '' &&  $scope.tour_date_two === ''){
                     var para = {
                      
                        start_time : '',
                        end_time : ''
                    };
                }
                 if($scope.tour_date !== '' &&  $scope.tour_date_two === ''){
                            var para = {
                            
                                start_time : $scope.tour_date + " 00:00:00",
                                end_time : ''
                            };
                 }

                    if($scope.tour_date === '' &&  $scope.tour_date_two !== ''){
                        var para = {
                           
                            start_time : '',
                            end_time :  $scope.tour_date_two + " 23:59:59"
                        };
                    }
                    
                    if($scope.tour_date !== '' &&  $scope.tour_date_two !== ''){
                        var para = {
                          
                            start_time :$scope.tour_date + " 00:00:00",
                            end_time :  $scope.tour_date_two + " 23:59:59"
                        };
                    }

        para = angular.extend($scope.searchform, para);

        console.log(para);
        
        cardreleaselist.save(para, function(res){

            console.log(res);

            if(res.errcode === 0)
            {
                $scope.objs = res.data;

                for(var i=0; i<=res.data.length; i++) {
                	var tmp = res.data[i];
                	$scope.total.num += tmp.num;
                	$scope.total.total += tmp.all_price;
                }
                
            }
            else
            {
                alert(res.errmsg);
            }

        });

    };
    $scope.load();

};