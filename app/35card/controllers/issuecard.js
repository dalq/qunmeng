module.exports = function($scope, $state, issuecard, takecardlists,takecardlist, toaster){
  $scope.nameobj = {
      'name': '' 
  };
 // 卡类型
  $scope.cardtypearr = [{'label' : '居游卡', 'value' : '0'},{'label' : '亲子卡', 'value' : '1'},{'label' : '烂遭卡', 'value' : '2'}];
  $scope.takecarduserlist = function(){ 
      takecardlists.save({}, function(res){ 
          if (res.errcode !== 0) { 
              alert(res.errmsg);
              return;
          }
              $scope.objs = res.data;
              $scope.nameobj.name = res.data[0].name; 
              console.log(res);
      });
  };
  $scope.takecarduserlist(); 

  $scope.selection = function(obj){ 
      console.log(obj);
      if (obj === null) { 
          //console.log(obj);
          $scope.userinfo.name = '';
          $scope.userinfo.mobile = '';
      } else {
          $scope.userinfo.mobile = obj.mobile;
          $scope.userinfo.name = obj.name;
          $scope.userinfo.id = obj.id;
      }
  };
  console.log($scope.nameobj.name);
  $scope.userinfo = {
      'id' : '',
      'type' : '1',
      'name' : '',	
      'mobile' : '',
      'company_code' : '',
      'unit_price' : '',
      'all_price' : '',
      'num' : '',
      'remarks' : '',
      'card_giveout_target' : ''
  };
  
  $scope.cardinfo = {
      'start_card_no' : '',
      'end_card_no' : ''
  }
  $scope.cardsegment = [];
  $scope.totalnum = 0;
  var card_num = '';
  $scope.addCardNum = function(item){
      if(item.start_card_no == '' || item.end_card_no == ''){
        toaster.error({title:"",body:"请填写卡段"});				
      }
      var str = item.start_card_no + '-' + item.end_card_no;
      var cardnumber = parseInt(item.end_card_no - item.start_card_no) + 1;
      $scope.totalnum =$scope.totalnum + cardnumber;
      $scope.userinfo.num = $scope.totalnum;
      console.log('卡数量' + cardnumber, 'zong卡数量' +$scope.totalnum);
      $scope.cardsegment.push(str);
      console.log($scope.cardsegment);
      // 卡号段
      $scope.userinfo.card_num = $scope.cardsegment.join(',');
      
  }
  $scope.cardsegmentdel = function(index,cardsegmentlist){
      console.log(cardsegmentlist);
      var strarr = cardsegmentlist.split('-');
      console.log(strarr[1], strarr[0]);
      var cardnumber = strarr[1] - strarr[0] + 1;
      $scope.totalnum =$scope.totalnum - cardnumber;
      $scope.userinfo.num = $scope.totalnum;
      console.log($scope.totalnum);
      $scope.cardsegment.splice(index, 1);
      console.log($scope.cardsegment);
      $scope.userinfo.card_num = $scope.cardsegment.join(',');
  }
  
  $scope.searchinfo = [];

    $scope.saveuserinfo = function(){
        console.log($scope.userinfo);
        $scope.userinfo.all_price = $scope.userinfo.unit_price * $scope.totalnum;
        console.log('总价' + $scope.userinfo.all_price);
        issuecard.save($scope.userinfo, function(res){
            if (res.errcode !== 0) {
                toaster.error({title:"", body:res.errmsg});
            } else {
                toaster.success({title:"", body:"记录成功"});
                
            }
                    
        }); 
    };



};