module.exports = function($scope, $resource, $modal, ticketlist, createBackOrder, 
	getRedCorridorOrderList, orderbacklist, getOrderSimInfo, updateTicketEffectTime, getDate, str2date, getroyalocOrdersState){

    $scope.searchform = {};
    /* 分页
       * ========================================= */
    $scope.maxSize = 5;            //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10;         //每页显示几条

    $scope.load = function () {

      var para = {
        pageNo: $scope.bigCurrentPage,
        pageSize: $scope.itemsPerPage
      };

      angular.extend(para, $scope.searchform);

      $resource('/api/as/tc/smartcard/cardorderlist', {}, {}).save(para, function (res) {
        console.log(para);
        console.log(res);

        if (res.errcode !== 0) {
          alert("数据获取失败");
          return;
        }

        $scope.objs = res.data.results;
        $scope.bigTotalItems = res.data.totalRecord;

      });

    };
    $scope.load();

    $scope.losscard = function(obj){
    	if (confirm("您确认要挂失吗？")) {
	    	$resource('/api/as/tc/smartcard/updatestate', {}, {}).save(obj, function (res) {

			    if(res.errcode === 0) {
			    	alert("挂失成功");
			    	$scope.load();
			    }else{
					alert(res.errmsg);
			    }
			    
			  });
    	}
	}


    //打开模态框
    $scope.changecard = function(obj){
        
    	var modalInstance = $modal.open({
          template: require('../views/changecard.html'),
          controller: 'changecard',
          size: 'xs',
          resolve: {
            obj : function(){
                return obj;
            }
          }
        });

        modalInstance.result.then(function () {
            $scope.load();
        }, function () {
            
        });
    }

    $scope.ticketlist = function(obj){
    	console.log(obj);
        //$state.go('app.orderticketlist', {'code' : code});

        var modalInstance = $modal.open({
          template: require('../views/jqticketlist.html'),
          controller: 'jqticketlist',
          size: 'lg',
          resolve: {
            obj : function(){
                return obj;
            },
            ticketlist : function(){
                return ticketlist;
            },
            createBackOrder : function(){
                return createBackOrder;
            },
            //红海滩
            getRedCorridorOrderList : function(){
                return getRedCorridorOrderList;
            },
            //退票历史
            orderbacklist : function(){
                return orderbacklist;
            },
            //北京票联  红海滩廊道
            getOrderSimInfo : function(){
                return getOrderSimInfo;
            },
            //修改生效时间
            updateTicketEffectTime : function(){
                return updateTicketEffectTime;
            },
            getDate : function(){
                return getDate;
            },
            str2date : function(){
                return str2date;
            },
            getroyalocOrdersState : function(){
                return getroyalocOrdersState;
            }
          }
        });

        modalInstance.result.then(function () {
          //load();
        }, function () {
          //$log.info('Modal dismissed at: ' + new Date());
        });

    };



};