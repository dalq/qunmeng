/**
* 子模块路由
* dlq
*/

var router = function ($urlRouterProvider, $stateProvider) {

    $stateProvider

        //商品列表
        .state('app.goods_list', {
            url: "/goods/list.html",
            views: {
                'main@' : {
                    template : require('../996tpl/views/table.html'),
                    controller : 'goodslist',
                }
            },
            resolve:{
                tableconfig : function(tableservice){
                    return tableservice.tableconfig();
                },
            }
        })
        //创建商品模板
        .state('app.goods_temp_create', {
            url: "/goods/temp_create.html",
            views: {
                'main@' : {
                    template : require('./views/create_tmp.html'),
                    controller : 'goodstempcreate',
                }
            },
        })
        //创建商品
        .state('app.goods_info_create', {
            url: "/goods/info_create/:id/:goodsid",
            views: {
                'main@' : {
                    template : require('./views/create_info.html'),
                    controller : 'goodsinfocreate',
                }
            },
            resolve:{
                formconfig : function(formservice){
                    return formservice.formconfig();
                },
                model : function(goodsservice){
                    return goodsservice.model();
                },
                category : function(goodsservice){
                    return goodsservice.category();
                },
                date2str : function(utilservice){
                    return utilservice.date2str;
                }
            }
        })
        //商品详情
        .state('app.goods_info', {
            url: "/goods/info/:id",
            views: {
                'main@' : {
                    template : require('../996tpl/views/form.html'),
                    controller : 'goodsinfo',
                }
            },
            resolve:{
                formconfig : function(formservice){
                    return formservice.formconfig();
                },
                model : function(goodsservice){
                    return goodsservice.model;
                }
            }
        })

        //商品编辑
        .state('app.goods_edit', {
            url: "/goods/edit/:id",
            views: {
                'main@' : {
                    template : require('../996tpl/views/form.html'),
                    controller : 'goodsedit',
                }
            },
            resolve:{
                formconfig : function(formservice){
                    return formservice.formconfig();
                },
                model : function(goodsservice){
                    return goodsservice.model;
                }
            }
        })


        //测试
        .state('app.goods_info1', {
            url: "/goods/info1.html",
            views: {
                'main@' : {
                    template : require('./views/test.html'),
                    controller : 'goodstest',
                }
            }
        })


        ;

};

module.exports = router;