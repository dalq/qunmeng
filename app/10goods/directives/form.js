module.exports = function($modal, $state){

	return {

		restrict : 'AE',
		template : require('../views/goodsform.html'),
		replace:true,
		scope:{
			'item' : '=',
			'result' : '=',
			'dateshow' : '=',
			'timeshow' : '=',
			'imageshow' : '=',
			'selectobj' : '=',
			'model' : '=',
			'hstep' : '=',
			'mstep' : '=',
			'ismeridian' : '=',
		},
		link : function(scope, elements, attrs){

			
			//---------  设置价格 --------------------//
			function setValue(key, value){

				if(key == 'guide'){
					scope.result['tpsp.period_model_guide_price'] = value;
				}
				if(key == 'rate'){
					scope.result['tpsp.period_model_rate'] = value;
				}
				if(key == 'settle'){
					scope.result['tpsp.period_model_settle_price'] = value;
				}
				if(key == 'ratePrice'){
					scope.result['tpsp.period_model_rate_price'] = value;
				}
			}


			scope.pp = {
				'guide' : false,
				'rate' : false,
				'settle' : false,
				'ratePrice' : false,
			};

			var ss = {
				'guide' : 0,
				'rate' : 0,
				'settle' : 0,
				'ratePrice' : 0,
			};

			var vv = {
				'guide' : 0,
				'rate' : 0,
				'settle' : 0,
				'ratePrice' : 0,
			}

			//有值
			if(scope.result['tpsp.period_model_guide_price'] != 0
			|| scope.result['tpsp.period_model_settle_price'] != 0){
				scope.pp.settle = true;
				scope.pp.ratePrice = true;
			}


			scope.pricechange = function(){

				vv.guide = scope.result['tpsp.period_model_guide_price'];
				vv.rate = scope.result['tpsp.period_model_rate'];
				vv.settle = scope.result['tpsp.period_model_settle_price'];
				vv.ratePrice = scope.result['tpsp.period_model_rate_price'];

				if(vv.guide < 0) scope.result['tpsp.period_model_guide_price'] = vv.guide = 0;
				if(vv.rate < 0) scope.result['tpsp.period_model_rate'] = vv.rate = 0;
				if(vv.rate > 100) scope.result['tpsp.period_model_rate'] = vv.rate = 100;
				if(vv.settle < 0) scope.result['tpsp.period_model_settle_price'] = vv.settle = 0;
				if(vv.ratePrice < 0) scope.result['tpsp.period_model_rate_price'] = vv.ratePrice = 0;

				var count = 0;

				//当文本框有值并且为可录入状态，count加1，当这种情况的数量为2的时候开始计算。
				//1,初始状态，用两个不为零的值进行计算，算出另外两个值，
				//2,计算后的状态，所有值都不为零，但是只有两个值可以录入，这时继续计算，
				//3,只有两个值可以录入，但是其中一个值为0(此时count是1)，这时将不可录入的文本框置成可录入，
				//	并且将他们的值设置成0
				if(vv.guide > 0 && scope.pp.guide == false){
					count++;
					ss.guide = 1;
				}
				if(vv.rate > 0 && scope.pp.rate == false){
					count++;
					ss.rate = 1;
				}
				if(vv.settle > 0 && scope.pp.settle == false){
					count++;
					ss.settle = 1;
				}
				if(vv.ratePrice > 0 && scope.pp.ratePrice == false){
					count++;
					ss.ratePrice = 1;
				}

				//
				if(count < 2){

					//之前数值不为零，因为是计算出来的
					angular.forEach(ss, function(value, key){
						//之前的状态是0，也就是不可以录入
						if(value == 0
							//之前状态是1，可以录入但是值是0
						|| (value == 1 && vv[key] == 0)){
							setValue(key, 0);
							vv[key] = 0;
							ss[key] = 0;
							scope.pp[key] = false;
						}
					});

				}else if(count === 2){
					//-------- 设置文本框状态 ----------//
					angular.forEach(ss, function(value, key){
						if(value == 0){
							scope.pp[key] = true;
						}
					});
					//-------- 设置文本框状态 ----------//

					//已知条件：
					//guide * rate = ratePrice
					//guide * (1 - rate) = settle
					//当值不为0并且可以录入
					if((vv.guide != 0 && scope.pp.guide == false) 
					&& (vv.rate  != 0 && scope.pp.rate == false)){
						scope.result['tpsp.period_model_settle_price'] = vv.guide * (1 - vv.rate * 0.01);
						scope.result['tpsp.period_model_rate_price'] = vv.guide * vv.rate * 0.01;
					}
					else if((vv.guide  != 0 && scope.pp.guide == false) 
						&&  (vv.settle != 0 && scope.pp.settle == false)){
						scope.result['tpsp.period_model_rate'] = (1 - vv.settle / vv.guide) * 100;
						scope.result['tpsp.period_model_rate_price'] = vv.guide - vv.settle;
					}
					else if((vv.guide != 0 && scope.pp.guide == false) 
						&&  (vv.ratePrice != 0 && scope.pp.ratePrice == false)){
						scope.result['tpsp.period_model_rate'] = (vv.ratePrice / vv.guide) * 100;
						scope.result['tpsp.period_model_settle_price'] = vv.guide - vv.ratePrice;
					}
					else if((vv.rate != 0 && scope.pp.rate == false) 
						&&  (vv.settle != 0 && scope.pp.settle == false)){
						scope.result['tpsp.period_model_guide_price'] = vv.settle / (1 - vv.rate * 0.01);
						scope.result['tpsp.period_model_rate_price'] = (vv.settle / (1 - vv.rate * 0.01)) * vv.rate * 0.01;
					}
					else if((vv.rate != 0 && scope.pp.rate == false) 
						&&  (vv.ratePrice != 0 && scope.pp.ratePrice == false)){
						scope.result['tpsp.period_model_guide_price'] = vv.ratePrice / (vv.rate * 0.01);
						scope.result['tpsp.period_model_settle_price'] = vv.ratePrice / (vv.rate * 0.01) - vv.ratePrice;
					}
					else if((vv.settle != 0 && scope.pp.settle == false) 
						&&  (vv.ratePrice != 0 && scope.pp.ratePrice == false)){
						scope.result['tpsp.period_model_guide_price'] = vv.settle + vv.ratePrice;
						scope.result['tpsp.period_model_rate'] = (vv.ratePrice / (vv.settle + vv.ratePrice))  * 100;
					}

				}else if(count > 2){
					alert('数据错误');
				}

			};

			//---------  设置价格 --------------------//


			//---------  设置票种 --------------------//

			//scope.ttlist = scope.selectobj['ticketTypeList'];
			scope.setticketype = function(){

				console.log(scope.result['tpst.ticket_type']);
		        var para = $state.get('app.tickettype_choice');
                var resolve = {
                    'resolve' : {
                        'data' : function(){
                            return {
                                'tickettype' : scope.result['tpst.ticket_type'],
                                'tickettypearr' : angular.copy(scope.selectobj['ticketTypeList']),
                            }
                        }
                    }
                };

                para = angular.extend(para, resolve);

                var modalInstance = $modal.open(para);
                modalInstance.opened.then(function() {// 模态窗口打开之后执行的函数  
                    console.log('modal is opened');  
                });  
                modalInstance.result.then(function(result) {  
                    console.log(result);  
                    scope.result['tpst[x].ticket_type_id'] = result.str;
                    scope.selectobj['ticketTypeList'] = result.arr;
                    // scope.result['ticket_type_code_arr'] = result.str;
                    // for(var i = 0; i < result.arr.length; i++){
                    // 	scope.result['tpst['+i+'].ticket_type'] = result.arr[i].id;
                    // }

                }, function(reason) {  
                    console.log(reason);// 点击空白区域，总会输出backdrop  
                    // click，点击取消，则会暑促cancel  
                    $log.info('Modal dismissed at: ' + new Date());  
                }); 


		    };


		    scope.tickettypeinfo = function(item){

		    	console.log(item);

		    	//app.tickettype_info1

		    	var para = $state.get('app.tickettype_info1');

		    	para['resolve']['id'] = function(){
	                return item.id;
	            }
                // var resolve = {
                //     'resolve' : {
                //         'data' : function(){
                //             return {
                //                 'tickettype' : scope.result['tpst.ticket_type'],
                //                 'tickettypearr' : angular.copy(scope.selectobj['ticketTypeList']),
                //             }
                //         }
                //     }
                // };

                // para = angular.extend(para, resolve);

                var modalInstance = $modal.open(para);
                


		    };
		    //---------  设置票种 --------------------//
		}

	};

};

