/**
 * 子模块service
 * dlq
 */
var service = function($resource, $q, $modal, $state){

    var model = {
        'tpsb.book_advance_day' : {
            'title' : '预订限制',
            'id' : 'tpsb.book_advance_day',
            'type' : 'select',
            'info' : [
                {'name' : '不限', 'value' : ''},
                {'name' : '当天', 'value' : '0'},
                {'name' : '提前一天', 'value' : '1'},
                {'name' : '提前两天', 'value' : '2'},
                {'name' : '提前三天', 'value' : '3'},
                {'name' : '提前四天', 'value' : '4'},
            ],
            'value' : '',
            'tip' : '提前预定天数',
        },
        'tpsb.book_advance_time' : {
            'title' : '提前预定时间',
            'id' : 'tpsb.book_advance_time',
            'type' : 'select',
            'info' : [
                {'name' : '不限', 'value' : ''},
                {'name' : '当天', 'value' : '0'},
                {'name' : '提前一天', 'value' : '1'},
                {'name' : '提前两天', 'value' : '2'},
                {'name' : '提前三天', 'value' : '3'},
                {'name' : '提前四天', 'value' : '4'},
            ],
            'value' : '',
            'tip' : '提前预定天数',
        },
        'tpsb.use_advance_hour' : {
            'title' : '使用限制',
            'id' : 'tpsb.use_advance_hour',
            'type' : 'select',
            'info' : [
                {'name' : '即时生效', 'value' : '0'},
                {'name' : '次日', 'value' : '-1'},
                {'name' : '1小时', 'value' : '1'},
                {'name' : '2小时', 'value' : '2'},
                {'name' : '3小时', 'value' : '3'},
                {'name' : '4小时', 'value' : '4'},
                {'name' : '5小时', 'value' : '5'},
                {'name' : '6小时', 'value' : '6'},
                {'name' : '7小时', 'value' : '7'},
                {'name' : '8小时', 'value' : '8'},
                {'name' : '9小时', 'value' : '9'},
                {'name' : '10小时', 'value' : '10'},
                {'name' : '11小时', 'value' : '11'},
                {'name' : '12小时', 'value' : '12'},
            ],
            'value' : '0',
            'tip' : '预定后几小时才能入园',
        },
        'tpsb.use_ticket_type' : {
            'title' : '使用票类型',
            'id' : 'tpsb.use_ticket_type',
            'type' : 'select',
            'info' : [
                {'name' : '允许多次消票一张一码', 'value' : '0'},
                {'name' : '允许多次消票：一单一码', 'value' : '1'},
                {'name' : '买几张销几张一单一码', 'value' : '2'},
                {'name' : '允许销少于购买数量的票，剩余票自动退一单一码', 'value' : '3'},
            ],
            'value' : '0',
            'tip' : '消票形式',
        },
        'tpsb.use_day_count' : {
            'title' : '此订单每日使用次数',
            'id' : 'tpsb.use_day_count',
            'type' : 'number',
            'value' : 0,
            'tip' : '0:无限制，其他:使用次数',
        },
        'tpsb.use_count' : {
            'title' : '使用数量',
            'id' : 'tpsb.use_count',
            'type' : 'number',
            'value' : 1,
            'tip' : '允许设置每张使用多张',
        },
        'tpsb.book_person_required_uid' : {
            'title' : '需要用户id',
            'id' : 'tpsb.book_person_required_uid',
            'type' : 'switch',
            'value' : '0',
            'tip' : '是否需要用户id',
        },
        'tpsb.book_person_required_name' : {
            'title' : '需要取票人姓名',
            'id' : 'tpsb.book_person_required_name',
            'type' : 'switch',
            'value' : '1',
            'tip' : '是否需要取票人姓名',
        },
        'tpsb.book_person_required_phone' : {
            'title' : '需要取票人手机号',
            'id' : 'tpsb.book_person_required_phone',
            'type' : 'switch',
            'value' : '1',
            'tip' : '是否需要取票人手机号',
        },
        'tpsb.book_person_required_credentials' : {
            'title' : '需要取票人身份证号',
            'id' : 'tpsb.book_person_required_credentials',
            'type' : 'switch',
            'value' : '1',
            'tip' : '是否需要取票人身份证号',
        },
        'tpsb.book_person_required_platenum' : {
            'title' : '需要车牌号',
            'id' : 'tpsb.book_person_required_platenum',
            'type' : 'switch',
            'value' : '0',
            'tip' : '是否需要车牌号',
        },
        'tpsb.book_person_required_remark' : {
            'title' : '需要下单备注',
            'id' : 'tpsb.book_person_required_remark',
            'type' : 'switch',
            'value' : '0',
            'tip' : '是否需要下单备注',
        },
        'tpsb.book_person_required_playdate' : {
            'title' : '需要游玩日期',
            'id' : 'tpsb.book_person_required_playdate',
            'type' : 'switch',
            'value' : '0',
            'tip' : '是否需要游玩日期',
        },
        'tpsb.book_person_realname_type' : {
            'title' : '需要预订人实名制',
            'id' : 'tpsb.book_person_realname_type',
            'type' : 'switch',
            'value' : '0',
            'tip' : '是否需要预订人实名制',
        },
        'tpsb.book_person_maximum_numtype' : {
            'title' : '最大购买数量限制条件',
            'id' : 'tpsb.book_person_maximum_numtype',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '不限制', 'value' : '0'},
                {'name' : '用户编号', 'value' : '1'},
                {'name' : '手机号', 'value' : '2'},
                {'name' : '身份证号', 'value' : '3'},
            ],
            'value' : '0',
        },
        'tpsb.book_person_maximum_num' : {
            'title' : '最大购买数量',
            'id' : 'tpsb.book_person_maximum_num',
            'type' : 'number',
            'value' : 0,
            //'disabled' : true
        },
        'tpsb.book_person_maximum_rule' : {
            'title' : '最大购买量的限制规则',
            'id' : 'tpsb.book_person_maximum_rule',
            'type' : 'select',
            'info' : [
                {'name' : '游玩日限制（日历有效）', 'value' : 'USE_DATE_MAX'},
                {'name' : '购买日限制', 'value' : 'BOOK_DATE_MAX'},
                {'name' : '产品限制手机号最多购买N张', 'value' : 'PRODUCT_MAX'},
            ],
            'value' : 'BOOK_DATE_MAX',
        },
        'tpsb.visit_person_num_type' : {
            'title' : '共享游客信息',
            'id' : 'tpsb.visit_person_num_type',
            'type' : 'number',
            'value' : 0,
            'tip' : '每几个游客共享一个游客信息  默认0不需要游玩人。1：即每个游客都需要填写游客信息。如果只需要一个游客信息，请传9999或更大数字'
        },
        'tpsb.visit_person_required_name' : {
            'title' : '需要游客姓名',
            'id' : 'tpsb.visit_person_required_name',
            'type' : 'switch',
            'value' : '1',
            'tip' : '是否需要游客姓名',
        },
        'tpsb.visit_person_required_phone' : {
            'title' : '需要游客手机号',
            'id' : 'tpsb.visit_person_required_phone',
            'type' : 'switch',
            'value' : '1',
            'tip' : '是否需要游客手机号',
        },
        'tpsb.visit_person_required_credentials' : {
            'title' : '需要游客身份证',
            'id' : 'tpsb.visit_person_required_credentials',
            'type' : 'switch',
            'value' : '1',
            'tip' : '是否需要游客身份证',
        },
        //--------- ticket_product_sub_info ------------//
        'tpsi.product_name' : {
            'title' : '商品名称',
            'id' : 'tpsi.product_name',
            'type' : 'text',
        },
        'tpsi.logo' : {
            'title' : 'logo',
            'id' : 'tpsi.logo',
            'type' : 'image',
        },
        'tpsi.top_pic' : {
            'title' : '顶图',
            'id' : 'tpsi.top_pic',
            'type' : 'image',
        },
        'tpsi.detail' : {
            'title' : '产品介绍',
            'id' : 'tpsi.detail',
            'type' : 'textarea',
        },
        'tpsi.bookingnotes' : {
            'title' : '预定须知',
            'id' : 'tpsi.bookingnotes',
            'type' : 'textarea',
            'state' : 'normal',
        },
        'tpsi.purchase_price' : {
            'title' : '采购价格',
            'id' : 'tpsi.purchase_price',
            'type' : 'number',
            'value' : 0,
        },
        'tpsi.pay_type' : {
            'title' : '支付方式',
            'id' : 'tpsi.pay_type',
            'type' : 'radiobutton',
            'value' : '0',
            'info' : [
                {'name' : '预存款支付', 'value' : '0'},
                {'name' : '在线支付', 'value' : '1'},
            ],
        },
        'tpsi.auto_cancel_minute' : {
            'title' : '自动取消订单时间',
            'id' : 'tpsi.auto_cancel_minute',
            'type' : 'number',
            'value' : 120,
            'show' : 'no'
        },
        'tpsi.use_date_type' : {
            'title' : '使用日期范围',
            'id' : 'tpsi.use_date_type',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '游玩日期', 'value' : 'calendar_date'},
                {'name' : '购买日', 'value' : 'BUY_DATE'},
                {'name' : '游客在有效日期期限内均可使用', 'value' : 'PERIOD_DATE'},
            ],
            'value' : 'PERIOD_DATE',
        },
        'tpsi.need_ticket_type_type' : {
            'title' : '需要选择票种',
            'id' : 'tpsi.need_ticket_type_type',
            'type' : 'switch',
            'value' : '0',
        },
        'tpsi.need_place_type' : {
            'title' : '需要选择目的地',
            'id' : 'tpsi.need_place_type',
            'type' : 'switch',
            'value' : '0',
        },
        'tpsi.sms_type' : {
            'title' : '发送短信',
            'id' : 'tpsi.sms_type',
            'type' : 'switch',
            'value' : '0',
            'change' : function(value){

                console.log(value);

            },
        },
        'tpsi.sms_templet' : {
            'title' : '短信模板',
            'id' : 'tpsi.sms_templet',
            'type' : 'select',
            'url' : '/api/as/tc/salesmstemplate/list',
            // 'info' : [
            //     {'name' : '请选择', 'value' : '0', 'sms' : ''},
            //     {'name' : '模板一', 'value' : '1', 'sms' : '模板一的内容模板一的内容模板一的内容模板,一的内容模板一的内容模板一,的内容模板一的内容模板一的内容模板一的内容模板一的内容模板一的内容'},
            //     {'name' : '模板二', 'value' : '2', 'sms' : '模板二的内容模板一的内容模板一的内容模板一的内容模板一的内容模板一的内容模板一的内容模板一的内容模板一的内容模板一的内容模板一的内容'},
            //     {'name' : '模板三', 'value' : '3', 'sms' : '模板三的内容模板一的内容模板一的内容模板一的内容模板一的内容模板一的内容模板一的内容模板一的内容模板一的内容模板一的内容模板一的内容'},
            // ],
            'value' : '0',
            'show' : 'no',
        },
        'tpsi.sms_diy' : {
            'title' : '短信补充说明',
            'id' : 'tpsi.sms_diy',
            'type' : 'textarea',
            'state' : 'normal',
            'show' : 'no',
        },
        'tpsi.sale_target_type' : {
            'title' : '销售目标类型',
            'id' : 'tpsi.sale_target_type',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '公开销售', 'value' : '0'},
                {'name' : '独立授权销售', 'value' : '1'},
            ],
            'value' : '0',
        },
        'tpsi.product_Interface_type' : {
            'title' : '接口产品',
            'id' : 'tpsi.product_Interface_type',
            'type' : 'switch',
            'value' : '0',
        },
        'tpsi.product_belong' : {
            'title' : '产品所属供应商系统 ',
            'id' : 'tpsi.product_belong',
            'type' : 'text',
        },
        'tpsi.product_category' : {
            'title' : '产品分类 ',
            'id' : 'tpsi.product_category',
            'type' : 'text',
        },
        'tpsi.product_sale_type' : {
            'title' : '分销类型',
            'id' : 'tpsi.product_sale_type',
            'type' : 'radiobutton',
            'info' : [
                {'name' : 'B2C', 'value' : 'B2C'},
                {'name' : 'B2B', 'value' : 'B2B'},
            ],
            'value' : 'B2B',
        },

        //--------------- 
        'tpsp.use_date_type' : {
            'title' : '使用日期范围',
            'id' : 'tpsp.use_date_type',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '游玩日期', 'value' : 'calendar_date'},
                {'name' : '购买日', 'value' : 'BUY_DATE'},
                {'name' : '游客在有效日期期限内均可使用', 'value' : 'PERIOD_DATE'},
            ],
            'value' : 'PERIOD_DATE',
            'data' : {
                'tpsp.valid_days_after_usedate' : {
                    'title' : '游客选定的游玩日期起x天内有效 ',
                    'id' : 'tpsp.valid_days_after_usedate',
                    'type' : 'number',
                    'value' : 1,
                },
                'tpsp.valid_days_after_buydate' : {
                    'title' : '游客购买日期起x天内有效 ',
                    'id' : 'tpsp.valid_days_after_buydate',
                    'type' : 'number',
                    'value' : 1,
                }
            },
        },
        'tpsp.valid_days_after_usedate' : {
            'title' : '游客选定的游玩日期起x天内有效， 1：当天',
            'id' : 'tpsp.valid_days_after_usedate',
            'type' : 'number',
            'value' : 1,
            'show' : 'no'
        },
        'tpsp.valid_days_after_buydate' : {
            'title' : '游客购买日期起x天内有效， 1：当天',
            'id' : 'tpsp.valid_days_after_buydate',
            'type' : 'number',
            'value' : 1,
            'show' : 'no'
        },


        'tpsp.period_model_start_date' : {
            'title' : '有效期模式-开始日期',
            'id' : 'tpsp.period_model_start_date',
            'type' : 'date1',
            'show' : 'no',
        },
        'tpsp.period_model_end_date' : {
            'title' : '有效期模式-结束日期',
            'id' : 'tpsp.period_model_end_date',
            'type' : 'date1',
            'show' : 'no',
        },
        'tpsp.period_model_valid_week' : {
            'title' : '有效期模式-周几有效',
            'id' : 'tpsp.period_model_valid_week',
            'type' : 'checkbox',
            'info' : [
                {'name' : '周一', 'value' : '1'},
                {'name' : '周二', 'value' : '2'},
                {'name' : '周三', 'value' : '3'},
                {'name' : '周四', 'value' : '4'},
                {'name' : '周五', 'value' : '5'},
                {'name' : '周六', 'value' : '6'},
                {'name' : '周日', 'value' : '7'},
            ],
            'value' : {
                '1' : true,
                '2' : true,
                '3' : true,
                '4' : false,
                '5' : true,
                '6' : true,
                '7' : true,
            },
            'show' : 'no',
        },
        'tpsp.period_model_invalid_date' : {
            'title' : '有效期模式-不可使用日期',
            'id' : 'tpsp.period_model_invalid_date',
            'type' : 'date1',
            'show' : 'no',
        },

        'tpsp.period_model_market_price' : {
            'title' : '有效期模式市场价',
            'id' : 'tpsp.period_model_market_price',
            'type' : 'float',
            'value' : 0,
            'show' : 'no',
        },
        'tpsp.period_model_guide_price' : {
            'title' : '有效期模式建议零售价',
            'id' : 'tpsp.period_model_guide_price',
            'type' : 'float',
            'value' : 0,
            'show' : 'no',
        },
        'tpsp.period_model_rate' : {
            'title' : '有效期模式费率',
            'id' : 'tpsp.period_model_rate',
            'type' : 'float',
            'value' : 0,
            'show' : 'no',
        },
        'tpsp.period_model_settle_price' : {
            'title' : '有效期模式结算价',
            'id' : 'tpsp.period_model_settle_price',
            'type' : 'float',
            'value' : 0,
            'show' : 'no',
        },
        'tpsp.period_model_rate_price' : {
            'title' : '有效期模式用金额',
            'id' : 'tpsp.period_model_rate_price',
            'type' : 'float',
            'value' : 0,
            'show' : 'no',
        },
        'tpsp.period_model_stock' : {
            'title' : '库存',
            'id' : 'tpsp.period_model_stock',
            'type' : 'number',
            'value' : 0,
            'show' : 'no',
        },




        'tpsp.day_usetime_period_start' : {
            'title' : '有效时间开始',
            'id' : 'tpsp.day_usetime_period_start',
            'type' : 'time',
            'value' : '08:00',
            'show' : 'no',
        },
        'tpsp.day_usetime_period_end' : {
            'title' : '有效时间结束',
            'id' : 'tpsp.day_usetime_period_end',
            'type' : 'time',
            'value' : '17:00',
            'show' : 'no',
        },
        'tpsp.minimum' : {
            'title' : '单次最小购买量',
            'id' : 'tpsp.minimum',
            'type' : 'number',
            'value' : 1,
        },
        'tpsp.maximum' : {
            'title' : '单次最大购买量',
            'id' : 'tpsp.maximum',
            'type' : 'number',
            'value' : 1,
        },

        'tpsp.refund_type' : {
            'title' : '退款规则',
            'id' : 'tpsp.refund_type',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '不可退款', 'value' : '0'},
                {'name' : '随时退', 'value' : '1'},
                {'name' : '有条件退', 'value' : '2'},
            ],
            'value' : '2',
        },
        'tpsp.refund_date_rule' : {
            'title' : '时间限制',
            'id' : 'tpsp.refund_date_rule',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '无限制', 'value' : '0'},
                {'name' : '预定日期开始前', 'value' : '1'},
                {'name' : '预定时间截止前', 'value' : '2'},
                {'name' : '预定时间截止后', 'value' : '3'},
                {'name' : '剩余全退', 'value' : '4'},
                {'name' : '未使用全退', 'value' : '5'},
            ],
            'value' : '0',
        },
        'tpsp.refund_date_limit' : {
            'title' : '退款日期限制',
            'id' : 'tpsp.refund_date_limit',
            'type' : 'text',
            'tip' : '格式：x_hh:mm,如：2_22:00，表示有效期开始前2天22点之前可退款。例如有效期开始日期是20号，则18号晚上22点前可退。',
        },
        'tpsp.refund_fee_type' : {
            'title' : '退款手续费类型',
            'id' : 'tpsp.refund_fee_type',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '无手续费', 'value' : '0'},
                {'name' : '每张票需要手续费', 'value' : '1'},
                {'name' : '每笔订单扣手续费', 'value' : '2'},
                {'name' : '销售价格的百分比', 'value' : '3'},
            ],
            'value' : '0',
        },
        'tpsp.refund_fee_price' : {
            'title' : '退费值',
            'id' : 'tpsp.refund_fee_price',
            'type' : 'number',
            'value' : 0,
        },
        'tpsp.refund_node' : {
            'title' : '退款说明',
            'id' : 'tpsp.refund_node',
            'type' : 'text',
        },

        'tpsp.sale_period_start_time' : {
            'title' : '上架时间',
            'id' : 'tpsp.sale_period_start_time',
            'type' : 'date1',
        },
        'tpsp.sale_period_end_time' : {
            'title' : '下架时间',
            'id' : 'tpsp.sale_period_end_time',
            'type' : 'date1',
        },

        //--------
        'tpst[x].ticket_type_id' : {
            'title' : '票种',
            'id' : 'tpst[x].ticket_type_id',
            'type' : 'text',
            'show' : 'no',
        },


    };



    var category = [
        {
            'name' : '基本信息',
            'data' : [
                'tpsi.product_name',
                'tpsi.purchase_price',
                'tpsi.pay_type',
                'tpsi.auto_cancel_minute',
                'tpsi.use_date_type',
                'tpsi.need_ticket_type_type',
                'tpst[x].ticket_type',
                'tpsi.need_place_type',
                'tpsi.sms_type',
                'tpsi.sms_templet',
                'tpsi.sms_diy',
                'tpsi.sale_target_type',
                'tpsi.product_Interface_type',
                'tpsi.product_belong',
                'tpsi.product_category',
                'tpsi.product_sale_type',
            ],
            'active' : true,
        },
        {
            'name' : '图片信息',
            'data' : [
                'tpsi.logo',
                'tpsi.top_pic',
            ],
        },
        {
            'name' : '商品简介',
            'data' : [
                'tpsi.detail',
                'tpsi.bookingnotes',//预定须知
            ],
        },
        {
            'name' : '预定信息',
            'data' : [
                'tpsb.book_advance_day',
                'tpsb.book_advance_time',
                'tpsb.use_advance_hour',
                'tpsb.use_ticket_type',
                'tpsb.use_day_count',
                'tpsb.use_count',
                'tpsb.book_person_required_uid',
                'tpsb.book_person_required_name',
                'tpsb.book_person_required_phone',
                'tpsb.book_person_required_credentials',
                'tpsb.book_person_required_platenum',
                'tpsb.book_person_required_remark',
                'tpsb.book_person_required_playdate',
                'tpsb.book_person_realname_type',
                'tpsb.book_person_maximum_numtype',
                'tpsb.book_person_maximum_num',
                'tpsb.book_person_maximum_rule',
                'tpsb.visit_person_num_type',
                'tpsb.visit_person_required_name',
                'tpsb.visit_person_required_phone',
                'tpsb.visit_person_required_credentials',
            ],
            'active' : false,
        },
        {
            'name' : '价格',
            'data' : [
                'tpsp.use_date_type',
                'tpsp.valid_days_after_usedate',
                'tpsp.valid_days_after_buydate',
                'tpsp.minimum',
                'tpsp.maximum',
                'tpsp.period_model_start_date',
                'tpsp.period_model_end_date',
                'tpsp.period_model_valid_week',
                'tpsp.period_model_invalid_date',
                'tpsp.period_model_market_price',
                'tpsp.period_model_guide_price',
                'tpsp.period_model_rate',
                'tpsp.period_model_settle_price',
                'tpsp.period_model_rate_price',
                'tpsp.day_usetime_period_start',
                'tpsp.day_usetime_period_end',
                'tpsp.period_model_stock',
                'tpsp.refund_type',
                'tpsp.refund_date_rule',
                'tpsp.refund_date_limit',
                'tpsp.refund_fee_type',
                'tpsp.refund_fee_price',
                'tpsp.refund_node',
                'tpsp.sale_period_start_time',
                'tpsp.sale_period_end_time',
            ],
            'active' : false,
        }


    ];


    return {

        model : function(){
            return model;
        },
        category : function(){
            return category;
        },

    };

};

module.exports = service;