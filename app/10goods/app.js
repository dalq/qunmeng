var App = angular.module('goods', []);

App.config(require('./router'));
App.factory('goodsservice', require('./service'));

App.controller('goodslist',require('./controllers/list'));
App.controller('goodscreate',require('./controllers/create'));
App.controller('goodsinfo',require('./controllers/info'));
App.controller('goodsedit',require('./controllers/edit'));

App.controller('goodstempcreate',require('./controllers/tempCreate'));
App.controller('goodsinfocreate',require('./controllers/infoCreate'));
App.controller('goodstest',require('./controllers/test'));


App.directive('goodsform',require('./directives/form'));


module.exports = App;