module.exports = function($scope, formconfig, $stateParams, model){

	var id = $stateParams.id;

	formconfig.start({
		'title' : '商品模板详情',
		'formtitle' : '商品模板基本信息',
		'elements' : model(),
		'info' : {
			'url' : '/api/ac/tc/goodsTemplete/info',
			'para' : {'id' : id}
		}
	}, $scope);

	$scope.form = formconfig;

};