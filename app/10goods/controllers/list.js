module.exports = function($scope, tableconfig, $state, $modal){

	tableconfig.start($scope, {
		'url' : '/api/ac/tc/ticketGoods/getTicketGoodsList',
		'col' : [
			{'title' : '商品编号', 'col' : 'goods_code', 'click' : 'edit1'},
			{'title' : '商品名称', 'col' : 'goods_name', 'click' : 'edit1', 'default' : '<span style="color:red;">未设置</span>'},
			// {'title' : '模板', 'col' : 'goods_templete_code'},
			{'title' : '渠道', 'col' : 'channel_id_arr', 'click' : 'channel' , 'default' : '设置渠道'},
			// {'title' : '景区', 'col' : 'place_code_arr'},
			// {'title' : '价格', 'col' : 'sale_price_arr'},
			//{'title' : '票种', 'col' : 'ticket_type_code_arr'},
			{'title' : '操作', 'col' : 'btn'},
		],
		'btn' : [
			{'title' : '查看详情', 'onclick' : 'info'},
			{'title' : '编辑', 'onclick' : 'edit1'},
			{'title' : '删除', 'onclick' : 'delete'},
		],
		'search' : [
			{'title' : '名称', 'type' : 'txt', 'name' : 'templete_name', 'show' : true},
			{'title' : '编号', 'type' : 'txt', 'name' : 'templete_code', 'show' : true},
		],
		'title' : '商品列表',
		'info' : {
			'to' : 'app.goods_info',
		},
		'delete' : {
			'url' : '/api/ac/tc/ticketGoods/updateTicketGoodsDelete',
		},
		// 'edit' : {
		// 	'to' : 'app.goods_edit'
		// }
	});
	$scope.table = tableconfig;


	$scope.channel = function(item){
		console.log(item);
		var para = $state.get('app.goods_channel');

        var resolve = {
            'resolve' : {
                'item' : function(){
                    return item;
                },
                model : function(goodsservice){
	                return goodsservice.model();
	            },
	            category : function(goodsservice){
	                return goodsservice.category();
	            },
	            date2str : function(utilservice){
	                return utilservice.date2str;
	            }
            }
        };

        para = angular.extend(para, resolve);

        console.log(para);

        var modalInstance = $modal.open(para);
        modalInstance.opened.then(function() {// 模态窗口打开之后执行的函数  
            console.log('modal is opened');  
        });  
        modalInstance.result.then(function(result) {  
            console.log(result);  


        }, function(reason) {  
            console.log(reason);// 点击空白区域，总会输出backdrop  
            // click，点击取消，则会暑促cancel  
            $log.info('Modal dismissed at: ' + new Date());  
        }); 
	}


	$scope.edit1 = function(item){
		console.log(item);
		console.log({'id' : item.goods_templete_id, 'goodsid' : item.id});
		$state.go('app.goods_info_create', {'id' : item.goods_templete_id, 'goodsid' : item.id});
	}

};