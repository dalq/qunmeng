module.exports = function($scope, formconfig, model){

	formconfig.start({
		'title' : '创建商品模板',
		'formtitle' : '商品模版基本信息',
		'elements' : model(),
		'save' : {
			'url' : '/api/ac/tc/goodsTemplete/create',
			'to' : 'app.goodsTemp_list',
		}
	}, $scope);

};