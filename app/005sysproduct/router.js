

var router = function ($urlRouterProvider, $stateProvider) {


    $stateProvider

        //群盟系统产品列表——待审批列表
        .state('app.qmshenpi', {
            url: "/qm/qmshenpi.html",
            views: {
                'main@': {
                    template: require('./views/qmshenpi.html'),
                    controller: 'qmshenpi',
                }
            },
            resolve:{
                date2str: function (utilservice) {
                    return utilservice.getDate;
                },
                str2date: function (utilservice) {
                    return utilservice.str2date;
                },
                what : function () {
                    return 'manager';
                }
            }
        })

        


        //群盟系统产品列表——管理端
        .state('app.qmmanagerplist', {
            url: "/qm/qmmanagerplist.html",
            views: {
                'main@': {
                    template: require('./views/qmmanagerplist.html'),
                    controller: 'qmmanagerplist',
                }
            },
            resolve:{
                date2str: function (utilservice) {
                    return utilservice.getDate;
                },
                str2date: function (utilservice) {
                    return utilservice.str2date;
                },
                what : function () {
                    return 'qunmeng';
                }
            }
        })



        //群盟系统产品列表——商家端
        .state('app.qmbusinessplist', {
            url: "/qm/qunmengproductlist.html",
            views: {
                'main@': {
                    template: require('./views/sys.html'),
                    controller: 'qmmanagerplist',
                }
            },
            resolve:{
                date2str: function (utilservice) {
                    return utilservice.getDate;
                },
                str2date: function (utilservice) {
                    return utilservice.str2date;
                },
                what : function () {
                    return 'qunmengshangjia';
                }
            }
        })


       


        ;

};

module.exports = router;