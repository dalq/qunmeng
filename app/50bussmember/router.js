/**
* 子模块路由
* dlq
*/

var router = function ($urlRouterProvider, $stateProvider) {


    $stateProvider

        //会员列表
        .state('app.memberlist', {
            url: "/bussmember/memberlist.html",
            views: {
                'main@': {
                    template: require('./views/memberlist.html'),
                    controller: 'memberlist',
                }
            },
            resolve: {
            	wxuserlist : function(bussmemberservice){
	                return bussmemberservice.wxuserlist();
	            },
	            updatelevel : function(bussmemberservice){
	                return bussmemberservice.updatelevel();
	            },
	            memberlevellist : function(bussmemberservice){
	                return bussmemberservice.memberlevellist();
	            },
                couproductlist : function(bussmemberservice){
                    return bussmemberservice.couproductlist();
                },
                createcouorder : function(bussmemberservice){
                    return bussmemberservice.createcouorder();
                },
                userinfo: function (dashboardservice) {
					return dashboardservice.userinfo;
				},
				updatefxlevel : function(bussmemberservice){
	                return bussmemberservice.updatefxlevel();
	            },
                date2str: function (utilservice) {
                    return utilservice.getDate;
                },
                str2date: function (utilservice) {
                    return utilservice.str2date;
                },
                
            }
        })

        //会员等级列表
        .state('app.memberlevellist', {
            url: "/bussmember/memberlevellist.html",
            views: {
                'main@': {
                    template: require('./views/memberlevellist.html'),
                    controller: 'memberlevellist',
                }
            },
            resolve: {
            	levellist : function(bussmemberservice){
	                return bussmemberservice.levellist();
	            },
	            save : function(bussmemberservice){
	                return bussmemberservice.save();
	            }
            }
        })

        //分销等级列表
        .state('app.distribution', {
            url: "/bussmember/memberfxlist.html",
            views: {
                'main@': {
                    template: require('./views/memberfxlist.html'),
                    controller: 'memberfxlist',
                }
            },
            resolve: {
            	fxlevellist : function(bussmemberservice){
	                return bussmemberservice.fxlevellist();
	            },
	            savefx : function(bussmemberservice){
	                return bussmemberservice.savefx();
	            }
            }
        })
        

};

module.exports = router;