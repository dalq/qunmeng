module.exports = function($scope, $modalInstance, savefx, what, obj){

	$scope.obj = {};
	if(what == 'edit') {
		$scope.obj = obj;
	}


	$scope.cancel = function(){

		$modalInstance.close();

	}

	$scope.gogo = function(){

		if($scope.obj.code === undefined || $scope.obj.code == '')
		{
			alert('等级编号不能为空');
			return;
		}

		if($scope.obj.title === undefined || $scope.obj.title == '')
		{
			alert('等级名称不能为空');
			return;
		}

		savefx.save($scope.obj, function(res){

			if(res.errcode === 0)
			{
				alert('保存成功');
				$modalInstance.close();
			}
			else
			{
				alert(res.errmsg);
			}

		});

	}



};