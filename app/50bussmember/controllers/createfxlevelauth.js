module.exports = function($scope, $modalInstance, savefx, what, obj){

	$scope.objs = [
		{
			'id':1,'name':'我的订单'
		},
		/*{
			'id':2,'name':'我的电子券'
		},*/
		{
			'id':3,'name':'我的优惠券'
		},
		{
			'id':4,'name':'销售统计'
		},
		{
			'id':5,'name':'我的账户'
		},
		{
			'id':6,'name':'我的货架'
		},
		{
			'id':7,'name':'分销查询'
		},
		// {
		// 	'id':8,'name':'分销商管理'
		// },
		{
			'id':9,'name':'我的二维码'
		},
		{
			'id':10,'name':'我要加入'
		},
		{
			'id':11,'name':'我的分销商'
		},
		{
			'id':12,'name':'我的收藏'
		},
		{
			'id':13,'name':'我的粉丝'
		},
		{
			'id':14,'name':'主推销售'
		},
		{
			'id':15,'name':'套票激活'
		},
		{
			'id':16,'name':'群盟统计'
		},
		{
			'id':17,'name':'我的积分'
		},
		{
			'id':18,'name':'群盟教程'
		}
	];
	var idobj = {};

	$scope.obj = {};
	if(what == 'edit') {
		$scope.obj = obj;
	}

	//菜单授权选择
	
	if($scope.obj.auth != undefined) {
		var autharr = $scope.obj.auth.split(",");
		for(var i = 0; i < $scope.objs.length; i++)
	    {
	        var tmp = $scope.objs[i];
	        tmp.iselected = 0;
	        for(var j = 0; j < autharr.length; j++)
	        {
	            if(autharr[j] == tmp.id)
	            {
	                tmp.iselected = 1;
	                idobj[tmp.id] = '';
	                break;
	            }
	        }
	    }
	}
    

	$scope.cancel = function(){

		$modalInstance.close();

	}

	$scope.gogo = function(){

		var auth = '';
        angular.forEach(idobj, function (value, key) {
            if(auth == '') {
            	auth = key;
            }else{
            	auth = auth+','+key;
            }
        });

        if(auth == '')
        {
            alert('请选择菜单权限');
            return;
        }
        $scope.obj.auth = auth;
		savefx.save($scope.obj, function(res){

			if(res.errcode === 0)
			{
				alert('保存成功');
				$modalInstance.close();
			}
			else
			{
				alert(res.errmsg);
			}

		});

	}

	$scope.selection = function($event, obj){

        console.log(obj);

        var checkbox = $event.target;
        
        if(checkbox.checked)
        {
            idobj[obj.id] = '';
        }
        else
        {
           delete idobj[obj.id];
        }

    };



};