module.exports = function ($scope, $resource, wxuserlist, updatelevel, memberlevellist, 
	couproductlist, createcouorder, userinfo, updatefxlevel, $modal, date2str, str2date) {

	$scope.fx_levelarr = [
		{title : '全部', code : ''},
		{title : '一级', code : '1'},
		{title : '二级', code : '2'},
	];

	$scope.searchform = {};


	$scope.start_time = {
        //'date': date2str(new Date()),
        'opened': false
    };

    $scope.end_time = {
        //'date': date2str(new Date()),
        'opened': false
    };

    $scope.open = function ($event, item) {
        $event.preventDefault();
        $event.stopPropagation();
        item.opened = true;
    };


   	memberlevellist.get({}, function(res){ 
    	console.log(res);
        if(res.errcode === 0)
        {
        	$scope.levelarr = res.data;
        	$scope.levelarr.unshift({ title : '最高权限', code: '1' });
        	$scope.levelarr.unshift({ title : '无级别', code: '0' });
        }
        else
        {
            alert(res.errmsg);
        }
    });


    // couproductlist.get({}, function(res){ 
    // 	console.log(res);
    //     if(res.errcode === 0)
    //     {
    //     	$scope.couarr = res.data.results;
    //     }
    //     else
    //     {
    //         alert(res.errmsg);
    //     }
    // });




	/* 分页
     * ========================================= */
	$scope.maxSize = 5;            //最多显示多少个按钮
	$scope.bigCurrentPage = 1;      //当前页码
	$scope.itemsPerPage = 10;         //每页显示几条

	$scope.conut = 0;
	$scope.load = function () {

		var s = '';
        var e = '';

        if($scope.start_time.date){
            if (typeof $scope.start_time.date === 'string') {
                s = $scope.start_time.date;
            } else {
                s = date2str($scope.start_time.date);
            }
        }
        if($scope.end_time.date){
            if (typeof $scope.end_time.date === 'string') {
                e = $scope.end_time.date;
            } else {
                e = date2str($scope.end_time.date);
            }
        }

        var para = {
            pageNo: $scope.bigCurrentPage,
            pageSize: $scope.itemsPerPage,
            start_time : s == '' ? '' : s + " 00:00:00",
            end_time : e == '' ? '' : e + " 23:59:59",
        };

		para = angular.extend($scope.searchform, para);

		wxuserlist.save(para, function(res){ 
			console.log(res);
			if (res.errcode !== 0) {
				alert("数据获取失败");
				return;
			}

			$scope.objs = res.data.results;
			$scope.bigTotalItems = res.data.totalRecord;
			$scope.bigCurrentPage = res.data.pageNo;

		});
	};
	$scope.load();

	$scope.auth = function (obj) {

		updatelevel.save(obj, function(res){ 

			if (res.errcode !== 0) {
				alert(res.errmsg);
				return;
			}
			alert("授权成功");
			$scope.load();

		});

	};

	$scope.unauth = function (obj) {

		updatefxlevel.save(obj, function(res){ 

			if (res.errcode !== 0) {
				alert(res.errmsg);
				return;
			}
			alert("解绑成功");
			$scope.load();

		});

	};


	$scope.coupon = function(obj){

		var para = {
			'userid' : obj.user_base,
			'product_code' : obj.coupon,
			'mobile' : obj.mobile,
		};


		createcouorder.save(para, function(res){

			console.log(res);

			if (res.errcode !== 0) {
				alert(res.errmsg);
				return;
			}
			alert("发放成功");

		});

	};


	$scope.getUpLevel = function(obj){

		$resource('/api/as/uc/userwxsk/getuplevel', {}, {}).save({'user_base' : obj.user_base}, function (res) {
			console.log(res);

			

		});


	};


	$scope.relation = function(obj){

		var modalInstance = $modal.open({
			template: require('../../51weshop/views/relation.html'),
			controller: 'weshoprelation',
			url: '/weshoprelation',
			//size : 'lg',
			resolve: {
				'user_base': function () {
					return obj.user_base;
				},
				// str2date: function () {
				// 	return str2date;
				// },
				// date2str: function () {
				// 	return date2str;
				// },
				
			}
		});

		modalInstance.opened.then(function () {// 模态窗口打开之后执行的函数  
			// $scope.load();
		});
		modalInstance.result.then(function (showResult) {
			$scope.load();
		}, function (reason) {
			$scope.load();
			// // click，点击取消，则会暑促cancel  
			// $log.info('Modal dismissed at: ' + new Date());
		});

	};


	//统计
	$scope.statistics = function(obj){

		///api/as/wc/usedrecord/sysgoodsstatisticslist
		var modalInstance = $modal.open({
			template: require('../../51weshop/views/statistics.html'),
			controller: 'weshopstatistics',
			url: '/weshopstatistics',
			//size : 'lg',
			resolve: {
				obj: function () {
					return obj;
				},
				// str2date: function () {
				// 	return str2date;
				// },
				// date2str: function () {
				// 	return date2str;
				// },
				
			}
		});

		modalInstance.opened.then(function () {// 模态窗口打开之后执行的函数  
			// $scope.load();
		});
		modalInstance.result.then(function (showResult) {
			//$scope.load();
		}, function (reason) {
			//$scope.load();
			// // click，点击取消，则会暑促cancel  
			// $log.info('Modal dismissed at: ' + new Date());
		});

		
	};


	$scope.getOrder = function(obj){

		console.log(obj);

		var modalInstance = $modal.open({
			template: require('../../51weshop/views/orderlist_pop.html'),
			controller: 'weshoporderlistpop',
			url: '/weshoporderlistpop',
			size : 'lg',
			resolve: {
				obj: function () {
					return obj;
				},
				// str2date: function () {
				// 	return str2date;
				// },
				// date2str: function () {
				// 	return date2str;
				// },
				
			}
		});

		modalInstance.opened.then(function () {// 模态窗口打开之后执行的函数  
			// $scope.load();
		});
		modalInstance.result.then(function (showResult) {
			//$scope.load();
		}, function (reason) {
			//$scope.load();
			// // click，点击取消，则会暑促cancel  
			// $log.info('Modal dismissed at: ' + new Date());
		});
		

	};

	//设置级别
    $scope.setlevel = function(item){

    	console.log(item);

    	var modalInstance = $modal.open({
          template: require('../../006member/views/pop_setlevel.html'),
          controller: 'qmsetlevel',
          //size: 'lg',
          resolve: {
            user_base : function(){
                return item.user_base;
            },
            levelarr : function() {
            	return $scope.levelarr;
            },
            levelcode : function(){
            	return item.level;
            },
            remark : function () {
            	return item.remark;
            }

          }
        });

        modalInstance.result.then(function () {
        	$scope.load();
        }, function () {
            
        });

    };

};