module.exports = function ($scope, $resource, fxlevellist, savefx, $modal) {

	$scope.searchform = {};

	/* 分页
     * ========================================= */
	$scope.maxSize = 5;            //最多显示多少个按钮
	$scope.bigCurrentPage = 1;      //当前页码
	$scope.itemsPerPage = 10;         //每页显示几条

	$scope.conut = 0;
	$scope.load = function () {
		var para = {
			pageNo: $scope.bigCurrentPage,
			pageSize: $scope.itemsPerPage,
		};

		para = angular.extend($scope.searchform, para);

		fxlevellist.save(para, function(res){ 
			console.log(res);
			if (res.errcode !== 0) {
				alert("数据获取失败");
				return;
			}

			$scope.objs = res.data.results;
			$scope.bigTotalItems = res.data.totalRecord;
			$scope.bigCurrentPage = res.data.pageNo;

		});

	};
	$scope.load();

	$scope.create = function(obj){
        
    	var modalInstance = $modal.open({
          template: require('../views/createfxlevel.html'),
          controller: 'createfxlevel',
          size: 'xs',
          resolve: {
          	what : function(){
                return 'create';
            },
            obj : function(){
                return obj;
            },
            savefx : function(){
                return savefx;
            }
          }
        });

        modalInstance.result.then(function () {
            $scope.load();
        }, function () {
            
        });
    }

    $scope.edit = function(obj){
        
    	var modalInstance = $modal.open({
          template: require('../views/createfxlevel.html'),
          controller: 'createfxlevel',
          size: 'xs',
          resolve: {
          	what : function(){
                return 'edit';
            },
            obj : function(){
                return obj;
            },
            savefx : function(){
                return savefx;
            }
          }
        });

        modalInstance.result.then(function () {
            $scope.load();
        }, function () {
            
        });
    }

    $scope.auth = function(obj){
        
    	var modalInstance = $modal.open({
          template: require('../views/createfxlevelauth.html'),
          controller: 'createfxlevelauth',
          size: 'xs',
          resolve: {
          	what : function(){
                return 'edit';
            },
            obj : function(){
                return obj;
            },
            savefx : function(){
                return savefx;
            }
          }
        });

        modalInstance.result.then(function () {
            $scope.load();
        }, function () {
            
        });
    }


};