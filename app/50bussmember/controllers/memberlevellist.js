module.exports = function ($scope, $resource, levellist, save, $modal) {

  var isadopt_arr = ['二级需要审核', '二级不需要审核'];

	$scope.searchform = {};

	/* 分页
     * ========================================= */
	$scope.maxSize = 5;            //最多显示多少个按钮
	$scope.bigCurrentPage = 1;      //当前页码
	$scope.itemsPerPage = 10;         //每页显示几条

	$scope.conut = 0;
	$scope.load = function () {
		var para = {
			pageNo: $scope.bigCurrentPage,
			pageSize: $scope.itemsPerPage,
		};

		para = angular.extend($scope.searchform, para);

		levellist.save(para, function(res){ 
			console.log(res);
			if (res.errcode !== 0) {
				alert("数据获取失败");
				return;
			}

      for(var i = 0; i < res.data.results.length; i++){
        var tmp = res.data.results[i];
        tmp['isadopt_label'] = isadopt_arr[parseInt(tmp.isadopt)];
      }

			$scope.objs = res.data.results;
			$scope.bigTotalItems = res.data.totalRecord;
			$scope.bigCurrentPage = res.data.pageNo;

		});

	};
	$scope.load();

	$scope.create = function(obj){
        
    	var modalInstance = $modal.open({
          template: require('../views/creatememberlevel.html'),
          controller: 'creatememberlevel',
          size: 'xs',
          resolve: {
          	what : function(){
                return 'create';
            },
            obj : function(){
                return obj;
            },
            save : function(){
                return save;
            }
          }
        });

        modalInstance.result.then(function () {
            $scope.load();
        }, function () {
            
        });
    }

    $scope.edit = function(obj){
        
    	var modalInstance = $modal.open({
          template: require('../views/creatememberlevel.html'),
          controller: 'creatememberlevel',
          size: 'xs',
          resolve: {
          	what : function(){
                return 'edit';
            },
            obj : function(){
                return obj;
            },
            save : function(){
                return save;
            }
          }
        });

        modalInstance.result.then(function () {
            $scope.load();
        }, function () {
            
        });
    }


    $scope.commission = function(obj){

      var modalInstance = $modal.open({
        template: require('../../51weshop/views/commission.html'),
        controller: 'weshopcommission',
        url: '/weshope/:code',
        size: 'lg',
        resolve: {
          'level': function () {
            return obj;
          },
          
        }
      });

      modalInstance.opened.then(function () {// 模态窗口打开之后执行的函数  
        // $scope.load();
      });
      modalInstance.result.then(function (showResult) {
        $scope.load();
      }, function (reason) {
        $scope.load();
        // // click，点击取消，则会暑促cancel  
        // $log.info('Modal dismissed at: ' + new Date());
      });


    }

};