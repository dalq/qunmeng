module.exports = function($scope, $modalInstance, save, what, obj, $resource){

	$scope.obj = {
		'isadopt' : '0',
	};
	
	if(what == 'edit') {
		$scope.obj = obj;
	}


	$scope.cancel = function(){

		$modalInstance.close();

	}

	$scope.gogo = function(){

		if($scope.obj.title === undefined || $scope.obj.title == '')
		{
			alert('分销体系不能为空');
			return;
		}

		$resource('/api/as/uc/userwxsk/save', {}, {}).save($scope.obj, function (res) {
			if(res.errcode === 0)
			{
				alert('保存成功');
				$modalInstance.close();
			}
			else
			{
				alert(res.errmsg);
			}

		});

	}



};