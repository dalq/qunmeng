var App = angular.module('bussmember', []);

App.config(require('./router'));
App.factory('bussmemberservice', require('./service'));

App.controller('memberlist',require('./controllers/memberlist'));
App.controller('memberlevellist',require('./controllers/memberlevellist'));
App.controller('memberfxlist',require('./controllers/memberfxlist'));
App.controller('creatememberlevel',require('./controllers/creatememberlevel'));
App.controller('createfxlevel',require('./controllers/createfxlevel'));
App.controller('createfxlevelauth',require('./controllers/createfxlevelauth'));
App.controller('memberlevelcommission',require('./controllers/commission'));





module.exports = App;