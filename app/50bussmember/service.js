/**
 * 子模块service
 * dlq
 */
var service = function($resource){

    var wxuserlist =  "/api/ac/uc/userWxSkService/wxuserlist";

    var updatelevel =  "/api/ac/uc/userWxSkService/updatelevel";

    var memberlevellist =  "/api/as/uc/userwxsk/memberlevellist";

    var levellist =  "/api/as/uc/userwxsk/levellist";

    var save =  "/api/as/uc/userwxsk/save";

    var fxlevellist =  "/api/as/uc/userwxsk/fxlevellist";

    var savefx =  "/api/as/uc/userwxsk/savefx";

    var updatefxlevel =  "/api/ac/uc/userWxSkService/updatefxlevel";

    //优惠券列表
    var couproductlist = '/api/as/puc/couponproduct/findCouProductList'; 
    //优惠券下单
    var createcouorder = '/api/ac/puc/couponOrderService/createCouOrder';
    
    return {

        wxuserlist : function(){
            return $resource(wxuserlist, {}, {});
        },
    	updatelevel : function(){
            return $resource(updatelevel, {}, {});
        },
        memberlevellist : function(){
            return $resource(memberlevellist, {}, {});
        },
        levellist : function(){
            return $resource(levellist, {}, {});
        },
        save : function(){
            return $resource(save, {}, {});
        },
        couproductlist : function(){
            return $resource(couproductlist, {}, {});
        },
        createcouorder : function(){
            return $resource(createcouorder, {}, {});
        },
        fxlevellist : function(){
            return $resource(fxlevellist, {}, {});
        },
        savefx : function(){
            return $resource(savefx, {}, {});
        },
        updatefxlevel : function(){
            return $resource(updatefxlevel, {}, {});
        },
        
    };

};

module.exports = service;