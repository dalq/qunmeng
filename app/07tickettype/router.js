/**
* 子模块路由
* dlq
*/

var router = function ($urlRouterProvider, $stateProvider) {

    $stateProvider

        //票种列表
        .state('app.tickettypeList', {
            url: "/tickettype/tickettypeList:placeid?type",
            views: {
                'main@': {
                    template: require('./views/tickettypeList.html'),
                    controller: 'tickettypeList',
                }
            },
            resolve: {
                viewlist: function (tickettypeservice) {
                    return tickettypeservice.viewlist;
                },
                typepagelist: function (tickettypeservice) {
                    return tickettypeservice.typepagelist();
                },
                tktlist: function (tickettypeservice) {
                    return tickettypeservice.tktlist();
                },
                tktupdate: function (tickettypeservice) {
                    return tickettypeservice.tktupdate();
                },
                updateTicketPeriod: function (tickettypeservice) {
                    return tickettypeservice.updateTicketPeriod();
                },
                str2date: function (utilservice) {
                    return utilservice.str2date;
                }
            }
        })

        //创建票种
        .state('app.tickettypeCreate', {
            url: "/tickettype/tickettypeCreate",
            views: {
                'main@': {
                    template: require('./views/tickettypeCreate.html'),
                    controller: 'tickettypeCreate',
                }
            },
            resolve: {
                viewlist: function (tickettypeservice) {
					return tickettypeservice.slist;
				},
				tktcreate: function (tickettypeservice) {
					return tickettypeservice.tktcreate();
				},
				placeinfo: function (tickettypeservice) {
					return tickettypeservice.info();
				},
				makeArr: function (tickettypeservice) {
					return tickettypeservice.makeArr;
				},
				makeStr: function (tickettypeservice) {
					return tickettypeservice.makeStr;
				},
				getDate: function (utilservice) {
					return utilservice.getDate;
				},
				str2date: function (utilservice) {
					return utilservice.str2date;
				}
            }
        })

        //票种详情
        .state('app.tickettypeInfo', {
            url: "/tickettype/tickettypeInfo/:id",
            views: {
                'main@': {
                    template: require('./views/tickettypeInfo.html'),
                    controller: 'tickettypeInfo',
                }
            },
            resolve: {
                
            }
        })

        //票种编辑
        .state('app.tickettypeEdit', {
            url: "/tickettype/tickettypeEdit/:id/:placeid",
            views: {
                'main@': {
                    template: require('./views/tickettypeEdit.html'),
                    controller: 'tickettypeEdit',
                }
            },
            resolve: {
                tktinfo: function (tickettypeservice) {
					return tickettypeservice.tktinfo();
				},
				tktupdate: function (tickettypeservice) {
					return tickettypeservice.tktupdate();
				},
				viewlist: function (tickettypeservice) {
					return tickettypeservice.viewlist;
				},
				placeinfo: function (tickettypeservice) {
					return tickettypeservice.info();
				},
				makeArr: function (tickettypeservice) {
					return tickettypeservice.makeArr;
				},
				makeStr: function (tickettypeservice) {
					return tickettypeservice.makeStr;
				},
				getDate: function (utilservice) {
					return utilservice.getDate;
				},
				str2date: function (utilservice) {
					return utilservice.str2date;
				}
            }
        })

        //票种编辑
        .state('app.tkttypeattr', {
            url: "/tickettype/tkttypeattr",
            views: {
                'main@': {
                    template: require('./views/tkttypeattr.html'),
                    controller: 'ttkttypeattr',
                }
            },
            resolve: {
            }
        })

        //票种编辑
        .state('app.ttkttypeattrcreate', {
            url: "/tickettype/ttkttypeattrcreate",
            views: {
                'main@': {
                    template: require('./views/ttkttypeattrcreate.html'),
                    controller: 'ttkttypeattrcreate',
                }
            },
            resolve: {
            }
        })

        //票种编辑
        .state('app.ttkttypeattredit', {
            url: "/tickettype/ttkttypeattredit/:type_attr",
            views: {
                'main@': {
                    template: require('./views/ttkttypeattredit.html'),
                    controller: 'ttkttypeattredit',
                }
            },
            resolve: {
            }
        })



        // //选择票种
        // .state('app.tickettypeChoice', {
        //     url: "/tickettype/tickettypeChoice",
        //     template: require('./views/tickettypeChoice.html'),
        //     controller: 'tickettypechoice',
        //     resolve: {

        //     }
        // })



        // //弹出票种详情
        // .state('app.tickettype_info1', {
        //     url: "/tickettype/info1/:id",
        //     template : require('../996tpl/views/popform.html'),
        //     controller : 'poptickettypeinfo',
        //     resolve:{
        //         formconfig : function(formservice){
        //             return formservice.formconfig();
        //         },
        //         model : function(tickettypeservice){
        //             return tickettypeservice.model;
        //         }
        //     }
        // })



        ;

};

module.exports = router;