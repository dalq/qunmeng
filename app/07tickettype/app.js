var App = angular.module('tickettype', []);

App.config(require('./router'));
App.factory('tickettypeservice', require('./service'));

App.controller('tickettypeList',require('./controllers/tickettypeList'));
App.controller('tickettypeCreate',require('./controllers/tickettypeCreate'));
App.controller('tickettypeInfo',require('./controllers/tickettypeInfo'));
App.controller('tickettypeEdit',require('./controllers/tickettypeEdit'));

App.controller('tickettypeChoice',require('./controllers/tickettypeChoice'));
// App.controller('poptickettypeinfo',require('./controllers/poptickettypeinfo'));
App.controller('ttkttypeattr',require('./controllers/tkttypeattr'));
App.controller('ttkttypeattrcreate',require('./controllers/ttkttypeattrcreate'));
App.controller('ttkttypeattredit',require('./controllers/ttkttypeattredit'));

module.exports = App;