module.exports = function($scope, $state, $resource){

	var para = {};

	$resource('/api/as/tc/attr/list', {}, {}).save(para, function(res){

     	console.log(res);

     	if(res.errcode !== 0)
     	{
     		alert("数据获取失败");
     		return;
     	}

     	$scope.objs = res.data;
    });


	$scope.create = function(){


		$state.go('app.ttkttypeattrcreate');


	};

	$scope.edit = function(type_attr){

    	$state.go('app.ttkttypeattredit', {'type_attr' : type_attr});

    };
	

};
