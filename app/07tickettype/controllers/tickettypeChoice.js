module.exports = function($scope, data, $resource, $rootScope, $modalInstance){

	var tickettype = data.tickettype;


	console.log(tickettype);

	$scope.obj = {
		'tt' : '',
	}

	$scope.ticketypearr = data.tickettypearr == undefined ? [] : data.tickettypearr;

	var tickettypeobj = {};

	if(angular.isArray(data.tickettypearr)){
		for(var i = 0; i < data.tickettypearr.length; i++){
			var t = data.tickettypearr[i];
			tickettypeobj[t.id] = t;
		}
	}

	//票种的对象，id作为key
	var tta = {};


	$resource('/api/ac/tc/ticketType/alllist', {}, {}).get({}, function(res){
        console.log(res);
        if(res.errcode === 0){
            $scope.arr = res.data.result;
            for(var i = 0; i < $scope.arr.length; i++){
            	var tmp = $scope.arr[i];
            	tta[tmp.id] = tmp;
            }
        }else{
            alert(res.errmsg);
        }
    });

	//追加票种
	$scope.add = function(){
		var tt = $scope.obj.tt
		if(tt == ''){
			alert('请选择一个票种');
			return;
		}
		if(angular.isUndefined(tickettypeobj[tt])){
			tickettypeobj[tt] = tta[tt];
			$scope.ticketypearr.push(tta[tt]);
		}
	};

	//删除票种
	$scope.del = function(item, index){

		console.log('del'+item);

		delete tickettypeobj[item.id];
		$scope.ticketypearr.splice(index, 1);

	}

	//票种详情
	$scope.info = function(item){

		console.log('info'+item);

	}




	$scope.ok = function () {  
		// console.log($scope.res);
		// console.log($scope.checkres);

		// var result = {
		// 	'lock' : {},
		// 	'unlock' : '#',
		// }

		// angular.forEach($scope.checkres, function(value, key){
		// 	if(value === 'yes')
		// 	{
		// 		var v = $scope.res[key];
		// 		if(angular.isUndefined(v) || v == ''){
		// 			result.unlock += key + '#';
		// 		} else {
		// 			result.lock[key] = v;
		// 		}
		// 	}
		// });


		var result = {
			'str' : '',
			'arr' : $scope.ticketypearr,
		};

		if($scope.ticketypearr.length !== 0){
			result.str = '#';
			for(var i = 0; i < $scope.ticketypearr.length; i++){
				var t = $scope.ticketypearr[i];
				result.str += t.id + '#';
			}
		}

        $modalInstance.close(result);  
    };  
    $scope.cancel = function () {  
        $modalInstance.dismiss('cancel');  
    }; 
};