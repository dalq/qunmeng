/**
 * 子模块路由
 * dlq
 */

var router = function($urlRouterProvider, $stateProvider){

 	$stateProvider


      .state('app.ticketinput', {
         url: '/ticketinput',
        views: {
            'main@' : {
                template : require('./views/input.html'),
                controller : 'check',
            }
        },
       
        resolve : {
        	checkcode:  function(ticketservice){
	     		  return ticketservice.checkcode();
	       	},
			   checkcard:  function(ticketservice){
	            return ticketservice.checkcard();
	        },
	        checkid:  function(ticketservice){
	            return ticketservice.checkid();
	        },
	        checkgroupcode:  function(ticketservice){
	            return ticketservice.checkgroupcode();
	        },
	        useticketbyid:  function(ticketservice){
	            return ticketservice.useticketbyid();
	        },
	        useticketbycode:  function(ticketservice){
	            return ticketservice.useticketbycode();
	        },
	        useticketbycard:  function(ticketservice){
	            return ticketservice.useticketbycard();
	        },
	        useticketbygroupcode : function(ticketservice){
	        	return ticketservice.useticketbygroupcode();
	        },
	        devicenamelist : function(ticketservice){
	        	return ticketservice.devicenamelist();
	        },
            list : function(ticketservice){
                return ticketservice.sslist;
            },
            devicelist : function(ticketservice){
            return ticketservice.devicelist();
            }
	        
        }
      })


    .state('app.bussinessticketinput', {
         url: '/bussinessticketinput',
        views: {
            'main@' : {
                template : require('./views/input.html'),
                controller : 'check',
            }
        },
       
        resolve : {
        	checkcode:  function(ticketservice){
	     		  return ticketservice.checkcode();
	       	},
			   checkcard:  function(ticketservice){
	            return ticketservice.checkcard();
	        },
	        checkid:  function(ticketservice){
	            return ticketservice.checkid();
	        },
	        checkgroupcode:  function(ticketservice){
	            return ticketservice.checkgroupcode();
	        },
	        useticketbyid:  function(ticketservice){
	            return ticketservice.useticketbyid();
	        },
	        useticketbycode:  function(ticketservice){
	            return ticketservice.useticketbycode();
	        },
	        useticketbycard:  function(ticketservice){
	            return ticketservice.useticketbycard();
	        },
	        useticketbygroupcode : function(ticketservice){
	        	return ticketservice.useticketbygroupcode();
	        },
	        devicenamelist : function(ticketservice){
	        	return ticketservice.devicenamelist();
	        },
            list : function(ticketservice){
                return ticketservice.nbussinesslist;
            },
            devicelist : function(ticketservice){
            return ticketservice.devicelist();
            }
	        
        }
    })



	.state('app.ticketlogin', {
        url: '/ticketlogin',
       views: {
            'main@' : {
					controller : 'login',
					template: require('./views/login.html'),
			}
	   },
        resolve : {
        	login:  function(ticketservice){
	     		return ticketservice.login();
	     	}
        }
      })

	  .state('app.refundticket', {
        url: '/refundticket',
         views: {
            'main@' : {
				controller : 'refundticket',
				template: require('./views/refundticket.html')
			}
		 }
      })
	   .state('app.testtest', {
        url: '/testtest',
         views: {
            'main@' : {
					controller : 'testtest',
					template: require('./views/testtest.html'),
			}
		 },
        resolve : {
         	createImportExcel:  function(ticketservice){
	     	 	return ticketservice.createImportExcel();
	     	 }
        }
      })
	  .state('app.deviceorder', {
        url: '/deviceorder',
       views: {
            'main@' : {
					controller : 'deviceorder',
					template: require('./views/deviceorder.html')
			}
	   }
      })



};

module.exports = router;