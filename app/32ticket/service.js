/**
 * 子模块service
 * dlq
 */
var service = function($resource,$q, $http){

	var url =  "/tktapi/sc";

    var checkid = url + "/queryService/byID";

    var checkcard = url + "/queryService/byCard";

    var checkcode = url + "/queryService/byCode";

    var checkgroupcode = url + "/queryService/byGroupCode";

   	var useticketbyid = url + "/destoryService/updateByID";

   	var useticketbycode = url + "/destoryService/updateByCode";

   	var useticketbycard = url + "/destoryService/updateByCard";

   	var useticketbygroupcode = url + "/destoryService/updateByGroupCode";

   	var login = url + "/queryService/login";

    //var list = "/api/as/tc/placeview/list";

    //获取对应景区设备列表
    var devicelist =  '/api/as/tc/device/watchdevicelist';

    //var devicelist =  '/api/as/tc/device/watchdevicelist';

    var devicenamelist =  '/api/as/tc/device/devicenamelist';

   	 var list = "/api/as/tc/placeview/list";
      //带景区判断的
    var sslist =  "/api/as/tc/placeview/jjlist";

    //当前机构下的商家列表（不分页）
    var nbussinesslist = '/api/as/wc/productbussiness/nbussinesslist';

    //var createImportExcel = "/api/uc/cdc/excelUtilService/createImportExcel";
    var createImportExcel = "http://localhost:8088/HelloWord/servlet/ServletDemo1";
    
    return {

    	checkid : function(){
            return $resource(checkid, {}, {});
        },
        checkcard : function(){
            return $resource(checkcard, {}, {});
        },
        checkcode : function(){
            return $resource(checkcode, {}, {});
        },
        checkgroupcode : function(){
            return $resource(checkgroupcode, {}, {});
        },
        useticketbyid : function(){
        	return $resource(useticketbyid, {}, {});
        },
        useticketbycode : function(){
        	return $resource(useticketbycode, {}, {});
        },
        useticketbycard : function(){
        	return $resource(useticketbycard, {}, {});
        },
        useticketbygroupcode : function(){
        	return $resource(useticketbygroupcode, {}, {});
        },
        login : function(){
            return $resource(login, {}, {});
        },
        // list : function(){
        //     return $resource(list, {}, {});
        // },
         devicelist : function(){
            return $resource(devicelist, {}, {});
        },
          devicenamelist : function(){
            return $resource(devicenamelist, {}, {});
        },
           list : function(){
            return $resource(list, {}, {});
        },
        createImportExcel : function(){
            return $resource(createImportExcel, {}, {});
        },
         sslist : function(obj){
            var deferred = $q.defer(); // 声明延后执行，表示要去监控后面的执行  
            $http({method: 'GET', params: obj, url: sslist}).then(
                function(data){
                    deferred.resolve(data.data);
                },
                function(data){
                    deferred.reject(data.data);
                });
            // success(function(data, status, headers, config) {  
            //     deferred.resolve(data);  // 声明执行成功，即http请求数据成功，可以返回数据了  
            // }).  
            // error(function(data, status, headers, config) {  
            //     deferred.reject(data);   // 声明执行失败，即服务器返回错误  
            // });  
            return deferred.promise;   // 返回承诺，这里并不是最终数据，而是访问最终数据的API 
        },

         nbussinesslist : function(obj){
            var deferred = $q.defer(); // 声明延后执行，表示要去监控后面的执行  
            $http({method: 'GET', params: obj, url: nbussinesslist}).then(
                function(data){
                    deferred.resolve(data.data);
                },
                function(data){
                    deferred.reject(data.data);
                });
            // success(function(data, status, headers, config) {  
            //     deferred.resolve(data);  // 声明执行成功，即http请求数据成功，可以返回数据了  
            // }).  
            // error(function(data, status, headers, config) {  
            //     deferred.reject(data);   // 声明执行失败，即服务器返回错误  
            // });  
            return deferred.promise;   // 返回承诺，这里并不是最终数据，而是访问最终数据的API 
        }

      
    };

};

module.exports = service;