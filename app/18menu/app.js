var App = angular.module('menu', []);

App.config(require('./router'));

//service
App.factory('menuservice', require('./service'));

//Controllers
App.controller('menuList', require('./controllers/menulist'));
App.controller('setInterface', require('./controllers/setInterface'));
App.controller('setIcon',require('./controllers/seticon'));

//Directive
App.directive('menuloadover',require('./directives/menuloadover'));
App.directive('setIcon',require('./directives/seticon'));

module.exports = App;