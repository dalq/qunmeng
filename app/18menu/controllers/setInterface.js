/**
 * 模态框
 */
module.exports = function($scope, $state, $modalInstance, $resource, items, iface, toaster){

    
    init();
    //初始化接口信息
    function init(){
        $scope.interface_list = [];
        for(var i = 0; i < iface.length; i++){
            iface[i].red = 1;
        }
        $scope.interface_sel = iface;
        $resource('/api/as/sc/dict/getDictInfoList', {}, {}).get({'type': 'sys_center_type'}, function(res){
			if(res.errcode === 0){
                $scope.center_list = res.data;
			} else {
				toaster.error({title: '', body: '加载搜索信息失败'});
			}
        });
    } 

    //选择
    $scope.search = function(item){
		$resource('/api/as/sc/menu/getForMenuList', {}, {}).save({'center_code': item}, function (res) {
            if (res.errcode === 0) {
                $scope.interface_list = res.data;
                for(var i = 0; i < $scope.interface_sel.length; i++){
                    var id = $scope.interface_sel[i].id;
                    if(id.indexOf(item) == 0){
                        for(var j = 0; j < $scope.interface_list.length; j++){
                            if(id == $scope.interface_list[j].id){
                                $scope.interface_list[j].exist = 1;
                                break;
                            }
                        }
                    }
                }
            } else {
                toaster.error({title: '', body: '加载搜索信息失败'});
            }
        });
    };

    //确定
    $scope.ok = function(){
        $modalInstance.close($scope.interface_sel);
    };

    //取消
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

    //勾选
    $scope.select = function(item){
		item.exist = 1;
        $scope.interface_sel.push(item);
    };

    //取消勾选
    $scope.unselect = function(index){
        var item = $scope.interface_sel[index];
        if(item.red == 1){
            for(var i = 0; i < $scope.interface_list.length; i++){
                if(item.id == $scope.interface_list[i].id){
                    $scope.interface_list[i].exist = 0;
                    break;
                }
            }
        } else {
            //查找候选接口列表中的位置,并使其显示
            var site = $scope.interface_list.indexOf(item);
            $scope.interface_list[site].exist = 0;
        }
        $scope.interface_sel.splice(index,1);
    };

};