module.exports = function($scope, $modal, menuInfo, transData, menuSave, menuList, menuOneList, deleteMenu, funInMenu){

	$scope.obj = {};
	$scope.tabname = {'name': '添加菜单', 'active': false};
	$scope.status = {'active': true };
	$scope.iface = [];
	$scope.bak_parent_ids = '';

	
	//取消
    $scope.cancel = function(){
		$scope.tabname = { 'name': '添加菜单', 'active': false };
		$scope.status.active = true;
		$scope.iface = [];
		$scope.obj = {};
    };

	init();
	//初始化菜单树
	function init(){
		$scope.cancel();
		$("#treetable").hide();
		menuList.save({'parent_ids' : '0,1'}, function(res){
			if(res.errcode !== 0){
				alert(res.errmsg);
				return ;
			}
			$scope.dataForTheTree = res.data;
		});
		menuOneList.save({}, function(res){
			if(res.errcode === 0){
				$scope.menu_list = res.data;
			}else{
				alert(err.errmsg);
			}
		});
	}

	//修改
    $scope.update = function(item){
		$scope.tabname = { 'name': '修改菜单', 'active': true };
		menuInfo.save(item, function(res){
			if(res.errcode === 0){
				$scope.obj = res.data;
				$scope.bak_parent_id = $scope.obj.parent_id;
			}else{
				alert(err.errmsg);
			}
		});
		$scope.iface = [];
		if(item.permission){
			funInMenu.save(item, function(res){
				if(res.errcode === 0){
					$scope.iface = res.data;
				}else{
					alert(res.errmsg);
				}
			});
		}
    };

	//添加
    $scope.add = function(item){
		if($scope.tabname.name == '修改菜单'){
			return;
		}
		$scope.tabname.active = true;
		$scope.obj = {};
		var sort = '';
		if(item == 1){
			$scope.obj.parent_id = '1';
			var ttt = $("#treetable").find("tr[pid='0']");
			sort = $(ttt.get(ttt.length-2)).attr("sort");
		} else {
			$scope.obj.parent_id = item.id;
			sort = $("#treetable").find("tr[pid="+item.id+"]:last").attr("sort");
		}
		$scope.obj.is_show = '1';
		sort = sort || '0';
		$scope.iface = [];
		$scope.bak_parent_id = '';
		$scope.obj.sort = parseInt(sort)+30;
    };

	//删除菜单的接口
    $scope.remove = function(index){
		$scope.iface.splice(index,1);
    };

	//删除
    $scope.delete = function(item, index){
		if(confirm('确认要删除此菜单吗？')==true){
			deleteMenu.save({'id': item.id}, function(res){
				if(res.errcode === 0){
					getIndex(item.id);
					var sum = 1;
					if($scope.last != -1){
						sum = $scope.last - index;
					}
					$scope.dataForTheTree.splice(index, sum);
				}else{
					alert(err.errmsg);
				}
			});
		}
    };

	//保存
    $scope.save = function(){
		var a = [];
		for(var i = 0; i < $scope.iface.length; i++){
			a.push($scope.iface[i].id);
		}
		if($scope.bak_parent_id != $scope.obj.parent_id){
			//修改父节点
			$scope.obj.parent_ids = $scope.bak_parent_ids + $scope.obj.parent_id + ',';
		} else {
			//父节点无修改,令parent_id=''作为标记
			$scope.obj.parent_id = '';
		}
		$scope.obj.permission = a.toString();
		menuSave.save($scope.obj, function(res){
			if(res.errcode === 0){
				init();
			} else if(res.errcode === 1106) {
				alert('保存菜单信息重复');
			} else {
				alert(err.errmsg);
			}
		});
    };

	$scope.getPid = function(pid){
		$scope.bak_parent_ids = pid.parent_ids;
	}

	//获取tr索引
	function getIndex(id){
		var tr = $("#treetable").find("tr[pid="+id+"]:last");
		var id = tr.attr("id");
		if(tr.attr("haschild")){
			getIndex(id);
		} else {
			$scope.last = $('#'+id).index('#treetable tr');
		}
	} 

	//添加接口
    $scope.setInterface = function(){
		var modalInstance = $modal.open({
			template: require('../views/setInterface.html'),
			controller: 'setInterface',
			size: 'lg',
			resolve: {
				items: function () {
					return $scope.obj;
				},
				iface: function () {
					return $scope.iface;
				}
			}
		});
		//关闭模态框刷新页面
		modalInstance.result.then(function(result) {
			$scope.iface = result;
		});
    };

	//设置图标
    $scope.setIcon= function(){
		var modalInstance = $modal.open({
			template: require('../views/ui_icons.html'),
			controller: 'setIcon',
			size: 'lg',
			resolve: {
				
			}
		});
		//关闭模态框刷新页面
		modalInstance.result.then(function(result) {
			console.log(result);
		});
    };

};