/**
 * 子模块service
 * DHpai
 */
var service = function($resource, $q, $state, $modal){
    
    //菜单列表
    var menuList = '/api/ac/sc/menuService/getMenuList';
    //菜单简表
    var menuOneList = '/api/as/sc/menu/getOneList';
    //菜单详情
    var menuInfo = '/api/as/sc/menu/getById';
    //保存菜单
    var menuSave = '/api/ac/sc/menuService/updateMenu';
    //删除菜单
    var deleteMenu = '/api/ac/sc/menuService/deleteMenu';
    //菜单内接口
    var funInMenu = '/api/ac/sc/menuService/getInForMenu';


    return {
        menuList : function(){
            return $resource(menuList, {}, {});
        },
        menuOneList : function(){
            return $resource(menuOneList, {}, {});
        },
        menuInfo : function(){
            return $resource(menuInfo, {}, {});
        },
        menuSave : function(){
            return $resource(menuSave, {}, {});
        },
        deleteMenu : function(){
            return $resource(deleteMenu, {}, {});
        },
        funInMenu : function(){
            return $resource(funInMenu, {}, {});
        }
    };

};

module.exports = service;