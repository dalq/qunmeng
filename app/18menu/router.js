 /**
 * 子模块路由
 * DHpai
 */

var router = function($urlRouterProvider, $stateProvider){


 	$stateProvider

   //菜单列表
    .state('app.menuList', {
        url: "/menu/menulist",
        views: {
            'main@' : {
                template : require('./views/menu.html'),
                controller : 'menuList',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
            menuInfo : function(menuservice){
                return menuservice.menuInfo();
            },
            transData : function(utilservice){
                return utilservice.transData;
            },
            menuSave : function(menuservice){
                return menuservice.menuSave();
            },
            menuList : function(menuservice){
                return menuservice.menuList();
            },
            menuOneList : function(menuservice){
                return menuservice.menuOneList();
            },
            deleteMenu : function(menuservice){
                return menuservice.deleteMenu();
            },
            funInMenu : function(menuservice){
                return menuservice.funInMenu();
            }
        }
    });

};

module.exports = router;