var App = angular.module('linestatistics', []);

App.config(require('./router'));
App.factory('linestatisticsservice', require('./service'));

App.controller('order_statistics',require('./controllers/order_statistics'));
App.controller('orderinfo',require('./controllers/orderinfo'));

App.controller('product_statistics',require('./controllers/product_statistics'));

App.controller('productinfo',require('./controllers/productinfo'));

App.controller('tostatistics',require('./controllers/tostatistics'));
App.controller('tostatisticsall',require('./controllers/tostatisticsall'));
module.exports = App;