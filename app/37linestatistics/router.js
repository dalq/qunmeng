/**
 * 子模块路由
 * dlq
 */

var router = function($urlRouterProvider, $stateProvider){

    $stateProvider
        //订单查询
      .state('app.order_statistics', {
        url: '/order_statistics/:type',
        views: {
            'main@' : {
                controller : 'order_statistics',
                template: require('./views/order_statistics.html'),
            }
        },
        resolve:{
            // namelist : function(linestatisticsservice){
            //     return linestatisticsservice.namelist();
            // },
        }
        
      })
      //产品统计
      .state('app.product_statistics', {
        url: '/product_statistics/:type',
        views: {
            'main@' : {
                controller : 'product_statistics',
                template: require('./views/product_statistics.html'),
            }
        },
        resolve:{
            // namelist : function(linestatisticsservice){
            //     return linestatisticsservice.namelist();
            // },
        }
        
      })

     

};

module.exports = router;