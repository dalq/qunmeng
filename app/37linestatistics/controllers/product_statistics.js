/**
 * 模态框
 */
module.exports = function ($scope, $state, $stateParams,$resource,$modal) {
	/* 分页
     * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.currentPage = 1;      //当前页码
    $scope.itemsPerPage = 10         //每页显示几条
	$scope.obj={};
	$scope.d={};


	$scope.pageChanged = function () {
       var para = {
        //    ta:1,
            pageNo:$scope.currentPage,
            pageSize:$scope.itemsPerPage,
            // product_id : $scope.id,
            // product_name : $scope.product_name,
            // product_type : $scope.product_type,
            // supplier_product_name : $scope.supplier_product_name,
            // start_city_arr : $scope.start_city_arr,
            // product_state : $scope.product_state,
            
        };
        		// console.log(para);

        $resource('/api/ac/lc/lineStatisticsListService/findInfoList', {}, {}).
			save(para, function(res){
				// console.log('购买须知');
                 
                if(res.errcode === 0 || res.errcode === 10003){
					$scope.a=res.data.results;
					console.log($scope.a);
					$scope.totalItems = res.data.totalRecord;
					
					

                }else{
                    alert(res.errmsg);
                }
            });

    };
	$scope.pageChanged();

			$scope.b= '0';
			$scope.c = '';
			$scope.show = function(index){				
				if($scope.b==0){
					$scope.b='1',
					$scope.c=index
				}else if($scope.b==1){
					if($scope.c!=index){
						$scope.c=index
					}else{
						$scope.b='0',
						$scope.c=index
					}	
				}		
			};
			


			$scope.search=function(){
			var dic = {
				pageNo:$scope.currentPage,
            	pageSize:$scope.itemsPerPage,
				product_id : $scope.id,
				product_name : $scope.product_name,
				product_type : $scope.d.product_type,
				supplier_product_name : $scope.supplier_product_name,
				start_city_arr : $scope.start_city_arr,
                product_state : $scope.d.product_state,
			}
				console.log(dic);
				$resource('/api/ac/lc/lineStatisticsListService/findInfoList', {}, {}).save(dic, function(res){
                if(res.errcode === 0 || res.errcode === 10003){
                    // console.log(res);
					$scope.a=res.data.results;
					console.log($scope.a);
                    $scope.totalItems = res.data.totalRecord;
                }else{
                    alert(res.errmsg);
                }
            });
			}

			$scope.ptarr=[
				{name:'请选择',value:''},
                {name:'周边游',value:'0'},
                {name:'国内长线',value:'1'},
                {name:'当地',value:'2'},
                {name:'处境当地参团',value:'3'},
                {name:'出境短线',value:'4'},
                {name:'出境长线',value:'5'}          
            ]

			$scope.psarr=[
				{name:'请选择',value:''},
                {name:'草稿',value:'0'},
                {name:'正在审核',value:'1'},
                {name:'已上架',value:'2'},
                {name:'已下架',value:'3'},
                {name:'驳回',value:'4'} 
            ]

             $scope.toinfo = function (index) {
				$scope.item = $scope.a[index];
				var modalInstance = $modal.open({
					template: require('../views/productinfo.html'),
					controller: 'productinfo',
					size: 'lg',
					resolve: {
						items: function () {
							return $scope.item;
						}
					}
				});
				modalInstance.result.then(function (showResult) {	
						$scope.pageChanged();
				});
			}

			$scope.toinfo2 = function (index,index2) {
				$scope.item = $scope.a[index].codeInfoList[index2];
				var modalInstance = $modal.open({
					template: require('../views/productinfo.html'),
					controller: 'productinfo',
					size: 'lg',
					resolve: {
						items: function () {
							return $scope.item;
						}
					}
				});
				modalInstance.result.then(function (showResult) {	
						$scope.pageChanged();
				});
			}

			$scope.tostatistics = function (index) {
				$scope.item = $scope.a[index];
				var modalInstance = $modal.open({
					template: require('../views/tostatistics.html'),
					controller: 'tostatistics',
					size: 'lg',
					resolve: {
						items: function () {
							return $scope.item;
						}
					}
				});
				modalInstance.result.then(function (showResult) {	
						$scope.pageChanged();
				});
			}

			$scope.tostatisticsall = function () {
				$scope.item = $scope.a;
				var modalInstance = $modal.open({
					template: require('../views/tostatisticsall.html'),
					controller: 'tostatisticsall',
					size: 'lg',
					resolve: {
						items: function () {
							return $scope.item;
						}
					}
				});
				modalInstance.result.then(function (showResult) {	
						$scope.pageChanged();
				});
			}

};

