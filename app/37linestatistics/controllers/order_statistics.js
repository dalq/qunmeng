/**
 * 模态框
 */
module.exports = function ($scope, $state, $stateParams,$resource,$modal) {
	/* 分页
     * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.currentPage = 1;      //当前页码
    $scope.itemsPerPage = 10         //每页显示几条


			$scope.search=function(){
                if($scope.tourist_mobile!=undefined&&$scope.tourist_mobile!=''){
                    if(!/^1[34578]\d{9}$/.test($scope.tourist_mobile)) {
                        alert('请输入正确的手机号码');
                        return;
                    }
                }
                
                
				if(($scope.tourist_name==undefined||$scope.tourist_name=='')&&($scope.tourist_mobile==undefined||$scope.tourist_mobile=='')&&($scope.tourist_cardno==undefined||$scope.tourist_cardno=='')){
                    alert('请在输入框内输入搜索条件');
                    $scope.a=[];
                    return;
                };                             
			var dic = {
                pageNo:$scope.currentPage, 
                pageSize:$scope.itemsPerPage,				
				name : $scope.tourist_name,
                mobile : $scope.tourist_mobile,
                cardno : $scope.tourist_cardno,
				
			}
                console.log($scope.tourist_name,$scope.tourist_tel,$scope.tourist_cardno);
				$resource('/api/ac/lc/lineStatisticsListService/findOrderByInfoList', {}, {}).save(dic, function(res){
                if(res.errcode === 0 || res.errcode === 10003){
					$scope.a=res.data.results;
                	if($scope.a.length==0){
                        alert('查询不到与之匹配的结果');
                    }
                }else{
                    alert(res.errmsg);
                }
            });
			}
            $scope.toinfo = function (index) {
                $scope.item = $scope.a[index];
                var modalInstance = $modal.open({
                    template: require('../views/orderinfo.html'),
                    controller: 'orderinfo',
                    size: 'lg',
                    resolve: {
                        items: function () {
                            return $scope.item;
                        }
                    }
                });
                modalInstance.result.then(function (showResult) {	
                        $scope.pageChanged();
                });
                
            }


};
