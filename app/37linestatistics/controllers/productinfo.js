/**
 * 模态框
 */
module.exports = function($scope, $state, $stateParams, $modalInstance, items, $resource,$modal){
    
    console.log(items);
    
    $resource('/api/ac/lc/lineStatisticsListService/findSupplierProductInfo', {}, {}).
			get({product_id : items.id}, function(res){
				// console.log('购买须知');
                // console.log(res);
                if(res.errcode === 0 || res.errcode === 10003){
					$scope.obj=res.data;
                    $scope.qitaarr=[];
                    $scope.qita='';
                    $scope.str = '';
                    $scope.baoxian='';
                    $scope.smallstr = '';
                    $scope.sleepstr = '';
                    $scope.eatstr = '';
                    $scope.orderstr = '';
                    $scope.cautionstr = '';
                    $scope.addstr = '';
                    $scope.importantstr = '';
                    $scope.cautionsleepstr = '';
                    $scope.speelstr = '';
                    $scope.paystr = '';
                    $scope.peoplestr = '';
                    $scope.days = angular.fromJson(res.data.trip_info_json);
                    $scope.coursestr = '';
                    $scope.coursestrinfo = '';
                    $scope.biaotiarr= [];
                    $scope.secondarr = [];
                    $scope.shuzu = [];
                    $scope.t0 = '';
                    $scope.t1 = '';
                    $scope.t2 = '';
                    $scope.t3 = '';
                    $scope.t4 = '';
                    $scope.t5 = '';
                    $scope.t6 = '';
                    $scope.t7 = '';
                    $scope.gentuan = '';
                    $scope.peoplemoney = '';
                    $scope.childmoney = '';
                    $scope.housemoney = '';
                    $scope.shijian = '';
                    $scope.peoplemoneyarr = [];
                    $scope.childmoneyarr = [];
                    $scope.housemoneyarr = [];
                    $scope.shijianarr = [];
                     $scope.firststr = '';
                    $scope.secondstr = '';
                    $scope.thirdstr = '';
                    $scope.numarr = [];
                    $scope.allarr = [];
                    $scope.imgarr=[];
                    $scope.activity = angular.fromJson(res.data.title_class_json);
                    $scope.traffic = angular.fromJson(res.data.roundtrafficInfoList);
                    $scope.trafficstr = '';
                    $scope.trafficarr = [];    
                    $scope.huochestr = ''; 
                    $scope.feijistr = '';  
                     $scope.jiaotongnum='';

                     if($scope.obj.cancel_order_time==30){
                         $scope.obj.cancel_order_time='30分钟'
                     }else if($scope.obj.cancel_order_time==40){
                         $scope.obj.cancel_order_time='40分钟'
                     } else if($scope.obj.cancel_order_time==50){
                         $scope.obj.cancel_order_time='50分钟'
                     } else if($scope.obj.cancel_order_time==60){
                         $scope.obj.cancel_order_time='60分钟'
                     } else if($scope.obj.cancel_order_time==120){
                         $scope.obj.cancel_order_time='2小时'
                     } else if($scope.obj.cancel_order_time==240){
                         $scope.obj.cancel_order_time='4小时'
                     } else if($scope.obj.cancel_order_time==480){
                         $scope.obj.cancel_order_time='8小时'
                     } else if($scope.obj.cancel_order_time==960){
                         $scope.obj.cancel_order_time='16小时'
                     } else if($scope.obj.cancel_order_time==1920){
                         $scope.obj.cancel_order_time='24小时'
                     } else if($scope.obj.cancel_order_time==3840){
                         $scope.obj.cancel_order_time='2天'
                     } else if($scope.obj.cancel_order_time==5760){
                         $scope.obj.cancel_order_time='3天'
                     }   
                    //  console.log($scope.days.length)  ; 
                    if($scope.days){
                        // console.log('12313123');
                        
                        for(var x=0; x<$scope.days.length;x++){
                            // console.log($scope.days[x]);
                            $scope.secondarr = [];
                            $scope.coursestrinfo = '';
                            
                            $scope.imgarr.push([[]]);
                            for(var y=0;y<$scope.days[x].trip.length;y++){
                                if($scope.days[x].trip[y].imgstr){
                                    $scope.imgarr[x][y]=$scope.days[x].trip[y].imgstr;   
                                }                                                  
                            };
                            
                            if($scope.days[x].title.by=='0'){
                                $scope.coursestrinfo+='第'+(x+1)+'天:'+$scope.days[x].title.start+$scope.days[x].title.end;
                            for(var y=0; y<$scope.days[x].trip.length;y++){
                                if(!$scope.days[x].trip[y].info){
                                    $scope.days[x].trip[y].info='';
                                }
                                    $scope.t0 = '';
                                    $scope.t1 = '';
                                    $scope.t2 = '';
                                    $scope.t3 = '';
                                    $scope.t4 = '';
                                    $scope.t5 = '';
                                    $scope.t6 = '';
                                    $scope.t7 = '';
                                    if($scope.days[x].trip[y].id=='t0'){
                                        if($scope.days[x].trip[y].by=='1'){
                                            $scope.t0+='交通：从'+$scope.days[x].trip[y].start+'坐飞机到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        }else if($scope.days[x].trip[y].by=='2'){
                                            $scope.t0+='交通：从'+$scope.days[x].trip[y].start+'坐火车到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        }else if($scope.days[x].trip[y].by=='3'){
                                            $scope.t0='交通：从'+$scope.days[x].trip[y].start+'坐大客到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        }else if($scope.days[x].trip[y].by=='4'){
                                            $scope.t0+='交通：从'+$scope.days[x].trip[y].start+'坐轮船到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        }else if($scope.days[x].trip[y].by==''){
                                            $scope.t0+='交通：从'+$scope.days[x].trip[y].start+'到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        }
                                    
                                        $scope.secondarr.push($scope.t0);
                                        
                                    }else if($scope.days[x].trip[y].id=='t1'){
                                            $scope.t1+='景点：'+$scope.days[x].trip[y].start+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                            $scope.secondarr.push($scope.t1);
                                    }else if($scope.days[x].trip[y].id=='t2'){
                                        $scope.t2+='住宿：在'+$scope.days[x].trip[y].start+'住宿。'+$scope.days[x].trip[y].info;
                                        $scope.secondarr.push($scope.t2);
                                    }else if($scope.days[x].trip[y].id=='t3'){
                                        $scope.t3+='用餐：在'+$scope.days[x].trip[y].start+'用餐，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        $scope.secondarr.push($scope.t3);
                                    }else if($scope.days[x].trip[y].id=='t4'){
                                        $scope.t4+='购物：在'+$scope.days[x].trip[y].start+'购物，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        $scope.secondarr.push($scope.t4);
                                    }else if($scope.days[x].trip[y].id=='t5'){
                                        $scope.coursestr='第'+(x+1)+'天:'+$scope.days.start+$scope.days.end;
                                        $scope.t5+='活动：在'+$scope.days[x].trip[y].start+'活动，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        $scope.secondarr.push($scope.t5);
                                    }else if($scope.days[x].trip[y].id=='t6'){
                                        $scope.coursestr='第'+(x+1)+'天:'+$scope.days.start+$scope.days.end;
                                        $scope.t6+='推荐：在'+$scope.days[x].trip[y].start+'比较好，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        $scope.secondarr.push($scope.t6);
                                    }else if($scope.days[x].trip[y].id=='t7'){
                                        $scope.coursestr='第'+(x+1)+'天:'+$scope.days.start+$scope.days.end;
                                        $scope.t7+='温馨提示:'+$scope.days[x].trip[y].start+$scope.days[x].trip[y].info;
                                        $scope.secondarr.push($scope.t7);
                                    }
                                }
                                
                            }else if($scope.days[x].title.by=='1'){
                                $scope.coursestrinfo+='第'+(x+1)+'天:'+$scope.days[x].title.start+'坐飞机到'+$scope.days[x].title.end;
                                for(var y=0; y<$scope.days[x].trip.length;y++){
                                    $scope.t0 = '';
                                    $scope.t1 = '';
                                    $scope.t2 = '';
                                    $scope.t3 = '';
                                    $scope.t4 = '';
                                    $scope.t5 = '';
                                    $scope.t6 = '';
                                    $scope.t7 = '';
                                    if(!$scope.days[x].trip[y].info){
                                    $scope.days[x].trip[y].info='';
                                }
                                    if($scope.days[x].trip[y].id=='t0'){
                                        if($scope.days[x].trip[y].by=='1'){
                                            $scope.t0+='交通：从'+$scope.days[x].trip[y].start+'坐飞机到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        }else if($scope.days[x].trip[y].by=='2'){
                                            $scope.t0+='交通：从'+$scope.days[x].trip[y].start+'坐火车到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        }else if($scope.days[x].trip[y].by=='3'){
                                            $scope.t0='交通：从'+$scope.days[x].trip[y].start+'坐大客到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        }else if($scope.days[x].trip[y].by=='4'){
                                            $scope.t0+='交通：从'+$scope.days[x].trip[y].start+'坐轮船到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        }else if($scope.days[x].trip[y].by==''){
                                            $scope.t0+='交通：从'+$scope.days[x].trip[y].start+'到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        }
                                        $scope.secondarr.push($scope.t0);
                                    }else if($scope.days[x].trip[y].id=='t1'){
                                            $scope.t1+='景点：'+$scope.days[x].trip[y].start+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                            $scope.secondarr.push($scope.t1);
                                    }else if($scope.days[x].trip[y].id=='t2'){
                                        $scope.t2+='住宿：在'+$scope.days[x].trip[y].start+'住宿。'+$scope.days[x].trip[y].info;
                                        $scope.secondarr.push($scope.t2);
                                    }else if($scope.days[x].trip[y].id=='t3'){
                                        $scope.t3+='用餐：在'+$scope.days[x].trip[y].start+'用餐，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        $scope.secondarr.push($scope.t3);
                                    }else if($scope.days[x].trip[y].id=='t4'){
                                        $scope.t4+='购物：在'+$scope.days[x].trip[y].start+'购物，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        $scope.secondarr.push($scope.t4);
                                    }else if($scope.days[x].trip[y].id=='t5'){
                                        $scope.coursestr='第'+(x+1)+'天:'+$scope.days.start+$scope.days.end;
                                        $scope.t5+='活动：在'+$scope.days[x].trip[y].start+'活动，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        $scope.secondarr.push($scope.t5);
                                    }else if($scope.days[x].trip[y].id=='t6'){
                                        $scope.coursestr='第'+(x+1)+'天:'+$scope.days.start+$scope.days.end;
                                        $scope.t6+='推荐：在'+$scope.days[x].trip[y].start+'比较好，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        $scope.secondarr.push($scope.t6);
                                    }else if($scope.days[x].trip[y].id=='t7'){
                                        $scope.coursestr='第'+(x+1)+'天:'+$scope.days.start+$scope.days.end;
                                        $scope.t7+='温馨提示:'+$scope.days[x].trip[y].start+$scope.days[x].trip[y].info;
                                        $scope.secondarr.push($scope.t7);
                                    }
                                }
                            }else if($scope.days[x].title.by=='2'){
                                $scope.coursestrinfo+='第'+(x+1)+'天:'+$scope.days[x].title.start+'坐火车到'+$scope.days[x].title.end;
                            for(var y=0; y<$scope.days[x].trip.length;y++){
                                $scope.t0 = '';
                                $scope.t1 = '';
                                $scope.t2 = '';
                                $scope.t3 = '';
                                $scope.t4 = '';
                                $scope.t5 = '';
                                $scope.t6 = '';
                                $scope.t7 = '';
                                    if(!$scope.days[x].trip[y].info){
                                    $scope.days[x].trip[y].info='';
                                }
                                    if($scope.days[x].trip[y].id=='t0'){
                                        if($scope.days[x].trip[y].by=='1'){
                                            $scope.t0+='交通：从'+$scope.days[x].trip[y].start+'坐飞机到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        }else if($scope.days[x].trip[y].by=='2'){
                                            $scope.t0+='交通：从'+$scope.days[x].trip[y].start+'坐火车到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        }else if($scope.days[x].trip[y].by=='3'){
                                            $scope.t0='交通：从'+$scope.days[x].trip[y].start+'坐大客到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        }else if($scope.days[x].trip[y].by=='4'){
                                            $scope.t0+='交通：从'+$scope.days[x].trip[y].start+'坐轮船到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        }else if($scope.days[x].trip[y].by==''){
                                            $scope.t0+='交通：从'+$scope.days[x].trip[y].start+'到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        }
                                        $scope.secondarr.push($scope.t0);
                                    }else if($scope.days[x].trip[y].id=='t1'){
                                            $scope.t1+='景点：'+$scope.days[x].trip[y].start+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                            $scope.secondarr.push($scope.t1);
                                    }else if($scope.days[x].trip[y].id=='t2'){
                                        $scope.t2+='住宿：在'+$scope.days[x].trip[y].start+'住宿。'+$scope.days[x].trip[y].info;
                                        $scope.secondarr.push($scope.t2);
                                    }else if($scope.days[x].trip[y].id=='t3'){
                                        $scope.t3+='用餐：在'+$scope.days[x].trip[y].start+'用餐，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        $scope.secondarr.push($scope.t3);
                                    }else if($scope.days[x].trip[y].id=='t4'){
                                        $scope.t4+='购物：在'+$scope.days[x].trip[y].start+'购物，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        $scope.secondarr.push($scope.t4);
                                    }else if($scope.days[x].trip[y].id=='t5'){
                                        $scope.coursestr='第'+(x+1)+'天:'+$scope.days.start+$scope.days.end;
                                        $scope.t5+='活动：在'+$scope.days[x].trip[y].start+'活动，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        $scope.secondarr.push($scope.t5);
                                    }else if($scope.days[x].trip[y].id=='t6'){
                                        $scope.coursestr='第'+(x+1)+'天:'+$scope.days.start+$scope.days.end;
                                        $scope.t6+='推荐：在'+$scope.days[x].trip[y].start+'比较好，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        $scope.secondarr.push($scope.t6);
                                    }else if($scope.days[x].trip[y].id=='t7'){
                                        $scope.coursestr='第'+(x+1)+'天:'+$scope.days.start+$scope.days.end;
                                        $scope.t7+='温馨提示:'+$scope.days[x].trip[y].start+$scope.days[x].trip[y].info;
                                        $scope.secondarr.push($scope.t7);
                                    }
                                }
                            }else if($scope.days[x].title.by=='3'){
                                
                                $scope.coursestrinfo+='第'+(x+1)+'天:'+$scope.days[x].title.start+'坐大客到'+$scope.days[x].title.end;
                                for(var y=0; y<$scope.days[x].trip.length;y++){
                                    $scope.t0 = '';
                                    $scope.t1 = '';
                                    $scope.t2 = '';
                                    $scope.t3 = '';
                                    $scope.t4 = '';
                                    $scope.t5 = '';
                                    $scope.t6 = '';
                                    $scope.t7 = '';
                                    if(!$scope.days[x].trip[y].info){
                                    $scope.days[x].trip[y].info='';
                                }
                                    if($scope.days[x].trip[y].id=='t0'){
                                        if($scope.days[x].trip[y].by=='1'){
                                            $scope.t0+='交通：从'+$scope.days[x].trip[y].start+'坐飞机到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        }else if($scope.days[x].trip[y].by=='2'){
                                            $scope.t0+='交通：从'+$scope.days[x].trip[y].start+'坐火车到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        }else if($scope.days[x].trip[y].by=='3'){
                                            $scope.t0='交通：从'+$scope.days[x].trip[y].start+'坐大客到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        }else if($scope.days[x].trip[y].by=='4'){
                                            $scope.t0+='交通：从'+$scope.days[x].trip[y].start+'坐轮船到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        }else {
                                            $scope.t0+='交通：从'+$scope.days[x].trip[y].start+'到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        }
                                        $scope.secondarr.push($scope.t0);
                                    }else if($scope.days[x].trip[y].id=='t1'){
                                            $scope.t1+='景点：'+$scope.days[x].trip[y].start+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                            $scope.secondarr.push($scope.t1);
                                    }else if($scope.days[x].trip[y].id=='t2'){
                                        $scope.t2+='住宿：在'+$scope.days[x].trip[y].start+'住宿。'+$scope.days[x].trip[y].info;
                                        $scope.secondarr.push($scope.t2);
                                    }else if($scope.days[x].trip[y].id=='t3'){
                                        $scope.t3+='用餐：在'+$scope.days[x].trip[y].start+'用餐，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        $scope.secondarr.push($scope.t3);
                                    }else if($scope.days[x].trip[y].id=='t4'){
                                        $scope.t4+='购物：在'+$scope.days[x].trip[y].start+'购物，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        $scope.secondarr.push($scope.t4);
                                    }else if($scope.days[x].trip[y].id=='t5'){
                                        $scope.coursestr='第'+(x+1)+'天:'+$scope.days.start+$scope.days.end;
                                        $scope.t5+='活动：在'+$scope.days[x].trip[y].start+'活动，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        $scope.secondarr.push($scope.t5);
                                    }else if($scope.days[x].trip[y].id=='t6'){
                                        $scope.coursestr='第'+(x+1)+'天:'+$scope.days.start+$scope.days.end;
                                        $scope.t6+='推荐：在'+$scope.days[x].trip[y].start+'比较好，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        $scope.secondarr.push($scope.t6);
                                    }else if($scope.days[x].trip[y].id=='t7'){
                                        $scope.coursestr='第'+(x+1)+'天:'+$scope.days.start+$scope.days.end;
                                        $scope.t7+='温馨提示:'+$scope.days[x].trip[y].start+$scope.days[x].trip[y].info;
                                        $scope.secondarr.push($scope.t7);
                                    }
                                }
                            }else if($scope.days[x].title.by=='4'){
                                
                                $scope.coursestrinfo+='第'+(x+1)+'天:'+$scope.days[x].title.start+'坐轮船到'+$scope.days[x].title.end;
                                for(var y=0; y<$scope.days[x].trip.length;y++){
                                    $scope.t0 = '';
                                    $scope.t1 = '';
                                    $scope.t2 = '';
                                    $scope.t3 = '';
                                    $scope.t4 = '';
                                    $scope.t5 = '';
                                    $scope.t6 = '';
                                    $scope.t7 = '';
                                    if(!$scope.days[x].trip[y].info){
                                    $scope.days[x].trip[y].info='';
                                }
                                    if($scope.days[x].trip[y].id=='t0'){
                                        if($scope.days[x].trip[y].by=='1'){
                                            $scope.t0+='交通：从'+$scope.days[x].trip[y].start+'坐飞机到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        }else if($scope.days[x].trip[y].by=='2'){
                                            $scope.t0+='交通：从'+$scope.days[x].trip[y].start+'坐火车到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        }else if($scope.days[x].trip[y].by=='3'){
                                            $scope.t0='交通：从'+$scope.days[x].trip[y].start+'坐大客到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        }else if($scope.days[x].trip[y].by=='4'){
                                            $scope.t0+='交通：从'+$scope.days[x].trip[y].start+'坐轮船到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        }else if($scope.days[x].trip[y].by==''){
                                            $scope.t0+='交通：从'+$scope.days[x].trip[y].start+'到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        }
                                        $scope.secondarr.push($scope.t0);
                                    }else if($scope.days[x].trip[y].id=='t1'){
                                            $scope.t1+='景点：'+$scope.days[x].trip[y].start+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                            $scope.secondarr.push($scope.t1);
                                    }else if($scope.days[x].trip[y].id=='t2'){
                                        $scope.t2+='住宿：在'+$scope.days[x].trip[y].start+'住宿。'+$scope.days[x].trip[y].info;
                                        $scope.secondarr.push($scope.t2);
                                    }else if($scope.days[x].trip[y].id=='t3'){
                                        $scope.t3+='用餐：在'+$scope.days[x].trip[y].start+'用餐，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        $scope.secondarr.push($scope.t3);
                                    }else if($scope.days[x].trip[y].id=='t4'){
                                        $scope.t4+='购物：在'+$scope.days[x].trip[y].start+'购物，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        $scope.secondarr.push($scope.t4);
                                    }else if($scope.days[x].trip[y].id=='t5'){
                                        $scope.coursestr='第'+(x+1)+'天:'+$scope.days.start+$scope.days.end;
                                        $scope.t5+='活动：在'+$scope.days[x].trip[y].start+'活动，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        $scope.secondarr.push($scope.t5);
                                    }else if($scope.days[x].trip[y].id=='t6'){
                                        $scope.coursestr='第'+(x+1)+'天:'+$scope.days.start+$scope.days.end;
                                        $scope.t6+='推荐：在'+$scope.days[x].trip[y].start+'比较好，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        $scope.secondarr.push($scope.t6);
                                    }else if($scope.days[x].trip[y].id=='t7'){
                                        $scope.coursestr='第'+(x+1)+'天:'+$scope.days.start+$scope.days.end;
                                        $scope.t7+='温馨提示:'+$scope.days[x].trip[y].start+$scope.days[x].trip[y].info;
                                        $scope.secondarr.push($scope.t7);
                                    }
                                }
                            }else if($scope.days[x].title.by=='5'){
                                
                                $scope.coursestrinfo+='第'+(x+1)+'天:'+$scope.days[x].title.start;
                                for(var y=0; y<$scope.days[x].trip.length;y++){
                                    $scope.t0 = '';
                                    $scope.t1 = '';
                                    $scope.t2 = '';
                                    $scope.t3 = '';
                                    $scope.t4 = '';
                                    $scope.t5 = '';
                                    $scope.t6 = '';
                                    $scope.t7 = '';
                                    if(!$scope.days[x].trip[y].info){
                                    $scope.days[x].trip[y].info='';
                                }
                                    if($scope.days[x].trip[y].id=='t0'){
                                        if($scope.days[x].trip[y].by=='1'){
                                            $scope.t0+='交通：从'+$scope.days[x].trip[y].start+'坐飞机到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        }else if($scope.days[x].trip[y].by=='2'){
                                            $scope.t0+='交通：从'+$scope.days[x].trip[y].start+'坐火车到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        }else if($scope.days[x].trip[y].by=='3'){
                                            $scope.t0='交通：从'+$scope.days[x].trip[y].start+'坐大客到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        }else if($scope.days[x].trip[y].by=='4'){
                                            $scope.t0+='交通：从'+$scope.days[x].trip[y].start+'坐轮船到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        }else if($scope.days[x].trip[y].by==''){
                                            $scope.t0+='交通：从'+$scope.days[x].trip[y].start+'到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        }
                                        $scope.secondarr.push($scope.t0);
                                    }else if($scope.days[x].trip[y].id=='t1'){
                                            $scope.t1+='景点：'+$scope.days[x].trip[y].start+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                            $scope.secondarr.push($scope.t1);
                                    }else if($scope.days[x].trip[y].id=='t2'){
                                        $scope.t2+='住宿：在'+$scope.days[x].trip[y].start+'住宿。'+$scope.days[x].trip[y].info;
                                        $scope.secondarr.push($scope.t2);
                                    }else if($scope.days[x].trip[y].id=='t3'){
                                        $scope.t3+='用餐：在'+$scope.days[x].trip[y].start+'用餐，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        $scope.secondarr.push($scope.t3);
                                    }else if($scope.days[x].trip[y].id=='t4'){
                                        $scope.t4+='购物：在'+$scope.days[x].trip[y].start+'购物，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        $scope.secondarr.push($scope.t4);
                                    }else if($scope.days[x].trip[y].id=='t5'){
                                        $scope.coursestr='第'+(x+1)+'天:'+$scope.days.start+$scope.days.end;
                                        $scope.t5+='活动：在'+$scope.days[x].trip[y].start+'活动，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        $scope.secondarr.push($scope.t5);
                                    }else if($scope.days[x].trip[y].id=='t6'){
                                        $scope.coursestr='第'+(x+1)+'天:'+$scope.days.start+$scope.days.end;
                                        $scope.t6+='推荐：在'+$scope.days[x].trip[y].start+'比较好，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        $scope.secondarr.push($scope.t6);
                                    }else if($scope.days[x].trip[y].id=='t7'){
                                        $scope.coursestr='第'+(x+1)+'天:'+$scope.days.start+$scope.days.end;
                                        $scope.t7+='温馨提示:'+$scope.days[x].trip[y].start+$scope.days[x].trip[y].info;
                                        $scope.secondarr.push($scope.t7);
                                    }
                                }
                            }
                            $scope.shuzu.push($scope.secondarr);
                            $scope.biaotiarr.push($scope.coursestrinfo);
                        }
                    }      
                	if(res.data.big_traffic_economy_ticket=='0'){
                        if(res.data.big_traffic_tax=='0'){
                            $scope.str+='含税经济舱机票、';
                        }else{
                            $scope.str+='不含税经济舱机票、';
                        }
                    }
                    if(res.data.big_traffic_train_ticket=='0'){
                        if(res.data.big_traffic_train_ticket_type=='0'){
                            $scope.str+='往返火车票、';
                        }else if(res.data.big_traffic_train_ticket_type=='1'){
                            $scope.str+='去程火车票、';
                        }else if(res.data.big_traffic_train_ticket_type=='2'){
                            $scope.str+='返程火车票、';
                        }else if(res.data.big_traffic_train_ticket_type=='3'){
                            $scope.str+='中间段火车票、';
                        }
                    }
                    if(res.data.big_traffic_tourist_bus=='0'){
                        if(res.data.big_traffic_bus_type=='0'){
                            $scope.str+='往返交通旅游大巴、';
                        }else if(res.data.big_traffic_bus_type=='1'){
                            $scope.str+='去程交通旅游大巴、';
                        }else if(res.data.big_traffic_bus_type=='2'){
                            $scope.str+='返程交通旅游大巴、';
                        }else if(res.data.big_traffic_bus_type=='3'){
                            $scope.str+='全程交通旅游大巴、';
                        }
                    }
                    if(res.data.big_traffic_local_tourist_bus=='0'){
                        $scope.str+='当地交通旅游大巴、';
                    }
                    if(res.data.big_traffic_return_ticket=='0'){
                        $scope.str+='往返车票、';
                    }
                    if(res.data.big_traffic_steamer_ticket=='0'){
                         if(res.data.big_traffic_steamer_ticket_type =='0'){
                            $scope.str+='往返船票、';
                        }else if(res.data.big_traffic_steamer_ticket_type =='1'){
                            $scope.str+='去程船票、';
                        }else if(res.data.big_traffic_steamer_ticket_type =='2'){
                            $scope.str+='返程船票、';
                        }else if(res.data.big_traffic_steamer_ticket_type =='3'){
                            $scope.str+='全程船票、';
                        }
                    }
                    if(res.data.big_traffic_ticket_remark){
                         $scope.str+=res.data.big_traffic_ticket_remark+'、';
                    }
                    $scope.str = $scope.str.substring(0,$scope.str.length-1);
                    if($scope.str!=''){
                        $scope.str+='。';
                    }

                    

                    if(res.data.little_traffic_traffic =='0'){
                        if(res.data.little_traffic_traffic_type  =='0'){
                            $scope.smallstr+='往返小交通、';
                        }else if(res.data.little_traffic_traffic_type  =='1'){
                            $scope.smallstr+='去程小交通、';
                        }else if(res.data.little_traffic_traffic_type  =='2'){
                            $scope.smallstr+='返程小交通、';
                        }
                    }
                    if(res.data.little_traffic_scenic_car=='0'){
                        $scope.smallstr+='景区内用车、';
                    }
                    if(res.data.little_traffic_describe){
                        $scope.smallstr+=res.data.little_traffic_describe+'、';
                    }
                    
                    $scope.smallstr = $scope.smallstr.substring(0,$scope.smallstr.length-1);
                     if($scope.smallstr!=''){
                        $scope.smallstr+='。';
                    }
                    if(res.data.safe_contain=='0'){
                        $scope.baoxian+=res.data.safe_content;
                    }
                    if(res.data.stay_type=='1'){
                        if(res.data.stay_type=='0'){
                            $scope.sleepstr+='行程所列酒店、';
                        }else if(res.data.stay_type=='1'){
                            $scope.sleepstr+=res.data.stay_hotel_star+'星级标准'+res.data.stay_room_capacity+'人间、';
                        }else if(res.data.stay_type=='2'){
                            $scope.sleepstr+='普通酒店标准'+res.data.stay_hotel_num+'人间、';
                        }else if(res.data.stay_type=='3'){
                            $scope.sleepstr+=res.data.dest_hotel_destination+'目的地酒店标准'+res.data.dest_room_capacity+'人间、';
                        }
                        if(res.data.cruises_dest_messages=='0'){
                            $scope.sleepstr+='目的地游轮、';
                        }
                        $scope.sleepstr = $scope.sleepstr.substring(0,$scope.sleepstr.length-1);
                        if($scope.sleepstr!=''){
                            $scope.sleepstr+='。';
                        }
                    }
                    
                    

                    if(res.data.dining_type=='0'){
                        $scope.eatstr+=res.data.dining_early+'早'+res.data.dining_dinner+'正、'+res.data.dining_spend+'正餐/人/餐、正餐'+res.data.dining_people_num+'人'+res.data.dining_table_num+'桌'+res.data.dining_food_num+'菜'+res.data.dining_soup_num+'汤、';
                    }else if(res.data.dining_type=='1'){

                    }else if(res.data.dining_type=='2'){
                        $scope.eatstr+='酒店含早正餐自理、';
                    }else if(res.data.dining_type=='3'){
                        $scope.eatstr+='其他：'+res.data.dining_custom+'、';
                    }
                    $scope.eatstr = $scope.eatstr.substring(0,$scope.eatstr.length-1);
                    if($scope.eatstr!=''){
                        $scope.eatstr+='。';
                    }

                    if(res.data.admission_ticket=='1'){
                        $scope.orderstr+='门票：'+res.data.admission_ticket_attractions+'(首道大门票)、'+res.data.admission_ticket_coupon_attractions+'(联票，放弃其中任何一个景点费用不退)、'+res.data.admission_ticket_free_attractions+'(赠送项目，如不参加或不能游览，不退任何费用)、';
                    }
                    if(res.data.guide_service_type=='0'){
                        $scope.orderstr+='当地中文导游、';
                    }else if(res.data.guide_service_type=='1'){
                        $scope.orderstr+='专职中文领队兼导游(境外)、';
                    }else if(res.data.guide_service_type=='2'){
                        $scope.orderstr+='全程陪同中文导游(境内)、';
                    }else if(res.data.guide_service_type=='3'){
                        $scope.orderstr+='专职领队和当地中文导游(境外)、';
                    }else if(res.data.guide_service_type=='4'){
                        $scope.orderstr+='全陪和当地中文导游(境内)、';
                    }
                    if(res.data.guide_service_explain){
                         $scope.orderstr+='导服说明：'+res.data.guide_service_explain+'、';
                    }
                    if(res.data.guide_service_driver_tip=='0'){
                        $scope.orderstr+='司机小费'+res.data.guide_service_driver_price+'元、小费说明：'+res.data.guide_service_driver_explain+'、';
                    }
                    if(res.data.child_fee_exist=='0'){
                        $scope.orderstr+='儿童费用标准：'+res.data.child_fee_age_start+'岁到'+res.data.child_fee_age_end+'岁、'+res.data.child_fee_height_start+'cm到'+res.data.child_fee_height_end+'cm。';
                    }
                    if(res.data.ticket_tax_round_trip=='0'){
                        $scope.orderstr+='往返机票税'+res.data.ticket_tax_fee+'元、';
                    }

                    // console.log(res.data.else_messages_arr);
                    if(res.data.else_messages_arr){
                        var arrtuijian = res.data.else_messages_arr.split("|");    
                    }
                    if(res.data.else_messages_arr){
                        // $scope.smallstr+=res.data.ticket_tax_fee+'元往返机票税、';
                        if(arrtuijian){
                            for(var x=0; x<arrtuijian.length; x++){
                                $scope.orderstr+=arrtuijian[x]+'、';
                            }
                        }
                        
                    }                    
                    $scope.orderstr = $scope.orderstr.substring(0,$scope.orderstr.length-1);
                    if($scope.orderstr!=''){
                        $scope.orderstr+='。';
                    }
                    //购买须知
                    if(res.data.hint_self_driving_writing=='0'){
                        if(res.data.hint_self_driving_writing_content){
                             $scope.cautionstr+='自驾游、自驾游手动编写内容：'+res.data.hint_self_driving_writing+'、'
                        }else{
                            $scope.cautionstr+='自驾游、' 
                        }
                    }
                    if(res.data.hint_payment_attention){
                        $scope.cautionstr+='付款预定须知：'+res.data.hint_payment_attention+'、'
                    }
                    if(res.data.hint_cancel_trip=='0'){
                        $scope.cautionstr+='此团在收容人数不足'+res.data.hint_min_travel_population+'人时提前'+res.data.hint_ravel_advance_notification+'天通知、';
                    }
                    if(res.data.hint_touring_drop_out=='0'){
                        $scope.cautionstr+='团队游览允许擅自离队、';
                    }else if(res.data.hint_touring_drop_out=='1'){
                        $scope.cautionstr+='团队游览不允许擅自离队、';
                    }
                    if(res.data.hint_touring_penalty=='0'){
                        $scope.cautionstr+='违约需要支付'+res.data.hint_touring_out_money+'元违约金、';
                    }
                    if(res.data.hint_not_outing_explain=='0'){
                        $scope.cautionstr+='甲方原因无法出游乙方负责说明。';
                    }else if(res.data.hint_not_outing_explain=='0'){
                        $scope.cautionstr+='不存在甲方原因无法出游乙方负责说明。';
                    }
                     $scope.cautionstr = $scope.cautionstr.substring(0,$scope.cautionstr.length-1);
                     if($scope.cautionstr!=''){
                        $scope.cautionstr+='。';
                    }

                    if(res.data.additional_lijiang_upkeep=='0'){
                        $scope.addstr+='丽江维护费80元每人、';
                    }
                    if(res.data.additional_hainan_payment=='0'){
                        $scope.addstr+='海南政府调节金'+res.data.additional_fee+'每人、';
                    }
                    if(res.data.additional_extra_fee=='0'){
                        $scope.addstr+='存在因交通延阻、战争、政变、罢工、天气、飞机机器故障、航班取消等导致额外费用、';
                    }
                    if(res.data.additional_personal_fee=='0'){
                        $scope.addstr+='酒店内洗漱、理发、电话、传真、收费电视、饮品、烟酒等个人消费。';
                    }
                    $scope.addstr = $scope.addstr.substring(0,$scope.addstr.length-1);
                    if($scope.addstr!=''){
                        $scope.addstr+='。';
                    }
                    if(res.data.bed_material_commit){
                        $scope.importantstr+='最晚收材料提前'+res.data.bed_material_commit+'个工作日、'                        
                    }
                    if(res.data.important_hair_regiment_type=='0'){
                        $scope.importantstr+='出发地成团、';
                    }else if(res.data.important_hair_regiment_type=='1'){
                        $scope.importantstr+='目的地成团、';
                    }else if(res.data.important_hair_regiment_type=='2'){
                        $scope.importantstr+='中转地联运、';
                    }
                    if(res.data.important_group_type=='0'){
                        $scope.importantstr+='独家发团、';
                    }else if(res.data.important_group_type=='1'){
                        $scope.importantstr+='联合发团、';
                    }
                    if(res.data.important_spell_group=='0'){
                        $scope.importantstr+='非行程中拼团、';
                    }else if(res.data.important_spell_group=='1'){
                        $scope.importantstr+='行程中拼团、';
                    }
                    if(res.data.important_local_join=='0'){
                        $scope.importantstr+='存在当地人员参团、';
                    }
                    if(res.data.important_spell_car=='0'){
                        $scope.importantstr+='存在拼车、';
                    }
                    if(res.data.important_change_car_guide=='0'){
                        $scope.importantstr+='存在当地换车或导游、';
                    }
                    $scope.importantstr = $scope.importantstr.substring(0,$scope.importantstr.length-1);
                    if($scope.importantstr!=''){
                        $scope.importantstr+='。';
                    }

                    if(res.data.stay_spell_room=='0'){
                        $scope.cautionsleepstr+='不可拼房、';
                    }else if(res.data.stay_spell_room=='1'){
                        $scope.cautionsleepstr+='可亲友加床、';
                    }
                    if(res.data.stay_toilet_articles=='0'){
                        $scope.cautionsleepstr+='提供洗漱用品。';
                    }else if(res.data.stay_toilet_articles=='1'){
                        $scope.cautionsleepstr+='不提供洗漱用品。';
                    }
                    $scope.cautionsleepstr = $scope.cautionsleepstr.substring(0,$scope.cautionsleepstr.length-1);
                    if($scope.cautionsleepstr!=''){
                        $scope.cautionsleepstr+='。';
                    }

                    if(res.data.spellgroup_count_right=='0'){
                        $scope.speelstr+='共拼团'+res.data.spellgroup_count+'次（准确）';
                    }else if(res.data.spellgroup_count_right=='1'){
                         $scope.speelstr+='共拼团'+res.data.spellgroup_count+'次（估算）';
                    }
                    if(res.data.payinfo_full){
                        $scope.paystr='1、为确保您能够按时出行，产品确认后请在'+res.data.payinfo_full+'小时内付款，同时按要求尽快提供出游所需要的材料并签订出游合同'+'2、为确保您能够按时出行，产品确认后请在'+res.data.payinfo_advance+'小时内付预付款，同时按要求尽快提供出游所需要的材料，并于出团前5个工作日交齐尾款并签订出游合同'+'3、预定时请告知您的出游人数，出发日期、住宿、用餐标准、以及您的特殊要求。';
                    }

                    if(res.data.special_order_min_right=='0'){
                        $scope.peoplestr+='订单少于'+res.data.special_order_min_num+'人时需要确认、';
                    }
                    if(res.data.special_order_max_right=='0'){
                        $scope.peoplestr+='订单多于'+res.data.special_order_max_num+'人时需要确认、';
                    }
                    if(res.data.special_age_min_right=='0'){
                        $scope.peoplestr+='出游人年龄小于'+res.data.special_age_min_num+'岁不接收、';
                    }
                    if(res.data.special_age_max_right=='0'){
                        $scope.peoplestr+='出游人年龄大于'+res.data.special_age_max_num+'岁不接收、';
                    }
                    if(res.data.special_exceed_age_right=='0'){
                        $scope.peoplestr+='出游人年龄大于'+res.data.special_exceed_age_num+'岁需要签署健康协议、';
                    }
                    if(res.data.special_receive_foreignness=='0'){
                        $scope.peoplestr+='接收外籍游客、';
                    }else if(res.data.special_receive_foreignness=='1'){
                        $scope.peoplestr+='不接收外籍游客、';
                    }
                    if(res.data.special_region_limit=='0'){
                        $scope.peoplestr+='存在地域限制：'+res.data.special_region_limit_list;
                    }
                    $scope.peoplestr = $scope.peoplestr.substring(0,$scope.peoplestr.length-1);
                    if($scope.peoplestr!=''){
                        $scope.peoplestr+='。';
                    }
                    
                    if(res.data.else_arr){
                        $scope.qitaarr = res.data.else_arr.split(",");
                        if($scope.qitaarr.length!=0){
                            for (var w=0;w<$scope.qitaarr.length;w++){
                                
                                if($scope.qitaarr[w]=='1'){
                                    $scope.qita+='酒店不提供一次性用品、';
                                }else if($scope.qitaarr[w]=='2'){
                                    $scope.qita+='山上酒店潮湿、';
                                }else if($scope.qitaarr[w]=='3'){
                                    $scope.qita+='酒店二次确认、';
                                }else if($scope.qitaarr[w]=='4'){
                                    $scope.qita+='火车票不能提前预售、';
                                }else if($scope.qitaarr[w]=='5'){
                                    $scope.qita+='拼车、';
                                }else if($scope.qitaarr[w]=='6'){
                                    $scope.qita+='伊斯兰教国家、';
                                }else if($scope.qitaarr[w]=='7'){
                                    $scope.qita+='车况较差、';
                                }else if($scope.qitaarr[w]=='8'){
                                    $scope.qita+='拼号、';
                                }else if($scope.qitaarr[w]=='9'){
                                    $scope.qita+='道路拥堵、';
                                }else if($scope.qitaarr[w]=='10'){
                                    $scope.qita+='路况较差、路程长。';
                                }
                            }
                        }
                    }
                    if($scope.qita!=''){
                        $scope.qita = $scope.qita.substring(0,$scope.qita.length-1);
                        if($scope.qita!=''){
                            $scope.qita+='。';
                        }
                    }
                    
                     
                    //条款
                    if(res.data.with_group_terms){
                        $scope.arrtuijian = res.data.with_group_terms.split(",");
                        if(res.data.else_terms){
                            $scope.othetarr = res.data.else_terms.split(",");           
                        }
                        for (var q=0;q<$scope.arrtuijian.length;q++){
                            
                            if($scope.arrtuijian[q]=='1'){
                                $scope.gentuan+='旅游者在出发前30日内提出解除合同的，应当按下列标准向组团社支付业务损失费:';
                            }else if($scope.arrtuijian[q]=='2'){
                                $scope.gentuan+='行程开始前29日至15日，按旅游费用总额的5%,';
                            }else if($scope.arrtuijian[q]=='3'){
                                $scope.gentuan+='行程开始前14日至7日，按旅游费用总额的20%,';
                            }else if($scope.arrtuijian[q]=='4'){
                                $scope.gentuan+='行程开始前6日至4日，按旅游费用总额的50%,';
                            }else if($scope.arrtuijian[q]=='5'){
                                $scope.gentuan+='行程开始前3日至1日，按旅游费用总额的60%,';
                            }else if($scope.arrtuijian[q]=='6'){
                                $scope.gentuan+='行程开始当日，按旅游费用总额的70%,';
                            }else if($scope.arrtuijian[q]=='7'){
                                $scope.gentuan+='软上述比例支付业务损失费不足以赔偿组社团的实际损失，旅游者当按实际损失对组团社予以赔偿，但最高不应当超过旅游费用总额,';
                            }else if($scope.arrtuijian[q]=='8'){
                                $scope.gentuan+='游客转让：出行前，在符合办理团队签证或签注期限或其他条件许可情况下，旅游者可以向组团社书面提出将其自身在本合同中的权利和义务转让给符合出游条件的第三人；并且由第三人与组团社重新签订合同；因此增加的费用由旅游者或第三人承担，减少的费用组团社退还旅游者。';
                            }
                        }
                    }
                    
                    // if(res.data.terms_type==0){
                    //     $scope.gentuan='跟团条款：'+res.data.with_group_terms;
                    // }else if(res.data.terms_type==1){
                    //     $scope.gentuan='其他条款：'+res.data.else_terms;
                    // }
                    // console.log(res.data.findGroupDatesList);
                    // console.log('res.data.findGroupDatesList.length');
                    
                    for(var m=0; m<res.data.findGroupDatesList.length;m++){
                        // console.log(res.data.findGroupDatesList[m].materials_advance_reserve_day);
                        // if(res.data.findGroupDatesList[m].materials_advance_reserve_day ){
                        //     alert(123);
                        //     $scope.shijian=res.data.findGroupDatesList[m].materials_advance_reserve_day+'天'+res.data.findGroupDatesList[m].materials_advance_reserve_hour+'时'+res.data.findGroupDatesList[m].materials_advance_reserve_minute+'分';
                        //     $scope.shijianarr.push($scope.shijian);
                        
                            
                        // }
                        if(res.data.findGroupDatesList[m].adult_ban==0){
                        
                            $scope.peoplemoney=res.data.findGroupDatesList[m].adult_call_price+'/'+res.data.findGroupDatesList[m].adult_sale_price;
                        }else if(res.data.findGroupDatesList[m].adult_ban==1){
                            $scope.peoplemoney='禁售';
                        }
                        if(res.data.findGroupDatesList[m].children_ban==0){
                            $scope.childmoney=res.data.findGroupDatesList[m].children_call_price+'/'+res.data.findGroupDatesList[m].children_sale_price;
                        }else if(res.data.findGroupDatesList[m].children_ban==1){
                            $scope.childmoney='禁售';
                        }
                        if(res.data.findGroupDatesList[m].single_room_ban==0){
                            $scope.housemoney=res.data.findGroupDatesList[m].single_room_call_price+'/'+res.data.findGroupDatesList[m].single_room_sale_price;
                        }else if(res.data.findGroupDatesList[m].single_room_ban==1){
                            $scope.housemoney='禁售';
                        }
                        $scope.peoplemoneyarr.push($scope.peoplemoney);
                        $scope.childmoneyarr.push($scope.childmoney);
                        $scope.housemoneyarr.push($scope.housemoney);
                        
                       

                    }
                    for(var a=0; a<$scope.activity.length; a++){
                        $scope.numarr=[];
                        if($scope.activity[a].firstname==0){
                            $scope.firststr='酒店介绍'; 
                        }else if($scope.activity[a].firstname==1){
                            $scope.firststr='景点介绍'; 
                        }else if($scope.activity[a].firstname==2){
                            $scope.firststr='美食推荐'; 
                        }else if($scope.activity[a].firstname==3){
                            $scope.firststr='交通信息'; 
                        }else if($scope.activity[a].firstname==4){
                            $scope.firststr='自定义'; 
                        }
                        $scope.secondstr='标题：'+$scope.activity[a].secondname;
                        $scope.thirdstr='正文：'+$scope.activity[a].thirdname;
                        $scope.numarr.push($scope.firststr);
                        $scope.numarr.push($scope.secondstr);
                        $scope.numarr.push($scope.thirdstr);
                        $scope.allarr.push($scope.numarr);
                         
                    }

                    for(var b=0; b<$scope.traffic.length; b++){
                       if($scope.traffic[b].shipping_space==0){
                            $scope.feijistr='一等座。';
                        }else if($scope.traffic[b].shipping_space==1){
                            $scope.feijistr='二等座。';
                        }else if($scope.traffic[b].shipping_space==2){
                            $scope.feijistr='站票。';
                        }
                        if($scope.traffic[b].shipping_space==0){
                            $scope.feijistr='经济舱。';
                        }else if($scope.traffic[b].shipping_space==1){
                            $scope.feijistr='商务舱。';
                        }else if($scope.traffic[b].shipping_space==2){
                            $scope.feijistr='头等舱。';
                        }
                       if($scope.traffic[b].traffic_info==1){//交通信息几
                           if($scope.traffic[b].mode==0){//去程还是返程
                               if($scope.traffic[b].transfer==0){
                                    $scope.jiaotongnum='交通信息1：';
                                    $scope.trafficarr.push($scope.jiaotongnum);
                               }
                               
                               if($scope.traffic[b].reference_flight==1){//是否有产考信息
                                    if($scope.traffic[b].transfer==0){//往返交通中是中转第几次
                                        if(res.data.departure_mode==0){//飞机
                                            if($scope.traffic[b].shipping_space==0){
                                                $scope.feijistr='经济舱。';
                                            }else if($scope.traffic[b].shipping_space==1){
                                                $scope.feijistr='商务舱。';
                                            }else if($scope.traffic[b].shipping_space==2){
                                                $scope.feijistr='头等舱。';
                                            }
                                            $scope.trafficstr='去程——飞机：坐'+$scope.traffic[b].airline_company+'的'+$scope.traffic[b].flt_no+'班次飞机，从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发、'+$scope.traffic[b].arrival_time+'到达。共'+$scope.traffic[b].total_time+'分钟。'+$scope.feijistr
                                        }else if(res.data.departure_mode==1){//火车
                                            if($scope.traffic[b].shipping_space==0){
                                                $scope.feijistr='一等座。';
                                            }else if($scope.traffic[b].shipping_space==1){
                                                $scope.feijistr='二等座。';
                                            }else if($scope.traffic[b].shipping_space==2){
                                                $scope.feijistr='站票。';
                                            }
                                            $scope.trafficstr='去程——火车：坐'+$scope.traffic[b].flt_no+'次火车，从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发、'+$scope.traffic[b].arrival_time+'到达。共'+$scope.traffic[b].total_time+'分钟。'+$scope.feijistr
                                        }else if(res.data.departure_mode==2){//轮船
                                            $scope.trafficstr='去程——轮船：从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发。' + $scope.traffic[b].remarks                                   
                                        }else if(res.data.departure_mode==3){//汽车
                                            $scope.trafficstr='去程——汽车：从'+$scope.traffic[b].departure_city+'，'+$scope.traffic[b].departure_time+'出发。'+ $scope.traffic[b].remarks                                   
                                        }
                                    }else if($scope.traffic[b].transfer==1){//往返交通中是中转第几次
                                        if(res.data.departure_mode==0){//飞机
                                            if($scope.traffic[b].shipping_space==0){
                                                $scope.feijistr='经济舱。';
                                            }else if($scope.traffic[b].shipping_space==1){
                                                $scope.feijistr='商务舱。';
                                            }else if($scope.traffic[b].shipping_space==2){
                                                $scope.feijistr='头等舱。';
                                            }
                                            $scope.trafficstr='中转1——飞机：坐'+$scope.traffic[b].airline_company+'的'+$scope.traffic[b].flt_no+'班次飞机，从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发、'+$scope.traffic[b].arrival_time+'到达。共'+$scope.traffic[b].total_time+'分钟。'+$scope.feijistr
                                        }else if(res.data.departure_mode==1){//火车
                                            if($scope.traffic[b].shipping_space==0){
                                                $scope.feijistr='一等座。';
                                            }else if($scope.traffic[b].shipping_space==1){
                                                $scope.feijistr='二等座。';
                                            }else if($scope.traffic[b].shipping_space==2){
                                                $scope.feijistr='站票。';
                                            }
                                            $scope.trafficstr='中转1——火车：坐'+$scope.traffic[b].flt_no+'次火车，从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发、'+$scope.traffic[b].arrival_time+'到达。共'+$scope.traffic[b].total_time+'分钟。'+$scope.feijistr
                                        }else if(res.data.departure_mode==2){//轮船
                                            $scope.trafficstr='中转1——轮船：从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发。' + $scope.traffic[b].remarks                                   
                                        }else if(res.data.departure_mode==3){//汽车
                                            $scope.trafficstr='中转1——汽车：从'+$scope.traffic[b].departure_city+'，'+$scope.traffic[b].departure_time+'出发。'+ $scope.traffic[b].remarks                                    
                                        }
                                    }else if($scope.traffic[b].transfer==2){//往返交通中是中转第几次
                                        if(res.data.departure_mode==0){//飞机
                                            if($scope.traffic[b].shipping_space==0){
                                                $scope.feijistr='经济舱。';
                                            }else if($scope.traffic[b].shipping_space==1){
                                                $scope.feijistr='商务舱。';
                                            }else if($scope.traffic[b].shipping_space==2){
                                                $scope.feijistr='头等舱。';
                                            }
                                            $scope.trafficstr='中转2——飞机：坐'+$scope.traffic[b].airline_company+'的'+$scope.traffic[b].flt_no+'班次飞机，从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发、'+$scope.traffic[b].arrival_time+'到达。共'+$scope.traffic[b].total_time+'分钟。'+$scope.feijistr
                                        }else if(res.data.departure_mode==1){//火车
                                            if($scope.traffic[b].shipping_space==0){
                                                $scope.feijistr='一等座。';
                                            }else if($scope.traffic[b].shipping_space==1){
                                                $scope.feijistr='二等座。';
                                            }else if($scope.traffic[b].shipping_space==2){
                                                $scope.feijistr='站票。';
                                            }
                                            $scope.trafficstr='中转2——火车：坐'+$scope.traffic[b].flt_no+'次火车，从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发、'+$scope.traffic[b].arrival_time+'到达。共'+$scope.traffic[b].total_time+'分钟。'+$scope.feijistr
                                        }else if(res.data.departure_mode==2){//轮船
                                            $scope.trafficstr='中转2——轮船：从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发。' + $scope.traffic[b].remarks                                    
                                        }else if(res.data.departure_mode==3){//汽车
                                            $scope.trafficstr='中转2——汽车：从'+$scope.traffic[b].departure_city+'，'+$scope.traffic[b].departure_time+'出发。'+ $scope.traffic[b].remarks                                    
                                        }
                                    }
                               }else if($scope.traffic[b].reference_flight==0){
                                   if(res.data.departure_mode==0){//飞机
                                        $scope.trafficstr='去程——飞机：从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'。'
                                    }else if(res.data.departure_mode==1){//火车
                                        $scope.trafficstr='去程——火车：从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'。'
                                    }else if(res.data.departure_mode==2){//轮船
                                        $scope.trafficstr='去程——轮船：从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'。'
                                    }else if(res.data.departure_mode==3){//汽车
                                        $scope.trafficstr='去程——汽车：从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'。'
                                    }
                               }
                               $scope.trafficarr.push($scope.trafficstr);                                   
                           }else if($scope.traffic[b].mode==1){//去程还是返程
                                if($scope.traffic[b].reference_flight==1){//是否有产考信息
                                    if($scope.traffic[b].transfer==0){//往返交通中是中转第几次
                                        if(res.data.return_mode==0){//飞机
                                            if($scope.traffic[b].shipping_space==0){
                                                $scope.feijistr='经济舱。';
                                            }else if($scope.traffic[b].shipping_space==1){
                                                $scope.feijistr='商务舱。';
                                            }else if($scope.traffic[b].shipping_space==2){
                                                $scope.feijistr='头等舱。';
                                            }
                                            $scope.trafficstr='返程——飞机：坐'+$scope.traffic[b].airline_company+'的'+$scope.traffic[b].flt_no+'班次飞机，从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发、'+$scope.traffic[b].arrival_time+'到达。共'+$scope.traffic[b].total_time+'分钟。'+$scope.feijistr
                                        }else if(res.data.return_mode==1){//火车
                                            if($scope.traffic[b].shipping_space==0){
                                                $scope.feijistr='一等座。';
                                            }else if($scope.traffic[b].shipping_space==1){
                                                $scope.feijistr='二等座。';
                                            }else if($scope.traffic[b].shipping_space==2){
                                                $scope.feijistr='站票。';
                                            }
                                            $scope.trafficstr='返程——火车：坐'+$scope.traffic[b].flt_no+'次火车，从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发、'+$scope.traffic[b].arrival_time+'到达。共'+$scope.traffic[b].total_time+'分钟。'+$scope.feijistr
                                        }else if(res.data.return_mode==2){//轮船
                                            $scope.trafficstr='返程——轮船：从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发。' + $scope.traffic[b].remarks                                    
                                        }else if(res.data.return_mode==3){//汽车
                                            $scope.trafficstr='返程——汽车：从'+$scope.traffic[b].departure_city+'，'+$scope.traffic[b].departure_time+'出发。'+ $scope.traffic[b].remarks                                    
                                        }
                                    }else if($scope.traffic[b].transfer==1){//往返交通中是中转第几次
                                        if(res.data.return_mode==0){//飞机
                                            if($scope.traffic[b].shipping_space==0){
                                                $scope.feijistr='经济舱。';
                                            }else if($scope.traffic[b].shipping_space==1){
                                                $scope.feijistr='商务舱。';
                                            }else if($scope.traffic[b].shipping_space==2){
                                                $scope.feijistr='头等舱。';
                                            }
                                            $scope.trafficstr='中转1——飞机：坐'+$scope.traffic[b].airline_company+'的'+$scope.traffic[b].flt_no+'班次飞机，从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发、'+$scope.traffic[b].arrival_time+'到达。共'+$scope.traffic[b].total_time+'分钟。'+$scope.feijistr
                                        }else if(res.data.return_mode==1){//火车
                                            if($scope.traffic[b].shipping_space==0){
                                                $scope.feijistr='一等座。';
                                            }else if($scope.traffic[b].shipping_space==1){
                                                $scope.feijistr='二等座。';
                                            }else if($scope.traffic[b].shipping_space==2){
                                                $scope.feijistr='站票。';
                                            }
                                            $scope.trafficstr='中转1——火车：坐'+$scope.traffic[b].flt_no+'班次飞机，从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发、'+$scope.traffic[b].arrival_time+'到达。共'+$scope.traffic[b].total_time+'分钟。'+$scope.feijistr
                                        }else if(res.data.return_mode==2){//轮船
                                            $scope.trafficstr='中转1——轮船：从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发。' + $scope.traffic[b].remarks                                    
                                        }else if(res.data.return_mode==3){//汽车
                                            $scope.trafficstr='中转1——汽车：从'+$scope.traffic[b].departure_city+'，'+$scope.traffic[b].departure_time+'出发。'+ $scope.traffic[b].remarks                                    
                                        }
                                    }else if($scope.traffic[b].transfer==2){//往返交通中是中转第几次
                                        if(res.data.return_mode==0){//飞机
                                            if($scope.traffic[b].shipping_space==0){
                                                $scope.feijistr='经济舱。';
                                            }else if($scope.traffic[b].shipping_space==1){
                                                $scope.feijistr='商务舱。';
                                            }else if($scope.traffic[b].shipping_space==2){
                                                $scope.feijistr='头等舱。';
                                            }
                                            $scope.trafficstr='中转2——飞机：坐'+$scope.traffic[b].airline_company+'的'+$scope.traffic[b].flt_no+'班次飞机，从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发、'+$scope.traffic[b].arrival_time+'到达。共'+$scope.traffic[b].total_time+'分钟。'+$scope.feijistr
                                        }else if(res.data.return_mode==1){//火车
                                            if($scope.traffic[b].shipping_space==0){
                                                $scope.feijistr='一等座。';
                                            }else if($scope.traffic[b].shipping_space==1){
                                                $scope.feijistr='二等座。';
                                            }else if($scope.traffic[b].shipping_space==2){
                                                $scope.feijistr='站票。';
                                            }
                                            $scope.trafficstr='中转2——火车：坐'+$scope.traffic[b].flt_no+'次火车，从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发、'+$scope.traffic[b].arrival_time+'到达。共'+$scope.traffic[b].total_time+'分钟。'+$scope.feijistr
                                        }else if(res.data.return_mode==2){//轮船
                                            $scope.trafficstr='中转2——轮船：从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发。' + $scope.traffic[b].remarks                                    
                                        }else if(res.data.return_mode==3){//汽车
                                            $scope.trafficstr='中转2——汽车：从'+$scope.traffic[b].departure_city+'，'+$scope.traffic[b].departure_time+'出发。'+ $scope.traffic[b].remarks                                    
                                        }
                                    }
                               }else if($scope.traffic[b].reference_flight==0){
                                   if(res.data.return_mode==0){//飞机
                                        $scope.trafficstr='返程——飞机：从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'。'
                                    }else if(res.data.return_mode==1){//火车
                                        $scope.trafficstr='返程——火车：从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'。'
                                    }else if(res.data.return_mode==2){//轮船
                                        $scope.trafficstr='返程——轮船：从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'。'
                                    }else if(res.data.return_mode==3){//汽车
                                        $scope.trafficstr='返程——汽车：从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'。'
                                    }
                               }
                               $scope.trafficarr.push($scope.trafficstr); 
                           }
                           
                       }
                       if($scope.traffic[b].traffic_info==2){//交通信息几
                           if($scope.traffic[b].mode==0){//去程还是返程
                               if($scope.traffic[b].transfer==0){
                                    $scope.jiaotongnum='交通信息2：';
                                    $scope.trafficarr.push($scope.jiaotongnum);
                               }
                               if($scope.traffic[b].reference_flight==1){//是否有产考信息
                                    if($scope.traffic[b].transfer==0){//往返交通中是中转第几次
                                        if(res.data.departure_mode==0){//飞机
                                            if($scope.traffic[b].shipping_space==0){
                                                $scope.feijistr='经济舱。';
                                            }else if($scope.traffic[b].shipping_space==1){
                                                $scope.feijistr='商务舱。';
                                            }else if($scope.traffic[b].shipping_space==2){
                                                $scope.feijistr='头等舱。';
                                            }
                                            $scope.trafficstr='去程——飞机：坐'+$scope.traffic[b].airline_company+'的'+$scope.traffic[b].flt_no+'班次飞机，从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发、'+$scope.traffic[b].arrival_time+'到达。共'+$scope.traffic[b].total_time+'分钟。'+$scope.feijistr
                                        }else if(res.data.departure_mode==1){//火车
                                            if($scope.traffic[b].shipping_space==0){
                                                $scope.feijistr='一等座。';
                                            }else if($scope.traffic[b].shipping_space==1){
                                                $scope.feijistr='二等座。';
                                            }else if($scope.traffic[b].shipping_space==2){
                                                $scope.feijistr='站票。';
                                            }
                                            $scope.trafficstr='去程——火车：坐'+$scope.traffic[b].flt_no+'次火车，从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发、'+$scope.traffic[b].arrival_time+'到达。共'+$scope.traffic[b].total_time+'分钟。'+$scope.feijistr
                                        }else if(res.data.departure_mode==2){//轮船
                                            $scope.trafficstr='去程——轮船：从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发。' + $scope.traffic[b].remarks                                   
                                        }else if(res.data.departure_mode==3){//汽车
                                            $scope.trafficstr='去程——汽车：从'+$scope.traffic[b].departure_city+'，'+$scope.traffic[b].departure_time+'出发。'+ $scope.traffic[b].remarks                                   
                                        }
                                    }else if($scope.traffic[b].transfer==1){//往返交通中是中转第几次
                                        if(res.data.departure_mode==0){//飞机
                                            if($scope.traffic[b].shipping_space==0){
                                                $scope.feijistr='经济舱。';
                                            }else if($scope.traffic[b].shipping_space==1){
                                                $scope.feijistr='商务舱。';
                                            }else if($scope.traffic[b].shipping_space==2){
                                                $scope.feijistr='头等舱。';
                                            }
                                            $scope.trafficstr='中转1——飞机：坐'+$scope.traffic[b].airline_company+'的'+$scope.traffic[b].flt_no+'班次飞机，从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发、'+$scope.traffic[b].arrival_time+'到达。共'+$scope.traffic[b].total_time+'分钟。'+$scope.feijistr
                                        }else if(res.data.departure_mode==1){//火车
                                            if($scope.traffic[b].shipping_space==0){
                                                $scope.feijistr='一等座。';
                                            }else if($scope.traffic[b].shipping_space==1){
                                                $scope.feijistr='二等座。';
                                            }else if($scope.traffic[b].shipping_space==2){
                                                $scope.feijistr='站票。';
                                            }
                                            $scope.trafficstr='中转1——火车：坐'+$scope.traffic[b].flt_no+'次火车，从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发、'+$scope.traffic[b].arrival_time+'到达。共'+$scope.traffic[b].total_time+'分钟。'+$scope.feijistr
                                        }else if(res.data.departure_mode==2){//轮船
                                            $scope.trafficstr='中转1——轮船：从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发。' + $scope.traffic[b].remarks                                   
                                        }else if(res.data.departure_mode==3){//汽车
                                            $scope.trafficstr='中转1——汽车：从'+$scope.traffic[b].departure_city+'，'+$scope.traffic[b].departure_time+'出发。'+ $scope.traffic[b].remarks                                    
                                        }
                                    }else if($scope.traffic[b].transfer==2){//往返交通中是中转第几次
                                        if(res.data.departure_mode==0){//飞机
                                            if($scope.traffic[b].shipping_space==0){
                                                $scope.feijistr='经济舱。';
                                            }else if($scope.traffic[b].shipping_space==1){
                                                $scope.feijistr='商务舱。';
                                            }else if($scope.traffic[b].shipping_space==2){
                                                $scope.feijistr='头等舱。';
                                            }
                                            $scope.trafficstr='中转2——飞机：坐'+$scope.traffic[b].airline_company+'的'+$scope.traffic[b].flt_no+'班次飞机，从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发、'+$scope.traffic[b].arrival_time+'到达。共'+$scope.traffic[b].total_time+'分钟。'+$scope.feijistr
                                        }else if(res.data.departure_mode==1){//火车
                                            if($scope.traffic[b].shipping_space==0){
                                                $scope.feijistr='一等座。';
                                            }else if($scope.traffic[b].shipping_space==1){
                                                $scope.feijistr='二等座。';
                                            }else if($scope.traffic[b].shipping_space==2){
                                                $scope.feijistr='站票。';
                                            }
                                            $scope.trafficstr='中转2——火车：坐'+$scope.traffic[b].flt_no+'次火车，从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发、'+$scope.traffic[b].arrival_time+'到达。共'+$scope.traffic[b].total_time+'分钟。'+$scope.feijistr
                                        }else if(res.data.departure_mode==2){//轮船
                                            $scope.trafficstr='中转2——轮船：从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发。' + $scope.traffic[b].remarks                                    
                                        }else if(res.data.departure_mode==3){//汽车
                                            $scope.trafficstr='中转2——汽车：从'+$scope.traffic[b].departure_city+'，'+$scope.traffic[b].departure_time+'出发。'+ $scope.traffic[b].remarks                                    
                                        }
                                    }
                               }else if($scope.traffic[b].reference_flight==0){
                                   if(res.data.departure_mode==0){//飞机
                                        $scope.trafficstr='去程——飞机：从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'。'
                                    }else if(res.data.departure_mode==1){//火车
                                        $scope.trafficstr='去程——火车：从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'。'
                                    }else if(res.data.departure_mode==2){//轮船
                                        $scope.trafficstr='去程——轮船：从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'。'
                                    }else if(res.data.departure_mode==3){//汽车
                                        $scope.trafficstr='去程——汽车：从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'。'
                                    }
                               }
                               $scope.trafficarr.push($scope.trafficstr);                                   
                           }else if($scope.traffic[b].mode==1){//去程还是返程
                                if($scope.traffic[b].reference_flight==1){//是否有产考信息
                                    if($scope.traffic[b].transfer==0){//往返交通中是中转第几次
                                        if(res.data.return_mode==0){//飞机
                                            if($scope.traffic[b].shipping_space==0){
                                                $scope.feijistr='经济舱。';
                                            }else if($scope.traffic[b].shipping_space==1){
                                                $scope.feijistr='商务舱。';
                                            }else if($scope.traffic[b].shipping_space==2){
                                                $scope.feijistr='头等舱。';
                                            }
                                            $scope.trafficstr='返程——飞机：坐'+$scope.traffic[b].airline_company+'的'+$scope.traffic[b].flt_no+'班次飞机，从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发、'+$scope.traffic[b].arrival_time+'到达。共'+$scope.traffic[b].total_time+'分钟。'+$scope.feijistr
                                        }else if(res.data.return_mode==1){//火车
                                            if($scope.traffic[b].shipping_space==0){
                                                $scope.feijistr='一等座。';
                                            }else if($scope.traffic[b].shipping_space==1){
                                                $scope.feijistr='二等座。';
                                            }else if($scope.traffic[b].shipping_space==2){
                                                $scope.feijistr='站票。';
                                            }
                                            $scope.trafficstr='返程——火车：坐'+$scope.traffic[b].flt_no+'次火车，从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发、'+$scope.traffic[b].arrival_time+'到达。共'+$scope.traffic[b].total_time+'分钟。'+$scope.feijistr
                                        }else if(res.data.return_mode==2){//轮船
                                            $scope.trafficstr='返程——轮船：从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发。' + $scope.traffic[b].remarks                                    
                                        }else if(res.data.return_mode==3){//汽车
                                            $scope.trafficstr='返程——汽车：从'+$scope.traffic[b].departure_city+'，'+$scope.traffic[b].departure_time+'出发。'+ $scope.traffic[b].remarks                                    
                                        }
                                    }else if($scope.traffic[b].transfer==1){//往返交通中是中转第几次
                                        if(res.data.return_mode==0){//飞机
                                            if($scope.traffic[b].shipping_space==0){
                                                $scope.feijistr='经济舱。';
                                            }else if($scope.traffic[b].shipping_space==1){
                                                $scope.feijistr='商务舱。';
                                            }else if($scope.traffic[b].shipping_space==2){
                                                $scope.feijistr='头等舱。';
                                            }
                                            $scope.trafficstr='中转1——飞机：坐'+$scope.traffic[b].airline_company+'的'+$scope.traffic[b].flt_no+'班次飞机，从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发、'+$scope.traffic[b].arrival_time+'到达。共'+$scope.traffic[b].total_time+'分钟。'+$scope.feijistr
                                        }else if(res.data.return_mode==1){//火车
                                            if($scope.traffic[b].shipping_space==0){
                                                $scope.feijistr='一等座。';
                                            }else if($scope.traffic[b].shipping_space==1){
                                                $scope.feijistr='二等座。';
                                            }else if($scope.traffic[b].shipping_space==2){
                                                $scope.feijistr='站票。';
                                            }
                                            $scope.trafficstr='中转1——火车：坐'+$scope.traffic[b].flt_no+'班次飞机，从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发、'+$scope.traffic[b].arrival_time+'到达。共'+$scope.traffic[b].total_time+'分钟。'+$scope.feijistr
                                        }else if(res.data.return_mode==2){//轮船
                                            $scope.trafficstr='中转1——轮船：从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发。' + $scope.traffic[b].remarks                                    
                                        }else if(res.data.return_mode==3){//汽车
                                            $scope.trafficstr='中转1——汽车：从'+$scope.traffic[b].departure_city+'，'+$scope.traffic[b].departure_time+'出发。'+ $scope.traffic[b].remarks                                    
                                        }
                                    }else if($scope.traffic[b].transfer==2){//往返交通中是中转第几次
                                        if(res.data.return_mode==0){//飞机
                                            if($scope.traffic[b].shipping_space==0){
                                                $scope.feijistr='经济舱。';
                                            }else if($scope.traffic[b].shipping_space==1){
                                                $scope.feijistr='商务舱。';
                                            }else if($scope.traffic[b].shipping_space==2){
                                                $scope.feijistr='头等舱。';
                                            }
                                            $scope.trafficstr='中转2——飞机：坐'+$scope.traffic[b].airline_company+'的'+$scope.traffic[b].flt_no+'班次飞机，从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发、'+$scope.traffic[b].arrival_time+'到达。共'+$scope.traffic[b].total_time+'分钟。'+$scope.feijistr
                                        }else if(res.data.return_mode==1){//火车
                                            if($scope.traffic[b].shipping_space==0){
                                                $scope.feijistr='一等座。';
                                            }else if($scope.traffic[b].shipping_space==1){
                                                $scope.feijistr='二等座。';
                                            }else if($scope.traffic[b].shipping_space==2){
                                                $scope.feijistr='站票。';
                                            }
                                            $scope.trafficstr='中转2——火车：坐'+$scope.traffic[b].flt_no+'次火车，从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发、'+$scope.traffic[b].arrival_time+'到达。共'+$scope.traffic[b].total_time+'分钟。'+$scope.feijistr
                                        }else if(res.data.return_mode==2){//轮船
                                            $scope.trafficstr='中转2——轮船：从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发。' + $scope.traffic[b].remarks                                    
                                        }else if(res.data.return_mode==3){//汽车
                                            $scope.trafficstr='中转2——汽车：从'+$scope.traffic[b].departure_city+'，'+$scope.traffic[b].departure_time+'出发。'+ $scope.traffic[b].remarks                                    
                                        }
                                    }
                               }else if($scope.traffic[b].reference_flight==0){
                                   if(res.data.return_mode==0){//飞机
                                        $scope.trafficstr='返程——飞机：从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'。'
                                    }else if(res.data.return_mode==1){//火车
                                        $scope.trafficstr='返程——火车：从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'。'
                                    }else if(res.data.return_mode==2){//轮船
                                        $scope.trafficstr='返程——轮船：从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'。'
                                    }else if(res.data.return_mode==3){//汽车
                                        $scope.trafficstr='返程——汽车：从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'。'
                                    }
                               }
                               $scope.trafficarr.push($scope.trafficstr); 
                           }
                           
                       }
                       if($scope.traffic[b].traffic_info==3){//交通信息几
                           if($scope.traffic[b].mode==0){//去程还是返程
                               if($scope.traffic[b].transfer==0){
                                    $scope.jiaotongnum='交通信息3：';
                                    $scope.trafficarr.push($scope.jiaotongnum);
                               }
                               if($scope.traffic[b].reference_flight==1){//是否有产考信息
                                    if($scope.traffic[b].transfer==0){//往返交通中是中转第几次
                                       
                                        if(res.data.departure_mode==0){//飞机
                                            if($scope.traffic[b].shipping_space==0){
                                                $scope.feijistr='经济舱。';
                                            }else if($scope.traffic[b].shipping_space==1){
                                                $scope.feijistr='商务舱。';
                                            }else if($scope.traffic[b].shipping_space==2){
                                                $scope.feijistr='头等舱。';
                                            }
                                            $scope.trafficstr='去程——飞机：坐'+$scope.traffic[b].airline_company+'的'+$scope.traffic[b].flt_no+'班次飞机，从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发、'+$scope.traffic[b].arrival_time+'到达。共'+$scope.traffic[b].total_time+'分钟。'+$scope.feijistr
                                        }else if(res.data.departure_mode==1){//火车
                                            if($scope.traffic[b].shipping_space==0){
                                                $scope.feijistr='一等座。';
                                            }else if($scope.traffic[b].shipping_space==1){
                                                $scope.feijistr='二等座。';
                                            }else if($scope.traffic[b].shipping_space==2){
                                                $scope.feijistr='站票。';
                                            }
                                            $scope.trafficstr='去程——火车：坐'+$scope.traffic[b].flt_no+'次火车，从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发、'+$scope.traffic[b].arrival_time+'到达。共'+$scope.traffic[b].total_time+'分钟。'+$scope.feijistr
                                        }else if(res.data.departure_mode==2){//轮船
                                            $scope.trafficstr='去程——轮船：从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发。' + $scope.traffic[b].remarks                                   
                                        }else if(res.data.departure_mode==3){//汽车
                                            $scope.trafficstr='去程——汽车：从'+$scope.traffic[b].departure_city+'，'+$scope.traffic[b].departure_time+'出发。'+ $scope.traffic[b].remarks                                   
                                        }
                                    }else if($scope.traffic[b].transfer==1){//往返交通中是中转第几次
                                        if(res.data.departure_mode==0){//飞机
                                            if($scope.traffic[b].shipping_space==0){
                                                $scope.feijistr='经济舱。';
                                            }else if($scope.traffic[b].shipping_space==1){
                                                $scope.feijistr='商务舱。';
                                            }else if($scope.traffic[b].shipping_space==2){
                                                $scope.feijistr='头等舱。';
                                            }
                                            $scope.trafficstr='中转1——飞机：坐'+$scope.traffic[b].airline_company+'的'+$scope.traffic[b].flt_no+'班次飞机，从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发、'+$scope.traffic[b].arrival_time+'到达。共'+$scope.traffic[b].total_time+'分钟。'+$scope.feijistr
                                        }else if(res.data.departure_mode==1){//火车
                                            if($scope.traffic[b].shipping_space==0){
                                                $scope.feijistr='一等座。';
                                            }else if($scope.traffic[b].shipping_space==1){
                                                $scope.feijistr='二等座。';
                                            }else if($scope.traffic[b].shipping_space==2){
                                                $scope.feijistr='站票。';
                                            }
                                            $scope.trafficstr='中转1——火车：坐'+$scope.traffic[b].flt_no+'次火车，从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发、'+$scope.traffic[b].arrival_time+'到达。共'+$scope.traffic[b].total_time+'分钟。'+$scope.feijistr
                                        }else if(res.data.departure_mode==2){//轮船
                                            $scope.trafficstr='中转1——轮船：从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发。' + $scope.traffic[b].remarks                                   
                                        }else if(res.data.departure_mode==3){//汽车
                                            $scope.trafficstr='中转1——汽车：从'+$scope.traffic[b].departure_city+'，'+$scope.traffic[b].departure_time+'出发。'+ $scope.traffic[b].remarks                                    
                                        }
                                    }else if($scope.traffic[b].transfer==2){//往返交通中是中转第几次
                                        if(res.data.departure_mode==0){//飞机
                                            if($scope.traffic[b].shipping_space==0){
                                                $scope.feijistr='经济舱。';
                                            }else if($scope.traffic[b].shipping_space==1){
                                                $scope.feijistr='商务舱。';
                                            }else if($scope.traffic[b].shipping_space==2){
                                                $scope.feijistr='头等舱。';
                                            }
                                            $scope.trafficstr='中转2——飞机：坐'+$scope.traffic[b].airline_company+'的'+$scope.traffic[b].flt_no+'班次飞机，从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发、'+$scope.traffic[b].arrival_time+'到达。共'+$scope.traffic[b].total_time+'分钟。'+$scope.feijistr
                                        }else if(res.data.departure_mode==1){//火车
                                            if($scope.traffic[b].shipping_space==0){
                                                $scope.feijistr='一等座。';
                                            }else if($scope.traffic[b].shipping_space==1){
                                                $scope.feijistr='二等座。';
                                            }else if($scope.traffic[b].shipping_space==2){
                                                $scope.feijistr='站票。';
                                            }
                                            $scope.trafficstr='中转2——火车：坐'+$scope.traffic[b].flt_no+'次火车，从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发、'+$scope.traffic[b].arrival_time+'到达。共'+$scope.traffic[b].total_time+'分钟。'+$scope.feijistr
                                        }else if(res.data.departure_mode==2){//轮船
                                            $scope.trafficstr='中转2——轮船：从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发。' + $scope.traffic[b].remarks                                    
                                        }else if(res.data.departure_mode==3){//汽车
                                            $scope.trafficstr='中转2——汽车：从'+$scope.traffic[b].departure_city+'，'+$scope.traffic[b].departure_time+'出发。'+ $scope.traffic[b].remarks                                    
                                        }
                                    }
                               }else if($scope.traffic[b].reference_flight==0){
                                   if(res.data.departure_mode==0){//飞机
                                        $scope.trafficstr='去程——飞机：从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'。'
                                    }else if(res.data.departure_mode==1){//火车
                                        $scope.trafficstr='去程——火车：从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'。'
                                    }else if(res.data.departure_mode==2){//轮船
                                        $scope.trafficstr='去程——轮船：从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'。'
                                    }else if(res.data.departure_mode==3){//汽车
                                        $scope.trafficstr='去程——汽车：从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'。'
                                    }
                               }
                               $scope.trafficarr.push($scope.trafficstr);                                   
                           }else if($scope.traffic[b].mode==1){//去程还是返程
                                if($scope.traffic[b].reference_flight==1){//是否有产考信息
                                    if($scope.traffic[b].transfer==0){//往返交通中是中转第几次
                                        if(res.data.return_mode==0){//飞机
                                            if($scope.traffic[b].shipping_space==0){
                                                $scope.feijistr='经济舱。';
                                            }else if($scope.traffic[b].shipping_space==1){
                                                $scope.feijistr='商务舱。';
                                            }else if($scope.traffic[b].shipping_space==2){
                                                $scope.feijistr='头等舱。';
                                            }
                                            $scope.trafficstr='返程——飞机：坐'+$scope.traffic[b].airline_company+'的'+$scope.traffic[b].flt_no+'班次飞机，从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发、'+$scope.traffic[b].arrival_time+'到达。共'+$scope.traffic[b].total_time+'分钟。'+$scope.feijistr
                                        }else if(res.data.return_mode==1){//火车
                                            if($scope.traffic[b].shipping_space==0){
                                                $scope.feijistr='一等座。';
                                            }else if($scope.traffic[b].shipping_space==1){
                                                $scope.feijistr='二等座。';
                                            }else if($scope.traffic[b].shipping_space==2){
                                                $scope.feijistr='站票。';
                                            }
                                            $scope.trafficstr='返程——火车：坐'+$scope.traffic[b].flt_no+'次火车，从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发、'+$scope.traffic[b].arrival_time+'到达。共'+$scope.traffic[b].total_time+'分钟。'+$scope.feijistr
                                        }else if(res.data.return_mode==2){//轮船
                                            $scope.trafficstr='返程——轮船：从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发。' + $scope.traffic[b].remarks                                    
                                        }else if(res.data.return_mode==3){//汽车
                                            $scope.trafficstr='返程——汽车：从'+$scope.traffic[b].departure_city+'，'+$scope.traffic[b].departure_time+'出发。'+ $scope.traffic[b].remarks                                    
                                        }
                                    }else if($scope.traffic[b].transfer==1){//往返交通中是中转第几次
                                        if(res.data.return_mode==0){//飞机
                                            if($scope.traffic[b].shipping_space==0){
                                                $scope.feijistr='经济舱。';
                                            }else if($scope.traffic[b].shipping_space==1){
                                                $scope.feijistr='商务舱。';
                                            }else if($scope.traffic[b].shipping_space==2){
                                                $scope.feijistr='头等舱。';
                                            }
                                            $scope.trafficstr='中转1——飞机：坐'+$scope.traffic[b].airline_company+'的'+$scope.traffic[b].flt_no+'班次飞机，从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发、'+$scope.traffic[b].arrival_time+'到达。共'+$scope.traffic[b].total_time+'分钟。'+$scope.feijistr
                                        }else if(res.data.return_mode==1){//火车
                                            if($scope.traffic[b].shipping_space==0){
                                                $scope.feijistr='一等座。';
                                            }else if($scope.traffic[b].shipping_space==1){
                                                $scope.feijistr='二等座。';
                                            }else if($scope.traffic[b].shipping_space==2){
                                                $scope.feijistr='站票。';
                                            }
                                            $scope.trafficstr='中转1——火车：坐'+$scope.traffic[b].flt_no+'班次飞机，从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发、'+$scope.traffic[b].arrival_time+'到达。共'+$scope.traffic[b].total_time+'分钟。'+$scope.feijistr
                                        }else if(res.data.return_mode==2){//轮船
                                            $scope.trafficstr='中转1——轮船：从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发。' + $scope.traffic[b].remarks                                    
                                        }else if(res.data.return_mode==3){//汽车
                                            $scope.trafficstr='中转1——汽车：从'+$scope.traffic[b].departure_city+'，'+$scope.traffic[b].departure_time+'出发。'+ $scope.traffic[b].remarks                                    
                                        }
                                    }else if($scope.traffic[b].transfer==2){//往返交通中是中转第几次
                                        if(res.data.return_mode==0){//飞机
                                            if($scope.traffic[b].shipping_space==0){
                                                $scope.feijistr='经济舱。';
                                            }else if($scope.traffic[b].shipping_space==1){
                                                $scope.feijistr='商务舱。';
                                            }else if($scope.traffic[b].shipping_space==2){
                                                $scope.feijistr='头等舱。';
                                            }
                                            $scope.trafficstr='中转2——飞机：坐'+$scope.traffic[b].airline_company+'的'+$scope.traffic[b].flt_no+'班次飞机，从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发、'+$scope.traffic[b].arrival_time+'到达。共'+$scope.traffic[b].total_time+'分钟。'+$scope.feijistr
                                        }else if(res.data.return_mode==1){//火车
                                            if($scope.traffic[b].shipping_space==0){
                                                $scope.feijistr='一等座。';
                                            }else if($scope.traffic[b].shipping_space==1){
                                                $scope.feijistr='二等座。';
                                            }else if($scope.traffic[b].shipping_space==2){
                                                $scope.feijistr='站票。';
                                            }
                                            $scope.trafficstr='中转2——火车：坐'+$scope.traffic[b].flt_no+'次火车，从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发、'+$scope.traffic[b].arrival_time+'到达。共'+$scope.traffic[b].total_time+'分钟。'+$scope.feijistr
                                        }else if(res.data.return_mode==2){//轮船
                                            $scope.trafficstr='中转2——轮船：从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'，'+$scope.traffic[b].departure_time+'出发。' + $scope.traffic[b].remarks                                    
                                        }else if(res.data.return_mode==3){//汽车
                                            $scope.trafficstr='中转2——汽车：从'+$scope.traffic[b].departure_city+'，'+$scope.traffic[b].departure_time+'出发。'+ $scope.traffic[b].remarks                                    
                                        }
                                    }
                               }else if($scope.traffic[b].reference_flight==0){
                                   if(res.data.return_mode==0){//飞机
                                        $scope.trafficstr='返程——飞机：从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'。'
                                    }else if(res.data.return_mode==1){//火车
                                        $scope.trafficstr='返程——火车：从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'。'
                                    }else if(res.data.return_mode==2){//轮船
                                        $scope.trafficstr='返程——轮船：从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'。'
                                    }else if(res.data.return_mode==3){//汽车
                                        $scope.trafficstr='返程——汽车：从'+$scope.traffic[b].departure_city+'到'+$scope.traffic[b].arrival_city+'。'
                                    }
                               }
                               $scope.trafficarr.push($scope.trafficstr); 
                           }
                           
                       }
                       
                    }

                    



                }else{
                    alert(res.errmsg);
                }
            });
            


            $scope.cancel = function () {  
                $modalInstance.dismiss('cancel'); 
            };
           

            
 
};