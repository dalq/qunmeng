/**
 * 子模块service
 * dlq
 */
var service = function ($resource,  $q, $http) {

	//公告列表
	var noticelist = "/api/as/tc/notice/list";
	//检测初始密码
	var checkPass = "/api/ac/sc/systemUserService/checkMobilePass";

	//公告详情
	var noticeinfo = "/api/as/tc/notice/info";

	var userinfo = "/api/as/info";

    //获取商户数据通过code
	var getSellerInfoByCode =  '/api/ac/tc/bmAccountService/setSellerInfoByCode';

	//查询充值记录
	var getSellerTrackInfoByCodeList =  '/api/as/tc/bmaccounttrack/getSellerTrackInfoByCodeList';


	return {

		updateInfo: function () {
			return $resource(updateInfo, {}, {});
		},
		checkPass: function () {
			return $resource(checkPass, {}, {});
		},
		noticelist: function () {
			return $resource(noticelist, {}, {});
		},
		noticeinfo: function () {
			return $resource(noticeinfo, {}, {});
		},
		userinfo: function () {
			var deferred = $q.defer(); // 声明延后执行，表示要去监控后面的执行  
			// $http({method: 'GET', url: userinfo}).
			// success(function(data, status, headers, config) {  
			//   deferred.resolve(data);  // 声明执行成功，即http请求数据成功，可以返回数据了  
			// }).  
			// error(function(data, status, headers, config) {  
			//   deferred.reject(data);   // 声明执行失败，即服务器返回错误  
			// });  
			$http({ method: 'GET', url: userinfo }).then(
				function (data) {
					deferred.resolve(data.data);
				},
				function (data) {
					deferred.reject(data.data);
				}
			);
			return deferred.promise;   // 返回承诺，这里并不是最终数据，而是访问最终数据的API  
			//return $resource(createinsuranceapi, {}, {});
		},
        getSellerInfoByCode : function(){
            return $resource(getSellerInfoByCode, {}, {});
        },
        trackinfo : function(){
            return $resource(getSellerTrackInfoByCodeList, {}, {});
        }

	};

};

module.exports = service;