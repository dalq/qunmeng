var App1 = angular.module('dashboard', [
     'ui.router',
     'ngResource',
     "oc.lazyLoad"
    // 'constant'
]);

App1.config(require('./router'));
App1.factory('dashboardservice', require('./service'));

App1.controller('supplier_dashboard',require('./controllers/supplier_dashboard'));
App1.controller('qmnoticeinfo',require('./controllers/noticeinfo'));
App1.controller('trackinfo',require('./controllers/trackinfo'));
App1.controller('setPassword',require('./controllers/setPassword'));

module.exports = App1;