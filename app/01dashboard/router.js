/**
 * 子模块路由
 * dlq
 */

var router = function ($urlRouterProvider, $stateProvider) {

	$stateProvider

		//供应商
		.state('app.supplier_dashboard', {
			url: '/supplier_dashboard',
			//template: require('./views/supplier_dashboard.html'),
			//templateUrl: './views/supplier_dashboard.html',
			// resolve: {
			// 	deps: ['$ocLazyLoad',function( $ocLazyLoad ){
			// 		return $ocLazyLoad.load(['./app/998mstp/js/controllers/chart.js']);
			// 	}]
			// }
			views: {

				'main@': {
					template: require('./views/supplier_dashboard.html'),
					controller: 'supplier_dashboard'
				}
			},
			resolve: {
				dataScope: function (utilservice) {
                    return utilservice.dataScope;
                },
				date2str: function (utilservice) {
                    return utilservice.date2str;
                },
                
				// checkPass: function (dashboardservice) {
				// 	return dashboardservice.checkPass();
				// },
				// noticelist: function (dashboardservice) {
				// 	return dashboardservice.noticelist();
				// },
				// noticeinfo: function (dashboardservice) {
				// 	return dashboardservice.noticeinfo();
				// },
				// userinfo: function (dashboardservice) {
				// 	return dashboardservice.userinfo;
				// },
				// getSellerInfoByCode: function (dashboardservice) {
				// 	return dashboardservice.getSellerInfoByCode();
				// }
			}
		})


		//供应商
		.state('app.trackinfo', {
			url: '/trackinfo/:seller_code',
			//template: require('./views/supplier_dashboard.html'),
			//templateUrl: './views/supplier_dashboard.html',
			// resolve: {
			// 	deps: ['$ocLazyLoad',function( $ocLazyLoad ){
			// 		return $ocLazyLoad.load(['./app/998mstp/js/controllers/chart.js']);
			// 	}]
			// }
			views: {
				'main@': {
					template: require('./views/trackinfo.html'),
					controller: 'trackinfo'
				}
			},
			resolve: {
				trackinfo: function (dashboardservice) {
					return dashboardservice.trackinfo();
				}
			}
		})



		//分销商
		.state('app.distributor_dashboard', {
			url: '/supplier_dashboard',
			views: {

				'main@': {
					template: require('./views/distributor_dashboard.html'),
					//controller : 'supplier_dashboard'
				}
			}
		})


		;

};

module.exports = router;