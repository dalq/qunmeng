module.exports = function ($scope, $resource, dataScope, date2str, $modal, $state) {

	console.log("%c 群盟 %c 做地球上最具互联网性、创新性、专业有爱的团队。 \xa9 2015-%d", 'font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;font-size:64px;color:#00bbee;-webkit-text-fill-color:#00bbee;-webkit-text-stroke: 1px #00bbee;', "font-size:12px;color:#999999;", (new Date).getFullYear());

	$scope.info = {
		'member' : {},
		'profit' : {},
		'salesranking' : [],
		'profitranking' : [],
		'noticelist' : [],
	};

	$scope.chartdata = {
		'profit' : [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
		'member' : [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
		'structure' : [0, 0, 0],
	}

	//排行和利润的切换开关,0:销量，1:利润
	$scope.what = '0';

	//------------ 获取数据 -----------------------------//
	//会员统计
	$resource('/api/ac/uc/userWxService/memberStatistics', {}, {}).save({}, function (res) {
		console.log(res);

		if(res.errcode != 0){
			//alert(res.errmsg);
			return;
		}

		$scope.info.member = res.data;

		for(var i = 0; i < res.data.seven_num.length; i++){
			var tmp = res.data.seven_num[i];
			$scope.chartdata.member[i] = tmp.num;
		}

		//一级
		$scope.chartdata.structure[0] = res.data.one_distributor_num;
		//二级
		$scope.chartdata.structure[1] = res.data.two_distributor_num;
		//普通会员
		$scope.chartdata.structure[2] = res.data.total_num - res.data.one_distributor_num - res.data.two_distributor_num - 1;
	});

	//利润统计
	$resource('/api/ac/wc/productOrderStatisticsService/profitStatistics', {}, {}).save({}, function (res) {
		console.log(res);

		if(res.errcode != 0){
			alert(res.errmsg);
			return;
		}

		$scope.info.profit = res.data;

		for(var i = 0; i < res.data.seven_num.length; i++){
		//for(var i = 0; i < 7; i++){
			var tmp = res.data.seven_num[i];
			$scope.chartdata.profit[i] = tmp.total_price;
		}

	});

	//订单统计
	$resource('/api/ac/wc/productOrderStatisticsService/orderStatistics', {}, {}).save({}, function (res) {
		console.log(res);

		if(res.errcode != 0){
			alert(res.errmsg);
			return;
		}

		$scope.info.order = res.data;

	});


	//销量排名
	$resource('/api/as/wc/productorderstatistics/salesrankinglist', {}, {}).save({
		pageNo: 1,
    	pageSize: 10,
	}, function (res) {
		console.log('销量排名');
		console.log(res);

		if(res.errcode != 0){
			alert(res.errmsg);
			return;
		}

		$scope.info.salesranking = res.data.results;

	});

	//利润排名
	$resource('/api/as/wc/productorderstatistics/profitrankinglist', {}, {}).save({
		pageNo: 1,
    	pageSize: 10,
	}, function (res) {
		console.log('利润排名');
		console.log(res);

		if(res.errcode != 0){
			alert(res.errmsg);
			return;
		}

		$scope.info.profitranking = res.data.results;

	});

	//公告列表
	$resource('/api/as/wc/notice/noticelist', {}, {}).save({
		pageNo: 1,
    	pageSize: 10,
	}, function (res) {
		console.log('公告列表');
		console.log(res);

		if(res.errcode != 0){
			alert(res.errmsg);
			return;
		}

		$scope.info.noticelist = res.data.results;

	});


	//查询机构的平台预存
	$resource('/api/ac/pc/payBmAccountService/setPlatfromPrice', {}, {}).save({}, function(res){
		console.log('//查询机构的平台预存');
		console.log(res);
		if(res.errcode === 0){
			$scope.platfomBalance = res.data.balance * 0.01;
			$scope.balance = {
				'balance': res.data.balance * 0.01,
				'send_sms_price': res.data.send_sms_price * 0.01,
				'send_sms_flag': res.data.send_sms_flag,
				'supplier_company_code': 'SYS',
				'deposit_system': '0'
			};
			
		}
	});
	

	//------------ 获取数据 -----------------------------//






	//------------ 生成图表 -----------------------------//

	var now = new Date();
	var nowxx = new Date();
	now.setDate(now.getDate());
	nowxx.setDate(now.getDate() - 13);

	var s = date2str(nowxx);
	var e = date2str(now);

	var arr = dataScope(s, e);
	
	for(var i = 0; i < arr.length; i++){
		arr[i] = arr[i].substring(5);
	}

	console.log(arr);

	//横坐标
	$scope.labels = arr;

	//会员组成
	$scope.structure = {
		'labels' : ["一级分销", "二级分销", "普通会员"],
		'options' : {
			legend: {
	            display : true,
	            position : 'left',
	            // labels: {
	            //     fontColor: 'rgb(255, 99, 132)'
	            // }
	        }
		}
	};

	//------------ 生成图表 -----------------------------//


	//------------ 界面操作 -----------------------------//
	$scope.change = function(){
		$scope.what = $scope.what == '0' ? '1' : '0'; 
	}

	$scope.noticeinfo = function(obj){
		var modalInstance = $modal.open({
          template: require('../views/noticeinfo.html'),
          controller: 'qmnoticeinfo',
          //size: 'lg',
          resolve: {
            obj : function(){
            	return obj;
            }
          }
        });
	};

	$scope.recharge = function(){
		$state.go('app.platform_deposit');
	}
	
	$scope.detail = function(){
		$state.go('app.transaction', {'deposit_system': '0'});
	}

	//设置平台余额预警提醒
	$scope.setBalanceInfo = function() {
		console.log('setBalanceInfo');
		var modalInstance = $modal.open({
			template: require('../../13deposit/views/depositEdit.html'),
			controller: 'depositEdit',
			size: 'lg',
			resolve: {
				obj: function () {
					return $scope.balance;
				}
			}
		});
		modalInstance.result.then(function(result) {
			$scope.balance.send_sms_price = result.send_sms_price;
			$scope.balance.send_sms_flag = result.send_sms_flag;
		});
	}

	//------------ 界面操作 -----------------------------//


	  $scope.options = {
	    scales: {
	      yAxes: [
	        {
	          id: 'y-axis-1',
	          type: 'linear',
	          display: true,
	          position: 'left',
	          ticks: {
                    beginAtZero:true,
              }
	        }
	      ],
	      xAxes : [
	      	{
	      		ticks: {
                    //maxRotation : 0,
              	}
	      	}
	      ],
	    }
	  };


};