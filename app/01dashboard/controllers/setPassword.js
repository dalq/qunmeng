module.exports = function ($scope, $resource, $interval, user, $modalInstance, code, toaster) {

    init();
    //初始化
    function init(){
        $scope.modify = {
            'mobile': user.mobile ? false : true,   //是否绑定手机
            'pass': code == '1'                     //是否是初始密码
        }
        $scope.obj = {};
    }

    //跳过
    $scope.nextStep = function(){
        $scope.modify.mobile = false;
        $scope.obj = {};
    }

    //修改密码
    $scope.save = function(){
        if(!$scope.obj.oldpassword || !$scope.obj.newpassword || !$scope.obj.repassword){
			toaster.warning({title: '', body: '信息不完整'});
			return;
		}
        $resource('/api/ac/sc/systemUserService/updatePwd', {}, {}).save($scope.obj, function (res) {
			if (res.errcode === 0 && res.data.message == 1) {
				alert('密码修改成功,请重新登录!');
				location.href = '/login.html';
			} else {
				toaster.error({title: '', body: res.errmsg});
			}
		});
    }

    //验证短信码
    $scope.validate = function(){
        if(!$scope.obj.code){
            toaster.warning({title: '', body: '验证码不能为空'});
            return;
        }
        $scope.obj.id = user.id;
        $resource('/api/ac/sc/systemUserService/updateMobile', {}, {}).save($scope.obj, function (res) {
			if (res.errcode === 0 && res.data == 1) {
                toaster.success({title: '', body: '绑定手机成功'});
                //绑定手机成功,如果未修改初始密码,则进入修改密码页
                if($scope.modify.pass){
                    $scope.modify.mobile = false;
                } else {
                    $modalInstance.close();
                }
			} else {
				toaster.error({title: '', body: res.errmsg});
			}
		});

    }

    //发送短息验证码
    $scope.sendMsg = function(){
        if(!$scope.obj.mobile || !(/^1[3|4|5|7|8|9]\d{9}$/.test($scope.obj.mobile))){
            toaster.warning({title: '', body: '手机号格式不正确'});
            return;
        }

        $scope.time = {'timeout': 60, 'btnable': true};
        $scope.timer = $interval(function(){
            $scope.time.timeout -= 1;
            if($scope.time.timeout < 0){
                $scope.time.btnable = false;
                $('#send').removeClass('active');
                $interval.cancel($scope.timer);
            }
        }, 1000);
        $resource('/api/ac/sc/systemUserService/sendCode', {}, {}).save($scope.obj, function (res) {
			if (res.errcode === 0) {
                toaster.success({title: '', body: '短信发送成功'});
			} else {
				toaster.error({title: '', body: res.errmsg});
			}
		});
    };
    //关闭当前页面时关闭计时器
    $scope.$on('$destroy',function(){
		$interval.cancel($scope.timer);
	});

    $scope.cancel = function(){
        $modalInstance.dismiss('cancel');
    };

};