var angular = require('angular');
require('angular-animate');
require('angular-cookies');
require('angular-bootstrap');
require('angular-resource');
require('angular-sanitize');
require('angular-touch');
require('ngstorage');
require('angular-ui-router');
require('angular-ui-tree');

require('angular-file-upload');

// require('eonasdan-bootstrap-datetimepicker');
require('angular-eonasdan-datetimepicker');

require('ui-select');
require('../node_modules/ui-select/dist/select.min.css');


require('./998mstp/js/services/ui-load');
require('./998mstp/js/directives/ui-jq');
require('./998mstp/js/directives/ui-validate');

require('./998mstp/js/directives/ui-nav');
require('./998mstp/js/directives/setnganimate');
require('./998mstp/js/directives/ui-butterbar');
require('./998mstp/js/directives/ui-focus');
require('./998mstp/js/directives/ui-fullscreen');
require('./998mstp/js/directives/ui-module');
require('./998mstp/js/directives/ui-scroll');
require('./998mstp/js/directives/ui-shift');
require('./998mstp/js/directives/ui-toggleclass');
require('./998mstp/js/controllers/bootstrap');
require('./998mstp/js/filters/fromNow');

require('angular-translate');
require('oclazyload');
require('angular-datatables');

// ------------ 颜色拾取 ----------------------//
// require('./998mstp/js/minicolors/jquery.minicolors');
// require('./998mstp/js/minicolors/jquery.minicolors.css');
// require('./998mstp/js/minicolors/jquery.minicolors.png');
// require('angular-minicolors');
// ------------ 颜色拾取 ----------------------//

// ------------ 百度富文本编辑器 ----------------------//
// window.UEDITOR_HOME_URL = 'ueditor/';
// require('../node_modules/angular-ueditor/dist/angular-ueditor');
// // ------------ 百度富文本编辑器 ----------------------//

// // ------------ 富文本编辑器 ----------------------//
//require('../node_modules/textangular/dist/textAngular-rangy.min');
require('../node_modules/textangular/dist/textAngular-sanitize.min');
require('../node_modules/textangular/dist/textAngular.min');
require('../node_modules/textangular/dist/textAngular.umd');
require('../node_modules/textangular/dist/textAngularSetup');
require('../node_modules/textangular/dist/textAngular.css');
// ------------ 富文本编辑器 ----------------------//


// ------------ 图表 ----------------------//
require('../node_modules/angular-chart.js/dist/angular-chart.min');
//require('../node_modules/angular-chart.js/dist/angular-chart.min.css');
// ------------ 图表 ----------------------//


// ------------ angular tree ----------------------//
require('../node_modules/angular-tree-control/angular-tree-control');
require('../node_modules/angular-tree-control/css/tree-control.css');
require('../node_modules/angular-tree-control/css/tree-control-attribute.css');
// ------------ angular tree ----------------------//

// ------------ 弹出框 ----------------------//
require('angularjs-toaster');
require('../node_modules/angularjs-toaster/toaster.min.css');
// ------------ 弹出框 ----------------------//


require('angular-drag');


//require('./998mstp/css/bootstrap.css');
require('./998mstp/vendor/jquery/datatables/dataTables.bootstrap.css');
//require('./998mstp/css/font-awesome.min.css');
require('./998mstp/css/simple-line-icons.css');
require('./998mstp/css/font.css');
// require('./998mstp/css/app.css');


require('../node_modules/angular-ui-tree/dist/angular-ui-tree.css');


require('./00pageframework/app');
require('./01dashboard/app');
//require('./02product/app');
require('./03person/app');
require('./04permission/app');
require('./05login/app');
//require('./06view/app');
//require('./07tickettype/app');
//require('./08channelTemp/app');
//require('./09goodsTemp/app');
//require('./10goods/app');
require('./11order/app');
require('./12device/app');
require('./13deposit/app');
//require('./14line/app');
require('./15interface/app');
//require('./16guide/app');
//require('./17module/app');
require('./18menu/app');
//require('./19system/app');
require('./20office/app');
//require('./21dict/app');
//require('./22area/app');
require('./23user/app');
//require('./24redpacket/app');
require('./25role/app');
//require('./26ylb/app'); // 商客组,马超群,葫芦岛游乐宝
require('./28customservice/app');
require('./29product/app');
//require('./30store/app');
//require('./31sign/app');
require('./32ticket/app');
require('./33sale/app');
require('./34statistics/app');
require('./35card/app');
require('./36docking/app');
//require('./36log/app');
//require('./37linestatistics/app');//lc线路统计
//require('./39coupon/app');//优惠券
//require('./40admin/app');//管理员专用 DHpai
//require('./45shangke/app'); // 商客组,马超群,商客大部分功能
//require('./46skstore/app'); // 商客组,马超群,商客商户模块
require('./47notice/app');
//require('./49shangkeexpand/app'); // 商客
require('./50bussmember/app'); // 商户会员
require('./51weshop/app');//微店，dlq，微店功能
//require('./52hldWX/app');//小裙子 葫芦岛微信
//require('./60viewSystem/app'); // 景区会员
//require('./61activity/app'); // 活动
require('./62film/app'); // 影院
//require('./70hoteltype/app');//酒店房型

//require('./994intercept/app');
require('./992test/app');
require('./994directive/app');
require('./995util/app');
require('./996tpl/app');
require('./997test/app');



//------------ 群盟 ----------------------//
require('./001shop/app');
require('./002product/app');
require('./003report/app');
require('./004order/app');
require('./005sysproduct/app');
require('./006member/app');
require('./007appoint/app');




//------------ 群盟 ----------------------//



require('./00pageframework/css/app.css');



//=================[ 权限模块加载 ]===========================//

(require('./04permission/boot'))(jQuery);

//=================[ 权限模块加载 ]===========================//




var juyou = angular.module('app', [
    // 'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngTouch',
    'ngStorage',
    'ui.router',
    'ui.tree',
    'ui.bootstrap',
    'ui.load',
    'ui.jq',
    'ui.validate',
    'ui.nav',
    'oc.lazyLoad',
    'pascalprecht.translate',

    'fromnow',
    'setnganimate',
    'ui.butterbar',
    'ui.focus',
    'ui.fullscreen',
    'ui.module',
    'ui.scroll',
    'ui.shift',
    'ui.select',
    'ui.toggleclass',
    'ctrl.bootstrap',

    'datatables',
    'datatables.bootstrap',
    'datatables.tabletools',
    'datatables.colvis',
    'datatables.light-columnfilter',
    'datatables.columnfilter',

    'chart.js',
    'textAngular',
    'treeControl',
    'angularFileUpload',
    //'ng.ueditor',
    'toaster', 
    'angular-drag',

    'pageframework',
    'dashboard',
    //'productOld',
    'person',
    'permission',
    'login',
    //'view',
    //'tickettype',
    //'channelTemp',
    //'goodsTemp',
    //'goods',
    'order',
    'device',
    'deposit',
    'interface',
    //'system',
    //'module',
    'menu',
    'office',
    //'dict',
    //'area',
    'user',
    'role',
    //'ylb',
    //'line',
    //'redpacket',
    //'equipment',
    'product',
    'custs',
    //'store',
    //'sign',
    //'log',
    //'shangke',
    //'skstore',
    //'shangkeexpand',
    'ticket',
    'notice',
    'sale',
    'statistics',
    //'admin',
    //'activitySystem',
    //'testSystem',
    //'guide',
    'directive',
    'util',
    'juyoutpl',
    'test', 
    'card',
    'docking',
    'ae-datetimepicker',
     //线路统计
    //'linestatistics',
    //专家评审
    //'expert_review',
    //'coupon',
    'bussmember',
    //'viewSystem',
    'film',
    'weshop',
    //'hldWX',
    // 'hotel',
    //酒店房型
    //'hoteltype'
    //'ae-datetimepicker',


    //-------- 群盟 -------------//
    'shop',
    'newproduct',
    'report',
    'qmorder',
    'qmsysproduct',
    'qmmember',
    'appointment',
    //-------- 群盟 -------------//
]);



juyou.config(require('./config'));
juyou.constant('JQ_CONFIG',require('./config.jq'));
juyou.constant('CONST',require('./config.const'));
juyou.config(require('./config.lazyLoad'));
juyou.run(require('./config.run'));
juyou.controller('AppCtrl', require('./main'));

juyou
//html过滤器
//用法<p class="form-control-static" ng-bind-html="viewobj.book_info | trustHtml"/>
.filter('trustHtml', function ($sce) {
    return function (input) {
        return $sce.trustAsHtml(input);
    }
})


juyou.factory('selectMenu', ['$rootScope', function ($rootScope) {
    // 定义所要返回的地址对象   
    var menu = {};
    menu.changeMenu = function (value) {
        menu.value = value;
        $rootScope.$broadcast('changeMenu');
    };
    return menu;
}]);



juyou.directive('imageonload', function () {
  return {
    restrict: 'A', link: function (scope, element, attrs) {
      element.bind('load', function () { 
        //call the function that was passed 
        scope.$apply(attrs.imageonload);
      });
    }
  };
})

