module.exports = function ($scope, $state, $resource) {


	$scope.name = '添加系统应用';
	$scope.obj = {};
	$scope.office = [];
	init();

	function init() {
		$scope.obj.state = '0';
		$scope.obj.proxy_type = '0';
		$scope.obj.notice_state = '0';
		$scope.obj.check_type = '0';
		$scope.obj.sort = 1;
	}

	//添加
	$scope.save = function () {
		$scope.obj.bind_company_code = $scope.office.toString();
		if($scope.obj.notice_state == '0'){
			$scope.obj.notice_url = '';
		}
		if($scope.obj.proxy_type == '1'){
			$scope.obj.bind_login_name = '';
		} else if($scope.obj.proxy_type == '9'){
			$scope.obj.bind_login_name = '';
			$scope.obj.bind_company_code = '';
		}
		$resource('/api/as/sc/syssigndao/save', {}, {}).save($scope.obj, function (res) {
			if (res.errcode === 0) {
				$scope.cancel();
			} else {
				alert(res.errmsg);
			}
		});
	};

	//后退
	$scope.cancel = function () {
		$scope.obj = {};
		$scope.office = [];
		$state.go('app.sys_sign', {})
	};

	//添加机构代理
	$scope.gogo = function () {
		if($scope.obj.company_code == '' || $scope.obj.company_code == undefined){
			console.log('空的! 加不上去');
			return;
		}
		for(var i = 0; i < $scope.office.length; i++){
			if($scope.obj.company_code == $scope.office[i]){
				alert('不能添加重复机构!');
				return;
			}
		}
		$scope.office.push($scope.obj.company_code);
		$scope.obj.company_code = '';
	};

	//移除机构代理
	$scope.remove = function (index) {
		$scope.office.splice(index,1);
	};

};