module.exports = function ($scope, $stateParams, $state, $resource) {

	// $scope.maxSize = '5';			//最多显示多少个按钮
    // $scope.currentPage = '1';		//当前页码
    // $scope.itemsPerPage = '20'		//每页显示几条
	$scope.appid = $stateParams.appid;

	init();
	
	//初始化页面
	function init(appid){
		var para = {
			'appid': $scope.appid,
			// 'pageNo':$scope.currentPage,
            // 'pageSize':$scope.itemsPerPage
		};
		$resource('/api/as/sc/syssignotauser/getOtaUserList', {}, {}).save(para, function (res) {
			if (res.errcode === 0) {
				$scope.ota_list = res.data;
				// $scope.totalItems = res.data.totalRecord;
			} else {
				alert(res.errmsg);
			}
		});
	}

	//添加
	$scope.add = function () {
		$state.go('app.addotaUser', {});
	}

	//搜索
	$scope.search = function () {
		init();
	}

	//修改
	$scope.update = function (item) {
		$state.go('app.otauserEdit', {'id': item.id});
	}

	//删除应用
	$scope.delete = function (item) {
		if (confirm('确认要删除吗？')==true){
			$resource('/api/as/sc/syssignotauser/delete', {}, {}).save({'id': item.id}, function (res) {
				if (res.errcode === 0) {
					init();
				} else {
					alert(res.errmsg);
				}
			});
		}
	};

	//回退
	$scope.back = function () {
		$state.go('app.sys_sign', {});
	}


};