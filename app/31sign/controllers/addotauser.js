module.exports = function($scope, $stateParams, $resource, $state){	

	$scope.name = '添加签名';
	$scope.obj = {};
	init();

	function init() {
		$scope.obj.ota_state = '1';
	}
	
	//添加
	$scope.save = function () {
		$resource('/api/as/sc/syssignotauser/save', {}, {}).save($scope.obj, function (res) {
			if (res.errcode === 0) {
				$scope.cancel();
			} else {
				alert(res.errmsg);
			}
		});
	};

	//后退
	$scope.cancel = function () {
		$state.go('app.sign_ota', {})
	};

};