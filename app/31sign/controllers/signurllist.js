module.exports = function ($scope, $state, $resource, $stateParams) {

	$scope.maxSize = '5';			//最多显示多少个按钮
    $scope.currentPage = '1';		//当前页码
    $scope.itemsPerPage = '20'		//每页显示几条
	$scope.appid = $stateParams.appid;

	init();
	
	//初始化页面
	function init(appid){
		var para = {
			'appid': $scope.appid,
			'url': $scope.url,
			'pageNo':$scope.currentPage,
            'pageSize':$scope.itemsPerPage
		};
		$resource('/api/as/sc/syssignurl/getUrlList', {}, {}).save(para, function (res) {
			if (res.errcode === 0) {
				$scope.signurl_list = res.data.results;
				$scope.totalItems = res.data.totalRecord;
			} else {
				alert(res.errmsg);
			}
		});
	}

	//添加
	$scope.add = function () {
		$state.go('app.addsignurl', {});
	}

	//搜索
	$scope.search = function () {
		init();
	}

	//修改
	$scope.update = function (item) {
		$state.go('app.signurlEdit', {'id': item.id});
	}

	//删除应用
	$scope.delete = function (item) {
		if (confirm('确认要删除吗？')==true){
			$resource('/api/as/sc/syssignurl/delete', {}, {}).save({'id': item.id}, function (res) {
				if (res.errcode === 0) {
					init();
				} else {
					alert(res.errmsg);
				}
			});
		}
	};

	//修改
	$scope.back = function () {
		$state.go('app.sys_sign', {});
	}


};