module.exports = function($scope, $resource, $stateParams, $state){

	$scope.name = '修改应用签名';

	$resource('/api/as/sc/syssignurl/getById', {}, {}).save({'id': $stateParams.id}, function (res) {
		if (res.errcode === 0) {
			$scope.obj = res.data;
			$scope.obj.sort = parseInt($scope.obj.sort);
		} else {
			alert(res.errmsg);
		}
	});

	//保存
	$scope.save = function () {
		$resource('/api/as/sc/syssignurl/save', {}, {}).save($scope.obj, function (res) {
			if (res.errcode === 0) {
				$scope.cancel();
			} else {
				alert(res.errmsg);
			}
		});
	};

	//取消
	$scope.cancel = function () {
		$state.go('app.sign_url', {})
	};

};