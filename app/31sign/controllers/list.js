module.exports = function ($scope, $state, $resource) {

	$scope.maxSize = '5';			//最多显示多少个按钮
    $scope.currentPage = '1';		//当前页码
    $scope.itemsPerPage = '20'		//每页显示几条

	init();
	//初始化页面
	function init(appid){
		var para = {
			'appid': appid,
			'pageNo':$scope.currentPage,
            'pageSize':$scope.itemsPerPage
		};
		$resource('/api/as/sc/syssigndao/getSysSignList', {}, {}).save(para, function (res) {
			if (res.errcode === 0) {
				$scope.sign_list = res.data.results;
				$scope.totalItems = res.data.totalRecord;
			} else {
				alert(res.errmsg);
			}
		});
	}

	//搜索
	$scope.search = function () {
		init($scope.search.appid);
	}

	//添加
	$scope.add = function () {
		$state.go('app.addsign', {});
	}

	//修改
	$scope.update = function (item) {
		$state.go('app.signEdit', {'id': item.id});
	}

	//删除应用
	$scope.delete = function (item) {
		if (confirm('确认要删除吗？\r\n此操作会删除对应的应用接口授权和绑定的OTA商户')==true){
			$resource('/api/as/sc/syssigndao/delete', {}, {}).save({'id': item.id}, function (res) {
				if (res.errcode === 0) {
					init();
				} else {
					alert(res.errmsg);
				}
			});
			$resource('/api/as/sc/syssignurl/delete', {}, {}).save({'appid': item.appid}, function (res) {
				console.log('delete signurl success');
			});
			$resource('/api/as/sc/syssignotauser/delete', {}, {}).save({'appid': item.appid}, function (res) {
				console.log('delete otauser success');
			});
		}
	};

	//设置应用URL
	$scope.setURL = function (item) {
		$state.go('app.sign_url', {'appid': item.appid});
	};

	//设置OTA商户
	$scope.setOTA = function (item) {
		$state.go('app.sign_ota', {'appid': item.appid});
	};



};