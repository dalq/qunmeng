var App = angular.module('sign', []);

App.config(require('./router'));

//service
App.factory('signservice', require('./service'));

//Controllers
App.controller('signList', require('./controllers/list'));
App.controller('signEdit', require('./controllers/edit'));
App.controller('addsign', require('./controllers/addsign'));

App.controller('signurlList', require('./controllers/signurllist'));
App.controller('signurlEdit', require('./controllers/signurledit'));
App.controller('addsignurl', require('./controllers/addsignurl'));

App.controller('otauserList', require('./controllers/otauserlist'));
App.controller('otauserEdit', require('./controllers/otauseredit'));
App.controller('addotaUser', require('./controllers/addotauser'));


module.exports = App;