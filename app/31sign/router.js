 /**
 * 子模块路由
 * DHpai
 */

var router = function($urlRouterProvider, $stateProvider){

 	$stateProvider

    //系统签名列表
    .state('app.sys_sign', {
        url: "/sign/list",
        views: {
            'main@' : {
                template : require('./views/signlist.html'),
                controller : 'signList',
            }
        }
    })

    //签名编辑
    .state('app.signEdit', {
        url: "/sign/signedit/:id",
        views: {
            'main@' : {
                template : require('./views/sign.html'),
                controller : 'signEdit',
            }
        }
    })

    //添加签名
    .state('app.addsign', {
        url: "/sign/add",
        views: {
            'main@' : {
                template : require('./views/sign.html'),
                controller : 'addsign',
            }
        }
    })

    //系统签名列表
    .state('app.sign_url', {
        url: "/signurl/list/:appid",
        views: {
            'main@' : {
                template : require('./views/signurllist.html'),
                controller : 'signurlList',
            }
        }
    })

    //签名地址编辑
    .state('app.signurlEdit', {
        url: "/signurl/edit/:id",
        views: {
            'main@' : {
                template : require('./views/signurl.html'),
                controller : 'signurlEdit',
            }
        }
    })

    //添加签名地址
    .state('app.addsignurl', {
        url: "/signurl/add",
        views: {
            'main@' : {
                template : require('./views/signurl.html'),
                controller : 'addsignurl',
            }
        }
    })

    //OTA对接商户列表
    .state('app.sign_ota', {
        url: "/otauser/list/:appid",
        views: {
            'main@' : {
                template : require('./views/otauserlist.html'),
                controller : 'otauserList',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            }
        }
    })

    //编辑OTA对接商户
    .state('app.otauserEdit', {
        url: "/otauser/edit/:id",
        views: {
            'main@' : {
                template : require('./views/otauser.html'),
                controller : 'otauserEdit',
            }
        }
    })

    //编辑OTA对接商户
    .state('app.addotaUser', {
        url: "/otauser/add/:id",
        views: {
            'main@' : {
                template : require('./views/otauser.html'),
                controller : 'addotaUser',
            }
        }
    })

   



	;

};

module.exports = router;