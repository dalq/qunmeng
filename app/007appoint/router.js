

var router = function ($urlRouterProvider, $stateProvider) {


    $stateProvider


        //商家发布的可预约产品列表
        .state('app.qmappoint',{
            url: '/qmappoint/:id',
              views: {
                'main@': {
                        controller : 'qmappoint',        
                        template: require('./views/appointmentlist.html'),
               }
            },
            resolve:{
               
                getDate : function(utilservice){
                    return utilservice.getDate;
                },
                str2date : function(utilservice){
                    return utilservice.str2date;
                },
                date2str : function(utilservice) {
                    return utilservice.date2str;
                },
               
                
            }
        })


        //设置预约
        .state('app.qmsetAppoint',{
            url: '/qmsetAppoint/:id',
             views: {
                'main@': {
                        controller : 'qmsetAppoint',
                        template: require('./views/setappointment.html'),
                }
             },
            resolve:{
                productid: function () {
                    return '';
                },
                what: function () {
                    return 'edit';
                },
                date2str: function (utilservice) {
                    return utilservice.getDate;
                },
                str2date: function (utilservice) {
                    return utilservice.str2date;
                },
                $modalInstance: function () {
                    return undefined;
                },
                auditing: function () {
                    return false;
                }
                
            }
        })

         //预约人列表
        .state('app.qmcustomerList',{
            url: '/qmcustomerList/:id',
             views: {
                'main@': {
                        controller : 'qmcustomerlist11',
                        template: require('./views/customerlist.html'),
                }
             },
            resolve:{
                findUserInfoList : function(productservice){
                    return productservice.findUserInfoList();
                },
                getDate : function(utilservice){
                     return utilservice.getDate;
                }
                
            }
        })


        ;

};

module.exports = router;