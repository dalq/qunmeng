module.exports = function ($resource, $state, $http, $q, FileUploader, toaster) {

	return {

		restrict: 'AE',
		template: require('../views/appointmentscreening.html'),
		replace: true,
		scope: {
			'saleobj': '=',
			'funobj': '=',
			'baseinfo': '=',
			'util': '=',
		},
		link: function (scope, elements, attrs) {


             scope.date = {
                'lable': date2str2(new Date()),
                'opened': false
            }
             scope.dateOpen = function ($event, item) {
                $event.preventDefault();
                $event.stopPropagation();
                item.opened = true;
            };

             function date2str2(d) {
                if (d === undefined) {
                    return "";
                }
                var month = (d.getMonth() + 1).toString();
                var day = d.getDate().toString();
                if (month.length == 1) month = '0' + month;
                if (day.length == 1) day = '0' + day;
                return d.getFullYear() + "-" + month + "-" + day;
            }

			scope.info = {
				'appoint_id': scope.saleobj.id
			}
			scope.auditing = scope.util.auditing;

			//页面需要的信息。
			scope.page = {};

			// 场次列表
			scope.findTimeInfoList = function () {
				var url = '';
				url = '/api/as/wc/appoint/findAppointTimeList';
				$resource(url, {}, {}).save({ 'appoint_id': scope.saleobj.id }, function (res) {
					if (res.errcode != 0) {
						toaster.error({ title: "提示", body: res.errmsg });
						return;
					}
					scope.time_list = res.data;
					// scope.obj.section.periodstart.date = scope.util.str2date(scope.res.date.startTime);
					// scope.obj.section.periodend.date = scope.util.str2date(scope.res.date.endTime);                    
					// toaster.success({ title: "提示", body: '操作成功' });

				});
			}
			scope.findTimeInfoList();
			// 添加
			scope.add = function () {

				if(!scope.info.stock_num || !parseInt(scope.info.stock_num)){
					toaster.warning({title:'',body:'请输入正确的库存数量'});
					return false;
				}
				
                if(scope.date.lable){
					if(typeof scope.date.lable === 'string'){
  						scope.tour_date=scope.date.lable;
					}else{
  						scope.tour_date=date2str2(scope.date.lable);;
					}
                  
                }else{
                    scope.tour_date=''
                }
				scope.info.start_time = scope.tour_date;
				scope.info.end_time = '';
				var url = '';
				var para = {};
				url = '/api/as/wc/appoint/saveAppointTime';
				para = scope.info;

				$resource(url, {}, {}).save(para, function (res) {
					if (res.errcode != 0) {
						toaster.error({ title: "提示", body: res.errmsg });
						return;
					}
					toaster.success({ title: "", body: "操作成功!" });
					scope.findTimeInfoList();
				});

			};
			scope.delete = function (id) {
				var url = '/api/as/wc/appoint/setAppointTimeDel';
				if (confirm('确定要删除吗~~~~亲~')) {
					$resource(url, {}, {}).save({ 'id': id }, function (res) {
						if (res.errcode != 0) {
							toaster.error({ title: "提示", body: res.errmsg });
							return;
						}
						toaster.success({ title: "", body: "删除成功!" });
						scope.findTimeInfoList();
					});
				}

			}

			scope.save = function (obj) {
				var url = '/api/as/wc/appoint/setAppointTime';
				$resource(url, {}, {}).save(obj, function (res) {
					if (res.errcode != 0) {
						toaster.error({ title: "提示", body: res.errmsg });
						return;
					}
					toaster.success({ title: "", body: "修改成功!" });
					scope.findTimeInfoList();
				});
			}
		}
	};
};

