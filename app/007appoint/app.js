var App = angular.module('appointment', []);

App.config(require('./router'));

App.controller('qmappoint',require('./controllers/appointmentlist'));
App.controller('qmsetAppoint',require('./controllers/setappointment'));
App.directive('qmappointbaseinfo',require('./directives/appointbaseinfo'));
App.directive('qmappointscreening',require('./directives/appointscreening'));
App.directive('qmappointticket',require('./directives/appointticket'));



module.exports = App;