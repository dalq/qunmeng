module.exports = function ($timeout) {
    return {
        restrict: 'E',
        // require: 'ngClick',
		template : require('./viewCell.html'),
        scope: {
            next:'&',
            url:'@',
            params:'@',
            direction: '@',             //方向
            img: '@',                   //日历数据
            titleCn: '@',               //数据显示规则
			titleEn: '@',               //日历样式丶函数等配置
            description: '@'            //当前显示月份所包含的任意一天
        },
        link: function ($scope, $element, $attrs, $ctrls) {
			$scope.$watch( '$scope.titleCn', function (newValue) {
				if(!$scope.titleCn){
                    $scope.titleCn = '中文标题';
                }
			}, true);
			$scope.$watch( '$scope.titleEn', function (newValue) {
				if(!$scope.titleEn){
                    $scope.titleEn = '英文标题';
                }
			}, true);
			$scope.$watch( '$scope.description', function (newValue) {
				if(!$scope.description){
                    $scope.description = '图片';
                }
			}, true);
		}
    };
}