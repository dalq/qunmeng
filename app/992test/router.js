/**
* 子模块路由
* ml
*/

var router = function ($urlRouterProvider, $stateProvider) {

	$stateProvider

		//测试选座系统
		.state('app.select', {
			url: "/viewSystem/select",
			views: {
				'main@': {
					template: require('./views/select.html'),
					controller: 'select'
				}
			},
			resolve: {

			}
		})

};

module.exports = router;