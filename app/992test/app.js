/**
 * 景区系统入口
 * ml
 */
var App = angular.module('testSystem', []);

App.config(require('./router'));

//service
App.factory('testSystemService', require('./service'));

//Controllers
App.controller('select', require('./controllers/select'));

//directive
// App.directive('viewCell', require('./directives/viewCell'));

module.exports = App;