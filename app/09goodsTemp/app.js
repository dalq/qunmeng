var App = angular.module('goodsTemp', []);

App.config(require('./router'));
App.factory('goodsTempservice', require('./service'));

App.controller('goodsTemplist',require('./controllers/list'));
App.controller('goodsTempcreate',require('./controllers/create'));
App.controller('goodsTempinfo',require('./controllers/info'));
App.controller('goodsTempedit',require('./controllers/edit'));


module.exports = App;