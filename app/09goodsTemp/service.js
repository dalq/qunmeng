/**
 * 子模块service
 * dlq
 */
var service = function($resource, $q, $modal, $state){

    //模型
    var model = [
        {
            'title' : '商品模板名称',
            'id' : 'templete_name',
            'type' : 'text',
            'required' : true,
            'placeholder' : '必填',
        },
        {
            'title' : '商品模板图片',
            'id' : 'templete_img',
            'type' : 'image',
        },
        {
            'title' : '操作',
            'type' : 'button',
            'info' : [
                {
                    'label' : '模板项',
                    'btnclick' : function(form){

                        var para = $state.get('app.product_temp');

                        var resolve = {
                            'resolve' : {
                                'data' : function(){
                                    return {
                                        'lock' : form.result['templete_lock_data_json'],
                                        'unlock' : form.result['templete_check_data_json']
                                    }
                                }
                            }
                        };

                        para = angular.extend(para, resolve);
                        


                        var modalInstance = $modal.open(para);
                        modalInstance.opened.then(function() {// 模态窗口打开之后执行的函数  
                            console.log('modal is opened');  
                        });  
                        modalInstance.result.then(function(result) {  
                            console.log(result);  

                            form.result['templete_lock_data_json'] = JSON.stringify(result.lock);
                            form.result['templete_check_data_json'] = result.unlock;

                        }, function(reason) {  
                            console.log(reason);// 点击空白区域，总会输出backdrop  
                            // click，点击取消，则会暑促cancel  
                            $log.info('Modal dismissed at: ' + new Date());  
                        }); 
                    }
                }
            ],
        },

        {
            'title' : '商品模板锁定模型',
            'id' : 'templete_lock_data_json',
            'type' : 'static',
            'f' : 'json',
        },
        {
            'title' : '校验模型',
            'id' : 'templete_check_data_json',
            'type' : 'static',
        },

    

    ];

   
    return {

        model : function(){
            return model;
        },
       
    };

};

module.exports = service;