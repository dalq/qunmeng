module.exports = function($scope, tableconfig, $state){

	tableconfig.start($scope, {
		'url' : '/api/ac/tc/goodsTemplete/list',
		'col' : [
			{'title' : '商品模版编号', 'col' : 'templete_code'},
			{'title' : '商品模板名称', 'col' : 'templete_name'},
			{'title' : '操作', 'col' : 'btn'},
		],
		'btn' : [
			{'title' : '查看详情', 'onclick' : 'info'},
			{'title' : '编辑', 'onclick' : 'edit'},
			{'title' : '删除', 'onclick' : 'delete'},
		],
		'search' : [
			{'title' : '名称', 'type' : 'txt', 'name' : 'templete_name', 'show' : true},
			{'title' : '编号', 'type' : 'txt', 'name' : 'templete_code', 'show' : true},
		],
		'title' : '商品模板列表',
		'info' : {
			'to' : 'app.goodsTemp_info',
		},
		'delete' : {
			'url' : '/api/ac/tc/ticketGoods/updateTicketGoodsDelete',
		},
		'edit' : {
			'to' : 'app.goodsTemp_edit'
		}
	});
	$scope.table = tableconfig;

};