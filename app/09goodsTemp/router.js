 /**
 * 子模块路由
 * dlq
 */

var router = function($urlRouterProvider, $stateProvider){

 	$stateProvider
    //商品模板列表
    .state('app.goodsTemp_list', {
        url: "/goodsTemp/list.html",
        views: {
            'main@' : {
                template : require('../996tpl/views/table.html'),
                controller : 'goodsTemplist',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
        }
    })
    
    //创建商品模板
    .state('app.goodsTemp_create', {
        url: "/goodsTemp/create.html",
        views: {
            'main@' : {
                template : require('../996tpl/views/form.html'),
                controller : 'goodsTempcreate',
            }
        },
        resolve:{
            formconfig : function(formservice){
                return formservice.formconfig();
            },
            model : function(goodsTempservice){
                return goodsTempservice.model;
            }
        }
    })
    //商品模板详情
    .state('app.goodsTemp_info', {
        url: "/goodsTemp/info/:id",
        views: {
            'main@' : {
                template : require('../996tpl/views/form.html'),
                controller : 'goodsTempinfo',
            }
        },
        resolve:{
            formconfig : function(formservice){
                return formservice.formconfig();
            },
            model : function(goodsTempservice){
                return goodsTempservice.model;
            }
        }
    })

    //商品模板编辑
    .state('app.goodsTemp_edit', {
        url: "/goodsTemp/edit/:id",
        views: {
            'main@' : {
                template : require('../996tpl/views/form.html'),
                controller : 'goodsTempedit',
            }
        },
        resolve:{
            formconfig : function(formservice){
                return formservice.formconfig();
            },
            model : function(goodsTempservice){
                return goodsTempservice.model;
            }
        }
    })
	;

};

module.exports = router;