var App = angular.module('qmmember', []);

App.config(require('./router'));
//App.factory('shopservice', require('./service'));

App.controller('qmmemberlist',require('./controllers/memberlist'));
App.controller('qmsetlevel',require('./controllers/setlevel'));
App.controller('qmstatistics',require('./controllers/statistics'));
App.controller('qmorderlistpop',require('./controllers/pop_orderlist'));
App.controller('qmrelation',require('./controllers/relation'));
// App.controller('viewinfo',require('./controllers/info'));
// App.controller('viewedit',require('./controllers/edit'));

//App.directive('qmorderlist',require('./directives/orderlist'));
// App.directive('book',require('./directives/book'));
// App.directive('cancellation',require('./directives/cancellation'));





// App.controller('list',require('./controllers/list'));

module.exports = App;