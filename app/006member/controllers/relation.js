module.exports = function($scope, $resource, $modal, $modalInstance, user_base){

	console.log(user_base);

	$resource('/api/as/uc/userwxsk/getuplevel', {}, {}).save({'user_base' : user_base}, function (res) {
		console.log(res);
		if(res.errcode != 0){
			return;
		}
		
		$scope.up = res.data;

	});


	/* 分页
     * ========================================= */
	$scope.maxSize = 5;            //最多显示多少个按钮
	$scope.bigCurrentPage = 1;      //当前页码
	$scope.itemsPerPage = 10;         //每页显示几条

	$scope.load = function () {

		var para = {
			'pageNo': $scope.bigCurrentPage,
			'pageSize': $scope.itemsPerPage,
			'user_base' : user_base
		};

		$resource('/api/as/uc/userwxsk/downlevellist', {}, {}).save(para, function (res) {
			console.log(res);
			if(res.errcode != 0){
				//alert(res.errmsg);
				return;
			}

			$scope.downs = res.data.results;
			$scope.bigTotalItems = res.data.totalRecord;
		});
	};
	$scope.load();



	$scope.orderlist = function(obj){

		console.log(obj);

		var modalInstance = $modal.open({
			template: require('../views/pop_orderlist.html'),
			controller: 'qmorderlistpop',
			url: '/qmorderlistpop',
			size : 'lg',
			resolve: {
				obj: function () {
					return obj;
				},
				// str2date: function () {
				// 	return str2date;
				// },
				// date2str: function () {
				// 	return date2str;
				// },
				
			}
		});

		modalInstance.opened.then(function () {// 模态窗口打开之后执行的函数  
			// $scope.load();
		});
		modalInstance.result.then(function (showResult) {
			//$scope.load();
		}, function (reason) {
			//$scope.load();
			// // click，点击取消，则会暑促cancel  
			// $log.info('Modal dismissed at: ' + new Date());
		});
		

	};


	//统计
	$scope.statistics = function(obj){

		///api/as/wc/usedrecord/sysgoodsstatisticslist
		var modalInstance = $modal.open({
			template: require('../views/pop_statistics.html'),
			controller: 'qmstatistics',
			url: '/qmstatistics',
			//size : 'lg',
			resolve: {
				obj: function () {
					return obj;
				},
				// str2date: function () {
				// 	return str2date;
				// },
				// date2str: function () {
				// 	return date2str;
				// },
				
			}
		});

		modalInstance.opened.then(function () {// 模态窗口打开之后执行的函数  
			// $scope.load();
		});
		modalInstance.result.then(function (showResult) {
			//$scope.load();
		}, function (reason) {
			//$scope.load();
			// // click，点击取消，则会暑促cancel  
			// $log.info('Modal dismissed at: ' + new Date());
		});

		
	};


	$scope.cancel = function () {
        $modalInstance.close();
    }

};
