module.exports = function ($scope, $resource, $modal, levelcode, user_base, levelarr, $modalInstance, remark) {

	console.log('设置级别');

    console.log(user_base);
    console.log(levelarr);
    console.log(remark);

    $scope.level = {
        'code' : levelcode,
    };

    $scope.levelarr = levelarr;

    $scope.remark = remark;

    $scope.save = function(){

        var para = {
            'user_base' : user_base,
            'level' : $scope.level.code,
            'remark' : $scope.remark,
        };

        $resource('/api/ac/uc/userWxSkService/updatelevel', {}, {}).save(para, function (res) {
            console.log(res);

            if (res.errcode !== 0) {
                alert(res.errmsg);
                return;
            }

            alert('设置成功');
            $modalInstance.close();

        });
    }
};