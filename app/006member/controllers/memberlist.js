module.exports = function ($scope, $resource, $modal) {

	
	console.log('asdasdad');

	$scope.fx_levelarr = [
		{title : '全部', code : ''},
		{title : '一级', code : '1'},
		{title : '二级', code : '2'},
	];

	$scope.searchform = {};

	$scope.levelobj = {};

   	$resource('/api/as/uc/userwxsk/memberlevellist', {}, {}).get({}, function(res){ 
    	console.log(res);
        if(res.errcode === 0)
        {
        	$scope.levelarr = res.data;
        	$scope.levelarr.unshift({ title : '最高权限', code: '1' });
        	$scope.levelarr.unshift({ title : '无级别', code: '0' });

        	for(var i = 0; i < $scope.levelarr.length; i++){
        		var tmp = $scope.levelarr[i];
        		$scope.levelobj[tmp.code] = tmp.title;
        	}

        }
        else
        {
            alert(res.errmsg);
        }
    });



	/* 分页
     * ========================================= */
	$scope.maxSize = 5;            //最多显示多少个按钮
	$scope.bigCurrentPage = 1;      //当前页码
	$scope.itemsPerPage = 10;         //每页显示几条

	$scope.conut = 0;
	$scope.load = function () {
		var para = {
			pageNo: $scope.bigCurrentPage,
			pageSize: $scope.itemsPerPage,
		};

		para = angular.extend($scope.searchform, para);

		$resource('/api/ac/uc/userWxSkService/wxuserlist', {}, {}).save(para, function (res) {
            console.log(para);
            console.log(res);

            if (res.errcode !== 0) {
                alert(res.errmsg);
                return;
            }

            $scope.objs = res.data.results;
            $scope.bigTotalItems = res.data.totalRecord;
            $scope.bigCurrentPage = res.data.pageNo;

        });
	};
	$scope.load();



	//设置级别
    $scope.setlevel = function(item){

    	console.log(item);

    	var modalInstance = $modal.open({
          template: require('../views/pop_setlevel.html'),
          controller: 'qmsetlevel',
          //size: 'lg',
          resolve: {
            user_base : function(){
                return item.user_base;
            },
            levelarr : function() {
            	return $scope.levelarr;
            },
            levelcode : function(){
            	return item.level;
            }

          }
        });

        modalInstance.result.then(function () {
        	$scope.load();
        }, function () {
            
        });

    };


    $scope.relation = function(obj){

		var modalInstance = $modal.open({
			template: require('../views/pop_relation.html'),
			controller: 'qmrelation',
			url: '/qmrelation',
			//size : 'lg',
			resolve: {
				'user_base': function () {
					return obj.user_base;
				},
				// str2date: function () {
				// 	return str2date;
				// },
				// date2str: function () {
				// 	return date2str;
				// },
				
			}
		});

		modalInstance.opened.then(function () {// 模态窗口打开之后执行的函数  
			// $scope.load();
		});
		modalInstance.result.then(function (showResult) {
			$scope.load();
		}, function (reason) {
			$scope.load();
			// // click，点击取消，则会暑促cancel  
			// $log.info('Modal dismissed at: ' + new Date());
		});

	};


	//统计
	$scope.statistics = function(obj){

		///api/as/wc/usedrecord/sysgoodsstatisticslist
		var modalInstance = $modal.open({
			template: require('../views/pop_statistics.html'),
			controller: 'qmstatistics',
			url: '/qmstatistics',
			//size : 'lg',
			resolve: {
				obj: function () {
					return obj;
				},
				// str2date: function () {
				// 	return str2date;
				// },
				// date2str: function () {
				// 	return date2str;
				// },
				
			}
		});

		modalInstance.opened.then(function () {// 模态窗口打开之后执行的函数  
			// $scope.load();
		});
		modalInstance.result.then(function (showResult) {
			//$scope.load();
		}, function (reason) {
			//$scope.load();
			// // click，点击取消，则会暑促cancel  
			// $log.info('Modal dismissed at: ' + new Date());
		});

		
	};


	$scope.order = function(obj){
		console.log(obj);

		var modalInstance = $modal.open({
			template: require('../views/pop_orderlist.html'),
			controller: 'qmorderlistpop',
			url: '/qmorderlistpop',
			size : 'lg',
			resolve: {
				obj: function () {
					return obj;
				},
				// str2date: function () {
				// 	return str2date;
				// },
				// date2str: function () {
				// 	return date2str;
				// },
				
			}
		});

		modalInstance.opened.then(function () {// 模态窗口打开之后执行的函数  
			// $scope.load();
		});
		modalInstance.result.then(function (showResult) {
			//$scope.load();
		}, function (reason) {
			//$scope.load();
			// // click，点击取消，则会暑促cancel  
			// $log.info('Modal dismissed at: ' + new Date());
		});

	};



};