/**
 * 模态框
 */
module.exports = function($scope, transData, $state, $modalInstance, $resource,items,selected_list){
    $scope.sel_user_list = angular.copy(selected_list);
    $resource('/api/as/sc/office/getForRoleList', {}, {}).save({}, function (res) {
        if (res.errcode !== 0) {
			alert(res.errmsg);
			return;
		};
        $scope.dataForTheTree = transData(res.data, 'id', 'parent_id', 'children');
        for(var i = 0; i < $scope.sel_user_list.length; i++){
            $scope.sel_user_list[i].red = 1;
        }
    });

    //菜单树选中事件
	$scope.showSelected = function (item) {
        $resource('/api/as/sc/role/getUserList', {}, {}).save({'company_id': item.id, 'parent_ids': item.parent_ids}, function (res) {
            if (res.errcode === 0) {
                $scope.user_list = res.data;
                for(var i = 0; i < selected_list.length; i++){
                    for(var j = 0; j < $scope.user_list.length; j++){
                        if(selected_list[i].id == $scope.user_list[j].id){
                            $scope.user_list.splice(j,1);
                        }
                    }
                }
            } else {
                alert(res.errmsg);
            }
        });
	};

    //选中用户
	$scope.select = function (index) {
        $scope.sel_user_list.push($scope.user_list[index]);
        $scope.user_list.splice(index,1);
	};

    //删除选中用户
	$scope.unselect = function (index) {
        if($scope.user_list != undefined){
            $scope.user_list.push($scope.sel_user_list[index]);
        }
        $scope.sel_user_list.splice(index,1);
	};

    //确定
    $scope.ok = function () {
        var selecteduser = [];
        for(var i = 0; i < $scope.sel_user_list.length; i++){
            selecteduser.push($scope.sel_user_list[i].id);
        }
        var para = {
            'user_id': selecteduser,
            'role_id': items.id
        };
        $resource('/api/ac/sc/roleService/setAllotUser', {}, {}).save(para, function (res) {
            if (res.errcode !== 0) {
                alert(res.errmsg);
            }
            //关闭模态框
            $modalInstance.close();
		});
    };

    //取消
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };


};