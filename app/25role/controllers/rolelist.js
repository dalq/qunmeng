module.exports = function ($scope, $modal, officeList, roleList, roleInfo, delRole, saveRole, userList,relieveUser) {

	$scope.what = 'list';			//查看状态 modify:修改用户,list:用户列表
	$scope.obj = {};				//用户对象
	$scope.company_name = '';
	$scope.status = {				//控制标签页的活跃状态
		'rolelist' : true,
		'allot': false,
	};
	// $scope.role_type_list = [		//角色类型列表
	// 	{'id': 'assignment','name': '任务分配'},
	// 	{'id': 'security-role','name': '管理角色'},
	// 	{'id': 'user','name': '普通角色'}
	// ];
	$scope.data_scope_list = [		//数据范围列表
		{'id': '1','name': '所有数据'},
		{'id': '2','name': '所在公司及以下数据'},
		{'id': '3','name': '所在公司数据'},
		{'id': '4','name': '所在部门及以下数据'},
		{'id': '5','name': '所在部门数据'},
		{'id': '8','name': '仅本人数据'},
		{'id': '9','name': '按明细设置'}
	];
	$scope.predicate = '';
	$scope.comparator = false;

	//取消==重置
	$scope.cancel = function () {
		$scope.what = 'list';
		$scope.status.rolelist = true;
		$scope.tabname = {'name': '添加角色', 'active': false};
	};

	init();
	//初始化
	function init(){
		$scope.cancel();
		// officeList.save({}, function (res) {
		// 	if (res.errcode === 0) {
		// 		$scope.offList = res.data;
		// 	} else {
		// 		alert(res.errmsg);
		// 	}
		// });
		roleList.save({}, function (res) {
			if (res.errcode === 0) {
				$scope.role_list = res.data;
			} else {
				alert(res.errmsg);
			}
		});
	}

	//搜索
	$scope.search = function () {
		$scope.cancel();
		var para = {
			'name': $scope.search.aname,
			'enname': $scope.search.enname,
		};
		roleList.save(para, function (res) {
			if (res.errcode === 0) {
				$scope.role_list = res.data;
			} else {
				alert(res.errmsg);
			}
		});
	};

	//添加界面
	$scope.add = function () {
		if($scope.tabname.name == '修改角色'){
			return;
		}
		$scope.tabname = {'name': '添加角色', 'active': true};
		$scope.obj = {};
		$scope.obj.is_sys = '1';
		$scope.obj.useable = '1';
		$scope.obj.role_type = 'assignment';
		$scope.obj.data_scope = '8';
		$scope.obj.auth_start = '1';
		$scope.obj.auth_end = '99';
	};

	//修改界面
	$scope.update = function (item) {
		$scope.tabname = {'name': '修改角色', 'active': true};
		roleInfo.save(item, function (res) {
			if (res.errcode === 0) {
				$scope.obj = res.data;
				if($scope.obj.auth_start == undefined || $scope.obj.auth_start == ''){
					$scope.obj.auth_start = '1';
				}
				if($scope.obj.auth_end == undefined || $scope.obj.auth_end == ''){
					$scope.obj.auth_end = '99';
				}
			} else {
				alert(res.errmsg);
			}
		});
	};

	//删除角色
	$scope.delete = function (item) {
		if (confirm('确认要删除该用户吗？')==true){
			delRole.save(item, function (res) {
				if (res.errcode === 0) {
					init();
				} else {
					alert(res.errmsg);
				}
			});
		}
	};

	//保存用户
	$scope.save = function () {
		saveRole.save($scope.obj, function (res) {
			if (res.errcode === 0) {
				init();
			} else {
				alert(res.errmsg);
			}
		});
	};

	//分配按钮
	$scope.allot = function (item) {
		$scope.what = 'allot';
		$scope.status.allot = true;
		$scope.obj = item;
		userList.save({'role_id': item.id}, function (res) {
			if (res.errcode === 0) {
				$scope.user_list = res.data;
			} else {
				alert(res.errmsg);
			}
		});
	};

	//角色分配
	$scope.allotuser = function () {
		var modalInstance = $modal.open({
			template: require('../views/allotuser.html'),
			controller: 'allotuser',
			size: 'lg',
			resolve: {
				items: function () {
					return $scope.obj;
				},
				selected_list: function () {
					return $scope.user_list;
				},
				transData : function(utilservice){
               		return utilservice.transData;
            	}
			}
		});
		//关闭模态框刷新页面
		modalInstance.result.then(function() {
  			$scope.allot($scope.obj);
		});
	}

	//移除角色
	$scope.remove = function (item) {
		if(confirm('确认要删除该用户吗？')==true){
			var para = {
				'user_id': item.id,
				'role_id': $scope.obj.id
			};
			relieveUser.save(para, function (res) {
				if (res.errcode === 0) {
					$scope.allot($scope.obj);
				} else {
					alert(res.errmsg);
				}
			});
		}
	};

	//角色授权
	$scope.setAuth = function (item) {
		var modalInstance = $modal.open({
			template: require('../views/setAuth.html'),
			controller: 'setAuth',
			size: 'lg',
			resolve: {
				items: function () {
					return $scope.obj;
				},
				transData : function(utilservice){
					return utilservice.transData;
				},
				auth : function(){
					return item;
				}
			}
		});
		//关闭模态框刷新页面
		modalInstance.result.then(function() {
  			$scope.allot($scope.obj);
		});
	};

	//角色绑定模板
	$scope.setTemplete = function (item) {
		var modalInstance = $modal.open({
			template: require('../views/setTemplete.html'),
			controller: 'setTemplete',
			size: 'lg',
			resolve: {
				roleInfo : function(){
					return item;
				}
			}
		});
	};

	//角色设置字典
	$scope.setDict = function (item) {
		var modalInstance = $modal.open({
			template: require('../views/setDict.html'),
			controller: 'setDict',
			size: 'lg',
			resolve: {
				roleInfo : function(){
					return item;
				}
			}
		});
	};

	//绑定渠道
	$scope.setChannel = function(item){
		var modalInstance = $modal.open({
			template: require('../views/setChannel.html'),
			controller: 'setRoleBindInfo',
			size: 'lg',
			resolve: {
				roleInfo: function(){
					return item;
				}
			}
		});
		//关闭模态框刷新页面
		modalInstance.result.then(function() {

		});
	};
	

};