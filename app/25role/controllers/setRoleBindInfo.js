/**
 * 模态框
 */
module.exports = function($scope, $modalInstance, $resource, roleInfo){

    init();
    //初始化查询全部可绑定渠道
    function init(){
        $resource('/api/ac/tc/officeBindService/getAllList', {}, {}).save({'tag': '2'}, function(res){
            if (res.errcode === 0) {
                $scope.channel_list = res.data.channelList;
                $scope.roleChannel();
            } else {
                alert(res.errmsg);
            }
        });
    };

    //加载角色已经绑定的渠道编号
    $scope.roleChannel = function(){
        $resource('/api/as/sc/role/getRoleChannel', {}, {}).save({'role_id': roleInfo.id}, function(res){
            if (res.errcode === 0) {
                var channels = ',' + res.data.bind_channel + ',';
                angular.forEach($scope.channel_list, function(item){
                    if(channels.indexOf(','+item.code+',') != -1){
                        item.check = true;
                    }
                });
            }
        });
    };

    //全选or全不选
    $scope.allSelect = function (){
        angular.forEach($scope.channel_list, function(item){
            item.check = $scope.all;
        });
    };
    
    //确定
    $scope.ok = function () {
        var selected = [];
        angular.forEach($scope.channel_list, function(item){
            if(item.check){
                selected.push(item.code);
            }
        });
        var para = {
            'role_id' : roleInfo.id,
            'bind_channel' : selected.toString()
        }
        $resource('/api/as/sc/role/addChannelForRole', {}, {}).save(para, function (res) {
            if (res.errcode !== 0) {
                alert(res.errmsg);
            }
            $modalInstance.close();
		});
    };

    //取消
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

};