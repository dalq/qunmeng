/**
 * 模态框
 */
module.exports = function($scope, $state, $resource, $modal){

    init();
	//初始化模板列表
	function init(){
        $resource('/api/ac/tc/ticketSaleTempleteService/getTempleteListForSys', {}, {}).save({}, function (res) {
            if (res.errcode === 0) {
                $scope.templete_list = res.data;
            } else {
                alert(res.errmsg);
            }
        });
    }

    //菜单树选中事件
	$scope.setRole = function (item) {
		var modalInstance = $modal.open({
			template: require('../views/setrolefortemp.html'),
			controller: 'setRoleForTemp',
			size: 'lg',
			resolve: {
				item: function () {
					return item;
				},
                roleList : function(roleservice){
                    return roleservice.roleList();
                }
			}
		});
		//关闭模态框刷新页面
		modalInstance.result.then(function() {
  			
		});
	};




};