module.exports = function ($scope, $resource, $modal, transData, toaster) {

	//加载公司内角色包含的全部菜单
	$scope.loadMenuList = function() {
		$resource('/api/ac/sc/menuService/getCompanyMenuList', {}, {}).save({}, function(res){
			if (res.errcode === 0){
				var data = transData(res.data, 'id', 'parent_id', 'children');
				$scope.menuList = data[0].children;
				$scope.formData.loadingMenu = false;
				$scope.formData.id = 0;
				$scope.getRoleMenuList(0);
			} else {
				toaster.error({title: '', body: res.errmsg});
			}
		});
	};

	//加载角色包含的菜单
	$scope.getRoleMenuList = function(index){
		if(index == $scope.formData.lastIndex){
			return;
		}
		if($scope.formData.addRole && index == $scope.roleList.length-1){
			return;
		}
		$scope.formData.loadindRole = true;
		$resource('/api/as/sc/menu/getMenuForRoleList', {}, {}).save({'role_id': $scope.roleList[index].id}, function(res){
			if (res.errcode === 0){
				$scope.formData.lastIndex = index;
				var menuSet = new Set();
				//第一次循环将角色包含的菜单存放Set中
				angular.forEach(res.data, function(item){
					menuSet.add(item.id);
				});
				//第二次循环判断选中的菜单
				angular.forEach($scope.menuList, function(menuOne){
					menuOne.open = true;
					angular.forEach(menuOne.children, function(menuTwo){
						menuTwo.selectChild = 0;
						menuTwo.check = false;
						angular.forEach(menuTwo.children, function(menuThree){
							if(menuSet.has(menuThree.id) || menuThree.parent_id == '28'){
								menuThree.check = true;
								menuTwo.selectChild ++;
							} else {
								menuThree.check = false;
							}
						});
						if(menuTwo.selectChild != 0){
							menuTwo.check = true;
						}
					});
				});
				$scope.formData.loadindRole = false;
			} else {
				toaster.error({title: '', body: '加载角色下菜单失败'});
			}
		});
	};
	
	init();
	//初始化加载公司内角色列表
	function init(){
		$scope.formData = {
			'loadingMenu': true,	//控制加载菜单页的等待圈
			'loadindRole': true		//控制加载角色菜单时的等待圈
		};

		$resource('/api/as/sc/role/getCompanyRoleList', {}, {}).save({}, function(res){
			if (res.errcode === 0){
				$scope.roleList = res.data;
				$scope.loadMenuList();
			} else {
				toaster.error({title: '', body: '加载角色列表失败,请重试'});
			}
		});
	};

	//保存角色,失去焦点(keyCode=0)或回车(keyCode=13)时保存
	$scope.saveRole = function(role, event) {
		//非修改状态无反应
		if(!role.edit){
			return;
		}
		var keyCode = event.keyCode || event.which;
		if(keyCode == 0 || keyCode == 13){
			if(checkRoleInfo(role)){
				role.name = $scope.formData.id;
				$resource('/api/ac/sc/roleService/saveRole', {}, {}).save(role, function(res){
					if (res.errcode === 0){
						toaster.success({title: '', body: '保存角色成功'});
						role.id = res.data.id;
						if($scope.formData.addRole){
							angular.forEach($scope.menuList, function(menuOne){
								menuOne.open = true;
								angular.forEach(menuOne.children, function(menuTwo){
									if(menuTwo.id == '28'){
										menuTwo.check = true;
										menuTwo.selectChild = menuTwo.children.length;
									} else {
										menuTwo.selectChild = 0;
										menuTwo.check = false;
									}
									angular.forEach(menuTwo.children, function(menuThree){
										menuThree.check = false;
										if(menuThree.parent_id == '28'){
											menuThree.check = true;
										}
									});
								});
							});
						}
					} else {
						toaster.error({title: '', body: res.errmsg});
					}
					$scope.formData.addRole = false;
				});
			}
		}
	};
	
	//删除角色
	$scope.delRole = function(role){
		if($scope.formData.loadindRole){
			return;
		}
		if(confirm('确认删除此角色吗？')){
			$resource('/api/ac/sc/roleService/deleteRole', {}, {}).save({'id': role.id}, function(res){
				if (res.errcode === 0){
					$scope.roleList.splice($scope.formData.id, 1);
					$scope.formData.id = 0;
				} else {
					toaster.error({title: '', body: res.errmsg});
				}
			});
		}
	};
	
	//添加角色
	$scope.addRole = function(){
		if(!$scope.formData.addRole){
			$scope.formData.addRole = true;
			$scope.roleList.push({'edit': true});
			$scope.formData.id = $scope.roleList.length - 1;
		}
	};
		
	//选择二级菜单
	$scope.selectMenuTwo = function(menuTwo) {
		if(menuTwo.id == '28'){
			return;
		}
		if(!menuTwo.children){
			menuTwo.check = !menuTwo.check;
		} else if(menuTwo.selectChild == menuTwo.children.length){
			menuTwo.selectChild = 0;
			menuTwo.check = false;
		} else {
			menuTwo.selectChild = menuTwo.children.length;
			menuTwo.check = true;
		}
		angular.forEach(menuTwo.children, function(menuThree){
			menuThree.check = menuTwo.check;
		});
	}

	//选择三级菜单,检测是否需要勾选二级菜单
	$scope.selectMenuThree = function(menuTwo, menuThree) {
		if(menuTwo.id == '28'){
			return;
		}
		menuThree.check = !menuThree.check;
		if(menuThree.check){
			menuTwo.check = true;
			menuTwo.selectChild ++;
		} else {
			menuTwo.selectChild --;
		}
		if(menuTwo.selectChild == 0){
			menuTwo.check = false;
		}
	}
	
	//保存菜单信息
	$scope.saveMenu = function() {
		var para = {
			'menus': []
		};
		var temp = $scope.roleList[$scope.roleList.length - 1];
		if($scope.formData.id == temp.name){
			para.role_id = temp.id;
		} else {
			para.role_id = $scope.roleList[$scope.formData.id].id;
		}
		angular.forEach($scope.menuList, function(menuOne){
			menuOne.selectChild = 0;
			angular.forEach(menuOne.children, function(menuTwo){
				if(menuTwo.check){
					menuOne.selectChild ++;
					para.menus.push(menuTwo.id);
					angular.forEach(menuTwo.children, function(menuThree){
						if(menuThree.check){
							para.menus.push(menuThree.id);
						}
					});
				}
			});
			if(menuOne.selectChild > 0){
				para.menus.push(menuOne.id);
			}
		});
		$resource('/api/ac/sc/roleService/setRoleForMenu', {}, {}).save(para, function(res){
			if (res.errcode === 0){
				toaster.success({title: '', body: '保存角色成功'});
			} else {
				toaster.error({title: '', body: res.errmsg});
			}
		});
	};

	//校验角色名称
	function checkRoleInfo(role){
		var site = $scope.roleList.length - 1;
		//此时为添加角色,角色名为空时不添加
		if($scope.formData.addRole && ($scope.formData.id == site || !$scope.formData.id)){
			$scope.formData.addRole = false;
			$scope.roleList.splice(site, 1);
			return false;
		}
		//无任何修改,直接返回不做处理
		if($scope.formData.id == $scope.formData.lastIndex){
			role.edit = false;
			return false;
		}
		if(!$scope.formData.id){
			toaster.warning({title: '', body: '角色名称不能为空'});
			return false;
		}
		if($scope.formData.id.length > 7){
			toaster.warning({title: '', body: '角色名称在7个字以内'});
			return false;
		}
		role.edit = false;
		return true;
	}


};