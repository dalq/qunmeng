var App = angular.module('role', []);

App.config(require('./router'));

//service
App.factory('roleservice', require('./service'));

//Controllers
App.controller('roleList', require('./controllers/rolelist'));
App.controller('allotuser', require('./controllers/allotuser'));
App.controller('setAuth', require('./controllers/setAuth'));
App.controller('setTemplete', require('./controllers/setTemplete'));
App.controller('setDict', require('./controllers/setDict'));
App.controller('setRoleBindInfo', require('./controllers/setRoleBindInfo'));
App.controller('templete', require('./controllers/templete'));
App.controller('setRoleForTemp', require('./controllers/setrolefortemp'));
App.controller('roleInfo_new', require('./controllers/roleInfo_new'));


module.exports = App;