/**
 * 子模块service
 * DHpai
 */
var service = function($resource, $q, $state, $modal){
    
    //机构列表
    var officeList = '/api/as/sc/office/forRoleList';
    //用户列表
    var roleList = '/api/as/sc/role/getList';
    //用户详情
    var roleInfo = '/api/ac/sc/roleService/getById';
    //删除用户
    var delRole = '/api/ac/sc/roleService/deleteRole';
    //添加用户
    var saveRole = '/api/ac/sc/roleService/save';
    //用户权限列表
    var userList = '/api/as/sc/role/userForRoleList';
    //移除角色分配
    var delUserFromRole = '/api/as/sc/role/delUserFromRole';
    

    return {
        officeList : function(){
            return $resource(officeList, {}, {});
        },
        roleList : function(){
            return $resource(roleList, {}, {});
        },
        roleInfo : function(){
            return $resource(roleInfo, {}, {});
        },
        delRole : function(){
            return $resource(delRole, {}, {});
        },
        saveRole : function(){
            return $resource(saveRole, {}, {});
        },
        userList : function(){
            return $resource(userList, {}, {});
        },
        relieveUser : function(){
            return $resource(delUserFromRole, {}, {});
        }
    };

};

module.exports = service;