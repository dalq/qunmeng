 /**
 * 子模块路由
 * DHpai
 */

var router = function($urlRouterProvider, $stateProvider){


 	$stateProvider


    //角色列表
    .state('app.roleList', {
        url: "/role/rolelist",
        views: {
            'main@' : {
                template : require('./views/role.html'),
                controller : 'roleList',
            }
        },
        resolve:{
            officeList : function(roleservice){
                return roleservice.officeList();
            },
            roleList : function(roleservice){
                return roleservice.roleList();
            },
            roleInfo : function(roleservice){
                return roleservice.roleInfo();
            },
            delRole : function(roleservice){
                return roleservice.delRole();
            },
            saveRole : function(roleservice){
                return roleservice.saveRole();
            },
            userList : function(roleservice){
                return roleservice.userList();
            },
            relieveUser : function(roleservice){
                return roleservice.relieveUser();
            }
        }
    })

    //角色列表(公司管理)
    .state('app.roleManagement', {
        url: "/role/management",
        views: {
            'main@' : {
                template : require('./views/roleInfo_new.html'),
                controller : 'roleInfo_new',
            }
        },
        resolve:{
            transData : function(utilservice){
                return utilservice.transData;
            }
        }
    })

    //模板绑定角色
    .state('app.bindTemplete', {
        url: "/role/templete",
        views: {
            'main@' : {
                template : require('./views/templete.html'),
                controller : 'templete',
            }
        },
        resolve:{
            
        }
    })

	;

};

module.exports = router;