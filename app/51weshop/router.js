/**
* 子模块路由
* dlq
*/

var router = function ($urlRouterProvider, $stateProvider) {


     $stateProvider

        //产品列表
        .state('app.weshopproductlist', {
            url: "/weshop/productlist/:state",
            views: {
                'main@': {
                    template: require('./views/productlist.html'),
                    controller: 'weshopproductlist',
                }
            },
            resolve: {
                date2str: function (utilservice) {
                    return utilservice.getDate;
                },
                str2date: function (utilservice) {
                    return utilservice.str2date;
                },
            }
        })

        //产品市场列表
        .state('app.productmarket', {
            url: "/weshop/productmarket",
            views: {
                'main@': {
                    template: require('./views/productmarket.html'),
                    controller: 'productmarket',
                }
            },
            resolve: {
            	userinfo: function (dashboardservice) {
					return dashboardservice.userinfo;
				}
            }
        })



        //商户列表
        .state('app.weshopbussinesslist', {
            url: "/weshop/bussinesslist.html",
            views: {
                'main@': {
                    template: require('./views/bussinesslist.html'),
                    controller: 'weshopbussinesslist',
                }
            },
            resolve: {
              
        
            }
        })


        //创建商家
        .state('app.weshopbussiness_create', {
            url: "/weshop/bussinesscreate.html",
            views: {
                'main@' : {
                    template : require('../996tpl/views/form.html'),
                    controller : 'weshopbussinesscreate',
                }
            },
            resolve:{
                formconfig : function(formservice){
                    return formservice.formconfig();
                },
                model : function(weshopservice){
                    return weshopservice.bussinessmodel;
                }
            }
        })


        //编辑商家
        .state('app.weshopbussiness_edit', {
            url: "/weshopbussiness/edit/:id",
            views: {
                'main@' : {
                    template : require('../996tpl/views/form.html'),
                    controller : 'weshopbussinessedit',
                }
            },
            resolve:{
                formconfig : function(formservice){
                    return formservice.formconfig();
                },
                model : function(weshopservice){
                    return weshopservice.bussinessmodel;
                }
            }
        })


        //商家详情
        .state('app.weshopbussiness_info', {
            url: "/weshopbussiness/info/:id",
            views: {
                'main@' : {
                    template : require('../996tpl/views/form.html'),
                    controller : 'weshopbussinessinfo',
                }
            },
            resolve:{
                formconfig : function(formservice){
                    return formservice.formconfig();
                },
                model : function(weshopservice){
                    return weshopservice.bussinessmodel;
                }
            }
        })


        //分类列表
        .state('app.weshopproducttypelist', {
            url: "/weshop/producttypelist.html",
            views: {
                'main@': {
                    template: require('./views/producttypelist.html'),
                    controller: 'weshopproducttypelist',
                }
            },
            resolve: {
              
        
            }
        })



        //轮播图列表
        .state('app.weshopadlist', {
            url: "/weshop/adlist.html",

            views: {
                'main@': {
                    template: require('./views/adlist.html'),
                    controller: 'weshopadlist',
                }
            },
            resolve: {
              
        
            }
        })



        //弹出轮播图详情
        .state('app.weshopad', {
            url: "/weshop/ad/:id",
            size: 'lg',
            template : require('../996tpl/views/popform.html'),
            controller : 'weshopad',
            resolve:{
                formconfig : function(formservice){
                    return formservice.formconfig();
                },
                model : function(weshopservice){
                    return weshopservice.admodel;
                }
            }
        })


        //商城页面设置
        .state('app.weshopconfigure', {
            url: "/weshop/configure.html",
            views: {
                'main@' : {
                    template : require('../996tpl/views/form.html'),
                    controller : 'weshopconfigure',
                }
            },
            resolve:{
                formconfig : function(formservice){
                    return formservice.formconfig();
                },
                model : function(weshopservice){
                    return weshopservice.configuremodel;
                }
            }
        })


        //弹出产品分类详情
        .state('app.weshopproducttype', {
            url: "/weshop/type/:id",
            size: 'lg',
            template : require('../996tpl/views/popform.html'),
            controller : 'weshopproducttype',
            resolve:{
                formconfig : function(formservice){
                    return formservice.formconfig();
                },
                model : function(weshopservice){
                    return weshopservice.producttypemodel;
                }
            }
        })


        //订单列表
        .state('app.weshoporderlist', {
            url: "/weshop/orderlist.html",
            views: {
                'main@': {
                    template: require('./views/orderlist.html'),
                    controller: 'weshoporderlist',
                }
            },
            resolve: {
                date2str: function (utilservice) {
                    return utilservice.getDate;
                },
                str2date: function (utilservice) {
                    return utilservice.str2date;
                },
        
            }
        })


        //弹出订单详情
        .state('app.weshoporderinfo', {
            url: "/weshop/order/:id",
            size: 'lg',
            template : require('./views/orderinfo.html'),
            controller : 'weshoporderinfo',
            resolve:{
                // formconfig : function(formservice){
                //     return formservice.formconfig();
                // },
                // model : function(weshopservice){
                //     return weshopservice.producttypemodel;
                // }
            }
        })



        //居游系统的产品列表
        .state('app.weshopsysproductlist', {
            url: "/weshop/sysproductlist?type&sys_area",
            views: {
                'main@': {
                    template: require('./views/sysproductlist.html'),
                    controller: 'weshopsysproductlist',
                }
            },
            resolve: {
                date2str: function (utilservice) {
                    return utilservice.getDate;
                },
                str2date: function (utilservice) {
                    return utilservice.str2date;
                },
            }
        })



        //微店系统首页
        .state('app.weshopdashboard', {
            url: "/weshop/dashboard",
            views: {
                'main@': {
                    template: require('./views/dashboard.html'),
                    //controller: 'weshopsysproductlist',
                }
            },
            resolve: {
                date2str: function (utilservice) {
                    return utilservice.getDate;
                },
                str2date: function (utilservice) {
                    return utilservice.str2date;
                },
            }
        })



        //栏目app.weshopcolumn
        .state('app.weshopcolumn', {
            url: "/weshop/column.html",
            views: {
                'main@': {
                    template: require('./views/column.html'),
                    controller: 'weshopcolumn',
                }
            },
            resolve: {
                
            }
        })

        //创建栏目app.weshopproducttype
        .state('app.weshopcolumnmodel', {
            url: "/weshop/column/:id",
            size: 'lg',
            template : require('../996tpl/views/popform.html'),
            controller : 'weshopcolumnmodel',
            resolve:{
                formconfig : function(formservice){
                    return formservice.formconfig();
                },
                model : function(weshopservice){
                    return weshopservice.columnmodel;
                }
            }
        })


        //海报
        // .state('app.weshopposter', {
        //     url: "/weshop/poster/:id",
        //     size: 'lg',
        //     template : require('../996tpl/views/popform.html'),
        //     controller : 'weshopposter',
        //     resolve:{
        //         formconfig : function(formservice){
        //             return formservice.formconfig();
        //         },
        //         model : function(weshopservice){
        //             return weshopservice.postermodel;
        //         }
        //     }
        // })


        //海报
        .state('app.weshopposter', {
            url: "/weshop/poster/:id",
            views: {
                'main@': {
                    template: require('./views/setposter.html'),
                    controller: 'weshopposter',
                }
            },
            resolve:{
                date2str: function (utilservice) {
                    return utilservice.getDate;
                },
                str2date: function (utilservice) {
                    return utilservice.str2date;
                },
            }
        })




        //居游系统产品列表——管理端
        .state('app.weshopsystemproductlist', {
            url: "/weshop/systemproductlist.html",
            views: {
                'main@': {
                    template: require('./views/weshopsysproductlist.html'),
                    controller: 'weshopsystemproductlist',
                }
            },
            resolve:{
                date2str: function (utilservice) {
                    return utilservice.getDate;
                },
                str2date: function (utilservice) {
                    return utilservice.str2date;
                },
                what : function () {
                    return 'xitong';
                }
            }
        })


        //居游系统产品列表——商家端
        .state('app.weshopsys', {
            url: "/weshop/sys.html",
            views: {
                'main@': {
                    template: require('./views/sys.html'),
                    controller: 'weshopsystemproductlist',
                }
            },
            resolve:{
                date2str: function (utilservice) {
                    return utilservice.getDate;
                },
                str2date: function (utilservice) {
                    return utilservice.str2date;
                },
                what : function () {
                    return 'shangjia';
                }
            }
        })




        //群盟系统产品列表——管理端
        .state('app.qunmengmanagerplist', {
            url: "/weshop/qunmengmanagerplist.html",
            views: {
                'main@': {
                    template: require('./views/weshopsysproductlist.html'),
                    controller: 'weshopsystemproductlist',
                }
            },
            resolve:{
                date2str: function (utilservice) {
                    return utilservice.getDate;
                },
                str2date: function (utilservice) {
                    return utilservice.str2date;
                },
                what : function () {
                    return 'qunmeng';
                }
            }
        })



        //群盟系统产品列表——商家端
        .state('app.qunmengbusinessplist', {
            url: "/weshop/qunmengbusinessplist.html",
            views: {
                'main@': {
                    template: require('./views/sys.html'),
                    controller: 'weshopsystemproductlist',
                }
            },
            resolve:{
                date2str: function (utilservice) {
                    return utilservice.getDate;
                },
                str2date: function (utilservice) {
                    return utilservice.str2date;
                },
                what : function () {
                    return 'qunmengshangjia';
                }
            }
        })


        //群盟系统产品列表——待审批列表
        .state('app.qunmengshenpi', {
            url: "/weshop/qunmengshenpi.html",
            views: {
                'main@': {
                    template: require('./views/qunmengshenpi.html'),
                    controller: 'qunmengplist',
                }
            },
            resolve:{
                date2str: function (utilservice) {
                    return utilservice.getDate;
                },
                str2date: function (utilservice) {
                    return utilservice.str2date;
                },
                what : function () {
                    return 'manager';
                }
            }
        })



        //优惠券列表
        .state('app.weshopcouponlist', {
            url: "/weshop/couponlist.html",
            views: {
                'main@': {
                    template: require('./views/couponlist.html'),
                    controller: 'weshopcouponlist',
                }
            },
            resolve:{
                date2str: function (utilservice) {
                    return utilservice.getDate;
                },
                str2date: function (utilservice) {
                    return utilservice.str2date;
                },
                
            }
        })


        //创建优惠券
        .state('app.weshopcoupon_create', {
            url: "/weshop/couponcreate.html",
            views: {
                'main@' : {
                    template : require('../996tpl/views/form.html'),
                    controller : 'weshopcouponcreate',
                }
            },
            resolve:{
                formconfig : function(formservice){
                    return formservice.formconfig();
                },
                model : function(weshopservice){
                    return weshopservice.couponmodel;
                }
            }
        })

        //编辑优惠券
        .state('app.weshopcoupon_edit', {
            url: "/weshop/couponedit/:id",
            views: {
                'main@' : {
                    template : require('../996tpl/views/form.html'),
                    controller : 'weshopcouponedit',
                }
            },
            resolve:{
                formconfig : function(formservice){
                    return formservice.formconfig();
                },
                model : function(weshopservice){
                    return weshopservice.couponmodel;
                }
            }
        })



        //资金流水明细
        .state('app.weshopbalance', {
            url: "/weshop/balance.html",
            views: {
                'main@': {
                    template: require('./views/balance.html'),
                    controller: 'weshopbalance',
                }
            },
            resolve: {
                date2str: function (utilservice) {
                    return utilservice.getDate;
                },
                str2date: function (utilservice) {
                    return utilservice.str2date;
                },
            }
        })

        //套票统计
        .state('app.ticketstatistical', {
            url: "/weshop/ticketstatistics.html",
            views: {
                'main@': {
                    template: require('./views/ticketstatistics.html'),
                    controller: 'ticketstatistics',
                }
            },
            resolve: {
            	
            }
        })

        //套票详情
        .state('app.statisticsinfo', {
            url: "/weshop/statisticsinfo.html?city",
            views: {
                'main@': {
                    template: require('./views/statisticsinfo.html'),
                    controller: 'statisticsinfo',
                }
            },
            resolve: {
            	
            }
        })

        
};

module.exports = router;