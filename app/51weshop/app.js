var App = angular.module('weshop', []);

App.config(require('./router'));
App.factory('weshopservice', require('./service'));

App.controller('weshopproductlist',require('./controllers/weshopproductlist'));
App.controller('weshopcommission',require('./controllers/commission'));
App.controller('weshopbussinesslist',require('./controllers/bussinesslist'));
App.controller('weshopbussinesscreate',require('./controllers/bussinesscreate'));
App.controller('weshopbussinessinfo',require('./controllers/bussinessinfo'));
App.controller('weshopbussinessedit',require('./controllers/bussinessedit'));
App.controller('weshopproducttypelist',require('./controllers/producttypelist'));
App.controller('weshopproducttype',require('./controllers/producttype'));
App.controller('weshopadlist',require('./controllers/adlist'));
App.controller('weshopad',require('./controllers/ad'));
App.controller('weshopconfigure',require('./controllers/configure'));
App.controller('weshoporderlist',require('./controllers/orderlist'));
App.controller('weshoporderinfo',require('./controllers/orderinfo'));
App.controller('weshopsysproductlist',require('./controllers/sysproductlist'));
App.controller('weshopsetprice',require('./controllers/setprice'));
App.controller('productmarket',require('./controllers/productmarket'));
App.controller('productconfig',require('./controllers/productconfig'));

//设置产品详情里的顶图。
App.controller('weshopsettoppic',require('./controllers/settoppic'));

//栏目
App.controller('weshopcolumn',require('./controllers/column'));
App.controller('weshopcolumnmodel',require('./controllers/columnmodel'));

//海报
App.controller('weshopposter',require('./controllers/setposter'));


//居游系统产品列表
App.controller('weshopsystemproductlist',require('./controllers/weshopsysproductlist'));
//群盟系统产品列表
App.controller('qunmengplist',require('./controllers/qunmengplist'));
//优惠券列表
App.controller('weshopcouponlist',require('./controllers/couponlist'));
//优惠券选择产品列表
App.controller('weshoppp',require('./controllers/weshoppp'));


App.controller('weshopcouponcreate',require('./controllers/couponcreate'));
App.controller('weshopcouponedit',require('./controllers/couponedit'));

//账户资金流水
App.controller('weshopbalance',require('./controllers/balance'));

//用户关系
App.controller('weshoprelation',require('./controllers/relation'));

//销售统计
App.controller('weshopstatistics',require('./controllers/statistics'));
//套票统计
App.controller('ticketstatistics',require('./controllers/ticketstatistics'));
//套票详情
App.controller('statisticsinfo',require('./controllers/statisticsinfo'));
App.controller('weshoporderlistpop',require('./controllers/orderlistpop'));

//票机列表
App.controller('weshopdevicelist',require('./controllers/devicelist'));


module.exports = App;