/**
 * 子模块service
 * dlq
 */
var service = function($resource, $q, $modal, $state){

    //模型
    var bussinessmodel = [
        {
            'title' : '商家名称',
            'id' : 'name',
            'type' : 'text',
            'required' : true,
            'placeholder' : '必填',
        },
        {
            'title' : '电话',
            'id' : 'mobile',
            'type' : 'text',
        },
        {
            'title' : '地址',
            'id' : 'address',
            'type' : 'text',
        },
        // {
        //     'title' : '经度',
        //     'id' : 'longitude',
        //     'type' : 'number',
        // },
        // {
        //     'title' : '维度',
        //     'id' : 'latitude',
        //     'type' : 'number',
        // },
        {
            'title' : '简介',
            'id' : 'content',
            'type' : 'textarea',
            'state' : 'normal',
        },
        {
            'title' : '营业时间',
            'id' : 'open_time',
            'type' : 'text',
        },
        {
            'title' : '图片',
            'id' : 'img',
            'type' : 'image',
        },
    ];


    //模型
    var admodel = [
        {
            'title' : '图片',
            'id' : 'url',
            'type' : 'image',
        },
        {
            'title' : '位置',
            'id' : 'type',
            'type' : 'select',
            'info' : [
                {'name' : '首页', 'value' : '0'}
            ],
            'value' : '0',
        },
        {
            'title' : '排序',
            'id' : 'asort',
            'type' : 'number',
        },

    ];


    //布局model
    var configuremodel = [
        {
            'title' : '是否启用轮播图',
            'id' : 'ad',
            'type' : 'switch',
            'open' : '1',
            'close' : '0',
            'value' : '1',
        },

        {
            'title' : '轮播图高度',
            'id' : 'topimgsize',
            'type' : 'text',
            'value' : '260',
        },


        {
            'title' : '是否启用商家介绍',
            'id' : 'isshow',
            'type' : 'switch',
            'open' : '1',
            'close' : '0',
            'value' : '1',
        },

        {
            'title' : '商家介绍',
            'id' : 'describe',
            'type' : 'textarea',
            'state' : 'normal',
        },

        {
            'title' : '是否启用分类',
            'id' : 'type',
            'type' : 'switch',
            'open' : '1',
            'close' : '0',
            'value' : '1',
        },

        {
            'title' : '分类图片大小',
            'id' : 'typeimgsize',
            'type' : 'text',
            'value' : '60',
        },

        {
            'title' : '首页布局方式',
            'id' : 'mui',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '商家和列表', 'value' : '0'},
                {'name' : '仅商家', 'value' : '1'},
                {'name' : '仅列表', 'value' : '2'},
            ],
            'value' : '2',
        },

        {
            'title' : '导航配置',
            'id' : 'nav',
            'type' : 'text',
        },

        {
            'title' : '二维码图片',
            'id' : 'qrcodeimg',
            'type' : 'image',
        },

        {
            'title' : '分享商场标题',
            'id' : 'share_title',
            'type' : 'text',
        },

        {
            'title' : '分享商场描述',
            'id' : 'share_describe',
            'type' : 'text',
        },

        {
            'title' : '分享商场logo',
            'id' : 'share_img',
            'type' : 'image',
        },

        {
            'title' : '成为会员赠积分',
            'id' : 'give_integral',
            'type' : 'number',
            'value' : '0',
        },
        

    ];




    //布局model
    var producttypemodel = [
        {
            'title' : '分类名称',
            'id' : 'type_name',
            'type' : 'text',
        },
        {
            'title' : '分类图标',
            'id' : 'logo',
            'type' : 'image',
        },
        {
            'title' : '排序',
            'id' : 'asort',
            'type' : 'number',
        },
    ];


    //栏目model
    var columnmodel = [
        {
            'title' : '栏目名称',
            'id' : 'column_name',
            'type' : 'text',
        },
        {
            'title' : '栏目布局方式',
            'id' : 'mui',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '商家和列表', 'value' : '0'},
                {'name' : '仅商家', 'value' : '1'},
                {'name' : '仅列表', 'value' : '2'},
                {'name' : '横向滚动', 'value' : '3'},
                {'name' : '样式四', 'value' : '4'},
                {'name' : '样式五', 'value' : '5'},
            ],
            'value' : '2',
        },
        {
            'title' : '排序',
            'id' : 'asort',
            'type' : 'number',
        },

        {
            'title' : '是否启用',
            'id' : 'state',
            'type' : 'switch',
            'open' : '1',
            'close' : '0',
            'value' : '1',
        },

    ];


    //海报
    var postermodel = [
        {
            'title' : '海报图片',
            'id' : 'share_pic',
            'type' : 'image',
            'tip' : '图片尺寸：750px * 1334px',
        },
        {
            'title' : '二维码大小',
            'id' : 'qrcode_size',
            'type' : 'select',
            'info' : [
                {'name' : '1', 'value' : '1'},
                {'name' : '2', 'value' : '2'},
                {'name' : '3', 'value' : '3'},
                {'name' : '4', 'value' : '4'},
                {'name' : '5', 'value' : '5'},
                {'name' : '6', 'value' : '6'},
                {'name' : '7', 'value' : '7'},
                {'name' : '8', 'value' : '8'},
                {'name' : '9', 'value' : '9'},
                {'name' : '10', 'value' : '10'},
            ],
            'value' : '5',
        },

        {
            'title' : '二维码x坐标',
            'id' : 'width',
            'type' : 'text',
            'value' : '535',
        },

        {
            'title' : '二维码y坐标',
            'id' : 'height',
            'type' : 'text',
            'value' : '1106',
        },

        {
            'title' : '微信名x坐标',
            'id' : 'nickname_width',
            'type' : 'text',
            'value' : '159',
        },

        {
            'title' : '微信名y坐标',
            'id' : 'nickname_height',
            'type' : 'text',
            'value' : '1172',
        },


        {
            'title' : '名字字体大小',
            'id' : 'nickname_size',
            'type' : 'text',
            'value' : '36',
        },

        {
            'title' : '字体颜色',
            'id' : 'nickname_color',
            'type' : 'text',
            'value' : '#333333',
        },


        {
            'title' : '显示文字长度',
            'id' : 'nickname_length',
            'type' : 'text',
            'value' : '9',
        },


    ];


    //优惠券
    var couponmodel = [

        {
            'title' : '金额',
            'id' : 'money',
            'type' : 'text',
            'value' : '0',
        },

        {
            'title' : '描述',
            'id' : 'description',
            'type' : 'text',
        },

        {
            'title' : '满多少可用',
            'id' : 'cut',
            'type' : 'text',
            'value' : '0',
        },

        {
            'title' : '库存',
            'id' : 'stock_num',
            'type' : 'text',
            'value' : '0',
        },

        {
            'title' : '开始时间',
            'id' : 'start_date',
            'type' : 'date1',
            'value' : 'now',
        },

        {
            'title' : '结束时间',
            'id' : 'end_date',
            'type' : 'date1',
            'value' : 'now',
        },

        {
            'title' : '使用范围',
            'id' : 'range',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '通用', 'value' : '0'},
                {'name' : '部分', 'value' : '1'},
            ],
            'value' : '0',
        },


       

        {
            'title' : '限制范围',
            'id' : 'max_limit_state',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '不限制', 'value' : '0'},
                {'name' : '全部限制', 'value' : '1'},
                {'name' : '当前机构限制', 'value' : '2'},
            ],
            'value' : '0',
        },

        {
            'title' : '限制方式',
            'id' : 'max_limit_type',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '用户id', 'value' : '0'},
                {'name' : '身份证号', 'value' : '1'},
            ],
            'value' : '0',
        },

        {
            'title' : '最多可领取',
            'id' : 'max_limit_num',
            'type' : 'text',
            'value' : '0',
        },


        {
            'title' : '校验id',
            'id' : 'check_id',
            'type' : 'text',
        },


        {
            'title' : '是否启用补贴校验',
            'id' : 'rule_check',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '不启用', 'value' : '0'},
                {'name' : '启用', 'value' : '1'},
            ],
            'value' : '0',
        },

        {
            'title' : '获得方式',
            'id' : 'get_mode',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '免费领取', 'value' : '0'},
                {'name' : '积分兑换', 'value' : '1'},
            ],
            'value' : '0',
        },

        {
            'title' : '兑换所需积分',
            'id' : 'exchange_integral',
            'type' : 'text',
            'value' : '0',
        },


        

       

        {
            'title' : '是否实名',
            'id' : 'real_name',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '不用', 'value' : '0'},
                {'name' : '需要', 'value' : '1'},
            ],
            'value' : '0',
        },

    ];

   
    return {

        bussinessmodel : function(){
            return bussinessmodel;
        },

        admodel : function(){
            return admodel;
        },

        configuremodel : function(){
            return configuremodel;
        },

        producttypemodel : function(){
            return producttypemodel;
        },
        columnmodel : function(){
            return columnmodel;
        },
        postermodel : function(){
            return postermodel;
        },
        couponmodel : function(){
            return couponmodel;
        },
       
    };

};

module.exports = service;