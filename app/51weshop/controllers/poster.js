module.exports = function($scope, formconfig, model, obj, $resource, $modalInstance){

	//如果没有数据
	formconfig.start({
		'title' : '海报详情',
		'formtitle' : '海报基本信息',
		'elements' : model(),
		'info' : {
			'obj' : {
				'data' : obj
			}
		},
		'save' : function(item){

			item['code'] = obj.code;
			item['title'] = obj.title;

			console.log(item)

			$resource('/api/ac/wc/productShareService/createProducShare', {}, {}).save(item, function (res) {
	          console.log(res);

	          if (res.errcode !== 0) {
	            alert("数据获取失败");
	            return;
	          }

	          $modalInstance.close();

	        });
		}
	}, $scope);

	$scope.form = formconfig;


	$scope.cancel = function(){

		$modalInstance.close();

	}

};