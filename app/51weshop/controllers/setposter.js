module.exports = function($scope, $modal,$modalInstance, $q, $http, $resource, FileUploader, obj){

  $scope.posterobj = {
      'share_pic' : '',
      'qrcode_size' : 1,
      'width' : 0,
      'height' : 0,
      'nickname_width' : 0,
      'nickname_height' : 0,
      'nickname_size' : 12,
      'nickname_color' : '#333333',
      'nickname_length' : 9,
  };


  $scope.pic = {
      'w' : 0,
      'h' : 0,
  }

  $scope.customSettings = {
    control: 'brightness',
    theme: 'bootstrap',
    position: 'top left',
    animationSpeed : 50,
    animationEasing: 'swing',
    hide: null,
    hideSpeed: 100,
  };


  angular.extend($scope.posterobj, obj);


  $scope.init = function(){

      console.log('init');

      var screenImage = $("#poster");  
      var theImage = new Image();  
      theImage.src = screenImage.attr("src");  
      var imageWidth = theImage.width;  
      var imageHeight = theImage.height; 

      $scope.pic.w = imageWidth;
      $scope.pic.h = imageHeight;


      var biLi = imageWidth / 250;

      //没有比例存起来。如果有比例，和之前的比较。
      if(!$scope.posterobj['biLi']){
          $scope.posterobj['biLi'] = biLi;
      }else{
          //如果两次比例不一致。重新换算坐标位置。重新报存比例。
          var oldBiLi = $scope.posterobj['biLi'];
          console.log('oldBiLi');
          console.log(oldBiLi);
          console.log($scope.qrcode);
          if(biLi != oldBiLi){
              $scope.posterobj.width = $scope.posterobj.width / oldBiLi * biLi;
              $scope.posterobj.height = $scope.posterobj.height / oldBiLi * biLi;
              $scope.posterobj.nickname_width = $scope.posterobj.nickname_width / oldBiLi * biLi;
              $scope.posterobj.nickname_height = $scope.posterobj.nickname_height / oldBiLi * biLi;

              $scope.posterobj['biLi'] = biLi;
              console.log('果两次比例不一致。重新换算坐标位置。重新报存比例');
              console.log($scope.posterobj);
          }
      }
      

      console.log(imageWidth);
      console.log(imageHeight);


      $scope.qrcode = {
        'left' : $scope.posterobj.width / biLi,
        'top' : $scope.posterobj.height / biLi,
      }

      $scope.wxname = {
        'left' : $scope.posterobj.nickname_width / biLi,
        'top' : $scope.posterobj.nickname_height / biLi,
      }

      console.log($scope.qrcode);
      console.log($scope.wxname);



  };




  $scope.gogo = function(){

    if($scope.posterobj.share_pic == ''){
        alert('请设置海报');
        return;
    }

    var screenImage = $("#poster");  
    var theImage = new Image();  
    theImage.src = screenImage.attr("src");  
    var imageWidth = theImage.width;  
    var imageHeight = theImage.height; 

    var biLi = imageWidth / 250;

    console.log(imageWidth);
    console.log(imageHeight);

    console.log($("#qrcode").position().left * biLi);
    console.log($("#qrcode").position().top * biLi);
    console.log($("#wxname").position().left * biLi);
    console.log($("#wxname").position().top * biLi);

    $scope.posterobj.width = $("#qrcode").position().left * biLi;
    $scope.posterobj.height = $("#qrcode").position().top * biLi;
    $scope.posterobj.nickname_width = $("#wxname").position().left * biLi;
    $scope.posterobj.nickname_height = $("#wxname").position().top * biLi;
    $scope.posterobj.qrcode_size = parseInt(1 * biLi) == 0 ? 1 : parseInt(1 * biLi);
    $scope.posterobj.nickname_size = parseInt(12 * biLi) == 0 ? 1 : parseInt(12 * biLi);

    console.log($scope.posterobj);

    $resource('/api/ac/wc/productShareService/createProducShare', {}, {}).save($scope.posterobj, function (res) {
      console.log(res);

      if (res.errcode !== 0) {
        alert("数据获取失败");
        return;
      }

      $modalInstance.close();

    });


  }


  $scope.uploader = new FileUploader({
      //url: 'http://cl.juyouhx.com/oss.php/oss/webuploader1?topdir=aa&selfdir=bb'
      url : 'https://txy.juyouhx.com/Api/Api/ObjectToOss'
  });

  $scope.uploader.filters.push({
      name: 'imageFilter',
      fn: function (item /*{File|FileLikeObject}*/, options) {
          var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
          return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
      }
  });

  $scope.uploader.formData.push({'dir' : 'weshop_poster' + "_" + Date.parse(new Date())});

  $scope.uploader.onSuccessItem = function (fileItem, response, status, headers) {
      console.log(fileItem);
      console.log(response);
      $scope.posterobj.share_pic = response.url;
  };

   //console.log(obj);

   // console.log('11111111');
   // console.log(jq("#dlq123").attr("src"));
   // console.log('111111111111');


   // $scope.init = function(){
  
   //    var cc = $('#dlq123').cropper({
   //      dragMode: 'move', //move  crop
   //      aspectRatio: 375 / 667, //显示比例
   //      //aspectRatio: 1,

   //      viewMode:1, //显示的样式
   //      autoCropArea: 0.65,//相对图片的剪裁区域
   //      restore: false,
   //      guides: false, //是否在剪裁框上显示虚线
   //      center: false,
   //      highlight: false,
   //      cropBoxMovable: true,//剪裁区域师傅可以移动
   //      cropBoxResizable: true,//剪裁区域是否可以缩放
   //      zoomable:false, //是否允许放大缩小图片
   //      touchDragZoom: false,//是否允许通过触摸移动来缩放图片
   //      mouseWheelZoom:false,//是否允许通过鼠标滚轮来缩放图片
   //      toggleDragModeOnDblclick: false,//双击启动 move  和crop 模式
   //      ready: function () {
   //          console.log('ready');
   //          getImageData =  $("#dlq123").cropper('getImageData');//获取显示的图片数据信息

   //          console.log(getImageData);
   //          //图片的比例
   //          biLi = getImageData.width / getImageData.naturalWidth;
   //          console.log('图片的比例:'+biLi);
   //          var qr_height = 150 * biLi;
   //          var qr_width = 150 * biLi;
   //          // Strict mode: set crop box data first
   //          var top = $('#qr_height').val() *biLi;
   //          var left = $('#qr_width').val() *biLi;

   //          cropBoxData = {"left":left,"top":top,"width":qr_width,"height":qr_height};
            
   //          console.log(cc);
   //          //console.log($().cropper());
   //          $("#dlq123").cropper('setCropBoxData', cropBoxData);
   //      },
   //      crop: function (e) {
   //        console.log('crop');
   //          cropBoxData = $("#dlq123").cropper('getCropBoxData');
   //          //二维码的坐标位置
   //          var send_qr_x = cropBoxData.left / biLi;
   //          var send_qr_y = cropBoxData.top / biLi;
   //          $('#qr_height').val(send_qr_y);
   //          $('#qr_width').val(send_qr_x);
   //          //二维码图片的实际大小
   //          var send_qr_width = cropBoxData.width / biLi;
   //          var send_qr_height = cropBoxData.height / biLi;
   //          $('#qrcode_height').val(send_qr_height);
   //          $('#qrcode_width').val(send_qr_width);
   //          if(send_qr_width < 150)
   //          {
   //              alert('二维码图片太小无法显示');
   //              cropBoxData.width = 150 * biLi;
   //              cropBoxData.height = 150 * biLi;

   //              $("#dlq123").cropper('setCropBoxData', cropBoxData);
   //              console.log('小于200啦');
   //          }

   //      }
   //    });
   // };

   
   
   


   //var image = jQuyer('#image');


   //console.log(image);


//     var biLi = 1;
//     //var cropper;
//      jQuery('#image').cropper({
//         dragMode: 'move', //move  crop
// //      aspectRatio: 375 / 667, //显示比例
//         aspectRatio: 1,

//         viewMode:1, //显示的样式
//         autoCropArea: 0.65,//相对图片的剪裁区域
//         restore: false,
//         guides: false, //是否在剪裁框上显示虚线
//         center: false,
//         highlight: false,
//         cropBoxMovable: true,//剪裁区域师傅可以移动
//         cropBoxResizable: true,//剪裁区域是否可以缩放
//         zoomable:false, //是否允许放大缩小图片
//         touchDragZoom: false,//是否允许通过触摸移动来缩放图片
//         mouseWheelZoom:false,//是否允许通过鼠标滚轮来缩放图片
//         toggleDragModeOnDblclick: false,//双击启动 move  和crop 模式
//         ready: function () {
//             console.log('ready');
//             // getImageData = jQuery(this).croppe.getImageData();//获取显示的图片数据信息

//             // console.log(getImageData);
//             // //图片的比例
//             // biLi = getImageData.width / getImageData.naturalWidth;
//             // console.log('图片的比例:'+biLi);
//             // var qr_height = 150 * biLi;
//             // var qr_width = 150 * biLi;
//             // // Strict mode: set crop box data first
//             // var top = $('#qr_height').val() *biLi;
//             // var left = $('#qr_width').val() *biLi;

//             // cropBoxData = {"left":left,"top":top,"width":qr_width,"height":qr_height};
//             // cropper.setCropBoxData(cropBoxData);
//         },
//         crop: function (e) {
//             // cropBoxData = cropper.getCropBoxData();
//             // //二维码的坐标位置
//             // var send_qr_x = cropBoxData.left / biLi;
//             // var send_qr_y = cropBoxData.top / biLi;
//             // $('#qr_height').val(send_qr_y);
//             // $('#qr_width').val(send_qr_x);
//             // //二维码图片的实际大小
//             // var send_qr_width = cropBoxData.width / biLi;
//             // var send_qr_height = cropBoxData.height / biLi;
//             // $('#qrcode_height').val(send_qr_height);
//             // $('#qrcode_width').val(send_qr_width);
//             // if(send_qr_width < 150)
//             // {
//             //     alert('二维码图片太小无法显示');
//             //     cropBoxData.width = 150 * biLi;
//             //     cropBoxData.height = 150 * biLi;

//             //     cropper.setCropBoxData(cropBoxData);
//             //     console.log('小于200啦');
//             // }

//         }
//     });





    $scope.cancel = function () {
        $modalInstance.close();
    }

};
