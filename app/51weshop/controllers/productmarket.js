module.exports = function($scope, $resource, userinfo){

	$scope.searchform = {};

	//用户信息
	userinfo().then(function (res) {

		$scope.company_code = res.company_code;

	});

	/* 分页
     * ========================================= */
	$scope.maxSize = 5;            //最多显示多少个按钮
	$scope.bigCurrentPage = 1;      //当前页码
	$scope.itemsPerPage = 10;         //每页显示几条

	$scope.load = function () {

		var para = {
			pageNo: $scope.bigCurrentPage,
			pageSize: $scope.itemsPerPage
		};

		console.log($scope.searchform);

		para = angular.extend($scope.searchform, para);

		console.log($scope.searchform);

		$resource('/api/as/wc/product/marketproductlist', {}, {}).save(para, function (res) {
			console.log(para);
			console.log(res);

			if (res.errcode !== 0) {
				alert("数据获取失败");
				return;
			}

			$scope.objs = res.data.results;
			$scope.bigTotalItems = res.data.totalRecord;

		});

	};
	$scope.load();

	$scope.up = function (obj) {
		$resource('/api/ac/wc/productService/createProductFromMarket', {}, {}).save(obj, function (res) {

			if (res.errcode !== 0) {
				alert(res.errmsg);
				return;
			}
			alert("上架成功");

		});
	}


};

