// 商家能看到的居游系统产品 what = shangjia
// 运营能看到的居游系统产品 what = xitong
// 运营能看到的群盟系统产品 what = qunmeng
// 商家能看到的群盟系统产品 what = qunmengshangjia

module.exports = function($scope, $modal, $state,  $resource, $stateParams, str2date, date2str, what){


	console.log(what);

	$scope.what = what;

	var url_list = '';
	var url_asort = '';

	if(what == 'xitong'){
		url_list = '/api/as/wc/product/wdsysproductlist';
		url_asort = '/api/as/wc/product/update';
	}else if(what == 'shangjia'){
		url_list = '/api/as/wc/product/wdsysproductlist';
		url_asort = '/api/as/wc/product/update';
	}else if(what == 'qunmeng'){
		url_list = '/api/as/wc/product/applyadoptproductlist';
		url_asort = '/api/as/wc/product/updateapplyasort';
	}else if(what == 'qunmengshangjia'){
		url_list = '/api/as/wc/product/applyadoptproductlist';
		url_asort = '/api/as/wc/product/updateapplyasort';
	}


	

	$scope.searchform = {};
	/* 分页
     * ========================================= */
	$scope.maxSize = 5;            //最多显示多少个按钮
	$scope.bigCurrentPage = 1;      //当前页码
	$scope.itemsPerPage = 10;         //每页显示几条

	$scope.load = function () {

		var para = {
			'pageNo': $scope.bigCurrentPage,
			'pageSize': $scope.itemsPerPage,
		};

		if(what == 'shangjia' || what == 'qunmengshangjia'){
			para['isallow'] = '1';
		}

		//console.log($scope.searchform);

		para = angular.extend($scope.searchform, para);

		console.log(para);

		$resource(url_list, {}, {}).save(para, function (res) {
			console.log(res);

			if (res.errcode !== 0) {
				alert("数据获取失败");
				return;
			}

			$scope.objs = res.data.results;
			$scope.bigTotalItems = res.data.totalRecord;

		});

	};
	$scope.load();


	$scope.addprice = function(obj){

		var para = {
			'id' : obj.id,
			'add_price' : obj.add_price,
		}

		aaa(para);
	}


	$scope.asort = function(obj){

		var para = {
			'asort' : obj.asort
		};

		if(what == 'xitong'){
			para['id'] = obj.id;
		}else if(what == 'qunmeng'){
			para['code'] = obj.code;
		}


		$resource(url_asort, {}, {}).save(para, function (res) {
			console.log(res);

			if (res.errcode !== 0) {
				alert(res.errmsg);
				return;
			}

			alert('修改成功');

			$scope.load();

		});

	}


	$scope.up = function(obj){

		var para = {
			'id' : obj.id,
			// 'code' : obj.code,
			// 'sale_code' : obj.sale_code,
			'isallow' : '1',
		}

		aaa(para);
	}


	$scope.down = function(obj){

		var para = {
			'id' : obj.id,
			// 'code' : obj.code,
			// 'sale_code' : obj.sale_code,
			'isallow' : '0',
		}

		aaa(para);
	}



	function aaa(para){


		$resource('/api/ac/wc/productService/createProductAddPrice', {}, {}).save(para, function (res) {
			console.log(res);

			if (res.errcode !== 0) {
				alert(res.errmsg);
				return;
			}

			alert('修改成功');

			$scope.load();

		});

	}


	$scope.uptoshangjia = function(obj){

		$resource('/api/ac/wc/productService/createSysProduct', {}, {}).save({'id' : obj.id, 'sale_code' : obj.sale_code}, function (res) {
			console.log(res);

			if (res.errcode !== 0) {
				alert(res.errmsg);
				return;
			}

			alert('修改成功');

			$scope.load();

		});
		
	}


	//群盟产品上架到商城
	$scope.qunmenguptoshangjia = function(obj){

		$resource('/api/ac/wc/productService/createPubProduct', {}, {}).save({'id' : obj.id, 'sale_code' : obj.sale_code}, function (res) {
			console.log(res);

			if (res.errcode !== 0) {
				alert(res.errmsg);
				return;
			}

			alert('修改成功');

			$scope.load();

		});
		
	}




	//设置多图-微分销用
	$scope.setImages = function(obj){

		$resource('/api/as/wc/productshare/getshareattr', {}, {}).save({code : obj.sale_code}, function (res) {
			console.log(res);
			var pobj = {
				'code' : obj.sale_code,
				'title' : obj.name,
			};

			var lala = {};

			if (res.errcode === 0) {
				//有数据
				// pobj['top_pic'] = res.data.top_pic;
				// pobj['words'] = res.data.words;
				// pobj['bookingnotes'] = res.data.bookingnotes;
				// pobj['detail'] = res.data.detail;

				angular.extend(res.data, pobj);

				lala = res.data;
			}
			else if(res.errcode === 10003) {
				//没数据
				angular.extend(lala, pobj);
			}
			else {
				alert(res.errmsg);
				return;
			}

			var modalInstance = $modal.open({
				template: require('../../51weshop/views/settoppic.html'),
				controller: 'weshopsettoppic',
				url: '/weshopsettoppic',
				size : 'lg',
				resolve: {
					'product': function () {
						return lala;
					},
					str2date: function () {
						return str2date;
					},
					date2str: function () {
						return date2str;
					},
					
				}
			});

			modalInstance.opened.then(function () {// 模态窗口打开之后执行的函数  
				// $scope.load();
			});
			modalInstance.result.then(function (showResult) {
				$scope.load();
			}, function (reason) {
				$scope.load();
				// // click，点击取消，则会暑促cancel  
				// $log.info('Modal dismissed at: ' + new Date());
			});

		});

	};

	//配置海报
	$scope.setPoster = function(obj){

		console.log(obj);

		$resource('/api/ac/wc/productShareService/getshareattr', {}, {}).save({code : obj.sale_code}, function (res) {

			console.log(res);

			var pobj = {
				'code' : obj.sale_code,
				'title' : obj.name,
			};

			if (res.errcode === 0) {
				//没有数据
				if(res.data.xxx || res.data.xxx == 'yyy'){

				}
				//有数据
				else{
					pobj['id'] = obj.id;
					if(res.data.img){
						pobj['share_pic'] = res.data.img;
					}
					if(res.data.qrcode_size){
						pobj['qrcode_size'] = res.data.qrcode_size;
					}
					if(res.data.width){
						pobj['width'] = res.data.width;
					}
					if(res.data.height){
						pobj['height'] = res.data.height;
					}
					if(res.data.nickname_width){
						pobj['nickname_width'] = res.data.nickname_width;
					}
					if(res.data.nickname_height){
						pobj['nickname_height'] = res.data.nickname_height;
					}
					if(res.data.nickname_size){
						pobj['nickname_size'] = res.data.nickname_size;
					}
					if(res.data.nickname_color){
						pobj['nickname_color'] = res.data.nickname_color;
					}
					if(res.data.nickname_length){
						pobj['nickname_length'] = res.data.nickname_length;
					}
				}
			}
			else if(res.errcode === 9999) {
				//没数据

			}
			else {
				alert(res.errmsg);
				return;
			}

			var modalInstance = $modal.open({
				template: require('../../51weshop/views/setposter.html'),
				controller: 'weshopposter',
				url: '/weshopposter',
				size : 'lg',
				resolve: {
					'obj': function () {
						return pobj;
					},
					str2date: function () {
						return str2date;
					},
					date2str: function () {
						return date2str;
					}
				}
			});

			modalInstance.opened.then(function () {// 模态窗口打开之后执行的函数  
				// $scope.load();
			});
			modalInstance.result.then(function (showResult) {
				$scope.load();
			}, function (reason) {
				$scope.load();
				// // click，点击取消，则会暑促cancel  
				// $log.info('Modal dismissed at: ' + new Date());
			});


			// var para = $state.get('app.weshopposter');

			// var resolve = {
		 //          obj : function(){
		 //              return pobj;
		 //          },
		 //      };
		 //      angular.extend(para.resolve, resolve);

		 //      var modalInstance = $modal.open(para);
		 //      modalInstance.opened.then(function() {// 模态窗口打开之后执行的函数  
		 //      });  
		 //      modalInstance.result.then(function(result) {  
		 //          //load(); 

		 //      }, function(reason) {  
		 //          // click，点击取消，则会暑促cancel  
		 //          $log.info('Modal dismissed at: ' + new Date());  
		 //      }); 

		});

	}
	


};
