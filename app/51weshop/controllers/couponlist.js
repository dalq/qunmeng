module.exports = function($scope, $modal, $state, $timeout, $resource, $stateParams, str2date, date2str, $modal){

	var rangename = ['全部产品可用', '部分产品可用'];

	$scope.searchform = {};
	/* 分页
     * ========================================= */
	$scope.maxSize = 5;            //最多显示多少个按钮
	$scope.bigCurrentPage = 1;      //当前页码
	$scope.itemsPerPage = 10;         //每页显示几条

	$scope.load = function () {

		var para = {
			pageNo: $scope.bigCurrentPage,
			pageSize: $scope.itemsPerPage,
		};

		console.log($scope.searchform);

		para = angular.extend($scope.searchform, para);

		console.log($scope.searchform);

		$resource('/api/as/puc/couponproduct/findCouSLDlqList', {}, {}).save(para, function (res) {
			console.log(para);
			console.log(res);

			if (res.errcode !== 0) {
				alert("数据获取失败");
				return;
			}

			for(var i = 0; i < res.data.results.length; i++){
				var tmp = res.data.results[i];
				tmp['rangename'] = rangename[parseInt(tmp.range)];
			}

			$scope.objs = res.data.results;
			$scope.bigTotalItems = res.data.totalRecord;

		});

	};
	$scope.load();



	$scope.product = function(obj) {

		console.log(obj);

		var modalInstance = $modal.open({
			template: require('../views/pp.html'),
			controller: 'weshoppp',
			//url: '/memberlevel/:code',
			url: '/weshop/pp.html',
			size: 'lg',
			resolve: {
				'coupon': function () {
					return obj;
				},
				
			}
		});

		modalInstance.opened.then(function () {// 模态窗口打开之后执行的函数  
			// $scope.load();
		});
		modalInstance.result.then(function (showResult) {
			$scope.load();
		}, function (reason) {
			$scope.load();
			// // click，点击取消，则会暑促cancel  
			// $log.info('Modal dismissed at: ' + new Date());
		});

	}


	$scope.del = function(obj){

		if(!confirm('你确定要删除这个优惠券吗？不能恢复!!')){
        	return false;      
        }

		$resource('/api/ac/puc/couponProduct/updateCouProductDel', {}, {}).save({
			'pcode' : obj.product_code
		}, function (res) {
			console.log(res);

			if (res.errcode !== 0) {
				alert(res.errmsg);
				return;
			}

			$scope.load();

		});


	};


	$scope.edit = function(id){

		$state.go('app.weshopcoupon_edit', {'id' : id});

	};


};
