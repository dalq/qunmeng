module.exports = function($scope, formconfig, model){

	formconfig.start({
		'title' : '创建商家',
		'formtitle' : '商家基本信息',
		'elements' : model(),
		'save' : {
			'url' : '/api/ac/wc/productbussinessService/create',
			'to' : 'app.weshopbussinesslist',
			'para' : {
				'type' : 'M'
			}
		}
	}, $scope);

};