module.exports = function($scope, $resource, $modal, $modalInstance, obj, $state, $timeout){

	console.log(obj);

	$scope.ExcelName = '全部';
	$scope.main = '1';
	$scope.signup = '0';

	$scope.searchform = {};

    $scope.start_time = {
        //'date': date2str(new Date()),
        'opened': false
    };

    $scope.end_time = {
        //'date': date2str(new Date()),
        'opened': false
    };

    $scope.travel_time = {
        //'date' : date2str(new Date()),
        'opened': false
    }

    $scope.open = function ($event, item) {
        $event.preventDefault();
        $event.stopPropagation();
        item.opened = true;
    };


    /* 分页
       * ========================================= */
    $scope.maxSize = 5;            //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10;         //每页显示几条

    $scope.load = function () {

        var s = '';
        var e = '';
        var t = '';

        if($scope.start_time.date){
            if (typeof $scope.start_time.date === 'string') {
                s = $scope.start_time.date;
            } else {
                s = date2str($scope.start_time.date);
            }
        }
        if($scope.end_time.date){
            if (typeof $scope.end_time.date === 'string') {
                e = $scope.end_time.date;
            } else {
                e = date2str($scope.end_time.date);
            }
        }
        if($scope.travel_time.date){
            if (typeof $scope.travel_time.date === 'string') {
                t = $scope.travel_time.date;
            } else {
                t = date2str($scope.travel_time.date);
            }
        }


        $scope.ExcelName = s+'~'+e + '~' + t;

        var para = {
            pageNo: $scope.bigCurrentPage,
            pageSize: $scope.itemsPerPage,
            start_time : s == '' ? '' : s + " 00:00:00",
            end_time : e == '' ? '' : e + " 23:59:59",
            group : t,
            openid : obj.user_base
        };

        angular.extend(para, $scope.searchform);

        $resource('/api/as/wc/productorder/orderlist', {}, {}).save(para, function (res) {
            console.log(para);
            console.log(res);

            if (res.errcode !== 0) {
                alert(res.errmsg);
                return;
            }

            $scope.objs = res.data.results;
            $scope.bigTotalItems = res.data.totalRecord;

        });

        $resource('/api/as/wc/productorder/noorderlist', {}, {}).save(para, function (res) {
            console.log(para);
            console.log(res);

            if (res.errcode !== 0) {
                alert(res.errmsg);
                return;
            }

            $scope.exobjs = res.data;

        });

    };
    $scope.load();


    //打开模态框
    $scope.orderinfo = function(obj){
     
        var para = $state.get('app.weshoporderinfo');
        var resolve = {
            obj : function(){
                return obj;
            },
        };
        angular.extend(para.resolve, resolve);

        var modalInstance = $modal.open(para);
        modalInstance.opened.then(function() {// 模态窗口打开之后执行的函数  
            console.log('modal is opened');  
        });  
        modalInstance.result.then(function(result) {  

        }, function(reason) {  
            console.log(reason);// 点击空白区域，总会输出backdrop  
            // click，点击取消，则会暑促cancel  
            $log.info('Modal dismissed at: ' + new Date());  
        }); 
    }

    $scope.exportToExcel = function (tableId) {
        $timeout(function () {
            document.getElementById("dlink").href = Excel.tableToExcel(tableId, 'sheet1');
            //document.getElementById("dlink").download = "1122b.xls";//这里是关键所在,当点击之后,设置a标签的属性,这样就可以更改标签的标题了
            document.getElementById("dlink").click();
        }, 100); // trigger download
    }

    $scope.signupinfo = function(obj){
		$scope.signup = '1';
		$scope.main = '0';
    }




	$scope.cancel = function () {
        $modalInstance.close();
    }



};
