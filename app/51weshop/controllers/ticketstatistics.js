module.exports = function ($scope, $resource, $state) {

	$scope.searchform = {};

	$scope.load = function () {

		$resource('/api/as/wc/productorder/ticketstatisticslist', {}, {}).save({}, function (res) {
			console.log(res);
			if (res.errcode !== 0) {
				alert("数据获取失败");
				return;
			}

			$scope.objs = res.data;

		});

	};
	$scope.load();

    $scope.info = function(obj){
        
    	$state.go('app.statisticsinfo',{'city' : obj.city});
    }



};