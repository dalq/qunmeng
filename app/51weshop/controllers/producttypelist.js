module.exports = function($scope, $modal, $state, $resource){


	$scope.myKeyup = function() {
		console.log('aaaaaa');
	}


	function load(){
		$resource('/api/as/wc/producttype/producttypelist', {}, {}).save({}, function (res) {
			console.log(res);

			if (res.errcode !== 0) {
				alert("数据获取失败");
				return;
			}

			$scope.objs = res.data;

		});
	}
	load();

	

	$scope.create = function(){

      var para = $state.get('app.weshopproducttype');
      var resolve = {
          obj : function(){
              return {};
          },
      };
      angular.extend(para.resolve, resolve);

      var modalInstance = $modal.open(para);
      modalInstance.opened.then(function() {// 模态窗口打开之后执行的函数  
          console.log('modal is opened');  
      });  
      modalInstance.result.then(function(result) {  
          load(); 

      }, function(reason) {  
          console.log(reason);// 点击空白区域，总会输出backdrop  
          // click，点击取消，则会暑促cancel  
          $log.info('Modal dismissed at: ' + new Date());  
      }); 
    }

    $scope.edit = function(obj){
      console.log(obj);
        
    	var para = $state.get('app.weshopproducttype');
      var resolve = {
          obj : function(){
              return obj;
          },
      };
      angular.extend(para.resolve, resolve);

      var modalInstance = $modal.open(para);
      modalInstance.opened.then(function() {// 模态窗口打开之后执行的函数  
          console.log('modal is opened');  
      });  
      modalInstance.result.then(function(result) {  
          load(); 

      }, function(reason) {  
          console.log(reason);// 点击空白区域，总会输出backdrop  
          // click，点击取消，则会暑促cancel  
          $log.info('Modal dismissed at: ' + new Date());  
      }); 
    }

    $scope.asort = function(id, asort){

    	$resource('/api/as/wc/producttype/create', {}, {}).save({'id' : id, 'asort' : asort}, function(res){
    		if (res.errcode !== 0) {
				alert("数据获取失败");
				return;
			}

			load();
    	})
    }

    $scope.del = function(obj){

      if(!confirm("您确认要删除吗？")) {
          return;
      }

  		$resource('/api/as/wc/producttype/create', {}, {}).save({'id' : obj.id, 'del_flg' : '1'}, function(res){
  			console.log(res);
        if (res.errcode !== 0) {
  				alert("数据获取失败");
  				return;
  			}

  			load();

      })


    }

};
