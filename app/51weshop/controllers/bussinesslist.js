module.exports = function($scope, $resource, $state){

	console.log('bussinesslist');


	$scope.myKeyup = function() {
		console.log('aaaaaa');
	}

	$scope.searchform = {};
	/* 分页
     * ========================================= */
	$scope.maxSize = 5;            //最多显示多少个按钮
	$scope.bigCurrentPage = 1;      //当前页码
	$scope.itemsPerPage = 10;         //每页显示几条

	$scope.load = function () {

		var para = {
			pageNo: $scope.bigCurrentPage,
			pageSize: $scope.itemsPerPage,
		};

		para = angular.extend($scope.searchform, para);

		$resource('/api/as/wc/productbussiness/bussinesslist', {}, {}).save(para, function (res) {
			console.log(para);
			console.log(res);

			if (res.errcode !== 0) {
				alert("数据获取失败");
				return;
			}

			$scope.objs = res.data.results;
			$scope.bigTotalItems = res.data.totalRecord;

		});

	};
	$scope.load();


	$scope.edit = function(obj){

		$state.go('app.weshopbussiness_edit', {'id' : obj.id});

	}


	$scope.asort = function(obj, asort){

		console.log(obj);
		console.log(asort);

		$resource('/api/ac/wc/productbussinessService/create', {}, {}).save(
			{'id' : obj.id, 'asort' : asort}, function (res) {
			console.log(res);

			if (res.errcode !== 0) {
				alert("数据获取失败");
				return;
			}

			$scope.load();

		});
	}


	// $scope.info = function(){

	// 	$state.go('app.weshopbussiness_info', {'id' : obj.id});

	// }
	

	$scope.del = function(obj){

		if(!confirm('你确定要删除该商家吗？')){
        	return false;      
        }

		$resource('/api/ac/wc/productbussinessService/create', {}, {}).save(
			{'id' : obj.id, 'del_flg' : '1'}, function (res) {
			console.log(res);

			if (res.errcode !== 0) {
				alert(res.errmsg);
				return;
			}

			$scope.load();

		});

	};
	

	

};