module.exports = function($scope, formconfig, model, obj, $modalInstance, $resource){


	console.log(model());


	formconfig.start({
		'title' : '栏目详情',
		'formtitle' : '栏目基本信息',
		'elements' : model(),
		'info' : {
			'obj' : {
				'data' : obj
			}
		},
		'save' : function(item){
			item['id'] = obj.id;
			console.log('item');
			console.log(item);
			$resource('/api/as/wc/configurecolumn/create', {}, {}).save(item, function (res) {
	          console.log(res);

	          if (res.errcode !== 0) {
	            alert("数据获取失败");
	            return;
	          }

	          $modalInstance.close();

	        });
		}
	}, $scope);

	$scope.form = formconfig;

	$scope.cancel = function(){

		$modalInstance.close();

	}

};
