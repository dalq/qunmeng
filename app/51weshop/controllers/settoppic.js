module.exports = function($scope, $modal, $modalInstance, $state, product, $resource, str2date, date2str){

	console.log(product);

	//推广图片
	$scope.imagearr = [];

	//推广文字数组
	$scope.textarr = [];

	//推广文字
	$scope.text = '';

	//预定须知
	$scope.bookingnotes = product.bookingnotes || '';

	//商品详情
	$scope.detailarr = [];

	//详情类型：1，文字，2，图片
	$scope.detailType = '1';
	//详情文字
	$scope.detailTxt = '';


	//限时购买
	$scope.flash_state = product.flash_state || '0';	//0，不启用；1，启用
	$scope.flashsale = {};
    $scope.flashsale.start = {};
    $scope.flashsale.start.date = {
		'lable': date2str(new Date()),
		'opened': false
	}
    $scope.flashsale.start.h = product.start_h || '00';
    $scope.flashsale.start.m = product.start_m || '00';

    $scope.flashsale.end = {};
    $scope.flashsale.end.date = {
		'lable': date2str(new Date()),
		'opened': false
	}
    $scope.flashsale.end.h = product.end_h || '23';
    $scope.flashsale.end.m = product.end_m || '59';
	$scope.dateOpen = function ($event, item) {
		$event.preventDefault();
		$event.stopPropagation();
		item.opened = true;
	};
	$scope.dateOpen1 = function ($event, item) {
		$event.preventDefault();
		$event.stopPropagation();
		item.opened = true;
	};



	//库存
	$scope.stock_type = product.stock_type || '0';	//0，不启用；1，启用
	$scope.current_stock_num = product.current_stock_num || '0';


	//购买限制
	$scope.order_num_state = product.order_num_state || '0';
	$scope.order_limit_num = product.order_limit_num || '0';
	$scope.order_limit_type = product.order_limit_type || '0';
	$scope.cardno_state = product.cardno_state || '0';

	$scope.auth_state = product.auth_state || '0';



	if(product.top_pic){
		$scope.imagearr = product.top_pic.split(',');
	}

	if(product.words){
		$scope.textarr = product.words.split(',');
	}

	if(product.detail){
		$scope.detailarr = angular.fromJson(product.detail);
	}

	if(product.start_time){
		$scope.flashsale.start.date.lable = date2str(str2date(product.start_time));
	}

	if(product.end_time){
		$scope.flashsale.end.date.lable = date2str(str2date(product.end_time));
	}


	//----------------- 多图 ------------------------------//
	$scope.setImage = function () {

		var para = $state.get('app.imageupload');
		//设置上传文件夹，以自己业务命名

		para.resolve.dir = function(){
            return 'weshop_toppic';
        };

        // para.resolve.xxx = function(){
        // 	return {
        // 		'queueLimit' : 1,
        // 	};
        // };

		console.log(para);

        var modalInstance = $modal.open(para);
        modalInstance.opened.then(function() {// 模态窗口打开之后执行的函数  
        });  
        modalInstance.result.then(function(result) {  

        	console.log(result);
        	for(var i = 0; i < result.length; i++){
        		$scope.imagearr.push(result[i]);
        	}

        }, function(reason) {  
            // click，点击取消，则会暑促cancel  
            $log.info('Modal dismissed at: ' + new Date());  
        }); 

	}
		
	$scope.imageDel = function(index){

		$scope.imagearr.splice(index, 1);
	};

	$scope.imageDown = function(index){
		$scope.imagearr[index + 1] = $scope.imagearr.splice(index, 1, $scope.imagearr[index + 1])[0];
	};

	$scope.imageUp = function(index){
		$scope.imagearr[index - 1] = $scope.imagearr.splice(index, 1, $scope.imagearr[index - 1])[0];
	};
	//----------------- 多图 ------------------------------//


	//---------- 推广文字 --------------------------//
	$scope.setText = function(){

		$scope.textarr.push($scope.text);
		$scope.text = '';

	}

	$scope.del_text = function(index){

		$scope.textarr.splice(index, 1);
	}

	$scope.textDown = function(index){
		$scope.textarr[index + 1] = $scope.textarr.splice(index, 1, $scope.textarr[index + 1])[0];
	}

	$scope.textUp = function(index){
		$scope.textarr[index - 1] = $scope.textarr.splice(index, 1, $scope.textarr[index - 1])[0];
	}
	//---------- 推广文字 --------------------------//



	//---------- 商品详情 --------------------------//
	$scope.setDetailImage = function () {

		var para = $state.get('app.imageupload');
		//设置上传文件夹，以自己业务命名
		para.resolve.dir = function(){
            return 'weshop_detail';
        };

        var modalInstance = $modal.open(para);
        modalInstance.opened.then(function() {// 模态窗口打开之后执行的函数  
        });  
        modalInstance.result.then(function(result) {  

        	console.log(result);
        	for(var i = 0; i < result.length; i++){
        		var obj = {
        			'type' : 'image',
        			'value' : result[i],
        		}
        		$scope.detailarr.push(obj);
        	}

        }, function(reason) {  
            // click，点击取消，则会暑促cancel  
            $log.info('Modal dismissed at: ' + new Date());  
        }); 

	}


	$scope.setDetail = function(txt){

		var obj = {
			'type' : 'text',
			'value' : txt,
		}

		$scope.detailarr.push(obj);
		console.log($scope.detailarr);
	};


	$scope.detailDel = function(index){
		$scope.detailarr.splice(index, 1);

	};

	// //上移动一天,index=3  [1,2,3,4,5]=>[1,2,4,3,5]
	// function arrup(arr, index){
	// 	arr[index - 1] = arr.splice(index, 1, arr[index - 1])[0];
	// }
	// //下移动一天,index=3  [1,2,3,4,5]=>[1,2,3,5,4]
	// function arrdown(arr, index){
	// 	arr[index + 1] = arr.splice(index, 1, arr[index + 1])[0];
	// }

	$scope.detailDown = function(index){

		$scope.detailarr[index + 1] = $scope.detailarr.splice(index, 1, $scope.detailarr[index + 1])[0];
	};

	$scope.detailUp = function(index){

		$scope.detailarr[index - 1] = $scope.detailarr.splice(index, 1, $scope.detailarr[index - 1])[0];
	};
	//---------- 商品详情 --------------------------//




	//---------- 限时购买 --------------------------//
	
	//---------- 限时购买 --------------------------//



	$scope.gogo = function(){

		var image_txt = '';
		for(var i = 0, j = $scope.imagearr.length; i < j; i++){
			image_txt += $scope.imagearr[i] + (i == j - 1 ? '' : ',');
		}

		var text_txt = '';
		for(var i = 0, j = $scope.textarr.length; i < j; i++){
			text_txt += $scope.textarr[i] + (i == j - 1 ? '' : ',');
		}


		if (typeof $scope.flashsale.start.date.lable !== 'string') {
			$scope.flashsale.start.date.lable = date2str($scope.flashsale.start.date.lable);
		}

		if (typeof $scope.flashsale.end.date.lable !== 'string') {
			$scope.flashsale.end.date.lable = date2str($scope.flashsale.end.date.lable);
		}



		var para = {
			'code' : product.code,
			'title' : product.title,
			'top_pic' : image_txt,
			'words' : text_txt,
			'bookingnotes' : $scope.bookingnotes,
			'detail' : angular.toJson($scope.detailarr),
			'flash_state' : $scope.flash_state,
			'start_time' : $scope.flashsale.start.date.lable + 
						   ' ' + $scope.flashsale.start.h +
						   ':' + $scope.flashsale.start.m +
						   ':00',
			'end_time' :   $scope.flashsale.end.date.lable + 
						   ' ' + $scope.flashsale.end.h +
						   ':' + $scope.flashsale.end.m +
						   ':00',
			'stock_type' : $scope.stock_type,
			'current_stock_num' : $scope.current_stock_num,
			'order_num_state' : $scope.order_num_state,
			'order_limit_num' : $scope.order_limit_num,
			'order_limit_type' : $scope.order_limit_type,
			'cardno_state' : $scope.cardno_state,
			'auth_state' : $scope.auth_state,
		}

		console.log(para);

		$resource('/api/as/wc/productshare/createproductshare', {}, {}).save(para, function (res) {
			console.log(res);

			if (res.errcode !== 0) {
				alert("数据获取失败");
				return;
			}

			$modalInstance.close();

		});

	}


	$scope.cancel = function () {
        $modalInstance.close();
    }

};
