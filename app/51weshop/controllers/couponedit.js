module.exports = function($scope, formconfig, $stateParams, model){

	var id = $stateParams.id;

	formconfig.start({
		'title' : '编辑优惠券',
		'formtitle' : '优惠券基本信息',
		'elements' : model(),
		

		'info' : {
			'url' : '/api/as/puc/couponproduct/productinfo',
			'para' : {'id' : id}
		},
		'save' : {
			'url' : '/api/ac/puc/couponProduct/createConDlq',
			'to' : 'app.weshopcouponlist',
			'para' : {'id' : id}
		}
		
	}, $scope);

};