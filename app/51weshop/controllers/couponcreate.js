module.exports = function($scope, formconfig, model){

	formconfig.start({
		'title' : '创建优惠券',
		'formtitle' : '优惠券基本信息',
		'elements' : model(),
		'save' : {
			'url' : '/api/ac/puc/couponProduct/createConDlq',
			'to' : 'app.weshopcouponlist',
		}
	}, $scope);

};