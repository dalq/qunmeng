module.exports = function($scope, formconfig, model, obj, $modalInstance, $resource){

	if(angular.isUndefined(obj.type)) obj['type'] = '0';

	formconfig.start({
		'title' : '图片详情',
		'formtitle' : '图片基本信息',
		'elements' : model(),
		'info' : {
			'obj' : {
				'data' : obj
			}
		},
		'save' : function(item){
			item['id'] = obj.id;
			$resource('/api/as/wc/ad/create', {}, {}).save(item, function (res) {
	          console.log(res);

	          if (res.errcode !== 0) {
	            alert("数据获取失败");
	            return;
	          }

	          $modalInstance.close();

	        });
		}
	}, $scope);

	$scope.form = formconfig;

	$scope.cancel = function(){

		$modalInstance.close();

	}

};