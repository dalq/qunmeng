module.exports = function($scope, formconfig, $stateParams, model){

	var id = $stateParams.id;

	formconfig.start({
		'title' : '商家详情',
		'formtitle' : '商家基本信息',
		'elements' : model(),
		'info' : {
			'url' : '/api/as/wc/productbussiness/bussinessinfo',
			'para' : {'id' : id}
		},
	}, $scope);

	$scope.form = formconfig;

};