module.exports = function($scope, $modal, $modalInstance, $q, $http, level, $resource){

	console.log('lalalala');

	$scope.level = level;

	console.log(level);

	$scope.maxSize = 5;            //最多显示多少个按钮
	$scope.bigCurrentPage = 1;      //当前页码
	$scope.itemsPerPage = 10;         //每页显示几条

	$scope.load = function(){

		var beforedata = {
			//产品
			'productlist':
			$http({
				'method': 'GET',
				'url': '/api/as/wc/product/productlist',
				'params' : {
					'sale_category' : 'W10', 
					'pageNo': $scope.bigCurrentPage,
					'pageSize': $scope.itemsPerPage,
				},
			}),
			//产品佣金
			'productprofitlist':
			$http({
				'method': 'GET',
				'url': '/api/as/wc/productprofit/productprofitlist',
				'params': { 'level_code': level.code },
			}),
			
		};


		$q.all(beforedata).then(function (res) {

			console.log(res);

			//级别列表
			if (res.productlist.data.errcode === 0) {
			} else {
				alert('/api/as/wc/product/productlist' + res.productlist.data.errmsg);
				return;
			}

			//佣金列表
			if (res.productprofitlist.data.errcode === 0) {
			} else {
				alert('/api/as/wc/productprofit/productprofitlist' + res.productprofitlist.data.errmsg);
				return;
			}

			console.log(res.productprofitlist.data.data);

			var productarr = res.productlist.data.data.results;
			var commissionarr = res.productprofitlist.data.data;

			for(var i = 0; i < productarr.length; i++){
				var temp = productarr[i];
				temp['profit'] = 0;
				temp['profit1'] = 0;
				temp['profit2'] = 0;
				temp['state'] = 0;
				temp['savestate'] = false;
				temp['optstate'] = false;

				for(var j = 0; j < commissionarr.length; j++){
					var commission = commissionarr[j];
					if(temp.code == commission.product_code){
						temp['profit'] = commission.profit;
						temp['profit1'] = commission.profit1;
						temp['profit2'] = commission.profit2;
						temp['state'] = commission.state;
						temp['commission_id'] = commission.id;
						break;
					}
				}
			}

			$scope.objs = productarr;
            $scope.bigTotalItems = res.productlist.data.data.totalRecord;
            console.log(res.productlist.data.data.totalRecord);

		});

	}
	$scope.load();


	$scope.opt = function(obj){
		console.log(obj);

		var para = {
			'level_code' : level.code,
			'code' : obj.code,
			'profit' : obj.profit,
			'profit1' : obj.profit1,
			'profit2' : obj.profit2,
			'state' : obj.state == '1' ? '0' : '1',
		};

		if(angular.isDefined(obj.commission_id)){
			para['id'] = obj.commission_id;
		}

		obj.optstate = true;
		obj.savestate = true;

		$resource('/api/ac/wc/productService/createProductProfit', {}, {}).save(para, function (res) {
			console.log(para);
			console.log(res);

			obj.optstate = false;
			obj.savestate = false;

			if (res.errcode !== 0) {
				alert("数据获取失败");
				return;
			}

			$scope.load();

		});


	}

	$scope.save = function(obj){

		var para = {
			'level_code' : level.code,
			'code' : obj.code,
			'profit' : obj.profit,
			'profit1' : obj.profit1,
			'profit2' : obj.profit2,
			'state' : '1',
		}

		if(angular.isDefined(obj.commission_id)){
			para['id'] = obj.commission_id;
		}

		obj.optstate = true;
		obj.savestate = true;

		$resource('/api/ac/wc/productService/createProductProfit', {}, {}).save(para, function (res) {
			console.log(para);
			console.log(res);

			obj.optstate = false;
			obj.savestate = false;

			if (res.errcode !== 0) {
				alert(res.errmsg);
				return;
			}

			$scope.load();

		});

	}


	$scope.cancel = function () {
        $modalInstance.close();
    }


};
