


module.exports = function($scope, $modal, $state, $resource, $timeout, Excel, str2date, date2str, $http){

	$scope.ExcelName = '全部';
	$scope.main = '1';
	$scope.signup = '0';

    $scope.ticketstate_arr=[
        {
        'label':'查询全部',
        'value':'',
       },
       {
        'label':'未出票',
        'value':'0',
       }, 
       {
        'label':'出票中',
        'value':'1',
       },
        {
        'label':'出票完成',
        'value':'2',
       },
       //  {
       //  'label':'无需出票',
       //  'value':'8',
       // },
        {
        'label':'出票失败',
        'value':'9',
       },
   ];


   $scope.usedstatearr = [
        {
        'label':'查询全部',
        'value':'',
       },
       {
        'label':'未使用',
        'value':'1',
       }, 
       {
        'label':'已使用',
        'value':'6',
       },
       {
        'label':'全部使用',
        'value':'2',
       },
       {
        'label':'部分使用',
        'value':'3',
       },
       {
        'label':'已退款',
        'value':'7',
       },
       {
        'label':'全部退款',
        'value':'4',
       },
       {
        'label':'部分退款',
        'value':'5',
       },
       
   ];



    $scope.seller_remark_flag = 0;

    console.log('orderlist');


    $scope.myKeyup = function() {
      console.log('aaaaaa');
    }


    $scope.searchform = {
        'ticket_state' : '2',
        'usedstate' : '',
    };

    $scope.start_time = {
        'date': date2str(new Date()),
        'opened': false
    };

    $scope.end_time = {
        'date': date2str(new Date()),
        'opened': false
    };

    $scope.travel_time = {
        //'date' : date2str(new Date()),
        'opened': false
    }

    $scope.open = function ($event, item) {
        $event.preventDefault();
        $event.stopPropagation();
        item.opened = true;
    };


    /* 分页
       * ========================================= */
    $scope.maxSize = 5;            //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10;         //每页显示几条

    $scope.load = function () {

        var s = '';
        var e = '';
        var t = '';

        if($scope.start_time.date){
            if (typeof $scope.start_time.date === 'string') {
                s = $scope.start_time.date;
            } else {
                s = date2str($scope.start_time.date);
            }
        }
        if($scope.end_time.date){
            if (typeof $scope.end_time.date === 'string') {
                e = $scope.end_time.date;
            } else {
                e = date2str($scope.end_time.date);
            }
        }
        if($scope.travel_time.date){
            if (typeof $scope.travel_time.date === 'string') {
                t = $scope.travel_time.date;
            } else {
                t = date2str($scope.travel_time.date);
            }
        }


        $scope.ExcelName = s+'~'+e + '~' + t;

        var para = {
            pageNo: $scope.bigCurrentPage,
            pageSize: $scope.itemsPerPage,
            start_time : s == '' ? '' : s + " 00:00:00",
            end_time : e == '' ? '' : e + " 23:59:59",
            group : t,
        };

        angular.extend(para, $scope.searchform);

        $resource('/api/as/wc/productorder/orderlist', {}, {}).save(para, function (res) {
            console.log(para);
            console.log(res);

            if (res.errcode !== 0) {
                alert(res.errmsg);
                return;
            }

            $scope.objs = res.data.results;
            $scope.bigTotalItems = res.data.totalRecord;

        });

        // $resource('/api/as/wc/productorder/noorderlist', {}, {}).save(para, function (res) {
        //     console.log(para);
        //     console.log(res);

        //     if (res.errcode !== 0) {
        //         //alert(res.errmsg);
        //         return;
        //     }

        //     $scope.exobjs = res.data;

        // });

    };
    $scope.load();


    //打开模态框
    $scope.orderinfo = function(obj){
     
        var para = $state.get('app.weshoporderinfo');
        var resolve = {
            obj : function(){
                return obj;
            },
        };
        angular.extend(para.resolve, resolve);

        var modalInstance = $modal.open(para);
        modalInstance.opened.then(function() {// 模态窗口打开之后执行的函数  
            console.log('modal is opened');  
        });  
        modalInstance.result.then(function(result) {  

        }, function(reason) {  
            console.log(reason);// 点击空白区域，总会输出backdrop  
            // click，点击取消，则会暑促cancel  
            $log.info('Modal dismissed at: ' + new Date());  
        }); 
    }

    $scope.exportToExcel = function (tableId) {
        $resource('/api/as/wc/productorder/noorderlist', {}, {}).save({}, function (res) {
            console.log(res);

            if (res.errcode !== 0) {
                //alert(res.errmsg);
                return;
            }

            console.log(res.data);

            var labels = ['code', 'title', 'name', 'mobile', 'cardno'];
            var arr = [];
            for(var i = 0; i< res.data.length; i++){
                var tmp = res.data[i];
                var haha = [];
                for(var j = 0; j < labels.length; j++){
                    haha.push(tmp[labels[j]]);
                }
                arr.push(haha);
            }
            arr.splice(0, 0, ['订单编号', '订单名称', '游客姓名', '电活', '身份证']);
            ExportUtil.toExcel(arr,"订单");
        });


    }

    $scope.signupinfo = function(obj){
		$scope.signup = '1';
		$scope.main = '0';
    }



    //----------- 卖家备注 --------------------------//
    $scope.seller_remark_click = function(obj){

        $scope.seller_remark_flag = 1;
    };

    $scope.seller_remark_save = function(obj){
        $resource('/api/as/wc/productorder/update', {}, {}).save({
            'id' : obj.id,
            'seller_remark' : obj.seller_remark,
        }, function (res) {
            console.log(res);
            if (res.errcode !== 0) {
                alert(res.errmsg);
                return;
            }
            $scope.seller_remark_flag = 0;
            $scope.load();
        });
    };

    $scope.seller_remark_cancel = function(obj){

        $scope.seller_remark_flag = 0;
    };
    //----------- 卖家备注 --------------------------//


    //----------- 重发短信 --------------------------//
    
    $scope.resend = function(obj){
        $resource('/api/as/tc/ticketorder/resend', {}, {}).save({
            'code' : obj.ticket_order_code,
        }, function (res) {
            console.log(res);
            if (res.errcode == 1105){
                alert('短信正在发送中。。。。');
            }
            else if(res.errcode == 0){
                alert('发送成功');
            }
            else {
                alert(res.errmsg);
                return;
            }
            
        });
    };
    //----------- 重发短信 --------------------------//


};