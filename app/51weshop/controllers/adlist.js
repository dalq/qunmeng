module.exports = function($scope, $modal, $state, $resource){

    console.log('asdsadadssas');

    $scope.searchform = {};
    /* 分页
       * ========================================= */
    $scope.maxSize = 5;            //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10;         //每页显示几条

    $scope.load = function () {

      var para = {
        pageNo: $scope.bigCurrentPage,
        pageSize: $scope.itemsPerPage,
      };

      console.log($scope.searchform);

      para = angular.extend($scope.searchform, para);

      console.log($scope.searchform);

      $resource('/api/as/wc/ad/adlist', {}, {}).save(para, function (res) {
        console.log(para);
        console.log(res);

        if (res.errcode !== 0) {
          alert("数据获取失败");
          return;
        }

        $scope.objs = res.data.results;
        $scope.bigTotalItems = res.data.totalRecord;

      });

    };
    $scope.load();


    $scope.create = function(){
      var para = $state.get('app.weshopad');
      var resolve = {
          obj : function(){
              return {};
          },
      };
      angular.extend(para.resolve, resolve);

      var modalInstance = $modal.open(para);
      modalInstance.opened.then(function() {// 模态窗口打开之后执行的函数  
          console.log('modal is opened');  
      });  
      modalInstance.result.then(function(result) {  
          $scope.load(); 

      }, function(reason) {  
          console.log(reason);// 点击空白区域，总会输出backdrop  
          // click，点击取消，则会暑促cancel  
          $log.info('Modal dismissed at: ' + new Date());  
      }); 
    }



    $scope.edit = function(obj){
      var para = $state.get('app.weshopad');
      var resolve = {
          obj : function(){
              return obj;
          }
      };
      angular.extend(para.resolve, resolve);

      var modalInstance = $modal.open(para);
      modalInstance.opened.then(function() {// 模态窗口打开之后执行的函数  
          console.log('modal is opened');  
      });  
      modalInstance.result.then(function(result) {  
          $scope.load();

      }, function(reason) {  
          console.log(reason);// 点击空白区域，总会输出backdrop  
          // click，点击取消，则会暑促cancel  
          $log.info('Modal dismissed at: ' + new Date());  
      }); 
    }



    $scope.asort = function(obj){
        var para = {'id' : obj.id, 'asort' : obj.asort};
        updateAd(para);
    }

    $scope.delete = function(obj){
        if(!confirm("您确认要删除吗？")) {
            return;
        }
        var para = {'id' : obj.id, 'del_flg' : '1'};
        updateAd(para);
    }

    function updateAd(para) {

        $resource('/api/as/wc/ad/create', {}, {}).save(para, function (res) {
          console.log(para);
          console.log(res);

          if (res.errcode !== 0) {
            alert("数据获取失败");
            return;
          }

          $scope.load();

        });
    }

};
