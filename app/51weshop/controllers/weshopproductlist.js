module.exports = function($scope, $modal, $state, $timeout, $resource, $stateParams, str2date, date2str){

	var state = $stateParams.state;

	var statename = ['', '上架', '下架'];

	//var product_typename = ['自营', '分销'];

	var auth_statename = ['不需要', '需要'];

	$scope.myKeyup = function() {
		console.log('aaaaaa');
	}

	$resource('/api/as/wc/producttype/producttypelist', {}, {}).save({}, function (res) {
		console.log(res);
        if(res.errcode === 0)
        {
        	$scope.typearr = res.data;
        	$scope.typearr.unshift({ type_name : '未设置类别', type_code: '0' });
        }
        else
        {
            alert(res.errmsg);
        }
	});

	$resource('/api/as/wc/configurecolumn/columnlist?state=1', {}, {}).save({}, function (res) {
		console.log(res);
        if(res.errcode === 0)
        {
        	$scope.columnarr = res.data;
        	$scope.columnarr.unshift({ column_name : '未设置栏目', column_code: '0' });
        }
        else
        {
            alert(res.errmsg);
        }
	});


	$scope.setType = function(obj){

		var para = {'id' : obj.id, 'type_code' : obj.type_code}

		updateProduct(para);
	};

	$scope.setColumn = function(obj){

		var para = {'id' : obj.id, 'column_code' : obj.column_code}

		updateProduct(para);
	};


	



	$scope.searchform = {};
	/* 分页
     * ========================================= */
	$scope.maxSize = 5;            //最多显示多少个按钮
	$scope.bigCurrentPage = 1;      //当前页码
	$scope.itemsPerPage = 10;         //每页显示几条

	$scope.load = function () {

		var para = {
			pageNo: $scope.bigCurrentPage,
			pageSize: $scope.itemsPerPage,
			'mystate' : state,
		};

		console.log($scope.searchform);

		para = angular.extend($scope.searchform, para);

		console.log($scope.searchform);

		$resource('/api/as/wc/product/productlist', {}, {}).save(para, function (res) {
			console.log(para);
			console.log(res);

			if (res.errcode !== 0) {
				alert("数据获取失败");
				return;
			}

			var objs = res.data.results;

			for(var i = 0; i < objs.length; i++){
				var tmp = objs[i];
				tmp['state_name'] = statename[parseInt(tmp.state)];
				//tmp['product_typename'] = product_typename[[parseInt(tmp.product_type)]];
				tmp['auth_statename'] = auth_statename[[parseInt(tmp.auth_state)]];

				if((tmp.product_type == '1') 
				|| (tmp.product_type == '0' && tmp.myproduct == '1') ){
					tmp['product_typename'] = '分销';
				}else{
					tmp['product_typename'] = '自营';
				}
			}

			$scope.objs = objs;
			$scope.bigTotalItems = res.data.totalRecord;

		});

	};
	$scope.load();


	$scope.commission = function(obj) {

		var modalInstance = $modal.open({
			template: require('../../50bussmember/views/commission.html'),
			controller: 'memberlevelcommission',
			url: '/memberlevel/:code',
			size: 'lg',
			resolve: {
				'product': function () {
					return obj;
				},
				
			}
		});

		modalInstance.opened.then(function () {// 模态窗口打开之后执行的函数  
			// $scope.load();
		});
		modalInstance.result.then(function (showResult) {
			$scope.load();
		}, function (reason) {
			$scope.load();
			// // click，点击取消，则会暑促cancel  
			// $log.info('Modal dismissed at: ' + new Date());
		});

	}

	$scope.setprice = function(obj) {
		var para = {
			'product_type' : '1',
		}
		var modalInstance = $modal.open({
			template: require('../../51weshop/views/setprice.html'),
			controller: 'weshopsetprice',
			url: '/weshopsetprice',
			resolve: {
				'product': function () {
					return obj;
				},
				id :function(){
					return obj.id;
				}
			}
		});

	}

	$scope.up = function(obj){

		var para = {
			'id' : obj.id,
			'mystate' : '1',
		}

		$resource('/api/ac/wc/productService/updatesjstate', {}, {}).save(para, function (res) {
			console.log(res);

			if (res.errcode !== 0) {
				alert(res.errmsg);
				return;
			}

			$scope.load();

		});

	}

	$scope.down = function(obj){

		var para = {
			'id' : obj.id,
			'code' : obj.code,
			'mystate' : '2',
		}

		$resource('/api/ac/wc/productService/updatestate', {}, {}).save(para, function (res) {
			console.log(res);

			if (res.errcode !== 0) {
				alert("数据获取失败");
				return;
			}

			$scope.load();

		});

	}


	$scope.asort = function(obj, asort){

		var para = {'id' : obj.id, 'asort' : asort};
		updateProduct(para);
	}



	function updateProduct(para) {

		$resource('/api/as/wc/product/update', {}, {}).save(para, function (res) {
			console.log(res);

			if (res.errcode !== 0) {
				alert("数据获取失败");
				return;
			}

			$scope.load();

		});

	}

	$scope.release = function(obj) {
		$resource('/api/as/wc/product/updaterelease', {}, {}).save(obj, function (res) {
			console.log(res);

			if (res.errcode !== 0) {
				alert(res.errmsg);
				return;
			}
			alert("发布成功");

			$scope.load();

		});

	}


	$scope.setImage = function(obj){

		var modalInstance = $modal.open({
			template: require('../../51weshop/views/settoppic.html'),
			controller: 'weshopsettoppic',
			url: '/weshopsettoppic',
			size : 'lg',
			resolve: {
				'product': function () {
					return obj;
				}
				
			}
		});

		modalInstance.opened.then(function () {// 模态窗口打开之后执行的函数  
			// $scope.load();
		});
		modalInstance.result.then(function (showResult) {
			$scope.load();
		}, function (reason) {
			$scope.load();
			// // click，点击取消，则会暑促cancel  
			// $log.info('Modal dismissed at: ' + new Date());
		});

	};

	
	//配置海报
	$scope.setPoster = function(obj){

		console.log(obj);
        
    	var para = $state.get('app.weshopposter');
	      var resolve = {
	          obj : function(){
	              return obj;
	          },
	      };
	      angular.extend(para.resolve, resolve);

	      var modalInstance = $modal.open(para);
	      modalInstance.opened.then(function() {// 模态窗口打开之后执行的函数  
	          console.log('modal is opened');  
	      });  
	      modalInstance.result.then(function(result) {  
	          //load(); 

	      }, function(reason) {  
	          console.log(reason);// 点击空白区域，总会输出backdrop  
	          // click，点击取消，则会暑促cancel  
	          $log.info('Modal dismissed at: ' + new Date());  
	      }); 
	};

	$scope.config = function(obj){
        
    	var modalInstance = $modal.open({
          template: require('../views/productconfig.html'),
          controller: 'productconfig',
          size: 'xs',
          resolve: {
            obj : function(){
                return obj;
            }
          }
        });

        modalInstance.result.then(function () {
            $scope.load();
        }, function () {
            
        });
    }


    $scope.del = function(obj){

    	if(!confirm('你确定要删除这个产品吗？不能恢复!!')){
        	return false;      
        }

    	//$resource('/api/as/wc/product/update', {}, {}).save({
    	$resource('/api/ac/wc/productService/deleteProduct', {}, {}).save({
    		'id' : obj.id, 'del_flg' : '1'
    	}, function (res) {
			console.log(res);

			if (res.errcode !== 0) {
				alert(res.errmsg);
				return;
			}

			$scope.load();

		});


    };


    //发布到群盟产品列表
    $scope.putOut = function(obj){

    	$resource('/api/ac/wc/productService/createProductSaleApply', {}, {}).save({
    		'id' : obj.id
    	}, function (res) {
			console.log(res);

			if (res.errcode !== 0) {
				alert(res.errmsg);
				return;
			}

			alert('发布成功，等待系统审核');

		});

    }

};
