module.exports = function($scope, $modal, $state, $timeout, $resource, $stateParams, str2date, date2str, $modal){

	var type_label = ['', '收入', '提现'];

	$scope.typearr = [
		{'title' : '全部', 'code' : ''},
		{'title' : '收入', 'code' : '1'},
		{'title' : '提现', 'code' : '2'},
	];

	$scope.start_time = {
        //'date': date2str(new Date()),
        'opened': false
    };

    $scope.end_time = {
        //'date': date2str(new Date()),
        'opened': false
    };

    $scope.open = function ($event, item) {
        $event.preventDefault();
        $event.stopPropagation();
        item.opened = true;
    };

	$scope.searchform = {};
	/* 分页
     * ========================================= */
	$scope.maxSize = 5;            //最多显示多少个按钮
	$scope.bigCurrentPage = 1;      //当前页码
	$scope.itemsPerPage = 10;         //每页显示几条

	$scope.load = function () {

		var s = '';
        var e = '';

        if($scope.start_time.date){
            if (typeof $scope.start_time.date === 'string') {
                s = $scope.start_time.date;
            } else {
                s = date2str($scope.start_time.date);
            }
        }
        if($scope.end_time.date){
            if (typeof $scope.end_time.date === 'string') {
                e = $scope.end_time.date;
            } else {
                e = date2str($scope.end_time.date);
            }
        }
       


		var para = {
			pageNo: $scope.bigCurrentPage,
			pageSize: $scope.itemsPerPage,
			start_time : s == '' ? '' : s + " 00:00:00",
			end_time : e == '' ? '' : e + " 23:59:59",
		};

		console.log($scope.searchform);

		para = angular.extend($scope.searchform, para);

		console.log($scope.searchform);

		$resource('/api/ac/wc/accountService/balanceInfo', {}, {}).save(para, function (res) {
			console.log(para);
			console.log(res);

			for(var i = 0; i < res.data.results.length; i++){
				var tmp = res.data.results[i];
				tmp['type_label'] = type_label[parseInt(tmp.type)];
			}

			$scope.objs = res.data.results;
			$scope.bigTotalItems = res.data.totalRecord;

		});

	};
	$scope.load();


	$scope.info = function(code){

		$resource('/api/as/wc/productorder/orderinfo', {}, {}).save({'code' : code}, function (res) {
			console.log(res);

			if(res.errcode != '0'){
				alert(res.errmsg);
				return;
			}

			var ooo = res.data;

			ooo.guide_price *= 0.01;
			ooo.pay_fee *= 0.01;
			ooo.profit *= 0.01;
			ooo.profit1 *= 0.01;
			ooo.profit_wb *= 0.01;
			ooo.voucher_price *= 0.01;

			var para = $state.get('app.weshoporderinfo');
	        var resolve = {
	            obj : function(){
	                return ooo;
	            },
	        };
	        angular.extend(para.resolve, resolve);

	        var modalInstance = $modal.open(para);
	        modalInstance.opened.then(function() {// 模态窗口打开之后执行的函数  
	            console.log('modal is opened');  
	        });  
	        modalInstance.result.then(function(result) {  

	        }, function(reason) {  
	            console.log(reason);// 点击空白区域，总会输出backdrop  
	            // click，点击取消，则会暑促cancel  
	            $log.info('Modal dismissed at: ' + new Date());  
	        }); 
			

		});



	};



};
