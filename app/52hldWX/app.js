var App = angular.module('hldWX', []);

App.config(require('./router'));
App.factory('hldWXservice', require('./service'));

// 景区列表
App.controller('touristlist',require('./controllers/touristlist')); 
// 添加景区
App.controller('addTourist',require('./controllers/addTourist')); 
// 类别tab
App.controller('alltype',require('./controllers/alltype')); 
// 菜品列表
App.controller('dishlist',require('./controllers/dishlist')); 
App.controller('adddish',require('./controllers/adddish')); 
// 创建场所
App.controller('addPlace_view',require('./controllers/addPlace_view'));  
App.controller('addplacetype',require('./controllers/addplacetype')); 
//评价管理
App.controller('evaluate',require('./controllers/evaluate')); 
//投诉管理
App.controller('complaintList',require('./controllers/complaintList')); 
//评价管理详情
App.controller('evaluateinfo',require('./controllers/evaluateinfo')); 
//投诉管理详情
App.controller('complaintinfo',require('./controllers/complaintinfo')); 
// 线路列表
App.controller('hldlinelist',require('./controllers/hldlinelist')); 
//添加线路产品
App.controller('addhldline',require('./controllers/addhldline')); 
// 线路详情
App.controller('hldlineinfo',require('./controllers/hldlineinfo'));
// 游记列表
App.controller('travelnotelist',require('./controllers/travelnotelist'));
// 游记详情
App.controller('travelnoteinfo',require('./controllers/travelnoteinfo'));
// 驳回原因
App.controller('nopassreason',require('./controllers/nopassreason'));
// 场所列表关联产品
App.controller('relationlist',require('./controllers/relationlist'));
// 根据游记提到的内容选择酒店
App.controller('selecthotel',require('./controllers/selecthotel'));
// 评价驳回原因
App.controller('evaluateReject',require('./controllers/evaluateReject'));




// 渲染tab页数据
App.directive('foodload',require('./directives/foodload'));  
App.directive('clothingload',require('./directives/clothingload'));  
App.directive('liveload',require('./directives/liveload'));  
App.directive('travelload',require('./directives/travelload'));  // placeview_baseinfo

App.directive('placeviewbaseinfo',require('./directives/placeview_baseinfo'));  // placeview_baseinfo
App.directive('clothinginfo',require('./directives/clothinginfo'));  // placeview_baseinfo
App.directive('liveinginfo',require('./directives/liveinginfo'));  // placeview_baseinfo
App.directive('foodinfo',require('./directives/foodinfo'));  // placeview_baseinfo
App.directive('travelinginfo',require('./directives/travelinginfo'));  // placeview_baseinfo






module.exports = App;
