module.exports = function($scope, $state, $stateParams, $modalInstance,$resource,$modal,toaster, id){
    $scope.info = {
        'id' : id,
        'reasons_rejection' : ''
    }
    $scope.ok = function (){
        $resource('/api/ac/tmc/tourismCommentService/updateNotAdopt', {}, {})
        .save($scope.info, function (res) {
            if (res.errcode !== 0) {
                toaster.error({ title: "提示", body: res.errmsg });
                return;
            }
            toaster.success({ title: "提示", body: "驳回成功!" });
            $modalInstance.close();
        });
    }
    $scope.cancel = function () {
      $modalInstance.close();
    }
}  