module.exports = function($scope, $resource, $state, $modal, toaster){
    
        $scope.obj = {};
        $scope.tabname = { 'name': '添加类别', 'active': false };
        $scope.status = { 'active': true };
        $scope.statuscloth = { 'active': false };
        $scope.statuslive = { 'active': false };
        $scope.statustravel = { 'active' : false};
        $scope.type = 'M';

        // 类别列表
        // $scope.super_type_list = [{'id' : '1', 'name' : '葫芦岛'}, {'id' : '2', 'name' : '枫叶·食'}, {'id' : '2', 'name' : '灵紫·购'}, {'id' : '2', 'name' : '蔚蓝·游'}, {'id' : '2', 'name' : '郁金·住'}];
        
    
        init();
        //初始化
        function init(){
            // $scope.status.active = true;
            $("#treetable").hide();
            $resource('/api/ac/tmc/tourismPlaceTypeService/findInfoList', {}, {}).
            save({'type' : $scope.type}, function(res){
                if(res.errcode === 0){
                    // $scope.status.active = true;
                    $scope.area_list = res.data;
                    $scope.super_type_list = res.data;
                    console.log($scope.area_list);
                    console.log($scope.super_type_list);
                }else{
                    alert(res.errmsg);
                }
            });
        }
    
        //添加
        $scope.add = function(item){
            console.log(item);
            console.log('添加下级');
            $state.go('app.addplacetype',{'id' : item.id,'name' : item.name,'type':item.type, 'flag': 'create'});
        };
    
        //修改界面
        $scope.update = function(item){
            console.log(item);
            $state.go('app.addplacetype', {'id' : item.id,'name' : item.name,'type':item.type, 'flag': 'edit'});
            
            // $scope.tabname = { 'name': '修改类别', 'active': true };
            // $resource('/api/as/sc/area/getById', {}, {}).
            
            // save(item, function(res){
            //     if(res.errcode === 0){
            //         $scope.obj = res.data;
            //     }else{
            //         alert(err.errmsg);
            //     }
            // });
        };

        //添加场所
        $scope.addplace = function(id){
            var modalInstance = $modal.open({
                template: require('../views/adddish.html'),
                controller: 'adddish',
                size: 'lg',
                resolve: {
                    id: function () {
                        return id;
                    },
                    // DateDiff : function () {
                    //     return DateDiff;
                    // }
                    
                }
            });
            modalInstance.result.then(function (showResult) {	
                     $scope.init();
            });
        }
    
        //取消=重置
        // 枫叶食
        $scope.cancel = function(){
            $scope.status.active = true;
            $scope.tabname = { 'name': '添加类别', 'active': false };
            $scope.type = 'M';
            init();
        };
        // 灵子够
        $scope.cancelcloth = function(){
            $scope.statuscloth.active = true;
            $scope.status.active = false;
            $scope.tabname = { 'name': '添加类别', 'active': false };
            $scope.type = 'S';
            init();
        };
        // 蔚蓝游
        $scope.canceltravel = function(){
            $scope.statustravel.active = true;
            $scope.status.active = false;
            $scope.tabname = { 'name': '添加类别', 'active': false };
            $scope.type = 'J';
            init();
        };
        // 郁金住
        $scope.cancellive = function(){
            $scope.statuslive.active = true;
            $scope.status.active = false;
            $scope.tabname = { 'name': '添加类别', 'active': false };
            $scope.type = 'H';
            init();
            
            
        };
        
    
        //删除
        $scope.delete = function(id){
            console.log(id);
            if(confirm('删除将会连同下级一起删除,确认要删除该类别及下级类别吗？')==true){
                $resource('/api/ac/tmc/tourismPlaceTypeService/updateDel', {}, {}).
                save({'id' : id}, function(res){
                    if(res.errcode === 0){
                        init();
                    }else{
                        alert(res.errmsg);
                    }
                });
            }
        };

        $scope.trafficinfo = {
            'traffic_bus_info' : '',
            'traffic_taxi_info' : '',
            'traffic_car_rental_info' : '',
            'traffic_bicycle_sharing_info' : ''
        };
           
        // 低碳公交,便捷出租
        $scope.gettrafficinfo = function(){
            $resource('/api/ac/tmc/tourismPlaceTrafficService/findInfo', {}, {}).
            save({}, function(res){
                if(res.errcode === 0){
                    $scope.trafficinfo.traffic_bus_info = res.data.traffic_bus_info;
                    $scope.trafficinfo.traffic_taxi_info = res.data.traffic_taxi_info;
                    $scope.trafficinfo.traffic_car_rental_info = res.data.traffic_car_rental_info;
                    $scope.trafficinfo.traffic_bicycle_sharing_info = res.data.traffic_bicycle_sharing_info;
                }else{
                    alert(res.errmsg);
                }
            });
        }
        $scope.gettrafficinfo();

        $scope.busflag = true;
        $scope.editbus = function(){
            $scope.busflag = false;
        }

        $scope.taxiflag = true;
        $scope.edittaxi = function(){
            $scope.taxiflag = false;
        }

        $scope.rentalflag = true;
        $scope.editrental = function(){
            $scope.rentalflag = false;
        }

        $scope.bikeflag = true;
        $scope.editbike = function(){
            $scope.bikeflag = false;
        }
        $scope.savebus = function(){
            $resource('/api/ac/tmc/tourismPlaceTrafficService/createDepict', {}, {}).
            save({'traffic_bus_info' : $scope.trafficinfo.traffic_bus_info}, function(res){
                if(res.errcode === 0){
                    toaster.success({title : '', body : '修改成功'});
                    $scope.busflag = true;
                }else{
                    toaster.error({title : '', body : res.errmsg});
                    
                }
            });
        }
        $scope.savetaxi = function(){
            $resource('/api/ac/tmc/tourismPlaceTrafficService/createDepict', {}, {}).
            save({'traffic_taxi_info' : $scope.trafficinfo.traffic_taxi_info}, function(res){
                if(res.errcode === 0){
                    toaster.success({title : '', body : '修改成功'});
                    $scope.taxiflag = true;
                }else{
                    toaster.error({title : '', body : res.errmsg});
                }
            });
        }
        $scope.saverental = function(){
            $resource('/api/ac/tmc/tourismPlaceTrafficService/createDepict', {}, {}).
            save({'traffic_car_rental_info' : $scope.trafficinfo.traffic_car_rental_info}, function(res){
                if(res.errcode === 0){
                    toaster.success({title : '', body : '修改成功'});
                    $scope.rentalflag = true;
                }else{
                    toaster.error({title : '', body : res.errmsg});
                }
            });
        }
        $scope.savebike = function(){
            $resource('/api/ac/tmc/tourismPlaceTrafficService/createDepict', {}, {}).
            save({'traffic_bicycle_sharing_info' : $scope.trafficinfo.traffic_bicycle_sharing_info}, function(res){
                if(res.errcode === 0){
                    toaster.success({title : '', body : '修改成功'});
                    $scope.bikeflag = true;
                }else{
                    toaster.error({title : '', body : res.errmsg});
                }
            });
        }
       
        /* 分页
        * ========================================= */
        $scope.maxSize = 5;             //最多显示多少个按钮
        $scope.bigCurrentPage = 1;      //当前页码
        $scope.itemsPerPage = 10         //每页显示几条
        $scope.searchform = {};
        $scope.getlist = function (bigCurrentPage) {
            var para = {
                pageNo:$scope.bigCurrentPage, 
                pageSize:$scope.itemsPerPage,
                'type' : 'T'
            };
            para = angular.extend($scope.searchform,para);
            $resource('/api/ac/tmc/tourismPlaceService/findList', {}, {}).
            save(para,function (res) {
                if (res.errcode != 0) {
                    toaster.success({title:"",body:res.errmsg});
                    return;
                }
                console.log(res);
                $scope.objs = res.data.results;
                $scope.bigTotalItems = res.data.totalRecord;
                $scope.objs.forEach(function(element) {
                    element.isSelected = element.state=='1'?true:false;
                }, this);
            });   
        };
        $scope.getlist();
        $scope.createrental = function(){
            $state.go('app.addPlace_view');
        }
        $scope.edit = function(id, type, code){
            console.log('编辑自由租车');
            $state.go('app.addPlace_view', {'id' : id, 'type' : type, 'code' : code});
            
        }
        $scope.deleterental = function(id){
          if(confirm('确定删除该场所吗')){
              $resource('/api/ac/tmc/tourismPlaceService/updateDel', {}, {}).
              save({'id' : id},function (res) {
                  if (res.errcode != 0) {
                      toaster.error({title:"",body:res.errmsg});
                      return;
                  }
                  console.log(res);
                  $scope.getlist();
              }); 
          }
        }
        // 停用/禁用
        $scope.useOrNoUse = function(id, state){
          if(state == '0') {
            // 启用
            $resource('/api/ac/tmc/tourismPlaceService/updateStateUp', {}, {}).
            save({'id' : id, 'state' : 1},function (res) {
                if (res.errcode != 0) {
                    toaster.error({title:"",body:res.errmsg});
                    return;
                }
                toaster.success({title:"",body:"启用成功"});
                $scope.getlist();
            }); 
          } else { // 停用
            $resource('/api/ac/tmc/tourismPlaceService/updateStateDown', {}, {}).
            save({'id' : id, 'state' : 0},function (res) {
                if (res.errcode != 0) {
                    toaster.error({title:"",body:res.errmsg});
                    return;
                }
                toaster.success({title:"",body:"停用成功"});
                $scope.getlist();
            }); 
          }
        }
    
    };