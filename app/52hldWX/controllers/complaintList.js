module.exports = function($scope, $modal, $state, $timeout, $resource){
    
        $scope.start = {
            'date' : '',
            'opened' : false,
        };
    
        $scope.end = {
            'date' : '',
            'opened' : false,
        };
    
        $scope.open = function(obj) {
            obj.opened = true;
        }
    
    
        $scope.myKeyup = function() {
        }
    
        $scope.searchform = {};
        /* 分页
         * ========================================= */
        $scope.maxSize = 5;            //最多显示多少个按钮
        $scope.bigCurrentPage = 1;      //当前页码
        $scope.itemsPerPage = 10;         //每页显示几条
        $scope.form = {};
        $scope.load = function () {
        //  alert($scope.searchform.state);
            var para = {
                pageNo: $scope.bigCurrentPage,
                pageSize: $scope.itemsPerPage
            };
            para = angular.extend($scope.searchform, para);
            $resource('/api/ac/tmc/tourismComplaintService/findComplaintAdaminList', {}, {}).save(para, function (res) {
    
                if (res.errcode !== 0) {
                    alert(res.errmsg);
                    return;
                }
                console.log(res);
                $scope.objs = res.data.results;
                $scope.bigTotalItems = res.data.totalRecord;
                for(var i = 0;i<$scope.objs.length;i++){
                    if($scope.objs[i].place_type=='M'){
                        $scope.objs[i].place_type = '商户'
                    }
                    if($scope.objs[i].place_type=='J'){
                        $scope.objs[i].place_type = '景区'
                    }
                    if($scope.objs[i].place_type=='H'){
                        $scope.objs[i].place_type = '酒店'
                    }
                    if($scope.objs[i].place_type=='S'){
                        $scope.objs[i].place_type = '商场'
                    }
                    if($scope.objs[i].place_type=='T'){
                        $scope.objs[i].place_type = '交通'
                    }
                }
                //出行方式转换
                for(var i = 0;i<$scope.objs.length;i++){
                    if($scope.objs[i].travel_mode=='0'){
                        $scope.objs[i].travel_mode = '自由行'
                    }
                    if($scope.objs[i].travel_mode=='1'){
                        $scope.objs[i].travel_mode = '跟团游'
                    }
                    if($scope.objs[i].travel_mode=='2'){
                        $scope.objs[i].travel_mode = '走亲访友'
                    }
                    if($scope.objs[i].travel_mode=='3'){
                        $scope.objs[i].travel_mode = '出差'
                    }
                    if($scope.objs[i].travel_mode=='4'){
                        $scope.objs[i].travel_mode = '其他'
                    }
                }  
                //处理状态转换
                for(var i = 0;i<$scope.objs.length;i++){
                    if($scope.objs[i].state=='0'){
                        $scope.objs[i].state = '处理中'
                    }
                    if($scope.objs[i].state=='1'){
                        $scope.objs[i].state = '已处理'
                    }
                }                  
            });
    
        };
        $scope.load();
        //类型
        $scope.typearr = [
            {
                code:'',
                name:'未选择'
            },
            {
                code:'H',
                name:'酒店'
            },
            {
                code:'M',
                name:'商户'
            },
            {
                code:'J',
                name:'景区'
            },
            {
                code:'T',
                name:'交通'
            },
            {
                code:'S',
                name:'商场'
            },
            {
                code:'L',
                name:'线路'
            },
            {
                code:'A',
                name:'游记'
            }   
        ]
       //出游方式下拉框
        $scope.travelmode = [
            {
                code:'',
                name:'未选择'
            },
            {
                code:'0',
                name:'自由行'
            },
            {
                code:'1',
                name:'跟团游'
            },
            {
                code:'2',
                name:'走亲访友'
            },
            {
                code:'3',
                name:'出差'
            },
            {
                code:'4',
                name:'其他'
            }
        ]
       //处理状态下拉框
        $scope.statearr = [
            {
                code:'',
                name:'未选择'
            },
            {
                code:'0',
                name:'处理中'
            },
            {
                code:'1',
                name:'已处理'
            }
        ]
        $scope.sourcetypearr = [{'name':'游记', 'code':'T'}, {'name':'线路', 'code':'L'}];
        $scope.search = function(){
            var para = {
                pageNo: $scope.bigCurrentPage,
                pageSize: $scope.itemsPerPage,
                place_id:$scope.form.place_id,
                title:$scope.form.title,
                place_type:$scope.form.type
            };
    
            $resource('/api/ac/tmc/tourismComplaintService/findComplaintAdaminList', {}, {}).save(para, function (res) {
    
                if (res.errcode !== 0) {
                    alert(res.errmsg);
                    return;
                }
                console.log(res);
                $scope.objs = res.data.results;
                $scope.bigTotalItems = res.data.totalRecord;
                for(var i = 0;i<$scope.objs.length;i++){
                    if($scope.objs[i].place_type=='M'){
                        $scope.objs[i].place_type = '商户'
                    }
                    if($scope.objs[i].place_type=='J'){
                        $scope.objs[i].place_type = '景区'
                    }
                    if($scope.objs[i].place_type=='H'){
                        $scope.objs[i].place_type = '酒店'
                    }
                    if($scope.objs[i].place_type=='S'){
                        $scope.objs[i].place_type = '商场'
                    }
                    if($scope.objs[i].place_type=='T'){
                        $scope.objs[i].place_type = '交通'
                    }
                     if($scope.objs[i].travel_mode=='0'){
                        $scope.objs[i].travel_mode = '自由行'
                    }
                    if($scope.objs[i].travel_mode=='1'){
                        $scope.objs[i].travel_mode = '跟团游'
                    }
                    if($scope.objs[i].travel_mode=='2'){
                        $scope.objs[i].travel_mode = '走亲访友'
                    }
                    if($scope.objs[i].travel_mode=='3'){
                        $scope.objs[i].travel_mode = '出差'
                    }
                    if($scope.objs[i].travel_mode=='4'){
                        $scope.objs[i].travel_mode = '其他'
                    }
                    if($scope.objs[i].state=='0'){
                        $scope.objs[i].state = '处理中'
                    }
                    if($scope.objs[i].state=='1'){
                        $scope.objs[i].state = '已处理'
                    }
                }
                //出行方式转换
                // for(var i = 0;i<$scope.objs.length;i++){
                //     if($scope.objs[i].travel_mode=='0'){
                //         $scope.objs[i].travel_mode = '自由行'
                //     }
                //     if($scope.objs[i].travel_mode=='1'){
                //         $scope.objs[i].travel_mode = '跟团游'
                //     }
                //     if($scope.objs[i].travel_mode=='2'){
                //         $scope.objs[i].travel_mode = '走亲访友'
                //     }
                //     if($scope.objs[i].travel_mode=='3'){
                //         $scope.objs[i].travel_mode = '出差'
                //     }
                //     if($scope.objs[i].travel_mode=='4'){
                //         $scope.objs[i].travel_mode = '其他'
                //     }
                // }  
                //处理状态转换
                // for(var i = 0;i<$scope.objs.length;i++){
                //     if($scope.objs[i].state=='0'){
                //         $scope.objs[i].state = '处理中'
                //     }
                //     if($scope.objs[i].state=='1'){
                //         $scope.objs[i].state = '已处理'
                //     }
                // }                
            });
        }

        $scope.info = function(id,source_id){
            console.log(id);
            var modalInstance = $modal.open({
                template: require('../views/complaintinfo.html'),
                controller: 'complaintinfo',
                size: 'lg',
                resolve: {
                    id: function () {
                        return id;
                    },
                   source_id: function () {
                        return source_id;
                    }    
                }
            });
            modalInstance.result.then(function (showResult) {	
                     $scope.load();
            });
        }
    
    };
    