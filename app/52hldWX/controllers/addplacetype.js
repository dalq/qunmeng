module.exports = function($scope, $state, $stateParams, $resource,$modal,toaster){
    var id = $stateParams.id;
    var name = $stateParams.name;
    var type = $stateParams.type;
    var flags = $stateParams.flag;
    console.log(id, name, type, flags);
    $scope.flag = 0;
    $scope.obj = {};
    // 获取上级列表
    $scope.getParentIdList = function(id){
      $resource('/api/ac/tmc/tourismPlaceTypeService/findInfoList', {}, {}).
        save({}, function(res){
          if(res.errcode !== 0){
              toaster.error({title : "" , body: res.errmsg});
              return;
          }
          console.log(res);
          $scope.super_type_list = res.data;
          $scope.super_type_list.forEach(element => {
              if(id === element.id){
                $scope.parent_name = element.name;
              }
          });
      });
    }
    $scope.obj.parent_id = id;
    $scope.obj.type = type;
    if(flags === 'edit'){
        $resource('/api/ac/tmc/tourismPlaceTypeService/findInfoById', {}, {}).
        save({'id' : id}, function(res){
            if(res.errcode !== 0){
                toaster.error({title : "" , body: res.errmsg});
                return;
            }
            console.log('类别详情');
            console.log(res);
            $scope.obj = res.data;
            $scope.getParentIdList(res.data.parent_id);
            $scope.imgarr = res.data.image.split(',');
            console.log($scope.imgarr);
    
        });
    } else {
      $scope.parent_name = name;
    }
    $scope.change = function(aaa){
        console.log(aaa);
    }

     //保存
     $scope.save = function(){
         console.log($scope.obj);
        $resource('/api/ac/tmc/tourismPlaceTypeService/createPlaceType', {}, {}).
        save($scope.obj, function(res){
            if(res.errcode === 0){
                toaster.success({title:"", body:"添加成功"});
                $state.go('app.alltype');
                // $scope.cancel();
                init();
            }else{
                toaster.error({title:"", body:res.errmsg});
            }
        });
    };
    $scope.cancel = function(){
      $state.go('app.alltype');
    }
    $scope.del=function(index){
        $scope.imgarr.splice(index,1);
        $scope.obj.image=$scope.imgarr.join(',');
        
        console.log($scope.imgarr);
    }
    $scope.image = function(){
        var para = $state.get('app.imageupload');
        //设置上传文件夹，以自己业务命名
        angular.extend(para, {
            resolve : {  
                'dir' : function(){
                    return 't1';
                }
            } 
        })
        console.log(para);
        var modalInstance = $modal.open(para);
        modalInstance.opened.then(function() {// 模态窗口打开之后执行的函数  
            console.log('modal is opened');  
        });  
        modalInstance.result.then(function(result) {  
            console.log(result);  
            $scope.imgarr = result;
            $scope.obj.image=result.join(',');
            console.log($scope.obj.image);

        }, function(reason) {  
            console.log(reason);// 点击空白区域，总会输出backdrop  
            // click，点击取消，则会暑促cancel  
            $log.info('Modal dismissed at: ' + new Date());  
        }); 
    };
   
}    