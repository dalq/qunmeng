module.exports = function ($scope, $state, $stateParams, $modalInstance, $resource, $modal, toaster, item, FileUploader) {
  console.log(item);
  $scope.imgs = ['http://shangke2.oss-cn-qingdao.aliyuncs.com/app/t1_1508204469000/2222222.jpg', 'http://shangke2.oss-cn-qingdao.aliyuncs.com/app/t1_1508204469000/2222222.jpg', 'http://shangke2.oss-cn-qingdao.aliyuncs.com/app/t1_1508204469000/2222222.jpg',
                  'http://shangke2.oss-cn-qingdao.aliyuncs.com/app/t1_1508204469000/2222222.jpg', 'http://shangke2.oss-cn-qingdao.aliyuncs.com/app/t1_1508204469000/2222222.jpg'];
  $scope.info = {
    'book': ''
  };
  $scope.obj = {
    'isSelected': false
  }
  if (item) {
    $scope.info = item;
    if ($scope.info.book == 0) {
      $scope.obj.isSelected = true;
    } else {
      $scope.obj.isSelected = false;
    }
  }
  // 奖品logo
  var uploader = $scope.uploader = new FileUploader({
    url: 'http://cl.juyouhx.com/oss.php/oss/webuploader1?topdir=aa&selfdir=bb'
  });

  uploader.filters.push({
    name: 'imageFilter',
    fn: function (item /*{File|FileLikeObject}*/, options) {
      var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
      return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
    }
  });
  uploader.onSuccessItem = function (fileItem, response, status, headers) {
    $scope.info.image = response.savename;

  };
  $scope.ok = function () {
    $resource('/api/ac/tmc/tourismLineService/createLine', {}, {}).
      save($scope.info, function (res) {
        if (res.errcode != 0) {
          toaster.success({ title: "", body: res.errmsg });
          return;
        }
        $modalInstance.close();
        console.log(res);
      });
  }
  $scope.onChange = function (isSelected) {
    console.log(isSelected);
    if (isSelected === true) {
      $scope.info.book = 0;
    } else {
      $scope.info.book = 1;
    }
  }
  $scope.cancel = function () {
    $modalInstance.close();
  }

}
