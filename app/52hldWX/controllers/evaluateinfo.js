module.exports = function ($scope, $stateParams, $modal, $http, $q, $resource, toaster, $modalInstance,id, nickname) {
    $scope.nickname = nickname;
        $scope.id = id;
        $resource('/api/ac/tmc/tourismCommentService/findInfoById', {}, {})
        .save({
            'id': $scope.id
        }, function (res) {
            if (res.errcode !== 0) {
                toaster.error({ title: "提示", body: res.errmsg });
                return;
            }
            toaster.success({ title: "提示", body: "接口通了!" });
            console.log(res);
            console.log('上面是res')
            $scope.obj = res.data;
        });

        $scope.pass = function () {
            $resource('/api/ac/tmc/tourismCommentService/updateAdopt', {}, {})
                .save({
                    'id': $scope.id
                }, function (res) {
                    if (res.errcode !== 0) {
                        toaster.error({ title: "提示", body: res.errmsg });
                        toaster.error({ title: "提示", body: res.errmsg });
                        return;
                    }
                    toaster.success({ title: "提示", body: "操作成功!" });
                    // toaster.success({title:"",body:'亲,记得重新保存商客专属设置信息哦💐💐'});
                    $scope.util.$modalInstance.close();
                });
        };
    
        $scope.nopass = function () {
            var modalInstance = $modal.open({
              template: require('../views/evaluateReject.html'),
              controller: 'evaluateReject',
              size: '',
              resolve: {
                  id: function () {
                      return $scope.id;
                  }
              }
            });
            modalInstance.result.then(function (showResult) {	
                $scope.getlist();
            });
        };
        $scope.cancel = function(){
          $modalInstance.close();
        }
    
    };