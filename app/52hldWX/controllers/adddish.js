module.exports = function($scope, $state, $stateParams, $modalInstance, $resource,$modal,toaster,id){
    console.log(id);
    $scope.typelist = [];
    $scope.searchform = {};
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10 ;

    $scope.typearr=[
        {name:'请选择',value:''},
        {name:'商户',value:'M'},
        {name:'酒店',value:'H'},
        {name:'商场',value:'S'},
        {name:'交通',value:'T'},
        {name:'景区',value:'J'} 
    ]
    $scope.getlist = function () {
        var para = {
            pageNo:$scope.bigCurrentPage, 
            pageSize:$scope.itemsPerPage,
        };
        para = angular.extend(para, $scope.searchform);
        $resource('/api/ac/tmc/tourismPlaceService/findList', {}, {}).
        save(para,function (res) {
            if (res.errcode != 0) {
                toaster.success({title:"",body:res.errmsg});
                return;
            }
            console.log(res);
            $scope.objs = res.data.results;
            $scope.bigTotalItems = res.data.totalRecord;  
            $resource('/api/ac/tmc/tourismPlaceService/findPlaceByTypeIdList', {}, {}).
            save({'type_id':id},function (res) {
                if (res.errcode == 0) {
                    console.log(res.data);
                    $scope.typelist = res.data;
                    angular.forEach($scope.objs, function(item){
                        for(var i = 0; i < $scope.typelist.length; i++){
                            console.log('qqq');
                            console.log( $scope.typelist[i]);
                            if(item.id == $scope.typelist[i].place_id){
                                console.log('qqqq');
                                console.log(item.id);
                                item.mark = true;                                
                                continue;
                            }
                        }
                    });
                   
                }           
            });           
                                               
        });        
    };
    $scope.getlist();
    $scope.change = function(obj){
        console.log($scope.typelist);
        if(obj.mark ==true){
            $scope.typelist.push({place_id:obj.id})
        }else{
            for(var i = 0;i<$scope.typelist.length;i++){
                if(obj.place_id == $scope.typelist[i].place_id){
                    $scope.typelist.splice(i,1);
                }
            }
        }
        console.log('yigong杜少');
        console.log($scope.typelist);

       
    }

    //         //回显
            // $scope.fanhui = function(){               
                // $resource('/api/ac/tmc/tourismPlaceService/findPlaceByTypeIdList', {}, {}).
                // save({'type_id':id},function (res) {
                //     if (res.errcode == 0) {
                //         console.log(res.data);
                //         $scope.datas = res.data;
                //         console.log('上面是res.data')
                //         angular.forEach($scope.objs, function(item){
                //             for(var i = 0; i < res.data.length; i++){
                //                 if(item.id == res.data[i].place_id){
                //                     item.mark = true;                                
                //                     $scope.typelist.push({place_id:item.id});
                //                     continue;
                //                 }
                //             }
                //         });
                //     }           
                // });   
            // }
    
    $scope.ok = function(){
        console.log($scope.typelist);
        $resource('/api/ac/tmc/tourismPlaceService/createTypePlace', {}, {}).
        save({'type_id':id,'typelist':$scope.typelist}, function(res){
            if(res.errcode === 0){
                alert('操作成功');
                $modalInstance.close();

            }else{
                alert(res.errmsg);
            }
        });
    }
    $scope.cancel = function(){
        $modalInstance.close();
    }
    $scope.back = function(){
        console.log('返回商户');
        $modalInstance.close();
        $state.go('app.touristlist');
    }
}    