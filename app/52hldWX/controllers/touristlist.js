module.exports = function ($scope, $state, $resource, toaster, $modal) {
    /* 分页
 * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10         //每页显示几条
    $scope.searchform = {};

    $scope.getlist = function () {
        var para = {
            pageNo:$scope.bigCurrentPage, 
            pageSize:$scope.itemsPerPage,
        };
        $resource('/api/ac/tmc/tourismPlaceService/findList', {}, {}).
        save(para,function (res) {
            if (res.errcode != 0) {
                toaster.success({title:"",body:res.errmsg});
                return;
            }
            console.log(res);
            $scope.objs = res.data.results;
            $scope.bigTotalItems = res.data.totalRecord;
            $scope.objs.forEach(function(element) {
                element.isSelected = element.state=='1'?true:false;
            }, this);
        });   
    };
    $scope.getlist();
    
    $scope.sousuo = function () {
        console.log($scope.searchform.type)
        var para = {
            pageNo:$scope.bigCurrentPage, 
            pageSize:$scope.itemsPerPage,
            type:$scope.searchform.type,
            name:$scope.searchform.name,
            code:$scope.searchform.code
        };
        $resource('/api/ac/tmc/tourismPlaceService/findList', {}, {}).
        save(para,function (res) {
            if (res.errcode != 0) {
                toaster.success({title:"",body:res.errmsg});
                return;
            }
            console.log(res);
            $scope.objs = res.data.results;
            $scope.bigTotalItems = res.data.totalRecord;
            $scope.objs.forEach(function(element) {
                element.isSelected = element.state=='1'?true:false;
            }, this);
        });   
    };
    $scope.typearr=[
        {name:'请选择',value:''},
        {name:'商户',value:'M'},
        {name:'酒店',value:'H'},
        {name:'商场',value:'S'},
        {name:'交通',value:'T'},
        {name:'景区',value:'J'} 
    ]
    $scope.edit = function (id, type, code) {
        $state.go('app.addPlace_view', {'id' : id, 'type' : type, 'code' : code});
    };
    

    $scope.create = function(id){
        $state.go('app.addPlace_view');
    }
    $scope.asort = function(code, asort){
        $resource('/api/as/tc/place/updateHotel', {}, {}).
        save({'code' : code, 'asort' : asort},function (res) {
            if (res.errcode != 0) {
                toaster.error({title:"",body:res.errmsg});
                return;
            }
            console.log(res);
            $scope.getlist();


        }); 
    }
    $scope.adddish = function(id){
        console.log(id);
        $state.go('app.dishlist',{'id':id});
    }
    $scope.delete = function(id){
        if(confirm('确定删除该场所吗')){
            $resource('/api/ac/tmc/tourismPlaceService/updateDel ', {}, {}).
            save({'id' : id},function (res) {
                if (res.errcode != 0) {
                    toaster.error({title:"",body:res.errmsg});
                    return;
                }
                console.log(res);
                $scope.getlist();
            }); 
        }
    }
    $scope.useOrNoUse = function(id, state, book, code){
      if(state == '0') {
        if(book == '1') {
           // 启用
           $resource('/api/ac/tmc/tourismPlaceService/updateStateUp', {}, {}).
           save({'id' : id, 'state' : 1},function (res) {
               if (res.errcode != 0) {
                   toaster.error({title:"",body:res.errmsg});
                   return;
               }
               toaster.success({title:"",body:"启用成功"});
               $scope.getlist();
           });
        } else if(book === '0' && code !== undefined){
          // 启用
          $resource('/api/ac/tmc/tourismPlaceService/updateStateUp', {}, {}).
          save({'id' : id, 'state' : 1},function (res) {
              if (res.errcode != 0) {
                  toaster.error({title:"",body:res.errmsg});
                  return;
              }
              toaster.success({title:"",body:"启用成功"});
              $scope.getlist();
          });
        } else {
          alert('请先关联');
        }
      } else { // 停用
        $resource('/api/ac/tmc/tourismPlaceService/updateStateDown', {}, {}).
        save({'id' : id, 'state' : 0},function (res) {
            if (res.errcode != 0) {
                toaster.error({title:"",body:res.errmsg});
                return;
            }
            toaster.success({title:"",body:"停用成功"});
            $scope.getlist();
        }); 
      }
    }
    $scope.relate = function(id, name, type, code){
      console.log(id, name);
      var modalInstance = $modal.open({
        template: require('../views/relationlist.html'),
        controller: 'relationlist',
        size: '',
        resolve: {
            name: function () {
                return name;
            },
            id: function () {
                return id;
            },
            type: function () {
                return type;
            },
            code: function () {
              return code;
          }
        }
      });
      modalInstance.result.then(function (showResult) {	
          $scope.getlist();
      });
    }

};