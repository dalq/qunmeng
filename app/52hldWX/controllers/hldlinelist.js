module.exports = function ($scope, $state, $resource, toaster, $modal) {
  /* 分页
* ========================================= */
  $scope.maxSize = 5;             //最多显示多少个按钮
  $scope.bigCurrentPage = 1;      //当前页码
  $scope.itemsPerPage = 10         //每页显示几条
  $scope.searchform = {};

  $scope.getlist = function () {
      var para = {
          pageNo:$scope.bigCurrentPage, 
          pageSize:$scope.itemsPerPage,
      };
      para = angular.extend($scope.searchform,para);
      $resource('/api/ac/tmc/tourismLineService/findLineAdaminList', {}, {}).
      save(para,function (res) {
          if (res.errcode != 0) {
              toaster.error({title:"",body:res.errmsg});
              return;
          }
          console.log(res);
          $scope.objs = res.data.results;
          $scope.bigTotalItems = res.data.totalRecord;
          $scope.objs.forEach(function(element) {
              element.isSelected = element.state=='1'?true:false;
          }, this);
      });   
  };
  $scope.getlist();
  
  $scope.edit = function (item) {
    var modalInstance = $modal.open({
      template: require('../views/addhldline.html'),
      controller: 'addhldline',
      size: 'lg',
      resolve: {
          item: function () {
              return item;
          }
          // id: function () {
          //     return $stateParams.id;
          // },
      }
    });
    modalInstance.result.then(function (showResult) {	
        $scope.getlist();
    });
  };
  $scope.create = function(item){
    var modalInstance = $modal.open({
        template: require('../views/addhldline.html'),
        controller: 'addhldline',
        size: 'lg',
        resolve: {
          item: function () {
            return item;
          }
        }
    });
    modalInstance.result.then(function (showResult) {	
        $scope.getlist();
    });
  }
  $scope.detail = function(id){
    var modalInstance = $modal.open({
      template: require('../views/hldlineinfo.html'),
      controller: 'hldlineinfo',
      size: 'lg',
      resolve: {
        id: function () {
          return id;
        }
      }
    });
    modalInstance.result.then(function (showResult) {	
        $scope.getlist();
    });
  }
  
  $scope.delete = function(id){
    if(confirm('确定删除该线路产品吗')){
        $resource('/api/ac/tmc/tourismLineService/updateDel', {}, {}).
        save({'id' : id},function (res) {
            if (res.errcode != 0) {
                toaster.error({title:"",body:res.errmsg});
                return;
            }
            console.log(res);
            $scope.getlist();
        }); 
    }
  }
  $scope.up = function(id){
    if(confirm('确定上架吗?')){
        $resource('/api/ac/tmc/tourismLineService/updateState', {}, {}).
        save({'id' : id},function (res) {
            if (res.errcode != 0) {
                toaster.success({title:"",body:res.errmsg});
                return;
            }
            console.log(res);
            $scope.getlist();
            
        });  
    }
  }
  $scope.down = function(id){
    if(confirm('确定下架吗?')){
        $resource('/api/ac/tmc/tourismLineService/updateNoState', {}, {}).
        save({'id' : id},function (res) {
            if (res.errcode != 0) {
                toaster.success({title:"",body:res.errmsg});
                return;
            }
            console.log(res);
            $scope.getlist();
        });
    }  
  }
  $scope.saveasort = function(id, asort){
    $resource('/api/ac/tmc/tourismLineService/updateAsort', {}, {}).
    save({'id' : id, asort : asort},function (res) {
        if (res.errcode != 0) {
            toaster.error({title:"",body:res.errmsg});
            return;
        }
        console.log(res);
        toaster.success({title:"",body:"更改排序成功"});
        $scope.getlist();
    });  
  }

};