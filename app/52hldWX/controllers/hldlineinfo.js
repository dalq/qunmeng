module.exports = function($scope, $state, $stateParams, $modalInstance,$resource,$modal,toaster, id){
  $scope.obj = {};
  $resource('/api/ac/tmc/tourismLineService/setLineInfo', {}, {}).
  save({'id' : id},function (res) {
      if (res.errcode != 0) {
          toaster.error({title:"",body:res.errmsg});
          return;
      }
      $scope.obj = res.data;
  });  
  
  $scope.ok = function(){
    $modalInstance.close();
  }
  
  $scope.cancel = function(){
    $modalInstance.close();
  }
  
}
  