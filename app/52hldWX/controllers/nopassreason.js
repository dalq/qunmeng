module.exports = function($scope, $state, $stateParams, $modalInstance,$resource,$modal,toaster, id){
  $scope.info = {
    'id' : id
  };
  $resource('/api/ac/tmc/tourismTravelsService/getTravelsInfo', {}, {}).
  save({'id' : id},function (res) {
      if (res.errcode != 0) {
          toaster.error({title:"",body:res.errmsg});
          return;
      }
      $scope.info.reason = res.data.reason;
      
  });  
  
  $scope.ok = function(){
    $resource('/api/ac/tmc/tourismTravelsService/updateNoState', {}, {}).
    save($scope.info,function (res) {
        if (res.errcode != 0) {
            toaster.error({title:"",body:res.errmsg});
            return;
        }
        toaster.success({title:"",body:'驳回成功'});
        $modalInstance.close();
    
        
    });  
  }
  
  $scope.cancel = function(){
    $modalInstance.close();
  }
  
}
  