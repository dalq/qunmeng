module.exports = function ($scope, $state, $resource, toaster, $modal) {
  /* 分页
* ========================================= */
  $scope.maxSize = 5;             //最多显示多少个按钮
  $scope.bigCurrentPage = 1;      //当前页码
  $scope.itemsPerPage = 10         //每页显示几条
  $scope.searchform = {};

  $scope.getlist = function () {
      var para = {
          pageNo:$scope.bigCurrentPage, 
          pageSize:$scope.itemsPerPage,
      };
      para = angular.extend($scope.searchform,para);
      $resource('/api/ac/tmc/tourismTravelsService/findTravelsAdaminList', {}, {}).
      save(para,function (res) {
          if (res.errcode != 0) {
              toaster.error({title:"",body:res.errmsg});
              return;
          }
          console.log(res);
          $scope.objs = res.data.results;
          $scope.bigTotalItems = res.data.totalRecord;
          $scope.objs.forEach(function(element) {
              console.log(element.destination.split(','));
          }, this);
      });   
  };
  $scope.getlist();
  
  $scope.detail = function(id, nickname){
    var modalInstance = $modal.open({
      template: require('../views/travelnoteinfo.html'),
      controller: 'travelnoteinfo',
      size: 'lg',
      resolve: {
        id: function () {
          return id;
        },
        nickname: function () {
          return nickname;
        }
      }
    });
    modalInstance.result.then(function (showResult) {	
        $scope.getlist();
    });
  }
  $scope.saveasort = function(id, asort){
    $resource('/api/ac/tmc/tourismTravelsService/updateAsort', {}, {}).
    save({'id' : id, asort : asort},function (res) {
        if (res.errcode != 0) {
            toaster.error({title:"",body:res.errmsg});
            return;
        }
        console.log(res);
        toaster.success({title:"",body:"更改排序成功"});
        $scope.getlist();
    });  
  }
  
  

};