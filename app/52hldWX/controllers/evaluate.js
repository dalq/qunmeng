module.exports = function($scope, $modal, $state, $timeout, $resource){
    
        $scope.start = {
            'date' : '',
            'opened' : false,
        };
    
        $scope.end = {
            'date' : '',
            'opened' : false,
        };
    
        $scope.open = function(obj) {
            obj.opened = true;
        }
    
    
        $scope.myKeyup = function() {
        }
    
        $scope.searchform = {};
        /* 分页
         * ========================================= */
        $scope.maxSize = 5;            //最多显示多少个按钮
        $scope.bigCurrentPage = 1;      //当前页码
        $scope.itemsPerPage = 10;         //每页显示几条
        $scope.form = {};
        $scope.load = function () {
    
            var para = {
                pageNo: $scope.bigCurrentPage,
                pageSize: $scope.itemsPerPage
            };
            para = angular.extend($scope.searchform, para);
            $resource('/api/ac/tmc/tourismCommentService/findList', {}, {}).save(para, function (res) {
    
                if (res.errcode !== 0) {
                    alert(res.errmsg);
                    return;
                }
                console.log(res);
                $scope.objs = res.data.results;
                $scope.bigTotalItems = res.data.totalRecord;
                for(var i = 0;i<$scope.objs.length;i++){
                    if($scope.objs[i].place_type=='M'){
                        $scope.objs[i].place_type = '商户'
                    }
                    if($scope.objs[i].place_type=='J'){
                        $scope.objs[i].place_type = '景区'
                    }
                    if($scope.objs[i].place_type=='H'){
                        $scope.objs[i].place_type = '酒店'
                    }
                    if($scope.objs[i].place_type=='S'){
                        $scope.objs[i].place_type = '商场'
                    }
                    if($scope.objs[i].place_type=='T'){
                        $scope.objs[i].place_type = '交通'
                    }
                }
            });
    
        };
        $scope.load();

        $scope.typearr = [
            {
                code:'',
                name:'未选择'
            },
            {
                code:'H',
                name:'酒店'
            },
            {
                code:'M',
                name:'商户'
            },
            {
                code:'J',
                name:'景区'
            },
            {
                code:'T',
                name:'交通'
            },
            {
                code:'S',
                name:'商场'
            },
            {
                code:'L',
                name:'线路'
            },
            {
              code:'A',
              name:'游记'
          },
        ]
        $scope.search = function(){
            var para = {
                pageNo: $scope.bigCurrentPage,
                pageSize: $scope.itemsPerPage,
                place_id:$scope.form.place_id,
                title:$scope.form.title,
                place_type:$scope.form.type
            };
    
            $resource('/api/ac/tmc/tourismCommentService/findList', {}, {}).save(para, function (res) {
    
                if (res.errcode !== 0) {
                    alert(res.errmsg);
                    return;
                }
                console.log(res);
                $scope.objs = res.data.results;
                $scope.bigTotalItems = res.data.totalRecord;
                for(var i = 0;i<$scope.objs.length;i++){
                    if($scope.objs[i].place_type=='M'){
                        $scope.objs[i].place_type = '商户'
                    }
                    if($scope.objs[i].place_type=='J'){
                        $scope.objs[i].place_type = '景区'
                    }
                    if($scope.objs[i].place_type=='H'){
                        $scope.objs[i].place_type = '酒店'
                    }
                    if($scope.objs[i].place_type=='S'){
                        $scope.objs[i].place_type = '商场'
                    }
                    if($scope.objs[i].place_type=='T'){
                        $scope.objs[i].place_type = '交通'
                    }
                }
            });
        }

        $scope.info = function(id, nickname){
            console.log(id);
            var modalInstance = $modal.open({
                template: require('../views/evaluateinfo.html'),
                controller: 'evaluateinfo',
                size: 'lg',
                resolve: {
                    id: function () {
                        return id;
                    },
                    nickname: function () {
                        return nickname;
                    }
                }
            });
            modalInstance.result.then(function (showResult) {	
                     $scope.init();
            });
        }
    
    };
    