module.exports = function ($scope, $state, $resource, toaster, $modal,$stateParams) {
    /* 分页
 * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10         //每页显示几条
    $scope.searchform = {};
    console.log($stateParams.id);
    $scope.getlist = function () {
        var para = {
            pageNo:$scope.bigCurrentPage, 
            pageSize:$scope.itemsPerPage,
            id:$stateParams.id
        };
        $resource('/api/ac/tmc/tourismPlaceMerchantService/findProductBytypeList', {}, {}).
        save(para,function (res) {
            if (res.errcode != 0) {
                toaster.success({title:"",body:res.errmsg});
                return;
            }
            console.log(res);
            $scope.objs = res.data.results;
            $scope.bigTotalItems = res.data.totalRecord;
            $scope.objs.forEach(function(element) {
                element.isSelected = element.state=='1'?true:false;
            }, this);
        });   
    };
    $scope.getlist();
    $scope.edit = function(obj){
        var modalInstance = $modal.open({
            template: require('../views/addTourist.html'),
            controller: 'addTourist',
            size: 'lg',
            resolve: {
                item: function () {
                    return obj;
                },
                id: function () {
                    return $stateParams.id;
                },
            }
        });
        modalInstance.result.then(function (showResult) {	
            $scope.getlist();
        });
    }

    $scope.create = function(obj){
        var modalInstance = $modal.open({
            template: require('../views/addTourist.html'),
            controller: 'addTourist',
            size: 'lg',
            resolve: {
                id: function () {
                    return $stateParams.id;
                },
                item: function () {
                    return obj;
                },
            }
        });
        modalInstance.result.then(function (showResult) {	
            $scope.getlist();
        });
    }
    $scope.asort = function(code, asort){
        $resource('/api/as/tc/place/updateHotel', {}, {}).
        save({'code' : code, 'asort' : asort},function (res) {
            if (res.errcode != 0) {
                toaster.error({title:"",body:res.errmsg});
                return;
            }
            console.log(res);
            $scope.getlist();


        }); 
    }
    $scope.delete = function(id){
        $resource('/api/ac/tmc/tourismPlaceMerchantService/updateProductDel', {}, {}).
        save({'id' :id },function (res) {
            if (res.errcode != 0) {
                toaster.error({title:"",body:res.errmsg});
                return;
            }else{
                toaster.success({title:"",body:"删除成功"});
            }
            $scope.getlist();


        }); 

    }


};