module.exports = function($scope, $resource, $state, $modal, $modalInstance, toaster, id, name, type, code){
    $scope.item = {
        'name' : name,
        'id' : id
    };
    $scope.searchform = {
      'selected' : {
        'name' : ''
      }
    };
    $scope.info = {
      'id' : id,
      'code' : '',
      'sub_code' : ''
    }
    console.log(code);
    $scope.getlist = function(){
        $resource('/api/as/tc/place/findHLDPlaceList ', {}, {}).
        save({'type' : type},function (res) {
            if (res.errcode != 0) {
                toaster.error({title:"",body:res.errmsg});
                return;
            }
            $scope.placelist = res.data;
            $scope.placelist.forEach(function(element) {
              if(element.code === code){
                  $scope.searchform.selected.name = element.name;
                  console.log(element.name);
              }
            }, this);
            console.log(res);
        });   
    }
    $scope.getlist();
    $scope.ok = function(){
      console.log($scope.searchform);
      $scope.info.code = $scope.searchform.selected.code;
      $scope.info.sub_code = $scope.searchform.selected.sub_code; 
      $resource('/api/ac/tmc/tourismPlaceService/updateCode', {}, {}).
      save($scope.info,function (res) {
          if (res.errcode != 0) {
              toaster.error({title:"",body:res.errmsg});
              return;
          }
          toaster.success({title:"",body:"绑定成功"});
          $modalInstance.close();
      });  
    }
    $scope.cancel = function(){
      $modalInstance.close();
    }
}  