module.exports = function($scope, $state, $stateParams, $modalInstance,$resource,$modal,toaster, id){
  $scope.obj = {};
  $scope.info = {};
  $scope.infos = {};
  $resource('/api/ac/tmc/tourismComplaintService/getComplaintInfo', {}, {}).
  save({'id' : id},function (res) {
      if (res.errcode != 0) {
          toaster.error({title:"",body:res.errmsg});
          return;
      }
    //  $scope.prosecutionlist=[];
      //   for(var z=0;z<$scope.prosecutionlist.length;z++){
      //     if($scope.prosecutionlist[z].prosecution_info!=null&&$scope.prosecutionlist[z].prosecution_info!=undefined&&$scope.prosecutionlist[z].prosecution_info!=''){
      //          $scope.prosecutionlist.push($scope.prosecutionlist[z]);
      //     }
      //  }
      $scope.obj = res.data;
      $scope.obj.image_complaint = $scope.obj.image_complaint.split(',');

      console.log( $scope.obj.image_complaint);
  });   
  
    $scope.ok = function (ss){
      // console.log($scope.prosecutionlist.length);
      console.log($scope.obj.prosecutionlist);
      console.log('-----------------------');
    console.log($scope.obj.prosecutionlist[$scope.obj.prosecutionlist.length-1]);
    console.log('-----------------------');
      $scope.pid= $scope.obj.prosecutionlist[$scope.obj.prosecutionlist.length-1].prosecution_id
      $scope.info = {
        'id' : $scope.obj.id,
        'prosecution_reply' : ss,
        'prosecution_id':$scope.pid
      }
      console.log($scope.info);
        $resource('/api/ac/tmc/tourismComplaintService/updateProsecutionInfo', {}, {})
        .save($scope.info, function (res) {
            if (res.errcode !== 0) {
                toaster.error({ title: "提示", body: res.errmsg });
                return;
            }
            toaster.success({ title: "提示", body: "回复成功!" });
            $modalInstance.close();
        });
    }
    $scope.ok1 = function (aa){
      $scope.infos = {
        'id' : $scope.obj.id,
        'reply' : aa
      }
        console.log($scope.infos);
        $resource('/api/ac/tmc/tourismComplaintService/updateComplaintInfo', {}, {})
        .save($scope.infos, function (res) {
            if (res.errcode !== 0) {
                toaster.error({ title: "提示", body: res.errmsg });
                return;
            }
            toaster.success({ title: "提示", body: "回复成功!" });
            $modalInstance.close();
        });
    }
    $scope.cancel = function () {
      $modalInstance.close();
    }
  
}
  