module.exports = function($scope, $state, $stateParams, $modalInstance,$resource,$modal,toaster, id, nickname){
  $scope.nickname = nickname;
  console.log(nickname);
  $scope.obj = {};
  $resource('/api/ac/tmc/tourismTravelsService/getTravelsInfo', {}, {}).
  save({'id' : id},function (res) {
      if (res.errcode != 0) {
          toaster.error({title:"",body:res.errmsg});
          return;
      }
      $scope.obj = res.data;
      $scope.destinationarr = $scope.obj.destination.split(',');
      console.log($scope.destinationarr);
  });  
  
  $scope.pass = function(){
    $resource('/api/ac/tmc/tourismTravelsService/updateState', {}, {}).
    save({'id' : id},function (res) {
        if (res.errcode != 0) {
            toaster.error({title:"",body:res.errmsg});
            return;
        }
        toaster.success({title:"",body:'💐你,审核通过啦啦啦'});
        $modalInstance.close();
    });  
  }
  $scope.nopass = function(id){
    var modalInstance = $modal.open({
      template: require('../views/nopassreason.html'),
      controller: 'nopassreason',
      size: 'lg',
      resolve: {
        id: function () {
          return id;
        }
      }
    });
    modalInstance.result.then(function (showResult) {	
    });
  }
  $scope.seehotel = function(id){
    var modalInstance = $modal.open({
      template: require('../views/selecthotel.html'),
      controller: 'selecthotel',
      size: 'lg',
      resolve: {
        id: function () {
          return id;
        }
      }
    });
    modalInstance.result.then(function (showResult) {	
    });
  }
  
  $scope.cancel = function(){
    $modalInstance.close();
  }
  
  /*********  酒店部分  *********/
  $scope.searchform = {};  
  var para = {
    'type' : 'H'
  }
  $scope.hotelarr = [];
  para = angular.extend($scope.searchform, para);
  $scope.getlist = function () {
      $resource('/api/ac/tmc/tourismTravelsService/findHotelProductList', {}, {}).
      save(para,function (res) {
          if (res.errcode != 0) {
              console.log(res.errmsg);
              return;
          }
          $scope.hotelProduct = res.data;
          console.log('酒店列表');
          console.log($scope.hotelProduct);
          // 循环比较回显
          $resource('/api/ac/tmc/tourismTravelsService/findHotelProductAdaminList', {}, {}).
          save({'id' : id, 'type' : 'H'},function (res) {
              if (res.errcode != 0) {
                  // toaster.error({title:"",body:res.errmsg});
                  return;
              }
              console.log('回显数据');
              // console.log(res);
              $scope.hotelarr = res.data;
              console.log($scope.hotelarr);
              $scope.hotelProduct.forEach(element => {
                for(var i = 0; i < $scope.hotelarr.length; i++){
                  if (element.place_id === $scope.hotelarr[i].place_id){
                      element.value = true;
                  }
            
                }
              });
              
          }); 
      });  
  }
  $scope.getlist();
  $scope.changeHotel = function (obj) {
    console.log(obj);
    if(obj.value === true){
      $scope.hotelarr.push(obj);
      console.log($scope.hotelarr);
  
    } else {
      for(var i = 0;i<$scope.hotelarr.length;i++){
          if(obj.place_id === $scope.hotelarr[i].place_id){
              $scope.hotelarr.splice(i,1);
          }
      }
      console.log($scope.hotelarr);
  
    }
  }
  $scope.savehotel = function () {
    console.log($scope.hotelarr);
    var para = {
      'id' : id,
      'list' : $scope.hotelarr,
      'type' : 'H'
    }
    $resource('/api/ac/tmc/tourismTravelsService/createSign', {}, {}).
    save(para,function (res) {
        if (res.errcode != 0) {
            toaster.error({title:"",body:res.errmsg});
            return;
        }
        console.log(res);
        toaster.success({title:"",body:'操作成功'});
        
    });  
  }

  /*********  景区部分  *********/
  $scope.searchform1 = {};
  var para1 = {
    'type' : 'J'
  }
  $scope.viewarr = [];
  para1 = angular.extend($scope.searchform1, para1);
  $scope.getViewList = function () {
    $resource('/api/ac/tmc/tourismTravelsService/findHotelProductList', {}, {}).
    save(para1,function (res) {
        if (res.errcode != 0) {
            console.log(res.errmsg);
            return;
        }
        $scope.viewProduct = res.data;
        console.log('景区列表');
        console.log(res);
        // 循环比较回显
        $resource('/api/ac/tmc/tourismTravelsService/findHotelProductAdaminList', {}, {}).
        save({'id' : id, 'type' : 'J' },function (res) {
            if (res.errcode != 0) {
                // toaster.error({title:"",body:res.errmsg});
                return;
            }
            console.log('回显数据');
            // console.log(res);
            $scope.viewarr = res.data;
            console.log($scope.viewarr);
            $scope.viewProduct.forEach(element => {
              for(var i = 0; i < $scope.viewarr.length; i++){
                if (element.place_id === $scope.viewarr[i].place_id){
                    element.value = true;
                }
          
              }
            });
            
        }); 
    })
  }
  $scope.getViewList();
  $scope.changeView = function (obj) {
    console.log(obj);
    if(obj.value === true){
      $scope.viewarr.push(obj);
      console.log($scope.viewarr);
  
    } else {
      for(var i = 0;i<$scope.viewarr.length;i++){
          if(obj.place_id === $scope.viewarr[i].place_id){
              $scope.viewarr.splice(i,1);
          }
      }
      console.log($scope.viewarr);
  
    }
  }
  // 保存景区
  $scope.saveview = function () {
    console.log($scope.viewarr);
    var para = {
      'id' : id,
      'list' : $scope.viewarr,
      'type' : 'J'
    }
    $resource('/api/ac/tmc/tourismTravelsService/createSign', {}, {}).
    save(para,function (res) {
        if (res.errcode != 0) {
            toaster.error({title:"",body:res.errmsg});
            return;
        }
        console.log(res);
        toaster.success({title:"",body:'操作成功'});
        
    });  
  }
    
  
 
}
  