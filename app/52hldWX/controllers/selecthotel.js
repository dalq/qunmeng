module.exports = function($scope, $state, $stateParams, $modalInstance,$resource,$modal,toaster, id){
  $scope.searchform = {};  
  var para = {
    'type' : 'H'
  }
  $scope.hotelarr = [];
  para = angular.extend($scope.searchform, para);
  $scope.getlist = function () {
        $resource('/api/ac/tmc/tourismTravelsService/findHotelProductList', {}, {}).
        save(para,function (res) {
            if (res.errcode != 0) {
                toaster.error({title:"",body:res.errmsg});
                return;
            }
            $scope.hotelProduct = res.data;
            console.log(res);
            // 循环比较回显
            $resource('/api/ac/tmc/tourismTravelsService/findHotelProductAdaminList', {}, {}).
            save({'id' : id},function (res) {
                if (res.errcode != 0) {
                    toaster.error({title:"",body:res.errmsg});
                    return;
                }
                console.log(res);
                $scope.hotelarr = res.data;
                $scope.hotelProduct.forEach(element => {
                  for(var i = 0; i < $scope.hotelarr.length; i++){
                    if (element.place_id === $scope.hotelarr[i].place_id){
                      element.value = true;
                    }
              
                  }
                });
                
            }); 
        });  
    }
    $scope.getlist();
    $scope.flag = {
      value : false
    }
    $scope.change = function (obj) {
        console.log(obj);
        if(obj.value === true){
          $scope.hotelarr.push(obj);
          console.log($scope.hotelarr);
      
        } else {
          for(var i = 0;i<$scope.hotelarr.length;i++){
              if(obj.place_id === $scope.hotelarr[i].place_id){
                  $scope.hotelarr.splice(i,1);
              }
          }
          console.log($scope.hotelarr);
      
        }
    }
    $scope.ok = function () {
      console.log($scope.hotelarr);
      var para = {
        'id' : id,
        'list' : $scope.hotelarr
      }
      $resource('/api/ac/tmc/tourismTravelsService/createSign', {}, {}).
      save(para,function (res) {
          if (res.errcode != 0) {
              toaster.error({title:"",body:res.errmsg});
              return;
          }
          console.log(res);
          toaster.success({title:"",body:'操作成功'});
          
      });  
    }
    
}  