module.exports = function($scope, $state, $stateParams, $modalInstance, $resource,$modal,toaster,id,item){
    console.log(item);
    $scope.item=item;
    if(item!=undefined&&item.product_pic!=''){
        $scope.array = $scope.item.product_pic.split(',')
    }
    $scope.image = function(){     
        var para = $state.get('app.imageupload');
        //设置上传文件夹，以自己业务命名
        angular.extend(para, {
            resolve : {  
                'dir' : function(){
                    return 't1';
                }
            } 
        })
        console.log(para);
        var modalInstance = $modal.open(para);
        modalInstance.opened.then(function() {// 模态窗口打开之后执行的函数  
            console.log('modal is opened');  
        });  
        modalInstance.result.then(function(result) {  
            console.log(result);  
            // $scope.imagearr = JSON.stringify(result);
            $scope.array = result;
            $scope.item.product_pic=$scope.array;	
        }, function(reason) {  
            console.log(reason);// 点击空白区域，总会输出backdrop  
            // click，点击取消，则会暑促cancel 
            $log.info('Modal dismissed at: ' + new Date());  
        }); 
    };
    $scope.del=function(index){
        $scope.array.splice(index,1);
        $scope.item.product_pic=$scope.array;
        console.log($scope.item.product_pic);
    }
    $scope.cancel=function(index){
        $modalInstance.close();
    }
    $scope.ok = function(){
        console.log($scope.item.img);
        if($scope.item.img==undefined){
            $scope.item.img = '';
        }else if($scope.item.img != ''){
            $scope.item.img = $scope.item.product_pic.join(",");
        }

        var para = {  
            id:$scope.item.product_id,
            place_id:id,   
            product_name:$scope.item.product_name,
            product_info:$scope.item.product_info,
            product_pic:$scope.item.img,
            product_characteristic:$scope.item.product_characteristic,
            product_rack_rate:$scope.item.product_rack_rate,
            product_membership_price:$scope.item.product_membership_price,
            product_online_price:$scope.item.product_online_price
         }
    
         console.log(para);
         console.log('上面是para');
         $resource('/api/ac/tmc/tourismPlaceMerchantService/insertProduct', {}, {}).save(para, function(res){
                // console.log(res);
                if(res.errcode === 0){  
                    toaster.success({title:"",body:"提交成功"});
                    $modalInstance.close();
                }else{
                    toaster.error({title:"",body:res.errmsg});
                }
            });
    }
}    