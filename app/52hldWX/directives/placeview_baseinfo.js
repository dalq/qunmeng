module.exports = function ($resource, $state, $http, $q, toaster, $modal) {
    return {
    
        restrict: 'AE',
        template: require('../views/placeview_baseinfo.html'),
        replace: true,
        scope: {
            'placeobj': '=',
            'funobj': '=',
            'baseinfo': '=',
            'util': '=',
        },
        link: function (scope, elements, attrs) {
            var obj = {
                'id': scope.placeobj.id,
                'type' : scope.placeobj.type,
                'province': '',	//省
                'city': '',		//市
                'area': '',	//区
                'business_district': '', 	//商圈
                'book' : '1'
            };
            scope.bookinfo = {
              'isSelected' : false
            }
            scope.onChange = function (isSelected) {
              console.log(isSelected);
              if (isSelected === true) {
                scope.placeobj.book = 0;
              } else {
                scope.placeobj.book = 1;
              }
            }
            angular.extend(scope.placeobj, obj);
            
            scope.cardtype = [
                {
                    'label': '免费wifi',
                    'value1': '0',
                    'checked': false,
                },
                {
                    'label': '停车场',
                    'value1': '1',
                    'checked': false,
                    
                },
                {
                    'label': '接送服务',
                    'value1': '2',
                    'checked': false
                },
                {
                    'label': '行李寄存',
                    'value1': '3',
                    'checked': false
                },
                {
                    'label': '餐厅',
                    'value1': '4',
                    'checked': false
                },
                {
                    'label': '24h热水',
                    'value1': '5',
                    'checked': false
                },
                {
                    'label': '吹风机',
                    'value1': '6',
                    'checked': false
                },
                {
                    'label': '免费洗漱用品',
                    'value1': '7',
                    'checked': false,
                },
                {
                    'label': '叫醒服务',
                    'value1': '8',
                    'checked': false,
                    
                },
                {
                    'label': '送餐服务',
                    'value1': '9',
                    'checked': false
                },
                {
                    'label': '会议室',
                    'value1': '10',
                    'checked': false
                },
                {
                    'label': '商务中心',
                    'value1': '11',
                    'checked': false
                },
                {
                    'label': '可吸烟',
                    'value1': '12',
                    'checked': false
                },
            ]
            //页面需要的信息。
            scope.categorylist = [{'value' : 'J', 'label' : '景区'},{'value' : 'M', 'label' : '商户'},{'value' : 'H', 'label' : '酒店'},{'value' : 'T', 'label' : '交通'},{'value' : 'S', 'label' : '商场'}];
            
            scope.page = {};
            
            var beforedata = {
                //省份列表
                'provincelist':
                $http({
                    'method': 'GET',
                    'url': '/api/us/sc/city/arealist',
                }),
                //城市列表
                'citylist':
                $http({
                    'method': 'GET',
                    'url': '/api/us/sc/city/arealist',
                    'params': { 'code': '210000' },
                }),
                //区
                'districtlist':
                $http({
                    'method': 'GET',
                    'url': '/api/us/sc/city/arealist',
                    'params': { 'code': '210100' },
                }),
            };

            if(scope.placeobj.id !== ''){
                beforedata['placeinfo'] = $http({
                    'method' : 'GET', 
                    'url': '/api/ac/tmc/tourismPlaceService/findInfoById',
                    'params' : {
                        'id' : scope.placeobj.id
                    }
                });
            }


            $q.all(beforedata).then(function(res){
              console.log(res);
                // 地址信息
                if(res.provincelist.data.errcode === 0){

                }else{
                    alert(res.provincelist.data.errmsg);
                    return ;
                }

                if(res.citylist.data.errcode === 0){

                }else{
                    alert(res.citylist.data.errmsg);
                    return ;
                }

                if(res.districtlist.data.errcode === 0){

                }else{
                    alert(res.districtlist.data.errmsg);
                    return ;
                }

                if(angular.isDefined(res.placeinfo)){
                    //地点信息
                    if(res.placeinfo.data.errcode === 0){
                        angular.extend(scope.placeobj, res.placeinfo.data.data);
                        console.log('闲情');
                        console.log(scope.placeobj);
                        console.log('轮播图' + scope.placeobj.pictures_arr);
                        if(scope.placeobj.pictures_arr !== undefined){
                          scope.imgarr = scope.placeobj.pictures_arr.split(',');
                        }
                        console.log(scope.placeobj.infomation);
                        console.log('上面是placeobj.infomation') ;
                        if (scope.placeobj.book == 0) {
                          scope.bookinfo.isSelected = true;
                        } else {
                          scope.bookinfo.isSelected = false;
                        }
                        //多选
                        var unionPayArr = [];
                        unionPayArr = scope.placeobj.infomation.split(',');
                        console.log(unionPayArr);
                        for(var i = 0; i < scope.cardtype.length; i++){
                            for(var j = 0; j < unionPayArr.length; j++){
                                if(scope.cardtype[i].value1 == unionPayArr[j]){
                                    console.log(scope.cardtype[i].value1, unionPayArr[j]);
                                    scope.cardtype[i].value = true;
                                }
                            }
                        }
                    }else{
                        alert(res.placeinfo.data.errmsg);
                        return
                    }
                    
                }

                scope.page = {
                    'provincelist' : res.provincelist.data.data,
                    'citylist' : res.citylist.data.data,
                    'districtlist' : res.districtlist.data.data,
                    'business_districtlist' : [],
                }

                if(scope.placeobj.province == ''){
                    scope.placeobj.province = '210000';	//辽宁省
                    scope.placeobj.city = '210100';		//沈阳市
                    scope.placeobj.area = '';		//铁西区
                    scope.placeobj.business_district = ''	//xxx商圈
                } else {
                    getarea('province', scope.placeobj.province);

                    if(!scope.placeobj.city == ''){
                        getarea('city', scope.placeobj.city);
                    }

                    if(!scope.placeobj.area == ''){
                        getarea('area', scope.placeobj.area);
                    }
                    
                }
              

            });


            //省
            scope.changeprovince = function(code){
                scope.placeobj.city = '';
                scope.placeobj.area = '';
                scope.placeobj.business_district = '';

                scope.page.citylist = [];
                scope.page.districtlist = [];
                scope.page.business_districtlist = [];

                getarea('province', code);
            };
            //市
            scope.changecity = function(code){
                scope.placeobj.area = '';
                scope.placeobj.business_district = '';

                scope.page.districtlist = [];
                scope.page.business_districtlist = [];

                getarea('city', code);
            };
            //区
            scope.changedistrict = function(code){
                console.log('ququuququ', code);
                scope.placeobj.business_district = '';
                scope.page.business_districtlist = [];
                getarea('area', code);
            };
            


            function getarea(what, code){
                $resource('/api/us/sc/city/arealist', {}, {})
                .get({'code' : code}, function(res){
                    if(res.errcode !== 0){
                        alert(res.errmsg);
                        return;
                    }

                    if(what === 'province'){
                        scope.page.citylist = res.data;
                    }else if(what === 'city'){
                        scope.page.districtlist = res.data;
                    }else if(what === 'area'){
                        scope.page.business_districtlist = res.data;
                    }
                });
            }

            
            scope.save = function(){
                console.log('保存');
                var str = '';
                for(var i = 0;i<scope.cardtype.length;i++){
                    if(scope.cardtype[i].value == true){                      
                        str = str+scope.cardtype[i].value1+',';
                    }
                }
                
                str = str.substring(0,str.length-1);
                console.log(str);
                scope.placeobj.infomation = str;
                $resource('/api/ac/tmc/tourismPlaceService/create', {}, {}).save(scope.placeobj, function (res) {
                    if (res.errcode !== 0) {
                        toaster.error({ title: "提示", body: res.errmsg });
                        return;
                    }
                    toaster.success({ title: "提示", body: "创建成功" });
                    scope.placeobj.id = res.data.id;
                    console.log(scope.placeobj.type);
                    console.log(scope.placeobj);
                });
            }
           
            scope.del=function(index){
                scope.imgarr.splice(index,1);
                scope.placeobj.pictures_arr=scope.imgarr;
            }
            scope.image = function(){
                var para = $state.get('app.imageupload');
                //设置上传文件夹，以自己业务命名
                angular.extend(para, {
                    resolve : {  
                        'dir' : function(){
                            return 't1';
                        }
                    } 
                })
                console.log(para);
                var modalInstance = $modal.open(para);
                modalInstance.opened.then(function() {// 模态窗口打开之后执行的函数  
                    console.log('modal is opened');  
                });  
                modalInstance.result.then(function(result) {  
                    console.log(result);  
                    scope.imgarr = result;
                    scope.placeobj.pictures_arr=result.join(',');
                    console.log(scope.placeobj.image);
        
                }, function(reason) {  
                    console.log(reason);// 点击空白区域，总会输出backdrop  
                    // click，点击取消，则会暑促cancel  
                    $log.info('Modal dismissed at: ' + new Date());  
                }); 
            };
            console.log(scope.placeobj);
            
        }
    };
};
    
    