module.exports = function ($resource, $state, $http, $q, FileUploader, toaster) {
    
        return {
    
            restrict: 'AE',
            template: require('../views/travelinginfo.html'),
            replace: true,
            scope: {
                'placeobj': '=',
                'funobj': '=',
                'baseinfo': '=',
                'util': '=',
            },
            link: function (scope, elements, attrs) {
                scope.info = {
                    'place_id': scope.placeobj.id,
                    'product_rack_rate':scope.product_rack_rate,
                    'product_membership_price':scope.product_membership_price,
                    'product_online_price':scope.product_online_price

                };
                if(scope.placeobj.id != ''){
                    $resource('/api/ac/tmc/tourismPlaceViewService/findInfoById', {}, {}).save({'id' : scope.placeobj.id}, function (res) {
                        if (res.errcode !== 0) {
                            if(res.errcode === 10010){
                                toaster.warning({ title: "提示", body: "还没添加信息,请先填写信息" });
                            } else {
                                toaster.error({ title: "提示", body: res.errmsg });
                            }
                            return;
                        }
                        console.log(res);
                        scope.info = res.data;
                        scope.vm1.date = scope.util.str2date(res.data.view_tourist_date);
                    });
                }

                 
                scope.vm1 = {
                    'date' : '',
                    'options' : {
                        format: "YYYY-MM-DD",
                        locale : 'zh-cn'
                    }
                }
                scope.info.view_tourist_date = scope.info.view_tourist_date;
                scope.save = function(){
                    if (typeof scope.vm1.date === 'string') {
                        scope.vm1.date = scope.vm1.date;
                    } else {
                        scope.vm1.date = scope.util.date2str(scope.vm1.date._d);
                    }
                    scope.info.view_tourist_date = scope.vm1.date;
                    console.log(scope.info.view_tourist_date);
                    $resource('/api/ac/tmc/tourismPlaceViewService/create', {}, {}).save(scope.info, function (res) {
                        if (res.errcode !== 0) {
                            toaster.error({ title: "提示", body: res.errmsg });
                            return;
                        }
                        toaster.success({ title: "提示", body: "保存成功" });
                        console.log(scope.placeobj.id);
                    });
                }
                console.log(scope.placeobj);
               
            }
        };
    };
    
    