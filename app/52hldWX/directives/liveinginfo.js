module.exports = function ($resource, $state, $http, $q, FileUploader, toaster) {
    
        return {
    
            restrict: 'AE',
            template: require('../views/liveinginfo.html'),
            replace: true,
            scope: {
                'placeobj': '=',
                'funobj': '=',
                'baseinfo': '=',
                'util': '=',
            },
            link: function (scope, elements, attrs) {
                scope.info = {
                    'place_id': scope.placeobj.id,
                    'hotel_parking' : '1',
                    'hotel_room_pet' : '1',
                    'hotel_room_smoke' : '1'
                };
                scope.vm1 = {
                    'date' : '',
                    'options' : {
                        // format: "YYYY-MM-DD",
                        locale : 'zh-cn'
                    }
                }
                scope.cardtype = [
                    {
                        'label': '停车费',
                        'value1': '0',
                        'checked': false,
                    },
                    {
                        'label': '携带宠物',
                        'value1': '1',
                        'checked': false,
                        
                    },
                    {
                        'label': '抽烟',
                        'value1': '2',
                        'checked': false
                    },
                    
                ]
                scope.changepark = function(aaa){
                    console.log(aaa);
                    if(aaa == true){
                        scope.info.hotel_parking = '0';
                    } else {
                        scope.info.hotel_parking = '1';
                    }
                }
                scope.changepet = function(aaa){
                    if(aaa == true){
                        scope.info.hotel_room_pet = '0';
                    } else {
                        scope.info.hotel_room_pet = '1';
                    }
                }
                scope.changesmoke = function(aaa){
                    if(aaa == true){
                        scope.info.hotel_room_smoke = '0';
                    } else {
                        scope.info.hotel_room_smoke = '1';
                    }
                }
                if(scope.placeobj.id != ''){
                    $resource('/api/ac/tmc/tourismPlaceHotelService/findInfoById', {}, {}).save({'id' : scope.placeobj.id}, function (res) {
                        if (res.errcode !== 0) {
                            if(res.errcode === 10010){
                                toaster.warning({ title: "提示", body: "还没添加信息,请先填写信息" });
                            } else {
                                toaster.error({ title: "提示", body: res.errmsg });
                            }
                            return;
                        }
                        console.log(res);
                        scope.info = res.data;
                        console.log(res.data.hotel_back_room);
                        scope.vm1.date = res.data.hotel_back_room;
                        if(res.data.hotel_parking == 0){
                            
                        }
                        if(res.data.hotel_room_pet == 0){
                            
                        }
                        if(res.data.hotel_room_smoke == 0){
                            
                        }
                    });
                }
                scope.save = function(){
                    console.log('保存');
                    if (typeof scope.vm1.date._d === 'string') {
                        scope.vm1.date._d = scope.vm1.date._d;
                    } else {
                        scope.vm1.date._d = time2str(scope.vm1.date._d);
                    }
                    scope.info.hotel_back_room = (scope.vm1.date._d);
                    $resource('/api/ac/tmc/tourismPlaceHotelService/create', {}, {}).save(scope.info, function (res) {
                        if (res.errcode !== 0) {
                            toaster.error({ title: "提示", body: res.errmsg });
                            return;
                        }
                        toaster.success({ title: "提示", body: "保存成功" });
                        $state.go('app.touristlist');
                        console.log(scope.placeobj.id);
                    });
                }
                console.log(scope.placeobj);
               

                function time2str(objDate) {
                    if(angular.isDate(objDate))
                    {
                        var y = objDate.getFullYear();
                        var m = objDate.getMonth();
                        var d = objDate.getDate();
                        var h = objDate.getHours();
                        var mt = objDate.getMinutes();
                        // var s = objDate.getSeconds();
                        return y + '-' + (m + 1) + '-' + d + ' ' + h + ':' + mt;
                    }
                    else
                    {
                        return '错误格式';
                    }
                }
            }
        };
    };
    
    