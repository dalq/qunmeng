module.exports = function ($resource, $state, $http, $q, FileUploader, toaster) {
    
        return {
    
            restrict: 'AE',
            template: require('../views/clothinginfo.html'),
            replace: true,
            scope: {
                'placeobj': '=',
                'funobj': '=',
                'baseinfo': '=',
                'util': '=',
            },
            link: function (scope, elements, attrs) {
                scope.info = {
                    'place_id': scope.placeobj.id,
                    'recommendation' : ''
                };
    
                if(scope.placeobj.id != ''){
                    $resource('/api/ac/tmc/tourismPlaceShopService/findInfoById', {}, {}).save({'id' : scope.placeobj.id}, function (res) {
                        if (res.errcode !== 0) {
                            if(res.errcode === 10010){
                                toaster.warning({ title: "提示", body: "还没添加信息,请先填写信息" });
                            } else {
                                toaster.error({ title: "提示", body: res.errmsg });
                            }
                            return;
                        }
                        console.log(res);
                        scope.info.recommendation = res.data.recommendation;
                    });
                }
                scope.save = function(){
                    console.log('保存');
                    $resource('/api/ac/tmc/tourismPlaceShopService/create', {}, {}).save(scope.info, function (res) {
                        if (res.errcode !== 0) {
                            toaster.error({ title: "提示", body: res.errmsg });
                            return;
                        }
                        toaster.success({ title: "提示", body: "保存成功" });
                        console.log(scope.placeobj.id);
                    });
                }
                console.log(scope.placeobj);
               
            }
        };
    };
    
    