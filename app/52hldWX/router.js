/**
* 子模块路由
* dlq
*/

var router = function ($urlRouterProvider, $stateProvider) {
    
    
         $stateProvider
    
            //场所列表
            .state('app.touristlist', {
                url: "/hldWX/touristlist/:id",
                views: {
                    'main@': {
                        template: require('./views/touristlist.html'),
                        controller: 'touristlist',
                    }
                },
                resolve: {
                   
                }
            })

            // 类别tab
            .state('app.alltype', {
                url: "/hldWX/alltype/:state",
                views: {
                    'main@': {
                        template: require('./views/alltype.html'),
                        controller: 'alltype',
                    }
                },
                resolve: {
                    date2str: function (utilservice) {
                        return utilservice.date2str;
                    },
                    str2date: function (utilservice) {
                        return utilservice.str2date;
                    },
                }
            })
            // 菜品列表
            .state('app.dishlist', {
                url: "/hldWX/dishlist/:id",
                views: {
                    'main@': {
                        template: require('./views/dishlist.html'),
                        controller: 'dishlist',
                    }
                },
                resolve: {
                    date2str: function (utilservice) {
                        return utilservice.date2str;
                    },
                    str2date: function (utilservice) {
                        return utilservice.str2date;
                    },
                }
            })
            // 创建场所
            .state('app.addPlace_view', {
                url: "/hldWX/addPlace_view/:id/:type/:code",
                views: {
                    'main@': {
                        template: require('./views/addPlace_view.html'),
                        controller: 'addPlace_view',
                    }
                },
                resolve: {
                    productid: function () {
                        return '';
                    },
                    what: function () {
                        return 'edit';
                    },
                    date2str: function (utilservice) {
                        return utilservice.getDate;
                    },
                    str2date: function (utilservice) {
                        return utilservice.str2date;
                    },
                    $modalInstance: function () {
                        return undefined;
                    },
                    auditing: function () {
                        return false;
                    }
                }
            }) // addtype
            // 添加类别
            .state('app.addplacetype', {
                url: "/hldWX/addplacetype/:id/:name/:type/:flag",
                views: {
                    'main@': {
                        template: require('./views/addplacetype.html'),
                        controller: 'addplacetype',
                    }
                },
                resolve: {
                    
                }
            })

            // 添加类别
            .state('app.evaluate', {
                url: "/hldWX/evaluate",
                views: {
                    'main@': {
                        template: require('./views/evaluate.html'),
                        controller: 'evaluate',
                    }
                },
                resolve: {
                    
                }
            })
            // 线路列表
            .state('app.hldlinelist', {
                url: "/hldWX/hldlinelist",
                views: {
                    'main@': {
                        template: require('./views/hldlinelist.html'),
                        controller: 'hldlinelist',
                    }
                },
                resolve: {
                    
                }
            })
            // 游记列表
            .state('app.travelnotelist', {
                url: "/hldWX/travelnotelist",
                views: {
                    'main@': {
                        template: require('./views/travelnotelist.html'),
                        controller: 'travelnotelist',
                    }
                },
                resolve: {
                    
                }
            })
            // 投诉列表
            .state('app.complaintList', {
                url: "/hldWX/complaintList",
                views: {
                    'main@': {
                        template: require('./views/complaintList.html'),
                        controller: 'complaintList',
                    }
                },
                resolve: {
                    
                }
            })

};

module.exports = router;