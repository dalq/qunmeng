

var router = function ($urlRouterProvider, $stateProvider) {


    $stateProvider


        //订单列表
        .state('app.qmorderlist', {
            url: "/qm/orderlist.html",
            views: {
                'main@': {
                    template: require('./views/orderlist.html'),
                    controller: 'qmorderlist',
                }
            },
            resolve: {
                date2str: function (utilservice) {
                    return utilservice.getDate;
                },
                str2date: function (utilservice) {
                    return utilservice.str2date;
                },
        
            }
        })


        //弹出订单详情
        .state('app.qmorderinfo', {
            url: "/qm/order/:id",
            size: 'lg',
            template : require('./views/orderinfo.html'),
            controller : 'qmorderinfo',
            resolve:{
                // formconfig : function(formservice){
                //     return formservice.formconfig();
                // },
                // model : function(weshopservice){
                //     return weshopservice.producttypemodel;
                // }
            }
        })


       

       


        ;

};

module.exports = router;