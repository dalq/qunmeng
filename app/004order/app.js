var App = angular.module('qmorder', []);

App.config(require('./router'));
//App.factory('shopservice', require('./service'));

App.controller('qmorderlist',require('./controllers/orderlist'));
App.controller('qmorderinfo',require('./controllers/orderinfo'));
App.controller('qmdevicelist',require('./controllers/devicelist'));
App.controller('qmbackticket',require('./controllers/backticket'));
// App.controller('qmaddplace',require('./controllers/addplace'));
// App.controller('viewinfo',require('./controllers/info'));
// App.controller('viewedit',require('./controllers/edit'));

//App.directive('qmorderlist',require('./directives/orderlist'));
// App.directive('book',require('./directives/book'));
// App.directive('cancellation',require('./directives/cancellation'));





// App.controller('list',require('./controllers/list'));

module.exports = App;