module.exports = function($scope, code, max, $modalInstance, $resource){

    $scope.num = 0;

    

	// //退票按钮
    $scope.ok = function () {

    	if(!angular.isNumber($scope.num) || $scope.num <= 0)
    	{
    		alert('请输入正确数字');
    		return;
    	}

    	if($scope.num > max){
    		alert('最多退' + max + '张');
    		return;
    	}

		var para = {
			'order_code' : code,
			'back_count' : $scope.num,
		};

		$resource('/api/ac/wc/newProductOrderService/createBackOrder', {}, {}).save(para, function (res) {

	        console.log(res);

	        if(res.errcode === 0)
	        {
	            if(res.data.result == '1'){
	                alert('退票成功');
	            } else if(res.data.result == '2') {
	                alert('退票申请已提交，待审核');
	            } else if(res.data.result == '3') {
	                alert(res.data.remark_err);
	            }
	            $modalInstance.close();
	        }
	        else
	        {
	            alert(res.errmsg);
	        }
	    });
	};


	$scope.cancel = function () {
		$modalInstance.dismiss('cancel');
	};

};