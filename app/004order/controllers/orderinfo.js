module.exports = function($scope, obj, $resource, $modalInstance, $modal ){

	console.log(obj);


	var label = {
		'back_state' : ['未退票', '已退票', '退票失败', '', '', '待审核'],
		'refund_state' : ['未退款', '已退款'],

	}



	var pay_state_label = ['未支付', '已支付'];
	obj.pay_state_label = pay_state_label[parseInt(obj.pay_state)];

	var payment_type_label = ['', '支付宝支付', '微信支付'];
	obj.payment_type_label = payment_type_label[parseInt(obj.payment_type)];

	$scope.obj = obj;

	$scope.ticketcode = '';

	function load(){

		//$resource('/api/as/tc/ticket2/orderticketlist', {}, {}).get(
		//$resource('/api/as/tc/ticket2/wdorderticketlist', {}, {}).get(
		$resource('/api/as/wc/newproductorder/ticketlist', {}, {}).get(
			{'order_code' : obj.code}, function (res) {

		    console.log('票种列表');
		    console.log(res);

		    if (res.errcode !== 0) {
		        alert("数据获取失败");
		        return;
		    }


		    var tkt = {};
		    var restkt = [];

		    for(var i = 0, j = res.data.length; i < j; i++)
	        {
	            var tt = res.data[i];

	            //---- 保存订单唯一码 和 失败原因 ------------//
	        	if(i == 0){
	        		$scope.ticketcode = tt.order_unique_code;
	        		
	        		console.log($scope.ticketcode);
	        	}
	        	//---- 保存订单唯一码  和 失败原因 ------------//

	            var v = tt.sequence;

	            if(!tkt.hasOwnProperty(v))
	            {
	                tkt[v] = new Object();
	                tkt[v].ticketarr = new Array();
	                tkt[v].sequence = tt.sequence;
	                tkt[v].name = tt.order_name;
	                tkt[v].newdate = tt.take_effect_time;
	            }
	            tkt[v].ticketarr.push(tt);
	        }

	        for(var key in tkt)
	        {
	            var o = tkt[key];
	            restkt.push(o);
	        }

	        console.log(restkt);

	        $scope.obj.tkt = restkt;

	    });

		$scope.backarr = [];
	    //$resource('/api/as/tc/ticketorderback/orderbacklist', {}, {}).get(
	    $resource('/api/as/wc/newproductorderback/orderbacklist', {}, {}).get(
			{'order_code' : obj.code}, function (res) {

		    console.log(res);

		    for(var i = 0; i < res.data.length; i++){
		    	var tmp = res.data[i];
		    	tmp['back_state_label'] = label.back_state[parseInt(tmp.back_state)];
		    	tmp['refund_state_label'] = label.refund_state[parseInt(tmp.refund_state)];
		    }

		    if (res.errcode !== 0) {
		        alert(res.errmsg);
		        return;
		    }

		    $scope.backarr = res.data;

	    });
	}
	load();
    
    $scope.back = function(ticket){

    	var flag = true;
        console.log(ticket);
        for(var i = 0; i < ticket.ticketarr.length; i++)
        {
            var tmp = ticket.ticketarr[i];
            if(tmp.state !== '1')
            {
                flag = false;
            }
        }

        if(!flag)
        {
            alert('销售品中有已经使用的商品。');
            return;
        }

        console.log(ticket);

        var para = {
            'order_code' : obj.ticket_order_code,
            'sequence' : ticket.sequence
        };

        if (!confirm("确定要退 " + ticket.name + ' 中的第 ' + ticket.sequence + ' 个吗？')) {
            return false;
        }

        

        $resource('/api/uc/wc/newProductOrderService/createBackOrder', {}, {}).save(para, function (res) {

        	console.log(res);

		    if(res.errcode === 0)
            {
                if(res.data.result == '1'){
					alert('退票成功');
				} else if(res.data.result == '2') {
					alert('退票申请已提交，待审核');
				} else if(res.data.result == '3') {
					alert(res.data.remark_err);
				}
                load();
            }
            else
            {
                alert(res.errmsg);
            }

	    });

    };

    //核销
    $scope.cancellation = function(ticket){

    	console.log(ticket);

    	var obj = {
    		'code' : ticket.code,
    		'place_code' : ticket.place_code,
    	};

    	var modalInstance = $modal.open({
          template: require('../views/pop_devicelist.html'),
          controller: 'qmdevicelist',
          //size: 'lg',
          resolve: {
            obj : function(){
                return obj;
            }
          }
        });

        modalInstance.result.then(function () {
        	load();
        }, function () {
            
        });




    };



    $scope.cancel = function(){

		$modalInstance.close();

	}

};