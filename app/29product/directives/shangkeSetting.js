module.exports = function($resource, $state, $http, $q, toaster){

	return {

		restrict : 'AE',
		template : require('../views/shangkeSetting.html'),
		replace:true,
		scope:{
			'productobj' : '=',
			'funobj' : '=',
		},
		link : function(scope, elements, attrs){
			var code = '';
			var productflag = scope.productobj.productflag;
			var guide_price = scope.productobj.guide_price;
			var cost_price = scope.productobj.cost_price;
			var sale_name = scope.productobj.name;
			if(scope.funobj.item.line_product_id != undefined){
				var line_product_id =  scope.funobj.item.line_product_id;  
				code = line_product_id;
			} else if(scope.funobj.item != ''){
				var line_product_id = scope.funobj.item.id;
				code = line_product_id;
			} else {
				code = scope.productobj.code;
				
			}
			scope.salefrobj = {
				'share_title' : '',
				'line_product_id' : line_product_id,
				'sale_name' : sale_name,
				'sale_code' : code,
				'rebate_lower' : 0,
				'profit_ratio' : 10.00,
				'profit' : '',
				'rebate_unlimited' : 0,
				'merchant_make_appointment' : '0',
				'recommend' : '1',
				'hot_search' : '0',
				'timely_profit' : '1',
				'goods_type' : 'L',
				'adult_rebate_unlimited' : 0,
				'adult_rebate_lower' : 0,
				'timely_integral' : '0'
				// 'sale_cost_price' : cost_price,
				// 'sk_price' : guide_price
				
			}
			scope.profit = {
				'flag' : '',
				'productflag' : ''
			}
			scope.sss = {
				'type' : '1'
			}
			if(productflag == 'productflag'){
			    scope.profit.productflag = '0';
				scope.salefrobj.goods_type = 'T';
			} else {
			    scope.profit.productflag = '1';  
			}
			$resource('/api/us/mc/mertradetypedao/findByTypeList', {}, {})
			.save({'type' : 'cheap_menu'},function(res){
			    if(res.errcode !== 0)
			    {
			        toaster.error({title:"",body:res.errmsg});
			        return;
			    }
			    scope.typelist = res.data;
			    // $modalInstance.close();
			    // $state.go('app.supplyProductList');

			}) 
			scope.changeRadio = function(type){
			    if(type == 1){
			        scope.profit.flag = '0';
			    } else if(type == 2){
			        scope.profit.flag = '1';
			    }

			}
			scope.saleobj = {
			    'guide_price' : guide_price,
			    'cost_price' : cost_price
			}
			scope.obj = {
			    'isSelected' : false // 搜索
			}

			scope.yyobj = {
			    'isSelected' : false // 预约
			}
			scope.listobj = {
			    'isSelected' : false // 首页
			}
			scope.profitobj = {
			    'isSelected' : false // 首页
			}
			scope.cardobj = {
			    'isSelected' : false
			}
			scope.integralobj = {
			    'isSelected' : true
			}
			scope.change = function(){
			    scope.salefrobj.profit = ((scope.saleobj.guide_price - scope.saleobj.cost_price) * (1-scope.salefrobj.profit_ratio *0.01)).toFixed(2);				
			    scope.salefrobj.erjiprofit = ((scope.saleobj.guide_price - scope.saleobj.cost_price)*scope.salefrobj.profit_ratio*0.01).toFixed(2);
			    scope.profit.flag = '0';
			}
			scope.changeJe = function(){
			    scope.salefrobj.profit_ratio = ((scope.salefrobj.erjiprofit / (scope.saleobj.guide_price - scope.saleobj.cost_price) * 100)).toFixed(2);
			    scope.salefrobj.profit = ((scope.saleobj.guide_price - scope.saleobj.cost_price) * (1-scope.salefrobj.profit_ratio *0.01)).toFixed(2);								
			    scope.profit.flag = '1';	
							
			}
			// scope.change1 = function(){
			// }
			var idcode = '';
			if(line_product_id){
			    idcode = line_product_id;
			} else {
			    idcode = code;
			}
			$resource('/api/as/mc/mergoods/getGoodsInfoByCode', {}, {})
			.save({'sale_code' : idcode}, function(res){
			    if(res.errcode === 0){
					if(scope.funobj.item != ''){
						scope.productobj.flag = 'no';
					} else {
						scope.productobj.flag = 'yes';
					}
					scope.productobj.aaa = res.data.id;
					scope.salefrobj = res.data;
					scope.salefrobj.sale_name = sale_name;
			        scope.salefrobj.line_product_id = line_product_id;
			        scope.salefrobj.sale_cost_price = res.data.sale_cost_price / 100;
			        scope.salefrobj.sk_price = res.data.sk_price / 100;
			        scope.salefrobj.rebate_lower = res.data.rebate_lower / 100;
			        scope.salefrobj.rebate_unlimited = res.data.rebate_unlimited / 100;
			        // angular.extend(scope.salefrobj, res.data);
			        if(scope.salefrobj.hot_search == 1){
			            scope.obj.isSelected = true;
			        } else if(scope.salefrobj.hot_search == 0){
			            scope.obj.isSelected = false;
			        }
			        if(scope.salefrobj.merchant_make_appointment == 1){
			            scope.yyobj.isSelected = true;
			        } else if(scope.salefrobj.merchant_make_appointment == 0){
			            scope.yyobj.isSelected = false;
			        }
			        if(scope.salefrobj.recommend == 0){
			            scope.listobj.isSelected = true;
			        } else if(scope.salefrobj.recommend == 1){
			            scope.listobj.isSelected = false;
			        } 
			        if(scope.salefrobj.timely_profit == 0){
			            scope.profitobj.isSelected = true;
			        } else if(scope.salefrobj.timely_profit == 1){
			            scope.profitobj.isSelected = false;
			        } 

			        if(scope.salefrobj.goods_type === 'C'){
			            scope.cardobj.isSelected = true;
			        } else if(scope.salefrobj.goods_type === 'T'){
			            scope.cardobj.isSelected = false;
			        } 

			         if(scope.salefrobj.timely_integral === '0'){
			            scope.integralobj.isSelected = true;
			        } else if(scope.salefrobj.timely_integral === '1'){
			            scope.integralobj.isSelected = false;
			        } 


					
					scope.salefrobj.profit_ratio =scope.salefrobj.profit_ratio.toFixed(2);
			        scope.salefrobj.profit = ((scope.saleobj.guide_price - scope.saleobj.cost_price) * (1-scope.salefrobj.profit_ratio * 0.01)).toFixed(2);
			        scope.salefrobj.erjiprofit = ((scope.saleobj.guide_price - scope.saleobj.cost_price)*scope.salefrobj.profit_ratio*0.01).toFixed(2);
			        scope.salefrobj.rebate_unlimited = parseFloat(scope.salefrobj.rebate_unlimited);

			        if(scope.salefrobj.rebate_unlimited == 0){
			            (scope.salefrobj.rebate_unlimited) = parseFloat(scope.salefrobj.profit);
						
			        }	
					
			    } else if(res.errcode === 10003){
					scope.productobj.flag = 'no';					
			        scope.salefrobj.profit = ((scope.saleobj.guide_price - scope.saleobj.cost_price) * (1-scope.salefrobj.profit_ratio * 0.01)).toFixed(2);
			        scope.salefrobj.erjiprofit = ((scope.saleobj.guide_price - scope.saleobj.cost_price)*scope.salefrobj.profit_ratio*0.01).toFixed(2);
			        if(scope.salefrobj.rebate_unlimited == 0){
			            scope.salefrobj.rebate_unlimited = parseFloat(scope.salefrobj.profit);
			        } 
			    }else{
			        toaster.error({title:"",body:res.errmsg});
			    }	
							
				// scope.salefrobj = res.data;

			});

			// 		// scope.salefrobj.hot_search = scope.obj.isSelected;
			scope.onChange = function(isSelected){
				if(isSelected == true) {
					scope.salefrobj.hot_search = '1'; 
				} else {
					scope.salefrobj.hot_search = '0';
				}
			}

			scope.onYYChange = function(isSelected){
				if(isSelected == true) {
					scope.salefrobj.merchant_make_appointment = '1'; 
				} else {
					scope.salefrobj.merchant_make_appointment = '0';
				}
			}

            scope.onListChange = function(isSelected){
				if(isSelected == true) {
					scope.salefrobj.recommend = '0'; 
				} else {
					scope.salefrobj.recommend = '1';
				}
			}
            scope.profitChange = function(isSelected){
				if(isSelected == true) {
					scope.salefrobj.timely_profit = '0'; 
				} else {
					scope.salefrobj.timely_profit = '1';
				}
			}
            scope.cardChange = function(isSelected){
				if(isSelected == true) {
					scope.salefrobj.goods_type = 'C'; 
				} else {
					scope.salefrobj.goods_type = 'T';
				}
			}
             scope.integralChange = function(isSelected){
				if(isSelected == true) {
					scope.salefrobj.timely_integral = '0'; 
				} else {
					scope.salefrobj.timely_integral = '1';
				}
			}


            scope.saleFrSetSave = function(){
                // scope.salefrobj.sale_code = scope.saleobj.code;
				if(scope.salefrobj.profit_ratio==undefined){
					toaster.success({title:"",body:"利润率不能为负数"});
				} else if(scope.salefrobj.profit_ratio>100){
					toaster.success({title:"",body:"利润率不能大于100"});
				} else if(scope.salefrobj.rebate_unlimited==undefined || scope.salefrobj.rebate_lower==undefined){
					toaster.success({title:"",body:"红包金额不能为负数"});
				} else if(scope.salefrobj.rebate_lower > scope.salefrobj.rebate_unlimited){
					toaster.success({title:"",body:"红包下限不能大于红包上限"});
				} else if(scope.salefrobj.rebate_lower>scope.salefrobj.profit){
					toaster.success({title:"",body:"红包下限不能大于利润"});
				} else if(scope.salefrobj.rebate_unlimited>scope.salefrobj.profit){
					toaster.success({title:"",body:"红包上限不能大于利润"});			
				} else if(scope.salefrobj.max_integral > scope.saleobj.guide_price*100){
					toaster.error({title:"",body:"最高可用积分不能大于售价"});			                    
                } else {
					$resource('/api/ac/mc/merplaceserviceimpl/createSKProprietaryInformation', {}, {})
                    .save(scope.salefrobj,function(res){
                        if(res.errcode !== 0)
                        {
                            toaster.error({title:"",body:res.errmsg});
                            return;
                        }
						toaster.success({title:"",body:"操作成功"});
						scope.productobj.flag = 'yes';
						scope.productobj.aaa = res.data.id;
                        // $state.go('app.supplyProductList');

                    }) 
				}
				
				
			};


        }
    }
}