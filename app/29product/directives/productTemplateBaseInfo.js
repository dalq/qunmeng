module.exports = function ($resource, $state, $http, $q, FileUploader, toaster, $modal) {
	return {
		restrict: 'AE',
		template: require('../views/productTemplateBaseInfo.html'),
		replace: true,
		scope: {
			'saleobj': '=',
			'template': '=',
			'templateBaseInfo': '=',
			'baseinfo': '=',
			'util': '='
		},
		link: function (scope, elements, attrs) {

			$resource('/api/as/tc/ticketsaletemplete/info', {}, {}).save({ id: scope.template.id }, function (res) {
				if (res.errcode != 0) {
					toaster.error({ title: "/api/as/tc/ticketsaletemplete/info", body: res.errmsg });
					return;
				}
				scope.templateBaseInfo = angular.copy(res.data);
				delete scope.templateBaseInfo.templete_lock_data_json;
				scope.templateBaseInfo.id = scope.template.id;

				var tempMap = {}
				var productMap = eval('(' + res.data.templete_lock_data_json + ')')
				for (var key in productMap) {
					(function (arg) {
						var templateStr = arg + '_flag';
						tempMap[templateStr] = true;
						scope.saleobj[arg] = productMap[arg];
						// if (scope.template[arg]) {
						// 	resJson[saleStr] = scope.saleobj[saleStr];
						// }
					})(key);//调用时参数  
				}
				var verification_modes_arr = scope.saleobj.verification_modes.split(',');
				for (var index = 0; index < verification_modes_arr.length; index++) {
					var element = verification_modes_arr[index];
					scope.saleobj['verification_modes_' + element] = true;
				}

				if (productMap.periodstart) {
					scope.baseinfo.dateshow.periodstart = {
						'date': {
							'label': scope.util.date2str(new Date(productMap.periodstart)),
							'value': scope.util.date2str(new Date(productMap.periodstart)),
							'opened': false
						}
					};
				}

				if (productMap.periodend) {
					scope.baseinfo.dateshow.periodend = {
						'date': {
							'label': scope.util.date2str(new Date(productMap.periodend)),
							'value': scope.util.date2str(new Date(productMap.periodend)),
							'opened': false
						}
					};
				}

				angular.extend(scope.template, tempMap)
			});

			//页面需要的信息。
			scope.page = {};

			var beforedata = {
				//类型列表
				'categorylist':
				$http({
					'method': 'GET',
					'url': '/api/as/sc/dict/dictbytypelist',
					'params': { 'type': 'sale_category' }
				}),
				//产品所属
				'salebelonglist':
				$http({
					'method': 'GET',
					'url': '/api/as/sc/dict/dictbytypelist',
					'params': { 'type': 'ticket_sale_belong' }
				}),
				//短信信息
				'smslist':
				$http({
					'method': 'GET',
					'url': '/api/as/tc/salesmstemplate/list'
				}),
			};

			$q.all(beforedata).then(function (res) {

				//分类信息
				if (res.categorylist.data.errcode === 0) {
				} else {
					toaster.errot({ title: "/api/as/sc/dict/dictbytypelist?type=sale_category", body: res.categorylist.data.errmsg })
					return;
				}
				//销售品所属列表
				if (res.salebelonglist.data.errcode === 0) {
				} else {
					toaster.errot({ title: "/api/as/sc/dict/dictbytypelist?type=ticket_sale_belong", body: res.salebelonglist.data.errmsg })
					return;
				}
				//短信列表
				if (res.smslist.data.errcode === 0) {
				} else {
					toaster.errot({ title: "/api/as/tc/salesmstemplate/list", body: res.smslist.data.errmsg })
					return;
				}

				//发短信时，默认短息模板里第一个短信模板。
				if (scope.saleobj.sms_type == '1') {
					scope.saleobj.sms_template_id = res.smslist.data.data[0].sms_template_id;
					scope.saleobj.sms_diy = res.smslist.data.data[0].sms_diy;
				}

				scope.page = {
					//分类列表
					'categorylist': res.categorylist.data.data,
					//产品所属
					'salebelonglist': res.salebelonglist.data.data,
					//短信列表
					'smslist': res.smslist.data.data,
					// //生效时间
					// 'take_effect_typearr': [
					// 	{ 'name': '次日', 'value': -1 },
					// 	{ 'name': '即时', 'value': 0 },
					// 	{ 'name': '1小时之后', 'value': 1 },
					// 	{ 'name': '2小时之后', 'value': 2 },
					// 	{ 'name': '3小时之后', 'value': 3 },
					// 	{ 'name': '4小时之后', 'value': 4 },
					// 	{ 'name': '5小时之后', 'value': 5 },
					// 	{ 'name': '6小时之后', 'value': 6 },
					// 	{ 'name': '7小时之后', 'value': 7 },
					// 	{ 'name': '8小时之后', 'value': 8 },
					// 	{ 'name': '9小时之后', 'value': 9 },
					// 	{ 'name': '10小时之后', 'value': 10 },
					// 	{ 'name': '11小时之后', 'value': 11 },
					// 	{ 'name': '12小时之后', 'value': 12 }
					// ],
				};

			});

			scope.save = function () {
				if (typeof scope.baseinfo.dateshow.periodstart.date.label === 'string') {
					scope.saleobj.periodstart = scope.baseinfo.dateshow.periodstart.date.label;
				} else {
					scope.saleobj.periodstart = scope.util.date2str(scope.baseinfo.dateshow.periodstart.date.label);
				}

				if (typeof scope.baseinfo.dateshow.periodend.date.label === 'string') {
					scope.saleobj.periodend = scope.baseinfo.dateshow.periodend.date.label;
				} else {
					scope.saleobj.periodend = scope.util.date2str(scope.baseinfo.dateshow.periodend.date.label);
				}

				var params = {};
				var resJson = {};
				for (var key in scope.template) {
					var result = (function (arg) {
						if (arg == 'id') {
							return true;
						}
						var saleStr = arg.substr(0, arg.length - 5);
						if (scope.template[arg]) {
							if (scope.saleobj[saleStr] === undefined || scope.saleobj[saleStr] === '' || scope.saleobj[saleStr] === null) {
								toaster.warning({ title: saleStr, body: '如需配置模板项,请填写默认值!' });
								return false;
							}
							resJson[saleStr] = scope.saleobj[saleStr];
						}
					})(key);//调用时参数  
					if (result === false) {
						return false;
					}
				}

				// 如身份证限制必填, 则保存号码
				if (scope.template['cardno_state_flag'] && (scope.saleobj['cardno_state'] == '1')) {
					resJson['cardno_limit'] = scope.saleobj['cardno_limit'];
				}

				if (resJson['address'] == '0') {
					resJson['vir_t_tkt_sale_ttype'] = { vir_type: '0' }
				} else if (resJson['address'] == '1') {
					resJson['vir_t_tkt_sale_ttype'] = { vir_type: '1' }
				} else if (resJson['address'] == '2') {
					resJson['vir_t_tkt_sale_ttype'] = { vir_type: '2' }
				} else {
					resJson['vir_t_tkt_sale_ttype'] = { vir_type: '0' }
				}

				params['templete_lock_data_json'] = JSON.stringify(resJson);

				var modalInstance = $modal.open({
					template: require('../views/productTemplateInfoEdit.html'),
					controller: 'productTemplateInfoEdit',
					url: '/productTemplateInfoEdit',
					size: 'lg',
					resolve: {
						template: function () {
							return scope.templateBaseInfo
						}
					}
				});
				modalInstance.opened.then(function () {// 模态窗口打开之后执行的函数  
				});
				modalInstance.result.then(function (result) {
					angular.extend(params, result)
					$resource('api/ac/tc/ticketSaleTempleteService/updateTemplete', {}, {}).save(params, function (res) {
						if (res.errcode != 0) {
							toaster.error({ title: "提示", body: res.errmsg });
							return;
						}
						toaster.success({ title: "(*^__^*) ……", body: '操作成功' });
						scope.util.$modalInstance.close();
					});
				}, function (reason) {
				});

			};

			scope.changeSms = function (obj) {
				var smsid = obj.sms_template_id;
				if (smsid == null) {
					obj.sms_diy = '';
				}
				else {
					for (var i = 0, j = scope.page.smslist.length; i < j; i++) {
						var tmp = scope.page.smslist[i];
						if (tmp.sms_template_id == smsid) {
							obj.sms_diy = tmp.sms_diy;
							break;
						}
					}
				}
			};
		}
	};
};

