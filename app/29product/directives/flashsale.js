module.exports = function($resource, $state, $http, $q, toaster){

	return {

		restrict : 'AE',
		template : require('../views/p_flashsale.html'),
		replace:true,
		scope:{
			'saleobj' : '=',
			'util' : '=',
		},
		link : function(scope, elements, attrs){

			scope.flashsale = {
				'what' : 'create',
			};
		    scope.flashsale.start = {};
		    scope.flashsale.start.date = {
				'lable': scope.util.date2str(new Date()),
				'opened': false
			}
		    scope.flashsale.start.h = '00';
		    scope.flashsale.start.m = '00';

		    scope.flashsale.end = {};
		    scope.flashsale.end.date = {
				'lable': scope.util.date2str(new Date()),
				'opened': false
			}
		    scope.flashsale.end.h = '23';
		    scope.flashsale.end.m = '59';
			scope.dateOpen = function ($event, item) {
				$event.preventDefault();
				$event.stopPropagation();
				item.opened = true;
			};
			scope.dateOpen1 = function ($event, item) {
				$event.preventDefault();
				$event.stopPropagation();
				item.opened = true;
			};
		    $resource('/api/as/tc/flashsale/info', {}, {})
		    .get({'sale_code' : scope.saleobj.code}, function(res){
		    	if(res.errcode === 0 ){
			        scope.flashsale.what = 'update';
			        scope.flashsale.start.date.lable = scope.util.date2str(scope.util.str2date(res.data.start_time));
					scope.flashsale.end.date.lable = scope.util.date2str(scope.util.str2date(res.data.end_time));
					scope.flashsale.start.h = res.data.start_h;
					scope.flashsale.start.m = res.data.start_m;
					scope.flashsale.end.h = res.data.end_h;
					scope.flashsale.end.m = res.data.end_m;
			    }else if(res.errcode === 10003){
			    	scope.flashsale.what = 'create';
			    }else{
					toaster.error({ title: "提示", body: res.sysaffirm.data.errmsg })
			        return ;
			    }
		    });


		    scope.save = function(){
				var url = '';
				if(scope.flashsale.what === 'create'){
					url = '/api/as/tc/flashsale/create';
				}else if(scope.flashsale.what === 'update'){
					url = '/api/as/tc/flashsale/update';
				}

				if (typeof scope.flashsale.start.date.lable === 'string') {
					scope.flashsale.start.date.lable = scope.flashsale.start.date.lable;
				} else {
					scope.flashsale.start.date.lable = scope.util.date2str(scope.flashsale.start.date.lable);
				}

				if (typeof scope.flashsale.end.date.lable === 'string') {
					scope.flashsale.end.date.lable = scope.flashsale.end.date.lable;
				} else {
					scope.flashsale.end.date.lable = scope.util.date2str(scope.flashsale.end.date.lable);
				}

				var para = {
					'start_time' : scope.flashsale.start.date.lable + 
								   ' ' + scope.flashsale.start.h +
								   ':' + scope.flashsale.start.m +
								   ':00',
					'end_time' :   scope.flashsale.end.date.lable + 
								   ' ' + scope.flashsale.end.h +
								   ':' + scope.flashsale.end.m +
								   ':00',
					'sale_code' : scope.saleobj.code
				};

				$resource(url, {}, {}).save(para, function(res){
					if(res.errcode != 0){
						toaster.error({ title: "提示", body: res.errmsg });
						return;
					}
					toaster.success({ title: "", body: '保存成功' });
					
				});
			};
		   
		}

	};
};

