module.exports = function($resource, $state, $http, $q, toaster){

	return {

		restrict : 'AE',
		template : require('../views/p_affirm.html'),
		replace:true,
		scope:{
			'saleobj' : '=',
			'util' : '=',
		},
		link : function(scope, elements, attrs){

			scope.page = {};
			scope.affirm = {
				'what' : 'create',		//create/update,
				'sysaffirm_sale_code' : scope.saleobj.code,
				'sysaffirm_target_code' : '',	//确认项
				'sysaffirm_target_goods_code' : '',	//商品id
				'sysaffirm_target_goods_back_type' : '0',	//退票类别
				'sysaffirm_target_goods_child_flag' : '0',	//成人/儿童
			};

			var beforedata = {
			    //确认列表
			    'affirmlist' :
			    $http({
			        'method' : 'GET', 
			        'url': '/api/as/sc/dict/dictbytypelist',
			        'params' : {'type' : 'ticket_sys_affirm'},

			    }),
			    //系统确认项的信息
			    'sysaffirm' :
			    $http({
			        'method' : 'GET', 
			        'url': '/api/as/tc/salesysaffirm/info',
			        'params' : {'sysaffirm_sale_code' : scope.saleobj.code},
			    }),
			};

			$q.all(beforedata).then(function(res){
			    if(res.affirmlist.data.errcode === 0){
			    }else{
					toaster.error({ title: "/api/as/sc/dict/dictbytypelist", body: res.affirmlist.data.errmsg });
			        return ;
			    }

			    if(res.sysaffirm.data.errcode === 0 ){
			        scope.affirm.what = 'update';
			        angular.extend(scope.affirm, res.sysaffirm.data.data);
			    }else if(res.sysaffirm.data.errcode === 10003){
			    	scope.affirm.what = 'create';
			    }else{
					toaster.error({ title: "/api/as/tc/salesysaffirm/info", body: res.sysaffirm.data.errmsg });
			        return ;
			    }

			    scope.page = {
			    	'affirmlist' : res.affirmlist.data.data,
			    }
			});


			scope.save = function(){
				if(scope.affirm.sysaffirm_target_code == '')
				{
					toaster.error({ title: "", body: '请选择对象' });
					return;
				}

				console.log(scope.affirm.sysaffirm_target_code);

				//如果是对接居游产品，居游产品不能是对接其他系统的。
				if (scope.affirm.sysaffirm_target_code == 'weshop_juyou'){
					$resource('/api/as/tc/sale/wdinfo', {}, {}).save({'code' : scope.affirm.sysaffirm_target_goods_code}, function (res) {
						console.log(res);
						if(res.errcode != 0){
							alert(res.errmsg);
							return;
						}
						if(res.data.sale_belong != 'juyou') {
							alert('只能是居游产品，不能是对接其他系统的产品');
							return;
						}
						var url = '';
						if (scope.affirm.what === 'create') {
							url = '/api/as/tc/salesysaffirm/create';
						} else if (scope.affirm.what === 'update') {
							url = '/api/as/tc/salesysaffirm/update';
						}
						$resource(url, {}, {}).save(scope.affirm, function (res) {
							if (res.errcode != 0) {
								toaster.error({ title: "", body: res.errmsg });
								return;
							}
							toaster.success({ title: "", body: '保存成功' });
						});
					});
				} else {
					var url = '';
					if (scope.affirm.what === 'create') {
						url = '/api/as/tc/salesysaffirm/create';
					} else if (scope.affirm.what === 'update') {
						url = '/api/as/tc/salesysaffirm/update';
					}
					$resource(url, {}, {}).save(scope.affirm, function (res) {
						if (res.errcode != 0) {
							toaster.error({ title: "", body: res.errmsg });
							return;
						}
						toaster.success({ title: "", body: '保存成功' });
					});
				}
				
				// var url = '';
				// if(scope.affirm.what === 'create'){
				// 	url = '/api/as/tc/salesysaffirm/create';
				// }else if(scope.affirm.what === 'update'){
				// 	url = '/api/as/tc/salesysaffirm/update';
				// }
				// $resource(url, {}, {}).save(scope.affirm, function(res){
				// 	if(res.errcode != 0){
				// 		toaster.error({ title: "提示", body: res.errmsg });
				// 		return;
				// 	}
				// 	toaster.success({ title: "", body: '保存成功' });
				// });
			};

			
		}

		




	};

	
		   
};

