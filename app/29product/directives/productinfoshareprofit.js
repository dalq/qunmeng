module.exports = function($resource, $state, $http, $q,toaster){
	return {

		restrict : 'AE',
		template : require('../views/productinfoshareprofit.html'),
		replace:true,
		scope:{
			'saleobj' : '=',
			'util' : '=',
		},
		link : function(scope, elements, attrs){
			scope.salefrobj = {
				'rebate_lower' : 0,
				'profit_ratio' : 0,
				'profit' : '',
				'rebate_unlimited' : 0,
				'merchant_make_appointment' : '0',
				'recommend' : '1',
				'hot_search' : '0',
				'timely_profit' : '1'
			}
			
			scope.obj = {
				'isSelected' : false
			}
			scope.yyobj = {
				'isSelected' : false
			}
			scope.listobj = {
				'isSelected' : false // 首页
			}
			scope.profitobj = {
				'isSelected' : false // 首页
			}
			
			scope.change = function(){
				scope.salefrobj.profit = ((scope.saleobj.guide_price - scope.saleobj.cost_price) * (1-scope.salefrobj.profit_ratio * 0.01)).toFixed(2);			
			}
			
			$resource('/api/as/mc/mergoods/getGoodsInfoByCode', {}, {})
			.save({'sale_code' : scope.saleobj.code}, function(res){

				if(res.errcode === 0){
					angular.extend(scope.salefrobj, res.data);
					//scope.salefrobj.rebate_unlimited == scope.salefrobj.profit;
					scope.salefrobj.rebate_lower = res.data.rebate_lower / 100;
            		scope.salefrobj.rebate_unlimited = res.data.rebate_unlimited / 100;
					if(scope.salefrobj.recommend == 0){
						scope.listobj.isSelected = true;
					} else if(scope.salefrobj.recommend == 1){
						scope.listobj.isSelected = false;
					}
					if(scope.salefrobj.hot_search == 1){
						scope.obj.isSelected = true;
					} else if(scope.salefrobj.hot_search == 0){
						scope.obj.isSelected = false;
					}

					if(scope.salefrobj.timely_profit == 0){
						scope.profitobj.isSelected = true;
					} else if(scope.salefrobj.timely_profit == 1){
						scope.profitobj.isSelected = false;
					}

					if(scope.salefrobj.merchant_make_appointment == 1){
						scope.yyobj.isSelected = true;
					} else if(scope.salefrobj.merchant_make_appointment == 0){
						scope.yyobj.isSelected = false;
					}

					

					scope.salefrobj.profit = ((scope.saleobj.guide_price - scope.saleobj.cost_price) * (1-scope.salefrobj.profit_ratio * 0.01)).toFixed(2);
					if(scope.salefrobj.rebate_unlimited == 0){
						scope.salefrobj.rebate_unlimited = parseInt(scope.salefrobj.profit);
					}	
					
				}else if(res.errcode === 10003){
					scope.salefrobj.profit = ((scope.saleobj.guide_price - scope.saleobj.cost_price) * (1-scope.salefrobj.profit_ratio * 0.01)).toFixed(2);
					if(scope.salefrobj.rebate_unlimited == 0){
						scope.salefrobj.rebate_unlimited = parseInt(scope.salefrobj.profit);
					}	
				}else{
					toaster.success({title:"",body:res.errmsg});
				}	
							
			});


			
			scope.salefrobj.search_type = scope.obj.isSelected;
			scope.onChange = function(isSelected){
				if(isSelected == true) {
					scope.salefrobj.search_type = '0'; 
				} else {
					scope.salefrobj.search_type = '1';
				}
			}
			scope.onYYChange = function(isSelected){
				if(isSelected == true) {
					scope.salefrobj.merchant_make_appointment = '1'; 
				} else {
					scope.salefrobj.merchant_make_appointment = '0';
				}
			}
		
            scope.saleFrSetSave = function(){
                scope.salefrobj.sale_code = scope.saleobj.code;
				if(scope.salefrobj.profit_ratio==undefined){
					toaster.success({title:"",body:"利润率不能为负数"});
				} else if(scope.salefrobj.profit_ratio>100){
					toaster.success({title:"",body:"利润率不能大于100"});
				} else if(scope.salefrobj.rebate_unlimited==undefined || scope.salefrobj.rebate_lower==undefined){
					toaster.success({title:"",body:"红包金额不能为负数"});
				} else if(scope.salefrobj.rebate_lower > scope.salefrobj.rebate_unlimited){
					toaster.success({title:"",body:"红包下限不能大于红包上限"});
				} else if(scope.salefrobj.rebate_lower>scope.salefrobj.profit){
					toaster.success({title:"",body:"红包下限不能大于利润"});
				} else {
					$resource('/api/as/tc/saleshangkeprice/save', {}, {})
                    .save(scope.salefrobj,function(res){
                        if(res.errcode !== 0)
                        {
                            toaster.success({title:"",body:res.errmsg});
                            return;
                        }
						toaster.success({title:"",body:"操作成功"});

                    })
				}
				
			};

			// 基本信息
		   
		}

	};
};

