module.exports = function ($resource, $state, $http, $q, FileUploader, toaster, $modal) {

	return {

		restrict: 'AE',
		template: require('../views/productTemplateImageInfo.html'),
		replace: true,
		scope: {
			'saleobj': '=',
			'template': '=',
			'templateBaseInfo': '=',
			'funobj': '=',
			'baseinfo': '=',
			'util': '=',
		},
		link: function (scope, elements, attrs) {

			scope.save = function () {

				if (typeof scope.baseinfo.dateshow.periodstart.date.label === 'string') {
					scope.saleobj.periodstart = scope.baseinfo.dateshow.periodstart.date.label;
				} else {
					scope.saleobj.periodstart = scope.util.date2str(scope.baseinfo.dateshow.periodstart.date.label);
				}

				if (typeof scope.baseinfo.dateshow.periodend.date.label === 'string') {
					scope.saleobj.periodend = scope.baseinfo.dateshow.periodend.date.label;
				} else {
					scope.saleobj.periodend = scope.util.date2str(scope.baseinfo.dateshow.periodend.date.label);
				}

				var params = {};
				var resJson = {};
				for (var key in scope.template) {
					var result = (function (arg) {
						if(arg == 'id'){
							return true;
						}
						var saleStr = arg.substr(0, arg.length - 5);
						if (scope.template[arg]) {
							if (scope.saleobj[saleStr] === undefined || scope.saleobj[saleStr] === '' || scope.saleobj[saleStr] === null) {
								toaster.warning({ title: saleStr, body: '如需配置模板项,请填写默认值!' });
								return false;
							}
							resJson[saleStr] = scope.saleobj[saleStr];
						}
					})(key);//调用时参数  
					if (result === false) {
						return false;
					}
				}
				
				// 如身份证限制必填, 则保存号码
				if (scope.template['cardno_state_flag'] && (scope.saleobj['cardno_state'] == '1')) {
					resJson['cardno_limit'] = scope.saleobj['cardno_limit'];
				}

				if (resJson['address'] == '0') {
					resJson['vir_t_tkt_sale_ttype'] = { vir_type: '0' }
				} else if (resJson['address'] == '1') {
					resJson['vir_t_tkt_sale_ttype'] = { vir_type: '1' }
				} else {
					resJson['vir_t_tkt_sale_ttype'] = { vir_type: '0' }
				}

				params['templete_lock_data_json'] = JSON.stringify(resJson);

				var modalInstance = $modal.open({
					template: require('../views/productTemplateInfoEdit.html'),
					controller: 'productTemplateInfoEdit',
					url: '/productTemplateInfoEdit',
					size: 'lg',
					resolve: {
						template: function(){
							return scope.templateBaseInfo
						}
					}
				});
				modalInstance.opened.then(function () {// 模态窗口打开之后执行的函数  
				});
				modalInstance.result.then(function (result) {
					angular.extend(params, result)
					$resource('api/ac/tc/ticketSaleTempleteService/updateTemplete', {}, {}).save(params, function (res) {
						if (res.errcode != 0) {
							toaster.error({ title: "提示", body: res.errmsg });
							return;
						}
						toaster.success({ title: "(*^__^*) ……", body: '操作成功' });
						scope.util.$modalInstance.close();
					});
				}, function (reason) {
				});

			};

		}

	};
};

