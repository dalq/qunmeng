module.exports = function ($resource, $state, $http, $q, FileUploader, toaster) {

	return {

		restrict: 'AE',
		template: require('../views/restrictrule.html'),
		replace: true,
		scope: {
			'saleobj': '=',
			'funobj': '=',
			'baseinfo': '=',
			'util': '=',
		},
		link: function (scope, elements, attrs) {

			var obj = {
				'id': scope.saleobj.id,
				// 'name' : '',
				// 'code' : '',
				// 'market_price' : 0,
				// 'guide_price' : 0,
				// 'cost_price' : 0,
				// 'sale_category' : 'F10',
				// 'sms_template_id' : '',
				// 'sms_diy' : '',
				// 'sms_type' : '1',
				// 'top_pic' : '',
				// 'logo' : '',
				// 'periodstart' : '',
				// 'periodend' : '',
				// 'sale_belong' : 'juyou',	//产品所属
				// 'sys_affirm_type' : '1',	//系统确认
				'price_type' : '0',
				'pay_type': '0', 	//支付类型
				//'stock_type': '0',	//库存类型
				'current_stock_num': 0,
				'sale_target_type': '0',	//销售目标
				'take_effect_type': 0,	//生效时间
				'max_limit': 0,	//最大购买数量
				'order_num_limit': 0,	//每单最大购买数量限制
				'tour_date_type': '0',	//是否启用出游时间
				'back_type': '0',	//是否允许退票
				'apply_state': '0',	//退票是否需要审核
				'ticket_type': '0',	//是否出票
				'user_status': '0',	//是否实名制
				'cardno_state': '0',	//是否必填
				'cardno_limit': '',	//是否必填
				'sms_ticketcode_type': '0',	//短信票码类型
				'over_period_off': '0',	//过期自动退票
				'over_period_off_mobile': '0',	//过期自动下架通知手机号
				// 'bookingnotes' : '',	//团产品预订须知
				// 'detail' : '',	//销售品简介
				'verification_modes': 'ID,card,ticket_code,order_code',	//销售品简介
			};

			//页面需要的信息。
			scope.page = {
				'take_effect_typearr': [
					{ 'name': '次日', 'value': -1 },
					{ 'name': '即时', 'value': 0 },
					{ 'name': '1小时之后', 'value': 1 },
					{ 'name': '2小时之后', 'value': 2 },
					{ 'name': '3小时之后', 'value': 3 },
					{ 'name': '4小时之后', 'value': 4 },
					{ 'name': '5小时之后', 'value': 5 },
					{ 'name': '6小时之后', 'value': 6 },
					{ 'name': '7小时之后', 'value': 7 },
					{ 'name': '8小时之后', 'value': 8 },
					{ 'name': '9小时之后', 'value': 9 },
					{ 'name': '10小时之后', 'value': 10 },
					{ 'name': '11小时之后', 'value': 11 },
					{ 'name': '12小时之后', 'value': 12 }
				],
			};

			scope.save = function () {
				scope.concatVerification_modes();
				var para = {};
				for (var key in obj) {
					para[key] = scope.saleobj[key];
				}


				//------- 价格日历模式的时候，库存设置成每日库存 --------------//
				if(para['price_type'] == '1'){
					para['stock_type'] = '2';
				}
				//------- 价格日历模式的时候，库存设置成每日库存 --------------//
				$resource('/api/as/tc/sale/update', {}, {}).save(para, function (res) {
					if (res.errcode != 0) {
						toaster.error({ title: "提示", body: res.errmsg });
						return;
					}
					if (angular.isDefined(res.data.uuid)) {
						scope.saleobj.id = res.data.uuid;
					}
					toaster.success({ title: "", body: '操作成功' });
					if (scope.util.$modalInstance == undefined) {
						$state.go('app.editproduct', { 'id': scope.saleobj.id });
					}
				});
			};

			scope.saveAndBack = function () {
				scope.concatVerification_modes();
				var para = {};
				for (var key in obj) {
					para[key] = scope.saleobj[key];
				}


				//------- 价格日历模式的时候，库存设置成每日库存 --------------//
				if(para['price_type'] == '1'){
					para['stock_type'] = '2';
				}
				//------- 价格日历模式的时候，库存设置成每日库存 --------------//

				$resource('/api/as/tc/sale/update', {}, {}).save(para, function (res) {
					if (res.errcode != 0) {
						toaster.error({ title: "提示", body: res.errmsg });
						return;
					}
					if (angular.isDefined(res.data.uuid)) {
						scope.saleobj.id = res.data.uuid;
					}
					toaster.success({ title: "", body: '操作成功' });
					scope.util.$modalInstance.close();
				});
			};

			scope.concatVerification_modes = function(){
				scope.saleobj.verification_modes = '';
				if(scope.saleobj.verification_modes_ID){
					scope.saleobj.verification_modes += 'ID,'
				}
				if(scope.saleobj.verification_modes_card){
					scope.saleobj.verification_modes += 'card,'
				}
				if(scope.saleobj.verification_modes_ticket_code){
					scope.saleobj.verification_modes += 'ticket_code,'
				}
				if(scope.saleobj.verification_modes_qr_code){
					scope.saleobj.verification_modes += 'qr_code,'
				}
				scope.saleobj.verification_modes = scope.saleobj.verification_modes.substring(0, scope.saleobj.verification_modes.length - 1);
			}

			scope.$watch('[saleobj, util.showFlag]', function (newValue) {
				if (!scope.saleobj.hasOwnProperty('templete_lock_data_json')) {
					return false;
				}
				if (scope.util.showFlag) {
					for (var key in scope.saleobj.templete_lock_data_json) {
						(function (arg) {
							var el = $("[ng-model='saleobj." + arg + "']").parent().parent().hasClass('form-group') ?
								$("[ng-model='saleobj." + arg + "']").parent().parent() :
								$("[ng-model='saleobj." + arg + "']").parent().parent().parent();
							$(el).show();
						})(key);//调用时参数   
					}
					if (scope.saleobj.templete_lock_data_json.hasOwnProperty('max_limit') &&
						scope.saleobj.templete_lock_data_json.hasOwnProperty('order_num_limit')) {
						$(".quarestrict").show();
					}
				} else {
					for (var key in scope.saleobj.templete_lock_data_json) {
						(function (arg) {
							var el = $("[ng-model='saleobj." + arg + "']").parent().parent().hasClass('form-group') ?
								$("[ng-model='saleobj." + arg + "']").parent().parent() :
								$("[ng-model='saleobj." + arg + "']").parent().parent().parent();
							$(el).hide();
						})(key);//调用时参数   
					}
					if (scope.saleobj.templete_lock_data_json.hasOwnProperty('max_limit') &&
						scope.saleobj.templete_lock_data_json.hasOwnProperty('order_num_limit')) {
						$(".quarestrict").hide();
					}
				}
			}, true);
		}
	};
};

