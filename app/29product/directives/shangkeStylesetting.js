module.exports = function($resource, $state, $http, $q, toaster, $modal){

	return {

		restrict : 'AE',
		template : require('../views/shangkeStylesetting.html'),
		replace:true,
		scope:{
			'productobj' : '=',
			'funobj' : '=',
			// 'open' : '=',
		},
		link : function(scope, elements, attrs){
            var goods_id = scope.productobj.aaa;
            scope.infoobj = {
                'goods_id' : goods_id
            };
            scope.array = [];
            if(goods_id){
                $resource('/api/as/mc/mergoodstp/getGoodsTPInfo', {}, {})
                .save({'goods_id' : goods_id},function(res){
                    if(res.errcode === 0)
                    {
                        scope.infoobj = res.data;
                        if(res.data.top_img == ''){
                            scope.array = [];
                        } else {
                            scope.array = res.data.top_img.split(",");
                        }
                    } else if(res.errcode === 10003){
                    } else {
                        toaster.error({title:"",body:res.errmsg});
                    }
                    
                    

                }) 
            }
           
            
            scope.image = function(){
				var para = $state.get('app.imageupload');
				//设置上传文件夹，以自己业务命名
				angular.extend(para, {
					resolve : {  
						'dir' : function(){
							return 't1';
						}
					} 
				})
				var modalInstance = $modal.open(para);
				modalInstance.opened.then(function() {// 模态窗口打开之后执行的函数  
				});  
				modalInstance.result.then(function(result) {  
					scope.array = result;
                    scope.infoobj.top_img=scope.array.join(',');
					
				}, function(reason) {  
					// click，点击取消，则会暑促cancel  
					$log.info('Modal dismissed at: ' + new Date());  
				}); 
            };
            scope.del=function(index){
                scope.array.splice(index,1);
                scope.infoobj.top_img=scope.array.join(',');
            }

            // 保存
            scope.save = function(){
                $resource('/api/as/mc/mergoodstp/saveGoodsTP', {}, {})
                .save(scope.infoobj,function(res){
                    if(res.errcode !== 0)
                    {
                        toaster.error({title:"",body:res.errmsg});
                        return;
                    }
                    toaster.success({title:"",body:"操作成功"});
                    
                    // $state.go('app.supplyProductList');

                }) 
            }
        }
    }
}