module.exports = function ($resource, $state, $http, $q, toaster) {
    return {
        restrict: 'AE',
        template: require('../views/productinfohotel.html'),
        replace: true,
        scope: {
            'saleobj': '=',
            'util': '='
        },
        link: function (scope, elements, attrs) {
            scope.hotelobj = {
                'hotel_sale_code': scope.saleobj.code,
                'hotel_predetermined_range_start': 0,
                'hotel_predetermined_range_end': 0,
                'hotel_breakfast': '1',
                'hotel_booking_time_day': 0,
                'hotel_booking_time_hour': 0,
                'hotel_booking_time_minute': 0,
                'hotel_latest_cancellation_day': 0,
                'hotel_latest_cancellation_hour': 0,
                'hotel_latest_cancellation_minute': 0,
                'hotel_debit_type': 0,
                'hotel_debit_info': '',
                'hotel_check_in_start': 0,
                'hotel_check_in_end': 0
            };

            //获取酒店信息
            $resource('/api/as/tc/salehotel/salehotelinfo', {}, {}).save({ 'hotel_sale_code': scope.saleobj.code }, function (res) {
                if (res.errcode !== 0) {
                    toaster.error({ title: "", body: res.errmsg });
                    return false;
                }
                scope.hotelobj = res.data;
            });
        }
    };
};

