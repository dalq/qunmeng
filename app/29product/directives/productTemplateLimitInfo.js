module.exports = function ($resource, $state, $http, $q, FileUploader, toaster, $modal) {

	return {

		restrict: 'AE',
		template: require('../views/productTemplateLimitInfo.html'),
		replace: true,
		scope: {
			'saleobj': '=',
			'template': '=',
			'templateBaseInfo': '=',
			'funobj': '=',
			'baseinfo': '=',
			'util': '=',
		},
		link: function (scope, elements, attrs) {

			//页面需要的信息
			scope.page = {
				// 		//分类列表
				// 		'categorylist': res.categorylist.data.data,
				// 		//产品所属
				// 		'salebelonglist': res.salebelonglist.data.data,
				// 		//短信列表
				// 		'smslist': res.smslist.data.data,
				// 		//生效时间
				'take_effect_typearr': [
					{ 'name': '次日', 'value': -1 },
					{ 'name': '即时', 'value': 0 },
					{ 'name': '1小时之后', 'value': 1 },
					{ 'name': '2小时之后', 'value': 2 },
					{ 'name': '3小时之后', 'value': 3 },
					{ 'name': '4小时之后', 'value': 4 },
					{ 'name': '5小时之后', 'value': 5 },
					{ 'name': '6小时之后', 'value': 6 },
					{ 'name': '7小时之后', 'value': 7 },
					{ 'name': '8小时之后', 'value': 8 },
					{ 'name': '9小时之后', 'value': 9 },
					{ 'name': '10小时之后', 'value': 10 },
					{ 'name': '11小时之后', 'value': 11 },
					{ 'name': '12小时之后', 'value': 12 }
				],
			};

			scope.save = function () {
				scope.concatVerification_modes();
				if (typeof scope.baseinfo.dateshow.periodstart.date.label === 'string') {
					scope.saleobj.periodstart = scope.baseinfo.dateshow.periodstart.date.label;
				} else {
					scope.saleobj.periodstart = scope.util.date2str(scope.baseinfo.dateshow.periodstart.date.label);
				}

				if (typeof scope.baseinfo.dateshow.periodend.date.label === 'string') {
					scope.saleobj.periodend = scope.baseinfo.dateshow.periodend.date.label;
				} else {
					scope.saleobj.periodend = scope.util.date2str(scope.baseinfo.dateshow.periodend.date.label);
				}

				var params = {};
				var resJson = {};
				for (var key in scope.template) {
					var result = (function (arg) {
						if (arg == 'id') {
							return true;
						}
						var saleStr = arg.substr(0, arg.length - 5);
						if (scope.template[arg]) {
							if (scope.saleobj[saleStr] === undefined || scope.saleobj[saleStr] === '' || scope.saleobj[saleStr] === null) {
								toaster.warning({ title: saleStr, body: '如需配置模板项,请填写默认值!' });
								return false;
							}
							resJson[saleStr] = scope.saleobj[saleStr];
						}
					})(key);//调用时参数  
					if (result === false) {
						return false;
					}
				}

				// 如身份证限制必填, 则保存号码
				if (scope.template['cardno_state_flag'] && (scope.saleobj['cardno_state'] == '1')) {
					resJson['cardno_limit'] = scope.saleobj['cardno_limit'];
				}

				if (resJson['address'] == '0') {
					resJson['vir_t_tkt_sale_ttype'] = { vir_type: '0' }
				} else if (resJson['address'] == '1') {
					resJson['vir_t_tkt_sale_ttype'] = { vir_type: '1' }
				} else {
					resJson['vir_t_tkt_sale_ttype'] = { vir_type: '0' }
				}

				params['templete_lock_data_json'] = JSON.stringify(resJson);

				var modalInstance = $modal.open({
					template: require('../views/productTemplateInfoEdit.html'),
					controller: 'productTemplateInfoEdit',
					url: '/productTemplateInfoEdit',
					size: 'lg',
					resolve: {
						template: function () {
							return scope.templateBaseInfo
						}
					}
				});
				modalInstance.opened.then(function () {// 模态窗口打开之后执行的函数  
				});
				modalInstance.result.then(function (result) {
					angular.extend(params, result)
					$resource('api/ac/tc/ticketSaleTempleteService/updateTemplete', {}, {}).save(params, function (res) {
						if (res.errcode != 0) {
							toaster.error({ title: "提示", body: res.errmsg });
							return;
						}
						toaster.success({ title: "(*^__^*) ……", body: '操作成功' });
						scope.util.$modalInstance.close();
					});
				}, function (reason) {
				});

			};

			scope.concatVerification_modes = function () {
				if (!scope.template.verification_modes_flag) {
					return false;
				}
				scope.saleobj.verification_modes = '';
				if (scope.saleobj.verification_modes_ID) {
					scope.saleobj.verification_modes += 'ID,'
				}
				if (scope.saleobj.verification_modes_card) {
					scope.saleobj.verification_modes += 'card,'
				}
				if (scope.saleobj.verification_modes_ticket_code) {
					scope.saleobj.verification_modes += 'ticket_code,'
				}
				if (scope.saleobj.verification_modes_qr_code) {
					scope.saleobj.verification_modes += 'qr_code,'
				}
				scope.saleobj.verification_modes = scope.saleobj.verification_modes.substring(0, scope.saleobj.verification_modes.length - 1);
			};
		}
	};
};

