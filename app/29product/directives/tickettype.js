module.exports = function ($resource, $state, $http, $q, toaster, $modal) {

	return {

		restrict: 'AE',
		template: require('../views/p_tickettype.html'),
		replace: true,
		scope: {
			'saleobj': '=',
			'util': '=',
			// 'open' : '=',
		},
		link: function (scope, elements, attrs) {

			scope.tickettypeobj = {
				'place_code': '',
				'ticket_type_code': '',
				'ticket_type_attr': '',
				'price': '0',
				'sheets_number': '1',
				'buy_effective_day': '9999',
				'is_available': '0',
				'is_live': '0'
			};
			// scope.date = {
			// 	'lable': scope.util.date2str(new Date()) + "00:00:00",
			// 	'opened': false
			// }
			// scope.date1 = {
			// 	'lable': scope.util.date2str(new Date()) + "23:59:59",
			// 	'opened': false
			// }

			scope.params = {};
			if (scope.saleobj.sale_type == 'H') {
				scope.params.type = 'H';
			}

			scope.tickettypelist = [];

			//页面需要的数据
			scope.page = {};

			var beforedata = {
				//景区列表
				'viewlist':
				$http({
					'method': 'GET',
					'url': '/api/as/tc/placeview/jlist',
					'params': scope.params
				}),
				//票种属性
				'attrlist':
				$http({
					'method': 'GET',
					'url': '/api/as/tc/attr/list',
				}),
			};


			scope.load = function(){
				$q.all(beforedata).then(function (res) {
	
					if (res.viewlist.data.errcode === 0) {
					} else {
						toaster.error({ title: "/api/as/tc/placeview/jlist", body: res.viewlist.data.errmsg });
						return;
					}
	
					if (res.attrlist.data.errcode === 0) {
						scope.tickettypeobj.ticket_type_attr = res.attrlist.data.data[0].ticket_attr_id;
					} else {
						toaster.error({ title: "/api/as/tc/attr/list", body: res.attrlist.data.errmsg });
						return;
					}
	
					gettickettypedetail();
	
					scope.page = {
						'viewlist': res.viewlist.data.data,
						'attrlist': res.attrlist.data.data,
						'tickettypelist': [],
						//'saletickettypelist' : res.saletickettypelist.data.data,
					}
	
				});
			}
			scope.load();

			//景区下拉
			scope.changeview = function () {
				$resource('/api/as/tc/goods/typelist', {}, {})
					.save({ 'view': scope.tickettypeobj.place_code }, function (res) {
						if (res.errcode !== 0) {
							toaster.error({ title: "", body: res.errmsg });
							return;
						}
						scope.page.tickettypelist = res.data;
						if (res.data.length > 0)
							scope.tickettypeobj.ticket_type_code = res.data[0].code;
					});
			};

			//添加票种
			scope.add = function () {

				if (!checkAdd()) return;

				scope.tickettypeobj.sale_code = scope.saleobj.code;
				scope.tickettypeobj.periodstart = scope.saleobj.periodstart;
				scope.tickettypeobj.periodend = scope.saleobj.periodend;
				scope.tickettypeobj.appoint = '0';

				$resource('/api/as/tc/salettype/save', {}, {})
					.save(scope.tickettypeobj, function (res) {
						if (res.errcode !== 0) {
							toaster.error({ title: "", body: res.errmsg });
							return;
						}
						gettickettypedetail();
					});
			};


			scope.save = function (item) {
				if (item.sheets_number == undefined) {
					toaster.error({ title: "", body: '可销次数不能为负数' });
				} else {
					var para = {
						id: item.id,
						appoint: item.appoint,
						periodend: item.periodend,
						periodstart: item.periodstart,
						price: item.price,
						sheets_number: item.sheets_number,
						buy_effective_day: item.buy_effective_day,
						period_scope: item.period_scope
					}
					$resource('/api/as/tc/salettype/save', {}, {})
						.save(para, function (res) {
							if (res.errcode !== 0) {
								toaster.error({ title: "", body: res.errmsg });
								return;
							}
							toaster.success({ title: "提示", body: '修改成功' });
							gettickettypedetail();
						});
				}



			};

			scope.addTime = function (obj){
				var modalInstance = $modal.open({
					template: require('../views/addTime.html'),
					controller: 'addTime',
					size: 'lg',
					resolve: {
						data : function (){
							return scope.date = {
								'lable': scope.util.date2str(new Date()),
								'opened': false
							}
						},
						toaster : function (){
							return toaster
						},
						time : function (){
							return obj.period_scope;
						}
					}
				});
				//关闭模态框刷新页面
				modalInstance.result.then(function(result) {
					obj.period_scope = result.toString();
				});
			}

			scope.save2 = function (item) {

				if (item.sheets_number == undefined) {
					toaster.error({ title: "", body: '可销次数不能为负数' });
				} else {
					var para = {
						id: item.id
					}
					$resource('/api/as/tc/salettype/save', {}, {})
						.save(para, function (res) {
							if (res.errcode !== 0) {
								toaster.error({ title: "", body: res.errmsg });
								return;
							}
							toaster.success({ title: "提示", body: '修改成功' });
							gettickettypedetail();
						});
				}



			};


			//详细信息	删除
			scope.del1 = function (id) {
				if (!confirm("确定要删除该票种吗，亲！！～～")) {
					return false;
				}
				// $resource('/api/as/tc/salettype/delete', {}, {})
				$resource('/api/ac/tc/ticketSaleService/updateDeleteSaleType', {}, {})
					.save({ 'id': id }, function (res) {
						if (res.errcode !== 0) {
							toaster.error({ title: "", body: res.errmsg });
							return;
						}
						toaster.success({ title: "提示", body: '删除成功' });
						gettickettypedetail();
					});
			}


			function checkAdd() {

				if (scope.tickettypeobj.ticket_type_code == '') {
					toaster.error({ title: "", body: '请选择一个票种' });
					return;
				}

				for (var i = 0; i < scope.page.saletickettypelist.length; i++) {
					var tmp = scope.page.saletickettypelist[i];
					if (tmp.ticket_type_code === scope.tickettypeobj.ticket_type_code) {
						toaster.error({ title: "", body: '不能添加重复票种！' });
						return false;
					}
				}

				return true;
			}



			//获取票种详情
			function gettickettypedetail() {
				$resource('/api/as/tc/salettype/list', {}, {})
					.save({ 'sale_code': scope.saleobj.code }, function (res) {
						if (res.errcode !== 0) {
							toaster.error({ title: "", body: res.errmsg });
							return;
						}

						// for (var i = 0; i < res.data.length; i++) {
						// 	// if(res.data[i].appoint=='1'){
						// 	// 	res.data[i].appoint='需要预约';
						// 	// }
						// 	// if(res.data[i].appoint=='0'){
						// 	//      res.data[i].appoint='无需预约';
						// 	// }
						// 	var tmp = res.data[i];
						// 	scope.date.lable = scope.util.date2str(scope.util.str2date(tmp.periodstart));
						// 	scope.date1.lable = scope.util.date2str(scope.util.str2date(tmp.periodend));
						// }
						scope.page.saletickettypelist = res.data;
						// for (var index = 0; index < scope.page.saletickettypelist.length; index++) {
						// 	scope.page.saletickettypelist[index].date = {
						// 		'lable': scope.page.saletickettypelist[index].periodstart,
						// 		'opened': false
						// 	}
						// 	scope.page.saletickettypelist[index].date1 = {
						// 		'lable': scope.page.saletickettypelist[index].periodend,
						// 		'opened': false
						// 	}
						// }
					});
			}

			scope.dateSet = function(item, index, prop){
				var modalInstance = $modal.open({
					template: require('../views/dateSet.html'),
					controller: 'dateSet',
					size: 'sm',
					resolve: {
						date : function (){
							return item;
						},
						toaster : function (){
							return toaster
						},
					}
				});
				//关闭模态框刷新页面
				modalInstance.result.then(function(result) {
					scope.page.saletickettypelist[index][prop] = result.toString();
				});
			}

			// scope.dateOpen = function ($event, item) {
			// 	$event.preventDefault();
			// 	$event.stopPropagation();
			// 	item.opened = true;
			// };

			// scope.dateOpen1 = function ($event, item) {
			// 	$event.preventDefault();
			// 	$event.stopPropagation();
			// 	item.opened = true;
			// };

			scope.sheets_numberarr = [];
			for (var i = 1; i < 61; i++) {
				scope.dicc = { 'label': '', 'value': '' };
				scope.dicc.label = i + ('张');
				scope.dicc.value = i;
				scope.sheets_numberarr.push(scope.dicc);
			}
			scope.bxdicc = { 'label': '不限', 'value': -1 };
			scope.sheets_numberarr.splice(0, 0, scope.bxdicc);

			scope.buy_effective_dayarr = [];
			for (var i = 1; i < 61; i++) {
				scope.dicc = { 'label': '', 'value': '' };
				scope.dicc.label = i + ('天');
				scope.dicc.value = i;
				scope.buy_effective_dayarr.push(scope.dicc);
			}
			scope.daydicc = { 'label': '不限', 'value': 9999 };
			scope.buy_effective_dayarr.splice(0, 0, scope.daydicc);

			scope.moveUp = function (index) {
				if (index == 0) {
					toaster.success({ title: "已是最靠前", body: '不能上移' });
					return false;
				}
				var oldValue = scope.page.saletickettypelist[index].sort;
				var newValue = scope.page.saletickettypelist[index - 1].sort;
				if (oldValue == newValue) {
					newValue = parseInt(newValue) - 1;
				}
				scope.page.saletickettypelist[index].sort = newValue;
				scope.page.saletickettypelist[index - 1].sort = oldValue;
				var para = {
					list: [
						{
							id: scope.page.saletickettypelist[index].id,
							value: scope.page.saletickettypelist[index].sort
						},
						{
							id: scope.page.saletickettypelist[index - 1].id,
							value: scope.page.saletickettypelist[index - 1].sort
						}
					]
				}
				$resource('/api/ac/tc/ticketSaleTypeService/updateSort', {}, {}).save(para, function (res) {
					if (res.errcode !== 0) {
						toaster.error({ title: "提示", body: res.errmsg });
						return;
					}
					toaster.success({ title: "提示", body: '修改成功' });
					scope.load();
				});
			}

			scope.moveDown = function (index) {
				if (index == scope.page.saletickettypelist.length -1) {
					toaster.success({ title: "已是最靠后", body: '不能下移' });
					return false;
				}
				var oldValue = scope.page.saletickettypelist[index].sort;
				var newValue = scope.page.saletickettypelist[index + 1].sort;
				if (oldValue == newValue) {
					newValue = parseInt(newValue) + 1;
				}
				scope.page.saletickettypelist[index].sort = newValue;
				scope.page.saletickettypelist[index + 1].sort = oldValue;
				var para = {
					list: [
						{
							id: scope.page.saletickettypelist[index].id,
							value: scope.page.saletickettypelist[index].sort
						},
						{
							id: scope.page.saletickettypelist[index + 1].id,
							value: scope.page.saletickettypelist[index + 1].sort
						}
					]
				}
				$resource('/api/ac/tc/ticketSaleTypeService/updateSort', {}, {}).save(para, function (res) {
					if (res.errcode !== 0) {
						toaster.error({ title: "提示", body: res.errmsg });
						return;
					}
					toaster.success({ title: "提示", body: '修改成功' });
					scope.load();
				});
			}

		}
	};
};

