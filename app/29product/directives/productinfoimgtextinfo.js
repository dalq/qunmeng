module.exports = function ($resource, $state, $http, $q, FileUploader, toaster) {

	return {

		restrict: 'AE',
		template: require('../views/productinfoimgtextinfo.html'),
		replace: true,
		scope: {
			'saleobj': '=',
			'funobj': '=',
			'baseinfo': '=',
			'util': '=',
		},
		link: function (scope, elements, attrs) {

			var obj = {
				'id': scope.saleobj.id,
				// 'name' : '',
				// 'code' : '',
				// 'market_price' : 0,
				// 'guide_price' : 0,
				// 'cost_price' : 0,
				// 'sale_category' : 'F10',
				// 'sms_template_id' : '',
				// 'sms_diy' : '',
				// 'sms_type' : '1',
				'top_pic': '',
				'logo': '',
				// 'periodstart' : '',
				// 'periodend' : '',
				// 'sale_belong' : 'juyou',	//产品所属
				// 'sys_affirm_type' : '1',	//系统确认
				// 'pay_type' : '0', 	//支付类型
				// 'stock_type' : '0',	//库存类型
				// 'current_stock_num' : 0,
				// 'sale_target_type' : '0',	//销售目标
				// 'take_effect_type' : 0,	//生效时间
				// 'max_limit' : 0,	//最大购买数量
				// 'order_num_limit' : 0,	//每单最大购买数量限制
				// 'tour_date_type' : '0',	//是否启用出游时间
				// 'back_type' : '0',	//是否允许退票
				// 'apply_state' : '0',	//退票是否需要审核
				// 'ticket_type' : '0',	//是否出票
				// 'user_status' : '0',	//是否实名制
				// 'sms_ticketcode_type' : '0',	//短信票码类型
				'bookingnotes': '',	//团产品预订须知
				'detail': '',	//销售品简介
			};

			scope.page = {
				// 		//分类列表
				// 		'categorylist': res.categorylist.data.data,
				// 		//产品所属
				// 		'salebelonglist': res.salebelonglist.data.data,
				// 		//短信列表
				// 		'smslist': res.smslist.data.data,
				// 		//生效时间
				'take_effect_typearr': [
					{ 'name': '次日', 'value': -1 },
					{ 'name': '即时', 'value': 0 },
					{ 'name': '1小时之后', 'value': 1 },
					{ 'name': '2小时之后', 'value': 2 },
					{ 'name': '3小时之后', 'value': 3 },
					{ 'name': '4小时之后', 'value': 4 },
					{ 'name': '5小时之后', 'value': 5 },
					{ 'name': '6小时之后', 'value': 6 },
					{ 'name': '7小时之后', 'value': 7 },
					{ 'name': '8小时之后', 'value': 8 },
					{ 'name': '9小时之后', 'value': 9 },
					{ 'name': '10小时之后', 'value': 10 },
					{ 'name': '11小时之后', 'value': 11 },
					{ 'name': '12小时之后', 'value': 12 }
				],
			};

			scope.save = function () {

				var url = '';
				var para = {};
				// if(scope.saleobj.id == ''){
				// url = '/api/as/tc/sale/create';
				// }else{
				url = '/api/as/tc/sale/update';
				for (var key in obj) {
					para[key] = scope.saleobj[key];
				}
				// }

				$resource(url, {}, {}).save(para, function (res) {
					if (res.errcode != 0) {
						toaster.error({ title: "提示", body: res.errmsg });
						return;
					}

					if (angular.isDefined(res.data.uuid)) {
						scope.saleobj.id = res.data.uuid;
					}
					toaster.success({ title: "提示", body: '操作成功' })
					// toaster.success({ title: "提示", body: '操作成功' });
					if (scope.util.$modalInstance == undefined) {
						$state.go('app.editproduct', { 'id': scope.saleobj.id });
					}
				});

			};


			scope.saveAndBack = function () {

				var url = '';
				var para = {};
				// if(scope.saleobj.id == ''){
				// url = '/api/as/tc/sale/create';
				// }else{
				url = '/api/as/tc/sale/update';
				for (var key in obj) {
					para[key] = scope.saleobj[key];
				}
				// }

				$resource(url, {}, {}).save(para, function (res) {
					if (res.errcode != 0) {
						toaster.error({ title: "提示", body: res.errmsg });
						return;
					}

					if (angular.isDefined(res.data.uuid)) {
						scope.saleobj.id = res.data.uuid;
					}
					toaster.success({ title: "提示", body: '操作成功' })
					scope.util.$modalInstance.close();
				});

			};

			scope.$watch('saleobj', function (newValue) {
				if (!scope.saleobj.hasOwnProperty('templete_lock_data_json')) {
					return false;
				}
				for (var key in scope.saleobj.templete_lock_data_json) {
					(function (arg) {
						// $timeout(function () {
						var el = $(".saleobj-" + arg );
						$(el).hide();
						// }, 800);
					})(key);//调用时参数   
				}
			}, true);

		}

	};
};

