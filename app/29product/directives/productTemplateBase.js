module.exports = function ($resource, $state, $http, $q, FileUploader, toaster, $modal) {
	return {
		restrict: 'AE',
		template: require('../views/productTemplateBase.html'),
		replace: true,
		scope: {
			'saleobj': '=',
			'template': '=',
			'baseinfo': '=',
			'util': '='
		},
		link: function (scope, elements, attrs) {
			//页面需要的信息。
			scope.page = {};

			var beforedata = {
				//类型列表
				'categorylist':
				$http({
					'method': 'GET',
					'url': '/api/as/sc/dict/dictbytypelist',
					'params': { 'type': 'sale_category' }
				}),
				//产品所属
				'salebelonglist':
				$http({
					'method': 'GET',
					'url': '/api/as/sc/dict/dictbytypelist',
					'params': { 'type': 'ticket_sale_belong' }
				}),
				//短信信息
				'smslist':
				$http({
					'method': 'GET',
					'url': '/api/as/tc/salesmstemplate/list'
				}),
			};

			$q.all(beforedata).then(function (res) {
				//分类信息
				if (res.categorylist.data.errcode === 0) {
				} else {
					toaster.errot({ title: "/api/as/sc/dict/dictbytypelist?type=sale_category", body: res.categorylist.data.errmsg })
					return;
				}
				//销售品所属列表
				if (res.salebelonglist.data.errcode === 0) {
				} else {
					toaster.errot({ title: "/api/as/sc/dict/dictbytypelist?type=ticket_sale_belong", body: res.salebelonglist.data.errmsg })
					return;
				}
				//短信列表
				if (res.smslist.data.errcode === 0) {
				} else {
					toaster.errot({ title: "/api/as/tc/salesmstemplate/list", body: res.smslist.data.errmsg })
					return;
				}
				//发短信时，默认短息模板里第一个短信模板。
				if (scope.saleobj.sms_type == '1') {
					scope.saleobj.sms_template_id = res.smslist.data.data[0].sms_template_id;
					scope.saleobj.sms_diy = res.smslist.data.data[0].sms_diy;
				}
				scope.page = {
					//分类列表
					'categorylist': res.categorylist.data.data,
					//产品所属
					'salebelonglist': res.salebelonglist.data.data,
					//短信列表
					'smslist': res.smslist.data.data,
					// //生效时间
				};
			});

			scope.save = function () {
				if (typeof scope.baseinfo.dateshow.periodstart.date.label === 'string') {
					scope.saleobj.periodstart = scope.baseinfo.dateshow.periodstart.date.label;
				} else {
					scope.saleobj.periodstart = scope.util.date2str(scope.baseinfo.dateshow.periodstart.date.label);
				}
				if (typeof scope.baseinfo.dateshow.periodend.date.label === 'string') {
					scope.saleobj.periodend = scope.baseinfo.dateshow.periodend.date.label;
				} else {
					scope.saleobj.periodend = scope.util.date2str(scope.baseinfo.dateshow.periodend.date.label);
				}
				var params = {};
				var resJson = {};
				for (var key in scope.template) {
					var result = (function (arg) {
						var saleStr = arg.substr(0, arg.length - 5);
						if (scope.template[arg]) {
							if (scope.saleobj[saleStr] === undefined || scope.saleobj[saleStr] === '' || scope.saleobj[saleStr] === null) {
								toaster.warning({ title: saleStr, body: '如需配置模板项,请填写默认值!' });
								return false;
							}
							resJson[saleStr] = scope.saleobj[saleStr];
						}
					})(key);//调用时参数  
					if (result === false) {
						return false;
					}
				}
				if (resJson['address'] == '0') {
					resJson['vir_t_tkt_sale_ttype'] = { vir_type: '0' }
				} else if (resJson['address'] == '1') {
					resJson['vir_t_tkt_sale_ttype'] = { vir_type: '1' }
				} else if (resJson['address'] == '2') {
					resJson['vir_t_tkt_sale_ttype'] = { vir_type: '2' }
				} else {
					resJson['vir_t_tkt_sale_ttype'] = { vir_type: '0' }
				}
				params['templete_lock_data_json'] = JSON.stringify(resJson);
				var modalInstance = $modal.open({
					template: require('../views/productTemplateInfo.html'),
					controller: 'productTemplateInfo',
					url: '/productTemplateInfo',
					size: 'lg',
					resolve: {
					}
				});
				modalInstance.opened.then(function () {// 模态窗口打开之后执行的函数  
				});
				modalInstance.result.then(function (result) {
					angular.extend(params, result)
					$resource('/api/as/tc/ticketsaletemplete/insert', {}, {}).save(params, function (res) {
						if (res.errcode != 0) {
							toaster.error({ title: "提示", body: res.errmsg });
							return;
						}
						toaster.success({ title: "(*^__^*) ……", body: '操作成功' });
						$state.go('app.editTemplate', { id: res.data.uuid })
					});
				}, function (reason) {
				});
			};

			scope.changeSms = function (obj) {
				var smsid = obj.sms_template_id;
				if (smsid == null) {
					obj.sms_diy = '';
				}
				else {
					for (var i = 0, j = scope.page.smslist.length; i < j; i++) {
						var tmp = scope.page.smslist[i];
						if (tmp.sms_template_id == smsid) {
							obj.sms_diy = tmp.sms_diy;
							break;
						}
					}
				}
			};
		}
	};
};

