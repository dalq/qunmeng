module.exports = function ($resource, $state, $http, $q, FileUploader, toaster) {
	return {
		restrict: 'AE',
		template: require('../views/p_baseinfo.html'),
		replace: true,
		scope: {
			'saleobj': '=',
			'funobj': '=',
			'baseinfo': '=',
			'util': '=',
		},
		link: function (scope, elements, attrs) {
			scope.auditing = scope.util.auditing;
			var obj = {
				'id': scope.saleobj.id,
				'name': '',
				'name_inside': '',
				'code': '',
				'price_type': '0',
				'market_price': 0,
				'guide_price': 0,
				'cost_price': 0,
				'purchase_price': 0,
				'asort': '0',
				'sale_category': 'F10',
				'sms_template_id': '',
				'sms_diy': '',
				'sms_type': '1',
				// 'top_pic' : '',
				// 'logo' : '',
				'periodstart': '',
				'periodend': '',
				'sale_belong': 'juyou',	//产品所属
				'nopay_cancel_minute': '60',	//未支付订单超时时间（单位分钟）
				'sys_affirm_type': '1',	//系统确认
				'is_platform_sale': '1',	//平台产品
				// 'periodstart_h': '00',
				// 'periodstart_m': '00',
				// 'periodend_h': '23',
				// 'periodend_m': '59',
				// 'pay_type' : '0', 	//支付类型
				// 'stock_type' : '0',	//库存类型
				// 'current_stock_num' : 0,
				// 'sale_target_type' : '0',	//销售目标
				// 'take_effect_type' : 0,	//生效时间
				// 'max_limit' : 0,	//最大购买数量
				// 'order_num_limit' : 0,	//每单最大购买数量限制
				// 'tour_date_type' : '0',	//是否启用出游时间
				// 'back_type' : '0',	//是否允许退票
				// 'apply_state' : '0',	//退票是否需要审核
				// 'ticket_type' : '0',	//是否出票
				// 'user_status' : '0',	//是否实名制
				// 'sms_ticketcode_type' : '0',	//短信票码类型
				// 'bookingnotes' : '',	//团产品预订须知
				// 'detail' : '',	//销售品简介
				// 'verification_modes': 'ID,card,ticket_code,order_code',	//验证方式
				// 'verification_modes_ID': true,	//销售品简介
				// 'verification_modes_card': true,	//销售品简介
				// 'verification_modes_ticket_code': true,	//销售品简介
				// 'verification_modes_order_code': true,	//销售品简介
			};

			angular.extend(scope.saleobj, obj);

			//页面需要的信息。
			scope.page = {};

			var beforedata = {
				//类型列表
				'categorylist':
				$http({
					'method': 'GET',
					'url': '/api/as/sc/dict/getSaleCategoryList',
					// 'url': '/api/as/sc/dict/dictbytypelist',
					'params': { 'type': 'sale_category' },
				}),
				//产品所属
				'salebelonglist':
				$http({
					'method': 'GET',
					'url': '/api/as/sc/dict/dictbytypelist',
					'params': { 'type': 'ticket_sale_belong' },
				}),
				//短信信息
				'smslist':
				$http({
					'method': 'GET',
					'url': '/api/as/tc/salesmstemplate/list',
				}),
			};

			if (scope.saleobj.id != '') {
				beforedata['saleinfo'] = $http({
					'method': 'GET',
					'url': '/api/as/tc/sale/info',
					'params': {
						'id': scope.saleobj.id
					}
				});
			}

			//有效期默认时间
			// console.log(scope.baseinfo);
			// if(angular.isUndefined(scope.baseinfo.dateshow.periodstart)){
			// 	scope.baseinfo.dateshow.periodstart = {
			// 		'date' : {}
			// 	};
			// }
			// if(angular.isUndefined(scope.baseinfo.dateshow.periodend)){
			// 	scope.baseinfo.dateshow.periodend = {
			// 		'date' : {}
			// 	};
			// }
			scope.baseinfo.dateshow.periodstart.date.h = '00';
			scope.baseinfo.dateshow.periodstart.date.m = '00';
			scope.baseinfo.dateshow.periodend.date.h = '23';
			scope.baseinfo.dateshow.periodend.date.m = '59';
			
			$q.all(beforedata).then(function (res) {
				//分类信息
				if (res.categorylist.data.errcode === 0) {
				} else {
					toaster.error({ title: "/api/as/sc/dict/dictbytypelist?type=sale_category", body: res.categorylist.data.errmsg });
					return;
				}
				//销售品所属列表
				if (res.salebelonglist.data.errcode === 0) {
				} else {
					toaster.error({ title: "/api/as/sc/dict/dictbytypelist?type=ticket_sale_belong", body: res.salebelonglist.data.errmsg });
					return;
				}
				//短信列表
				if (res.smslist.data.errcode === 0) {
				} else {
					toaster.error({ title: "/api/as/tc/salesmstemplate/list", body: res.smslist.data.errmsg });
					return;
				}
				if (res.categorylist.data.data.length < 1) {
					toaster.error({ title: "您的账号无权查看销售品", body: "请配置销售品权限" });
					scope.util.$modalInstance.close();
					return false;
				}
				//现在的产品分类是根据机构配置的，不是哪个机构都有F10了，产品分类默认值。
				scope.saleobj.sale_category = res.categorylist.data.data[0].value;
				//发短信时，默认短息模板里第一个短信模板。
				if (scope.saleobj.sms_type == '1') {
					scope.saleobj.sms_template_id = res.smslist.data.data[0].sms_template_id;
					scope.saleobj.sms_diy = res.smslist.data.data[0].sms_diy;
				}
				if (angular.isDefined(res.saleinfo)) {
					//销售品信息
					if (res.saleinfo.data.errcode === 0) {
						//赋值给销售品对象。
						angular.extend(scope.saleobj, res.saleinfo.data.data);
						if (scope.saleobj.periodstart != '') {
							var arr1= scope.saleobj.periodstart.split(' ');
							var arr2= arr1[1].split(':');
							scope.baseinfo.dateshow.periodstart.date = {
								'label': arr1[0],
								'value': arr1[0],
								'opened': false
							};
							scope.baseinfo.dateshow.periodstart.date.h = arr2[0];
							scope.baseinfo.dateshow.periodstart.date.m = arr2[1];
						}
						if (scope.saleobj.periodend != '') {
							var arr1= scope.saleobj.periodend.split(' ');
							var arr2= arr1[1].split(':');
							scope.baseinfo.dateshow.periodend.date = {
								'label': arr1[0],
								'value': arr1[0],
								'opened': false
							};
							scope.baseinfo.dateshow.periodend.date.h = arr2[0];
							scope.baseinfo.dateshow.periodend.date.m = arr2[1];
						}
						if (scope.saleobj.templete_lock_data_json) {
							scope.saleobj.templete_lock_data_json = eval('(' + scope.saleobj.templete_lock_data_json + ')')
						}
						if (scope.saleobj.verification_modes) {
							var verification_modes_arr = scope.saleobj.verification_modes.split(',');
							for (var index = 0; index < verification_modes_arr.length; index++) {
								var element = verification_modes_arr[index];
								scope.saleobj['verification_modes_' + element] = true;
							}
						}
					} else {
						toaster.error({ title: "/api/as/tc/sale/info", body: res.saleinfo.data.errmsg });
						return;
					}
				}
				scope.page = {
					//分类列表
					'categorylist': res.categorylist.data.data,
					//产品所属
					'salebelonglist': res.salebelonglist.data.data,
					//短信列表
					'smslist': res.smslist.data.data,
				};
				//获取功能列表
				$resource('/api/as/tc/salecategory/list', {}, {})
					.get({ 'sale_category_code': scope.saleobj.sale_category }, function (res) {
						if (res.errcode !== 0) {
							toaster.error({ title: "", body: res.errmsg });
							return;
						}
						for (var i = 0; i < res.data.length; i++) {
							var tmp = res.data[i];
							scope.funobj[tmp.sub_table_code] = tmp;
						}
					});
			});
			scope.save = function () {
				if (!scope.saleobj.name_flag && (scope.saleobj.name === undefined || scope.saleobj.name == '')) {
					toaster.error({ title: "", body: '销售品名称不能为空' });
					return;
				}
				if (!scope.saleobj.sms_type_flag && (scope.saleobj.sms_type == '1'
					&& (scope.saleobj.sms_diy == undefined || scope.saleobj.sms_diy == ''))) {
					toaster.error({ title: "", body: '请配置短信内容' });
					return;
				}
				if (typeof scope.baseinfo.dateshow.periodstart.date.label === 'string') {
					scope.saleobj.periodstart = scope.baseinfo.dateshow.periodstart.date.label + 
												' ' + scope.baseinfo.dateshow.periodstart.date.h +
												':' + scope.baseinfo.dateshow.periodstart.date.m +
												':00';
				} else {
					scope.saleobj.periodstart = scope.util.date2str(scope.baseinfo.dateshow.periodstart.date.label) + 
												' ' + scope.baseinfo.dateshow.periodstart.date.h +
												':' + scope.baseinfo.dateshow.periodstart.date.m +
												':00';
				}
				if (typeof scope.baseinfo.dateshow.periodend.date.label === 'string') {
					scope.saleobj.periodend = scope.baseinfo.dateshow.periodend.date.label + 
												' ' + scope.baseinfo.dateshow.periodend.date.h +
												':' + scope.baseinfo.dateshow.periodend.date.m +
												':00';
				} else {
					scope.saleobj.periodend = scope.util.date2str(scope.baseinfo.dateshow.periodend.date.label) + 
												' ' + scope.baseinfo.dateshow.periodend.date.h +
												':' + scope.baseinfo.dateshow.periodend.date.m +
												':00';
				}
				if(scope.saleobj.state == '2' && !scope.is_reloaded){
					delete scope.saleobj.is_platform_sale
				}
				var url = '';
				var para = {};
				if (scope.saleobj.id == '') {
					url = '/api/ac/tc/ticketSaleService/createSaleByTemplete';
					//创建时默认所有字段
					para = scope.saleobj;
				} else {
					url = '/api/as/tc/sale/update';
					//只更新本页字段
					for (var key in obj) {
						para[key] = scope.saleobj[key];
					}
				}

				//------- 价格日历模式的时候，库存设置成每日库存 --------------//
				if(para['price_type'] == '1'){
					para['stock_type'] = '2';
				}
				//------- 价格日历模式的时候，库存设置成每日库存 --------------//

				$resource(url, {}, {}).save(para, function (res) {
					if (res.errcode != 0) {
						toaster.error({ title: "提示", body: res.errmsg });
						return;
					}
					if (angular.isDefined(res.data.uuid)) {
						scope.saleobj.id = res.data.uuid;
					}
					toaster.success({ title: "提示", body: '操作成功' });
					//新建
					if (scope.util.$modalInstance == undefined) {
						$state.go('app.editProduct', { 'id': scope.saleobj.id });
					}
				});
			};

			scope.saveAndBack = function () {
				if (!scope.saleobj.name_flag && (scope.saleobj.name === undefined || scope.saleobj.name == '')) {
					toaster.error({ title: "", body: '销售品名称不能为空' });
					return;
				}
				if (!scope.saleobj.sms_type_flag && (scope.saleobj.sms_type == '1'
					&& (scope.saleobj.sms_diy == undefined || scope.saleobj.sms_diy == ''))) {
					toaster.error({ title: "", body: '请配置短信内容' });
					return;
				}

				if (typeof scope.baseinfo.dateshow.periodstart.date.label === 'string') {
					scope.saleobj.periodstart = scope.baseinfo.dateshow.periodstart.date.label + 
												' ' + scope.baseinfo.dateshow.periodstart.date.h +
												':' + scope.baseinfo.dateshow.periodstart.date.m +
												':00';
				} else {
					scope.saleobj.periodstart = scope.util.date2str(scope.baseinfo.dateshow.periodstart.date.label) + 
												' ' + scope.baseinfo.dateshow.periodstart.date.h +
												':' + scope.baseinfo.dateshow.periodstart.date.m +
												':00';
				}
				if (typeof scope.baseinfo.dateshow.periodend.date.label === 'string') {
					scope.saleobj.periodend = scope.baseinfo.dateshow.periodend.date.label + 
												' ' + scope.baseinfo.dateshow.periodend.date.h +
												':' + scope.baseinfo.dateshow.periodend.date.m +
												':00';
				} else {
					scope.saleobj.periodend = scope.util.date2str(scope.baseinfo.dateshow.periodend.date.label) + 
												' ' + scope.baseinfo.dateshow.periodend.date.h +
												':' + scope.baseinfo.dateshow.periodend.date.m +
												':00';
				}
				var url = '';
				var para = {};
				if (scope.saleobj.id == '') {
					url = '/api/ac/tc/ticketSaleService/createSaleByTemplete';
					//创建时默认所有字段
					para = scope.saleobj;
				} else {
					url = '/api/as/tc/sale/update';
					//只更新本页字段
					for (var key in obj) {
						para[key] = scope.saleobj[key];
					}
				}
				if(scope.saleobj.state == '2' && !scope.is_reloaded){
					delete scope.saleobj.is_platform_sale
				}

				//------- 价格日历模式的时候，库存设置成每日库存 --------------//
				if(para['price_type'] == '1'){
					para['stock_type'] = '2';
				}
				//------- 价格日历模式的时候，库存设置成每日库存 --------------//

				
				$resource(url, {}, {}).save(para, function (res) {
					if (res.errcode != 0) {
						toaster.error({ title: "提示", body: res.errmsg });
						return;
					}

					if (angular.isDefined(res.data.uuid)) {
						scope.saleobj.id = res.data.uuid;
					}
					toaster.success({ title: "", body: '操作成功' });
					scope.util.$modalInstance.close();
				});
			};

			scope.changeSms = function (obj) {
				var smsid = obj.sms_template_id;
				if (smsid == null) {
					obj.sms_diy = '';
				}
				else {
					for (var i = 0, j = scope.page.smslist.length; i < j; i++) {
						var tmp = scope.page.smslist[i];
						if (tmp.sms_template_id == smsid) {
							obj.sms_diy = tmp.sms_diy;
							break;
						}
					}
				}
			};

			scope.pass = function () {
				if (!scope.saleobj.apply_note) {
					toaster.success({ title: "", body: '审核意见必填' });
					return false;
				}
				$resource('/api/ac/tc/ticketSaleService/updateSaleApplyPass', {}, {})
					.save({
						'id': scope.saleobj.id,
						'apply_note': scope.saleobj.apply_note
					}, function (res) {
						if (res.errcode !== 0) {
							toaster.error({ title: "", body: res.errmsg });
							return;
						}
						toaster.success({ title: "", body: '操作成功' });
						scope.util.$modalInstance.close();
					});
			};

			scope.nopass = function () {
				if (!scope.saleobj.apply_note) {
					toaster.error({ title: "", body: '审核意见必填' });
					return false;
				}
				$resource('/api/ac/tc/ticketSaleService/updateSaleApplyNoPass', {}, {})
					.save({
						'id': scope.saleobj.id,
						'apply_note': scope.saleobj.apply_note
					}, function (res) {
						if (res.errcode !== 0) {
							toaster.error({ title: "提示", body: res.errmsg });
							return;
						}
						toaster.success({ title: "", body: '操作成功' });
						scope.util.$modalInstance.close();
					});
			};

			scope.synize = function () {
				scope.saleobj.name_inside = scope.saleobj.name;
			}

			scope.$watch('saleobj', function (newValue) {
				if (!scope.saleobj.hasOwnProperty('templete_lock_data_json')) {
					return false;
				}
				for (var key in scope.saleobj.templete_lock_data_json) {
					(function (arg) {
						var el = $("[ng-model='saleobj." + arg + "']").parent().parent().hasClass('form-group') ?
							$("[ng-model='saleobj." + arg + "']").parent().parent() :
							$("[ng-model='saleobj." + arg + "']").parent().parent().parent();
						$(el).hide();
					})(key);//调用时参数   
				}
				if (scope.saleobj.templete_lock_data_json.hasOwnProperty('sms_type') &&
					(scope.saleobj.templete_lock_data_json.sms_type == '0' ||
						(scope.saleobj.templete_lock_data_json.hasOwnProperty('sms_template_id') &&
							scope.saleobj.templete_lock_data_json.hasOwnProperty('sms_diy')))) {
					$(".message").hide();
				}
				if (scope.saleobj.templete_lock_data_json.hasOwnProperty('periodstart')) {
					$(".periodstart").hide();
				}
				if (scope.saleobj.templete_lock_data_json.hasOwnProperty('periodend')) {
					$(".periodend").hide();
				}
			}, true);

			scope.is_reloaded = false;
			scope.reload = function(){
				$resource('/api/as/tc/saleauth/delSaleAuth', {}, {}).save({'sale_code': scope.saleobj.code}, function (res) {
					if (res.errcode === 0 ) {
						toaster.success({ title: "提示", body: "修改成功" });
						scope.is_reloaded = true;
						return false;
					} else if(res.errcode === 1105){
						toaster.warning({ title: "提示", body: "无可更新内容" });
						scope.is_reloaded = true;
						return false;
					}
				});
			}
		}
	};
};

