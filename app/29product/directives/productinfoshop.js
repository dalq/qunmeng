module.exports = function($resource, $state, $http, $q, toaster, toaster){

	return {

		restrict : 'AE',
		template : require('../views/productinfoshop.html'),
		replace:true,
		scope:{
			'saleobj' : '=',
			'util' : '=',
			// 'open' : '=',
		},
		link : function(scope, elements, attrs){
			scope.tickettypeobj = {
				'id': '',
				'sale_code': scope.saleobj.code,
				'place_code': '',
				'periodstart': '',
				'periodend': '',
				'appoint': '0',
				'type': 'M'
			};
			scope.periodstart =
				{
					'lable': scope.util.date2str(new Date()),
					'opened': false
				};
			scope.periodend =
				{
					'lable': scope.util.date2str(new Date()),
					'opened': false
				};
			scope.shopArr = [];
			scope.shopMap = {};
			scope.ticMap = {};
			scope.codeStr = {
				str: ''
			};
			//页面需要的数据
			scope.page = {};
			$resource('/api/as/tc/placeview/jlist', {}, {})
				.save(scope.tickettypeobj, function (res) {
					if (res.errcode !== 0) {
						toaster.error({ title: "提示", body: res.errmsg });
						return;
					}
					scope.page = {
						'viewlist': res.data
					}
					for (var index = 0; index < res.data.length; index++) {
						var element = res.data[index];
						scope.shopMap[element.code + ''] = element.name;
					}
					scope.codeStr.str = res.data[0].code;
					gettickettypedetail();
				});

			//添加票种
			scope.add = function () {
				if (scope.ticMap[scope.codeStr.str]) {
					toaster.warning({title: '', body: '请勿添加重复票种'});
					return false;
				}
				scope.ticMap[scope.codeStr.str] = scope.shopMap[scope.codeStr.str];
				scope.shopArr.push({ code: scope.codeStr.str, name: scope.shopMap[scope.codeStr.str] });
				scope.tickettypeobj.place_code += scope.codeStr.str + ',';
			};

			scope.save = function (item) {
				scope.tickettypeobj.place_code = scope.tickettypeobj.place_code.substring(0, scope.tickettypeobj.place_code.length - 1)
				if (typeof scope.periodstart.lable === 'string') {
					scope.tickettypeobj.periodstart = scope.periodstart.lable;
				} else {
					scope.tickettypeobj.periodstart = scope.util.date2str(scope.periodstart.lable);
				}
				if (typeof scope.periodend.lable === 'string') {
					scope.tickettypeobj.periodend = scope.periodend.lable;
				} else {
					scope.tickettypeobj.periodend = scope.util.date2str(scope.periodend.lable);
				}
				$resource('/api/as/tc/salettype/save', {}, {})
					.save( scope.tickettypeobj, function (res) {
						if (res.errcode !== 0) {
							toaster.error({ title: "提示", body: res.errmsg });
							return;
						}
						toaster.success({ title: "提示", body: '修改成功' });
						gettickettypedetail();
					});
			};
			//详细信息	删除
			scope.del = function (index) {
				scope.tickettypeobj.place_code.replace(scope.shopArr[index]['code']  + ',', '');
				delete scope.ticMap[scope.shopArr[index]['code']];
				scope.shopArr.splice(index,1);
			}
			//获取票种详情
			function gettickettypedetail() {
				$resource('/api/as/tc/salettype/list', {}, {})
					.save({ 'sale_code': scope.saleobj.code }, function (res) {
						if (res.errcode !== 0) {
							toaster.error({ title: "提示", body: res.errmsg });
							return;
						}
						scope.shopArr = res.data;
						for (var index = 0; index < scope.shopArr.length; index++) {
							var element = scope.shopArr[index];
							scope.shopArr[index].start_date = {
								'lable': element.periodstart? element.periodstart: scope.util.date2str(new Date()),
								'opened': false
							};
							scope.shopArr[index].end_date = {
								'lable': element.periodend? element.periodend: scope.util.date2str(new Date(new Date().getFullYear(), 11, 31)),
								'opened': false
							};
							scope.shopArr[index].place_code = element.view_code;
							var placeArr = element.view_code.split(',');
							for (var i = 0; i < placeArr.length; i++) {
								var element = angular.copy(placeArr[i])
								placeArr[i] = {
									name: scope.shopMap[element],
									code: element
								}
							}
							scope.shopArr[index].placeArr = placeArr;
						}
						// scope.shopArr = [];
						// if (!res.data.length) {
						// 	return false;
						// }
						// var tmp = res.data[0];
						// for (var key in scope.tickettypeobj) {
						// 	if (res.data[0].hasOwnProperty(key)) {
						// 		scope.tickettypeobj[key] = res.data[0][key]
						// 	}
						// }
						// scope.tickettypeobj['place_code'] = '';
						// for (var index = 0; index < res.data.length; index++) {
						// 	var element = res.data[index];
						// 	scope.shopArr.push({ code: element.view_code, name: scope.shopMap[element.view_code] })
						// 	scope.ticMap[element.view_code] = scope.shopMap[element.view_code];
						// 	scope.tickettypeobj.place_code += element.view_code + ',';
						// }
						// var tempArr = res.data[0].view_code.split(',');
						// for (var index = 0; index < tempArr.length; index++) {
						// 	var element = tempArr[index];
						// 	scope.shopArr.push({ code: element, name: scope.shopMap[element] })
						// 	scope.ticMap[element] = scope.shopMap[element];
						// 	scope.tickettypeobj.place_code += element + ',';
						// }
					});
			}
			scope.dateOpen = function ($event, item) {
				$event.preventDefault();
				$event.stopPropagation();
				item.opened = true;
			};
		}

	};
};

