module.exports = function ($resource, $state, $http, $q, FileUploader, toaster) {

	return {

		restrict: 'AE',
		template: require('../views/productinfobaseinfo.html'),
		replace: true,
		scope: {
			'saleobj': '=',
			'funobj': '=',
			'baseinfo': '=',
			'util': '=',
		},
		link: function (scope, elements, attrs) {
			var obj = {
				'id': scope.saleobj.id,
				'name': '',
				'name_inside': '',
				'code': '',
				'market_price': 0,
				'guide_price': 0,
				'cost_price': 0,
				'asort': '0',
				'sale_category': 'F10',
				'sms_template_id': '',
				'sms_diy': '',
				'sms_type': '1',
				// 'top_pic' : '',
				// 'logo' : '',
				'periodstart': '',
				'periodend': '',
				'sale_belong': 'juyou',	//产品所属
				'nopay_cancel_minute': '60',	//未支付订单超时时间（单位分钟）
				'sys_affirm_type': '1',	//系统确认
				// 'pay_type' : '0', 	//支付类型
				// 'stock_type' : '0',	//库存类型
				// 'current_stock_num' : 0,
				// 'sale_target_type' : '0',	//销售目标
				// 'take_effect_type' : 0,	//生效时间
				// 'max_limit' : 0,	//最大购买数量
				// 'order_num_limit' : 0,	//每单最大购买数量限制
				// 'tour_date_type' : '0',	//是否启用出游时间
				// 'back_type' : '0',	//是否允许退票
				// 'apply_state' : '0',	//退票是否需要审核
				// 'ticket_type' : '0',	//是否出票
				// 'user_status' : '0',	//是否实名制
				// 'sms_ticketcode_type' : '0',	//短信票码类型
				// 'bookingnotes' : '',	//团产品预订须知
				// 'detail' : '',	//销售品简介
				// 'verification_modes': 'ID,card,ticket_code,order_code',	//验证方式
				// 'verification_modes_ID': true,	//销售品简介
				// 'verification_modes_card': true,	//销售品简介
				// 'verification_modes_ticket_code': true,	//销售品简介
				// 'verification_modes_order_code': true,	//销售品简介
			};

			angular.extend(scope.saleobj, obj);

			//页面需要的信息。
			scope.page = {};

			var beforedata = {
				//类型列表
				'categorylist':
				$http({
					'method': 'GET',
					'url': '/api/as/sc/dict/dictbytypelist',
					'params': { 'type': 'sale_category' },
				}),
				//产品所属
				'salebelonglist':
				$http({
					'method': 'GET',
					'url': '/api/as/sc/dict/dictbytypelist',
					'params': { 'type': 'ticket_sale_belong' },
				}),
				//短信信息
				'smslist':
				$http({
					'method': 'GET',
					'url': '/api/as/tc/salesmstemplate/list',
				}),
			};

			if (scope.saleobj.id != '') {
				beforedata['saleinfo'] = $http({
					'method': 'GET',
					'url': '/api/as/tc/sale/info',
					'params': {
						'id': scope.saleobj.id
					}
				});
			}

			$q.all(beforedata).then(function (res) {

				//分类信息
				if (res.categorylist.data.errcode === 0) {
				} else {
					toaster.error({ title: "/api/as/sc/dict/dictbytypelist?type=sale_category", body: res.categorylist.data.errmsg })
					return;
				}
				//销售品所属列表
				if (res.salebelonglist.data.errcode === 0) {
				} else {
					toaster.error({ title: "/api/as/sc/dict/dictbytypelist?type=ticket_sale_belong", body: res.salebelonglist.data.errmsg })
					return;
				}
				//短信列表
				if (res.smslist.data.errcode === 0) {
				} else {
					toaster.error({ title: "/api/as/tc/salesmstemplate/list", body: res.smslist.data.errmsg })
					return;
				}

				//发短信时，默认短息模板里第一个短信模板。
				if (scope.saleobj.sms_type == '1') {
					scope.saleobj.sms_template_id = res.smslist.data.data[0].sms_template_id;
					scope.saleobj.sms_diy = res.smslist.data.data[0].sms_diy;
				}

				if (angular.isDefined(res.saleinfo)) {
					//销售品信息
					if (res.saleinfo.data.errcode === 0) {
						//赋值给销售品对象。
						angular.extend(scope.saleobj, res.saleinfo.data.data);
						if (scope.saleobj.periodstart != '') {
							scope.baseinfo.dateshow.periodstart.date = scope.util.str2date(scope.saleobj.periodstart);
						}
						if (scope.saleobj.periodend != '') {
							scope.baseinfo.dateshow.periodend.date = scope.util.str2date(scope.saleobj.periodend);
						}
						if (scope.saleobj.templete_lock_data_json) {
							scope.saleobj.templete_lock_data_json = eval('(' + scope.saleobj.templete_lock_data_json + ')')
						}
						//审核提示赋值
						scope.util.apply_note_temp = scope.saleobj.apply_note ? scope.saleobj.apply_note : '审核意见必填';
						scope.saleobj.apply_note = '';
						
					} else {
						toaster.error({ title: "/api/as/tc/sale/info", body: res.saleinfo.data.errmsg })
						return;
					}
				}

				scope.page = {
					//分类列表
					'categorylist': res.categorylist.data.data,
					//产品所属
					'salebelonglist': res.salebelonglist.data.data,
					//短信列表
					'smslist': res.smslist.data.data,
					// //生效时间
					// 'take_effect_typearr': [
					// 	{ 'name': '次日', 'value': -1 },
					// 	{ 'name': '即时', 'value': 0 },
					// 	{ 'name': '1小时之后', 'value': 1 },
					// 	{ 'name': '2小时之后', 'value': 2 },
					// 	{ 'name': '3小时之后', 'value': 3 },
					// 	{ 'name': '4小时之后', 'value': 4 },
					// 	{ 'name': '5小时之后', 'value': 5 },
					// 	{ 'name': '6小时之后', 'value': 6 },
					// 	{ 'name': '7小时之后', 'value': 7 },
					// 	{ 'name': '8小时之后', 'value': 8 },
					// 	{ 'name': '9小时之后', 'value': 9 },
					// 	{ 'name': '10小时之后', 'value': 10 },
					// 	{ 'name': '11小时之后', 'value': 11 },
					// 	{ 'name': '12小时之后', 'value': 12 }
					// ],
				};
				scope.sale_category_info = '';
				for (var index = 0; index < scope.page.categorylist.length; index++) {
					var element = scope.page.categorylist[index];
					if (element.value == scope.saleobj.sale_category) {
						scope.sale_category_info = element.label;
					}
				}

				scope.sale_belong_info = '';
				for (var index = 0; index < scope.page.salebelonglist.length; index++) {
					var element = scope.page.salebelonglist[index];
					if (element.value == scope.saleobj.sale_belong) {
						scope.sale_belong_info = element.label;
					}
				}

				scope.saleobj.verification_modes_info = '';
				if(scope.saleobj.verification_modes){
					var verification_modes_arr = scope.saleobj.verification_modes.split(',');
					for (var index = 0; index < verification_modes_arr.length; index++) {
						var element = verification_modes_arr[index];
						if (element == 'ID') {
							scope.saleobj.verification_modes_info += '身份证,';
						}
						if (element == 'card') {
							scope.saleobj.verification_modes_info += '卡号,';
						}
						if (element == 'ticket_code') {
							scope.saleobj.verification_modes_info += '票码,';
						}
						if (element == 'qr_code') {
							scope.saleobj.verification_modes_info += '二维码,';
						}
					}
					scope.saleobj.verification_modes_info = scope.saleobj.verification_modes_info.substring(0, scope.saleobj.verification_modes_info.length - 1)
				}

				//获取功能列表
				$resource('/api/as/tc/salecategory/list', {}, {})
					.get({ 'sale_category_code': scope.saleobj.sale_category }, function (res) {
						if (res.errcode !== 0) {
							toaster.error({ title: "提示", body: res.errmsg });
							return;
						}

						for (var i = 0; i < res.data.length; i++) {
							var tmp = res.data[i];
							scope.funobj[tmp.sub_table_code] = tmp;
						}

					});

			});

			scope.save = function () {

				if (scope.saleobj.name === undefined || scope.saleobj.name == '') {
					toaster.error({ title: "提示", body: '销售品名称不能为空' });
					return;
				}
				if (scope.saleobj.sms_type == '1'
					&& (scope.saleobj.sms_diy == undefined || scope.saleobj.sms_diy == '')) {
					toaster.error({ title: "提示", body: '请配置短信内容' });
					return;
				}

				scope.saleobj.periodstart = scope.util.date2str(scope.baseinfo.dateshow.periodstart.date);
				scope.saleobj.periodend = scope.util.date2str(scope.baseinfo.dateshow.periodend.date);

				var url = '';
				var para = {};
				if (scope.saleobj.id == '') {
					url = '/api/as/tc/sale/create';
					//创建时默认所有字段
					para = scope.saleobj;
				} else {
					url = '/api/as/tc/sale/update';
					//只更新本页字段
					for (var key in obj) {
						para[key] = scope.saleobj[key];
					}
				}

				$resource(url, {}, {}).save(para, function (res) {
					if (res.errcode != 0) {
						toaster.error({ title: "提示", body: res.errmsg });
						return;
					}

					if (angular.isDefined(res.data.uuid)) {
						scope.saleobj.id = res.data.uuid;
					}
					toaster.success({ title: "提示", body: '操作成功' });

					//新建
					if (scope.util.$modalInstance == undefined) {
						$state.go('app.editproduct', { 'id': scope.saleobj.id });
					}
				});
			};

			scope.saveAndBack = function () {

				if (scope.saleobj.name === undefined || scope.saleobj.name == '') {
					toaster.error({ title: "提示", body: '销售品名称不能为空' });
					return;
				}
				if (scope.saleobj.sms_type == '1'
					&& (scope.saleobj.sms_diy == undefined || scope.saleobj.sms_diy == '')) {
					toaster.error({ title: "提示", body: '请配置短信内容' });
					return;
				}

				scope.saleobj.periodstart = scope.util.date2str(scope.baseinfo.dateshow.periodstart.date);
				scope.saleobj.periodend = scope.util.date2str(scope.baseinfo.dateshow.periodend.date);

				var url = '';
				var para = {};
				if (scope.saleobj.id == '') {
					url = '/api/as/tc/sale/create';
					//创建时默认所有字段
					para = scope.saleobj;
				} else {
					url = '/api/as/tc/sale/update';
					//只更新本页字段
					for (var key in obj) {
						para[key] = scope.saleobj[key];
					}
				}

				$resource(url, {}, {}).save(para, function (res) {
					if (res.errcode != 0) {
						toaster.error({ title: "提示", body: res.errmsg });
						return;
					}

					if (angular.isDefined(res.data.uuid)) {
						scope.saleobj.id = res.data.uuid;
					}
					toaster.success({ title: "提示", body: '操作成功' });
					scope.util.$modalInstance.close();
				});

			};

			scope.changeSms = function (obj) {
				var smsid = obj.sms_template_id;
				if (smsid == null) {
					obj.sms_diy = '';
				}
				else {
					for (var i = 0, j = scope.page.smslist.length; i < j; i++) {
						var tmp = scope.page.smslist[i];
						if (tmp.sms_template_id == smsid) {
							obj.sms_diy = tmp.sms_diy;
							break;
						}
					}
				}
			};

			scope.pass = function () {
				if (!scope.saleobj.apply_note) {
					toaster.success({ title: "提示", body: '审核意见必填' })
					return false;
				}
				$resource('/api/ac/tc/ticketSaleService/updateSaleApplyPass', {}, {})
					.save({
						'id': scope.saleobj.id,
						'apply_note': scope.saleobj.apply_note
					}, function (res) {
						if (res.errcode !== 0) {
							toaster.error({ title: "提示", body: res.errmsg });
							// toaster.error({ title: "提示", body: res.errmsg });
							return;
						}
						toaster.success({ title: "提示", body: "操作成功!" });
						scope.util.$modalInstance.close();
					});
			};

			scope.nopass = function () {
				if (!scope.saleobj.apply_note) {
					toaster.success({ title: "提示", body: '审核意见必填' })
					return false;
				}
				$resource('/api/ac/tc/ticketSaleService/updateSaleApplyNoPass', {}, {})
					.save({ 'id': scope.saleobj.id, 'apply_note': scope.saleobj.apply_note }, function (res) {
						if (res.errcode !== 0) {
							toaster.error({ title: "提示", body: res.errmsg });
							// toaster.error({ title: "提示", body: res.errmsg });
							return;
						}
						toaster.success({ title: "提示", body: "操作成功!" });
						scope.util.$modalInstance.close();
					});
			};

			scope.$watch('[saleobj, util.showFlag]', function (newValue, oldValue) {
				if (!scope.saleobj.hasOwnProperty('templete_lock_data_json')) {
					return false;
				}
				if (scope.util.showFlag) {
					for (var key in scope.saleobj.templete_lock_data_json) {
						(function (arg) {
							// $timeout(function () {
							var el = $(".saleobj-" + arg);
							$(el).show();
							// }, 800);
						})(key);//调用时参数   
					}
					if (scope.saleobj.templete_lock_data_json.hasOwnProperty('sms_type') && scope.saleobj.templete_lock_data_json.sms_type == '0') {
						$(".saleobj-message").show();
					}
				} else {
					for (var key in scope.saleobj.templete_lock_data_json) {
						(function (arg) {
							// $timeout(function () {
							var el = $(".saleobj-" + arg);
							$(el).hide();
							// }, 800);
						})(key);//调用时参数   
					}
					if (scope.saleobj.templete_lock_data_json.hasOwnProperty('sms_type') && scope.saleobj.templete_lock_data_json.sms_type == '0') {
						$(".saleobj-message").hide();
					}
				}
			}, true);
		}
	};
};

