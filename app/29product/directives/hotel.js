module.exports = function ($resource, $state, $http, $q, toaster) {
    return {
        restrict: 'AE',
        template: require('../views/hotel.html'),
        replace: true,
        scope: {
            'saleobj': '=',
            'util': '='
        },
        link: function (scope, elements, attrs) {
            scope.hotelobj = {
                'hotel_sale_code': scope.saleobj.code,
                'hotel_predetermined_range_start': 0,
                'hotel_predetermined_range_end': 0,
                'hotel_breakfast': '1',
                'hotel_booking_time_day': 0,
                'hotel_booking_time_hour': 0,
                'hotel_booking_time_minute': 0,
                'hotel_latest_cancellation_day': 0,
                'hotel_latest_cancellation_hour': 0,
                'hotel_latest_cancellation_minute': 0,
                'hotel_debit_type': '0',
                'hotel_debit_info': '',
                'hotel_check_in_start': 0,
                'hotel_check_in_end': 0,
                // 'hotel_pay_state' : '0',
                // 'hotel_back_state' : '0',
                'hotel_confirm' : '0'
            };

            //保存酒店信息
            scope.save = function () {
                $resource('/api/as/tc/salehotel/createsalehotel', {}, {}).save(scope.hotelobj , function (res) {
                    if (res.errcode !== 0) {
                        toaster.error({ title: "", body: res.errmsg });
                        return false;
                    }
                    toaster.success({ title: "", body: '修改成功' });
                    getHotelInfo();
                });
            }

            //保存酒店信息 >>> 返回列表页
            scope.saveAndBack = function () {
                $resource('/api/as/tc/salehotel/createsalehotel', {}, {}).save(scope.hotelobj , function (res) {
                    if (res.errcode !== 0) {
                        toaster.error({ title: "", body: res.errmsg });
                        return false;
                    }
                    toaster.success({ title: "", body: '修改成功' });
					scope.util.$modalInstance.close();
                });
            }

            //获取酒店信息
            function getHotelInfo() {
                $resource('/api/as/tc/salehotel/salehotelinfo', {}, {}).save({ 'hotel_sale_code': scope.saleobj.code }, function (res) {
                    if (res.errcode !== 0 && res.errcode !== 10003) {
                        toaster.error({ title: "", body: res.errmsg });
                        return false;
                    }
                    if (res.errcode !== 10003) {
                        scope.hotelobj = res.data;
                    }
                });
            }
            getHotelInfo();

            //根据扣款类型清除扣款详情value 放开即用
            // scope.$watch('hotelobj', function(newValue, oldValue){
            //     if (scope.hotelobj.debit_type != '2' && scope.hotelobj.debit_type != '3') {
            //         scope.hotelobj.debit_info = '';
            //     }
            // }, true);

        }
    };
};

