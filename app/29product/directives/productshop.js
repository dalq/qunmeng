module.exports = function ($resource, $state, $http, $q, toaster) {
	return {
		restrict: 'AE',
		template: require('../views/productshop.html'),
		replace: true,
		scope: {
			'saleobj': '=',
			'util': '=',
			// 'open' : '=',
		},
		link: function (scope, elements, attrs) {
			scope.tickettypeobj = {
				'id': '',
				'sale_code': scope.saleobj.code,
				'place_code': '',
				'periodstart': '',
				'periodend': '',
				'type': 'M',
				'appoint': '0'
			};
			scope.periodstart =
				{
					'lable': scope.util.date2str(new Date()),
					'opened': false
				};
			scope.periodend =
				{
					'lable': scope.util.date2str(new Date(new Date().getFullYear(), 11, 31)),
					'opened': false
				};
			scope.shopArr = [];
			scope.shopMap = {};
			scope.ticMap = {};
			scope.codeStr = {
				str: ''
			};
			//页面需要的数据
			scope.page = {};
			scope.load = function(){
				$resource('/api/as/tc/placeview/jlist', {}, {})
					.save(scope.tickettypeobj, function (res) {
						if (res.errcode !== 0) {
							toaster.error({ title: "提示", body: res.errmsg });
							return;
						}
						scope.page = {
							'viewlist': res.data
						}
						for (var index = 0; index < res.data.length; index++) {
							var element = res.data[index];
							scope.shopMap[element.code + ''] = element.name;
						}
						scope.codeStr.str = res.data[0].code;
						gettickettypedetail();
					});
			}
			scope.load();

			//添加票种
			scope.tempArr = [];
			scope.codeArr = [];
			scope.add = function () {
				if (scope.ticMap[scope.codeStr.str]) {
					toaster.warning({title: '', body: '请勿添加重复票种'});
					return false;
				}
				if (!scope.codeStr.str || !scope.shopMap[scope.codeStr.str]) {
					toaster.warning({title: '', body: '没有code或没有name'});
					return false;
				}
				scope.ticMap[scope.codeStr.str] = scope.shopMap[scope.codeStr.str];
				scope.tempArr.push({ 
					code: scope.codeStr.str, 
					view_name: scope.shopMap[scope.codeStr.str]
				});
				scope.codeArr.push(scope.codeStr.str);
			};

			scope.save = function (index) {
				if (index === undefined) {
					if(!scope.codeArr.length){
						toaster.warning({title: '', body: '请添加门店'});
						return false;
					}
					var periodstart = "";
					var periodend = "";
					if (typeof scope.periodstart.lable === 'string') {
						periodstart = scope.periodstart.lable;
					} else {
						periodstart = scope.util.date2str(scope.periodstart.lable);
					}
					if (typeof scope.periodend.lable === 'string') {
						periodend = scope.periodend.lable;
					} else {
						periodend = scope.util.date2str(scope.periodend.lable);
					}
					var para = {
						'name': scope.saleobj.name,
						'sale_code': scope.tickettypeobj.sale_code,
						'place_code': scope.codeArr.toString(),
						'periodstart': periodstart,
						'periodend': periodend,
						'type': 'M',
						'appoint': scope.tickettypeobj.appoint
					};
					scope.ticMap = {};
					scope.tickettypeobj.place_code = "";
					scope.tempArr = [];
					scope.codeArr = [];
				} else {
					var periodstart = "";
					var periodend = "";
					if (typeof scope.shopArr[index].start_date.lable === 'string') {
						periodstart = scope.shopArr[index].start_date.lable;
					} else {
						periodstart = scope.util.date2str(scope.shopArr[index].start_date.lable);
					}
					if (typeof scope.shopArr[index].end_date.lable === 'string') {
						periodend = scope.shopArr[index].end_date.lable;
					} else {
						periodend = scope.util.date2str(scope.shopArr[index].end_date.lable);
					}
					var placeStr = [];
					for (var i = 0; i < scope.shopArr[index].placeArr.length; i++) {
						placeStr[i] = scope.shopArr[index].placeArr[i].code
					}
					var para = {
						'id': scope.shopArr[index].id,
						'name': scope.shopArr[index].name,
						'sale_code': scope.tickettypeobj.sale_code,
						'place_code': placeStr.toString(),
						'periodstart': periodstart,
						'periodend': periodend,
						'type': 'M',
						'appoint': scope.shopArr[index].appoint
					};
				}
				$resource('/api/as/tc/salettype/save', {}, {})
					.save( para, function (res) {
						if (res.errcode !== 0) {
							toaster.error({ title: "提示", body: res.errmsg });
							return;
						}
						toaster.success({ title: "提示", body: '修改成功' });
						// gettickettypedetail();
						scope.load();
					});
			};

			scope.save2 = function (item) {
				var para = {
					id: item.id
				}
				$resource('/api/as/tc/salettype/save', {}, {})
					.save(para, function (res) {
						if (res.errcode !== 0) {
							toaster.error({ title: "", body: res.errmsg });
							return;
						}
						toaster.success({ title: "提示", body: '修改成功' });
						gettickettypedetail();
					});
			};

			//详细信息	删除
			scope.del1 = function (item) {
				var para = {
					id: item.id
				}
				// $resource('/api/as/tc/salettype/delete', {}, {})
				$resource('/api/ac/tc/ticketSaleService/updateDeleteSaleType', {}, {})
					.save(para, function (res) {
						if (res.errcode !== 0) {
							toaster.error({ title: "", body: res.errmsg });
							return;
						}
						toaster.success({ title: "提示", body: '删除成功' });
						gettickettypedetail();
					});
			}
			
			//详细信息	删除
			scope.del = function (index) {
				if(scope.tempArr.length == 1){
					if (!confirm('请确保至少有一个门店, 确定删除?')) {
						return false;
					}
				}
				scope.tickettypeobj.place_code = scope.tickettypeobj.place_code.replace(scope.tempArr[index]['code']  + ',', '');
				delete scope.ticMap[scope.tempArr[index]['code']];
				scope.tempArr.splice(index,1);
				scope.codeArr.splice(index,1);
			}
			
			//详细信息	删除
			scope.delPlace = function (index, parent_index) {
				if(scope.shopArr[parent_index].placeArr.length == 1){
					toaster.error({ title: "提示", body: '请确保至少有一个门店' });
					return false;
				}
				scope.shopArr[parent_index].placeArr.splice(index, 1);
				// scope.tickettypeobj.place_code = scope.tickettypeobj.place_code.replace(scope.tempArr[index]['code']  + ',', '');
				// delete scope.ticMap[scope.tempArr[index]['code']];
				// scope.tempArr.splice(index,1);
			}

			//获取票种详情
			function gettickettypedetail() {
				$resource('/api/as/tc/salettype/list', {}, {})
					.save({ 'sale_code': scope.saleobj.code }, function (res) {
						if (res.errcode !== 0) {
							toaster.error({ title: "提示", body: res.errmsg });
							return;
						}
						for (var key in scope.tickettypeobj) {
							if (res.data[0].hasOwnProperty(key)) {
								scope.tickettypeobj[key] = res.data[0][key]
							}
						}
						scope.tickettypeobj['place_code'] = '';
						scope.shopArr = res.data;
						for (var index = 0; index < scope.shopArr.length; index++) {
							var element = scope.shopArr[index];
							scope.shopArr[index].start_date = {
								'lable': element.periodstart? element.periodstart: scope.util.date2str(new Date()),
								'opened': false
							};
							scope.shopArr[index].end_date = {
								'lable': element.periodend? element.periodend: scope.util.date2str(new Date(new Date().getFullYear(), 11, 31)),
								'opened': false
							};
							scope.shopArr[index].place_code = element.view_code;
							var placeArr = element.view_code.split(',');
							for (var i = 0; i < placeArr.length; i++) {
								var element = angular.copy(placeArr[i])
								placeArr[i] = {
									name: scope.shopMap[element],
									code: element
								}
							}
							scope.shopArr[index].placeArr = placeArr;
						}
						// if (!res.data.length) {
						// 	return false;
						// }
						// var tmp = res.data[0];
						// for (var index = 0; index < res.data.length; index++) {
						// 	var element = res.data[index];
						// 	scope.shopArr.push({ 
						// 		code: element.view_code, 
						// 		view_name: scope.shopMap[element.view_code],
						// 		start_date: {
						// 			'lable': element.periodstart? element.periodstart: scope.util.date2str(new Date()),
						// 			'opened': false
						// 		},
						// 		end_date: {
						// 			'lable': element.periodend? element.periodend: scope.util.date2str(new Date()),
						// 			'opened': false
						// 		},
						// 		appoint: element.appoint
						// 	})
						// 	scope.ticMap[element.view_code] = scope.shopMap[element.view_code];
						// 	scope.tickettypeobj.place_code += element.view_code + ',';
						// }
						// var tempArr = res.data[0].view_code.split(',');
						// for (var index = 0; index < tempArr.length; index++) {
						// 	var element = tempArr[index];
						// 	scope.shopArr.push({ code: element, name: scope.shopMap[element] })
						// 	scope.ticMap[element] = scope.shopMap[element];
						// 	scope.tickettypeobj.place_code += element + ',';
						// }
					});
			}
			scope.dateOpen = function ($event, item) {
				$event.preventDefault();
				$event.stopPropagation();
				item.opened = true;
			};
		}
	};
};

