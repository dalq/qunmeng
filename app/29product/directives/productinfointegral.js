module.exports = function($resource, $state, $http, $q,toaster){

	return {

		restrict : 'AE',
		template : require('../views/productinfointegral.html'),
		replace:true,
		scope:{
			'saleobj' : '=',
			'util' : '=',
		},
		link : function(scope, elements, attrs){
			scope.obj = {
				'integral_price' : ''
			}

			$resource('/api/as/mc/mergoods/getGoodsInfoByCode', {}, {})
			.save({'sale_code' : scope.saleobj.code}, function(res){

				scope.obj.integral_price = res.data.max_integral;


			});

			scope.salejfsave = function(){
				scope.obj.integral_sale_code = scope.saleobj.code;
				scope.obj.integral_type = '0';
				var array = [];
				array.push(scope.obj);
				$resource('/api/ac/tc/ticketSaleIntegralService/saveSaleInteral', {}, {})
				.save({'integral_sale_code':scope.obj.integral_sale_code,'integral_price':scope.obj.integral_price,'list' : array}, function(res){
					if(res.errcode !== 0)
					{
						toaster.errot({ title: "", body: res.errmsg })
						return;
					}
					toaster.success({title:"",body:"操作成功!"});					
				});

			};
		   
		}

	};
};

