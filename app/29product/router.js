/**
* 子模块路由
* dlq
*/

var router = function ($urlRouterProvider, $stateProvider) {

    $stateProvider

        //产品列表
        .state('app.supplyProductList', {
            url: "/product/supplyList?sale_type",
            views: {
                'main@': {
                    template: require('./views/supplyProductList.html'),
                    controller: 'supplyProductList',
                }
            },
            resolve: {
                date2str: function (utilservice) {
                    return utilservice.getDate;
                },
                str2date: function (utilservice) {
                    return utilservice.str2date;
                },
                saleupdate: function (productservice) {
                    return productservice.saleupdate();
                },
                saleup: function (productservice) {
                    return productservice.saleup();
                },
                saledown: function (productservice) {
                    return productservice.saledown();
                },
                talist: function (productservice) {
                    return productservice.talist;
                },
                sellerListno: function (productservice) {
                    return productservice.sellerListno();
                },
                sellerList: function (productservice) {
                    return productservice.sellerList();
                },
                tstcreateno: function (productservice) {
                    return productservice.tstcreateno();
                },
                tstcreate: function (productservice) {
                    return productservice.tstcreate();
                },
                tststartno: function (productservice) {
                    return productservice.tststartno();
                },
                tststart: function (productservice) {
                    return productservice.tststart();
                },
                tststopno: function (productservice) {
                    return productservice.tststopno();
                },
                tststop: function (productservice) {
                    return productservice.tststop();
                }
            }
        })

        //产品编辑
        .state('app.editProduct', {
            url: '/product/edit/:id',
            views: {
                'main@': {
                    template: require('./views/product.html'),
                    controller: 'editProduct',
                }
            },
            resolve: {
                productid: function () {
                    return '';
                },
                what: function () {
                    return 'edit';
                },
                date2str: function (utilservice) {
                    return utilservice.getDate;
                },
                str2date: function (utilservice) {
                    return utilservice.str2date;
                },
                $modalInstance: function () {
                    return undefined;
                },
                auditing: function () {
                    return false;
                }
            }
        })

        //待审核产品列表
        .state('app.applyProductList', {
            url: '/product/applyProductList?type',
            views: {
                'main@': {
                    template: require('./views/applyProductList.html'),
                    controller: 'applyProductList',
                }
            },
            resolve: {
                date2str: function (utilservice) {
                    return utilservice.getDate;
                },
                str2date: function (utilservice) {
                    return utilservice.str2date;
                },
                saleupdate: function (productservice) {
                    return productservice.saleupdate();
                },
                saleup: function (productservice) {
                    return productservice.saleup();
                },
                saledown: function (productservice) {
                    return productservice.saledown();
                },
                talist: function (depositservice) {
                    return depositservice.talist;
                },

                sellerListno: function (productservice) {
                    return productservice.sellerListno();
                },
                tstcreateno: function (productservice) {
                    return productservice.tstcreateno();
                },
                tststartno: function (productservice) {
                    return productservice.tststartno();
                },
                tststopno: function (productservice) {
                    return productservice.tststopno();
                },


            }
        })

        //商客专属设置
		.state('app.skprofit', {
			url: '/product/skprofit.html/:code/:name/:guide_price/:cost_price',
            views: {
                'main@': {
                    template: require('./views/skprofit.html'),
                    controller: 'skprofit',
                }
            },
			resolve: {
				
				getDate: function (utilservice) {
					return utilservice.getDate;
				}


			}
		})


        //销售品类型管理
		.state('app.salecategory', {
			url: '/salecategory',
            views: {
                'main@': {
                        controller: 'salecategory',
                        template: require('./views/salecategory.html'),
                }
            },
			resolve: {
				salecategorylist: function (productservice) {
					return productservice.salecategorylist();
				},
				salecategoryinsert: function (productservice) {
					return productservice.salecategoryinsert();
				},
				salecategorydelete: function (productservice) {
					return productservice.salecategorydelete();
				},
				dictbytypelist: function (productservice) {
					return productservice.dictbytypelist;
				}
			}
		})

          //创建销售品
		.state('app.newproduct', {
			url: '/product/new/:params',
             views: {
                'main@': {
                    controller: 'newproduct',
                    template: require('./views/productCreate.html')
                }
             },
			resolve: {
				productid: function () {
					return '';
				},
				what: function () {
					return 'edit';
				},
				date2str: function (utilservice) {
					return utilservice.getDate;
				},
				str2date: function (utilservice) {
					return utilservice.str2date;
				},
				$modalInstance: function () {
					return undefined;
				},
				auditing: function () {
					return false;
				}
			}
		})


        //待审核产品列表
		.state('app.backTicketApply', {
			url: '/product/backTicketApply.html',
             views: {
                'main@': {
                        controller: 'backTicketApply',
                        template: require('./views/backTicketApply.html'),
                }
             },
			resolve: {
				date2str: function (utilservice) {
					return utilservice.getDate;
				},
				str2date: function (utilservice) {
					return utilservice.str2date;
				},
				saleupdate: function (productservice) {
					return productservice.saleupdate();
				},
				saleup: function (productservice) {
					return productservice.saleup();
				},
				saledown: function (productservice) {
					return productservice.saledown();
				},
				talist: function (productservice) {
					return productservice.talist;
				},
				sellerListno: function (productservice) {
					return productservice.sellerListno();
				},
				tstcreateno: function (productservice) {
					return productservice.tstcreateno();
				},
				tststartno: function (productservice) {
					return productservice.tststartno();
				},
				tststopno: function (productservice) {
					return productservice.tststopno();
				},
				getDate: function (utilservice) {
					return utilservice.getDate;
				},


			}
		})

        //商家发布的可预约产品列表
        .state('app.appoint',{
            url: '/appoint/:id',
              views: {
                'main@': {
                        controller : 'appoint',        
                        template: require('./views/appointmentlist.html'),
               }
              },
            resolve:{
               
                getDate : function(utilservice){
                	 return utilservice.getDate;
                },
                str2date : function(utilservice){
                return utilservice.str2date;
                },
                date2str : function(utilservice) {
                    return utilservice.date2str;
                },
               
                
            }
        })

        //设置预约
        .state('app.setAppoint',{
            url: '/setAppoint/:id',
             views: {
                'main@': {
                        controller : 'setAppoint',
                        template: require('./views/setappointment.html'),
                }
             },
            resolve:{
                productid: function () {
					return '';
				},
				what: function () {
					return 'edit';
				},
				date2str: function (utilservice) {
					return utilservice.getDate;
				},
				str2date: function (utilservice) {
					return utilservice.str2date;
				},
				$modalInstance: function () {
					return undefined;
				},
				auditing: function () {
					return false;
				}
                
            }
        })

         //预约人列表
        .state('app.customerList',{
            url: '/customerList/:id',
             views: {
                'main@': {
                        controller : 'customerlist11',
                        template: require('./views/customerlist.html'),
                }
             },
            resolve:{
                findUserInfoList : function(productservice){
                    return productservice.findUserInfoList();
                },
                getDate : function(utilservice){
                	 return utilservice.getDate;
                }
                
            }
        })

         //模板列表
        .state('app.productTemplate',{
            url: '/productTemplate/list',
             views: {
                'main@': {
                        controller : 'productTemplate',
                        template: require('./views/productTemplate.html'),
                }
             },
            resolve:{
            }
        })

         //创建模板
        .state('app.createTemplate',{
            url: '/productTemplate/createTemplate',
             views: {
                'main@': {
                    controller : 'createTemplate',
                    template: require('./views/createTemplate.html')
                }
             },
            resolve:{
				date2str: function (utilservice) {
					return utilservice.getDate;
				},
				str2date: function (utilservice) {
					return utilservice.str2date;
				},
				$modalInstance: function () {
					return undefined;
				}
            }
        })

         //模板列表
        .state('app.templateList',{
            url: '/productTemplate/templateList',
             views: {
                'main@': {
                    controller : 'templateList',
                    template: require('./views/templateList.html')
                }
             },
            resolve:{
				date2str: function (utilservice) {
					return utilservice.getDate;
				},
				str2date: function (utilservice) {
					return utilservice.str2date;
				},
				$modalInstance: function () {
					return undefined;
				}
            }
        })

         //模板[详情][编辑]
        .state('app.editTemplate',{
            url: '/productTemplate/editTemplate/:id',
             views: {
                'main@': {
                    controller : 'editTemplate',
                    template: require('./views/editTemplate.html')
                }
             },
            resolve:{
				date2str: function (utilservice) {
					return utilservice.getDate;
				},
				str2date: function (utilservice) {
					return utilservice.str2date;
				},
				$modalInstance: function () {
					return undefined;
				},
				id: function () {
					return undefined;
				}
            }
        })

         //模板[详情][编辑]
        .state('app.dockingproduct',{
            url: '/product/dockingproduct',
             views: {
                'main@': {
                    controller : 'dockingproduct',
                    template: require('./views/dockingproduct.html')
                }
             },
            resolve:{
				date2str: function (utilservice) {
					return utilservice.getDate;
				},
				str2date: function (utilservice) {
					return utilservice.str2date;
				}
            }
        })

        //更新数据列表
        .state('app.update_data_list',{
            url: '/product/update_data_list',
             views: {
                'main@': {
                    controller : 'update_data_list',
                    template: require('./views/update_data_list.html')
                }
             },
            resolve: {
                date2str: function (utilservice) {
                    return utilservice.getDate;
                },
                str2date: function (utilservice) {
                    return utilservice.str2date;
                },
                saleupdate: function (productservice) {
                    return productservice.saleupdate();
                },
                saleup: function (productservice) {
                    return productservice.saleup();
                },
                saledown: function (productservice) {
                    return productservice.saledown();
                },
                talist: function (productservice) {
                    return productservice.talist;
                },
                sellerListno: function (productservice) {
                    return productservice.sellerListno();
                },
                sellerList: function (productservice) {
                    return productservice.sellerList();
                },
                tstcreateno: function (productservice) {
                    return productservice.tstcreateno();
                },
                tstcreate: function (productservice) {
                    return productservice.tstcreate();
                },
                tststartno: function (productservice) {
                    return productservice.tststartno();
                },
                tststart: function (productservice) {
                    return productservice.tststart();
                },
                tststopno: function (productservice) {
                    return productservice.tststopno();
                },
                tststop: function (productservice) {
                    return productservice.tststop();
                }
            }
        })

        //协议价管理
        .state('app.agreementPrice', {
            url: '/agreementprice/list',
            views: {
                'main@': {
                    template: require('./views/agreementPrice.html'),
                    controller: 'agreementPrice',
                }
            },
            resolve: {
                
            }
        })

        //协议价变更记录
        .state('app.agreementRecord', {
            url: '/agreementrecord/list',
            views: {
                'main@': {
                    template: require('./views/agreementPriceRecord.html'),
                    controller: 'agreementPriceRecord',
                }
            },
            resolve: {
                
            }
        })

        //平台仓库
        .state('app.platform_repository', {
            url: '/platformrepository/list',
            views: {
                'main@': {
                    template: require('./views/platform_repository.html'),
                    controller: 'platformRepository',
                }
            },
            resolve: {
                
            }
        })

        //平台仓库
        .state('app.repository', {
            url: '/repository/list',
            views: {
                'main@': {
                    template: require('./views/repository.html'),
                    controller: 'repository',
                }
            },
            resolve: {
                
            }
        })

        //平台仓库
        .state('app.my_repository', {
            url: '/myrepository/list',
            views: {
                'main@': {
                    template: require('./views/my_repository.html'),
                    controller: 'myRepository',
                }
            },
            resolve: {
                
            }
        })

        //产品优惠
        .state('app.setProductDiscount', {
            url: '/productdiscount/list',
            views: {
                'main@': {
                    template: require('./views/setProductDiscount.html'),
                    controller: 'setProductDiscount',
                }
            },
            resolve: {
                
            }
        })

        //产品优惠详情页
        .state('app.discountInfo', {
            url: '/discountInfo/list/:id/:discount',
            views: {
                'main@': {
                    template: require('./views/discountInfo.html'),
                    controller: 'discountInfo',
                }
            },
            resolve: {
                
            }
        })

        //产品优惠详情页
        .state('app.channelList', {
            url: '/channel/list',
            views: {
                'main@': {
                    template: require('./views/channelList.html'),
                    controller: 'channelList',
                }
            },
            resolve: {
                
            }
        })

        //产品优惠详情页
        .state('app.myChannel', {
            url: '/mychannel/list',
            views: {
                'main@': {
                    template: require('./views/myChannel.html'),
                    controller: 'myChannel',
                }
            },
            resolve: {
                
            }
        })

        //产品优惠详情页
        .state('app.channelSale', {
            url: '/channelsale/list/:code/:name',
            views: {
                'main@': {
                    template: require('./views/setSaleList.html'),
                    controller: 'setSaleList',
                }
            },
            resolve: {
                
            }
        })

        //查看产品授权断层信息
        .state('app.faultList', {
            url: '/fault/list',
            views: {
                'main@': {
                    template: require('./views/faultList.html'),
                    controller: 'faultList',
                }
            },
            resolve: {
                
            }
        })

        ;

};

module.exports = router;