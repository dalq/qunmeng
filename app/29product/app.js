var App = angular.module('product', []);

App.config(require('./router'));
App.factory('productservice', require('./service'));

App.controller('editProduct',require('./controllers/editProduct'));
App.controller('infoProduct',require('./controllers/infoProduct'));
App.controller('supplyProductList',require('./controllers/supplyProductList'));
App.controller('applyProductList',require('./controllers/applyProductList'));

App.controller('distribution',require('./controllers/distribution'));
App.controller('nodistribution',require('./controllers/nodistribution'));

App.controller('skprofit',require('./controllers/skprofit'));
App.controller('salecategorycreate',require('./controllers/salecategorycreate'));
App.controller('salecategory',require('./controllers/salecategory'));
App.controller('newproduct',require('./controllers/newproduct'));
App.controller('backTicketApply',require('./controllers/backTicketApply'));
App.controller('backTickketApplyDetails',require('./controllers/backTickketApplyDetails'));

App.directive('productbaseinfo',require('./directives/baseinfo'));
App.directive('producttickettype',require('./directives/tickettype'));
App.directive('productshop',require('./directives/productshop'));
App.directive('productaffirm',require('./directives/affirm'));
App.directive('productintegral',require('./directives/integral'));
App.directive('productflashsale',require('./directives/flashsale'));
App.directive('productshareprofit',require('./directives/shareprofit'));
App.directive('producthotel',require('./directives/hotel'));
App.directive('restrictrule',require('./directives/restrictrule'));
App.directive('imgtextinfo',require('./directives/imgtextinfo'));
App.directive('govsubsidy',require('./directives/govsubsidy'));
App.directive('juyousubsidy',require('./directives/juyousubsidy'));

App.directive('productinfobaseinfo',require('./directives/productinfobaseinfo'));
App.directive('productinfotickettype',require('./directives/productinfotickettype'));
App.directive('productinfoshop',require('./directives/productinfoshop'));
App.directive('productinfoaffirm',require('./directives/productinfoaffirm'));
App.directive('productinfointegral',require('./directives/productinfointegral'));
App.directive('productinfoflashsale',require('./directives/productinfoflashsale'));
App.directive('productinfoshareprofit',require('./directives/productinfoshareprofit'));
App.directive('productinforestrictrule',require('./directives/productinforestrictrule'));
App.directive('productinfoimgtextinfo',require('./directives/productinfoimgtextinfo'));
App.directive('productinfogovsubsidy',require('./directives/productinfogovsubsidy'));
App.directive('productinfojuyousubsidy',require('./directives/productinfojuyousubsidy'));
App.directive('productinfohotel',require('./directives/productinfohotel'));
App.directive('priceSetting',require('./directives/priceSetting'));
App.directive('productinfoPriceSetting',require('./directives/productinfoPriceSetting'));
App.directive('productinfoAllowDistributor',require('./directives/productinfoAllowDistributor'));
App.directive('productinfoNoAllowDistributor',require('./directives/productinfoNoAllowDistributor'));
App.directive('productViewCell', require('./directives/productViewCell'));

App.controller('appoint',require('./controllers/appointmentlist'));
App.controller('setAppoint',require('./controllers/setappointment'));
App.directive('appointbaseinfo',require('./directives/appointbaseinfo'));
App.directive('appointscreening',require('./directives/appointscreening'));
App.directive('appointticket',require('./directives/appointticket'));
App.controller('customerlist11',require('./controllers/customerlist11'));
App.controller('updatePriceCalendar',require('./controllers/updatePriceCalendar'));
App.controller('customPriceCalendar',require('./controllers/customPriceCalendar'));

//商客设置tab页
App.directive('shangkesetting', require('./directives/shangkeSetting'));
// 商客freestyle设置tab页
App.directive('shangkestylesetting', require('./directives/shangkeStylesetting'));





App.controller('productTemplate',require('./controllers/productTemplate'));
App.controller('createTemplate',require('./controllers/createTemplate'));
App.controller('editTemplate',require('./controllers/editTemplate'));
App.controller('productTemplateInfo',require('./controllers/productTemplateInfo'));
App.controller('templateList',require('./controllers/templateList'));
App.controller('productTemplateInfoEdit',require('./controllers/productTemplateInfoEdit'));
App.directive('productTemplateBase', require('./directives/productTemplateBase'));
App.directive('productTemplateLimit', require('./directives/productTemplateLimit'));
App.directive('productTemplateImage', require('./directives/productTemplateImage'));
App.directive('productTemplateBaseInfo', require('./directives/productTemplateBaseInfo'));
App.directive('productTemplateLimitInfo', require('./directives/productTemplateLimitInfo'));
App.directive('productTemplateImageInfo', require('./directives/productTemplateImageInfo'));
App.controller('dockingproduct',require('./controllers/dockingproduct'));
App.directive('supplyXiaojing',require('./directives/supply_xiaojing'));
App.directive('supplyZiwoyou',require('./directives/supply_ziwoyou'));
App.controller('xiaojing_detailinfo',require('./controllers/xiaojing_detailinfo'));
App.controller('ziwoyou_detailinfo',require('./controllers/ziwoyou_detailinfo'));
//lc更新数据
App.controller('update_data_list',require('./controllers/update_data_list'));//update_order_list
App.controller('update_order_list',require('./controllers/update_order_list'));//update_order_list
//lc t_ticket_list
App.controller('t_ticket_list',require('./controllers/t_ticket_list'));
App.controller('salecategoryChildren',require('./controllers/salecategoryChildren'));
//合作商协议价管理
App.controller('agreementPrice',require('./controllers/agreementPrice'));
App.controller('setAgreementPrice',require('./controllers/setAgreementPrice'));
App.controller('agreementPriceRecord',require('./controllers/agreementPriceRecord'));
//平台仓库
App.controller('platformRepository',require('./controllers/platform_repository'));
App.controller('repository',require('./controllers/repository'));
App.controller('myRepository',require('./controllers/my_repository'));
App.controller('setPtPgrant',require('./controllers/setPtPgrant'));
App.controller('setFloatingPrice',require('./controllers/setFloatingPrice'));
App.controller('setChannel',require('./controllers/setChannel'));
App.controller('setFxs',require('./controllers/setFxs'));
//设置产品优惠
App.controller('setProductDiscount',require('./controllers/setProductDiscount'));
App.controller('setDiscount',require('./controllers/setDiscount'));
App.controller('discountInfo',require('./controllers/discountInfo'));
App.controller('addDiscountInfo',require('./controllers/addDiscountInfo'));

App.controller('addTime',require('./controllers/addTime'));
App.controller('dateSet',require('./controllers/dateSet'));

//销售品渠道管理
App.controller('channelList',require('./controllers/channelList'));
App.controller('addChannel',require('./controllers/addChannel'));
App.controller('myChannel',require('./controllers/myChannel'));
App.controller('setSaleList',require('./controllers/setSaleList'));
App.controller('addSaleList',require('./controllers/addSaleList'));
App.controller('faultList',require('./controllers/faultList'));


module.exports = App;