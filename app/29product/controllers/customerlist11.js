module.exports = function($scope, $stateParams, $state,$modal,FileUploader,findUserInfoList,getDate,toaster,$resource){
    /* 分页
     * ========================================= */
    $scope.maxSize = 5;            //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10;         //每页显示几条
    
     $scope.date = {
                 //'lable': date2str2(new Date()),
                'opened': false
    }
     $scope.dateOpen = function ($event, item) {
                $event.preventDefault();
                $event.stopPropagation();
                item.opened = true;
    };
    function date2str2(d) {
     if (d === undefined) {
                    return "";
        }
                var month = (d.getMonth() + 1).toString();
                var day = d.getDate().toString();
                if (month.length == 1) month = '0' + month;
                if (day.length == 1) day = '0' + day;
                return d.getFullYear() + "-" + month + "-" + day;
     }
    //有效区间
    // $scope.section = {};
    // $scope.section.start = {};
    // $scope.section.start.date = new Date();
 
    // $scope.section.end = {};
    // $scope.section.end.date = new Date();

    // $scope.open = function(obj) {
    //     obj.opened = true;
    // };
    $scope.info = {
        'name' : '',
        'mobile' : '',
        'title' : '',
        'start_time' :'' 
    }

    $scope.search = function(){

         if($scope.date.lable){
             $scope.tour_date=date2str2($scope.date.lable);
             
        }else{
             $scope.tour_date='';
       }
        $scope.info.start_time =  $scope.tour_date;
        var para = {
            pageNo:$scope.bigCurrentPage, 
            pageSize:$scope.itemsPerPage 
        };
        para = angular.extend($scope.info,para);
         $resource('/api/as/tc/appoint/findAppointUserList', {}, {}).save(para,function(res){
           
            if(res.errcode!=0){
                toaster.success({title:"",body:res.errmsg});
                return;
            }
            $scope.objs = res.data.results;
            $scope.bigTotalItems = res.data.totalRecord;
        })
    };
    $scope.search();

	$scope.myKeyup = function (e) {

		//IE 编码包含在window.event.keyCode中，Firefox或Safari 包含在event.which中
		var keycode = window.event ? e.keyCode : e.which;
		if (keycode == 13) {
			$scope.search();
		}
	};
    
    
    

};