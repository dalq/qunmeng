module.exports = function ($scope, $modalInstance, $resource, $modal, data, toaster, time){


    $scope.dateOpen = function ($event, item) {
        $event.preventDefault();
        $event.stopPropagation();
        item.opened = true;
    };

    $scope.dateOpen1 = function ($event, item) {
        $event.preventDefault();
        $event.stopPropagation();
        item.opened = true;
    };

    init();
    function init(){
        $scope.date = angular.copy(data);
        $scope.date1 = angular.copy(data);
        $scope.sel_time = [];
        if(time){
            $scope.sel_time = time.split(',');
        }
        
    }

    $scope.add = function (){
        if($scope.sel_time.length > 9){
            toaster.error({title: "", body: '最多保存10个时间段' })
            return;
        }
        var time1 = typeof $scope.date.lable === 'string'? $scope.date.lable:$scope.date2str($scope.date.lable);
        var time2 = typeof $scope.date1.lable === 'string'? $scope.date1.lable:$scope.date2str($scope.date1.lable);
        if(time1 > time2){
            toaster.error({title: "", body: '开始日期不能大于结束日期' })
            return;
        }
        var time = time1 + '~' + time2;
        for(var i = 0; i < $scope.sel_time.length; i++){
            if($scope.sel_time[i] == time){
                toaster.error({title: "", body: '不能重复添加日期' })
                return;
            }
        }
        $scope.sel_time.push(time);
    }

    $scope.remove = function (index){
        $scope.sel_time.splice(index,1);
    }

    $scope.date2str = function (objDate){
        if (angular.isDate(objDate)) {
            var y = objDate.getFullYear();
            var m = objDate.getMonth() + 1;
            var d = objDate.getDate();
            m = m < 10? '0' + m: m;
            d = d < 10? '0' + d: d;
            return y + '-' + m + '-' + d;
        } else {
            return '错误格式';
        }
    }

    $scope.back = function (){
        $modalInstance.dismiss('cancel');
    }

    $scope.ok = function (){
        $modalInstance.close($scope.sel_time);
    }


};