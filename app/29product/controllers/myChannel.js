module.exports = function ($scope, $state, $resource, $modal, toaster){
	var bindChannel = '';
	
	//查询渠道列表
	$scope.search = function (pageNo){
		var para = {
			'pageNo': pageNo || $scope.currentPage,
			'pageSize': $scope.itemsPerPage,
			'channel_list_name': $scope.searchform.channel_name
		}
		$resource('/api/as/tc/salechannel/findAllList', {}, {}).save(para, function(res){
			if(res.errcode === 0){
				$scope.list = res.data.results;
				$scope.totalItems = res.data.totalRecord;
				angular.forEach($scope.list, function(item){
					if(bindChannel.indexOf(item.channel_list_code) != -1){
						item.selected = true;
					}
				});
			}else{
				toaster.error({title: '', body: res.errmsg});
			}
		});
	}

	//查询默认渠道
	$scope.getBindChannel = function() {
		$resource('/api/as/sc/office/getBindChannel', {}, {}).save({}, function(res){
			if(res.errcode === 0){
				bindChannel = res.data.bind_channel;
				$scope.search(1);
			}else{
				toaster.error({title: '', body: res.errmsg});
			}
		});
	}

	init();
    //初始化列表
    function init(){
		$scope.currentPage = 1;		//当前页码
		$scope.itemsPerPage = 10;	//每页显示几条
		$scope.searchform = {};
		$scope.getBindChannel();
	}

	//查看销售品
	$scope.getSaleType = function(info) {
		$state.go('app.channelSale', {'name': info.channel_list_name, 'code': info.channel_list_code});
	}

	//设置或取消默认渠道
	$scope.change = function(info) {
		info.used = true;
		var para = {
			'type': info.selected ? '1' : '0',
			'bind_channel': info.channel_list_code
		}
		$resource('/api/as/sc/office/setBindChannel', {}, {}).save(para, function(res){
			if(res.errcode === 0){
				toaster.success({title: '', body: '设置默认渠道成功'});
				info.used = false;
			}else{
				toaster.error({title: '', body: '设置默认渠道失败,请重试'});
			}
		});
	}

};