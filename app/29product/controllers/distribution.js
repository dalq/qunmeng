module.exports = function ($scope, talist, sellerList, code, tstcreate, tststart, tststop, title, $modalInstance, $resource, toaster) {


    $scope.obj = {
        'title': title
    };

    $scope.user = {};

    $resource('/api/as/sc/office/getofficelist', {}, {})
        .save({}, function (res) {
            if (res.errcode === 0) {
                $scope.taarr = res.data;
                $scope.obj.seller_code = $scope.taarr[0].CODE;
            }
            else {
                toaster.error({ title: "提示", body: res.errmsg });
            }

        });

    function load() {
        sellerList.get({ 'sale_code': code }, function (res) {

            if (res.errcode === 0) {
                $scope.sellers = res.data;
            }
            else {
                toaster.error({ title: "提示", body: res.errmsg });
            }

        });
    }
    load();

    $scope.createCompany = function () {
        tstcreate.save({ 'sale_code': code, 'company_code': $scope.obj.seller_code }, function (res) {
            if (res.errcode === 0) {
                load();
            }
            else {
                toaster.error({ title: "提示", body: res.errmsg });
            }

        });
    };

    $scope.createUser = function () {
        tstcreate.save({ 'sale_code': code, 'company_code': $scope.obj.seller_code }, function (res) {
            if (res.errcode === 0) {
                load();
            }
            else {
                toaster.error({ title: "提示", body: res.errmsg });
            }

        });
    };

    $scope.start = function (sellercode) {
        tststart.save({ 'sale_code': code, 'company_code': sellercode }, function (res) {
            if (res.errcode === 0) {
                load();
            }
            else {
                toaster.error({ title: "提示", body: res.errmsg });
            }
        });
    };

    $scope.stop = function (sellercode) {
        tststop.save({ 'sale_code': code, 'company_code': sellercode }, function (res) {
            if (res.errcode === 0) {
                load();
            }
            else {
                toaster.error({ title: "提示", body: res.errmsg });
            }
        });
    };
    $scope.userArray = [];
    $scope.getUser = function () {
        if (!$scope.obj.seller_code) {
            return false;
        }
        $resource('/api/ac/sc/systemUserService/getUserForTCList', {}, {}).save({ 'code': $scope.obj.seller_code }, function (res) {
            if (res.errcode === 0) {
                $scope.userArray = res.data;
                $scope.user.code = $scope.userArray[0].login_name;
            }
            else {
                toaster.error({ title: "提示", body: res.errmsg });
            }

        });
    }

    $scope.cancel = function () {
        $modalInstance.close();
    }


};