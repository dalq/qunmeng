module.exports = function ($scope, $modalInstance, $resource, $modal, date, toaster) {

    $scope.date =  {
        'lable': angular.copy(date),
        'opened': false
    }

    $scope.dateOpen = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.date.opened = true;
    };

    $scope.date2str = function (objDate) {
        if (angular.isDate(objDate)) {
            var y = objDate.getFullYear();
            var m = objDate.getMonth() + 1;
            var d = objDate.getDate();
            m = m < 10 ? '0' + m : m;
            d = d < 10 ? '0' + d : d;
            return y + '-' + m + '-' + d;
        } else {
            return '错误格式';
        }
    }

    $scope.back = function () {
        $modalInstance.dismiss('cancel');
    }

    $scope.ok = function () {
        if(typeof $scope.date.lable === 'String'){
            $modalInstance.close($scope.date.lable);
        } else {
            $modalInstance.close($scope.date2str($scope.date.lable));
        }
    }


};