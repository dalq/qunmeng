module.exports = function ($scope, $state, $resource, $modal, toaster){

	
	//加载已经设置协议价的机构列表
	$scope.loadList = function (){
		var para = {
			'pageNo': $scope.currentPage,
			'pageSize': $scope.itemsPerPage
		}
        $resource('/api/as/tc/agreementprice/getList', {}, {}).save(para, function(res){
			if(res.errcode === 0){
				$scope.list = res.data.results;
				$scope.totalItems = res.data.totalRecord;
			}else{
				toaster.error({title: '', body: res.errmsg});
			}
		});
	}

	//加载登录账号的协议价信息
	$scope.loadOfficeInfo = function (){
		$resource('/api/ac/tc/agreementPriceService/setOfficeInfo', {}, {}).save({}, function(res){
			if(res.errcode === 0){
				$scope.officeInfo = res.data;
				$scope.officeInfo.id = $scope.officeInfo.id || '0';
			} else {
				toaster.error({title: '', body: '加载个人信息失败'});
			}
		});
	}

	//加载可添加下级协议价的下拉菜单
	$scope.loadOfficeList = function (){
		$resource('/api/ac/tc/agreementPriceService/getOfficeList', {}, {}).save({}, function(res){
			if(res.errcode === 0){
				$scope.officeList = res.data;
			}
		});
	}

    init();
    //初始化列表
    function init(){
		$scope.currentPage = 1;			//当前页码
		$scope.itemsPerPage = 10;		//每页显示几条	
		$scope.loadOfficeInfo();
		$scope.loadList();
		$scope.loadOfficeList();
	}

    //添加一个新的协议金额 && 修改协议金额
    $scope.setMoney = function (item){
		var param = {};
		if(item){
			param.flag = false;
			param.item = item;
		} else {
			param.flag = true;
			param.item = $scope.officeList;
		}
        var modalInstance = $modal.open({
			template: require('../views/setAgreementPrice.html'),
			controller: 'setAgreementPrice',
			size: 'lg',
			resolve: {
				param : function () {
					return param;
				},
				officeInfo : function () {
					return $scope.officeInfo;
				}
			}
		});
		//关闭模态框刷新页面
		modalInstance.result.then(function(result) {
			if(param.flag){
				$scope.loadList();
				$scope.loadOfficeList();
			} else if(result == $scope.officeInfo){
				$scope.loadOfficeInfo()
			} else {
				$scope.loadList();
			}
		});
	}

    //删除
    $scope.delete = function(item){
		if(confirm('确认删除协议价信息吗？')==true){
			var para = {
				'id': item.id,
				'target_company_code': item.target_company_code
			}
			$resource('/api/ac/tc/agreementPriceService/deleteAgreementPrice', {}, {}).save(para, function(res){
                if(res.errcode === 0){
					init();
                } else {
                    toaster.error({title: '', body: res.errmsg});
                }
            });
		}
	}
	
	//查看协议价变更记录
    $scope.recrod = function(){
		$state.go('app.agreementRecord', {})
	}

};