module.exports = function ($scope, $state, $resource, $modal, toaster){


	//加载已经设置协议价的机构列表
	$scope.loadList = function (pageNo){
		var para = {
			'pageNo': pageNo || $scope.currentPage,
			'pageSize': $scope.itemsPerPage,
			'target_company_code': $scope.sea.code
		}
        $resource('/api/as/tc/agreementprice/getPriceRecordList', {}, {}).save(para, function(res){
			if(res.errcode === 0){
				$scope.record_list = res.data.results;
				$scope.totalItems = res.data.totalRecord;
			}else{
				toaster.error({title: '', body: res.errmsg});
			}
		});
	};

	//加载登录账号的协议价信息
	$scope.loadOfficeInfo = function (){
		$resource('/api/ac/tc/agreementPriceService/setOfficeInfo', {}, {}).save({}, function(res){
			if(res.errcode === 0){
				$scope.officeInfo = res.data;
			} else {
				toaster.error({title: '', body: '加载个人信息失败'});
			}
		});
	}

	//加载登录账号的协议价信息
	$scope.loadSearchList = function (){
		$resource('/api/as/tc/agreementprice/getSimPriceRecordList', {}, {}).save({}, function(res){
			if(res.errcode === 0){
				$scope.company_list = res.data;
			}
		});
	}

    init();
    //初始化列表
    function init(){
		$scope.currentPage = 1;			//当前页码
		$scope.itemsPerPage = 10;		//每页显示几条
		$scope.sea = {};
		$scope.loadList();
		$scope.loadSearchList();
		$scope.loadOfficeInfo();
	}

	//返回
	$scope.back = function (){
		$state.go('app.agreementPrice', {});
	}

};