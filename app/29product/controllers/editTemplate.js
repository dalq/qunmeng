
module.exports = function ($scope, $stateParams, $http, $q, FileUploader, id, str2date, date2str, $resource, toaster, $modalInstance, $timeout) {

	//产品对象，保存着产品的基本信息
	$scope.saleobj = {
		// 'templete_code': $scope.params.templete_code,
		// 'id':'',
		// 'name_flag': $scope.params.templete_lock_data_json.name,
		// 'sms_type_flag': $scope.params.templete_lock_data_json.sms_type
	};

	//模板信息
	$scope.template = {
        id: id || $stateParams.id
    };

	$scope.templateBaseInfo = {};

	$scope.util = {
		'str2date': str2date,
		'date2str': date2str,
		'$modalInstance': $modalInstance
	}
	//销售品功能列表
	$scope.funobj = {};

	//基本信息中需要提前弄好的信息。
	$scope.baseinfo = {
		'imageshow': {},
		'dateshow': {},
	};

	var uploader1 = $scope.uploader1 = new FileUploader({
		url: 'https://dlqt.juyouhx.com/Api/Api/ObjectToOss?topdir=aa&selfdir=bb'
	});
	uploader1.filters.push({
		name: 'imageFilter',
		fn: function (item /*{File|FileLikeObject}*/, options) {
			var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
			return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
		}
	});
	uploader1.onSuccessItem = function (fileItem, response, status, headers) {
		$scope.saleobj.top_pic = response.savename;
	};

	var uploader2 = $scope.uploader2 = new FileUploader({
		url: 'https://dlqt.juyouhx.com/Api/Api/ObjectToOss?topdir=aa&selfdir=bb'
	});
	uploader2.filters.push({
		name: 'imageFilter',
		fn: function (item /*{File|FileLikeObject}*/, options) {
			var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
			return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
		}
	});
	uploader2.onSuccessItem = function (fileItem, response, status, headers) {
		$scope.saleobj.logo = response.savename;
	};

	$scope.baseinfo.imageshow.uploader1 = uploader1;
	$scope.baseinfo.imageshow.uploader2 = uploader2;


	// $scope.baseinfo.dateshow.periodstart = {
	// 	'date': new Date(),
	// };
	$scope.baseinfo.dateshow.periodstart = {
		'date': {
			'label': $scope.util.date2str(new Date()),
			'value': $scope.util.date2str(new Date()),
			'opened': false
		}
	};

	$scope.baseinfo.dateshow.periodend = {
		'date': {
			'label': $scope.util.date2str(new Date(parseInt($scope.baseinfo.dateshow.periodstart.date.value), 11, 31)),
			'value': $scope.util.date2str(new Date(parseInt($scope.baseinfo.dateshow.periodstart.date.value), 11, 31)),
			'opened': false
		}
	};

	$scope.baseinfo.dateshow.open = function ($event, item) {
		$event.preventDefault();
		$event.stopPropagation();
		item.opened = true;
	}

	$scope.$watch('template', function (newValue) {
		for (var key in $scope.template) {
			(function (arg) {
				var saleStr = arg.substr(0, arg.length - 5);
				if($scope.template[arg]){
					$("[ng-model='saleobj." + saleStr + "']").prop("disabled", false);
				} else {
					$("[ng-model='saleobj." + saleStr + "']").prop("disabled", true);
					$scope.saleobj[saleStr] = undefined;
				}
			})(key);//调用时参数  
		}
	}, true);


};