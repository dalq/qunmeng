/**
 * 模态框
 */
module.exports = function ($scope, $state, $resource, $stateParams, $modalInstance, $http, items, toaster) {

    $scope.week = [  
        {id : 1, name : '周一'},  
        {id : 2, name : '周二'},  
        {id : 3, name : '周三'},  
        {id : 4, name : '周四'},  
        {id : 5, name : '周五'},  
        {id : 6, name : '周六'},  
        {id : 0, name : '周日'}  
    ] ;  

    $scope.saleobj = {};
    $scope.tour = {};
    $scope.selected = [1,1,1,1,1,1,1] ;  
    // $scope.tour.start = {};
    // $scope.tour.end = {};
    // $scope.tour.start.date = new Date();
    // $scope.tour.end.date = new Date();
    $scope.open = function ($event, item) {
        $event.preventDefault();
        $event.stopPropagation();
        item.opened = true;
    };
    $scope.tour.start = {
        'date': {
            'label': date2str(new Date()),
            'value': date2str(new Date()),
            'opened': false
        }
    };
    $scope.tour.end = {
        'date': {
            'label': date2str(new Date()),
            'value': date2str(new Date()),
            'opened': false
        }
    };
    $scope.selectedDate = [];

    $scope.save = function () {

        var start_date = '';
        var end_date = '';
        if (typeof $scope.tour.start.date.label === 'string') {
            start_date = $scope.tour.start.date.label;
        } else {
            start_date = date2str($scope.tour.start.date.label);
        }
        if (typeof $scope.tour.end.date.label === 'string') {
            end_date = $scope.tour.end.date.label;
        } else {
            end_date = date2str($scope.tour.end.date.label);
        }
        $scope.selectedDate = dataScope(start_date,end_date);
        var result = [];
        var para = {};
        for (var index = 0; index < $scope.selectedDate.length; index++) {

            var element = $scope.selectedDate[index] + '';

            result.push({
                id: items.dateArray[element],
                date: element,
                sale_code: items.sale_code,
                market_price: $scope.saleobj.market_price,
                guide_price: $scope.saleobj.guide_price,
                cost_price: $scope.saleobj.cost_price,
                purchase_price: $scope.saleobj.purchase_price,
                current_stock_num: $scope.saleobj.current_stock_num
            });
        }

        para.daylist = result;
        $resource('/api/ac/tc/ticketSaleDayPriceService/createDayPrice', {}, {}).save(para, function (res) {
            if (res.errcode === 0) {
                toaster.success({ title: '', body: '保存成功' });
                $modalInstance.close({});
            } else {
                toaster.warning({ title: '', body: res.errmsg });
                $scope.lastResult = [];
            }
        });
        // $modalInstance.close({});
    }


    
      
    $scope.isChecked = function(id){  
        //return $scope.selected.indexOf(id) >= 0 ;  
        return $scope.selected[id] == 1;
    } ;  
      
    $scope.updateSelection = function($event,id){  
        var checkbox = $event.target ;  
        var checked = checkbox.checked ;  
        if(checked){  
            //$scope.selected.push(id) ;  
            $scope.selected[id] = 1;
        }else{  
            var idx = $scope.selected.indexOf(id) ;  
            //$scope.selected.splice(idx,1) ;  
            $scope.selected[id] = 0;
        }  
    } ;  




    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

    function date2str(d) {
        if (d === undefined) {
            return "";
        }
        var month = (d.getMonth() + 1).toString();
        var day = d.getDate().toString();
        if (month.length == 1) month = '0' + month;
        if (day.length == 1) day = '0' + day;
        return d.getFullYear() + "-" + month + "-" + day;
    }

    function dataScope(value1, value2) {
        // var getDate1 = function (str) {
        //     var tempDate = new Date();
        //     var list = str.split("-");
        //     tempDate.setFullYear(list[0]);
        //     tempDate.setMonth(list[1] - 1);
        //     tempDate.setDate(list[2]);
        //     return tempDate;
        // }
        var date1 = new Date(value1);
        var date2 = new Date(value2);
        if (date1 > date2) {
            var tempDate = date1;
            date1 = date2;
            date2 = tempDate;
        }
        date2.setDate(date2.getDate() + 1);
        var dateArr = [];

        while (!(date1.getFullYear() == date2.getFullYear()
            && date1.getMonth() == date2.getMonth()
            && date1.getDate() == date2.getDate())) {

            //当前日期的星期。
            var ddd = date1.getDay();

            if($scope.selected[ddd] == 1){
                var dayStr = date1.getDate().toString();
                if (dayStr.length == 1) {
                    dayStr = "0" + dayStr;
                }

                var month = (date1.getMonth() + 1).toString();
                if (month.length == 1) month = '0' + month;

                dateArr.push(date1.getFullYear() + "-" +
                    month + "-"
                    + dayStr);
            }

            date1.setDate(date1.getDate() + 1);

        }
        return dateArr;
    }

};