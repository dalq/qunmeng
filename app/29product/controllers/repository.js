module.exports = function ($scope, $state, $resource, $modal, toaster) {

	//加载销售品分类下拉搜索
	$scope.loadSaleCategory = function (){
		$resource('/api/as/sc/dict/dictbytypelist', {}, {}).save({'type': 'sale_category'}, function(res){
			if(res.errcode === 0){
				$scope.sale_category = res.data;
				$scope.sale_category.splice(0,0,{'label': '全部', 'value': '', 'type': 'sale_category'});
			}else{
				toaster.error({title: '', body: '加载销售品列表失败'});
			}
		});
	}

	//加载平台产品库景区列表-----作废
	// $scope.loadPtpPlaceList = function (){
	// 	$resource('/api/as/puc/authorizedt/findPtPlaceList', {}, {}).save({}, function(res){
	// 		if(res.errcode === 0){
	// 			$scope.ptp_place_list = res.data;
	// 			if(res.data.length > 0){
	// 				$scope.ptp_place_list.splice(0,0,{'name': '全部', 'place_code': ''})
	// 			}
	// 		}else{
	// 			alert(res.errmsg);
	// 		}
	// 	});
	// }

	//加载登录账号的协议价信息
	$scope.loadOfficeInfo = function (){
		$resource('/api/ac/tc/agreementPriceService/setOfficeInfo', {}, {}).save({}, function(res){
			if(res.errcode === 0){
				$scope.officeInfo = res.data;
			} else {
				toaster.error({title: '', body: '加载个人信息失败'});
			}
		});
	}

	//搜索我的采纳产品库
	$scope.search = function (pageNo){
		$scope.currentPage = pageNo;
		$scope.searchform.pageNo = pageNo;
		$scope.searchform.pageSize = $scope.itemsPerPage;
		$resource('/api/ac/tc/ticketSaleAuthService/findAllSaleList', {}, {}).save($scope.searchform, function(res){
			if(res.errcode === 0){
                $scope.sale_list = res.data.results;
                for(var i = 0; i < $scope.sale_list.length; i++){
                    $scope.sale_list[i].is_selected = ($scope.sale_list[i].state == '1');
					$scope.sale_list[i].is_selected3 = ($scope.sale_list[i].downstream_open_company == '1');
					
					if ($scope.sale_list[i].channel_list_name && typeof $scope.sale_list[i].channel_list_name == 'string') {
						$scope.sale_list[i].channel_list_name = $scope.sale_list[i].channel_list_name.replace(/,/g, '，');
					}
                }
				$scope.totalItems = res.data.totalRecord;
				$scope.loading = false;
			}else{
				toaster.error({title: '', body: res.errmsg});
			}
		});
	}
	
	init();
	//初始化查询产品列表
	function init(){
		$scope.currentPage = 1;			//当前页码
		$scope.itemsPerPage = 10;		//每页显示几条;
		$scope.searchform = {};
		$scope.nothave = [
			{'name': '全部', 'value': null},
			{'name': '未采购', 'value': 'balala'}
		];
		$scope.loading = true;
		$scope.search(1);
		$scope.loadSaleCategory();
		// $scope.loadPtpPlaceList();
		$scope.loadOfficeInfo();
	}

	//添加至我的产品库
	$scope.change1 = function (sale_code, type, flag, index){
        if (flag) {
            $resource('/api/as/tc/saleauth/saveOpen', {}, {}).save({'sale_code': sale_code}, function(res){
            	if(res.errcode === 0){
            		toaster.success({title: '', body: '成功添加到我的产品库'});
            		$scope.search(1);
            	}else{
            		toaster.error({title: '', body: res.errmsg});
                    $scope.sale_list[index].is_selected = !flag;
            	}
            });
        } else {
            if(type != 1){
				$resource('/api/as/tc/saleauth/del', {}, {}).save({'sale_code': sale_code}, function(res){
					if(res.errcode === 0){
						$scope.search(1);
                        toaster.success({title: '', body: '修改成功'});
					}else{
						toaster.error({title: '', body: res.errmsg});
                        $scope.sale_list[index].is_selected = !flag;
					}
				});
			} else {
                toaster.warning({title: '', body: '不是平台产品'});
                $scope.sale_list[index].is_selected = !flag;
            }
        }
    }

	//点对点授权
	$scope.PtPgrant = function (info){
		var modalInstance = $modal.open({
			template: require('../views/setPtPgrant.html'),
			controller: 'setPtPgrant',
			size: 'lg',
			resolve: {
				item : function () {
					return info;
                }
			}
		});
		//关闭模态框刷新页面
		modalInstance.result.then(function(num) {
			info.num += num;
			$scope.bak.spread = false;
		});
	}

	//设置渠道商
    $scope.setChannel = function (item){
        var modalInstance = $modal.open({
			template: require('../views/setChannel.html'),
			controller: 'setChannel',
			size: 'lg',
			resolve: {
				sale_code : function () {
					return item.code;
				}
			}
		});
		//关闭模态框刷新页面
		modalInstance.result.then(function(result) {
			$scope.search(1);
		});
    }
    
	//设置公开范围
	$scope.change2 = function (info, flag, index){
		var para = {'sale_code': info.code};
		var msg = info.downstream_open_company == '0'? '确认修改为公开销售吗?': '修改后将删除已授权的分销商，确认修改为单独授权销售吗?';
        para.open_company = flag? '1': '0';
		if(confirm(msg) == true){
			$resource('/api/ac/tc/ticketSaleAuthService/updateOpen', {}, {}).save(para, function (res) {
				if (res.errcode === 0 && res.data.msg === 'success') {
					toaster.success({title: '', body: '修改成功'});
					$scope.search($scope.currentPage);
            		// $scope.sale_list[index].downstream_open_company = flag? '1': '0';
            		// $scope.sale_list[index].is_selected3 = flag;
				} else {
					toaster.error({title: '', body: '修改失败'});
                    $scope.sale_list[index].is_selected3 = !flag;
				}
			});
		} else {
            $scope.sale_list[index].is_selected3 = !flag;
		}
	}

	$scope.setFxs = function (item) {
		var modalInstance = $modal.open({
			template: require('../views/setFxs.html'),
			controller: 'setFxs',
			size: 'lg',
			resolve: {
				item : function () {
					return item;
				}
			}
		});
		//关闭模态框刷新页面
		modalInstance.result.then(function(result) {
			$scope.search(1);
		});
	}

	//跳转我的渠道
	$scope.myChannel = function (item) {
		$state.go('app.myChannel', {});
	}

	//是否自动获取公开产品
    $scope.autoSave = function (){
		var msg, para = {'target_company_code': $scope.officeInfo.code};
		if($scope.officeInfo.automatic_save == '1'){
			msg = '已采购的产品不会取消，确认修改为手动采购可售的产品吗？';
			para.automatic_save = '0';
		} else {
			msg = '确认修改为自动采购可售的产品吗？';
			para.automatic_save = '1';

		}
		
		if(confirm(msg)){
			$resource('/api/as/tc/agreementprice/setAutoSave', {}, {}).save(para, function(res){
				if(res.errcode !== 0){
					$scope.officeInfo.automatic_save = para.automatic_save;
				}
			});
		} else {
			$scope.officeInfo.automatic_save = para.automatic_save;
		}
	}


};