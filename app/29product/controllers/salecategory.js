module.exports = function($scope, salecategorylist, $modal, dictbytypelist, salecategoryinsert, salecategorydelete, $resource, toaster){
    $scope.obj = {
        'showSpline' : '0'
    }
    function load(){
        salecategorylist.get({}, function(res){

            if(res.errcode === 0)
            {
                $scope.objs = res.data;
                $scope.objs.forEach(function(element) {
                    element.showSpline = element.sub_table_used_type=='0'?true:false;
                }, this);
            }
            else
            {
                toaster.error({ title: "提示", body: res.errmsg });
            }

           
        });
    }
    load();

    $scope.changeChildren = function(item){
        var modalInstance = $modal.open({
          template: require('../views/salecategoryChildren.html'),
          controller: 'salecategoryChildren',
          resolve: {
            // dictbytypelist : function(){
            //     return dictbytypelist;
            // },
            item : function(){
                return item;
            }
          }
        });

        modalInstance.result.then(function () {
          load();
        }, function () {
          load();
        });
    }

        //打开模态框
    $scope.create = function(){


        var modalInstance = $modal.open({
          template: require('../views/salecategorymodel.html'),
          controller: 'salecategorycreate',
          resolve: {
            dictbytypelist : function(){
                return dictbytypelist;
            },
            salecategoryinsert : function(){
                return salecategoryinsert;
            }
          }
        });

        modalInstance.result.then(function () {
          
          load();

        }, function () {
          //$log.info('Modal dismissed at: ' + new Date());
        });
    }

   
    $scope.del = function(obj){

        salecategorydelete.save(obj, function(res){

            if(res.errcode === 0)
            {
                load();
            }
            else
            {
                toaster.error({ title: "提示", body: res.errmsg });
            }


        });

    };

    $scope.change = function(showSpline,sale_category_code,sub_table_code,sub_table_used_type){
        $scope.para = {
            'sale_category_code' : sale_category_code,
            'sub_table_code' : sub_table_code,
            'sub_table_used_type' : ''
        }
        if(showSpline == true){
            $scope.para.sub_table_used_type = '0';
            $resource('/api/as/tc/salecategory/updateType', {}, {}).save($scope.para,function(res){
                if(res.errcode!=0){
                    toaster.success({title:"",body:res.errmsg});
                    return;
                }
                load();
                
            })
        } else {
            $scope.para.sub_table_used_type = '1';
            $resource('/api/as/tc/salecategory/updateType', {}, {}).save($scope.para,function(res){
                if(res.errcode!=0){
                    toaster.success({title:"",body:res.errmsg});
                    return;
                }
                load();
                
            })
        }
    }

};