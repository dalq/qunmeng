module.exports = function ($scope, item, $modalInstance, toaster, $resource) {

    $scope.obj = item;

    $scope.gogo = function () {

        $resource('/api/as/tc/salecategory/updateType', {}, {}).save($scope.obj, function (res) {
            if (res.errcode != 0) {
                toaster.error({ title: "", body: res.errmsg });
                return;
            }
            toaster.success({ title: "", body: '修改成功' });
            $modalInstance.close();
        })

    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

};