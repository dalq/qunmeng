module.exports = function ($scope, $state, $resource, $stateParams, toaster, $modalInstance, item) {

    $scope.saleobj = {};
    $scope.saleobj.id = item.id;

    $scope.obj = item;

    $scope.pass = function () {
        if (!$scope.saleobj.apply_note) {
            toaster.error({ title: "", body: '审核意见必填' })
            return false;
        }
        $resource('/api/ac/tc/ticketOrderBackService/updateApplyPass', {}, {})
            .save({
                'id': $scope.saleobj.id,
                'apply_note': $scope.saleobj.apply_note
            }, function (res) {
                if (res.errcode !== 0) {
                    toaster.error({ title: "提示", body: res.errmsg });
                    // toaster.error({ title: "提示", body: res.errmsg });
                    return;
                }
                // toaster.success({ title: "提示", body: "操作成功!" });
                toaster.success({ title: "", body: '操作成功' })
	            $modalInstance.close();
            });
    };

    $scope.nopass = function () {
        if (!$scope.saleobj.apply_note) {
            toaster.error({ title: "", body: '审核意见必填' })
            return false;
        }
        $resource('/api/ac/tc/ticketOrderBackService/updateApplyNoPass', {}, {})
            .save({
                'id': $scope.saleobj.id,
                'apply_note': $scope.saleobj.apply_note
            }, function (res) {
                if (res.errcode !== 0) {
                    toaster.error({ title: "提示", body: res.errmsg });
                    return;
                }
                toaster.success({ title: "提示", body: "操作成功!" });
	            $modalInstance.close();
            });
    };

    $scope.cancel = function(){
        $modalInstance.dismiss('cancel');
    }

};