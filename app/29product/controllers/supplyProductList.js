module.exports = function ($scope, $state, $resource, $modal, str2date, date2str, $stateParams,
	saleup, saledown, saleupdate, talist, sellerListno, sellerList, tstcreateno, tstcreate, tststartno, tststart, tststopno, tststop, toaster

) {
	$scope.searchform = {};
	// $scope.searchform.sale_category = '';
	$scope.dictbytypelist = [];

	$scope.create = function () {
		$state.go('app.editProduct');
	};

    /* 分页
     * ========================================= */
	$scope.maxSize = 5;            //最多显示多少个按钮
	$scope.bigCurrentPage = 1;      //当前页码
	$scope.itemsPerPage = 10;         //每页显示几条

	$scope.statusArr = [
		{
			name: '----全部----',
			value: ''
		},
		{
			name: '草稿',
			value: '0'
		},
		{
			name: '上架',
			value: '1'
		},
		{
			name: '下架',
			value: '2'
		}
	];


	if ($stateParams.sale_type == 'H') {
		$resource('/api/as/sc/dict/dictbytypelist', {}, {})
			.save({ type: 'sale_category', class_type: 'H' }, function (res) {

				if (res.errcode !== 0) {
					toaster.error({ title: "提示", body: res.errmsg });
					return;
				}

				$scope.dictbytypelist = res.data;
				$scope.dictbytypelist.splice(0, 0, {
					label: '----全部----',
					value: ''
				})

			});
	} else {
		$resource('/api/as/sc/dict/dictbytypelist', {}, {})
			.save({ type: 'sale_category' }, function (res) {

				if (res.errcode !== 0) {
					toaster.error({ title: "提示", body: res.errmsg });
					return;
				}

				$scope.dictbytypelist = res.data;
				$scope.dictbytypelist.splice(0, 0, {
					label: '----全部----',
					value: ''
				})

			});
	}


	if ($stateParams.sale_type == 'H') {
		$resource('/api/as/tc/placeview/hlist', {}, {})
			.save({ type: 'H' }, function (res) {
				if (res.errcode === 0) {
					$scope.viewarr = res.data.results;
					$scope.viewarr.unshift({ name: '--全部场所--', code: '' });
				}
				else {
					toaster.error({ title: "", body: res.data.errmsg })
				}

			});
	} else {
		$resource('/api/as/tc/placeview/jlist', {}, {})
			.save({}, function (res) {

				if (res.errcode === 0) {
					$scope.viewarr = res.data.results;
					$scope.viewarr.unshift({ name: '--全部场所--', code: '' });
				}
				else {
					toaster.error({ title: "", body: res.data.errmsg })
				}

			});
	}

	$scope.myKeyup = function (e) {

		//IE 编码包含在window.event.keyCode中，Firefox或Safari 包含在event.which中
		var keycode = window.event ? e.keyCode : e.which;
		if (keycode == 13) {
			$scope.load();
		}
	};

	$scope.load = function () {

		var para = {
			pageNo: $scope.bigCurrentPage,
			pageSize: $scope.itemsPerPage,

		};
		if ($stateParams.sale_type == 'H') {
			para.sale_type = 'H'
		}
		para = angular.extend($scope.searchform, para);


		$resource('/api/ac/tc/ticketSaleService/getSaleList', {}, {})
			.save(para, function (res) {

				if (res.errcode !== 0) {
					toaster.error({ title: "提示", body: res.errmsg });
					return;
				}
				$scope.objs = res.data.results;
				for (var index = 0; index < $scope.objs.length; index++) {
					switch ($scope.objs[index].sale_apply_state) {
						case 'new':
							$scope.objs[index].sale_apply_state_name = '';
							break;
						case 'apply_sale':
							$scope.objs[index].sale_apply_state_name = '申请上架';
							break;
						case 'wait_sale':
							$scope.objs[index].sale_apply_state_name = '审核通过等待上架';
							break;
						case 'for_sale':
							$scope.objs[index].sale_apply_state_name = '销售中';
							break;
						case 'pause':
							$scope.objs[index].sale_apply_state_name = '暂停销售';
							break;
						case 'sale_end':
							$scope.objs[index].sale_apply_state_name = '下架';
							break;
						case 'ban_sale':
							$scope.objs[index].sale_apply_state_name = '禁止销售';
							break;

						default:
							$scope.objs[index].sale_apply_state_name = '';
							break;
					}
					if ($scope.objs[index].channel_list_name && typeof $scope.objs[index].channel_list_name == 'string') {
						$scope.objs[index].channel_list_name = $scope.objs[index].channel_list_name.replace(/,/g, '，');
					}
					$scope.objs[index].is_selected3 = ($scope.objs[index].open_company == '1');
				}
				$scope.bigTotalItems = res.data.totalRecord;

			});

	};
	$scope.load();

	//申请上架
	$scope.start = function (id) {
		saleup.get({ 'id': id }, function (res) {
			if (res.errcode === 0) {
				$scope.load();
			} else {
				toaster.error({ title: "提示", body: res.errmsg });
			}
		});
	}

	//下架
	$scope.stop = function (id) {
		saledown.get({ 'id': id }, function (res) {
			if (res.errcode === 0) {
				$scope.load();
			} else {
				toaster.error({ title: "提示", body: res.errmsg });
			}
		});
	}

	$scope.edit = function (id, sale_category) {

		$resource('/api/as/sc/dict/getSaleCategoryList', {}, {})
			.save({ 'type': 'sale_category' }, function (res) {
				if (res.errcode !== 0) {
					toaster.error({ title: "/api/as/sc/dict/getSaleCategoryList", body: res.errmsg });
					return false;
				}

				var flag = false;
				for (var index = 0; index < res.data.length; index++) {
					var element = res.data[index];
					if (element.value == sale_category) {
						flag = true;
					}
				}
				if (!flag) {
					toaster.error({ title: "您的账号无权查看该类型销售品", body: "请配置销售品分类权限" });
					return false;
				}

				var modalInstance = $modal.open({
					template: require('../views/product.html'),
					controller: 'editProduct',
					url: '/product/edit/:id',
					size: 'lg',
					resolve: {
						'productid': function () {
							return id;
						},
						what: function () {
							return 'edit';
						},
						str2date: function () {
							return str2date;
						},
						date2str: function () {
							return date2str;
						},
						auditing: function () {
							return false;
						}
					}
				});

				modalInstance.opened.then(function () {// 模态窗口打开之后执行的函数  
				});
				modalInstance.result.then(function (showResult) {
					$scope.load();
				}, function (reason) {
					$scope.load();
				});

			});

	};


	$scope.asort = function (id, asort) {
		saleupdate.save({ 'id': id, 'asort': asort }, function (res) {
			if (res.errcode === 0) {
				$scope.load();
			}
			else {
				toaster.error({ title: "提示", body: res.errmsg });
			}
		});
	};


	$scope.info = function (id, sale_category) {

		$resource('/api/as/sc/dict/getSaleCategoryList', {}, {})
			.save({ 'type': 'sale_category' }, function (res) {
				if (res.errcode !== 0) {
					toaster.error({ title: "/api/as/sc/dict/getSaleCategoryList", body: res.errmsg });
					return false;
				}

				var flag = false;
				for (var index = 0; index < res.data.length; index++) {
					var element = res.data[index];
					if (element.value == sale_category) {
						flag = true;
					}
				}
				if (!flag) {
					toaster.error({ title: "您的账号无权查看该类型销售品", body: "请配置销售品权限" });
					return false;
				}

				var modalInstance = $modal.open({
					template: require('../views/productInfo.html'),
					controller: 'infoProduct',
					url: '/infoProduct/:id',
					size: 'lg',
					resolve: {
						'productid': function () {
							return id;
						},
						str2date: function () {
							return str2date;
						},
						date2str: function () {
							return date2str;
						},
						auditing: function () {
							return false;
						}
					}
				});

				modalInstance.result.then(function () {
					//load();
				}, function () {
					//$log.info('Modal dismissed at: ' + new Date());
				});

			});


	};



	//flag:1,分配经销商
	//flga:0,不允许销售
	$scope.distribution = function (code, flag) {

		var modalInstance;

		if (flag == 1) {
			modalInstance = $modal.open({
				template: require('../views/distribution.html'),
				controller: 'distribution',
				//size: 'lg',
				resolve: {
					code: function () {
						return code;
					},
					talist: function () {
						return talist;
					},
					sellerList: function () {
						return sellerList;
					},
					tstcreate: function () {
						return tstcreate;
					},
					tststart: function () {
						return tststart;
					},
					tststop: function () {
						return tststop;
					},
					title: function () {
						return '分配经销商';
					}
				}
			});
		}
		else if (flag == 0) {
			modalInstance = $modal.open({
				template: require('../views/nodistribution.html'),
				controller: 'nodistribution',
				//size: 'lg',
				resolve: {
					code: function () {
						return code;
					},
					talist: function () {
						return talist;
					},
					sellerList: function () {
						return sellerListno;
					},
					tstcreate: function () {
						return tstcreateno;
					},
					tststart: function () {
						return tststartno;
					},
					tststop: function () {
						return tststopno;
					},
					title: function () {
						return '分配不允许销售经销商';
					}
				}
			});
		}


		modalInstance.opened.then(function () {// 模态窗口打开之后执行的函数  
			// $scope.load();
		});
		modalInstance.result.then(function (showResult) {
			$scope.load();
		}, function (reason) {
			$scope.load();
			// // click，点击取消，则会暑促cancel  
			// $log.info('Modal dismissed at: ' + new Date());
		});

	};

	$scope.del = function (id) {

		if (confirm("您确认要删除吗？")) {

			saleupdate.save({ 'id': id, 'del_flg': '1' }, function (res) {

				if (res.errcode === 0) {
					toaster.success({ title: "", body: '删除成功' });
					$scope.load();
				}
				else {
					toaster.error({ title: "提示", body: res.errmsg });
				}

			});

		}
	};
	$scope.skset = function (id, code, name, guide_price, cost_price, productflag) {
		$scope.item = '';
		productflag = 'productflag';
		var modalInstance = $modal.open({
			template: require('../views/skprofit.html'),
			controller: 'skprofit',
			url: '/skprofit',
			size: 'lg',
			resolve: {
				item: function () {
					return $scope.item;
				},
				id: function () {
					return id;
				},
				code: function () {
					return code;
				},
				name: function () {
					return name;
				},
				guide_price: function () {
					return guide_price;
				},
				cost_price: function () {
					return cost_price;
				},
				productflag: function () {
					return productflag;
				},
				what: function () {
					return 'edit';
				},

			}
		});

		modalInstance.result.then(function () {
		}, function () {
		});
		// $state.go('app.skprofit');
		// $state.go('app.skprofit',{'code' : code, 'name' : name, 'guide_price' : guide_price, 'cost_price' : cost_price});
	}

	$scope.offSale = function (id) {
		$resource('/api/as/tc/sale/offSale', {}, {})
			.save({ id: id }, function (res) {
				if (res.errcode !== 0) {
					toaster.error({ title: "提示", body: res.errmsg });
					return;
				}
				toaster.success({ title: "提示", body: '修改成功!' });
				$scope.load();
			});
	}

	$scope.onSale = function (id) {
		$resource('/api/as/tc/sale/onSale', {}, {})
			.save({ id: id }, function (res) {
				if (res.errcode !== 0) {
					toaster.error({ title: "提示", body: res.errmsg });
					return;
				}
				toaster.success({ title: "提示", body: '修改成功!' });
				$scope.load();
			});
	}


	//设置多图-微分销用
	$scope.setImages = function (obj) {

		$resource('/api/as/wc/productshare/getshareattr', {}, {}).save({ code: obj.code }, function (res) {

			var pobj = {
				'code': obj.code,
				'title': obj.name,
			};

			var lala = {};

			if (res.errcode === 0) {
				//有数据
				// pobj['top_pic'] = res.data.top_pic;
				// pobj['words'] = res.data.words;
				// pobj['bookingnotes'] = res.data.bookingnotes;
				// pobj['detail'] = res.data.detail;

				angular.extend(res.data, pobj);

				lala = res.data;
			}
			else if (res.errcode === 10003) {
				//没数据
				angular.extend(lala, pobj);
			}
			else {
				alert(res.errmsg);
				return;
			}

			var modalInstance = $modal.open({
				template: require('../../51weshop/views/settoppic.html'),
				controller: 'weshopsettoppic',
				url: '/weshopsettoppic',
				size: 'lg',
				resolve: {
					'product': function () {
						return lala;
					},
					str2date: function () {
						return str2date;
					},
					date2str: function () {
						return date2str;
					},

				}
			});

			modalInstance.opened.then(function () {// 模态窗口打开之后执行的函数  
				// $scope.load();
			});
			modalInstance.result.then(function (showResult) {
				$scope.load();
			}, function (reason) {
				$scope.load();
				// // click，点击取消，则会暑促cancel  
				// $log.info('Modal dismissed at: ' + new Date());
			});

		});

	};



	//配置海报
	$scope.setPoster = function (obj) {

		$resource('/api/ac/wc/productShareService/getshareattr', {}, {}).save({ code: obj.code }, function (res) {

			var pobj = {
				'code': obj.code,
				'title': obj.name,
			};

			if (res.errcode === 0) {
				//没有数据
				if (res.data.xxx || res.data.xxx == 'yyy') {

				}
				//有数据
				else {
					pobj['id'] = obj.id;
					if (res.data.img) {
						pobj['share_pic'] = res.data.img;
					}
					if (res.data.qrcode_size) {
						pobj['qrcode_size'] = res.data.qrcode_size;
					}
					if (res.data.width) {
						pobj['width'] = res.data.width;
					}
					if (res.data.height) {
						pobj['height'] = res.data.height;
					}
					if (res.data.nickname_width) {
						pobj['nickname_width'] = res.data.nickname_width;
					}
					if (res.data.nickname_height) {
						pobj['nickname_height'] = res.data.nickname_height;
					}
					if (res.data.nickname_size) {
						pobj['nickname_size'] = res.data.nickname_size;
					}
					if (res.data.nickname_color) {
						pobj['nickname_color'] = res.data.nickname_color;
					}
					if (res.data.nickname_length) {
						pobj['nickname_length'] = res.data.nickname_length;
					}
				}
			}
			else if (res.errcode === 9999) {
				//没数据

			}
			else {
				alert(res.errmsg);
				return;
			}

			var modalInstance = $modal.open({
				template: require('../../51weshop/views/setposter.html'),
				controller: 'weshopposter',
				url: '/weshopposter',
				size: 'lg',
				resolve: {
					'obj': function () {
						return pobj;
					},
					str2date: function () {
						return str2date;
					},
					date2str: function () {
						return date2str;
					}
				}
			});

			modalInstance.opened.then(function () {// 模态窗口打开之后执行的函数  
				// $scope.load();
			});
			modalInstance.result.then(function (showResult) {
				$scope.load();
			}, function (reason) {
				$scope.load();
				// // click，点击取消，则会暑促cancel  
				// $log.info('Modal dismissed at: ' + new Date());
			});


			// var para = $state.get('app.weshopposter');

			// var resolve = {
			//          obj : function(){
			//              return pobj;
			//          },
			//      };
			//      angular.extend(para.resolve, resolve);

			//      var modalInstance = $modal.open(para);
			//      modalInstance.opened.then(function() {// 模态窗口打开之后执行的函数  
			//      });  
			//      modalInstance.result.then(function(result) {  
			//          //load(); 

			//      }, function(reason) {  
			//          // click，点击取消，则会暑促cancel  
			//          $log.info('Modal dismissed at: ' + new Date());  
			//      }); 

		});

	};
    
	//设置公开范围
	$scope.change2 = function (info, flag, index){
		var para = {'sale_code': info.code};
		var msg = info.open_company == '0'? '确认修改为公开销售吗?': '修改后将删除已授权的分销商，确认修改为单独授权销售吗?';
        para.open_company = flag? '1': '0';
		if(confirm(msg) == true){
			$resource('/api/ac/tc/ticketSaleAuthService/updateOpen', {}, {}).save(para, function (res) {
				if (res.errcode === 0 && res.data.msg === 'success') {
					toaster.success({title: '', body: '修改成功'});
					$scope.load();
				} else {
					toaster.error({title: '', body: '修改失败'});
                    $scope.objs[index].is_selected3 = !flag;
				}
			});
		} else {
            $scope.objs[index].is_selected3 = !flag;
		}
	}

	$scope.setFxs = function (item) {
		var modalInstance = $modal.open({
			template: require('../views/setFxs.html'),
			controller: 'setFxs',
			size: 'lg',
			resolve: {
				item : function () {
					return item;
				}
			}
		});
		//关闭模态框刷新页面
		modalInstance.result.then(function(result) {
			$scope.load();
		});
	}
	//设置渠道商
    $scope.setChannel = function (item){
        var modalInstance = $modal.open({
			template: require('../views/setChannel.html'),
			controller: 'setChannel',
			size: 'lg',
			resolve: {
				sale_code : function () {
					return item.code;
				}
			}
		});
		//关闭模态框刷新页面
		modalInstance.result.then(function(result) {
			$scope.load();
		});
	}
	
	//跳转我的渠道
	$scope.myChannel = function (item) {
		$state.go('app.myChannel', {});
	}

};