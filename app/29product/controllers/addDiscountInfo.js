module.exports = function ($scope, $modalInstance, $resource, $modal, discount, toaster){
    
    //加载产品分类下拉列表
    $scope.loadDict = function (){
        $resource('/api/as/sc/dict/dictbytypelist', {}, {}).save({'type': 'sale_category'}, function (res) {
            if (res.errcode === 0) {
                $scope.sale_type = res.data;
                $scope.sale_type.unshift({'label':'全部','value':null,'type':'sale_category'});
            } else {
                toaster.error({title: '', body: res.errmsg});
            }
        });
    }

    //加载产品列表
    $scope.loadProductList = function (pageNo){
        var para = {
			'pageNo': pageNo,
			'pageSize': $scope.itemsPerPage,
			'sale_category': $scope.searchform.type,
			'name': $scope.searchform.name
        }
        $resource('/api/as/tc/sale/forDiscountList', {}, {}).save(para, function (res) {
            if (res.errcode === 0) {
                $scope.discount_list = res.data.results;
                $scope.totalItems = res.data.totalRecord;
                if($scope.save_list.length > 0){
                    angular.forEach($scope.discount_list, function(item){
                        for(var i = 0; i < $scope.save_list.length; i++){
                            if(item.code == $scope.save_list[i].code){
                                item.state = true;
                                continue;
                            }
                        }
                    });
                }
                //以下两条保证数据一致性  -_- 
                $scope.status.all = false;
                $scope.currentPage = pageNo;
            } else {
                toaster.error({title: '', body: res.errmsg});
            }
        });

    }

    init();
    function init() {
        $scope.currentPage = 1;		    //当前页码
        $scope.itemsPerPage = 10;		//每页显示几条
        $scope.status = {
            'save': true,               //true: 产品列表状态, false: 确认添加产品状态
            'all' : false
        };
        $scope.searchform = {};
        $scope.save_list = [];
        $scope.loadDict();
        $scope.loadProductList(1);
    }

    //确认添加指定产品
    $scope.ok = function (){
        if($scope.save_list.length > 0){
            angular.forEach($scope.save_list, function(item){
                item.discount_id = discount.id;
                item.discount_type = discount.type || '2';
                item.price = discount.price || 0;
                item.nopass = false;
            });
            $scope.status.save = false;
        }
    }

    //返回到产品列表
    $scope.back = function () {
        $scope.status.save = true;
    }

    //保存已设置好的优惠产品
    $scope.save = function () {
        for(var i = 0; i < $scope.save_list.length; i++){
            if($scope.save_list[i].nopass){
                toaster.error({title: '', body: '优惠信息填写错误'});
                return;
            }
        }
        $resource('/api/ac/puc/pubCityDiscountService/addDiscountInfo', {}, {})
            .save({'product_list': $scope.save_list}, function (res) {
            if (res.errcode === 0) {
                $modalInstance.close();
            } else {
                toaster.error({title: '', body: res.errmsg});
            }
        });
    }

    //检查优惠金额是否超过市场价,优惠折扣超过100%
    $scope.check = function(item){
        var max = item.discount_type==1 ? 100 : item.market_price;
        if(item.price > max || item.price < 0){
            item.nopass = true;
        } else {
            item.nopass = false;
        }
    }

    //取消
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    }

    //全选or全不选
    $scope.allSelect = function() {
        if($scope.status.all){
            angular.forEach($scope.discount_list, function(item){
                if(!item.state){
                    $scope.save_list.push(item);
                }
                item.state = true;
            });
        } else {
            angular.forEach($scope.discount_list, function(item){
                item.state = false;
                for(var i = 0; i < $scope.save_list.length; i++){
                    if(item.code == $scope.save_list[i].code){
                        $scope.save_list.splice(i, 1);
                    }
                }
            });
        }
    }

    //列表勾选,添加到已选列表
    $scope.selectOne = function (item) {
        if(item.state){
            $scope.save_list.push(item);
        } else {
            for(var i = 0; i < $scope.save_list.length; i++){
                if(item.code == $scope.save_list[i].code){
                    $scope.save_list.splice(i, 1);
                }
            }
        }
    }

    //已选列表删除
    $scope.remove = function (index) {
        var code = $scope.save_list[index].code;
        angular.forEach($scope.discount_list, function(item){
            if(code == item.code){
                item.state = false;
            }
        });
        $scope.save_list.splice(index, 1);
    }

};