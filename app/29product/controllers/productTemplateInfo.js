
module.exports = function ($scope, $stateParams, $http, $resource, toaster, $modalInstance, $timeout, FileUploader) {

    $scope.template = {}
    $scope.baseinfo = {
		'imageshow': {}
	};

    var uploader1 = $scope.uploader1 = new FileUploader({
		url: 'https://dlqt.juyouhx.com/Api/Api/ObjectToOss?topdir=aa&selfdir=bb'
	});
	uploader1.filters.push({
		name: 'imageFilter',
		fn: function (item /*{File|FileLikeObject}*/, options) {
			var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
			return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
		}
	});
	uploader1.onSuccessItem = function (fileItem, response, status, headers) {
		$scope.template.templete_img = response.savename;
	};
    $scope.baseinfo.imageshow.uploader1 = uploader1;

    $scope.save = function(){
        $modalInstance.close($scope.template);
    }
    
};