module.exports = function ($scope, $state, $resource, $modal, toaster) {
    $scope.searchform = {};

    $scope.create = function () {
        $state.go('app.editProduct');
    };

    // $resource('/api/ac/tc/ticketSaleTempleteService/getTempleteList', {}, {})
    //     .save({}, function (res) {
    //         if (res.errcode !== 0) {
    //             toaster.error({ title: "提示", body: res.errmsg });
    //             return;
    //         }
    //         var data = angular.copy(res.data);
    //         data.templete_lock_data_json = res.templete_lock_data_json.parseJSON();
    //         $scope.functionArray = data;
    //     });

    $scope.load = function () {

        // var para = {
        //     pageNo: $scope.bigCurrentPage,
        //     pageSize: $scope.itemsPerPage
        // };

        // para = angular.extend($scope.searchform, para);
        $resource('/api/ac/tc/ticketSaleTempleteService/getTempleteList', {}, {})
            .save({}, function (res) {

                if (res.errcode !== 0) {
                    toaster.error({ title: "提示", body: res.errmsg });
                    return;
                }
                $scope.data = [];
                $scope.data = angular.copy(res.data);
                for (var index = 0; index < $scope.data.length; index++) {
                    var element = $scope.data[index];
                    element.templete_lock_data_json = eval('(' + element.templete_lock_data_json + ')');
                    element.url = 'app.newproduct';
                }
                $scope.functionArray = $scope.data;

            });

    };
    $scope.load();

    $scope.menuTail = function(url, params){
        $state.go(url, {'params':JSON.stringify(params)})
    }


    $scope.goNewProduct = function(obj){
        console.log(obj);
        //$state.go('app.newproduct', {'params':angular.toJson(obj)});
        //$state.go('app.newproduct', {'params':'angular.toJson(obj)'});
        $state.go('app.newproduct', {'params':obj.templete_code});
    };

};