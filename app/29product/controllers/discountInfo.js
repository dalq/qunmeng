module.exports = function ($scope, $state, $resource, $modal, $stateParams, toaster){

	//查询优惠详情列表
	$scope.search = function (pageNo){
		var para = {
			'pageNo': pageNo || $scope.currentPage,
			'pageSize': $scope.itemsPerPage,
			'discount_type': $scope.searchform.type,
			'discount_id': $scope.discountInfo.id,
			'sale_name': $scope.searchform.sale_name
		}
		$resource('/api/as/puc/citydiscountinfo/getDiscountInfoList', {}, {}).save(para, function(res){
			if(res.errcode === 0){
				$scope.list = res.data.results;
				$scope.totalItems = res.data.totalRecord;
			}else{
				toaster.error({title: '', body: res.errmsg});
			}
		});
	}

	init();
    //初始化列表
    function init(){
		$scope.currentPage = '1';		//当前页码
		$scope.itemsPerPage = '10';		//每页显示几条
		$scope.searchform = {};
		$scope.discount_type = [
			{'name': '全部', 'value': ''},
			{'name': '打折优惠', 'value': '1'},
			{'name': '减价优惠', 'value': '2'}
		];
		$scope.dis_state = false;
		$scope.discountInfo = {};
		if($stateParams.discount){
			$scope.dis_state = true;
			var a = $stateParams.discount.split('_');
			$scope.discountInfo.id = $stateParams.id;
			$scope.discountInfo.type = a[0];
			$scope.discountInfo.price = parseFloat(a[1]);
			$scope.discountInfo.name = a[2];
		}
		$scope.discount_name = [];
		$scope.search();
	};

	//删除优惠信息
	$scope.delete = function (index){
		var para = {
			'discount_id': $scope.list[index].discount_id,
			'sale_code': $scope.list[index].sale_code
		}
		if(confirm('确认删除优惠信息吗？')==true){
			$resource('/api/as/puc/citydiscountinfo/deleteDiscountInfo', {}, {}).save(para, function(res){
				if(res.errcode === 0){
					$scope.list.splice(index,1);
				}else{
					toaster.error({title: '', body: res.errmsg});
				}
			});
		}
	};

	//设置优惠详情
	$scope.setDiscountInfo = function(item) {
		var modalInstance = $modal.open({
			template: require('../views/setDiscount.html'),
			controller: 'setDiscount',
			size: 'lg',
			resolve: {
				item : function () {
					return item;
				},
				flag : function () {
					return false;
				}
			}
		});
		//关闭模态框刷新页面
		modalInstance.result.then(function(result) {
			item.discount_type = result.discount_type;
			item.discount_proportion = result.discount_proportion;
			item.discount_price = result.discount_price;
		});
	};

	//输入关键字搜索优惠名称 暂时不启用
	// $scope.loadItem = function (search){
	// 	if(search){
	// 		$resource('/api/as/puc/citydiscount/getSimDiscountList', {}, {}).save({'name': search}, function(res){
	// 			if(res.errcode === 0){
	// 				$scope.discount_name = res.data;
	// 			}else{
	// 				alert(res.errmsg);
	// 			}
	// 		});
	// 	}
	// }

	//添加优惠产品
	$scope.addDiscountInfo = function() {
		var modalInstance = $modal.open({
			template: require('../views/addDiscountInfo.html'),
			controller: 'addDiscountInfo',
			size: 'lg',
			resolve: {
				discount : function () {
					return $scope.discountInfo;
				}
			}
		});
		//关闭模态框刷新页面
		modalInstance.result.then(function() {
			$scope.search(1);
		});
	}

};