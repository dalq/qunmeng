module.exports = function ($scope, productNo, $modalInstance, $resource, toaster) {
    $resource("/api/uc/dc/ziwoyouService/getProductInfoByCode", {}, {}).save({ productNo: productNo, sale_belong: 'supply_ziwoyou' }, function (res) {
        if (res.errcode === 0) {
            $scope.obj = res.data.product;
        }
        else {
            toaster.error({ title: "提示", body: res.errmsg });
        }
    });
};