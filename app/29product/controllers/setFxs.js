module.exports = function ($scope, $modalInstance, $resource, $modal, item, toaster){

    $scope.office = item;
    $scope.sel_list = [];
    $scope.search = {};
    $scope.floating_price = 0;
    $scope.load = function () {
        $resource('/api/ac/tc/ticketSaleAuthService/findDownCompanyCode', {}, {}).save({'sale_code': item.code}, function(res){
            if(res.errcode === 0){
                $scope.office_list = res.data;
                $scope.search.office = $scope.office_list[0];
            }else{
                toaster.error({title: '', body: res.errmsg});
            }
        });
        $resource('/api/as/tc/saleauth/findDownstreamList', {}, {}).save({'sale_code': item.code}, function(res){
            if(res.errcode === 0){
                $scope.company_list = res.data;
                for (let index = 0; index < $scope.company_list.length; index++) {
                    const element = $scope.company_list[index];
                    element.is_edit = false;
                }
            }else{
                toaster.error({title: '', body: res.errmsg});
            }
        });
    }
    $scope.load();

    //添加协议价信息
    $scope.add = function () {
        if (!$scope.search.office.name || !$scope.search.office.code) {
            toaster.error({title: '', body: '请选择机构'});
            return false;
        }
        for(var i = 0; i < $scope.company_list.length; i++){
            if($scope.search.office.code == $scope.company_list[i].downstream_company_code){
                toaster.error({title: '', body: '不能重复添加'});
                return;
            }
        }
        var para = []
        var temp = {
            'company_name': $scope.search.office.name,
            'company_code': $scope.search.office.code,
            'floating_price': $scope.floating_price,
            'sale_code': item.code
        };
        para.push(temp);
        $resource('/api/ac/tc/ticketSaleAuthService/save', {}, {}).save({'company_list': para}, function (res) {
            if (res.errcode === 0) {
                $scope.floating_price = 0;
                $scope.load();
                toaster.success({title: '', body: '添加成功'});
            } else {
                toaster.error({title: '', body: res.errmsg});
            }
        });
    }
    
    //删除已选机构
    $scope.remove = function (code) {
        var para = {
            company_code: code,
            sale_code: item.code
        }
        $resource('/api/as/tc/saleauth/delDown', {}, {}).save(para, function (res) {
            if (res.errcode === 0) {
                $scope.load();
                toaster.success({title: '', body: '删除成功'});
            } else {
                toaster.error({title: '', body: res.errmsg});
            }
        });
    };
    
    //确认
    $scope.save = function (company, index) {
        if (company.floating_price < 0) {
            toaster.error({title: '', body: '加价只能大于0'});
            return false;
        }
        var para = {
            company_code: company.downstream_company_code,
            sale_code: item.code,
            floating_price: company.floating_price
        }
        $resource('api/as/tc/saleauth/updateFloatingPrice', {}, {}).save(para, function (res) {
            if (res.errcode === 0) {
                // $scope.load();
                $scope.company_list[index].is_edit = false;
                toaster.success({title: '', body: '修改成功'});
            } else {
                toaster.error({title: '', body: res.errmsg});
            }
        });
    }

    $scope.change = function (index) {
        $scope.company_list[index].is_edit = true;
    }

    //确认
    $scope.ok = function () {
        $modalInstance.close($scope.sel_list.length);
    }
    
    //取消
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

};