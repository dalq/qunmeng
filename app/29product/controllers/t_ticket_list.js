module.exports = function($scope, $state, $stateParams, $modalInstance, $resource,$modal,FileUploader,toaster,code,date2str,str2date){
    var sale_code = code;
    $scope.a = '1';
    $resource('/api/as/tc/salettype/typelist', {}, {}).
            save({'sale_code' : sale_code},function (res) {
                if (res.errcode != 0) {
                    toaster.error({title:"",body:res.errmsg});
                    return;
                }
                $scope.typearr = res.data;                   
            });   
    

    $scope.updatearr = [{name:'开始时间',value:'0'},
                                    {name:'结束时间',value:'1'},
                                    {name:'是否需要预约',value:'2'},
                                    {name:'景区编号',value:'3'}]
    $scope.vm = {
        'date' : '',
        'options' : {
            //  format: "YYYY-MM-DD",
            locale : 'zh-cn',
            showClear: true  ,                      
            //  clearBtn:true
        }
    }
    $scope.vm1 = {
        'date' : '',
        'options' : {
            //  format: "YYYY-MM-DD",
            locale : 'zh-cn',
            showClear: true                        
            
        }
    }

    $scope.searchform = {};
    /* 分页
     * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10         //每页显示几条

    $scope.getlist = function () {
        if($scope.searchform.type==''||$scope.searchform.type ==undefined){
            toaster.error({title: "", body: "请选择一条票种"});
            return;
        }

        if ( $scope.vm.date === null) {
            $scope.vm.date = '';
        }
        if (typeof $scope.vm.date._d === 'string') {
            $scope.vm.date._d = $scope.vm.date._d;
        } else {
            $scope.vm.date._d = time2str($scope.vm.date._d);
        }

        if ( $scope.vm1.date === null) {
            $scope.vm1.date = '';
        }
        if (typeof $scope.vm1.date._d === 'string') {
            $scope.vm1.date._d = $scope.vm1.date._d;
        } else {
            $scope.vm1.date._d = time2str($scope.vm1.date._d);
        }
         
        
        var para = {
            pageNo:$scope.bigCurrentPage, 
            pageSize:$scope.itemsPerPage,
            sale_code:sale_code,
            create_time_start : ($scope.vm.date._d),
            create_time_end : ($scope.vm1.date._d),
            type : $scope.searchform.type.ticket_type_code,
            type_attr :$scope.searchform.type.ticket_type_attr,
            order_code : $scope.searchform.order_code

        };

        if($scope.searchform.update == '0'){
            para.period_start = '1';
            para.period = '';
            para.appoint = '';
        }
        if($scope.searchform.update == '1'){
            para.period_start = '';
            para.period = '1';
            para.appoint = '';
        }
        if($scope.searchform.update == '2'){
            para.period_start = '';
            para.period = '';
            para.appoint = '1';
        }
        
        // var para = angular.extend($scope.searchform,para);
        $resource('/api/as/tc/salettype/ticketList', {}, {}).
        save(para,function (res) {
            if (res.errcode != 0) {
                toaster.error({title:"",body:res.errmsg});
                return;
            }
            $scope.objs = res.data.results;
            $scope.bigTotalItems = res.data.totalRecord;
            if($scope.objs.length == 0){
                $scope.a = '1';
            }else {
                $scope.a = '0';
            }
            for(var i = 0;i<$scope.objs.length;i++){
                if($scope.objs[i].appoint=='0'){
                    $scope.objs[i].appoint = '无需预约'
                }
                if($scope.objs[i].appoint=='1'){
                    $scope.objs[i].appoint = '需要预约'
                }
            }
            // $scope.objs.forEach(function(element) {
            //     element.isSelected = element.state=='1'?true:false;
            // }, this);
        });   
    };
    // $scope.getlist();

    $scope.updateticket = function(index){
        $scope.item = $scope.objs[index];
        if($scope.searchform.update == undefined){
            toaster.error({title: "", body: "请选择要更新的数据"});
            return;
        }

        var paras = {
            sale_code:sale_code,
            period_start : ($scope.vm.date._d),
            period : ($scope.vm1.date._d),
            type_attr :$scope.searchform.type.ticket_type_attr,
            type : $scope.searchform.type.ticket_type_code,
            order_code : $scope.searchform.order_code   ,
            code : $scope.item.code,
            place_code : ''

        };

        if($scope.searchform.update == '0'){
            paras.period_start = '1';
            paras.period = '';
            paras.appoint = '';
        }
        if($scope.searchform.update == '1'){
            paras.period_start = '';
            paras.period = '1';
            paras.appoint = '';
        }
        if($scope.searchform.update == '2'){
            paras.period_start = '';
            paras.period = '';
            paras.appoint = '1';
        }
        if($scope.searchform.update == '3'){
          paras.period_start = '';
          paras.period = '';
          paras.appoint = '';
          paras.place_code = '1';
      }
        if(confirm('亲~~~确定要更新吗?')){
            $resource('/api/as/tc/salettype/updatePeriod', {}, {}).
            save(paras,function (res) {
                if (res.errcode != 0) {
                    toaster.error({title:"",body:res.errmsg});
                    return;
                }
                toaster.success({title:"",body:"更新成功"});
                $scope.getlist();
                
            });   
        }

    }

    $scope.oneupdate = function(){
        if($scope.searchform.update == undefined){
            toaster.error({title:"",body:'请选择需要更新的数据'});
            return;
        }
        if($scope.objs.length ==0){
            toaster.error({title:"",body:'无可更新的数据'});
            return;
        }

        var paras2 = {
            sale_code:sale_code,
            create_time_start : ($scope.vm.date._d),
            create_time_end: ($scope.vm1.date._d),
            type_attr :$scope.searchform.type.ticket_type_attr,
            type : $scope.searchform.type.ticket_type_code,
            order_code : $scope.searchform.order_code ,
            place_code : ''    
        };

        if($scope.searchform.update == '0'){
            paras2.period_start = '1';
            paras2.period = '';
            paras2.appoint = '';
        }
        if($scope.searchform.update == '1'){
            paras2.period_start = '';
            paras2.period = '1';
            paras2.appoint = '';
        }
        if($scope.searchform.update == '2'){
            paras2.period_start = '';
            paras2.period = '';
            paras2.appoint = '1';
        }
        if($scope.searchform.update == '3'){
            paras2.period_start = '';
            paras2.period = '';
            paras2.appoint = '';
            paras2.place_code = '1';
        }
        if(confirm('亲~~~确定要全部更新吗?')){      
            $resource('/api/as/tc/salettype/updatePeriod', {}, {}).
            save(paras2,function (res) {
                if (res.errcode != 0) {
                    toaster.error({title:"",body:res.errmsg});
                    return;
                }
                toaster.success({title:"",body:'更新成功'});
                $scope.getlist();
                
            });   
        }
    }


    function time2str(objDate) {
        if(angular.isDate(objDate))
        {
            var y = objDate.getFullYear();
            var m = objDate.getMonth();
            var d = objDate.getDate();
            var h = objDate.getHours();
            var mt = objDate.getMinutes();
            // var s = objDate.getSeconds();
            return y + '-' + (m + 1) + '-' + d + ' ' + h + ':' + mt;
        }
        else
        {
            return '错误格式';
        }
    }



}