module.exports = function ($scope, $state, $resource, $modal, toaster) {


	//加载销售品分类下拉搜索
	$scope.loadSaleCategory = function (){
		$resource('/api/as/sc/dict/dictbytypelist', {}, {}).save({'type': 'sale_category'}, function(res){
			if(res.errcode === 0){
				$scope.sale_category = res.data;
				$scope.sale_category.splice(0,0,{'label': '全部', 'value': '', 'type': 'sale_category'});
			}else{
				toaster.error({title: '', body: res.errmsg});
			}
		});
	}

	//加载我的仓库景区列表
	// $scope.loadMyPlaceList = function (){
	// 	$resource('/api/as/puc/authorizedt/findMyPlaceList', {}, {}).save({}, function(res){
	// 		if(res.errcode === 0){
	// 			$scope.my_place_list = res.data;
	// 			if(res.data.length > 0){
	// 				$scope.my_place_list.splice(0,0,{'name': '全部', 'place_code': ''})
	// 			}
	// 		}else{
	// 			toaster.error({title: '', body: res.errmsg});
	// 		}
	// 	});
	// }

	//加载登录账号的协议价信息
	$scope.loadOfficeInfo = function (){
		$resource('/api/ac/tc/agreementPriceService/setOfficeInfo', {}, {}).save({}, function(res){
			if(res.errcode === 0){
				$scope.officeInfo = res.data;
			} else {
				toaster.error({title: '', body: '加载个人信息失败'});
			}
		});
	}

	//搜索我的采纳产品库
	$scope.search = function (pageNo){
		$scope.all = false;		//每次搜索都重置全选按钮
		$scope.searchform.pageNo = pageNo;
		$scope.searchform.pageSize = $scope.page.itemsPerPage;
		$resource('/api/ac/tc/ticketSaleAuthService/findSaleAuthList', {}, {}).save($scope.searchform, function(res){
			if(res.errcode === 0){
				$scope.sale_list = res.data.results;
				$scope.page.totalItems = res.data.totalRecord;
				$scope.loading = false;
				$scope.page.currentPage = pageNo;
			}else{
				toaster.error({title: '', body: res.errmsg});
			}
		});
	}

	init();
	//初始化查询产品列表
	function init(){
		$scope.page = {'currentPage': 1, 'itemsPerPage': 10};
		$scope.bak = {};
		$scope.loading = true;
		$scope.searchform = {};
		$scope.loadOfficeInfo();
		$scope.search(1);
		$scope.loadSaleCategory();
		// $scope.loadMyPlaceList();
	}

	//查看产品已分配的下属浮动价机构
	$scope.open = function (info, index){
		//关闭上一个已打开的下拉信息
		$scope.bak.spread = $scope.bak == info ? info.spread : false;
		if(!info.spread){
			if(info.issysarea == 1){
				info.delBtn = info.open_company == 0 && info.open_sysarea == 1;
			} else {
				info.delBtn = info.open_company == 0;
			}
			$scope.bak = info;
			$scope.site = index;
			info.loading = true;
			info.spread = true;
			if(info.num != 0){
				$resource('/api/as/tc/saleauth/findDownstreamList', {}, {}).save({'sale_code': info.code}, function(res){
					if(res.errcode === 0){
						$scope.price_list = res.data;
						info.loading = false;
					}else{
						toaster.error({title: '', body: res.errmsg});
					}
				});
			}
		} else {
			info.spread = false;
		}
	}

	//下拉信息中修改协议价
	$scope.setDownPrice = function (price){
		var modalInstance = $modal.open({
			template: require('../views/setFloatingPrice.html'),
			controller: 'setFloatingPrice',
			size: 'lg',
			resolve: {
				item : function () {
					return price;
                },
				sale_code : function () {
					return $scope.bak.code;
                }
			}
		});
		//关闭模态框刷新页面
		modalInstance.result.then(function(floating_price) {
			price.floating_price = floating_price;
		});
	}

	//删除加载信息
	$scope.delDownPrice = function (info, price, index){
		var para = {
			'sale_code': info.code,
			'company_code': price.downstream_company_code
		}
		if(confirm('确认删除此授权信息?') == true){
			$resource('/api/as/tc/saleauth/delDown', {}, {}).save(para, function(res){
				if(res.errcode === 0){
					toaster.success({title: '', body: '删除成功'});
					$scope.price_list.splice(index, 1);
					info.num -= 1;
				}else{
					toaster.error({title: '', body: res.errmsg});
				}
			});
		}
	}
	
	//移出我的产品库
	$scope.delete = function (item){
		if(confirm('确认要删除该产品吗？') == true){
			if(item.type != 1){
				$resource('/api/as/tc/saleauth/del', {}, {}).save({'sale_code': item.code}, function(res){
					if(res.errcode === 0){
						$scope.search(1);
					}else{
						toaster.error({title: '', body: res.errmsg});
					}
				});
			}
		}

	}

	//批量删除
	$scope.delBatch = function(){
		var delList = [];
		angular.forEach($scope.sale_list, function(item){
			if(item.state && item.type != 1){
				delList.push(item.code);
			}
		});
		if(delList.length > 0){
			$resource('/api/as/tc/saleauth/del', {}, {}).save({'sale_code': delList.toString()}, function(res){
				if(res.errcode === 0){
					toaster.success({title: '', body: '成功删除' + delList.length + '条产品信息'});
					$scope.search(1);
				}else{
					toaster.error({title: '', body: res.errmsg});
				}
			});
		} else {
			toaster.warning({title: '', body: '请选择要删除的产品'});
		}
	}

	//全选or全不选----批量删除使用
    $scope.allSelect = function (){
		angular.forEach($scope.sale_list, function(item){
			item.state = $scope.all;
		});
    }

	//点对点授权
	$scope.PtPgrant = function (info){
		var modalInstance = $modal.open({
			template: require('../views/setPtPgrant.html'),
			controller: 'setPtPgrant',
			size: 'lg',
			resolve: {
				item : function () {
					return info;
                }
			}
		});
		//关闭模态框刷新页面
		modalInstance.result.then(function(num) {
			info.num += num;
			$scope.bak.spread = false;
		});
	}

	//设置公开范围
	$scope.openToDown = function (info, flag){
		var para = {'sale_code': info.code};
		var msg = '';
		if(flag){
			msg = info.open_company=='0'?'修改后将删除已授权的分销商，确认修改为对分销商不公开吗?':'确认修改为对分销商公开吗?';
			info.open_company = info.open_company=='0'?'1':'0';
			para.open_company = info.open_company;
		} else {
			if(info.issysarea == 1){
				info.open_sysarea = info.open_sysarea=='1'?'2':'1';
				para.open_sysarea = info.open_sysarea;
			} else {
				para.open_sysarea = '0';
			}
			msg = info.open_sysarea=='1'?'确认修改为对其他运营商的公开吗?':'修改后将删除已授权的运营商，确认修改为对其他运营商的不公开吗?';
		}
		if(confirm(msg) == true){
			$resource('/api/ac/tc/ticketSaleAuthService/updateOpen', {}, {}).save(para, function (res) {
				if (res.errcode === 0) {
					$scope.search($scope.page.currentPage);
				} else {
					$scope.reback(info, flag);
					toaster.error({title: '', body: '修改失败'});
				}
			});
		} else {
			$scope.reback(info, flag);
		}
	}

	//修改公开范围时,取消修改或者修改失败回退之前状态
	$scope.reback = function(info, flag){
		if(flag){
			info.open_company = info.open_company=='0'?'1':'0';
		} else {
			info.open_sysarea = info.open_sysarea=='1'?'2':'1';
		}
	}

	//跳转到摄制协议价页面
	$scope.setAgreementPrice = function (){
		$state.go('app.agreementPrice', {});
	}

	//修改协议金额
    $scope.setMoney = function (item){
		var param = {
			'flag': false,
			'item': item
		};
        var modalInstance = $modal.open({
			template: require('../views/setAgreementPrice.html'),
			controller: 'setAgreementPrice',
			size: 'lg',
			resolve: {
				param : function () {
					return param;
				},
				officeInfo : function () {
					return $scope.officeInfo;
				}
			}
		});
		//关闭模态框刷新页面
		modalInstance.result.then(function(result) {
			$scope.officeInfo.agreement_money = result.agreement_money;
			$scope.officeInfo.remark = result.remark;
		});
	}

	//修改协议金额
    $scope.autoSave = function (){
		var msg = $scope.officeInfo.automatic_save=='1'?'确认修改为非自动获取上级公开产品吗?':'确认修改为自动获取上级公开产品吗?';
		var para = {
			'automatic_save': $scope.officeInfo.automatic_save=='1'?'0':'1',
			'target_company_code': $scope.officeInfo.code
		}
		if(confirm(msg) == true){
			$resource('/api/as/tc/agreementprice/setAutoSave', {}, {}).save(para, function(res){
				if(res.errcode !== 0){
					$scope.officeInfo.automatic_save = para.automatic_save;
				}
			});
		} else {
			$scope.officeInfo.automatic_save = para.automatic_save;
		}
	}

	//设置渠道商
    $scope.setChannel = function (item){
        var modalInstance = $modal.open({
			template: require('../views/setChannel.html'),
			controller: 'setChannel',
			size: 'lg',
			resolve: {
				sale_code : function () {
					return item.code;
				}
			}
		});
		//关闭模态框刷新页面
		modalInstance.result.then(function(result) {
			$scope.search(1);
		});
	}

	//popover样式,绑定html格式
	$scope.showChannelInfo = function(data){
		$("#" + data.code).popover({
			trigger: 'hover',
			placement : 'top',
			html: 'true',
			content : data.channel_list_name
		});
		$("#" + data.code).popover('show');
	}
	

};