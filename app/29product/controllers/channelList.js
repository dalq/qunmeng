module.exports = function ($scope, $state, $resource, $modal, toaster){

	//查询渠道列表
	$scope.search = function (pageNo){
		var para = {
			'pageNo': pageNo || $scope.currentPage,
			'pageSize': $scope.itemsPerPage,
			'channel_list_name': $scope.searchform.channel_name
		}
		$resource('/api/as/tc/salechannel/findAllList', {}, {}).save(para, function(res){
			if(res.errcode === 0){
				$scope.list = res.data.results;
				$scope.totalItems = res.data.totalRecord;
				$scope.currentPage = pageNo;
			}else{
				toaster.error({title: '', body: res.errmsg});
			}
		});
	}

	init();
    //初始化列表
    function init(){
		$scope.currentPage = 1;		//当前页码
		$scope.itemsPerPage = 10;	//每页显示几条
		$scope.searchform = {};
		$scope.search(1);
	}

	//添加or修改
	$scope.setChannel = function(info) {
		var modalInstance = $modal.open({
			template: require('../views/addChannel.html'),
			controller: 'addChannel',
			size: 'lg',
			resolve: {
				item : function () {
					return info;
				},
				flag : function () {
					return info == 1;
				}
			}
		});
		//关闭模态框刷新页面
		modalInstance.result.then(function() {
			init();
		});
	}


};