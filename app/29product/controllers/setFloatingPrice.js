module.exports = function ($scope, $modalInstance, $resource, $modal, item, sale_code, toaster){

    $scope.item = angular.copy(item);
    //确认
    $scope.ok = function () {
        var para = {
            'floating_price': $scope.item.floating_price,
            'company_code': $scope.item.downstream_company_code,
            'sale_code': sale_code
        }
        $resource('/api/as/tc/saleauth/updateFloatingPrice', {}, {}).save(para, function(res){
            if(res.errcode === 0){
                $modalInstance.close(para.floating_price);
            }else{
                toaster.error({title: '', body: res.errmsg});
            }
        });
    }
    
    //取消
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

};