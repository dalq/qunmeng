module.exports = function($scope, dictbytypelist, salecategoryinsert, $modalInstance, toaster){

    $scope.obj = {};

    dictbytypelist({'type' : 'sale_category'}).then(function(res) {
        if(res.errcode === 0)
        {
            $scope.typearr = res.data;
            $scope.obj.sale_category_code=$scope.typearr[0].value;
        }
        else
        {
            toaster.error({ title: "提示", body: res.errmsg });
        }
    });

    dictbytypelist({'type' : 'ticket_sale_sub_table'}).then(function(res) {
        if(res.errcode === 0)
        {
            $scope.salesubarr = res.data;
            $scope.obj.sub_table_code=$scope.salesubarr[0].value;
        }
        else
        {
            toaster.error({ title: "提示", body: res.errmsg });
        }
    });

    $scope.gogo = function(){

        salecategoryinsert.save($scope.obj, function(res){

            if(res.errcode === 0)
            {
                $modalInstance.close();
            }
            else
            {
                toaster.error({ title: "提示", body: res.errmsg });
            }

        });

    };
   
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
	
};