module.exports = function ($scope, $modalInstance, $resource, $modal, item, flag, toaster){
    
    init();
    function init() {
        $scope.flag = flag;
        $scope.obj = angular.copy(item) || {
            'discount_type': '1',
            'verification_type': '0',
            'discount_limit_type': '0'
        };
    };


    //确认
    $scope.ok = function () {
        var url = flag ? '/api/as/puc/citydiscount/saveDiscount' : '/api/as/puc/citydiscountinfo/setDiscountInfo'
        $resource(url, {}, {}).save($scope.obj, function (res) {
            if (res.errcode === 0) {
                $modalInstance.close($scope.obj);
            } else {
                toaster.error({title: '', body: res.errmsg});
            }
        });
    };
    
    //取消
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

};