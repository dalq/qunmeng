module.exports = function ($scope, $stateParams, $http, $q, FileUploader, str2date, date2str, $resource, toaster, productid, $modalInstance, auditing) {

	var id = $stateParams.id || productid;


	//产品对象，保存着产品的基本信息
	$scope.saleobj = {
		'id': id || '',
	};

	$scope.util = {
		'str2date': str2date,
		'date2str': date2str,
		'auditing': auditing,
		'$modalInstance': $modalInstance,
		'apply_note_temp': '',
		'showFlag': false
	};
	//销售品功能列表
	$scope.funobj = {};

	//基本信息中需要提前弄好的信息。
	$scope.baseinfo = {
		'imageshow': {},
		'dateshow': {},
	};

	var uploader1 = $scope.uploader1 = new FileUploader({
		url: 'https://dlqt.juyouhx.com/Api/Api/ObjectToOss?topdir=aa&selfdir=bb'
	});
	uploader1.filters.push({
		name: 'imageFilter',
		fn: function (item /*{File|FileLikeObject}*/, options) {
			var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
			return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
		}
	});
	uploader1.onSuccessItem = function (fileItem, response, status, headers) {
		$scope.saleobj.top_pic = response.savename;
	};

	var uploader2 = $scope.uploader2 = new FileUploader({
		url: 'https://dlqt.juyouhx.com/Api/Api/ObjectToOss?topdir=aa&selfdir=bb'
	});
	uploader2.filters.push({
		name: 'imageFilter',
		fn: function (item /*{File|FileLikeObject}*/, options) {
			var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
			return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
		}
	});
	uploader2.onSuccessItem = function (fileItem, response, status, headers) {
		$scope.saleobj.logo = response.savename;
	};

	$scope.baseinfo.imageshow.uploader1 = uploader1;
	$scope.baseinfo.imageshow.uploader2 = uploader2;


	$scope.baseinfo.dateshow.periodstart = {
		'date': new Date(),
	};

	$scope.baseinfo.dateshow.periodend = {
		'date': new Date($scope.baseinfo.dateshow.periodstart.date.getFullYear(), 11, 31),
	};

	$scope.baseinfo.dateshow.open = function (obj) {
		obj.opened = true;
	};


	// $scope.apply_note_temp = $scope.saleobj.apply_note ? $scope.saleobj.apply_note : '审核意见必填';

	$scope.pass = function () {
		if (!$scope.saleobj.apply_note) {
			toaster.error({ title: "提示", body: '审核意见必填' })
			return false;
		}
		$resource('/api/ac/tc/ticketSaleService/updateSaleApplyPass', {}, {})
			.save({
				'id': $scope.saleobj.id,
				'apply_note': $scope.saleobj.apply_note
			}, function (res) {
				if (res.errcode !== 0) {
					toaster.error({ title: "提示", body: res.errmsg });
					toaster.error({ title: "提示", body: res.errmsg });
					return;
				}
				toaster.success({ title: "提示", body: "操作成功!" });
				// toaster.success({title:"",body:'亲,记得重新保存商客专属设置信息哦💐💐'});
				$scope.util.$modalInstance.close();
			});
	};

	$scope.nopass = function () {
		if (!$scope.saleobj.apply_note) {
			toaster.error({ title: "提示", body: '审核意见必填' })
			return false;
		}
		$resource('/api/ac/tc/ticketSaleService/updateSaleApplyNoPass', {}, {})
			.save({
				'id': $scope.saleobj.id,
				'apply_note': $scope.saleobj.apply_note
			}, function (res) {
				if (res.errcode !== 0) {
					toaster.error({ title: "提示", body: res.errmsg });
					// toaster.error({ title: "提示", body: res.errmsg });
					return;
				}
				// toaster.success({ title: "提示", body: "操作成功!" });
				toaster.success({ title: "提示", body: "操作成功!" });
				$scope.util.$modalInstance.close();
			});
	};


	$scope.auditingFlag = false;
	$scope.auditingInfo = 5;

	function timing() {
		if ($scope.auditingInfo > 0) {
			$scope.auditingInfo--;
			$scope.$apply();
		} else if ($scope.auditingInfo == 0) {
			clearInterval(intervalProcess);
			$scope.auditingFlag = true;
			$scope.$apply();
		}
	}
	var intervalProcess = setInterval(function () { timing() }, 1000);

	$scope.showattrarr = [
		{
			key: 'market_price_display',
			position: 'left',
			// before: '<font color=red >市场价格: ¥</font>'
		},
		{
			key: 'guide_price_display',
			position: 'left',
			// before: '<font color=green >平台价格: ¥</font>'
		},
		{
			key: 'cost_price_display',
			position: 'left',
			// before: '<font color=red >成本价格: ¥</font>'
		},
		{
			key: 'purchase_price_display',
			position: 'left',
			// before: '<font color=green >采购价格: ¥</font>'
		},
		{
			key: 'current_stock_num_display',
			position: 'left',
			// before: '<font color=red >当前库存: ¥</font>'
		}
	];

	$scope.data = [];

	
	$scope.findinfoList = function (str) {
		var para = {
			sale_code: $scope.saleobj.code
		}
		$resource('/api/ac/tc/ticketSaleDayPriceService/getDayPriceBySaleCode', {}, {}).save(para, function (res) {
			if (res.errcode === 0) {
				for (var index = 0; index < res.data.daylist.length; index++) {
					var date = res.data.daylist[index].date;
					var id = res.data.daylist[index].id;
					// $scope.dateArray[date] = id;
					var element = {};
					element.d = res.data.daylist[index].date;
					// element.market_price_display = '<font color=red >' + res.data.daylist[index].market_price + '</font>';
					// element.guide_price_display = '<font color=green >' + res.data.daylist[index].guide_price + '</font>';
					// element.cost_price_display = '<font color=red >' + res.data.daylist[index].cost_price + '</font>';
					// element.purchase_price_display = '<font color=green >' + res.data.daylist[index].purchase_price + '</font>';
					// element.current_stock_num_display = '<font color=red >' + res.data.daylist[index].current_stock_num + '</font>';
					if( res.data.daylist[index].state == '0'){
						element.market_price_display = '<font color=red >市场价格: ¥</font><font color=red >' + 
																res.data.daylist[index].market_price + '</font>';
						element.guide_price_display = '<font color=green >平台价格: ¥</font><font color=green >' + 
																res.data.daylist[index].guide_price + '</font>';
						element.cost_price_display = '<font color=red >成本价格: ¥</font><font color=red >' + 
																res.data.daylist[index].cost_price + '</font>';
						element.purchase_price_display = '<font color=green >采购价格: ¥</font><font color=green >' + 
																res.data.daylist[index].purchase_price + '</font>';
						element.current_stock_num_display = '<font color=red >当前库存: </font><font color=red >' + 
																res.data.daylist[index].current_stock_num + '</font>';
					} else if( res.data.daylist[index].state == '1') {
						element.market_price_display = '<font color=grey >市场价格: ¥</font><font color=grey >' + 
												res.data.daylist[index].market_price + '</font>';
						element.guide_price_display = '<font color=grey >平台价格: ¥</font><font color=grey >' + 
												res.data.daylist[index].guide_price + '</font>';
						element.cost_price_display = '<font color=grey >成本价格: ¥</font><font color=grey >' + 
												res.data.daylist[index].cost_price + '</font>';
						element.purchase_price_display = '<font color=grey >采购价格: ¥</font><font color=grey >' + 
												res.data.daylist[index].purchase_price + '</font>';
						element.current_stock_num_display = '<font color=grey >当前库存: </font><font color=grey >' + 
												res.data.daylist[index].current_stock_num + '</font>';
					}
					element.market_price = res.data.daylist[index].market_price;
					element.guide_price = res.data.daylist[index].guide_price;
					element.cost_price = res.data.daylist[index].cost_price;
					element.purchase_price = res.data.daylist[index].purchase_price;
					element.current_stock_num = res.data.daylist[index].current_stock_num;
					element.state = res.data.daylist[index].state;
					element.id = res.data.daylist[index].id;
					element.sale_code = res.data.daylist[index].sale_code;
					$scope.data[index] = element;
				}
			} else {
				toaster.warning({ title: '', body: res.errmsg });
			}
		});
	}

	$scope.$watch('saleobj', function (newValue) {
		$scope.findinfoList();
	}, true);

	$scope.options = {
		state: 'state',
		checkbox: false
	}

	$scope.changeShow = function(){
		$scope.util.showFlag = !$scope.util.showFlag;
	}

};