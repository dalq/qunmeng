module.exports = function ($scope, $resource, $modalInstance, $modal, saleCode, toaster){

    //加载全部销售品列表
	$scope.loadSaleList = function (pageNo){
        var para = {
            'channel_list_code': saleCode,
            'pageNo': pageNo,
			'pageSize': $scope.itemsPerPage
        }
        $resource('/api/as/tc/salechannel/findCanSetSalelList', {}, {}).save(para, function (res) {
            if (res.errcode === 0) {
                $scope.sale_list = res.data.results;
                $scope.totalItems = res.data.totalRecord;
                if($scope.save_list.length > 0){
                    angular.forEach($scope.sale_list, function(item){
                        for(var i = 0; i < $scope.save_list.length; i++){
                            if(item.code == $scope.save_list[i].code){
                                item.state = true;
                                continue;
                            }
                        }
                    });
                }
                $scope.all = false;
            } else {
                toaster.error({title: '', body: res.errmsg});
            }
        });
    }

    init();
    function init(){
        $scope.currentPage = 1;		//销售品列表页码
        $scope.itemsPerPage = 10;	//每页显示几条
        $scope.save_list = [];
        $scope.loadSaleList(1);
    }

    //保存渠道设置销售品
    $scope.save = function () {
        var savestr = '';
        angular.forEach($scope.save_list, function(item){
            savestr += item.code + ',';
        });
        var para = {
            'sale_code': savestr.substring(0, savestr.length - 1),
            'channel_list_code': saleCode
        }
        $resource('/api/as/tc/salechannel/saveChannel', {}, {}).save(para, function (res) {
            if (res.errcode === 0) {
                $modalInstance.close();
            } else {
                toaster.error({title: '', body: res.errmsg});
            }
        });
    }

    //全选or全不选
    $scope.allSelect = function (all){
        if(all){
            angular.forEach($scope.sale_list, function(item){
                if(!item.state){
                    $scope.save_list.push(item);
                }
                item.state = true;
            });
        } else {
            angular.forEach($scope.sale_list, function(item){
                item.state = false;
                for(var i = 0; i < $scope.save_list.length; i++){
                    if(item.code == $scope.save_list[i].code){
                        $scope.save_list.splice(i, 1);
                    }
                }
            });
        }
    }

    //列表勾选,添加到已选列表
    $scope.selectOne = function (item) {
        if(item.state){
            $scope.save_list.push(item);
        } else {
            for(var i = 0; i < $scope.save_list.length; i++){
                if(item.code == $scope.save_list[i].code){
                    $scope.save_list.splice(i, 1);
                }
            }
        }
    }

    //已选列表删除
    $scope.remove = function (index) {
        var code = $scope.save_list[index].code;
        angular.forEach($scope.sale_list, function(item){
            if(code == item.code){
                item.state = false;
            }
        });
        $scope.save_list.splice(index, 1);
    }

    //取消
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    }


};