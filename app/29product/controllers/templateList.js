module.exports = function ($scope, $state, $resource, $modal, str2date, date2str, toaster) {
	$scope.searchform = {};

    /* 分页
     * ========================================= */
	$scope.maxSize = 5;            //最多显示多少个按钮
	$scope.bigCurrentPage = 1;      //当前页码
	$scope.itemsPerPage = 10;         //每页显示几条

	$scope.myKeyup = function (e) {
		//IE 编码包含在window.event.keyCode中，Firefox或Safari 包含在event.which中
		var keycode = window.event ? e.keyCode : e.which;
		if (keycode == 13) {
			$scope.load();
		}
	};


	$scope.load = function () {
		var para = {
			pageNo: $scope.bigCurrentPage,
			pageSize: $scope.itemsPerPage
		};
		para = angular.extend($scope.searchform, para);
		$resource('/api/as/tc/ticketsaletemplete/list', {}, {})
			.save(para, function (res) {

				if (res.errcode !== 0) {
					toaster.error({ title: "提示", body: res.errmsg });
					return;
				}
				$scope.objs = res.data;
				$scope.bigTotalItems = res.data.totalRecord;

			});

	};
	$scope.load();

	$scope.info = function (id) {
		var modalInstance = $modal.open({
			template: require('../views/editTemplate.html'),
			controller: 'editTemplate',
			url: '/editTemplate/:id',
			size: 'lg',
			resolve: {
				'id': function () {
					return id;
				},
				str2date: function () {
					return str2date;
				},
				date2str: function () {
					return date2str;
				}
			}
		});

		modalInstance.result.then(function () {
			$scope.load();
		}, function () {
			$scope.load();
		});

	};

};