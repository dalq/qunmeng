module.exports = function ($scope, $state, $resource, $modal, str2date, date2str, toaster,$stateParams


) {
    $scope.searchform = {};

    $scope.create = function () {
        $state.go('app.editProduct');
    };


    /* 分页
     * ========================================= */
    $scope.maxSize = 5;            //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10;         //每页显示几条

	$scope.statusArr = [
		{
			name:'----全部----',
			value:''
		},
		{
			name:'草稿',
			value:'0'
		},
		{
			name:'上架',
			value:'1'
		},
		{
			name:'下架',
			value:'2'
		}
	];


	// $resource('/api/as/sc/dict/dictbytypelist', {}, {})
	// 	.save({ type: 'sale_category' }, function (res) {

	// 		if (res.errcode !== 0) {
	// 			toaster.error({ title: "提示", body: res.errmsg });
	// 			return;
	// 		}

	// 		$scope.dictbytypelist = res.data;
	// 		$scope.dictbytypelist.splice(0,0,{
	// 			label:'----全部----',
	// 			value:''
	// 		})

    //     });
        if($stateParams.type == 'H'){
            $resource('/api/as/sc/dict/dictbytypelist', {}, {})
            .save({ type: 'sale_category',class_type : 'H' }, function (res) {
    
                if (res.errcode !== 0) {
                    toaster.error({ title: "提示", body: res.errmsg });
                    return;
                }
    
                $scope.dictbytypelist = res.data;
                $scope.dictbytypelist.splice(0,0,{
                    label:'----全部----',
                    value:''
                })
    
            });
        }else{
            $resource('/api/as/sc/dict/dictbytypelist', {}, {})
            .save({ type: 'sale_category' }, function (res) {
    
                if (res.errcode !== 0) {
                    toaster.error({ title: "提示", body: res.errmsg });
                    return;
                }
    
                $scope.dictbytypelist = res.data;
                $scope.dictbytypelist.splice(0,0,{
                    label:'----全部----',
                    value:''
                })
    
            });
        }



    
    if($stateParams.type == 'H'){
        $resource('/api/as/tc/placeview/jlist', {}, {})
		.save({type:'H'}, function (res) {

			if (res.errcode === 0) {
				$scope.viewarr = res.data;
				$scope.viewarr.unshift({ name: '--全部场所--', code: '' });
			}
			else {
                toaster.error({ title: "", body: res.data.errmsg })
			}

        });
    }else{
        $resource('/api/as/tc/placeview/jlist', {}, {})
		.save({}, function (res) {

			if (res.errcode === 0) {
				$scope.viewarr = res.data;
				$scope.viewarr.unshift({ name: '--全部场所--', code: '' });
			}
			else {
                toaster.error({ title: "", body: res.data.errmsg })
			}

        });
    }
    

	$scope.myKeyup = function (e) {

		//IE 编码包含在window.event.keyCode中，Firefox或Safari 包含在event.which中
		var keycode = window.event ? e.keyCode : e.which;
		if (keycode == 13) {
			$scope.load();
		}
	};

    $scope.load = function () {

        var para = {
            pageNo: $scope.bigCurrentPage,
            pageSize: $scope.itemsPerPage,
            sale_type :$stateParams.type
        };

        para = angular.extend($scope.searchform, para);

        $resource('/api/ac/tc/ticketSaleService/getApplySaleList', {}, {})
            .save(para, function (res) {

                if (res.errcode !== 0) {
                    toaster.error({ title: "提示", body: res.errmsg });
                    return;
                }

                $scope.objs = res.data.results;
                $scope.bigTotalItems = res.data.totalRecord;
                
            });

    };
    $scope.load();




    $scope.info = function (id) {

        //$state.go('app.editsale', {'id' : id, 'type' : 'info'});

        var modalInstance = $modal.open({
            template: require('../views/productInfo.html'),
            controller: 'infoProduct',
            url: '/infoProduct/:id',
            size: 'lg',
            resolve: {
                'productid': function () {
                    return id;
                },
                str2date: function () {
                    return str2date;
                },
                date2str: function () {
                    return date2str;
                },
                auditing: function () {
                    return true;
                }

            }
        });
        modalInstance.opened.then(function () {// 模态窗口打开之后执行的函数  
            // $scope.load();
        });
        modalInstance.result.then(function (showResult) {
            $scope.load();
        }, function (reason) {
            // // click，点击取消，则会暑促cancel  
            // $log.info('Modal dismissed at: ' + new Date());
        });

    };






};