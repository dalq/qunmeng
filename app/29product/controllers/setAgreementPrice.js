module.exports = function ($scope, $modalInstance, $resource, $modal, param, officeInfo, toaster){
    
    init();
    //初始化操作
    function init(){
        $scope.flag = param.flag;
        $scope.obj = param.flag ? param.item : angular.copy(param.item);
        $scope.officeInfo = officeInfo;
        $scope.sel_list = [];
        $scope.search = {};
    }

    //添加协议价信息
    $scope.add = function () {
        for(var i = 0; i < $scope.sel_list.length; i++){
            if($scope.search.office.code == $scope.sel_list[i].target_company_code){
                toaster.error({title: '', body: '不能重复添加'});
                return;
            }
        }
        var temp = {
            'target_company_name': $scope.search.office.name,
            'target_company_code': $scope.search.office.code,
            'agreement_money': 0
        };
        $scope.sel_list.push(temp);
    }

    //删除已选机构
    $scope.remove = function (index) {
        $scope.sel_list.splice(index,1)
    };

    //确认
    $scope.ok = function () {
        var para = {};
        var url = '';
        if(param.flag){
            if($scope.sel_list.length == 0){
                toaster.warning({title: '', body: '无添加数据'});
                return;
            }
            para.add_list = $scope.sel_list;
            url = '/api/ac/tc/agreementPriceService/save';
        } else {
            para.id = param.item.id;
            para.target_company_code = param.item.target_company_code;
            para.agreement_money = $scope.obj.agreement_money;
            para.remark = $scope.obj.remark;
            url = '/api/ac/tc/agreementPriceService/updatePrice';
        }
        $resource(url, {}, {}).save(para, function (res) {
            if (res.errcode === 0) {
                param.item.agreement_money = para.agreement_money;
                param.item.remark = para.remark;
                $modalInstance.close(param.item);
            } else {
                toaster.error({title: '', body: res.errmsg});
            }
        });
    }
    
    //取消
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

};