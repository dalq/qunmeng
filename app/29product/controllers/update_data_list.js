module.exports = function ($scope, $state, $resource, $modal, str2date, date2str,
	saleup, saledown, saleupdate, talist, sellerListno, sellerList, tstcreateno, tstcreate, tststartno, tststart, tststopno, tststop, toaster

) {
	$scope.searchform = {};
	// $scope.searchform.sale_category = '';
	$scope.dictbytypelist = [];

	$scope.create = function () {
		$state.go('app.editProduct');
	};

    /* 分页
     * ========================================= */
	$scope.maxSize = 5;            //最多显示多少个按钮
	$scope.bigCurrentPage = 1;      //当前页码
	$scope.itemsPerPage = 10;         //每页显示几条

	$scope.statusArr = [
		{
			name:'----全部----',
			value:''
		},
		{
			name:'草稿',
			value:'0'
		},
		{
			name:'上架',
			value:'1'
		},
		{
			name:'下架',
			value:'2'
		}
	];


	$resource('/api/as/sc/dict/dictbytypelist', {}, {})
		.save({ type: 'sale_category' }, function (res) {

			if (res.errcode !== 0) {
				toaster.error({ title: "提示", body: res.errmsg });
				return;
			}

			$scope.dictbytypelist = res.data;
			$scope.dictbytypelist.splice(0,0,{
				label:'----全部----',
				value:''
			})

		});


	$resource('/api/as/tc/placeview/jlist', {}, {})
		.save({}, function (res) {

			if (res.errcode === 0) {
				$scope.viewarr = res.data;
				$scope.viewarr.unshift({ name: '--全部景区--', code: '' });
			}
			else {
				toaster.error({ title: "", body: res.data.errmsg });
			}

		});

	$scope.myKeyup = function (e) {

		//IE 编码包含在window.event.keyCode中，Firefox或Safari 包含在event.which中
		var keycode = window.event ? e.keyCode : e.which;
		if (keycode == 13) {
			$scope.load();
		}
	};


	$scope.load = function () {

		var para = {
			pageNo: $scope.bigCurrentPage,
			pageSize: $scope.itemsPerPage,
			state : '1'
		};

		para = angular.extend($scope.searchform, para);


		$resource('/api/ac/tc/ticketSaleService/getSaleList', {}, {})
			.save(para, function (res) {

				if (res.errcode !== 0) {
					toaster.error({ title: "提示", body: res.errmsg });
					return;
				}
				$scope.objs = res.data.results;
				for (var index = 0; index < $scope.objs.length; index++) {
					switch ($scope.objs[index].sale_apply_state) {
						case 'new':
							$scope.objs[index].sale_apply_state_name = '';
							break;
						case 'apply_sale':
							$scope.objs[index].sale_apply_state_name = '申请上架';
							break;
						case 'wait_sale':
							$scope.objs[index].sale_apply_state_name = '审核通过等待上架';
							break;
						case 'for_sale':
							$scope.objs[index].sale_apply_state_name = '销售中';
							break;
						case 'pause':
							$scope.objs[index].sale_apply_state_name = '暂停销售';
							break;
						case 'sale_end':
							$scope.objs[index].sale_apply_state_name = '下架';
							break;
						case 'ban_sale':
							$scope.objs[index].sale_apply_state_name = '禁止销售';
							break;

						default:
							$scope.objs[index].sale_apply_state_name = '';
							break;
					}
				}
				$scope.bigTotalItems = res.data.totalRecord;

			});

	};
	$scope.load();

	$scope.updateDate = function(code){
		var modalInstance = $modal.open({
            template: require('../views/update_order_list.html'),
            controller: 'update_order_list',
            size: 'lg',
            resolve: {
                code: function () {
                    return code;
                },
                date2str : function(){
                    return date2str;
                },
                str2date: function () {
                    return str2date;
                },
                //  id : function(){
                // return id;
            // },
            }
        });
        modalInstance.result.then(function (showResult) {	
            $scope.load();
        });
	}

		$scope.updateDate2 = function(code){
		var modalInstance = $modal.open({
            template: require('../views/t_ticket_list.html'),
            controller: 't_ticket_list',
            size: 'lg',
            resolve: {
                code: function () {
                    return code;
                },
                date2str : function(){
                    return date2str;
                },
                str2date: function () {
                    return str2date;
                },
                //  id : function(){
                // return id;
            // },
            }
        });
        modalInstance.result.then(function (showResult) {	
            $scope.load();
        });
	}
	
	$scope.info = function (id) {

		// $state.go('app.productInfo', {'id' : id});
		var modalInstance = $modal.open({
			template: require('../views/productInfo.html'),
			controller: 'infoProduct',
			url: '/infoProduct/:id',
			size: 'lg',
			resolve: {
				'productid': function () {
					return id;
				},
				str2date: function () {
					return str2date;
				},
				date2str: function () {
					return date2str;
				},
				auditing: function () {
					return false;
				}
			}
		});

		modalInstance.result.then(function () {
			//load();
		}, function () {
			//$log.info('Modal dismissed at: ' + new Date());
		});

	};



	
	

};