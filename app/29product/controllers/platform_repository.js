module.exports = function ($scope, $state, $resource, $modal, toaster) {

	//加载销售品分类下拉搜索
	$scope.loadSaleCategory = function (){
		$resource('/api/as/sc/dict/dictbytypelist', {}, {}).save({'type': 'sale_category'}, function(res){
			if(res.errcode === 0){
				$scope.sale_category = res.data;
				$scope.sale_category.splice(0,0,{'label': '全部', 'value': '', 'type': 'sale_category'});
			}else{
				toaster.error({title: '', body: '加载销售品列表失败'});
			}
		});
	}

	//加载平台产品库景区列表-----作废
	// $scope.loadPtpPlaceList = function (){
	// 	$resource('/api/as/puc/authorizedt/findPtPlaceList', {}, {}).save({}, function(res){
	// 		if(res.errcode === 0){
	// 			$scope.ptp_place_list = res.data;
	// 			if(res.data.length > 0){
	// 				$scope.ptp_place_list.splice(0,0,{'name': '全部', 'place_code': ''})
	// 			}
	// 		}else{
	// 			alert(res.errmsg);
	// 		}
	// 	});
	// }

	//加载登录账号的协议价信息
	$scope.loadOfficeInfo = function (){
		$resource('/api/ac/tc/agreementPriceService/setOfficeInfo', {}, {}).save({}, function(res){
			if(res.errcode === 0){
				$scope.officeInfo = res.data;
			} else {
				toaster.error({title: '', body: '加载个人信息失败'});
			}
		});
	}

	//搜索我的采纳产品库
	$scope.search = function (pageNo){
		$scope.searchform.pageNo = pageNo;
		$scope.searchform.pageSize = $scope.itemsPerPage;
		$resource('/api/ac/tc/ticketSaleAuthService/findSaleList', {}, {}).save($scope.searchform, function(res){
			if(res.errcode === 0){
				$scope.sale_list = res.data.results;
				$scope.totalItems = res.data.totalRecord;
				$scope.loading = false;
			}else{
				toaster.error({title: '', body: res.errmsg});
			}
		});
	}
	
	init();
	//初始化查询产品列表
	function init(){
		$scope.currentPage = 1;			//当前页码
		$scope.itemsPerPage = 10;		//每页显示几条;
		$scope.searchform = {};
		$scope.nothave = [
			{'name': '全部', 'value': null},
			{'name': '未采购', 'value': 'balala'}
		];
		$scope.loading = true;
		$scope.search(1);
		$scope.loadSaleCategory();
		// $scope.loadPtpPlaceList();
		$scope.loadOfficeInfo();
	}

	//添加至我的产品库
	$scope.addToMy = function (sale_code){
		$resource('/api/as/tc/saleauth/saveOpen', {}, {}).save({'sale_code': sale_code}, function(res){
			if(res.errcode === 0){
				toaster.success({title: '', body: '成功添加到我的产品库'});
				$scope.loading = true;
				$scope.search(1);
			}else{
				toaster.error({title: '', body: res.errmsg});
			}
		});
	}


};