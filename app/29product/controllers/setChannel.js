module.exports = function ($scope, $modalInstance, $resource, $modal, sale_code, toaster){
    
    $scope.all = false;
    init();
    //初始化操作
    function init(){
        $resource('/api/as/tc/saleauth/findCanSetChannelList', {}, {}).save({'sale_code': sale_code}, function(res){
            if(res.errcode === 0){
                $scope.channel_list = res.data;
                angular.forEach($scope.channel_list, function(item){
                    item.state = item.state==1?true:false;
                });
            }else{
                toaster.error({title: '', body: res.errmsg});
            }
        });
    }

    //全选or全不选
    $scope.allSelect = function () {
        angular.forEach($scope.channel_list, function(item){
            item.state = $scope.all;
        });
    }

    //确认
    $scope.ok = function () {
        var channel_code = '';
        angular.forEach($scope.channel_list, function(item){
            if(item.state == 1){
                channel_code += item.channel_list_code + ',';
            }
        });
        var para = {
            'channel_list_code': channel_code.substring(0,channel_code.length-1),
            'sale_code': sale_code
        }
        $resource('/api/as/tc/saleauth/updateChannelList', {}, {}).save(para, function (res) {
            if (res.errcode === 0) {
                toaster.success({title: '', body: '产品绑定渠道成功'});
                $modalInstance.close();
            } else {
                toaster.error({title: '', body: res.errmsg});
            }
        });
        
    }
    
    //取消
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

};