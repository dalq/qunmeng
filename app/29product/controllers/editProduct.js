
module.exports = function ($scope, $stateParams, $http, $q, productid, what, $modal, FileUploader, str2date, date2str, $resource, toaster, $modalInstance, auditing, $timeout) {

	var id = $stateParams.id || productid;

	//产品对象，保存着产品的基本信息
	$scope.saleobj = {
		'id': id || '',
	};

	$scope.util = {
		'what': what,
		'str2date': str2date,
		'date2str': date2str,
		'$modalInstance': $modalInstance,
		'auditing': auditing
	}
	//销售品功能列表
	$scope.funobj = {};

	//基本信息中需要提前弄好的信息。
	$scope.baseinfo = {
		'imageshow': {},
		'dateshow': {},
	};

	var uploader1 = $scope.uploader1 = new FileUploader({
		url: 'https://dlqt.juyouhx.com/Api/Api/ObjectToOss?topdir=aa&selfdir=bb'
	});
	uploader1.filters.push({
		name: 'imageFilter',
		fn: function (item /*{File|FileLikeObject}*/, options) {
			var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
			return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
		}
	});
	uploader1.onSuccessItem = function (fileItem, response, status, headers) {
		$scope.saleobj.top_pic = response.savename;
	};

	var uploader2 = $scope.uploader2 = new FileUploader({
		url: 'https://dlqt.juyouhx.com/Api/Api/ObjectToOss?topdir=aa&selfdir=bb'
	});
	uploader2.filters.push({
		name: 'imageFilter',
		fn: function (item /*{File|FileLikeObject}*/, options) {
			var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
			return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
		}
	});
	uploader2.onSuccessItem = function (fileItem, response, status, headers) {
		$scope.saleobj.logo = response.savename;
	};

	$scope.baseinfo.imageshow.uploader1 = uploader1;
	$scope.baseinfo.imageshow.uploader2 = uploader2;


	$scope.baseinfo.dateshow.periodstart = {
		'date': {
			'label': $scope.util.date2str(new Date()),
			'value': $scope.util.date2str(new Date()),
			'opened': false
		}
	};

	$scope.baseinfo.dateshow.periodend = {
		'date': {
			'label': $scope.util.date2str(new Date(parseInt($scope.baseinfo.dateshow.periodstart.date.value), 11, 31)),
			'value': $scope.util.date2str(new Date(parseInt($scope.baseinfo.dateshow.periodstart.date.value), 11, 31)),
			'opened': false
		}
	};

	$scope.baseinfo.dateshow.open = function ($event, item) {
		$event.preventDefault();
		$event.stopPropagation();
		item.opened = true;
	};

	//日历显示信息的顺序和样式
	$scope.showattrarr = [
		{
			key: 'market_price_display',
			position: 'left',
			//before: '<font color=red >市场价格: ¥</font>'
		},
		{
			key: 'guide_price_display',
			position: 'left',
			// before: '<font color=green >平台价格: ¥</font>'
		},
		{
			key: 'cost_price_display',
			position: 'left',
			// before: '<font color=red >成本价格: ¥</font>'
		},
		{
			key: 'purchase_price_display',
			position: 'left',
			// before: '<font color=green >采购价格: ¥</font>'
		},
		{
			key: 'current_stock_num_display',
			position: 'left',
			// before: '<font color=red >当前库存: ¥</font>'
		}
	];

	$scope.data = [];

	$scope.date = $scope.util.date2str(new Date());

	$scope.dateArray = {};

	$scope.findinfoList = function (str) {
		var para = {
			sale_code: $scope.saleobj.code
		}
		$resource('/api/ac/tc/ticketSaleDayPriceService/getDayPriceBySaleCode', {}, {}).save(para, function (res) {
			if (res.errcode === 0) {
				for (var index = 0; index < res.data.daylist.length; index++) {
					var date = res.data.daylist[index].date;
					var id = res.data.daylist[index].id;
					$scope.dateArray[date] = id;
					var element = {};
					element.d = res.data.daylist[index].date;
					if( res.data.daylist[index].state == '0'){
						element.market_price_display = '<font color=red >市场价格: ¥</font><font color=red >' + 
																res.data.daylist[index].market_price + '</font>';
						element.guide_price_display = '<font color=green >平台价格: ¥</font><font color=green >' + 
																res.data.daylist[index].guide_price + '</font>';
						element.cost_price_display = '<font color=red >成本价格: ¥</font><font color=red >' + 
																res.data.daylist[index].cost_price + '</font>';
						element.purchase_price_display = '<font color=green >采购价格: ¥</font><font color=green >' + 
																res.data.daylist[index].purchase_price + '</font>';
						element.current_stock_num_display = '<font color=red >当前库存: </font><font color=red >' + 
																res.data.daylist[index].current_stock_num + '</font>';
					} else if( res.data.daylist[index].state == '1') {
						element.market_price_display = '<font color=grey >市场价格: ¥</font><font color=grey >' + 
												res.data.daylist[index].market_price + '</font>';
						element.guide_price_display = '<font color=grey >平台价格: ¥</font><font color=grey >' + 
												res.data.daylist[index].guide_price + '</font>';
						element.cost_price_display = '<font color=grey >成本价格: ¥</font><font color=grey >' + 
												res.data.daylist[index].cost_price + '</font>';
						element.purchase_price_display = '<font color=grey >采购价格: ¥</font><font color=grey >' + 
												res.data.daylist[index].purchase_price + '</font>';
						element.current_stock_num_display = '<font color=grey >当前库存: </font><font color=grey >' + 
												res.data.daylist[index].current_stock_num + '</font>';
					}
					// element.market_price_display = res.data.daylist[index].market_price;
					// element.guide_price_display = res.data.daylist[index].guide_price;
					// element.cost_price_display =  res.data.daylist[index].cost_price;
					// element.purchase_price_display = res.data.daylist[index].purchase_price;
					// element.current_stock_num_display = res.data.daylist[index].current_stock_num;
					element.market_price = res.data.daylist[index].market_price;
					element.guide_price = res.data.daylist[index].guide_price;
					element.cost_price = res.data.daylist[index].cost_price;
					element.purchase_price = res.data.daylist[index].purchase_price;
					element.current_stock_num = res.data.daylist[index].current_stock_num;
					element.state = res.data.daylist[index].state;
					element.id = res.data.daylist[index].id;
					element.sale_code = res.data.daylist[index].sale_code;
					$scope.data[index] = element;
				}
			} else {
				toaster.warning({ title: '', body: res.errmsg });
			}
		});
	}

	//base页根据ID检索出sale信息后 根据sale_code 检索出价格日历信息
	$scope.$watch('saleobj', function (newValue, oldValue) {
		if ($scope.data.length) {
			return false;
		}
		$scope.findinfoList();
	}, true);

	$scope.options = {
		btn_flag: true,//是否显示按钮
		btn_left: '批量更改',
		btn_right: {
			label: '&nbsp;&nbsp; 上&nbsp;&nbsp;架&nbsp;&nbsp;/&nbsp;&nbsp;下&nbsp;&nbsp;架&nbsp;&nbsp;',
			style: 'btns',
			btns: [
				{
					label: '上架',
					click: function(obj, dataobj){
						var params = []
						for (var i = 0; i < obj.data.length; i++) {
							var element = obj.data[i];
							for(var index = 0; index < element.length; index++){
								var element2 = element[index];
								if (element2.selected == true) {
									params.push({id: dataobj[element2.d].id, sale_code: dataobj[element2.d].sale_code})
								}
							}
						}
						if (params.length < 1) {
							toaster.success({ title: '', body: '请选择日期' });
							return false;
						}
						$resource('/api/ac/tc/ticketSaleDayPriceService/updateDayPriceStateUp', {}, {}).save({daylist: params}, function (res) {
							if (res.errcode === 0) {
								toaster.success({ title: '', body: '操作成功' });
								$scope.findinfoList();
							} else {
								toaster.warning({ title: '', body: res.errmsg });
							}
						});
					}
				},
				{
					label: '下架',
					click: function(obj, dataobj){
						var params = []
						for (var i = 0; i < obj.data.length; i++) {
							var element = obj.data[i];
							for(var index = 0; index < element.length; index++){
								var element2 = element[index];
								if (element2.selected == true) {
									params.push({id: dataobj[element2.d].id, sale_code: dataobj[element2.d].sale_code})
								}
							}
						}
						if (params.length < 1) {
							toaster.success({ title: '', body: '请选择日期' });
							return false;
						}
						$resource('/api/ac/tc/ticketSaleDayPriceService/updateDayPriceStateDown', {}, {}).save({daylist: params}, function (res) {
							if (res.errcode === 0) {
								toaster.success({ title: '', body: '操作成功' });
								$scope.findinfoList();
							} else {
								toaster.warning({ title: '', body: res.errmsg });
							}
						});
					}
				},
			]
		},
		leftClick: function () {//左按钮函数
			var modalInstance = $modal.open({
				template: require('../views/updatePriceCalendar.html'),
				controller: 'updatePriceCalendar',
				size: 'lg',
				resolve: {
					items: function () {
						return {
							dateArray: $scope.dateArray,
							sale_code: $scope.saleobj.code
						};
					}
				}
			});
			modalInstance.opened.then(function () {// 模态窗口打开之后执行的函数  
			});
			modalInstance.result.then(function (showResult) {
				$scope.findinfoList();
			}, function (reason) {
				$scope.findinfoList();
				// click，点击取消，则会暑促cancel  
				// $log.info('Modal dismissed at: ' + new Date());
			});
		},
		rightClick: function(){//右按钮函数
		},
		click: function (item) {//每天的点击事件
			if (item.d == '0' || item.d == 0) {
				toaster.info({ title: '', body: '请选择当月日期' });
				return false;
			}
			var today = date2str(new Date());
			if (item.d < today) {
				toaster.info({ title: item.d, body: '出游日期不能小于当天日期' });
				return false;
			}

			var modalInstance = $modal.open({
				template: require('../views/customPriceCalendar.html'),
				controller: 'customPriceCalendar',
				size: 'lg',
				resolve: {
					items: function () {
						return {
							data: item.data,
							date: item.d,
							dateArray: $scope.dateArray,
							sale_code: $scope.saleobj.code
						};
					}
				}
			});
			modalInstance.opened.then(function () {// 模态窗口打开之后执行的函数  
			});
			modalInstance.result.then(function (result) {
				$scope.findinfoList(result.date);
				$scope.date = item.d;
			}, function (reason) {
				$scope.findinfoList();
				// click，点击取消，则会暑促cancel  
				// $log.info('Modal dismissed at: ' + new Date());
			});

		},
		state: 'state',
		checkbox: true
	}


};