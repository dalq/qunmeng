module.exports = function ($scope, $state, $resource, $modal, $stateParams, toaster){

	//查询优惠详情列表
	$scope.search = function (){
		var para = {
			'groupby': $scope.searchform.type
		}
		$resource('/api/as/tc/saleauth/findFaultList', {}, {}).save(para, function(res){
			if(res.errcode === 0){
				$scope.list = res.data;
			}else{
				toaster.error({title: '', body: res.errmsg});
			}
		});
	}

	init();
    //初始化列表
    function init(){
		$scope.searchform = {};
		$scope.type = [
			{'name': '不分组查询', 'value': undefined},
			{'name': '分组查询', 'value': '1'}
		];
		$scope.search();
	};

};