
module.exports = function ($scope, $stateParams, $http, $q, productid, what, FileUploader, str2date, date2str, $resource, toaster, $modalInstance, auditing, $timeout) {
	
	console.log($stateParams.params);

	console.log(auditing);


	$scope.saleobj = {
		//'templete_code': templete.templete_code,
		'id':'',
		//'name_flag': templete.templete_lock_data_json.name,
		//'sms_type_flag': templete.templete_lock_data_json.sms_type
	};

	$scope.util = {
		'what': what,
		'str2date': str2date,
		'date2str': date2str,
		'$modalInstance': $modalInstance,
		'auditing': auditing
	}

	//销售品功能列表
	$scope.funobj = {};

	//基本信息中需要提前弄好的信息。
	$scope.baseinfo = {
		'imageshow': {},
		'dateshow': {
			'periodstart': {
				'date': {
					'label': $scope.util.date2str(new Date()),
					'value': $scope.util.date2str(new Date()),
					'opened': false
				}
			},
			'periodend': {
				'date': {
					'label': $scope.util.date2str(new Date()),
					'value': $scope.util.date2str(new Date()),
					'opened': false
				}
			},
			'open': function ($event, item) {
				$event.preventDefault();
				$event.stopPropagation();
				item.opened = true;
			}
		},
	};



	$resource('/api/as/tc/ticketsaletemplete/getTempleteInfo', {}, {})
    .save({'templete_code' : $stateParams.params}, function (res) {

    	console.log(res);

    	if (res.errcode !== 0) {
            toaster.error({ title: "提示", body: res.errmsg });
            return;
        }

    	var templete = res.data;


    	templete.templete_lock_data_json = angular.fromJson(templete.templete_lock_data_json);
    	$scope.saleobj['templete_code'] = templete.templete_code;
    	$scope.saleobj['name_flag'] = templete.templete_lock_data_json.name;
    	$scope.saleobj['sms_type_flag'] = templete.templete_lock_data_json.sms_type;


    	// //产品对象，保存着产品的基本信息
	
	

	var uploader1 = $scope.uploader1 = new FileUploader({
		url: 'https://dlqt.juyouhx.com/Api/Api/ObjectToOss?topdir=aa&selfdir=bb'
	});
	uploader1.filters.push({
		name: 'imageFilter',
		fn: function (item /*{File|FileLikeObject}*/, options) {
			var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
			return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
		}
	});
	uploader1.onSuccessItem = function (fileItem, response, status, headers) {
		$scope.saleobj.top_pic = response.savename;
	};

	var uploader2 = $scope.uploader2 = new FileUploader({
		url: 'https://dlqt.juyouhx.com/Api/Api/ObjectToOss?topdir=aa&selfdir=bb'
	});
	uploader2.filters.push({
		name: 'imageFilter',
		fn: function (item /*{File|FileLikeObject}*/, options) {
			var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
			return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
		}
	});
	uploader2.onSuccessItem = function (fileItem, response, status, headers) {
		$scope.saleobj.logo = response.savename;
	};

	$scope.baseinfo.imageshow.uploader1 = uploader1;
	$scope.baseinfo.imageshow.uploader2 = uploader2;


	// $scope.baseinfo.dateshow.periodstart = {
	// 	'date': new Date(),
	// };
	// $scope.baseinfo.dateshow.periodstart = {
	// 	'date': {
	// 		'label': $scope.util.date2str(new Date()),
	// 		'value': $scope.util.date2str(new Date()),
	// 		'opened': false
	// 	}
	// };

	// $scope.baseinfo.dateshow.periodend = {
	// 	'date': {
	// 		'label': $scope.util.date2str(new Date(parseInt($scope.baseinfo.dateshow.periodstart.date.value), 11, 31)),
	// 		'value': $scope.util.date2str(new Date(parseInt($scope.baseinfo.dateshow.periodstart.date.value), 11, 31)),
	// 		'opened': false
	// 	}
	// };

	// $scope.baseinfo.dateshow.open = function ($event, item) {
	// 	$event.preventDefault();
	// 	$event.stopPropagation();
	// 	item.opened = true;
	// }

	// $scope.baseinfo.dateshow.periodend = {
	// 	'date': new Date($scope.baseinfo.dateshow.periodstart.date.getFullYear(), 11, 31),
	// };

	// $scope.baseinfo.dateshow.open = function (obj) {
	// 	obj.opened = true;
	// };


	$scope.$watch('saleobj', function (newValue) {

		for (var key in templete.templete_lock_data_json) {
			(function (arg) {
				// $timeout(function () {
					var el = $("[ng-model='saleobj." + arg + "']").parent().parent().hasClass('form-group') ? 
							$("[ng-model='saleobj." + arg + "']").parent().parent() : 
							$("[ng-model='saleobj." + arg + "']").parent().parent().parent();
					$(el).hide();
				// }, 800);
			})(key);//调用时参数   
		}
		if(templete.templete_lock_data_json.hasOwnProperty('sms_type') && 
				(templete.templete_lock_data_json.sms_type == '0' || 
					(templete.templete_lock_data_json.hasOwnProperty('sms_template_id') &&
							templete.templete_lock_data_json.hasOwnProperty('sms_diy')))){
			$(".message").hide();
		}
		if (templete.templete_lock_data_json.hasOwnProperty('periodstart')) {
			$(".periodstart").hide();
		}
		if (templete.templete_lock_data_json.hasOwnProperty('periodend')) {
			$(".periodend").hide();
		}
	}, true);

        
        // $scope.data = [];
        // $scope.data = angular.copy(res.data);
        // for (var index = 0; index < $scope.data.length; index++) {
        //     var element = $scope.data[index];
        //     element.templete_lock_data_json = eval('(' + element.templete_lock_data_json + ')');
        //     element.url = 'app.newproduct';
        // }
        // $scope.functionArray = $scope.data;

    });

	// $scope.params = eval('(' + $stateParams.params + ')')

	


};