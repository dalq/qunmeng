module.exports = function ($scope, $modalInstance, $resource, $modal, item, toaster){

    init();
    //初始化操作
    function init(){
        $scope.office = item;
        $scope.sel_list = [];
        $scope.search = {};
		$resource('/api/ac/tc/ticketSaleAuthService/findDownCompanyCode', {}, {}).save({'sale_code': item.code}, function(res){
			if(res.errcode === 0){
                $scope.office_list = res.data;
			}else{
				toaster.error({title: '', body: res.errmsg});
			}
		});
	}

    //添加协议价信息
    $scope.add = function () {
        for(var i = 0; i < $scope.sel_list.length; i++){
            if($scope.search.office.code == $scope.sel_list[i].company_code){
                toaster.error({title: '', body: '不能重复添加'});
                return;
            }
        }
        var temp = {
            'company_name': $scope.search.office.name,
            'company_code': $scope.search.office.code,
            'company_sys_area': $scope.search.office.sys_area,
            'floating_price': 0,
            'sale_code': item.code
        };
        $scope.sel_list.push(temp);
    }

    //删除已选机构
    $scope.remove = function (index) {
        $scope.sel_list.splice(index,1)
    };

    //确认
    $scope.ok = function () {
        $resource('/api/ac/tc/ticketSaleAuthService/save', {}, {}).save({'company_list': $scope.sel_list}, function (res) {
            if (res.errcode === 0) {
                $modalInstance.close($scope.sel_list.length);
            } else {
                toaster.error({title: '', body: res.errmsg});
            }
        });
    }
    
    //取消
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

};