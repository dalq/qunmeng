module.exports = function ($scope, $state, $resource, $modal, str2date, date2str,
    saleup, saledown, saleupdate, talist, sellerListno, toaster, getDate

) {
    var ticketstate_arr=[
        {
        'label':'--全部--',
        'value':'',
       },
       {
        'label':'待审核',
        'value':'5',
       }, 
       {
        'label':'审核通过',
        'value':'1',
       },
        {
        'label':'审核不通过',
        'value':'2',
       },
      
   ];
   $scope.tstate=ticketstate_arr;

    $scope.searchform = {};

    //有效区间
     $scope.section = {};
    // $scope.section.start = {};
    // // $scope.section.start.date = new Date();

    // $scope.section.end = {};
    // // $scope.section.end.date = new Date();

    // $scope.open = function (obj) {
    //     obj.opened = true;
    // };
           $scope.date = {
                // 'lable': date2str2(new Date()),
                //'lable': new Date(),
                //'value': date2str(new Date()),
                'opened': false
            }


             $scope.dateOpen = function ($event, item) {
                $event.preventDefault();
                $event.stopPropagation();
                item.opened = true;
            };

            $scope.date1 = {
                 // 'lable': date2str2(new Date()),
                //'value': date2str(new Date()),
                'opened': false
            }


             $scope.dateOpen1 = function ($event, item) {
                $event.preventDefault();
                $event.stopPropagation();
                item.opened = true;
            };
             function date2str2(d) {
                if (d === undefined) {
                    return "";
                }
                var month = (d.getMonth() + 1).toString();
                var day = d.getDate().toString();
                if (month.length == 1) month = '0' + month;
                if (day.length == 1) day = '0' + day;
                return d.getFullYear() + "-" + month + "-" + day;
            }

    $scope.create = function () {
        $state.go('app.newproduct');
    };

    /* 分页
     * ========================================= */
    $scope.maxSize = 5;            //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10;         //每页显示几条


    $scope.load = function () {
       
       if($scope.date.lable){
                    $scope.tour_date=date2str2($scope.date.lable);;
                }else{
                    $scope.tour_date=''
                }
                if($scope.date1.lable){
                    $scope.tour_date_two=date2str2($scope.date1.lable);;
                }else{
                    $scope.tour_date_two=''
                }
                if($scope.date.lable!=null && $scope.date1.lable!=null){
                    if($scope.tour_date>$scope.tour_date_two){
                        toaster.error({ title: "", body: '开始日期不能大于结束日期' })
                        return;
                    }
                }
                if($scope.date.lable){
                    if(!$scope.date1.lable){
                        toaster.error({ title: "", body: '开始和结束日期必须同时输入进行搜索' })
                        return;
                    }
                }
                if($scope.date1.lable){
                    if(!$scope.date.lable){
                        toaster.error({ title: "", body: '开始和结束日期必须同时输入进行搜索' })
                        return;
                    }
                }
                if( $scope.tour_date === ''){
                    var para = {
                        pageNo: $scope.bigCurrentPage,
                        pageSize: $scope.itemsPerPage,
                        start_time:'',
                        end_time:'',
                    };

                }else{
                    var para = {
                        pageNo: $scope.bigCurrentPage,
                        pageSize: $scope.itemsPerPage,
                        start_time: $scope.tour_date + ' 00:00:00',
                        end_time: $scope.tour_date_two + ' 23:59:59',
                    };

                }
     
        para = angular.extend($scope.searchform, para);

        $resource('/api/ac/tc/ticketOrderBackService/getApplyBackTicketList', {}, {})
            .save(para, function (res) {

                if (res.errcode !== 0) {
                    toaster.error({ title: "提示", body: res.errmsg });
                    return;
                }

                $scope.objs = res.data.results;
                for (var index = 0; index < $scope.objs.length; index++) {
                    switch ($scope.objs[index].ticket_state) {
                        case '1':
                            $scope.objs[index].ticket_state_name = '审核通过';
                            break;
                        case '2':
                            $scope.objs[index].ticket_state_name = '审核不通过';
                            break;
                        case '5':
                            $scope.objs[index].ticket_state_name = '待审核';
                            break;
                    
                        default:
                            $scope.objs[index].ticket_state_name = '';
                            break;
                    }
                }

                $scope.bigTotalItems = res.data.totalRecord;

            });

    };
    $scope.load();

    $scope.info = function (obj) {

        //$state.go('app.editsale', {'id' : id});

        var modalInstance = $modal.open({
            template: require('../views/backTickketApplyDetails.html'),
            controller: 'backTickketApplyDetails',
            url: '/product/backTickketApplyDetails',
            size: 'lg',
            resolve: {
                'item': function () {
                    return obj;
                }
            }
        });

        modalInstance.opened.then(function () {// 模态窗口打开之后执行的函数  
            // $scope.load();
        });
        modalInstance.result.then(function (showResult) {
            $scope.load();
        }, function (reason) {
            $scope.load();
            // // click，点击取消，则会暑促cancel  
            // $log.info('Modal dismissed at: ' + new Date());
        });
    };

};