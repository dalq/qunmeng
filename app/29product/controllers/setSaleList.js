module.exports = function ($scope, $resource, $state, $stateParams, $modal, toaster){

    //加载全部销售品列表
	$scope.loadSaleList = function (pageNo){
        var para = {
            'channel_list_code': $stateParams.code,
            'pageNo': pageNo,
			'pageSize': $scope.itemsPerPage
        }
        $resource('/api/as/tc/salechannel/findSaleList', {}, {}).save(para, function (res) {
            if (res.errcode === 0) {
                $scope.sale_list = res.data.results;
                $scope.totalItems = res.data.totalRecord;
            } else {
                toaster.error({title: '', body: res.errmsg});
            }
        });
    }
    
    init();
    function init(){
        $scope.currentPage = 1;		//销售品列表页码
        $scope.itemsPerPage = 10;	//每页显示几条
        $scope.save_list = [];
        $scope.obj = $stateParams;
        $scope.loadSaleList(1);
    }
    
	//添加销售品分类
	$scope.addSaleType = function() {
		var modalInstance = $modal.open({
			template: require('../views/addSaleList.html'),
			controller: 'addSaleList',
			size: 'lg',
			resolve: {
				saleCode : function () {
					return $stateParams.code;
				}
			}
		});
		//关闭模态框刷新页面
		modalInstance.result.then(function() {
			$scope.loadSaleList(1);
		});
	}

    //删除销售品
    $scope.delete = function (item){
        if(confirm('确认删除此产品吗？') == true){
            var para = {
                'sale_code': item.code,
                'channel_list_code': $stateParams.code
            }
            $resource('/api/as/tc/salechannel/deleteChannel', {}, {}).save(para, function (res) {
                if (res.errcode === 0) {
                    toaster.success({title: '', body: '删除成功'});
                    $scope.loadSaleList(1);
                } else {
                    toaster.error({title: '', body: res.errmsg});
                }
            });
        }
    }

    //回退到渠道列表
    $scope.back = function (){
        $state.go('app.myChannel', {})
    }


};