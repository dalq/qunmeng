module.exports = function($scope, $state, $stateParams, $modalInstance, $resource,$modal,FileUploader,toaster,code,date2str,str2date){
    var sale_code = code;
    $scope.vm = {
        'date' : '',
        'options' : {
            // format: "YYYY-MM-DD",
            locale : 'zh-cn',
            showClear: true                        
            // clearBtn:true
        }
    }
    $scope.vm1 = {
        'date' : '',
        'options' : {
            // format: "YYYY-MM-DD",
            locale : 'zh-cn',
            showClear: true                        
            
        }
    }
    $scope.searchform = {};
    /* 分页
     * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10         //每页显示几条

    function showClear() {
    }

    $scope.getlist = function () {
        if ( $scope.vm.date === null) {
            $scope.vm.date = '';
        }
        if (typeof $scope.vm.date._d === 'string') {
            $scope.vm.date._d = $scope.vm.date._d;
        } else {
            $scope.vm.date._d = time2str($scope.vm.date._d);
        }

        if ( $scope.vm1.date === null) {
            $scope.vm1.date = '';
        }
        if (typeof $scope.vm1.date._d === 'string') {
            $scope.vm1.date._d = $scope.vm1.date._d;
        } else {
            $scope.vm1.date._d = time2str($scope.vm1.date._d);
        }
        
        var para = {
            pageNo:$scope.bigCurrentPage, 
            pageSize:$scope.itemsPerPage,
            sale_code:sale_code,
            create_time_start : ($scope.vm.date._d),
            create_time_end : ($scope.vm1.date._d)

        };
        para = angular.extend($scope.searchform,para);
        if(para.create_time_start != undefined && para.create_time_end != undefined){
            $resource('/api/as/tc/salettype/orderList', {}, {}).
            save(para,function (res) {
                if (res.errcode != 0) {
                    toaster.error({title:"",body:res.errmsg});
                    return;
                }
                $scope.objs = res.data.results;
                $scope.bigTotalItems = res.data.totalRecord;
                // $scope.objs.forEach(function(element) {
                //     element.isSelected = element.state=='1'?true:false;
                // }, this);
            }); 
        } else {
            toaster.warning({title:"",body:"起始日期和结束日期需要同时选中"});
            
        }
        
              
        
        
    };
    $scope.getlist();

    $scope.updateticket = function(code){
        if(confirm('亲~~~确定要更新吗?')){
            $resource('/api/as/tc/salettype/updateStateByCode', {}, {}).
            save({'code' : code},function (res) {
                if (res.errcode != 0) {
                    toaster.error({title:"",body:res.errmsg});
                    return;
                }
                toaster.success({title:"",body:"更新成功"});
                
                
            });   
        }

    }

    $scope.cancel = function(){
        $modalInstance.close();
    }

    $scope.oneupdate = function(){
        if(confirm('亲~~~确定要全部更新吗?')){
            $resource('/api/as/tc/salettype/updateTicketState', {}, {}).
            save({'sale_code' : sale_code},function (res) {
                if (res.errcode != 0) {
                    toaster.error({title:"",body:res.errmsg});
                    return;
                }
                toaster.success({title:"",body:"全部更新成功"});
                
                
            });   
        }
    }


    function time2str(objDate) {
        if(angular.isDate(objDate))
        {
            var y = objDate.getFullYear();
            var m = objDate.getMonth();
            var d = objDate.getDate();
            var h = objDate.getHours();
            var mt = objDate.getMinutes();
            // var s = objDate.getSeconds();
            return y + '-' + (m + 1) + '-' + d + ' ' + h + ':' + mt;
        }
        else
        {
            return '错误格式';
        }
    }

}