 /**
 * 子模块路由
 * dlq
 */

var router = function($urlRouterProvider, $stateProvider){

 	$stateProvider
    //价格日历
    .state('app.price_calendar', {
        url: "/directive/price_calendar.html",
        views: {
            'main@' : {
                template : require('./views/test.html'),
                controller : 'pctest',
            }
        },
        // resolve:{
        //     tableconfig : function(tableservice){
        //         return tableservice.tableconfig();
        //     },
        // }
    })


    //列表demo
    .state('app.table_demo', {
        url: "/demo/table.html",
        views: {
            'main@' : {
                template : require('./views/tabledemo.html'),
                controller : 'tabledemo',
            }
        },
        // resolve:{
        //     tableconfig : function(tableservice){
        //         return tableservice.tableconfig();
        //     },
        // }
    })

   
	;

};

module.exports = router;