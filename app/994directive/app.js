var App = angular.module('directive', []);

App.config(require('./router'));


App.directive('priceCalendar',require('./directives/priceCalendar'));
App.directive('priceCalendar1',require('./directives/newPriceCalendar'));
App.directive('intelligentButton',require('./directives/intelligentButton'));
App.controller('pctest',require('./controllers/pctest'));
App.controller('tabledemo',require('./controllers/tabledemo'));

module.exports = App;