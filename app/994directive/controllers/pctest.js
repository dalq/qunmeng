module.exports = function($scope, $modal, $state, $timeout){

	//日历要显示的数据
	$scope.data = [
		{'d' : '20170303', 'market_price' : '132', 'count' : '80'},
		{'d' : '20170305', 'market_price' : '23', 'count' : '100'},
		{'d' : '20170308', 'market_price' : '42', 'count' : '32'},
	];

	//日历显示信息的顺序和样式
	$scope.showattrarr = [
		{
			'key' : 'market_price',
			'position' : 'right',
			'before' : '¥'
		},
		{
			'key' : 'count',
			'position' : 'left',
			'before' : '库存',
			'after' : '个',
		}
	];

	//每一天的点击事件
	$scope.clickday = function(item){

	};

	$scope.refresh = function(){

		
	};



	$scope.image = function(){
		var para = $state.get('app.imageupload');
		//设置上传文件夹，以自己业务命名
		angular.extend(para, {
			resolve : {  
	            'dir' : function(){
	                return 't1';
	            }
	        } 
		})

        var modalInstance = $modal.open(para);
        modalInstance.opened.then(function() {// 模态窗口打开之后执行的函数  
        });  
        modalInstance.result.then(function(result) {  
            $scope.imagearr = JSON.stringify(result);
            // form.result['templete_lock_data_json'] = JSON.stringify(result.lock);
            // form.result['templete_check_data_json'] = result.unlock;

        }, function(reason) {  
            // click，点击取消，则会暑促cancel  
            $log.info('Modal dismissed at: ' + new Date());  
        }); 
	};




	var vm = this;

    $timeout(function() { // simulating a REST API Call that takes 500 ms
        vm.date = moment('2015-11-20T22:10Z');
    }, 500);

    vm.options = {format: 'YYYY/MM/DD HH:mm', showClear: true};

    vm.getTime = function () {
        alert('Selected time is:' + vm.date.format('YYYY/MM/DD HH:mm'));
    };

    vm.addTime = function (val, selector) {
        vm.date = moment(vm.date.add(val, selector));
    };

    vm.clearTime = function () {
        vm.date = undefined;
    };

};
