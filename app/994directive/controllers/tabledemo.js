module.exports = function($scope, $modal, $state, $timeout, $resource){

	$scope.start = {
		'date' : '',
		'opened' : false,
	};

	$scope.end = {
		'date' : '',
		'opened' : false,
	};

	$scope.open = function(obj) {
		obj.opened = true;
	}


	$scope.myKeyup = function() {
	}

	$scope.searchform = {};
	/* 分页
     * ========================================= */
	$scope.maxSize = 5;            //最多显示多少个按钮
	$scope.bigCurrentPage = 1;      //当前页码
	$scope.itemsPerPage = 10;         //每页显示几条

	$scope.load = function () {

		var para = {
			pageNo: $scope.bigCurrentPage,
			pageSize: $scope.itemsPerPage,
			'type': 'J',
		};


		para = angular.extend($scope.searchform, para);

		$resource('/api/as/tc/placeview/list', {}, {}).save(para, function (res) {

			if (res.errcode !== 0) {
				alert("数据获取失败");
				return;
			}

			$scope.objs = res.data.results;
			$scope.bigTotalItems = res.data.totalRecord;

		});

	};
	$scope.load();

};
