module.exports = function(){

	/*

		var data = [
			{'d' : '20180503', 'market_price' : '132', 'count' : '80'},
			{'d' : '20170505', 'market_price' : '23', 'count' : '100'},
			{'d' : '20170508', 'market_price' : '42', 'count' : '32'},
		];

		var dataobj = {};
		for(var i = 0; i < data.length; i++){
			var tmp = data[i];
			dataobj[tmp.d] = tmp;
		}

		//每个日历格里需要显示的数据顺序和样式。
		var showattrarr = [
			{
				'key' : 'market_price',
				'position' : 'right',
				'before' : '¥'
			},
			{
				'key' : 'count',
				'position' : 'left',
				'before' : '库存',
				'after' : '个',
			}
		];

	*/
	function makedata(year, month, dataobj, showattrarr){

	    var montharray = [31,28,31,30,31,30,31,31,30,31,30,31];
		
		var obj = {
			'y' : year,
			'm' : month,
			'data' : [],
		};

		if((obj.m === 1) && (obj.y % 4 === 0) 
		&&((obj.y % 100 !== 0) || (obj.y % 400 === 0)))
        {
            montharray[1] = 29;
        }

        //1号
        var firstdate = new Date(obj.y, obj.m ,1);
        //最后一号
        var lastdate = new Date(obj.y, obj.m, montharray[obj.m]);

        //1号星期几
        var fxingqi = firstdate.getDay();
        //最后一号星期几
        var lxingqi = lastdate.getDay();

        var dataarr = [];
        //日期之前的空位
        for(var f = 0; f < fxingqi; f++)
        {
            var dayobj = {
                'label' : '',
                'd' : '0',
            };
            dataarr.push(dayobj);
        }
        //日期
        for(var j = 0; j < montharray[obj.m]; j++)
        {
            var dayobj = {
                'label' : j + 1,
                'd' : obj.y + '',
                'labelarr' : [],	//具体显示的信息
            };

            if(obj.m < 9){
            	dayobj.d += '0' + (obj.m + 1);
            }else{
            	dayobj.d += obj.m + 1;
            }

            if(j < 9){
            	dayobj.d += '0' + (j+1);
            }else{
            	dayobj.d += (j+1);
            }

            //有数据要显示。
            var show = dataobj[dayobj.d];
            if(angular.isDefined(dataobj[dayobj.d])){
            	for(var x = 0; x < showattrarr.length; x++){
            		var xx = showattrarr[x];
            		var ooo = {
	            		'show' : (xx.before || '') + show[xx.key] + (xx.after || ''),
	            		'position' : xx.position
	            	};
            		dayobj.labelarr.push(ooo);
            		dayobj['data'] = show;
            	}
            }
            dataarr.push(dayobj);
        }
        //日期之后的空位
        for(var l = lxingqi; l < 6; l++)
        {
            var dayobj = {
                'label' : '',
                'd' : '0',
            };
            dataarr.push(dayobj);
        }

        for(var i = 0; i < dataarr.length; i++){
        	//每七个重新组装一个数组。
        	var x = i % 7;
        	if(x === 0){
        		obj.data.push(new Array());
        	}
        	obj.data[obj.data.length - 1].push(dataarr[i]);
        }
        return  obj;
	}


	//step 负数，之前几个月，-1：表示之前一个月
	//     正数，之后几个月，1 ：表示之后一个月
	function getYM(y, m, step){
		var yy = y;
		var mm = m;

		if(mm + step < 0){
			yy -= 1;
			mm = 12 + mm + step;
		}else if(mm + step > 11){
			yy += 1;
			mm = mm + step - 12;
		}else{
			mm += step;
		}

		return {
			'y' : yy,
			'm' : mm,
		}
	}

	return {

		restrict : 'AE',
		template : require('../views/priceCalendar.html'),
		replace:true,
		scope:{
			'data' : '=',
			'showattrarr' : '=',
			'clickday' : '=',
			'refresh' : '=',
		},
		link : function(scope, elements, attrs){



			var dataobj = {};
			var showattrarr = [];
			scope.refresh = function(){
				dataobj = {};
				if(angular.isDefined(scope.data) && angular.isArray(scope.data)){
					for(var i = 0; i < scope.data.length; i++){
						var tmp = scope.data[i];
						dataobj[tmp.d] = tmp;
					}
				}

				showattrarr = [];
				if(angular.isDefined(scope.showattrarr) && angular.isArray(scope.showattrarr)){
					showattrarr = scope.showattrarr;
				}

				scope.weekarr = ["日", "一", "二", "三", "四", "五", "六"];

				var obj = {};
				var date = new Date();
				var year = date.getFullYear();
		    	var month = date.getMonth();

				scope.obj = makedata(year, month, dataobj, showattrarr);
			};
			scope.refresh();

			scope.pre = function(){
				var dd = getYM(scope.obj.y, scope.obj.m, -1);
				scope.obj = makedata(dd.y, dd.m, dataobj, showattrarr);
			};

			scope.back = function(){
				var dd = getYM(scope.obj.y, scope.obj.m, 1);
				scope.obj = makedata(dd.y, dd.m, dataobj, showattrarr);
			};
		}

	};

};

