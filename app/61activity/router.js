/**
* 子模块路由
* ml
*/

var router = function ($urlRouterProvider, $stateProvider) {

	$stateProvider

		//新建/编辑活动
		.state('app.activityEdit', {
			url: "/viewSystem/activityEdit",
			views: {
				'main@': {
					template: require('./views/activityEdit.html'),
					controller: 'activityEdit'
				}
			},
			resolve: {
				is_new: function(){
					return true
				},
				item: function(){
					return {}
				},
				$modalInstance: function(){
					return undefined
				}
			}
		})

		//活动列表
		.state('app.activityList', {
			url: "/viewSystem/activityList",
			views: {
				'main@': {
					template: require('./views/activityList.html'),
					controller: 'activityList'
				}
			},
			resolve: {

			}
		})

		//机构列表
		.state('app.activityCompany', {
			url: "/viewSystem/activityCompany",
			views: {
				'main@': {
					template: require('./views/company.html'),
					controller: 'company'
				}
			},
			resolve: {

			}
		})

		//机构列表
		.state('app.viewUserList', {
			url: "/activitySystem/viewUserList",
			views: {
				'main@': {
					template: require('./views/viewUserList.html'),
					controller: 'viewUserList'
				}
			},
			resolve: {

			}
		})

};

module.exports = router;