/**
 * 景区系统入口
 * ml
 */
var App = angular.module('activitySystem', []);

App.config(require('./router'));

//service
App.factory('activitySystemService', require('./service'));

//Controllers
App.controller('activityList', require('./controllers/activityList'));
App.controller('activityEdit', require('./controllers/activityEdit'));
App.controller('company', require('./controllers/company'));
App.controller('activityRule', require('./controllers/activityRule'));
App.controller('userList', require('./controllers/userList'));
App.controller('viewUserList', require('./controllers/viewUserList'));

//directive
// App.directive('viewCell', require('./directives/viewCell'));

module.exports = App;