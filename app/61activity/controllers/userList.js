module.exports = function ($scope, $resource, $modalInstance, item, toaster) {

    Date.prototype.format = function(format) {
        var date = {
               "M+": this.getMonth() + 1,
               "d+": this.getDate(),
               "h+": this.getHours(),
               "m+": this.getMinutes(),
               "s+": this.getSeconds(),
               "q+": Math.floor((this.getMonth() + 3) / 3),
               "S+": this.getMilliseconds()
        };
        if (/(y+)/i.test(format)) {
               format = format.replace(RegExp.$1, (this.getFullYear() + '').substr(4 - RegExp.$1.length));
        }
        for (var k in date) {
               if (new RegExp("(" + k + ")").test(format)) {
                      format = format.replace(RegExp.$1, RegExp.$1.length == 1
                             ? date[k] : ("00" + date[k]).substr(("" + date[k]).length));
               }
        }
        return format;
 }

    $scope.currentPage = 1;		//当前页码
    $scope.itemsPerPage = 10;	//每页显示几条
    $scope.load = function(){
        let para = {
			'pageNo': $scope.currentPage,
			'pageSize': $scope.itemsPerPage
        }
        Object.assign(item, para);
        //查找可以被当做赠品的产品
        $resource('/api/as/puc/subsidynoallow/getUserList', {}, {}).save(item, function (res) {
            if (res.errcode === 0) {
                $scope.userList = res.data.results;
                // for (let index = 0; index < $scope.userList.length; index++) {
                //     const element = $scope.userList[index];
                //     element.create_time = new Date(element.create_time).format('yyyy-MM-dd hh:mm:ss');
                // }
				$scope.totalItems = res.data.totalRecord;
            } else {
                toaster.error({ title: '', body: res.errmsg });
            }
        });
    }    
    $scope.load();

    //添加景区产品
    $scope.ok = function () {
        if (!$scope.obj.subsidy_id || !$scope.obj.subsidy_name) {
            toaster.warning({ title: '', body: '补贴信息不完整' });
            return false;
        }
        $resource($scope.status_info.url, {}, {}).save($scope.obj, function (res) {
            if (res.errcode === 0) {
                toaster.success({ title: '', body: $scope.status_info.msg });
                $scope.status_info.url = '/api/as/puc/subsidy/update';
                $scope.status_info.msg = '修改补贴信息成功';
                $modalInstance.close();
            } else {
                toaster.error({ title: '', body: res.errmsg });
            }
        });
    }

    $scope.canel = function () {
        $modalInstance.close();
    }

};