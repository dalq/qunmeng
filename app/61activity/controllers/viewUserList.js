module.exports = function ($scope, $resource, toaster) {

    $scope.currentPage = 1;		//当前页码
    $scope.itemsPerPage = 10;	//每页显示几条
    $scope.load = function () {
        let para = {
            'pageNo': $scope.currentPage,
            'pageSize': $scope.itemsPerPage
        }
        //查找可以被当做赠品的产品
        $resource('/api/as/tc/place/findUserList', {}, {}).save(para, function (res) {
            if (res.errcode === 0) {
                $scope.userList = res.data.results;
                for (let index = 0; index < $scope.userList.length; index++) {
                    const element = $scope.userList[index];
                    element.create_time = new Date(element.create_time).format('yyyy-MM-dd h:m:s');
                }
                $scope.totalItems = res.data.totalRecord;
            } else {
                toaster.error({ title: '', body: res.errmsg });
            }
        });
    }
    $scope.load();

    $scope.isbig = false;
    $scope.big = function () {
        console.log('big')
        $scope.isbig = true;
    }
    $scope.small = function () {
        console.log('small')
        $scope.isbig = false;
    }

};