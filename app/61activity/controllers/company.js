module.exports = function ($scope, $state, $resource, $modal, toaster, item) {

    //机构列表
    $scope.total = 0;
    $scope.num = 0;
    $scope.loadlist = function () {
        $resource('/api/as/puc/subsidycompany/list', {}, {}).save(item, function (res) {
            if (res.errcode === 0) {
                $scope.companyList = res.data;
                for (var index = 0; index < $scope.companyList.length; index++) {
                    var element = $scope.companyList[index];
                    element.state = true;
                    element.is_new = false;
                }
            } else {
                toaster.error({ title: '', body: res.errmsg });
            }
        });
    }
    $scope.loadlist();

    $scope.add = function () {
        $scope.companyList.push({
            subsidy_id: item.subsidy_id,
            subsidy_company_code: "",
            subsidy_company_total: 0,
            subsidy_company_num: 0,
            subsidy_company_state: '1',
            subsidy_company_num_once: 0,
            subsidy_company_nums_max: 0,
            subsidy_conpany_unit: item.subsidy_unit,
            state: false,
            is_new: true
        })
    }

    $scope.edit = function (index) {
        $scope.companyList[index].state = false;
    }

    $scope.save = function (obj) {
        $scope.total = 0;
        $scope.num = 0;
        for (var index = 0; index < $scope.companyList.length; index++) {
            var element = $scope.companyList[index];
            $scope.total += (element.subsidy_company_total || 0);
            $scope.num += (element.subsidy_company_num | 0);
        }
        if ($scope.total > item.subsidy_total) {
            toaster.error({ title: '', body: '机构总量大于本次活动总量' });
            return false;
        }
        if ($scope.num > item.subsidy_num) {
            toaster.error({ title: '', body: '机构剩余量大于本次活动剩余量' });
            return false;
        }
        $resource('/api/as/puc/subsidycompany/update', {}, {}).save(obj, function (res) {
            if (res.errcode === 0) {
                toaster.success({ title: '', body: '更新成功' });
                $scope.loadlist();
            } else {
                toaster.error({ title: '', body: res.errmsg });
            }
        });
    }

    $scope.create = function (obj) {
        $scope.total = 0;
        $scope.num = 0;
        for (var index = 0; index < $scope.companyList.length; index++) {
            var element = $scope.companyList[index];
            $scope.total += (element.subsidy_company_total || 0);
            $scope.num += (element.subsidy_company_num || 0);
        }
        if ($scope.total > item.subsidy_total) {
            toaster.error({ title: '', body: '机构总量大于本次活动总量' });
            return false;
        }
        if ($scope.num > item.subsidy_num) {
            toaster.error({ title: '', body: '机构剩余量大于本次活动剩余量' });
            return false;
        }
        $resource('/api/as/puc/subsidycompany/insert', {}, {}).save(obj, function (res) {
            if (res.errcode === 0) {
                toaster.success({ title: '', body: '新建成功' });
                $scope.loadlist();
            } else {
                toaster.error({ title: '', body: res.errmsg });
            }
        });
    }

    ////设置活动结束
    $scope.start = function (obj) {
        $resource('/api/as/puc/subsidycompany/updateStart', {}, {}).save(obj, function (res) {
            if (res.errcode === 0) {
                toaster.success({ title: '', body: '操作成功' });
                $scope.loadlist();
            } else {
                toaster.error({ title: '', body: res.errmsg });
            }
        });
    }

    //设置活动结束
    $scope.end = function (obj) {
        $resource('/api/as/puc/subsidycompany/updateEnd', {}, {}).save(obj, function (res) {
            if (res.errcode === 0) {
                toaster.success({ title: '', body: '操作成功' });
                $scope.loadlist();
            } else {
                toaster.error({ title: '', body: res.errmsg });
            }
        });
    }
};