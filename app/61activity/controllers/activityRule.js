module.exports = function ($scope, $state, $resource, $modal, toaster, item) {
    $scope.activity = {};
    //机构列表
    $scope.loadlist = function () {
        $resource('/api/as/puc/subsidynoallow/list', {}, {}).save(item, function (res) {
            if (res.errcode === 0) {
                $scope.list = res.data;
                for (var index = 0; index < $scope.list.length; index++) {
                    var element = $scope.list[index];
                    element.state = true;
                    element.is_new = false;
                }
            } else {
                toaster.error({ title: '', body: res.errmsg });
            }
        });
    }
    $scope.loadlist();
    $resource('/api/as/puc/subsidy/list', {}, {}).save({}, function (res) {
        if (res.errcode === 0) {
            $scope.activity_list = res.data;
        } else {
            toaster.error({ title: '', body: res.errmsg });
        }
    });

    $scope.add = function () {
        $scope.list.push({
            subsidy_id: item.subsidy_id,
            subsidy_name: item.subsidy_name,
            subsidy_id_no: "",
            state: false,
            is_new: true
        })
    }

    $scope.edit = function (index) {
        $scope.list[index].state = false;
    }

    $scope.del = function (obj) {
        $resource('/api/as/puc/subsidynoallow/delete', {}, {}).save(obj, function (res) {
            if (res.errcode === 0) {
                toaster.success({ title: '', body: '删除成功' });
                $scope.loadlist();
            } else {
                toaster.error({ title: '', body: res.errmsg });
            }
        });
    }

    $scope.create = function (obj) {
        obj.subsidy_id_no = $scope.activity.data.subsidy_id;
        $resource('/api/as/puc/subsidynoallow/insert', {}, {}).save(obj, function (res) {
            if (res.errcode === 0) {
                toaster.success({ title: '', body: '新建成功' });
                $scope.loadlist();
            } else {
                toaster.error({ title: '', body: res.errmsg });
            }
        });
    }
};