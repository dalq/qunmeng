module.exports = function ($scope, $state, $resource, $modal, toaster) {

    //卡列表
    $scope.searchform = {};
    $scope.loadlist = function () {
        var para = {}
        angular.extend(para, $scope.searchform);
        $resource('/api/as/puc/subsidy/list', {}, {}).save(para, function (res) {
            if (res.errcode === 0) {
                $scope.activityList = res.data;
            } else {
                toaster.error({ title: '', body: res.errmsg });
            }
        });
    }
    $scope.loadlist();

    ////设置活动结束
    $scope.start = function (obj) {
        $resource('/api/as/puc/subsidy/updateStart', {}, {}).save(obj, function (res) {
            if (res.errcode === 0) {
                toaster.success({ title: '', body: '操作成功' });
                $scope.loadlist();
            } else {
                toaster.error({ title: '', body: res.errmsg });
            }
        });
    }

    //设置活动结束
    $scope.end = function (obj) {
        $resource('/api/as/puc/subsidy/updateEnd', {}, {}).save(obj, function (res) {
            if (res.errcode === 0) {
                toaster.success({ title: '', body: '操作成功' });
                $scope.loadlist();
            } else {
                toaster.error({ title: '', body: res.errmsg });
            }
        });
    }

    //修改
    $scope.edit = function (obj) {
        var modalInstance = $modal.open({
            template: require('../views/activityEdit.html'),
            controller: 'activityEdit',
            size: 'lg',
            resolve: {
                item: function () {
                    return obj;
                },
                is_new: function () {
                    return false
                }
            }
        });
        //关闭模态框刷新页面
        modalInstance.result.then(function () {
            $scope.loadlist();
        });
    }

    //分配机构数量
    $scope.company = function (obj) {
        var modalInstance = $modal.open({
            template: require('../views/company.html'),
            controller: 'company',
            size: 'lg',
            resolve: {
                item: function () {
                    return obj;
                }
            }
        });
        //关闭模态框刷新页面
        modalInstance.result.then(function () {
            $scope.loadlist();
        });
    }

    //分配机构数量
    $scope.userList = function (obj) {
        var modalInstance = $modal.open({
            template: require('../views/userList.html'),
            controller: 'userList',
            size: 'lg',
            resolve: {
                item: function () {
                    return obj;
                }
            }
        });
        //关闭模态框刷新页面
        modalInstance.result.then(function () {
            $scope.loadlist();
        });
    }

    //分配机构数量
    $scope.noAllow = function (obj) {
        var modalInstance = $modal.open({
            template: require('../views/activityRule.html'),
            controller: 'activityRule',
            size: 'lg',
            resolve: {
                item: function () {
                    return obj;
                }
            }
        });
        //关闭模态框刷新页面
        modalInstance.result.then(function () {
            $scope.loadlist();
        });
    }

};