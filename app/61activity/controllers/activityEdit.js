module.exports = function ($scope, $resource, $modalInstance, FileUploader, is_new, item, toaster) {

    $scope.is_new = is_new;
    //查找可以被当做赠品的产品
    $scope.getGiftList = function () {
        $resource('/api/as/puc/subsidy/insert', {}, {}).save({}, function (res) {
            if (res.errcode === 0) {
                $scope.obj = res.data;
            } else {
                toaster.error({ title: '', body: res.errmsg });
            }
        });
    }

    //初始化列表
    function init() {
        $scope.status_info = {};
        $scope.searchitem = {};
        if (is_new) {
            $scope.obj = {
                'subsidy_id': '',
                'subsidy_name': '',
                'subsidy_check_flag': '1',
                'subsidy_total': 0,
                'subsidy_type': '1',
                'subsidy_state': '1',
                'subsidy_num': 0,
                'subsidy_num_once': 0,
                'subsidy_nums_max': 0,
                'subsidy_unit': '1',
                'subsidy_type_check_flag': '1'
            };
            $scope.status_info.url = '/api/as/puc/subsidy/insert';
            $scope.status_info.msg = '成功添加一条补贴';
        } else {
            $scope.obj = angular.copy(item);
            $scope.status_info.url = '/api/as/puc/subsidy/update';
            $scope.status_info.msg = '修改补贴信息成功';
        }
    }
    init();

    //添加景区产品
    $scope.ok = function () {
        if (!$scope.obj.subsidy_id || !$scope.obj.subsidy_name) {
            toaster.warning({ title: '', body: '补贴信息不完整' });
            return false;
        }
        $resource($scope.status_info.url, {}, {}).save($scope.obj, function (res) {
            if (res.errcode === 0) {
                toaster.success({ title: '', body: $scope.status_info.msg });
                $scope.status_info.url = '/api/as/puc/subsidy/update';
                $scope.status_info.msg = '修改补贴信息成功';
                $modalInstance.close();
            } else {
                toaster.error({ title: '', body: res.errmsg });
            }
        });
    }

    // $scope.uploader = new FileUploader({
    //     url: 'http://cl.juyouhx.com/oss.php/oss/webuploader1?topdir=aa&selfdir=bb'
    // });

    // $scope.uploader.filters.push({
    //     name: 'imageFilter',
    //     fn: function (item /*{File|FileLikeObject}*/, options) {
    //         var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
    //         return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
    //     }
    // });

    // $scope.uploader.onSuccessItem = function (fileItem, response, status, headers) {
    //     $scope.obj.img = response.savename;
    // };

};