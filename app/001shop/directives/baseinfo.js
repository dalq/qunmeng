module.exports = function ($resource, $state, $http, $q, $modal, dlq) {
	return {
		restrict: 'AE',
		template: require('../views/baseinfo.html'),
		replace: true,
		scope: {
			'configure': '=',
			'gogogo' : '=',
		},
		link: function (scope, elements, attrs) {

	scope.myInterval = 3000;

	scope.title = dlq.title;

	scope.adlist = {};
	scope.typelist = {};
	scope.columnlist = {};

	scope.load = function(){
		var beforedata = {
			// //商城配置信息
			// 'configure':
			// $http({
			// 	'method': 'GET',
			// 	'url': '/api/as/wc/configure/info',
			// }),
			//轮播图列表
			'adlist':
			$http({
				'method': 'GET',
				'url': '/api/as/wc/ad/adlist',
			}),
			//类型列表
			'typelist':
			$http({
				'method': 'GET',
				'url': '/api/as/wc/producttype/producttypelist',
			}),
			//栏目列表
			'columnlist':
			$http({
				'method': 'GET',
				'url': '/api/as/wc/configurecolumn/columnlist',
			}),
		};

		$q.all(beforedata).then(function (res) {
			// //商城配置信息
			// if (res.configure.data.errcode === 0) {
			// } else {
			// 	alert(res.configure.data.errmsg)
			// 	return;
			// }
			//轮播图列表
			if (res.adlist.data.errcode === 0) {
			} else {
				alert(res.adlist.data.errmsg);
				return;
			}
			//分类列表
			if (res.typelist.data.errcode === 0) {
			} else {
				alert(res.typelist.data.errmsg);
				return;
			}
			//栏目列表
			if (res.columnlist.data.errcode === 0) {
			} else {
				alert(res.columnlist.data.errmsg);
				return;
			}

			//console.log(res.configure.data.data);
			console.log(res.adlist.data.data.results);
			console.log(res.typelist.data.data);
			console.log(res.columnlist.data.data);

			//scope.configure = res.configure.data.data;
			scope.adlist = res.adlist.data.data.results;
			scope.typelist = res.typelist.data.data;
			scope.columnlist = res.columnlist.data.data;

		});
	};

	scope.load();

	scope.init = function(){

		//处理图片的高度。
		$("#ad img").css('height' , scope.configure.topimgsize);
		$("#type img").css('width' , scope.configure.typeimgsize * 0.7);
		$("#type img").css('height' , scope.configure.typeimgsize * 0.7);

	};
	
	//----------- 轮播图 ----------------------------//
	scope.adCreate = function(){
		var para = $state.get('app.weshopad');
		var resolve = {
		  obj : function(){
		      return {};
		  },
		};
		angular.extend(para.resolve, resolve);

		var modalInstance = $modal.open(para);
		modalInstance.opened.then(function() {// 模态窗口打开之后执行的函数  
		  console.log('modal is opened');  
		});  
		modalInstance.result.then(function(result) {  
		  scope.load(); 

		}, function(reason) {  
		  console.log(reason);// 点击空白区域，总会输出backdrop  
		  // click，点击取消，则会暑促cancel  
		  $log.info('Modal dismissed at: ' + new Date());  
		}); 
	}


	scope.adRemove = function(obj){
		if(!confirm("您确认要删除吗？")) {
            return;
        }
        var para = {'id' : obj.id, 'del_flg' : '1'};
        updateAd(para);
	};

	scope.adAsort = function(obj){
		var para = {'id' : obj.id, 'asort' : obj.asort};
		updateAd(para);
	};

	scope.adEdit = function(obj){
		var para = $state.get('app.weshopad');
		var resolve = {
		  obj : function(){
		      return obj;
		  }
		};
		angular.extend(para.resolve, resolve);

		var modalInstance = $modal.open(para);
		modalInstance.opened.then(function() {// 模态窗口打开之后执行的函数  
		  console.log('modal is opened');  
		});  
		modalInstance.result.then(function(result) {  
		  scope.load();

		}, function(reason) {  
		  console.log(reason);// 点击空白区域，总会输出backdrop  
		  // click，点击取消，则会暑促cancel  
		  $log.info('Modal dismissed at: ' + new Date());  
		}); 
	};

	function updateAd(para) {
        $resource('/api/as/wc/ad/create', {}, {}).save(para, function (res) {
          console.log(para);
          console.log(res);

          if (res.errcode !== 0) {
            alert("数据获取失败");
            return;
          }
          scope.load();
        });
    }

	//----------- 轮播图 ----------------------------//

	//----------- 类型 ------------------------------//
	scope.typeCreate = function(){
		var para = $state.get('app.weshopproducttype');
		var resolve = {
		  obj : function(){
		      return {};
		  },
		};
		angular.extend(para.resolve, resolve);

		var modalInstance = $modal.open(para);
		modalInstance.opened.then(function() {// 模态窗口打开之后执行的函数  
		  console.log('modal is opened');  
		});  
		modalInstance.result.then(function(result) {  
		  scope.load(); 

		}, function(reason) {  
		  console.log(reason);// 点击空白区域，总会输出backdrop  
		  // click，点击取消，则会暑促cancel  
		  $log.info('Modal dismissed at: ' + new Date());  
		}); 
	};

	scope.typeAsort = function(obj){
		var para = {'id' : obj.id, 'asort' : obj.asort}
		updateType(para);
	};

	scope.typeEdit = function(obj){
		console.log(obj);

		var para = $state.get('app.weshopproducttype');
		var resolve = {
		  obj : function(){
		      return obj;
		  },
		};
		angular.extend(para.resolve, resolve);

		var modalInstance = $modal.open(para);
		modalInstance.opened.then(function() {// 模态窗口打开之后执行的函数  
		  console.log('modal is opened');  
		});  
		modalInstance.result.then(function(result) {  
		  scope.load(); 

		}, function(reason) {  
		  console.log(reason);// 点击空白区域，总会输出backdrop  
		  // click，点击取消，则会暑促cancel  
		  $log.info('Modal dismissed at: ' + new Date());  
		}); 
	};

	scope.typeRemove = function(obj){
		if(!confirm("您确认要删除吗？")) {
            return;
        }
		var para = {'id' : obj.id, 'del_flg' : '1'}
		updateType(para);
	};

	function updateType(para){
    	$resource('/api/as/wc/producttype/create', {}, {}).save(para, function(res){
    		if (res.errcode !== 0) {
				alert(res.errmsg);
				return;
			}
			scope.load();
		});
    }
	//----------- 类型 ------------------------------//


	//----------- 栏目 ------------------------------//
	scope.columnCreate = function(){
		var para = $state.get('app.weshopcolumnmodel');
		var resolve = {
		  obj : function(){
		      return {};
		  },
		};
		angular.extend(para.resolve, resolve);

		var modalInstance = $modal.open(para);
		modalInstance.opened.then(function() {// 模态窗口打开之后执行的函数  
		  console.log('modal is opened');  
		});  
		modalInstance.result.then(function(result) {  
		  scope.load(); 

		}, function(reason) {  
		  console.log(reason);// 点击空白区域，总会输出backdrop  
		  // click，点击取消，则会暑促cancel  
		  $log.info('Modal dismissed at: ' + new Date());  
		}); 
	};
	scope.columnAsort = function(obj){
		var para = {'id' : obj.id, 'asort' : obj.asort};
		updateColumn(para);
	};
	scope.columnEdit = function(obj){
		console.log(obj);

    	var para = $state.get('app.weshopcolumnmodel');
		var resolve = {
		  obj : function(){
		      return obj;
		  },
		};
		angular.extend(para.resolve, resolve);

		var modalInstance = $modal.open(para);
		modalInstance.opened.then(function() {// 模态窗口打开之后执行的函数  
		  console.log('modal is opened');  
		});  
		modalInstance.result.then(function(result) {  
		  scope.load(); 

		}, function(reason) {  
		  console.log(reason);// 点击空白区域，总会输出backdrop  
		  // click，点击取消，则会暑促cancel  
		  $log.info('Modal dismissed at: ' + new Date());  
		}); 

	};
	scope.columnRemove = function(obj){
		if(!confirm("您确认要删除吗？")) {
            return;
        }
		var para = {'id' : obj.id, 'del_flg' : '1'};
		updateColumn(para);
	};

	function updateColumn(para){
		$resource('/api/as/wc/configurecolumn/create', {}, {}).save(para, function(res){
    		if (res.errcode !== 0) {
				alert(res.errmsg);
				return;
			}
			scope.load();
    	})
	}
	//----------- 栏目 ------------------------------//






		}
	};
};

