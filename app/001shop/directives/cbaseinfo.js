module.exports = function ($resource, $state, $http, $q, $modal, dlq) {
	return {
		restrict: 'AE',
		template: require('../views/cbaseinfo.html'),
		replace: true,
		scope: {
			'configure': '=',
			'gogogo' : '=',
		},
		link: function (scope, elements, attrs) {

			console.log('cccccccc');
			console.log(scope.configure);
			console.log('cccccccc');

			scope.qrcodeCreate = function () {
				var para = $state.get('app.imageupload');
				para.resolve.dir = function(){
		            return 'weshop_qrcode';
		        };
		        para.resolve.xxx = function(){
		        	return {
		        		'queueLimit' : 1,
		        	};
		        };
		        var modalInstance = $modal.open(para);
		        modalInstance.opened.then(function() {// 模态窗口打开之后执行的函数  
		        });  
		        modalInstance.result.then(function(result) {  
		        	if(result.length > 0){
		        		scope.configure.qrcodeimg = result[0];
		        	}
		        }, function(reason) {  
		            // click，点击取消，则会暑促cancel  
		            $log.info('Modal dismissed at: ' + new Date());  
		        }); 

			}


			scope.shareimgCreate = function(){
				var para = $state.get('app.imageupload');
				para.resolve.dir = function(){
		            return 'weshop_shareimg';
		        };
		        para.resolve.xxx = function(){
		        	return {
		        		'queueLimit' : 1,
		        	};
		        };
		        var modalInstance = $modal.open(para);
		        modalInstance.opened.then(function() {// 模态窗口打开之后执行的函数  
		        });  
		        modalInstance.result.then(function(result) {  
		        	if(result.length > 0){
		        		scope.configure.share_img = result[0];
		        	}
		        }, function(reason) {  
		            // click，点击取消，则会暑促cancel  
		            $log.info('Modal dismissed at: ' + new Date());  
		        }); 

			};

		}
	};
};

