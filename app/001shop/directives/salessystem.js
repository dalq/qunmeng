module.exports = function ($resource, $state, $http, $q, $modal) {
	return {
		restrict: 'AE',
		template: require('../views/salessystem.html'),
		replace: true,
		scope: {
			'configure': '=',
		},
		link: function (scope, elements, attrs) {

	scope.load = function(){
		var beforedata = {
			
			//分销体系
			'levellist':
			$http({
				'method': 'GET',
				'url': '/api/as/uc/userwxsk/levellist',
			}),
			//分销级别
			'fxlevellist':
			$http({
				'method': 'GET',
				'url': '/api/as/uc/userwxsk/fxlevellist',
			}),
			
		};

		$q.all(beforedata).then(function (res) {
			
			//分销体系
			if (res.levellist.data.errcode === 0) {
			} else {
				alert(res.levellist.data.errmsg);
				return;
			}
			//分销级别
			if (res.fxlevellist.data.errcode === 0) {
			} else {
				alert(res.fxlevellist.data.errmsg);
				return;
			}

			console.log('分销体系');
			console.log(res.levellist.data.data.results);
			console.log(res.fxlevellist.data.data.results);

			scope.levellist = res.levellist.data.data.results;
			scope.fxlevellist = res.fxlevellist.data.data.results;

		});
	};

	scope.load();


	scope.levelCreate = function(obj){
        
    	var modalInstance = $modal.open({
          template: require('../views/pop_level.html'),
          controller: 'qmlevel',
          size: 'xs',
          resolve: {
          	what : function(){
                return 'create';
            },
            obj : function(){
                return obj;
            },
          }
        });

        modalInstance.result.then(function () {
            scope.load();
        }, function () {
            
        });
    }

    scope.levelEdit = function(obj){
        
    	var modalInstance = $modal.open({
          template: require('../views/pop_level.html'),
          controller: 'qmlevel',
          size: 'xs',
          resolve: {
          	what : function(){
                return 'edit';
            },
            obj : function(){
                return obj;
            },
          }
        });

        modalInstance.result.then(function () {
            scope.load();
        }, function () {
            
        });
    }




    scope.fxlevelCreate = function(obj){
        
    	var modalInstance = $modal.open({
          template: require('../views/pop_fxlevel.html'),
          controller: 'qmfxlevel',
          size: 'xs',
          resolve: {
          	what : function(){
                return 'create';
            },
            obj : function(){
                return obj;
            },
          }
        });

        modalInstance.result.then(function () {
            scope.load();
        }, function () {
            
        });
    }

    scope.fxlevelEdit = function(obj){

    	console.log(obj);
        
    	var modalInstance = $modal.open({
          template: require('../views/pop_fxlevel.html'),
          controller: 'qmfxlevel',
          size: 'xs',
          resolve: {
          	what : function(){
                return 'edit';
            },
            obj : function(){
                return obj;
            },
          }
        });

        modalInstance.result.then(function () {
            scope.load();
        }, function () {
            
        });
    }





 //    scope.edit = function(obj){
        
 //    	var modalInstance = $modal.open({
 //          template: require('../views/creatememberlevel.html'),
 //          controller: 'creatememberlevel',
 //          size: 'xs',
 //          resolve: {
 //          	what : function(){
 //                return 'edit';
 //            },
 //            obj : function(){
 //                return obj;
 //            },
 //            save : function(){
 //                return save;
 //            }
 //          }
 //        });

 //        modalInstance.result.then(function () {
 //            scope.load();
 //        }, function () {
            
 //        });
 //    }


 //    scope.commission = function(obj){

 //      var modalInstance = $modal.open({
 //        template: require('../../51weshop/views/commission.html'),
 //        controller: 'weshopcommission',
 //        url: '/weshope/:code',
 //        size: 'lg',
 //        resolve: {
 //          'level': function () {
 //            return obj;
 //          },
          
 //        }
 //      });

 //      modalInstance.opened.then(function () {// 模态窗口打开之后执行的函数  
 //        // $scope.load();
 //      });
 //      modalInstance.result.then(function (showResult) {
 //        scope.load();
 //      }, function (reason) {
 //        scope.load();
 //        // // click，点击取消，则会暑促cancel  
 //        // $log.info('Modal dismissed at: ' + new Date());
 //      });


 //    }




		}
	};
};

