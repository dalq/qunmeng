var App = angular.module('shop', []);

App.config(require('./router'));
App.factory('shopservice', require('./service'));

App.controller('shopconfig',require('./controllers/config'));
App.controller('qmlevel',require('./controllers/level'));
App.controller('qmfxlevel',require('./controllers/fxlevel'));
// App.controller('viewinfo',require('./controllers/info'));
// App.controller('viewedit',require('./controllers/edit'));

// App.directive('placebaseinfo',require('./directives/baseinfo'));
// App.directive('placeview',require('./directives/view'));
// App.directive('placestore',require('./directives/store'));


App.directive('shopbaseinfo',require('./directives/baseinfo'));
App.directive('companybaseinfo',require('./directives/cbaseinfo'));
App.directive('salessystem',require('./directives/salessystem'));
App.directive('membershipsystem',require('./directives/membershipsystem'));




// App.controller('list',require('./controllers/list'));

module.exports = App;