module.exports = function($scope, $modalInstance, what, obj, $resource){

	console.log('级别');

	$scope.obj = {
		'code' : '',
		'title' : '',
	};

	if(what == 'edit') {
		$scope.obj = obj;
	}

	$scope.cancel = function(){
		$modalInstance.close();
	};

	$scope.gogo = function(){

		if($scope.obj.code == '')
		{
			alert('等级编号不能为空');
			return;
		}

		if($scope.obj.title == '')
		{
			alert('等级名称不能为空');
			return;
		}

		$resource('/api/as/uc/userwxsk/savefx', {}, {}).save($scope.obj, function (res) {
			if(res.errcode === 0)
			{
				alert('保存成功');
				$modalInstance.close();
			}
			else
			{
				alert(res.errmsg);
			}

		});

	};



};