module.exports = function($scope, $resource, obj, what, $modalInstance){

    console.log('分销体系');


    $scope.obj = {
		'isadopt' : '0',
		'title' : '',
	};
	
	if(what == 'edit') {
		$scope.obj = obj;
	}


	$scope.cancel = function(){

		$modalInstance.close();

	}

	$scope.gogo = function(){

		if($scope.obj.title == '')
		{
			alert('分销体系不能为空');
			return;
		}

		$resource('/api/as/uc/userwxsk/save', {}, {}).save($scope.obj, function (res) {
			if(res.errcode === 0)
			{
				alert('保存成功');
				$modalInstance.close();
			}
			else
			{
				alert(res.errmsg);
			}

		});

	}

};
