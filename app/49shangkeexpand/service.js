var service = function($resource, $q, $state, $modal){
     //机构列表
    var officeList = '/api/ac/sc/office/getList';
    return {
        officeList : function(){
            return $resource(officeList, {}, {});
        }
    };
};
module.exports = service;

