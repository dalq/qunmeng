var router = function($urlRouterProvider, $stateProvider){


 	$stateProvider
     // 奖品列表
     .state('app.newprizelist', {
        url: "/shangkeexpand/newprizelist.html",
        views: {
            'main@' : {
                template : require('../49shangkeexpand/views/newprizelist.html'),
                controller : 'newprizelist',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
            getDate : function(utilservice){
		             return utilservice.getDate;
		    },
            date2str: function (utilservice) {
                    return utilservice.date2str;
            },
            str2date: function (utilservice) {
                return utilservice.str2date;
            },

        }
    })

    // 砍价用户列表
     .state('app.prizeuser', {
        url: "/shangkeexpand/prizeuser.html/:id",
        views: {
            'main@' : {
                template : require('../49shangkeexpand/views/prizeuser.html'),
                controller : 'prizeuser',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
            date2str: function (utilservice) {
                return utilservice.date2str;
            },
            str2date: function (utilservice) {
                return utilservice.str2date;
            }
        }
    })

    // 获奖用户列表
    .state('app.getPrizeUser', {
        url: "/shangkeexpand/getPrizeUser.html/:id",
        views: {
            'main@' : {
                template : require('../49shangkeexpand/views/getPrizeUser.html'),
                controller : 'getPrizeUser',
            }
        },
        resolve:{
            date2str: function (utilservice) {
                return utilservice.date2str;
            },
            str2date: function (utilservice) {
                return utilservice.str2date;
            },
            
        }
    })
    // 砍价明细
     .state('app.bargaininfo', {
        url: "/shangkeexpand/bargaininfo.html/:active_id",
        views: {
            'main@' : {
                template : require('../49shangkeexpand/views/bargaininfo.html'),
                controller : 'bargaininfo',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
        }
    })

    // 公共广告列表
     .state('app.publicadlist', {
        url: "/shangkeexpand/publicadlist.html",
        views: {
            'main@' : {
                template : require('../49shangkeexpand/views/publicadlist.html'),
                controller : 'publicadlist',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
        }
    })

    // 企业列表
     .state('app.enterpriselist', {
        url: "/shangkeexpand/enterpriselist.html",
        views: {
            'main@' : {
                template : require('../49shangkeexpand/views/enterpriselist.html'),
                controller : 'enterpriselist',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
        }
    })

     // 企业广告列表
     .state('app.corporateadverlist', {
        url: "/shangkeexpand/corporateadverlist.html/:id",
        views: {
            'main@' : {
                template : require('../49shangkeexpand/views/corporateadverlist.html'),
                controller : 'corporateadverlist',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
        }
    })

    // 商客消息列表
     .state('app.sknewslist', {
        url: "/shangkeexpand/sknewslist.html",
        views: {
            'main@' : {
                template : require('../49shangkeexpand/views/sknewslist.html'),
                controller : 'sknewslist',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
            transData : function(utilservice){
                return utilservice.transData;
            },
            officeList : function(shangkeexpandservice){
                return shangkeexpandservice.officeList();
            }

        }
    })

    // 预约
    //商家发布的可预约产品列表
        .state('app.skappointmentlist',{
            url: '/appointmentlist/:id',
            views:{
                'main@':{
                    template: require('./views/appointmentlist.html'),
                    controller : 'skappointmentlist',
                }
            },
            resolve:{
                
            }
        })

        //设置预约
        .state('app.setappointment',{
            url: '/setappointment/:id',
            views:{
                'main@':{
                    template: require('./views/setappointment.html'),
                    controller : 'setappointment',                    
                }
            },
            resolve:{
                productid: function () {
					return '';
				},
				what: function () {
					return 'edit';
				},
				date2str: function (utilservice) {
					return utilservice.date2str;
				},
				str2date: function (utilservice) {
					return utilservice.str2date;
				},
				$uibModalInstance: function () {
					return undefined;
				},
				auditing: function () {
					return false;
				}
                
            }
        })

         //预约人列表
        .state('app.customerlist',{
            url: '/customerlist/:id',
            views:{
                'main@':{
                    template: require('./views/customerlist.html'),
                    controller : 'customerlist',
                }
            },
            resolve:{
                // findUserInfoList : function(appointmentservice){
                //     return appointmentservice.findUserInfoList();
                // },
                date2str: function (utilservice) {
                  return utilservice.date2str;
                },
                getDate : function(utilservice){
                	 return utilservice.getDate;
                },
                str2date: function (utilservice) {
                  return utilservice.str2date;
                }
                
            }
        })


        // 限制人数
        //平台分配一级下发展的二级最大人数
      .state('app.maxpeople', {
        url: '/maxpeople/:id',
        views:{
            'main@':{
                template: require('./views/maxpeople.html'),
                controller : 'maxpeople',
            },
        },
        resolve:{
            
        }
      })

      // 积分充值
       .state('app.integralrecharge', {
        url: '/integralrecharge/:id',
        views:{
            'main@':{
                template: require('./views/integralrecharge.html'),
                controller : 'integralrecharge',
            },
        },
        resolve:{
           
        }
      })

      //------- 看看赚 ---------//
      // 看看赚广告列表
       .state('app.seeadlist', {
        url: '/seeadlist',
        views:{
            'main@':{
                template: require('./views/seeadlist.html'),
                controller : 'seeadlist',
            },
        },
        resolve:{
            date2str: function (utilservice) {
                return utilservice.date2str;
            },
            str2date: function (utilservice) {
                return utilservice.str2date;
            }
           
        }
      })

      // 看看赚广告子表列表
       .state('app.typeadlist', {
        url: '/typeadlist',
        views:{
            'main@':{
                template: require('./views/typeadlist.html'),
                controller : 'typeadlist',
            },
        },
        resolve:{
           
        }
      })

      // 看看赚看广告用户列表
       .state('app.seeaduserlist', { //help_buddy
        url: '/seeaduserlist',
        views:{
            'main@':{
                template: require('./views/seeaduserlist.html'),
                controller : 'seeaduserlist',
            },
        },
        resolve:{
            date2str: function (utilservice) {
                return utilservice.date2str;
            },
            str2date: function (utilservice) {
                return utilservice.str2date;
            }
        }
      })

      // 助力好友
       .state('app.help_buddy', { //help_buddy
        url: '/help_buddy/:id',
        views:{
            'main@':{
                template: require('./views/help_buddy.html'),
                controller : 'help_buddy',
            },
        },
        resolve:{
           
        }
      })

      // 解绑微信
       .state('app.unbundling_wx', { //help_buddy
        url: '/unbundling_wx/:id',
        views:{
            'main@':{
                template: require('./views/unbundling_wx.html'),
                controller : 'unbundling_wx',
            },
        },
        resolve:{
           
        }
      })

       // 解绑用户关联二级
       .state('app.unbundling_relation', { //help_buddy
        url: '/unbundling_relation/:id',
        views:{
            'main@':{
                template: require('./views/unbundling_relation.html'),
                controller : 'unbundling_relation',
            },
        },
        resolve:{
           
        }
      })

       // 商客产品管理列表
       .state('app.skProductList', { //help_buddy
        url: '/skProductList/:id',
        views:{
            'main@':{
                template: require('./views/skProductList.html'),
                controller : 'skProductList',
            },
        },
        resolve:{
           
        }
      })

      // 商客订单列表
       .state('app.skOrderList', { 
        url: '/skOrderList/:id',
        views:{
            'main@':{
                template: require('./views/skOrderList.html'),
                controller : 'skOrderList',
            },
        },
        resolve:{
           
        }
      })

      // 商客线路列表
       .state('app.skLineProduct', { 
        url: '/skLineProduct/:id',
        views:{
            'main@':{
                template: require('./views/skLineProduct.html'),
                controller : 'skLineProduct',
            },
        },
        resolve:{
           
        }
      }) 

      // 商客样式设置(tab页)
       .state('app.skProductStyle', { 
        url: '/skProductStyle/:id',
        views:{
            'main@':{
                template: require('./views/skProductStyle.html'),
                controller : 'skProductStyle',
            },
        },
        resolve:{
           
        }
      }) 

      // 商客样式设置
      .state('app.shangkeProductStyle', { 
        url: '/shangkeProductStyle/:id',
        views:{
            'main@':{
                template: require('./views/shangkeProductStyle.html'),
                controller : 'shangkeProductStyle',
            },
        },
        resolve:{
           
        }
      }) 
      // 集赞活动列表
      .state('app.supportActivityList', { 
        url: '/supportActivityList/:id',
        views:{
            'main@':{
                template: require('./views/supportActivityList.html'),
                controller : 'supportActivityList',
            },
        },
        resolve:{
           
        }
      }) //skPru




};

module.exports = router;