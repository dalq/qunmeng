var App = angular.module('shangkeexpand', []);

App.config(require('./router'));

//service
App.factory('shangkeexpandservice', require('./service'));

App.controller('newprizelist',require('./controllers/newprizelist')); 
App.controller('addnewprize',require('./controllers/addnewprize')); 
App.controller('prizeuser',require('./controllers/prizeuser')); 
App.controller('publicadlist',require('./controllers/publicadlist')); 
App.controller('enterpriselist',require('./controllers/enterpriselist')); 
App.controller('bargaininfo',require('./controllers/bargaininfo')); 
App.controller('addadverpublic',require('./controllers/addadverpublic')); 
App.controller('addprizeadver',require('./controllers/addprizeadver')); 
App.controller('addcorporate',require('./controllers/addcorporate')); 
App.controller('corporateadverlist',require('./controllers/corporateadverlist')); 
App.controller('addcorporateadver',require('./controllers/addcorporateadver')); 
App.controller('sknewslist',require('./controllers/sknewslist')); 
App.controller('addsknews',require('./controllers/addsknews'));  
App.controller('adgetaddress',require('./controllers/adgetaddress'));  //看看赚广告列表获取地址 help_buddy
App.controller('help_buddy',require('./controllers/help_buddy'));  //助力好友 getPrizeUser
App.controller('getPrizeUser',require('./controllers/getPrizeUser'));  //获奖用户 getPrizeUser



// 预约
App.controller('skappointmentlist',require('./controllers/appointmentlist'));
App.controller('setappointment',require('./controllers/setappointment'));
App.directive('appointmentbaseinfo',require('./directives/appointmentbaseinfo'));
App.directive('appointmentscreening',require('./directives/appointmentscreening'));
App.directive('appointmentticket',require('./directives/appointmentticket'));  

App.controller('customerlist',require('./controllers/customerlist'));

// 限制人数
App.controller('maxpeople',require('./controllers/maxpeople'));
App.controller('addlimit',require('./controllers/addlimit'));

// 积分充值
App.controller('integralrecharge',require('./controllers/integralrecharge'));
App.controller('skacountpicture',require('./controllers/skacountpicture'));
App.controller('skrecharge',require('./controllers/skrecharge'));

// 看看赚
App.controller('seeadlist',require('./controllers/seeadlist'));
App.controller('addseeadver',require('./controllers/addseeadver'));
App.controller('typeadlist',require('./controllers/typeadlist'));
App.controller('addadtype',require('./controllers/addadtype'));
App.controller('addadver',require('./controllers/addadver'));
App.controller('seeaduserlist',require('./controllers/seeaduserlist'));

// 解绑
App.controller('unbundling_relation',require('./controllers/unbundling_relation')); // 解绑用户关联二级
App.controller('unbundling_wx',require('./controllers/unbundling_wx')); // 解绑微信 skProductList

// 商客产品管理
App.controller('skProductList',require('./controllers/skProductList')); 
// 商客线路管理
App.controller('skLineProduct',require('./controllers/skLineProduct'));  
// 商客订单列表
App.controller('skOrderList',require('./controllers/skOrderList'));  
// 商客样式设置
App.controller('skProductStyle',require('./controllers/skProductStyle'));  
// 列表样式
App.directive('tpliststyle',require('./directives/tpListStyle'));  
// 预约样式
App.directive('tpappointmentstyle',require('./directives/tpAppointmentStyle'));  
// 激活yangshi
App.directive('tpactivatestyle',require('./directives/tpActivateStyle'));   

App.controller('shangkeProductStyle',require('./controllers/shangkeProductStyle')); 

// 集赞活动列表
App.controller('supportActivityList',require('./controllers/supportActivityList')); //addSupportActivity
App.controller('addSupportActivity',require('./controllers/addSupportActivity')); //addSupportActivity









module.exports = App;