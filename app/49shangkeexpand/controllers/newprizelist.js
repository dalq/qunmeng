module.exports = function($scope, $state, $stateParams,$resource,$modal,toaster,getDate,date2str,str2date){  
    /* 分页
     * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10         //每页显示几条

    $scope.getlist = function () {
        var para = {
            pageNo:$scope.bigCurrentPage, 
            pageSize:$scope.itemsPerPage,
        };
        $resource('/api/as/ac/bargaingoods/findBargainGoodsList', {}, {}).
        save(para,function (res) {
            if (res.errcode != 0) {
                toaster.success({title:"",body:res.errmsg});
                return;
            }
            console.log(res);
            $scope.objs = res.data.results;
            $scope.bigTotalItems = res.data.totalRecord;
            $scope.objs.forEach(function(element) {
                element.isSelected = element.state=='1'?true:false;
            }, this);
        });   
    };
    $scope.getlist();
    $scope.obj = {
        'isSelected' : 'false'
    }
    $scope.onChange = function(isSelected,id,state){
        console.log(isSelected);    
       if(isSelected==false){
        $resource('/api/as/ac/bargaingoods/updateStateEnd', {}, {}).           
            save({'id':id},function(res){
                if(res.errcode!=0){
                        toaster.error({title: "", body:res.errmsg});
                        return;
                }
                console.log(res);
                $scope.getlist();
                return;
            });
       } else {
        $resource('/api/as/ac/bargaingoods/updateState', {}, {}).                      
            save({'id':id},function(res){
                if(res.errcode!=0){
                    toaster.error({title: "", body:res.errmsg});
                    return;
                }
                console.log(res);
                $scope.getlist();
                return;
            });                           
            
       }
    }

    // 添加奖品
    $scope.add = function(id){
        var modalInstance = $modal.open({
            template: require('../views/addnewprize.html'),
            controller: 'addnewprize',
            size: 'lg',
            resolve: {
                getDate: function () {
                    return getDate;
                },
                date2str : function(){
                    return date2str;
                },
                str2date: function () {
                    return str2date;
                },
                 id : function(){
                return id;
            },
            }
        });
        modalInstance.result.then(function (showResult) {	
            $scope.getlist();
        });
        
    };

    // 编辑奖品
    $scope.edit = function (id) {
        var modalInstance = $modal.open({
          template: require('../views/addnewprize.html'),
          controller: 'addnewprize',
          size: 'lg',
          resolve: {
            getDate: function () {
                return getDate;
            },
            date2str : function(){
                return date2str;
            },
            str2date: function () {
                return str2date;
            },
            id : function(){
                return id;
            },
            // savePrize : function(){
            //     return savePrize;
            // },
            
          }
        });

        modalInstance.result.then(function () {
          $scope.getlist();
          
        }, function () {
          //$scope.load();
        });
    };

    $scope.seeprizeuser = function(id){
        console.log(id);
        $state.go('app.prizeuser',{'id' : id});
    }
    $scope.getprizeuser = function(id){
        console.log(id);
        $state.go('app.getPrizeUser',{'id' : id});
    }

    // 删除奖品
    $scope.delete = function (id) {
      if (confirm('确定删除该奖品吗？')) {
        $resource('/api/as/ac/bargaingoods/updateDel', {}, {}).          
          save({'id' : id},function (res) {
            if (res.errcode != 0) {
              toaster.error({title:"",body:res.errmsg});
              return;
            }
            console.log(res);
            $scope.getlist();
        })
      }
        
    };
    // 添加奖品广告
    $scope.addad = function(id){
        var modalInstance = $modal.open({
          template: require('../views/addprizeadver.html'),
          controller: 'addprizeadver',
          size: 'lg',
          resolve: {
            id : function(){
                return id;
            },
            
            
          }
        });

        modalInstance.result.then(function () {
          $scope.getlist();
          
        }, function () {
          //$scope.load();
        });
    }
};