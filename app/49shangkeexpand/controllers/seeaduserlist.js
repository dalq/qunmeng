module.exports = function($scope, $state, $stateParams,$resource,$modal,toaster, date2str, str2date){  
    /* 分页
     * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10         //每页显示几条

    $scope.searchform = {
      'selected' : {
      }
    };
    $scope.vm = {
      'date' : '',
      'options' : {
          format: "YYYY-MM-DD",
          locale : 'zh-cn',
          showClear: true                        
          // clearBtn:true
      }
    }
    $resource('/api/as/ac/activeSeeAd/findChannelList', {}, {}).
    save({},function (res) {
        if (res.errcode != 0) {
            toaster.error({title:"",body:res.errmsg});
            return;
        }
        console.log(res);
        $scope.channelarr = res.data;
    });   
    $scope.getlist = function () {
      console.log($scope.searchform.selected);
      if ( $scope.vm.date._d === null) {
            $scope.vm.date._d = '';
        }
        if (typeof $scope.vm.date._d === 'string') {
            $scope.vm.date._d = $scope.vm.date._d;
        } else {
            $scope.vm.date._d = date2str($scope.vm.date._d);
        }
        var para = {
            pageNo:$scope.bigCurrentPage, 
            pageSize:$scope.itemsPerPage,
            create_time : $scope.vm.date._d,
            channel : $scope.searchform.selected.channel
        };
        $resource('/api/as/ac/activeSeeAd/findSeeApplyaAdList', {}, {}).
        save(para,function (res) {
            if (res.errcode != 0) {
                toaster.success({title:"",body:res.errmsg});
                return;
            }
            console.log(res);
            $scope.objs = res.data.results;
            $scope.bigTotalItems = res.data.totalRecord;
            $scope.objs.forEach(function(element) {
                element.isSelected = element.state=='1'?true:false;
            }, this);
        });   
    };
    $scope.getlist();

    $scope.helppeople = function(id){
        $state.go('app.help_buddy', {'id' : id});
    }

    function time2str(objDate) {
        if(angular.isDate(objDate))
        {
            var y = objDate.getFullYear();
            var m = objDate.getMonth();
            var d = objDate.getDate();
            var h = objDate.getHours();
            var mt = objDate.getMinutes();
            // var s = objDate.getSeconds();
            return y + '-' + (m + 1) + '-' + d + ' ' + h + ':' + mt;
        }
        else
        {
            return '错误格式';
        }
    }
    
};