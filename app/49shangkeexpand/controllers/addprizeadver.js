module.exports = function($scope, $state, $stateParams, $modalInstance,$resource,$modal,FileUploader,toaster,id){
    console.log('奖品id' + id); 
     /* 分页
     * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.currentPage = 1;      //当前页码
    $scope.itemsPerPage = 10         //每页显示几条

    // $scope.obj = {
    //     'isSelected' : 'false'
    // }
    $scope.getlist = function(){
        $resource('/api/ac/ac/activeCorporateAdverService/findGoodsAdverList', {}, {}).          
          save({'goods_id' : id},function (res) {
            if (res.errcode != 0) {
              toaster.error({title:"",body:res.errmsg});
              return;
            }
            console.log(res);
            $scope.objs = res.data.results;
            $scope.totalItems = res.data.totalRecord;
            $scope.objs.forEach(function(element) {
                element.isSelected = element.relation_id == undefined ?false:true;
            }, this);
        })
    }
    $scope.getlist();
     $scope.obj = {
        'isSelected' : 'false'
    }
    
    $scope.onChange = function(isSelected,corporate_adver_id,relation_id){
        console.log(isSelected,corporate_adver_id,relation_id);    
       if(isSelected==false){
        $resource('/api/as/ac/corporate/updateGoodsAdver', {}, {}).           
            save({'relation_id':relation_id},function(res){
                if(res.errcode!=0){
                        toaster.success({title: "", body:res.errmsg});
                        return;
                }
                console.log(res);
                // $scope.getlist();
                return;
            });
       } else {
        $resource('/api/as/ac/corporate/saveGoodsAdver', {}, {}).                      
            save({'goods_id':id, 'adver_id' : corporate_adver_id},function(res){
                if(res.errcode!=0){
                    toaster.success({title: "", body:res.errmsg});
                    return;
                }
                console.log(res);
                // $scope.getlist();
                return;
            });                           
            
       }
    }


    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

    

};