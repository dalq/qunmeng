module.exports = function($scope, $state, $stateParams, $modalInstance,$resource,$modal,FileUploader,toaster,getDate,date2str,str2date,id){
    console.log(id); 
    $scope.selectgood = {
        'flag' : ''
    } 
    $scope.obj = {
        'isSelected' : 'false'
    }
    if(id){
        $scope.selectgood.flag = '0';
    }   
    $scope.info = {
        'sale_code' : '',
        'sale_name' : '',
        'market_price' : '',
        'guide_price' : ''
    }
    $scope.subsidySearchform = {
        'selected' :{
          'subsidy_name' : ''
        }
    };
    $scope.searchform = {
      'selected' :{
          'name' : ''
      }
  };
    $scope.vm = {
        'date' : '',
        'options' : {
            // format: "YYYY-MM-DD",
            locale : 'zh-cn'
        }
    }
    $scope.vm1 = {
        'date' : '',
        'options' : {
            // format: "YYYY-MM-DD",
            locale : 'zh-cn'
        }
    }
    
    var imgarray = [];
    var imgstr = '';
    $scope.getsalelist = function() {///api/as/tc/sale/alllist
        $resource('/api/as/tc/sale/alllist', {}, {}).
        save({'sale_category' : 'S14'},function(res) {
            if (res.errcode!=0) {
                toaster.success({title: "", body:res.errmsg});
                return;
            }
            console.log(res);
            $scope.datas = res.data;
            var array = res.data;
            // 获得奖品详情
            if (id) {
                $resource('/api/as/ac/bargaingoods/getBargainGoods', {}, {}).                
                save({'id':id},function(res) {
                    if (res.errcode!=0) {
                        toaster.error({title: "", body:res.errmsg});
                        return;
                    }       
                    $scope.item = res.data;    
                    $scope.info = res.data;
                    $scope.info.market_price = res.data.market_price/100;
                    $scope.info.price = res.data.price/100;
                    $scope.vm.date = str2date(res.data.start_time);
                    $scope.vm1.date = str2date(res.data.end_time);
                    if(res.data.open_state == '0'){
                        $scope.obj.isSelected = true;
                    } else {
                        $scope.obj.isSelected = false;
                    }
                    console.log('详情');
                    console.log(res);
                    console.log('000' + res.data.subsidy_id);
                    getsubsidylist(res.data.subsidy_id);
                    console.log('!!!!!');
                    for (var i = 0; i < array.length ; i++) {
                        var codeStr = array[i].code;
                        if (res.data.sale_code==codeStr) {
                            $scope.searchform.selected.name = array[i].name;
                            $scope.info.guide_price = array[i].guide_price / 100;
                            console.log('指导价' + $scope.info.guide_price);
                            return;
                        }
                    }
                    
                    
                })
            } 
        });
        
    
    };

    $scope.getsalelist();
    // 活动补贴列表
    function getsubsidylist (subsidy_id) {
      $resource('/api/as/puc/subsidy/list', {}, {}).
      save({},function(res) {
          if (res.errcode!=0) {
              toaster.success({title: "", body:res.errmsg});
              return;
          }
          console.log('~~~~~~~~活动补贴~~~~~');
          console.log(res);
          $scope.subsidyarr = res.data;
          $scope.subsidyarr.forEach(element => {
            if(subsidy_id === element.subsidy_id){
              $scope.subsidySearchform.selected.subsidy_name = element.subsidy_name;
            }
          });
      })
    }
    getsubsidylist();
     // 奖品图片
    var uploader1 = $scope.uploader1 = new FileUploader({
        url: 'http://cl.juyouhx.com/oss.php/oss/webuploader1?topdir=aa&selfdir=bb'
    });

    uploader1.filters.push({
        name: 'imageFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });    
    uploader1.onSuccessItem = function(fileItem, response, status, headers) {
        $scope.info.goods_explain = response.savename; 

    };
    
    
    // 详情图片
    var uploader2 = $scope.uploader2 = new FileUploader({
        url: 'http://cl.juyouhx.com/oss.php/oss/webuploader1?topdir=aa&selfdir=bb'
    });

    uploader2.filters.push({
        name: 'imageFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });
    
    
    uploader2.onSuccessItem = function(fileItem, response, status, headers) {   
        $scope.info.img = response.savename;
    };

    // 奖品logo
    var uploader = $scope.uploader = new FileUploader({
        url: 'http://cl.juyouhx.com/oss.php/oss/webuploader1?topdir=aa&selfdir=bb'
    });

    uploader.filters.push({
        name: 'imageFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });    
    uploader.onSuccessItem = function(fileItem, response, status, headers) {
        $scope.info.logo = response.savename; 

    };
    $scope.change = function(searchform){
        $scope.info.guide_price = searchform.guide_price / 100;
    }
    
     $scope.onChange = function(isSelected,id,state){
        console.log(isSelected);    
       if(isSelected==false){
            $scope.info.open_state = '1';
       } else {
            $scope.info.open_state = '0';
       }
        
    }
    $scope.ok = function () {
        console.log($scope.searchform);
        console.log($scope.subsidySearchform);
        $scope.info.sale_code = $scope.searchform.selected.code;
        $scope.info.sale_name = $scope.searchform.selected.name;
        $scope.info.start_time = time2str($scope.vm.date._d);
        $scope.info.end_time = time2str($scope.vm1.date._d);
        $scope.info.subsidy_id = $scope.subsidySearchform.selected.subsidy_id;
        if(id){
            var para = {
                'id' : id,
                'guide_price' : $scope.info.guide_price * 100,
                'market_price' : $scope.info.guide_price * 100,
                'price' : $scope.info.price * 100
            }
            $scope.info.market_price = $scope.info.guide_price;
            para = angular.extend($scope.info,para);
            $resource('/api/as/ac/bargaingoods/updateBargainGoods', {}, {}).                                
            save(para,function (res) {
                console.log(para);
                if (res.errcode!=0) {
                    toaster.error({title: "", body:res.errmsg});
                    return;
                }
                console.log(res);
                toaster.success({title: "", body:'修改成功'});                    
                $modalInstance.close();
            });
            // }
        } else {
            // 设置价格 准备提交
            var para = {
                'guide_price' : $scope.info.guide_price * 100,
                'market_price' : $scope.info.guide_price * 100,
                'price' : $scope.info.price * 100
            }
            para = angular.extend($scope.info,para);
            $resource('/api/as/ac/bargaingoods/saveBargainGoods', {}, {}).                                
            save(para,function (res) {
                console.log($scope.info);
                if (res.errcode!=0) {
                    toaster.error({title: "", body:res.errmsg});
                    // 错误回滚价格设置
                    // $scope.info.guide_price = $scope.info.guide_price;
                    // $scope.info.market_price = $scope.info.guide_price;
                    return;
                }
                console.log(res);
                toaster.success({title: "", body:'添加成功'});                    
                $modalInstance.close();
            });
        }
        
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

    function time2str(objDate) {
        if(angular.isDate(objDate))
        {
            var y = objDate.getFullYear();
            var m = objDate.getMonth();
            var d = objDate.getDate();
            var h = objDate.getHours();
            var mt = objDate.getMinutes();
            // var s = objDate.getSeconds();
            return y + '-' + (m + 1) + '-' + d + ' ' + h + ':' + mt;
        }
        else
        {
            return '错误格式';
        }
    }


};