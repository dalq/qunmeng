module.exports = function($scope,$state,$modal,$resource,toaster){
    $scope.payment_statusarr = [{'value' : '' , 'label' : '请选择'}, {'value' : '0' , 'label' : '未支付'},{'value' : '1' , 'label' : '已支付'}];
    $scope.searchform = {};

    $scope.vm = {
        'date' : '',
        'options' : {
            // format: "YYYY-MM-DD",
            locale : 'zh-cn',
            showClear: true                        
            // clearBtn:true
        }
    }
    $scope.vm1 = {
        'date' : '',
        'options' : {
            // format: "YYYY-MM-DD",
            locale : 'zh-cn',
            showClear: true                        
            
        }
    }
    /* 分页
    * ========================================= */
    $scope.maxSize = 5;            //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10;         //每页显示几条
    $scope.load = function () {
        if ( $scope.vm.date === null) {
            $scope.vm.date = '';
        }
        if (typeof $scope.vm.date._d === 'string') {
            $scope.vm.date._d = $scope.vm.date._d;
        } else {
            $scope.vm.date._d = time2str($scope.vm.date._d);
        }

        if ( $scope.vm1.date === null) {
            $scope.vm1.date = '';
        }
        if (typeof $scope.vm1.date._d === 'string') {
            $scope.vm1.date._d = $scope.vm1.date._d;
        } else {
            $scope.vm1.date._d = time2str($scope.vm1.date._d);
        }
        

		var para = {
			pageNo: $scope.bigCurrentPage,
            pageSize: $scope.itemsPerPage,
            start_time : $scope.vm.date._d,
            end_time : $scope.vm1.date._d,
		};

		para = angular.extend($scope.searchform, para);


		$resource('/api/as/mc/merorder/findSKOrderList', {}, {})
			.save(para, function (res) {

				if (res.errcode !== 0) {
					toaster.error({ title: "提示", body: res.errmsg });
					return;
                }
                console.log(res);
				$scope.objs = res.data.results;
				$scope.bigTotalItems = res.data.totalRecord;

			});

	};
    $scope.load();

     $scope.orderinfo = function(obj){
     
        var modalInstance = $modal.open({
          template: require('../../11order/views/orderinfo.html'),
          controller: 'orderinfo11',
          size: 'lg',
          resolve: {
            obj : function(){
                return obj;
            }
            // device_code : function(){
            //     return device_code;
            // },
            // typelist : function(){
            //     return typelist;
            // },
            // add : function(){
            //     return add;
            // },
            // del : function(){
            //     return del;
            // }
          }
        });

        modalInstance.result.then(function () {
          //load();
        }, function () {
          //$log.info('Modal dismissed at: ' + new Date());
        });
    }

    function time2str(objDate) {
        if(angular.isDate(objDate))
        {
            var y = objDate.getFullYear();
            var m = objDate.getMonth();
            var d = objDate.getDate();
            var h = objDate.getHours();
            var mt = objDate.getMinutes();
            // var s = objDate.getSeconds();
            return y + '-' + (m + 1) + '-' + d + ' ' + h + ':' + mt;
        }
        else
        {
            return '错误格式';
        }
    }

}