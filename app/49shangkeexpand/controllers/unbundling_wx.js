module.exports = function($scope, $state, $stateParams,$resource,$modal,toaster){   
	/* 分页
        * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10         //每页显示几条
    $scope.info = {};
    $scope.statusarr = [{'label' : '已解绑', 'value' : '0'},{'label' : '已绑定', 'value' : '1'}];
    
    $scope.getlist = function () {
        var para = {
            pageNo:$scope.bigCurrentPage, 
            pageSize:$scope.itemsPerPage
        };
        para = angular.extend($scope.info, para);
        $resource('/api/as/mc/merchantdealerapply/findTwoLevelList', {}, {}).
        save(para,function(res) {
            console.log(para);
            if (res.errcode !== 0) {
                toaster.error({title:"",body:res.errmsg});
                return;
            }
            console.log(para);
            console.log(res);
            $scope.objs = res.data.results;
            $scope.bigTotalItems = res.data.totalRecord;
            $scope.objs.forEach(function(element) {
                element.isSelected = element.status=='1'?false:true;
            }, this);

            
        })
    };
     
    $scope.getlist();

    $scope.onChange = function(isSelected,openid,ticket_id,status){
        console.log(isSelected);  
        // 恢复  
       if(isSelected==false){
        $resource('/api/ac/mc/merchantdealerapplyservice/updateTwoLevelRecover', {}, {}).           
            save({'openid':openid, 'ticket_id' : ticket_id},function(res){
                if(res.errcode!=0){
                    toaster.error({title: "", body:res.errmsg});
                    return;
                }
                toaster.success({title: "", body:"恢复绑定成功"});
                $scope.getlist();
                return;
            });
       } else {
        // 解绑
        $resource('/api/as/mc/merchantdealerapply/updateBindingStatus', {}, {}).                      
            save({'openid':openid, 'ticket_id' : ticket_id},function(res){
                if(res.errcode!=0){
                    toaster.error({title: "", body:res.errmsg});
                    return;
                }
                toaster.success({title: "", body:"解绑成功"});
                $scope.getlist();
                return;
            });                           
            
       }
    }

    // $scope.addnews = function(item) {
    //     var modalInstance = $modal.open({
    //     template: require('../views/addsknews.html'),
    //     controller: 'addsknews',
    //     size: 'lg',
    //     resolve: {
    //         item: function () {
    //             return item;
    //         },
    //      }
    //     });
    //     modalInstance.result.then(function (showResult) {	
    //         $scope.getlist();
    //     });
    // };
    // $scope.delete = function(id) {
    //     if (confirm('确定要删除吗?')) {
    //         $resource('/api/as/mc/mermessage/updateDel', {}, {}).            
    //         save({'id':id},function(res) {
    //             console.log({'id':id});
    //             if (res.errcode !== 0) {
    //                 toaster.error({title:"",body:res.errmsg});
    //                 return;
    //             } 
    //             toaster.success({title:"",body:"删除成功"});                
    //             $scope.getlist();
    //         })
    //         return;
    //     } 
        
    // };

    // $scope.edit = function(item) {
    //     var modalInstance = $modal.open({
    //     template: require('../views/addsknews.html'),
    //     controller: 'addsknews',
    //     size: 'lg',
    //     resolve: {
    //         item: function () {
    //             return item;
    //         },
    //      }
    //     });
    //     modalInstance.result.then(function (showResult) {	
    //         $scope.getlist();
    //     });
    //     // $state.go('app.addnewsrolling',{'id':id});
    // };
    

 

};