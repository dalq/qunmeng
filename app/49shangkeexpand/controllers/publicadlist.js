module.exports = function($scope, $state, $stateParams,$resource,$modal,toaster){  
    // var id =  $stateParams.id;
    // console.log(id);
    // $scope.info = {
    //     'id' : id
    // };
    /* 分页
     * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.currentPage = 1;      //当前页码
    $scope.itemsPerPage = 10         //每页显示几条

    $scope.getlist = function () {
        var para = {
            pageNo:$scope.currentPage, 
            pageSize:$scope.itemsPerPage,
        };
        $resource('/api/as/ac/adverpublic/findAdverPublicList', {}, {}).
        save(para,function (res) {
            if (res.errcode != 0) {
                toaster.success({title:"",body:res.errmsg});
                return;
            }
            console.log(res);
            $scope.objs = res.data.results;
            $scope.totalItems = res.data.totalRecord;
            $scope.objs.forEach(function(element) {
                console.log(element.prompt_info.substring(0,5)+'...');
                element.isSelected = element.state=='1'?false:true;
            }, this);
        });   
    };
    $scope.getlist();
    $scope.obj = {
        'isSelected' : 'false'
    }
    $scope.onChange = function(isSelected,id,state){
        console.log(isSelected);    
       if(isSelected==false){
        $resource('/api/as/ac/adverpublic/updateNotUseAdverPublic', {}, {}).           
            save({'id':id},function(res){
                if(res.errcode!=0){
                        toaster.error({title: "", body:res.errmsg});
                        return;
                }
                console.log(res);
                $scope.getlist();
                return;
            });
       } else {
        $resource('/api/as/ac/adverpublic/updateUseAdverPublic', {}, {}).                      
            save({'id':id},function(res){
                if(res.errcode!=0){
                    toaster.error({title: "", body:res.errmsg});
                    return;
                }
                console.log(res);
                $scope.getlist();
                return;
            });                           
            
       }
    }

    // 添加广告
    $scope.add = function(id){
        var modalInstance = $modal.open({
            template: require('../views/addadverpublic.html'),
            controller: 'addadverpublic',
            size: 'lg',
            resolve: {
                id: function () {
                    return id;
                }
            }
        });
        modalInstance.result.then(function (showResult) {	
                $scope.getlist();
        });
        
    };

    // 编辑广告
    $scope.edit = function (id) {
        var modalInstance = $modal.open({
          template: require('../views/addadverpublic.html'),
          controller: 'addadverpublic',
          size: 'lg',
          resolve: {
            id : function(){
                return id;
            },
            
          }
        });

        modalInstance.result.then(function () {
          $scope.getlist();
          
        }, function () {
          //$scope.load();
        });
    };

    // 删除奖品
    $scope.delete = function (id) {
      if (confirm('确定删除该奖品吗？')) {
        $resource('/api/as/ac/adverpublic/updateDel', {}, {}).          
          save({'id' : id},function (res) {
            if (res.errcode != 0) {
              toaster.error({title:"",body:res.errmsg});
              return;
            }
            console.log(res);
            $scope.getlist();
        })
      }
        
    };
    $scope.seeadver_img = function(pimg){
        var modalInstance = $modal.open({
            template: require('../../45shangke/views/skacountpicture.html'),
            controller: 'skacountpicture',
            // size: 'lg',
            resolve: {
                pimg: function () {
                    return pimg;
                }
            }
        });
        modalInstance.result.then(function (showResult) {	
            $scope.getlist();
        });
    }
};