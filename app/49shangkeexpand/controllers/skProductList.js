module.exports = function ($scope, $state, $resource, $modal,toaster){
    $scope.statusArr = [{'value' : '0', 'name' : '草稿'},{'value' : '1', 'name' : '已上架'},{'value' : '2', 'name' : '已下架'}];
    $scope.statusArr.splice(0,0,{
        'name':'----全部----',
        'value':''
    })
    $scope.statusArr1 = [{'value' : '0', 'name' : '已上架'},{'value' : '1', 'name' : '已下架'}];
    $scope.statusArr1.splice(0,0,{
        'name':'----全部----',
        'value':''
    })
    $scope.searchform = {};
    
    // 产品分类 
    $resource('/api/us/mc/mertradetypedao/findByTypeList', {}, {})
    .save({'type' : 'cheap_menu'},function(res){
        if(res.errcode !== 0)
        {
            toaster.error({title:"",body:res.errmsg});
            return;
        }
        $scope.dictbytypelist = res.data;
        // $modalInstance.close();

    }) 
    /* 分页
    * ========================================= */
    $scope.maxSize = 5;            //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10;         //每页显示几条
    $scope.load = function () {

		var para = {
			pageNo: $scope.bigCurrentPage,
			pageSize: $scope.itemsPerPage
		};

		para = angular.extend($scope.searchform, para);


		$resource('/api/as/mc/mergoods/findSKGoodsList', {}, {})
			.save(para, function (res) {

				if (res.errcode !== 0) {
					toaster.error({ title: "提示", body: res.errmsg });
					return;
                }
                console.log(res);
                $scope.objs = res.data.results;
                $scope.bigTotalItems = res.data.totalRecord;
                $scope.objs.forEach(function(element) {
                    for(var i = 0; i < $scope.dictbytypelist.length; i++){
                        if(element.sale_genre == $scope.dictbytypelist[i].code){
                            element.sale_genre = $scope.dictbytypelist[i].label;
                        }
                    }
                    element.isSelected = element.is_disable =='1'?true:false;
                }, this);
                
                
                

			});

	};
    $scope.load();
    $scope.obj = {
        'isSelected' : 'false'
    }
    $scope.onChange = function(isSelected,sale_code){
        console.log('禁售');
        console.log(isSelected);    
        if(isSelected==false){
         $resource('/api/as/mc/mergoods/updateSKSaleAppAble', {}, {}).           
             save({'sale_code':sale_code},function(res){
                 if(res.errcode!=0){
                         toaster.error({title: "", body:res.errmsg});
                         return;
                 }
                 console.log(res);
                 $scope.load();
                 return;
             });
        } else {
         $resource('/api/as/mc/mergoods/updateSKSaleAppDisable', {}, {}).                      
             save({'sale_code':sale_code},function(res){
                 if(res.errcode!=0){
                     toaster.error({title: "", body:res.errmsg});
                     return;
                 }
                 console.log(res);
                 $scope.load();
                 return;
             });                           
             
        }
        
    }
    
    $scope.changed = function(aaa){
        console.log(aaa);
    }
    $scope.change = function(){
        console.log('5555');
    }
    
    // $resource('/api/as/sc/dict/dictbytypelist', {}, {})
    // .save({ type: 'sale_category' }, function (res) {

    //     if (res.errcode !== 0) {
    //         toaster.error({ title: "提示", body: res.errmsg });
    //         return;
    //     }

    //     $scope.dictbytypelist = res.data;
    //     $scope.dictbytypelist.splice(0,0,{
    //         label:'----全部----',
    //         value:''
    //     })

    // });
    // 上架
    $scope.start = function(item){
        $resource('/api/as/mc/mergoods/updateOpenSkState', {}, {})
        .save({ 'id':  item.id}, function (res) {

            if (res.errcode !== 0) {
                toaster.error({ title: "提示", body: res.errmsg });
                return;
            }
            toaster.success({ title: "提示", body: "上架成功"});
            $scope.load();
            
        });
    }
    // 下架
    $scope.stop = function(item){
        $resource('/api/as/mc/mergoods/updateCloseSkState', {}, {})
        .save({ 'id':  item.id}, function (res) {

            if (res.errcode !== 0) {
                toaster.error({ title: "提示", body: res.errmsg });
                return;
            }
            toaster.success({ title: "提示", body: "下架成功"});
            $scope.load();
            

        });
    }
    $scope.tbplatform = function(obj){
        console.log('同步');
        $resource('/api/ac/mc/mergoodsserviceimpl/updateSaleState', {}, {})
        .save({ 'sale_code':  obj.sale_code}, function (res) {

            if (res.errcode !== 0) {
                toaster.error({ title: "提示", body: res.errmsg });
                return;
            }
            toaster.success({ title: "提示", body: "同步成功"});
            $scope.load();
            

        });
    }
    $scope.skset = function(id,code,name,guide_price,cost_price,productflag){
		$scope.item = '';
		productflag = 'productflag';
		var modalInstance = $modal.open({
			template: require('../../29product/views/skprofit.html'),
			controller: 'skprofit',
			url: '/skprofit',
			size: 'lg',
			resolve: {
				item: function () {
                    return $scope.item;
				},
				id: function () {
					return id;
				},
				code: function () {
					return code;
				},
				name: function () {
					return name;
				},
				guide_price: function () {
					return guide_price;
				},
				cost_price: function () {
					return cost_price;
				},
				productflag: function () {
					return productflag;
				},
				what: function () {
					return 'edit';
				},

			}
		});

		modalInstance.result.then(function () {
		}, function () {
		});
		// $state.go('app.skprofit');
		// $state.go('app.skprofit',{'code' : code, 'name' : name, 'guide_price' : guide_price, 'cost_price' : cost_price});
    }
    
    // 样式设置
    $scope.styleset = function(obj){
        console.log(obj);
        $state.go('app.shangkeProductStyle', {'id' : obj.id});
    }
    
    



}
