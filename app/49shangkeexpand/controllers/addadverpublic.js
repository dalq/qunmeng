module.exports = function($scope, $state, $stateParams, $modalInstance,$resource,$modal,FileUploader,toaster,id){
    // console.log(id); 
    $scope.info = {
        'corporate_name' : '',
        'adver_img' : '',
        'prompt_title' : '',
        'prompt_info' : ''
    }
    if(id){
        $resource('/api/as/ac/adverpublic/getAdverPublic', {}, {}).                
        save({'id' : id},function(res) {
            if (res.errcode!=0) {
                toaster.error({title: "", body:res.errmsg});
                return;
            }
            console.log(res);
            $scope.info = res.data;

        });
    }
    // 广告图片
    var uploader1 = $scope.uploader1 = new FileUploader({
        url: 'http://cl.juyouhx.com/oss.php/oss/webuploader1?topdir=aa&selfdir=bb'
    });

    uploader1.filters.push({
        name: 'imageFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });    
    uploader1.onSuccessItem = function(fileItem, response, status, headers) {
        $scope.info.adver_img = response.savename; 

    };
    
    $scope.ok = function () {       
        // 编辑公共广告
        if (id) {
            if ($scope.info.corporate_name!=''&&$scope.info.adver_img!=''&&$scope.info.prompt_title!=''&&$scope.info.prompt_info) {
                var para = {
                    'id' : id
                }
                para = angular.extend($scope.info,para);
                console.log(para);
                $resource('/api/as/ac/adverpublic/updateAdverPublic', {}, {}).                
                save(para,function(res) {
                    if (res.errcode!=0) {
                        toaster.error({title: "", body:res.errmsg});
                        return;
                    }
                    console.log(res);
                    toaster.success({title: "", body:"修改成功"});
                    $modalInstance.close();
                });
            } else {
                toaster.success({title: "", body:"请将数据补充完整"});
            }           
        } else {
            if ($scope.info.corporate_name!=''&&$scope.info.adver_img!=''&&$scope.info.prompt_title!=''&&$scope.info.prompt_info) {
                // 添加公共广告             
                $resource('/api/as/ac/adverpublic/saveAdverPublic', {}, {}).                                
                save($scope.info,function (res) {
                    console.log($scope.info);
                    if (res.errcode!=0) {
                        toaster.error({title: "", body:res.errmsg});
                        return;
                    }
                    console.log(res);
                    toaster.success({title: "", body:'添加成功'});                    
                    $modalInstance.close();
                });
            } else {
                toaster.error({title: "", body:'请将数据补充完整'});                
            }      
        }
        
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

    

};