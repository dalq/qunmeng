module.exports = function($scope, $stateParams, $state, $modal,toaster,$resource){ 
    /* 分页
     * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10         //每页显示几条

    $scope.info = {
        'mobile' : '',
        'nickname' : ''
    }
    $scope.search = function(){
        var para = {
            pageNo:$scope.bigCurrentPage, 
            pageSize:$scope.itemsPerPage,
        }
         $resource('/api/as/uc/userwxsk/findUserinfForSkList', {}, {}).save($scope.info,function(res){
            if(res.errcode != 0){
                alert(res.errmsg);
                // toaster.success({title:"",body:res.errmsg});
                return;
            }
            console.log(res);
            $scope.objs = res.data.results;
            $scope.bigTotalItems = res.data.totalRecord;
        })
    }

    $scope.recharge = function(userid){
        var modalInstance = $modal.open({
          template: require('../views/skrecharge.html'),
          controller: 'skrecharge',
          size: 'lg',
          resolve: {
            userid : function(){
                return userid;
            }
            
          }
        });

        modalInstance.result.then(function () {
          //$scope.getlist();
          
        }, function () {
          //$scope.load();
        });
    }

   $scope.seepicture = function(pimg){
        var modalInstance = $modal.open({
          template: require('../views/skacountpicture.html'),
          controller: 'skacountpicture',
          //size: 'lg',
          resolve: {
            pimg : function(){
                return pimg;
            }          
          }
        });

        modalInstance.result.then(function (showResult) {
          
         
        }, function () {
         //  $scope.getlist();
          //$log.info('Modal dismissed at: ' + new Date());
        });
     //  $scope.getlist();
   }
    
};