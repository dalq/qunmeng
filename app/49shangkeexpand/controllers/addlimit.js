module.exports = function($scope, $stateParams, $state, $modal, $modalInstance,FileUploader,code,toaster,$resource){  
    $scope.info = {
        'company_code' : code,
        'limits' : ''

    }
    $scope.ok = function(){
        $resource('/api/ac/mc/merchantdealerapplyservice/saveLimit', {}, {}).
        save($scope.info,function(res){
            console.log($scope.info);
            if(res.errcode != 0){
                toaster.error({title: "", body:res.errmsg});
                return;
            }
            console.log(res);
            toaster.success({title: "", body:'设置成功'});                    
            $modalInstance.close();
        })
    }
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

};