module.exports = function($scope, $stateParams, $state,$modal,$resource,FileUploader,toaster,date2str,str2date){
    /* 分页
     * ========================================= */
    $scope.maxSize = 5;            //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10;         //每页显示几条
    $scope.info = {
        'name' : '',
        'mobile' : '',
        'title' : '',
        'startTime' : ''
    }
   $scope.date = {
        'lable': '',
        'opened': false
    }

    $scope.getlist = function(){
        if (typeof $scope.date.lable === 'string') {
            $scope.date.lable = $scope.date.lable;
        } else {
            $scope.date.lable = date2str($scope.date.lable);
        }
        var para = {
            pageNo:$scope.bigCurrentPage, 
            pageSize:$scope.itemsPerPage,
            'startTime' : $scope.date.lable
        };
        para = angular.extend($scope.info,para);
        $resource('/api/as/mc/mermakeappointmentdao/findUserInfoList', {}, {}).save(para,function(res){
            console.log(para);
            if(res.errcode!=0){
                toaster.success({title:"",body:res.errmsg});
                return;
            }
            console.log(res);
            $scope.objs = res.data.results;
            $scope.bigTotalItems = res.data.totalRecord;
        })
    };
    $scope.getlist();

    $scope.dateOpen = function ($event, item) {
        $event.preventDefault();
        $event.stopPropagation();
        item.opened = true;
    };


};