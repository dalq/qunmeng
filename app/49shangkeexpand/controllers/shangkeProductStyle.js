module.exports = function($scope,$resource, $state, $stateParams,$http, $q,toaster, $modal){
    var id = $stateParams.id;
    console.log(id);
    $scope.getlist = function(){
        $resource('/api/as/sc/dict/dictbytypelist ', {}, {}).save({'type' : 'mc_tp_category'}, function(res){
            if(res.errcode != 0){
                toaster.error({title:"",body:res.errmsg});
                return;
            }
            console.log(res);
            $scope.objs = res.data;
        });
    }
    $scope.getlist();
    $scope.setstyle = function(item){
        $scope.item = item;
		var modalInstance = $modal.open({
			template: require('../views/skProductStyle.html'),
			controller: 'skProductStyle',
			url: '/skProductStyle',
			size: 'lg',
			resolve: {
				item: function () {
                    return $scope.item;
                },
                id : function(){
                    return id;
                }
				
			}
		});

		modalInstance.result.then(function () {
		}, function () {
        });
    }
}    