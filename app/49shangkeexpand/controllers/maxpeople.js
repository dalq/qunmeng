module.exports = function($scope, $stateParams, $state,$modal,toaster,$resource){   
	/* 分页
     * ========================================= */
    $scope.maxSize = 5;            //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10;         //每页显示几条
    $scope.searchform = {};
    $scope.getlist = function () {
        var para = {
            pageNo:$scope.bigCurrentPage, 
            pageSize:$scope.itemsPerPage
        };
        para = angular.extend($scope.searchform, para);
        $resource('/api/ac/sc/systemUserService/getOfficeUserList', {}, {}).save(para, function(res){
            if(res.errcode !== 0){
                toaster.error({title:"",body:res.errmsg});
                return;
            }else{
                console.log(res);
                $scope.objs = res.data.results;
                $scope.bigTotalItems = res.data.totalRecord;
            }
        });
    };
     
    $scope.getlist();

    $scope.limit = function(code){
        var modalInstance = $modal.open({
        template: require('../views/addlimit.html'),
        controller: 'addlimit',
        size: 'lg',
        resolve: {
            code: function () {
                return code;
            },
         }
        });
        modalInstance.result.then(function (showResult) {	
            $scope.getlist();
        });
        
    }

    $scope.seepeople = function(code){
        $resource('/api/as/mc/merchantdealerapply/getLimit', {}, {}).save({'company_code' : code},function(res){
            if(res.errcode === 10003){
                toaster.error({title : "", body:"未设置限制人数"});
            } else if(res.errcode == 0){
                console.log(res);
                alert(res.data.limits + '人');
            } else {
                toaster.error({title : "", body:res.errmsg});
            }
            
            
        })
    }

   

};