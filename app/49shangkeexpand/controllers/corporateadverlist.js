module.exports = function($scope, $state, $stateParams,$resource,$modal,toaster){  
    // 企业id
    var id = $stateParams.id;
    console.log('企业id' + id);
    /* 分页
     * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.currentPage = 1;      //当前页码
    $scope.itemsPerPage = 10         //每页显示几条

    $scope.getlist = function () {
        var para = {
            pageNo:$scope.currentPage, 
            pageSize:$scope.itemsPerPage,
            'corporate_id' : id
        };
        $resource('/api/as/ac/corporate/findCorporateAdverList', {}, {}).
        save(para,function (res) {
            if (res.errcode != 0) {
                toaster.success({title:"",body:res.errmsg});
                return;
            }
            console.log(res);
            $scope.objs = res.data.results;
            $scope.totalItems = res.data.totalRecord;
        });   
    };
    $scope.getlist();
    
    // 添加企业广告
    $scope.add = function(ad_id){
        var modalInstance = $modal.open({
            template: require('../views/addcorporateadver.html'),
            controller: 'addcorporateadver',
            size: 'lg',
            resolve: {
                id : function(){
                    return id;
                },
                ad_id : function(){
                    return ad_id;
                }
            }
        });
        modalInstance.result.then(function (showResult) {	
            $scope.getlist();
        });
        
    };

    // 编辑企业广告
    $scope.edit = function (ad_id) {
        var modalInstance = $modal.open({
          template: require('../views/addcorporateadver.html'),
          controller: 'addcorporateadver',
          size: 'lg',
          resolve: {
            id : function(){
                return id;
            },
            ad_id : function(){
                return ad_id;
            }
          }
        });

        modalInstance.result.then(function () {
          $scope.getlist();
          
        }, function () {
          //$scope.load();
        });
    };

    // 删除企业广告
    $scope.delete = function (corporate_adver_id) {
      if (confirm('确定删除该企业广告吗？')) {
        $resource('/api/as/ac/corporate/updateCorporateAdverDel', {}, {}).          
          save({'id' : corporate_adver_id},function (res) {
            if (res.errcode != 0) {
              toaster.error({title:"",body:res.errmsg});
              return;
            }
            console.log(res);
            $scope.getlist();
        })
      }
        
    };

    $scope.getaddress = function(id){
        var flag = 'enterprise';
        var modalInstance = $modal.open({
            template: require('../views/adgetaddress.html'),
            controller: 'adgetaddress',
            size: 'lg',
            resolve: {
                id: function () {
                    return id;
                },
                flag: function () {
                    return flag;
                },
            }
            });
            modalInstance.result.then(function (showResult) {	
                $scope.getlist();
            });
    }
    
};