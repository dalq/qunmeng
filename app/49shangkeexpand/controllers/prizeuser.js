module.exports = function($scope, $stateParams, $state, $modal,$resource,toaster,date2str, str2date){
  	var id = $stateParams.id;
    console.log(id);
  /* 分页
     * ========================================= */
    $scope.maxSize = 5;            //最多显示多少个按钮
    $scope.currentPage = 1;      //当前页码
    $scope.itemsPerPage = 10;         //每页显示几条

    $scope.vm = {
      'date' : '',
      'options' : {
          format: "YYYY-MM-DD",
          locale : 'zh-cn',
          showClear: true                        
          // clearBtn:true
      }
    }
    $scope.searchform = {
      'selected' : {
      }
    };
    $resource('/api/as/ac/bargaingoods/findChannelList', {}, {}).
    save({},function (res) {
        if (res.errcode != 0) {
            toaster.error({title:"",body:res.errmsg});
            return;
        }
        console.log(res);
        $scope.channelarr = res.data;
    });   
    $scope.getlist = function () {
        if ( $scope.vm.date === null) {
            $scope.vm.date = '';
        }
        if (typeof $scope.vm.date._d === 'string') {
            $scope.vm.date._d = $scope.vm.date._d;
        } else {
            $scope.vm.date._d = date2str($scope.vm.date._d);
        }
        var para = {
            pageNo:$scope.currentPage, 
            pageSize:$scope.itemsPerPage,
            create_time : $scope.vm.date._d,
            goods_id : id,
            channel : $scope.searchform.selected.channel
        };
        // para = angular.extend($scope.info,para); 
        $resource('/api/as/ac/bargaingoods/findBargainActiveList', {}, {}).
        save(para, function (res) {
                if (res.errcode != 0) {
                    alert(res.errmsg);
                        // toaster.success({title:"",body : res.errmsg});
                    return;
                }
                console.log(res);
                $scope.objs = res.data.results;
                $scope.totalItems = res.data.totalRecord;
        });
    };
    $scope.getlist();	
    $scope.seebargaininfo = function(active_id){
        $state.go('app.bargaininfo',{'active_id' : active_id});
    }
    function time2str(objDate) {
      if(angular.isDate(objDate))
      {
          var y = objDate.getFullYear();
          var m = objDate.getMonth();
          var d = objDate.getDate();
          var h = objDate.getHours();
          var mt = objDate.getMinutes();
          // var s = objDate.getSeconds();
          return y + '-' + (m + 1) + '-' + d + ' ' + h + ':' + mt;
      }
      else
      {
          return '错误格式';
      }
  }




};