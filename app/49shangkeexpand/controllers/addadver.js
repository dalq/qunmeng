module.exports = function($scope, $state, $stateParams, $modalInstance, $resource,$modal,FileUploader,toaster,ad_type,id){
    console.log(ad_type,id);
    $scope.info = {
        'ad_type' : ad_type
    }
    $scope.searchform = {
        'id1' : '',
        'id2' : '',
        'id3' : ''
    };
    $resource('/api/as/ac/activeSeeAd/getSeeListAd', {}, {}).
    save({'id' : id}, function(res){
        if(res.errcode == 10010){
            toaster.error({title:"",body:"没有已选择广告"});				
        }
        else if(res.errcode == 0){
            console.log('~~~~~~~~~');
            console.log(res);
            var list_adarr = res.data.list_ad.split(",");
            console.log(list_adarr);
            if(ad_type == '1'){
                $scope.searchform.id1 = list_adarr[0];
            } else if(ad_type == '2'){
                $scope.searchform.id1 = list_adarr[0];
                $scope.searchform.id2 = list_adarr[1];
            } else if(ad_type == '3'){
                $scope.searchform.id1 = list_adarr[0];
                $scope.searchform.id3 = list_adarr[1];
            } else {
                $scope.searchform.id1 = list_adarr[0];
                $scope.searchform.id2 = list_adarr[1];                
                $scope.searchform.id3 = list_adarr[2];
            }
        } else {
            toaster.error({title:"",body:res.errmsg});				
        }

    });
    $resource('/api/ac/ac/activeSeeAdService/getSeeListAd', {}, {}).
    save({'ad_type' : ad_type, 'id' : id}, function(res){
        if(res.errcode !== 0)
        {
            toaster.error({title:"",body:res.errmsg});				
        }
        else
        {
            console.log(res);
            $scope.gzhListArr = res.data.gzhList;
            $scope.gzhtwListArr = res.data.gzhtwList;
            $scope.zdyListArr = res.data.zdyList;
            console.log($scope.gzhListArr);
            console.log($scope.gzhtwListArr);
            console.log($scope.zdyListArr);
            // console.log($scope.gzhListArr[0].content);
            // getSimpleText($scope.gzhListArr[0].content);
        }

    });
    //  function getSimpleText(html){
    //     var re1 = new RegExp("<.+?>","g");//匹配html标签的正则表达式，"g"是搜索匹配多个符合的内容
    //     var msg = html.replace(re1,'');//执行替换成空字符
    //     console.log(msg);
    //     return msg;
    // }
	$scope.ok = function(){
        if(ad_type == '1'){
            var idarr = [$scope.searchform.id1];  
        } else if(ad_type == '2'){
            var idarr = [$scope.searchform.id1, $scope.searchform.id2];  
        } else if(ad_type == '3'){
            var idarr = [$scope.searchform.id1, $scope.searchform.id3];              
        } else {
            var idarr = [$scope.searchform.id1, $scope.searchform.id2, $scope.searchform.id3];  
        }
        var idstr = idarr.join(",");
        console.log(id);
		$resource('/api/ac/ac/activeSeeAdService/updateSeeListAd', {}, {}).
		save({'list_ad' : idstr, 'id' : id}, function(res){
			if(res.errcode === 0)
			{
                toaster.success({title:"",body:"操作成功"});
				$modalInstance.close();
			}
			else
			{
                toaster.error({title:"",body:res.errmsg});				
			}
 
		});

	}
    $scope.cancel = function(){
		$modalInstance.close();
	}




};