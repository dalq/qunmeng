module.exports = function($scope,$state,$modal,$resource,toaster){
    $scope.searchform = {};
    $scope.producttypelist = [{'value' : '', 'name' : '请选择'},{'value' : '0', 'name' : '周边游'},{'value' : '1', 'name' : '国内长线'},
                                {'value' : '2', 'name' : '国内当地参团'},{'value' : '3', 'name' : '出境当地参团'}, {'value' : '4', 'name' : '处境短线'}, 
                                {'value' : '5', 'name' : '处境长线'}];
    /* 分页
    * ========================================= */
    $scope.maxSize = 5;            //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10;         //每页显示几条
    $scope.load = function () {

		var para = {
			pageNo: $scope.bigCurrentPage,
			pageSize: $scope.itemsPerPage
		};

		para = angular.extend($scope.searchform, para);


		$resource('/api/as/mc/mergoods/findLineGoodsList', {}, {})
			.save(para, function (res) {

				if (res.errcode !== 0) {
					toaster.error({ title: "提示", body: res.errmsg });
					return;
                }
                console.log(res);
				$scope.objs = res.data.results;
                $scope.bigTotalItems = res.data.totalRecord;
                $scope.objs.forEach(function(element) {
                    for(var i = 0; i < $scope.producttypelist.length; i++){
                        if(element.product_type == $scope.producttypelist[i].value){
                            element.product_type = $scope.producttypelist[i].name;
                        }
                    }
                }, this);

			});

	};
    $scope.load();

    // 上架
    $scope.start = function(item){
        $resource('/api/as/mc/mergoods/updateOpenSkState', {}, {})
        .save({ 'id':  item.id}, function (res) {

            if (res.errcode !== 0) {
                toaster.error({ title: "提示", body: res.errmsg });
                return;
            }
            toaster.success({ title: "提示", body: "上架成功"});
            $scope.load();
            
        });
    }
    // 下架
    $scope.stop = function(item){
        $resource('/api/as/mc/mergoods/updateCloseSkState', {}, {})
        .save({ 'id':  item.id}, function (res) {

            if (res.errcode !== 0) {
                toaster.error({ title: "提示", body: res.errmsg });
                return;
            }
            toaster.success({ title: "提示", body: "下架成功"});
            $scope.load();
            

        });
    }

    $scope.skset = function(index,id,code,name,guide_price,cost_price,productflag){
				$scope.item = $scope.objs[index];                
                console.log($scope.item);
                console.log('ssssss');
                var modalInstance = $modal.open({
					template: require('../../29product/views/skprofit.html'),
					controller: 'skprofit',
					size: 'lg',
					resolve: {
                        item: function () {
                            return $scope.item;
                        },
                        id: function () {
                            return id;
                        },
                        code: function () {
                            return code;
                        },
                        name: function () {
                            return name;
                        },
                        guide_price: function () {
                            return guide_price;
                        },
                        cost_price: function () {
                            return cost_price;
                        },
                        productflag: function () {
                            return productflag;
                        },
                        what: function () {
                            return 'edit';
                        },
					}
				});
				modalInstance.result.then(function (showResult) {	
						$scope.pageChanged();
				});
            }
}
