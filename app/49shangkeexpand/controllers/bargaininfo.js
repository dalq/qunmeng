module.exports = function($scope, $state, $stateParams,$resource,$modal,FileUploader,toaster){
    var active_id = $stateParams.active_id;
    console.log(active_id); 
     /* 分页
     * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10         //每页显示几条
    $scope.getlist = function(){
        var para = {
            pageNo:$scope.bigCurrentPage, 
            pageSize:$scope.itemsPerPage,
            'active_id' : active_id
        }
        $resource('/api/as/ac/bargaingoods/findBargainHelpInfoList', {}, {}).                                
        save(para,function (res) {
            if (res.errcode!=0) {
                toaster.error({title: "", body:res.errmsg});
                return;
            }
            console.log(res);
            $scope.objs = res.data.results;
            $scope.bigTotalItems = res.data.totalRecord;
        });
    }
    $scope.getlist();
    $scope.seeheadimg = function(pimg){
        var modalInstance = $modal.open({
            template: require('../../45shangke/views/skacountpicture.html'),
            controller: 'skacountpicture',
            size: 'lg',
            resolve: {
                pimg: function () {
                    return pimg;
                }
            }
        });
        modalInstance.result.then(function (showResult) {	
            $scope.getlist();
        });
    }
    // $scope.selectgood = {
    //     'flag' : ''
    // } 
    // if(id){
    //     $scope.selectgood.flag = '0';
    // }   
    // $scope.info = {
    //     'sale_code' : '',
    //     'sale_name' : ''
    // }
    // $scope.searchform = {
    //     'selected' :{
    //         'name' : ''
    //     }
    // };
    // $scope.vm = {
    //     'date' : date2str(new Date()),
    //     'options' : {
    //         format: "YYYY-MM-DD",
    //         locale : 'zh-cn'
    //     }
    // }
    // $scope.vm1 = {
    //     'date' : date2str(new Date()),
    //     'options' : {
    //         format: "YYYY-MM-DD",
    //         locale : 'zh-cn'
    //     }
    // }
    // var imgarray = [];
    // var imgstr = '';
    // $scope.getsalelist = function() {///api/as/tc/sale/alllist
    //     $resource('/api/as/tc/sale/alllist', {}, {}).
    //     save({'sale_category' : 'S14'},function(res) {
    //         if (res.errcode!=0) {
    //             toaster.success({title: "", body:res.errmsg});
    //             return;
    //         }
    //         console.log(res);
    //         $scope.datas = res.data;
    //         var array = res.data;
    //         // 获得奖品详情
    //         if (id) {
    //             $resource('/api/as/ac/bargaingoods/getBargainGoods', {}, {}).                
    //             save({'id':id},function(res) {
    //                 if (res.errcode!=0) {
    //                     toaster.error({title: "", body:res.errmsg});
    //                     return;
    //                 }           
    //                 $scope.info = res.data;
    //                 $scope.info.market_price = res.data.market_price/100;
    //                 $scope.info.price = res.data.price/100;
    //                 $scope.vm.date = str2date(res.data.sale_start_time);
    //                 $scope.vm1.date = str2date(res.data.sale_end_time);
                    
    //                 console.log('详情');
    //                 console.log(res);
    //                 for (var i = 0; i < array.length ; i++) {
    //                     var codeStr = array[i].code;
    //                     //console.log(codeStr);
    //                     if (res.data.sale_code==codeStr) {
    //                         $scope.searchform.selected.name = array[i].name;
    //                         return;
    //                     }
    //                 }
                    
    //             })
    //         } 
    //     });
        
    
    // };

    // $scope.getsalelist();
    
    // // 详情图片
    // var uploader = $scope.uploader = new FileUploader({
    //     url: 'http://cl.juyouhx.com/oss.php/oss/webuploader1?topdir=aa&selfdir=bb'
    // });

    // uploader.filters.push({
    //     name: 'imageFilter',
    //     fn: function(item /*{File|FileLikeObject}*/, options) {
    //         var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
    //         return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
    //     }
    // });
    
    
    // uploader.onSuccessItem = function(fileItem, response, status, headers) {   
    //     // imgarray.splice(0,1,response.savename);
    //     // console.log(imgarray);
    //     // $scope.info.img1 = response.savename;
    //     // imgstr = imgarray.join(","); 
    //     // console.log(imgstr);
    //     // $scope.info.img = imgstr; 
    //     $scope.info.img = response.savename;
    //     // $scope.info.img1 = response.savename;
    //     // imgarray.push($scope.info.img1);
    //     // console.log(imgarray);
    //     // imgstr = imgarray.split(',');
    //     // console.log(imgstr);
    // };

    // // 奖品logo
    // var uploader1 = $scope.uploader1 = new FileUploader({
    //     url: 'http://cl.juyouhx.com/oss.php/oss/webuploader1?topdir=aa&selfdir=bb'
    // });

    // uploader1.filters.push({
    //     name: 'imageFilter',
    //     fn: function(item /*{File|FileLikeObject}*/, options) {
    //         var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
    //         return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
    //     }
    // });    
    // uploader1.onSuccessItem = function(fileItem, response, status, headers) {
    //     $scope.info.logo = response.savename; 

    // };
    
    // $scope.ok = function () {
    //     $scope.info.sale_code = $scope.searchform.selected.code;
    //     $scope.info.sale_name = $scope.searchform.selected.name;
    //     $scope.info.sale_start_time = date2str($scope.vm.date._d);
    //     $scope.info.sale_end_time = date2str($scope.vm1.date._d);
    //     $scope.info.market_price = $scope.info.market_price * 100;
    //     $scope.info.price = $scope.info.price * 100;
    //     // 编辑奖品
    //     if (id) {
    //         if ($scope.info.price!=''&&$scope.info.market_price!='') {
    //             var para = {
    //                 'id' : id
    //             }
    //             para = angular.extend($scope.info,para);
    //             console.log(para);
    //             $resource('/api/as/ac/bargaingoods/updateBargainGoods', {}, {}).                
    //             save(para,function(res) {
    //                 if (res.errcode!=0) {
    //                     toaster.error({title: "", body:res.errmsg});
    //                     return;
    //                 }
    //                 console.log(res);
    //                 toaster.success({title: "", body:"修改成功"});
    //                 $modalInstance.close();
    //             });
    //         } else {
    //             toaster.success({title: "", body:"请将数据补充完整"});
    //         }           
    //     } else {
    //         if ($scope.info.sale_start_time!=''&&$scope.info.sale_end_time!='') {
    //             // 添加奖品              
    //             $resource('/api/as/ac/bargaingoods/saveBargainGoods', {}, {}).                                
    //             save($scope.info,function (res) {
    //                 console.log($scope.info);
    //                 if (res.errcode!=0) {
    //                     toaster.error({title: "", body:res.errmsg});
    //                     return;
    //                 }
    //                 console.log(res);
    //                 toaster.success({title: "", body:'添加成功'});                    
    //                 $modalInstance.close();
    //             });
    //         } else {
    //             toaster.error({title: "", body:'请将数据补充完整'});                
    //         }      
    //     }
        
    // };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

    // function time2str(objDate) {
    //     if(angular.isDate(objDate))
    //     {
    //         var y = objDate.getFullYear();
    //         var m = objDate.getMonth();
    //         var d = objDate.getDate();
    //         var h = objDate.getHours();
    //         var mt = objDate.getMinutes();
    //         // var s = objDate.getSeconds();
    //         return y + '-' + (m + 1) + '-' + d + ' ' + h + ':' + mt;
    //     }
    //     else
    //     {
    //         return '错误格式';
    //     }
    // }


};