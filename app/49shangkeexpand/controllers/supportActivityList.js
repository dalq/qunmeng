module.exports = function($scope, $state, $stateParams,$resource,$modal,toaster){  
    /* 分页
     * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10         //每页显示几条

    $scope.getlist = function () {
        var para = {
            pageNo:$scope.bigCurrentPage, 
            pageSize:$scope.itemsPerPage,
        };
        $resource('/api/as/ac/dz/findDZList', {}, {}).
        save(para,function (res) {
            if (res.errcode != 0) {
                toaster.error({title:"",body:res.errmsg});
                return;
            }
            console.log(res);
            $scope.objs = res.data.results;
            $scope.bigTotalItems = res.data.totalRecord;
            $scope.objs.forEach(function(element) {
                element.isSelected = element.state=='0'?true:false;
            }, this);
        });   
    };
    $scope.getlist();
    $scope.obj = {
        'isSelected' : 'false'
    }
    $scope.onChange = function(isSelected,id){
        console.log(isSelected);    
       if(isSelected==false){
        $resource('/api/as/ac/dz/updateClose', {}, {}).           
            save({'id':id},function(res){
                if(res.errcode!=0){
                        toaster.error({title: "", body:res.errmsg});
                        return;
                }
                console.log(res);
                $scope.getlist();
                return;
            });
       } else {
        $resource('/api/as/ac/dz/updateOpen', {}, {}).                      
            save({'id':id},function(res){
                if(res.errcode!=0){
                    toaster.error({title: "", body:res.errmsg});
                    return;
                }
                console.log(res);
                $scope.getlist();
            });                           
            
       }
    }

    // 添加奖品
    $scope.add = function(id){
        var modalInstance = $modal.open({
            template: require('../views/addSupportActivity.html'),
            controller: 'addSupportActivity',
            size: 'lg',
            resolve: {
                id : function(){
                    return id;
                },
            }
        });
        modalInstance.result.then(function (showResult) {	
            $scope.getlist();
        });
        
    };

    // 编辑奖品
    $scope.edit = function (id) {
        var modalInstance = $modal.open({
          template: require('../views/addSupportActivity.html'),
          controller: 'addSupportActivity',
          size: 'lg',
          resolve: {
            id : function(){
                return id;
            },
           
            
          }
        });

        modalInstance.result.then(function () {
          $scope.getlist();
          
        }, function () {
          //$scope.load();
        });
    };

    $scope.delete = function(id){
        if(confirm('确定要删除该活动吗?')){
            $resource('/api/as/ac/dz/updateDel', {}, {}).                                
            save({'id' : id},function (res) {
                console.log($scope.info);
                if (res.errcode!=0) {
                    toaster.error({title: "", body:res.errmsg});
                    return;
                }
                toaster.success({title: "", body:'删除成功'});  
                $scope.getlist();                  
            });
        }
    }

   
};