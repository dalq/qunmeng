module.exports = function($scope, $state, $stateParams,$resource,$modal,toaster){   
	/* 分页
        * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10         //每页显示几条

    $scope.info = {};

    $scope.statusarr = [{'label' : '已解绑', 'value' : '0'},{'label' : '已绑定', 'value' : '1'}];
    $scope.getlist = function () {
        var para = {
            pageNo:$scope.bigCurrentPage, 
            pageSize:$scope.itemsPerPage
        };
        para = angular.extend($scope.info, para);
        $resource('/api/as/uc/userwxsk/findWxUserBindingList', {}, {}).
        save(para,function(res) {
            console.log(para);
            if (res.errcode !== 0) {
                toaster.error({title:"",body:res.errmsg});
                return;
            }
            console.log(para);
            console.log(res);
            $scope.objs = res.data.results;
            $scope.bigTotalItems = res.data.totalRecord;
             $scope.objs.forEach(function(element) {
                element.isSelected = element.status=='1'?false:true;
            }, this);

            
        })
    };
     
    $scope.getlist();
    $scope.obj = {
        'isSelected' : ''
    }
    var chk = document.getElementById('swifkda');//通过getElementById获取节点
    $scope.onChange = function(isSelected,user_id,company_user_name,status){
        console.log(isSelected);  
        // 恢复  
       if(isSelected==false){
        $resource('/api/ac/uc/userWxSkService/updateRecover', {}, {}).           
            save({'user_id':user_id, 'company_user_name' : company_user_name},function(res){
                if(res.errcode == 9999){
                    toaster.error({title: "", body:"huifukaiuguan"});
                    chk.checked = true;
                } else if(res.errcode != 0){
                    toaster.error({title: "", body:res.errmsg});
                } else {
                    toaster.success({title: "", body:"恢复绑定成功"});
                    $scope.getlist();
                }
            });
       } else {
        // 解绑
        $resource('/api/as/uc/userwxsk/updateWxSkStatus', {}, {}).                      
            save({'user_id':user_id, 'company_user_name' : company_user_name},function(res){
                if(res.errcode!=0){
                    toaster.error({title: "", body:res.errmsg});
                    return;
                }
                toaster.success({title: "", body:"解绑成功"});
                $scope.getlist();
                return;
            });                           
            
       }
    }


   
 

};