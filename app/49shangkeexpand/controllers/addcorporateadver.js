module.exports = function($scope, $state, $stateParams, $modalInstance,$resource,$modal,FileUploader,toaster,id,ad_id){
    // 企业id 广告id
    console.log(id,ad_id); 
    $scope.info = {
        'corporate_id' : id
    }
    if(ad_id){
        $resource('/api/as/ac/corporate/getCorporateAdver', {}, {}).                
        save({'id' : ad_id},function(res) {
            if (res.errcode!=0) {
                toaster.error({title: "", body:res.errmsg});
                return;
            }
            console.log(res);
            $scope.info = res.data;

        });
    }
   
     // 二维码
    var uploader1 = $scope.uploader1 = new FileUploader({
        url: 'http://cl.juyouhx.com/oss.php/oss/webuploader1?topdir=aa&selfdir=bb'
    });

    uploader1.filters.push({
        name: 'imageFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });    
    uploader1.onSuccessItem = function(fileItem, response, status, headers) {
        $scope.info.url = response.savename; 

    };
    $scope.ok = function () {       
        // 编辑企业广告
        if (ad_id) {
            var para = {
                'id' : ad_id
            }
            para = angular.extend($scope.info,para);
            console.log(para);
            $resource('/api/as/ac/corporate/updateCorporateAdver', {}, {}).                
            save(para,function(res) {
                if (res.errcode!=0) {
                    toaster.error({title: "", body:res.errmsg});
                    return;
                }
                console.log(res);
                toaster.success({title: "", body:"修改成功"});
                $modalInstance.close();
            });
        } else {
            // 添加企业广告             
            $resource('/api/as/ac/corporate/saveCorporateAdver', {}, {}).                                
            save($scope.info,function (res) {
                console.log($scope.info);
                if (res.errcode!=0) {
                    toaster.error({title: "", body:res.errmsg});
                    return;
                }
                console.log(res);
                toaster.success({title: "", body:'添加成功'});                    
                $modalInstance.close();
            });
        }
        
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

    

};