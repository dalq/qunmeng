module.exports = function($scope, $state, $stateParams,$resource,$modal,toaster,date2str, str2date){   
	/* 分页
        * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10         //每页显示几条
    
    $scope.getlist = function () {
        var para = {
            pageNo:$scope.bigCurrentPage, 
            pageSize:$scope.itemsPerPage
        };
        // para = angular.extend($scope.parameters, para);
        $resource('/api/as/ac/activeSeeAd/findSeeAdList', {}, {}).
        save(para,function(res) {
            console.log(para);
            if (res.errcode !== 0) {
                toaster.error({title:"",body:res.errmsg});
                return;
            }
            console.log(para);
            console.log(res);
            $scope.objs = res.data.results;
            $scope.bigTotalItems = res.data.totalRecord;
            $scope.objs.forEach(function(element) {
                element.isSelected = element.see_state=='1'?true:false;
            }, this);

            
        })
    };
     
    $scope.getlist();

    $scope.add = function(item) {
        var modalInstance = $modal.open({
        template: require('../views/addseeadver.html'),
        controller: 'addseeadver',
        size: 'lg',
        resolve: {
            item: function () {
                return item;
            },
            date2str : function(){
                return date2str;
            },
            str2date: function () {
                return str2date;
            },
         }
        });
        modalInstance.result.then(function (showResult) {	
            $scope.getlist();
        });
    };
    $scope.delete = function(id) {
        if (confirm('确定要删除吗?')) {
            $resource('/api/as/ac/activeSeeAd/updateSeeAdDel', {}, {}).            
            save({'id':id},function(res) {
                console.log({'id':id});
                if (res.errcode !== 0) {
                    toaster.error({title:"",body:res.errmsg});
                    return;
                } 
                toaster.success({title:"",body:"删除成功"});                
                $scope.getlist();
            })
            return;
        } 
        
    };

    $scope.edit = function(item) {
        var modalInstance = $modal.open({
        template: require('../views/addseeadver.html'),
        controller: 'addseeadver',
        size: 'lg',
        resolve: {
            item: function () {
                return item;
            },
            date2str : function(){
                return date2str;
            },
            str2date: function () {
                return str2date;
            },
         }
        });
        modalInstance.result.then(function (showResult) {	
            $scope.getlist();
        });
    };

    $scope.typeadlist = function(ad_type,id){
        var modalInstance = $modal.open({
        template: require('../views/addadver.html'),
        controller: 'addadver',
        size: 'lg',
        resolve: {
            ad_type: function () {
                return ad_type;
            },
            id: function () {
                return id;
            },
         }
        });
        modalInstance.result.then(function (showResult) {	
            $scope.getlist();
        });
    }

    $scope.getaddress = function(id){
        var flag = 'ad';
        var modalInstance = $modal.open({
            template: require('../views/adgetaddress.html'),
            controller: 'adgetaddress',
            size: 'lg',
            resolve: {
                id: function () {
                    return id;
                },
                flag: function () {
                    return flag;
                },
            }
            });
            modalInstance.result.then(function (showResult) {	
                $scope.getlist();
            });
    }

    $scope.obj = {
        'isSelected' : 'false'
    }
    $scope.onChange = function(isSelected,id,state){
        console.log(isSelected);    
       if(isSelected==false){
        $resource('/api/as/ac/activeSeeAd/updateEndState', {}, {}).           
            save({'id':id},function(res){
                if(res.errcode!=0){
                        toaster.error({title: "", body:res.errmsg});
                        return;
                }
                console.log(res);
                $scope.getlist();
                return;
            });
       } else {
        $resource('/api/ac/ac/activeSeeAdService/updateSeeAdState', {}, {}).                      
            save({'id':id},function(res){
                if(res.errcode!=0){
                    toaster.error({title: "", body:res.errmsg});
                    return;
                }
                console.log(res);
                $scope.getlist();
                return;
            });                           
            
       }
    }

    

 

};