module.exports = function($scope, $stateParams, $state, $modal, $modalInstance,FileUploader,toaster,$resource,item,date2str,str2date){  
    console.log(item);
    $scope.vm = {
        'date' : '',
        'options' : {
            format: "YYYY-MM-DD",
            locale : 'zh-cn'
        }
    }
    $scope.vm1 = {
        'date' : '',
        'options' : {
            format: "YYYY-MM-DD",
            locale : 'zh-cn'
        }
    }
    $scope.subsidySearchform = {
      'selected' : {
        'subsidy_id' : ''
      }
    }
    var uploader1 = $scope.uploader1 = new FileUploader({
        url: 'http://cl.juyouhx.com/oss.php/oss/webuploader1?topdir=aa&selfdir=bb'
    });

    uploader1.filters.push({
        name: 'imageFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });    
    uploader1.onSuccessItem = function(fileItem, response, status, headers) {
        $scope.info.top_img = response.savename; 

    };
    
    
    // 详情图片
    var uploader = $scope.uploader = new FileUploader({
        url: 'http://cl.juyouhx.com/oss.php/oss/webuploader1?topdir=aa&selfdir=bb'
    });

    uploader.filters.push({
        name: 'imageFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });
    
    uploader.onSuccessItem = function(fileItem, response, status, headers) {   
        $scope.info.body_img = response.savename;
    };
     // 活动补贴列表
     $scope.getsubsidylist = function () {
      $resource('/api/as/puc/subsidy/list', {}, {}).
      save({},function(res) {
          if (res.errcode!=0) {
              toaster.success({title: "", body:res.errmsg});
              return;
          }
          console.log('~~~~~~~~活动补贴~~~~~');
          console.log(res);
          $scope.subsidyarr = res.data;
          console.log(item.subsidy_id);
          $scope.subsidyarr.forEach(element => {
            if(item.subsidy_id === element.subsidy_id){
              $scope.subsidySearchform.selected.subsidy_name = element.subsidy_name;
            }
          });
      })
    }
    $scope.getsubsidylist();
    $scope.item = item;
    if(item){
        $scope.info = item;
        $scope.vm.date = item.start_time;
        $scope.vm1.date = item.end_time;
        var id = item.id;
    }
    
    $scope.ok = function(){
        if ( $scope.vm.date === null) {
            $scope.vm.date = '';
        }
        if ( $scope.vm1.date === null) {
            $scope.vm1.date = '';
        }
        if (typeof $scope.vm.date._d === 'string') {
            $scope.vm.date._d = $scope.vm.date._d;
        } else {
            $scope.vm.date._d = date2str($scope.vm.date._d);
        }
        if (typeof $scope.vm1.date._d === 'string') {
            $scope.vm1.date._d = $scope.vm1.date._d;
        } else {
            $scope.vm1.date._d= date2str($scope.vm1.date._d);
        }
        console.log($scope.subsidySearchform.selected);
        $scope.info.subsidy_id = $scope.subsidySearchform.selected.subsidy_id;
        if(id){
            var para = {
                'id' : id,
                'start_time' : $scope.vm.date._d,
                'end_time' : $scope.vm1.date._d
            }
            para = angular.extend($scope.info, para);
            $resource('/api/as/ac/activeSeeAd/updateSeeAd', {}, {}).
            save(para,function(res){
                console.log($scope.para);
                if(res.errcode != 0){
                    toaster.error({title: "", body:res.errmsg});
                    return;
                }
                console.log(res);
                toaster.success({title: "", body:'修改成功'});                    
                $modalInstance.close();
            })
        } else {
            var para = {
                'start_time' : $scope.vm.date._d,
                'end_time' : $scope.vm1.date._d
            }
            para = angular.extend($scope.info, para);
            $resource('/api/as/ac/activeSeeAd/insertSeeAd', {}, {}).
            save(para,function(res){
                console.log($scope.info);
                if(res.errcode != 0){
                    toaster.error({title: "", body:res.errmsg});
                    return;
                }
                console.log(res);
                toaster.success({title: "", body:'添加成功'});                    
                $modalInstance.close();
            })
        }
    }

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

    function time2str(objDate) {
        if(angular.isDate(objDate))
        {
            var y = objDate.getFullYear();
            var m = objDate.getMonth();
            var d = objDate.getDate();
            var h = objDate.getHours();
            var mt = objDate.getMinutes();
            // var s = objDate.getSeconds();
            return y + '-' + (m + 1) + '-' + d + ' ' + h + ':' + mt;
        }
        else
        {
            return '错误格式';
        }
    }

};