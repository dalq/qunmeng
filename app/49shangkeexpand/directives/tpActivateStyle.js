module.exports = function ($resource, $state, $http, $q, FileUploader, toaster, $modal) {

	return {

		restrict: 'AE',
		template: require('../views/tpListStyle.html'),
		replace: true,
		scope: {
			'productobj': '=',
			'funobj': '=',
		},
		link: function (scope, elements, attrs) {
            console.log(scope.productobj);
            scope.infoobj = {
                'category' : scope.productobj.activate,
                'goods_id' : scope.productobj.id,
            }
            if(scope.productobj.id){
                $resource('/api/as/mc/mergoodstp/getGoodsTPInfo', {}, {})
                .save({'goods_id' : scope.productobj.id, 'category' : scope.productobj.activate},function(res){
                    if(res.errcode === 0)
                    {
                        scope.infoobj = res.data;
                        if(res.data.top_img == ''){
                            scope.array = [];
                        } else {
                            scope.array = res.data.top_img.split(",");
                        }
                    } else if(res.errcode === 10003){
                    } else {
                        toaster.error({title:"",body:res.errmsg});
                    }
                    
                    

                }) 
            }
           
            
            scope.image = function(){
				var para = $state.get('app.imageupload');
				//设置上传文件夹，以自己业务命名
				angular.extend(para, {
					resolve : {  
						'dir' : function(){
							return 't1';
						}
					} 
				})
				var modalInstance = $modal.open(para);
				modalInstance.opened.then(function() {// 模态窗口打开之后执行的函数  
				});  
				modalInstance.result.then(function(result) {  
					scope.array = result;
                    scope.infoobj.top_img=scope.array.join(',');
					
				}, function(reason) {  
					// click，点击取消，则会暑促cancel  
					$log.info('Modal dismissed at: ' + new Date());  
				}); 
            };
            scope.del=function(index){
                scope.array.splice(index,1);
                scope.infoobj.top_img=scope.array.join(',');
            }

            // 保存
            scope.save = function(){
                $resource('/api/as/mc/mergoodstp/saveGoodsTP', {}, {})
                .save(scope.infoobj,function(res){
                    if(res.errcode !== 0)
                    {
                        toaster.error({title:"",body:res.errmsg});
                        return;
                    }
                    toaster.success({title:"",body:"操作成功"});
                    
                    // $state.go('app.supplyProductList');

                }) 
            }
			// console.log('基本信息界面的id');
			// console.log(scope.saleobj.id);
			// scope.auditing = scope.util.auditing;
			// var obj = {
			// 	'id' : scope.saleobj.id
			// }
			// angular.extend(scope.saleobj, obj);
			
			// var beforedata = {
			// 	//类型列表
			// 	'categorylist':
			// 	$http({
			// 		'method': 'GET',
			// 		'url': '/api/as/sc/dict/dictbytypelist',
			// 		'params': { 'type': 'sale_category' },
			// 	}),
			// 	//产品所属
			// 	'salebelonglist':
			// 	$http({
			// 		'method': 'GET',
			// 		'url': '/api/as/sc/dict/dictbytypelist',
			// 		'params': { 'type': 'ticket_sale_belong' },
			// 	}),
			// 	//短信信息
			// 	'smslist':
			// 	$http({
			// 		'method': 'GET',
			// 		'url': '/api/as/tc/salesmstemplate/list',
			// 	}),
			// };

			// // 获得信息
			// if(scope.saleobj.id != ''){
			// 	var url = '/api/as/mc/mermakeappointmentdao/getMakeAppointmentById';
			// 	$resource(url, {}, {}).save({'id' : scope.saleobj.id}, function(res){
			// 		if(res.errcode != 0){
			// 			alert(res.errmsg);
			// 			return;
			// 		}
			// 		console.log('详情');
			// 		console.log(res);
			// 		scope.info = res.data;
			// 		// scope.saleobj.id = res.data.uuid;
					
			// 	});
			// }


			// scope.save = function(){
			// 	console.log(scope.saleobj.id);
			// 	scope.para = {
			// 		'id' : scope.saleobj.id
			// 	}
			// 	var url = '';
			// 	if(scope.saleobj.id) {
			// 		// 编辑
			// 		url = '/api/as/mc/mermakeappointmentdao/updateMakeAppointment';
			// 		scope.info = angular.extend(scope.info,scope.para);
			// 	} else {
			// 		url = '/api/as/mc/mermakeappointmentdao/insertMakeAppointment';
					
			// 	}
			// 	console.log(scope.info);
			// 	$resource(url, {}, {}).save(scope.info, function(res){
			// 		if(res.errcode != 0){
			// 			toaster.error({title:"",body:res.errmsg});
			// 			return;
			// 		}
			// 		toaster.success({title:"",body:"操作成功!"});
			// 		console.log(res);
			// 		scope.saleobj.id = res.data.uuid;
			// 		console.log('id=' + scope.saleobj.id);
					
			// 	});
			//};
        }
    }
}