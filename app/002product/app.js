var App = angular.module('newproduct', []);

App.config(require('./router'));
//App.factory('shopservice', require('./service'));

App.controller('newproductlist',require('./controllers/list'));
App.controller('qmnewproduct',require('./controllers/product'));
App.controller('qmplace',require('./controllers/place'));
App.controller('qmdevice',require('./controllers/device'));
App.controller('qmaddplace',require('./controllers/addplace'));
App.controller('qmcreateproduct',require('./controllers/create'));
App.controller('qmcommission',require('./controllers/commission'));
App.controller('qmposter',require('./controllers/poster'));

// App.directive('placebaseinfo',require('./directives/baseinfo'));
// App.directive('placeview',require('./directives/view'));
// App.directive('placestore',require('./directives/store'));


App.directive('productlist',require('./directives/productlist'));
App.directive('productbase',require('./directives/base'));
App.directive('productrestricted',require('./directives/restricted'));
App.directive('productorder',require('./directives/order'));
App.directive('productticket',require('./directives/ticket'));
App.directive('productimgtext',require('./directives/imgtext'));
App.directive('productcommission',require('./directives/commission'));
App.directive('productposter',require('./directives/poster'));
App.directive('productshopconfig',require('./directives/shop'));
App.directive('productmodel',require('./directives/model'));
App.directive('productdocking',require('./directives/docking'));
App.directive('productcalendar',require('./directives/pricecalendar'));
App.directive('productmail',require('./directives/mail'));




// App.controller('list',require('./controllers/list'));

module.exports = App;