module.exports = function ($resource, $state, $http, $q, $modal, dlq, $timeout) {
	return {
		restrict: 'AE',
		template: require('../views/p_imgtext.html'),
		replace: true,
		scope: {
			'p' : '=',
			'close': '=',
			//'state' : '=',
			//'vm' : '=',
		},
		link: function (scope, elements, attrs) {


//推广图片
scope.imagearr = [];

//推广文字数组
scope.textarr = [];

//推广文字
scope.text = '';

//预定须知
scope.bookingnotes = scope.p.bookingnotes || '';

//商品详情
scope.detailarr = [];

//详情类型：1，文字，2，图片
scope.detailType = '1';

//详情文字
scope.detailTxt = '';



if(scope.p.top_pic){
	//scope.imagearr = scope.p.top_pic.split(',');
	//scope.imagearr =  angular.fromJson(scope.p.top_pic);
	try{
		scope.imagearr =  angular.fromJson(scope.p.top_pic);
		
	}catch (err){
		scope.imagearr = [];
	}
	console.log(scope.imagearr);
}

if(scope.p.words){
	scope.textarr = scope.p.words.split(',');
}

if(scope.p.detail){
	scope.detailarr = angular.fromJson(scope.p.detail);
}



//-------------------- 顶图 --------------------------//
scope.setImage = function () {

	var para = $state.get('app.imageupload');
	//设置上传文件夹，以自己业务命名

	para.resolve.dir = function(){
        return 'weshop_toppic';
    };

	console.log(para);

    var modalInstance = $modal.open(para);
    modalInstance.opened.then(function() {// 模态窗口打开之后执行的函数  
    });  
    modalInstance.result.then(function(result) {  

    	console.log(result);
    	for(var i = 0; i < result.length; i++){
 
			var obj = {
				'url' : result[i],
				'w' : 0,
				'h' : 0,
			};
    		scope.imagearr.push(obj);
    	}

    }, function(reason) {  
        // click，点击取消，则会暑促cancel  
        $log.info('Modal dismissed at: ' + new Date());  
    }); 

}
	
scope.imageDel = function(index){
	scope.imagearr.splice(index, 1);
};

scope.imageDown = function(index){
	scope.imagearr[index + 1] = scope.imagearr.splice(index, 1, scope.imagearr[index + 1])[0];
};

scope.imageUp = function(index){
	scope.imagearr[index - 1] = scope.imagearr.splice(index, 1, scope.imagearr[index - 1])[0];
};

//-------------------- 顶图 --------------------------//

//------------------- 推广文字 ------------------------//
scope.setText = function(){
	scope.textarr.push(scope.text);
	scope.text = '';
}

scope.del_text = function(index){
	scope.textarr.splice(index, 1);
}

scope.textDown = function(index){
	scope.textarr[index + 1] = scope.textarr.splice(index, 1, scope.textarr[index + 1])[0];
}

scope.textUp = function(index){
	scope.textarr[index - 1] = scope.textarr.splice(index, 1, scope.textarr[index - 1])[0];
}
//-------------------- 推广文字 ------------------------//


//---------- 商品详情 --------------------------//
scope.setDetailImage = function () {

	var para = $state.get('app.imageupload');
	//设置上传文件夹，以自己业务命名
	para.resolve.dir = function(){
        return 'weshop_detail';
    };

    var modalInstance = $modal.open(para);
    modalInstance.opened.then(function() {// 模态窗口打开之后执行的函数  
    });  
    modalInstance.result.then(function(result) {  

    	console.log(result);
    	for(var i = 0; i < result.length; i++){
    		var obj = {
    			'type' : 'image',
    			'value' : result[i],
    		}
    		scope.detailarr.push(obj);
    	}

    }, function(reason) {  
        // click，点击取消，则会暑促cancel  
        $log.info('Modal dismissed at: ' + new Date());  
    }); 

}


scope.setDetail = function(txt){

	var obj = {
		'type' : 'text',
		'value' : txt,
	};

	scope.detailarr.push(obj);
	console.log(scope.detailarr);
};


scope.detailDel = function(index){
	scope.detailarr.splice(index, 1);
};

scope.detailDown = function(index){

	scope.detailarr[index + 1] = scope.detailarr.splice(index, 1, scope.detailarr[index + 1])[0];
};

scope.detailUp = function(index){

	scope.detailarr[index - 1] = scope.detailarr.splice(index, 1, scope.detailarr[index - 1])[0];
};
//---------- 商品详情 --------------------------//



	scope.gogo = function(){

		//顶图要把图片宽度高度也存进去。
		var image_txt = '';
		for(var i = 0, j = scope.imagearr.length; i < j; i++){
			//image_txt += scope.imagearr[i] + (i == j - 1 ? '' : ',');
			var tmp = scope.imagearr[i];
			var theImage = new Image();  
            theImage.src = tmp.url;
            tmp.w = theImage.width;
            tmp.h = theImage.height;
		}

		image_txt = angular.toJson(scope.imagearr);

		console.log(image_txt);

		var text_txt = '';
		for(var i = 0, j = scope.textarr.length; i < j; i++){
			text_txt += scope.textarr[i] + (i == j - 1 ? '' : ',');
		}

		var para = {
			'id' : scope.p.id,
			'sale_code' : scope.p.sale_code,
			'top_pic' : image_txt,
			'words' : text_txt,
			'bookingnotes' : scope.bookingnotes,
			'detail' : angular.toJson(scope.detailarr),
		}

		console.log(para);

		$resource('/api/ac/wc/newProductService/createProduct', {}, {}).save(para, function(res){
			console.log(res);
			if (res.errcode !== 0) {
				alert(res.errmsg);
				return;
			}
			
			alert('操作成功');
		});
	}

			
		}
	};
};

