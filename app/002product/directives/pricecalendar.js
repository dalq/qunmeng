module.exports = function ($resource, $state, $http, $q, $modal, dlq, $timeout, $rootScope) {
	return {
		restrict: 'AE',
		template: require('../views/p_calendar.html'),
		replace: true,
		scope: {
			'p' : '=',
			'load': '=',
			//'state' : '=',
			//'vm' : '=',
		},
		link: function (scope, elements, attrs) {

	scope.vm = {
        'options' : {
            format: "YYYY-MM-DD",
            locale : 'zh-cn',
            ignoreReadonly : true,
        }
    };

    scope.weekDay = [
    	{ day: '日', value: true },
        { day: '一', value: true },
        { day: '二', value: true },
        { day: '三', value: true },
        { day: '四', value: true },
        { day: '五', value: true },
        { day: '六', value: true },
    ];

    scope.now = new Date();

    scope.showattrarr = [
		{
			'key' : 'guide_price',
			'position' : 'left',
			'before' : '卖价：¥'
		},
		{
			'key' : 'cost_price',
			'position' : 'left',
			'before' : '成本：¥'
		},
		{
			'key' : 'current_stock_num',
			'position' : 'left',
			'before' : '库存：',
			//'after' : '个',
		}
	];


    scope.save = function(item){
    	console.log(item);

    	if(item.calendar.guide_price <= 0){
    		alert('请输入正确商城价格');
    		return;
    	}
    	if(item.calendar.cost_price < 0){
    		alert('请输入正确成本价格');
    		return;
    	}
    	if(item.calendar.current_stock_num <= 0){
    		alert('请输入正确库存');
    		return;
    	}

    	var s = item.calendar.s.format('YYYY-MM-DD');
    	var e = item.calendar.e.format('YYYY-MM-DD');


        



    	var datearr = dataScope(s, e);
    	var week = item.calendar.week;
    	var result = [];
    	for(var i = 0; i < datearr.length; i++){
    		var tmp = datearr[i];
    		var day = new Date(tmp).getDay();
    		if(!week[day]){
    			continue;
    		}
    		var p = {
    			'attr_id' : item.attr_id,
    			'date' : tmp,
    			'sale_code' : item.sale_code,
    			'market_price' : 0,
    			'guide_price' : item.calendar.guide_price,
    			'cost_price' : item.calendar.cost_price,
    			'current_stock_num' : item.calendar.current_stock_num,
    		};
    		//查看这一天是否已经有数据了
    		if(angular.isDefined(item.calendar.flag[tmp])){
    			p['id'] = item.calendar.flag[tmp].id;
    		}
    		result.push(p);
    	}

    	console.log(result);

    	$resource('/api/ac/wc/newProductService/createDayPrice', {}, {}).save({
    		'daylist' : result
    	}, function(res){
			console.log(res);
			if (res.errcode !== 0) {
				alert(res.errmsg);
				return;
			}
			scope.load();
			alert('操作成功');
		});


    };



    scope.del = function(item){
    	
    	if(!confirm("您确认要删除这段时间的价格吗？")) {
	        return;
	    }
    	console.log(item);
    	var s = item.calendar.s.format('YYYY-MM-DD');
    	var e = item.calendar.e.format('YYYY-MM-DD');
    	var datearr = dataScope(s, e);

    	console.log(datearr);

    	var week = item.calendar.week;
    	var result = [];
    	for(var i = 0; i < datearr.length; i++){
    		var tmp = datearr[i];
    		var day = new Date(tmp).getDay();
    		if(!week[day]){
    			continue;
    		}
    		
    		//查看这一天是否已经有数据了
    		if(angular.isDefined(item.calendar.flag[tmp])){
    			var p = {
	    			'attr_id' : item.attr_id,
	    			'date' : tmp,
	    			'sale_code' : item.sale_code,
	    			'id' : item.calendar.flag[tmp].id,
	    			'state' : '1',
	    		};
	    		result.push(p);
    		}
    		
    	}

    	console.log(result);

    	$resource('/api/ac/wc/newProductService/createDayPrice', {}, {}).save({
    		'daylist' : result
    	}, function(res){
			console.log(res);
			if (res.errcode !== 0) {
				alert(res.errmsg);
				return;
			}
			scope.load();
			alert('操作成功');
		});


    };


    //每一天的点击事件
	scope.clickday = function(item){
		console.log(item);
	};

	scope.refresh = function(){};


	scope.cancel = function(){

		scope.close();

	}

	//返回两个日期之间的所有日期
	function dataScope (value1, value2) {  
	    
	    var date1 = new Date(value1);
	    var date2 = new Date(value2);
	    // console.log(date1);
	    // console.log(date2);

	    if (date1 > date2) {  
	        var tempDate = date1;  
	        date1 = date2;  
	        date2 = tempDate;  
	    }  
	    date2.setDate(date2.getDate() + 1);  
	    var dateArr = [];  

	    // console.log('月份比较');
	    // console.log(date1.getFullYear() + '-' + parseInt(date1.getMonth())+1 + '-' + date1.getDate());
	    // console.log(date2.getFullYear() + '-' + parseInt(date2.getMonth())+1 + '-' + date2.getDate());

	    while (!(date1.getFullYear() == date2.getFullYear()  
	            && date1.getMonth() == date2.getMonth() 
	            && date1.getDate() == date2.getDate())) {  

	        var dayStr =date1.getDate().toString();
            if(dayStr.length ==1){  
                dayStr="0"+dayStr;  
            }  
            var month = (date1.getMonth() + 1).toString();
            if(month.length == 1) month = '0' + month;
	        dateArr.push(date1.getFullYear() + "-" + 
	        			month + "-"  
	                	+ dayStr);  
	        date1.setDate(date1.getDate() + 1);  
	    }  
	    return dateArr;  
	}		

		}
	};
};

