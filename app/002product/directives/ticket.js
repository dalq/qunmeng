module.exports = function ($resource, $state, $http, $q, $modal, dlq, $timeout) {
	return {
		restrict: 'AE',
		template: require('../views/p_ticket.html'),
		replace: true,
		scope: {
			'p' : '=',
			'load': '=',
			//'state' : '=',
			//'vm' : '=',
		},
		link: function (scope, elements, attrs) {

		    scope.vm1 = {
		        'options' : {
		            format: "YYYY-MM-DD",
		            locale : 'zh-cn',
		            ignoreReadonly : true,
		            //showClear: true ,                       
		           // clearBtn:true
		        }
		    };

		    scope.ticket = {
		    	'name' : scope.p.name,
		    	'appoint': '0',
		    };

		    scope.place_code_array = [];

		    scope.s = new Date();
		    var lastday = scope.s.getFullYear() + '-12-31';
		    scope.e = new Date(lastday);
		    
		    //商家列表
		    function getPlaceList(){
		    	//$resource('/api/as/wc/productbussiness/nobussinesslist', {}, {}).save({}, function(res){
		    	$resource('/api/as/wc/productbussiness/storeselectlist', {}, {}).save({}, function(res){
					if (res.errcode !== 0) {
						alert(res.errmsg);
						return;
					}
					scope.view_array = res.data;
				});
		    }
		    getPlaceList();

		    //添加票种。
			scope.addTicket = function(){
				console.log(scope.ticket.place_code_array);

				if(angular.isUndefined(scope.ticket.place_code_array)
				||	scope.ticket.place_code_array.length == 0){
					alert('请选择一个商家');
					return;
				}

				var pc = scope.ticket.place_code_array[0];
				for(var i = 1; i < scope.ticket.place_code_array.length; i++){
					pc += ',' + scope.ticket.place_code_array[i];
				}

				var para = {
					'name' : scope.ticket.name,
					'sale_code' : scope.p.sale_code,
					'place_code' : pc,
					'periodstart' : scope.s.format('YYYY-MM-DD'),
					'periodend' : scope.e.format('YYYY-MM-DD'),
					'type': 'M',
					'appoint': scope.ticket.appoint,
					'code': scope.p.code,
				}
				optTicket(para);
			};

			scope.delTicket = function(obj){

				if(!confirm("您确认要不出该门票吗！？")) {
		            return;
		        }

				var para = {
					'id' : obj.id,
					'code' : scope.p.code,
				};

				//$resource('/api/ac/wc/productService/updateTicketType', {}, {}).save(para, function(res){
				$resource('/api/ac/wc/productbussinessService/updateDeleteSaleType', {}, {}).save(para, function(res){
					console.log(res);
					if (res.errcode !== 0) {
						alert(res.errmsg);
						return;
					}
					scope.load();
				});
			};

			scope.recoveryTicket = function(obj){

				var para = {
					'id' : obj.id,
					'code' : scope.p.code,
				};

				optTicket(para);
			};

			function optTicket(para, fun){

				//$resource('/api/ac/wc/productService/createTicketType', {}, {}).save(para, function(res){
				$resource('/api/as/wc/productbussiness/save', {}, {}).save(para, function(res){
					console.log(res);
					if (res.errcode !== 0) {
						alert(res.errmsg);
						return;
					}
					scope.load();
					if(angular.isDefined(fun)){
						fun();
					}
				});
			}

			scope.setName = function(obj){

				var para = {
					'id' : obj.id,
					'code' : scope.p.code,
					'name' : obj.name,
				}
				optTicket(para, function(){
					obj.opt.name = obj.opt.name == '0' ? '1' : '0';
				});

			};

			scope.setPeriod = function(obj){
				var para = {
					'id' : obj.id,
					'code' : scope.p.code,
					'periodstart' : obj.periodstart_date.format('YYYY-MM-DD'),
					'periodend' : obj.periodend_date.format('YYYY-MM-DD')
				}
				optTicket(para, function(){
					obj.opt.period = obj.opt.period == '0' ? '1' : '0';
				});
			};

			scope.setAppoint = function(obj){
				var para = {
					'id' : obj.id,
					'code' : scope.p.code,
					'appoint' : obj.appoint,
				}
				optTicket(para, function(){
					obj.opt.appoint = obj.opt.appoint == '0' ? '1' : '0';

					console.log(obj.opt);
					//getTicketList();
				});
			}

			scope.delPlace = function(obj,index){

				if(!confirm("删除后，该产品以后出的票在该商家中将不能使用。")) {
		            return;
		        }

		        console.log(obj);

				obj.placelist.splice(index,1);

				//按理说永远不会执行。
				if(angular.isUndefined(obj.placelist)
				||	obj.placelist.length == 0){
					alert('至少包含一个商家');
					return;
				}

				var pc = obj.placelist[0].code;
				for(var i = 1; i < obj.placelist.length; i++){
					pc += ',' + obj.placelist[i].code;
				}

				var para = {
					'id' : obj.id,
					'code' : scope.p.code,
					'place_code' : pc,
				}
				optTicket(para);
			};

			//创建商家
			scope.createPlace = function(place){

				var para = {};

				if(angular.isUndefined(place)){
					para = {
	                	'type' : 'M'
	                };
				}else{
					para = place;
				}

				console.log('创建商家');

				var modalInstance = $modal.open({
		          template: require('../views/pop_createplace.html'),
		          controller: 'qmplace',
		          //size: 'lg',
		          resolve: {
		            obj : function(){
		                return para;
		            }
		          }
		        });

		        modalInstance.result.then(function () {
		            getPlaceList();
		            scope.load();
		        }, function () {
		            
		        });
			};


			//创建设备
			scope.createDevice = function(place, device){

				console.log(place);
				console.log(device);

				var para = {};

				if(angular.isDefined(device)){
					para = device;
				}

				para['view'] = place.code;

				console.log('创建设备');

				var modalInstance = $modal.open({
		          template: require('../views/pop_createdevice.html'),
		          controller: 'qmdevice',
		          //size: 'lg',
		          resolve: {
		            obj : function(){
		                return para;
		            }
		          }
		        });

		        modalInstance.result.then(function () {
		            //getPlaceList();
		            scope.load();
		        }, function () {
		            
		        });

			};


			scope.addPlace = function(obj){

				var modalInstance = $modal.open({
		          template: require('../views/pop_addPlace.html'),
		          controller: 'qmaddplace',
		          //size: 'lg',
		          resolve: {
		            views : function(){
		                return scope.view_array;
		            },
		            obj : function(){
		            	return obj;
		            }
		          }
		        });

		        modalInstance.result.then(function () {
		            //getPlaceList();
		            scope.load();
		        }, function () {
		            
		        });

			};

			scope.changeOpt = function(obj, para){
				obj.opt[para] = obj.opt[para] == '0' ? '1' : '0';
			};

		}
	};
};

