module.exports = function ($resource, $state, $http, $q, $modal, dlq, $timeout) {
	return {
		restrict: 'AE',
		template: require('../views/p_mail.html'),
		replace: true,
		scope: {
			'p' : '=',
			'close': '=',
			//'state' : '=',
			//'vm' : '=',
		},
		link: function (scope, elements, attrs) {

scope.vm = {
    //'date' : '',
    'options' : {
        format: "YYYY-MM-DD HH:mm",
        locale : 'zh-cn',
        ignoreReadonly : true,
        //showClear: true ,                       
       // clearBtn:true
    }
};

scope.vm1 = {
    //'date' : '',
    'options' : {
        format: "YYYY-MM-DD",
        locale : 'zh-cn',
        ignoreReadonly : true,
        //showClear: true ,                       
       // clearBtn:true
    }
};

scope.label = {

	'product_attr' : [
		'',
		'电子票产品',
		'儿童剧产品',
		'实物',
	],

};


scope.p.product_attr_label = scope.label.product_attr[parseInt(scope.p.product_attr)];

scope.gogo = function(){

	var para = {
		'id' : scope.p.id,
		'sale_code' : scope.p.sale_code,
		//------------- 价格 -------------------------//
		//'guide_price' : scope.p.guide_price,
		//'cost_price' : scope.p.cost_price,
		'market_price' : scope.p.market_price,
		//------------- 价格 -------------------------//

		//------------- 属性 -------------------------//
		'back_type' : scope.p.back_type,
		//'sms_type' : scope.p.sms_type,
		//'sms_diy' : scope.p.sms_diy,
		'flash_state' : scope.p.flash_state,
		//'tour_date_type' : scope.p.tour_date_type,
		//'take_effect_type' : scope.p.take_effect_type,
		//'nopay_cancel_minute' : scope.p.nopay_cancel_minute,

		'cardno_state' : scope.p.cardno_state,
		'isshow' : scope.p.isshow,

		'postage' : scope.p.postage,
		//------------- 属性 -------------------------//
	};

	//购买时间处理
	if(scope.p.flash_state == '1'){
		if(angular.isUndefined(scope.p.start_time_date)){
			alert('请选择开始购买时间');
			return;
		}
		if(angular.isUndefined(scope.p.end_time_date)){
			alert('请选择结束购买时间');
			return;
		}

		para['start_time'] = scope.p.start_time_date.format('YYYY-MM-DD HH:mm') + ':00';
		para['end_time'] = scope.p.end_time_date.format('YYYY-MM-DD HH:mm') + ':00';
	}

	//出游时间处理
	// if(scope.p.tour_date_type == '1'){
	// 	if(angular.isUndefined(scope.p.tour_date_limit_start_date)){
	// 		alert('请选择出游开始时间');
	// 		return;
	// 	}
	// 	if(angular.isUndefined(scope.p.tour_date_limit_end_date)){
	// 		alert('请选择出游结束时间');
	// 		return;
	// 	}

	// 	para['tour_date_limit_start'] = scope.p.tour_date_limit_start_date.format('YYYY-MM-DD');
	// 	para['tour_date_limit_end'] = scope.p.tour_date_limit_end_date.format('YYYY-MM-DD');
 // 	}

	//$resource('/api/ac/wc/productService/createWdProduct', {}, {}).save(para, function(res){
	$resource('/api/ac/wc/newProductService/createProduct', {}, {}).save(para, function(res){
		console.log(res);
		if (res.errcode !== 0) {
			alert(res.errmsg);
			return;
		}
		
		alert('操作成功');

	});

};


	scope.cancel = function(){

		scope.close();

	}

		

		}
	};
};

