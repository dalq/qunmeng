module.exports = function ($resource, $state, $http, $q, $modal, dlq, $timeout) {
	return {
		restrict: 'AE',
		template: require('../views/p_restricted.html'),
		replace: true,
		scope: {
			'p' : '=',
			'close': '=',
			//'state' : '=',
			//'vm' : '=',
		},
		link: function (scope, elements, attrs) {

scope.vm = {
    //'date' : '',
    'options' : {
        format: "YYYY-MM-DD HH:mm",
        locale : 'zh-cn',
        ignoreReadonly : true,
        //showClear: true ,                       
       // clearBtn:true
    }
};

scope.vm1 = {
    //'date' : '',
    'options' : {
        format: "YYYY-MM-DD",
        locale : 'zh-cn',
        ignoreReadonly : true,
        //showClear: true ,                       
       // clearBtn:true
    }
};

scope.label = {
	'take_effect_type_array' : [
		{'name' : '次日生效', 'code' : -1},
		{'name' : '即时生效', 'code' : 0},
		{'name' : '1小时后生效', 'code' : 1},
		{'name' : '2小时后生效', 'code' : 2},
		{'name' : '4小时后生效', 'code' : 4},
		{'name' : '12小时后生效', 'code' : 12},
	],
	'nopay_cancel_minute_array' : [
		{'name' : '10分钟', 'code' : '10'},
		{'name' : '15分钟', 'code' : '15'},
		{'name' : '30分钟', 'code' : '30'},
		{'name' : '60分钟', 'code' : '60'},
	],
	'product_attr' : [
		'',
		'电子票产品',
		'儿童剧产品',
		'实物',
	],
	'sale_belong' : [
        {'label' : '自营', 'value' : 'qunmeng'},
        {'label' : '居游', 'value' : 'weshop_juyou'},
        {'label' : '票付通', 'value' : 'weshop_piaofutong'},
        {'label' : '天时同城（爱旅）', 'value' : 'weshop_tstc_al'},
        {'label' : '天时同城（北国）', 'value' : 'weshop_tstc_bg'},
        {'label' : '海洋馆', 'value' : 'weshop_royaloc'},
    ]
};

console.log('scope.p.sale_belong');
console.log(scope.p.sale_belong);

scope.p.product_attr_label = scope.label.product_attr[parseInt(scope.p.product_attr)];

for(var i = 0; i < scope.label.sale_belong.length; i++){
	var tmp = scope.label.sale_belong[i];
	if(scope.p.sale_belong == tmp.value){
		scope.p.sale_belong_label = tmp.label;
		break;
	}
}
//scope.p.sale_belong_label = scope.label.sale_belong[parseInt(scope.p.sale_belong)];

console.log('hahhaha');


scope.gogo = function(){

	var para = {
		'id' : scope.p.id,
		'sale_code' : scope.p.sale_code,
		//------------- 价格 -------------------------//
		//'guide_price' : scope.p.guide_price,
		//'cost_price' : scope.p.cost_price,
		'market_price' : scope.p.market_price,
		//------------- 价格 -------------------------//

		//------------- 属性 -------------------------//
		'back_type' : scope.p.back_type,
		'sms_type' : scope.p.sms_type,
		'sms_diy' : scope.p.sms_diy,
		'flash_state' : scope.p.flash_state,
		'tour_date_type' : scope.p.tour_date_type,
		'take_effect_type' : scope.p.take_effect_type,
		'nopay_cancel_minute' : scope.p.nopay_cancel_minute,

		'cardno_state' : scope.p.cardno_state,
		'isshow' : scope.p.isshow,

		//------------- 属性 -------------------------//
	};

	//购买时间处理
	if(scope.p.flash_state == '1'){
		if(angular.isUndefined(scope.p.start_time_date)){
			alert('请选择开始购买时间');
			return;
		}
		if(angular.isUndefined(scope.p.end_time_date)){
			alert('请选择结束购买时间');
			return;
		}

		para['start_time'] = scope.p.start_time_date.format('YYYY-MM-DD HH:mm') + ':00';
		para['end_time'] = scope.p.end_time_date.format('YYYY-MM-DD HH:mm') + ':00';
	}

	//出游时间处理
	if(scope.p.tour_date_type == '1'){
		if(angular.isUndefined(scope.p.tour_date_limit_start_date)){
			alert('请选择出游开始时间');
			return;
		}
		if(angular.isUndefined(scope.p.tour_date_limit_end_date)){
			alert('请选择出游结束时间');
			return;
		}

		para['tour_date_limit_start'] = scope.p.tour_date_limit_start_date.format('YYYY-MM-DD');
		para['tour_date_limit_end'] = scope.p.tour_date_limit_end_date.format('YYYY-MM-DD');
 	}

	//$resource('/api/ac/wc/productService/createWdProduct', {}, {}).save(para, function(res){
	$resource('/api/ac/wc/newProductService/createProduct', {}, {}).save(para, function(res){
		console.log(res);
		if (res.errcode !== 0) {
			alert(res.errmsg);
			return;
		}
		
		alert('操作成功');

	});

};


	scope.cancel = function(){

		scope.close();

	}

		

		}
	};
};

