module.exports = function ($resource, $state, $http, $q, $modal, dlq, $timeout, FileUploader) {
	return {
		restrict: 'AE',
		template: require('../views/p_poster.html'),
		replace: true,
		scope: {
			'p' : '=',
			'uploader': '=',
			'close' : '=',
			//'vm' : '=',
		},
		link: function (scope, elements, attrs) {

			console.log('poster');

			console.log(scope.p);

scope.posterobj = {
	'share_pic' : '',
	'code': scope.p.sale_code,
	'title': scope.p.name,
	//'id' : scope.p.id,
};


scope.init = function(){
	console.log('init');
	$resource('/api/ac/wc/newProductShareService/getshareattr', {}, {}).save({ code: scope.p.sale_code }, function (res) {

		console.log('海报');
		console.log(res);

		if(res.errcode != 0){
			alert(res.errmsg);
			return;
		}

		angular.extend(scope.posterobj, {
		      'qrcode_size' : 1,
		      'width' : 0,
		      'height' : 0,
		      'nickname_width' : 0,
		      'nickname_height' : 0,
		      'nickname_size' : 12,
		      'nickname_color' : '#333333',
		      'nickname_length' : 9,
		});

		if (angular.isDefined(res.data.share_id)) {
			if (res.data.img) {
				scope.posterobj['share_pic'] = res.data.img;
			}
		}

		scope.pic = {
		    'w' : 0,
		    'h' : 0,
		};

		scope.customSettings = {
		    control: 'brightness',
		    theme: 'bootstrap',
		    position: 'top left',
		    animationSpeed : 50,
		    animationEasing: 'swing',
		    hide: null,
		    hideSpeed: 100,
		};

		console.log('extend start');
	  	console.log(scope.posterobj);

	  	angular.extend(scope.posterobj, res.data);

	  	console.log('extend end');
	  	console.log(scope.posterobj);

	  	var screenImage = $("#poster");  
		var theImage = new Image();  
		theImage.src = screenImage.attr("src");  
		var imageWidth = theImage.width;  
		var imageHeight = theImage.height; 

		scope.pic.w = imageWidth;
		scope.pic.h = imageHeight;


		var biLi = imageWidth / 250;

		//没有比例存起来。如果有比例，和之前的比较。
		if(!scope.posterobj['biLi']){
		    scope.posterobj['biLi'] = biLi;
		}else{
		      //如果两次比例不一致。重新换算坐标位置。重新报存比例。
		      var oldBiLi = scope.posterobj['biLi'];
		      console.log('oldBiLi');
		      console.log(oldBiLi);
		      console.log(scope.qrcode);
		      if(biLi != oldBiLi){
		          scope.posterobj.width = scope.posterobj.width / oldBiLi * biLi;
		          scope.posterobj.height = scope.posterobj.height / oldBiLi * biLi;
		          scope.posterobj.nickname_width = scope.posterobj.nickname_width / oldBiLi * biLi;
		          scope.posterobj.nickname_height = scope.posterobj.nickname_height / oldBiLi * biLi;

		          scope.posterobj['biLi'] = biLi;
		          console.log('果两次比例不一致。重新换算坐标位置。重新报存比例');
		          console.log(scope.posterobj);
		      }
		  }
		  

		  console.log(imageWidth);
		  console.log(imageHeight);


		  scope.qrcode = {
		    'left' : scope.posterobj.width / biLi,
		    'top' : scope.posterobj.height / biLi,
		  }

		  scope.wxname = {
		    'left' : scope.posterobj.nickname_width / biLi,
		    'top' : scope.posterobj.nickname_height / biLi,
		  }

		  console.log(scope.qrcode);
		  console.log(scope.wxname);


		  console.log('init end');
		  console.log(scope.posterobj);
	});
}
scope.init();


  scope.gogo = function(){

  	console.log(scope.posterobj);
  	console.log(scope.posterobj.share_pic);

    if(scope.posterobj.share_pic == ''){
        alert('请设置海报');
        return;
    }

    var screenImage = $("#poster");  
    var theImage = new Image();  
    theImage.src = screenImage.attr("src");  
    var imageWidth = theImage.width;  
    var imageHeight = theImage.height; 

    var biLi = imageWidth / 250;

    console.log(imageWidth);
    console.log(imageHeight);

    console.log($("#qrcode").position().left * biLi);
    console.log($("#qrcode").position().top * biLi);
    console.log($("#wxname").position().left * biLi);
    console.log($("#wxname").position().top * biLi);

    scope.posterobj.width = $("#qrcode").position().left * biLi;
    scope.posterobj.height = $("#qrcode").position().top * biLi;
    scope.posterobj.nickname_width = $("#wxname").position().left * biLi;
    scope.posterobj.nickname_height = $("#wxname").position().top * biLi;
    scope.posterobj.qrcode_size = parseInt(1 * biLi) == 0 ? 1 : parseInt(1 * biLi);
    scope.posterobj.nickname_size = parseInt(12 * biLi) == 0 ? 1 : parseInt(12 * biLi);

    console.log(scope.posterobj);

    $resource('/api/ac/wc/newProductShareService/createProducShare', {}, {}).save(scope.posterobj, function (res) {
      console.log(res);

      if (res.errcode !== 0) {
        alert(res.errmsg);
        return;
      }

      alert('保存成功');

      //$modalInstance.close();

    });


  }


  	scope.uploader.onSuccessItem = function (fileItem, response, status, headers) {

  			console.log('上传成功');

	      console.log(fileItem);
	      console.log(response);
	      scope.posterobj.share_pic = response.url;

	      console.log(scope.posterobj);

	};
 
	scope.cancel = function(){

		scope.close();

	}	

		}

	};
};

