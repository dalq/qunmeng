module.exports = function($scope, $resource, p, $modalInstance, $http, $q){

	console.log('commission');

	console.log(p);

function load(){

	var beforedata = {
		//级别列表
		'memberlevellist':
		$http({
			'method': 'GET',
			'url': '/api/as/uc/userwxsk/memberlevellist',
		}),
		//产品佣金
		'productprofitlist':
		$http({
			'method': 'GET',
			'url': '/api/as/wc/productprofit/productprofitlist',
			'params': { 'product_code': p.code },
		}),
		
	};


	$q.all(beforedata).then(function (res) {

		console.log(res);


		//级别列表
		if (res.memberlevellist.data.errcode === 0) {
		} else {
			alert('/api/as/uc/userwxsk/memberlevellist' + res.memberlevellist.data.errmsg);
			return;
		}

		//佣金列表
		if (res.productprofitlist.data.errcode === 0) {
		} else {
			alert('/api/as/wc/productprofit/productprofitlist' + res.productprofitlist.data.errmsg);
			return;
		}

		// console.log(res.productprofitlist.data.data);
		// console.log('scope.p');
		var attrarr = p.attrlist;
		var levelarr = res.memberlevellist.data.data;
		var commissionarr = res.productprofitlist.data.data;
		// console.log(attrarr);
		// console.log(levelarr);
		// console.log(commissionarr);



		for(var m = 0; m < attrarr.length; m++){
			var attr = attrarr[m];
			attr['levelarr'] = [];

			for(var i = 0; i < levelarr.length; i++){
				var level = angular.copy(levelarr[i]);
				//attr['level'] = angular.copy(level);

				level['profit'] = 0;
				level['profit1'] = 0;
				level['profit2'] = 0;
				level['state'] = 0;

				for(var j = 0; j < commissionarr.length; j++){
					var commission = commissionarr[j];
					if(level.code == commission.level_code
					&& attr.attr_id == commission.attr_id){
						level['profit'] = commission.profit;
						level['profit1'] = commission.profit1;
						level['profit2'] = commission.profit2;												
						level['state'] = commission.state;
						level['id'] = commission.id;
						break;
					}
				}

				console.log(level);
				attr.levelarr.push(level);
			}
		}

		console.log(attrarr);

		$scope.objs = attrarr;
	});

}
load();


$scope.opt = function(attr_id, obj){
	console.log(obj);

	var para = {
		'attr_id' : attr_id,
		'level_code' : obj.code,
		'code' : p.code,
		'profit' : obj.profit,
		'profit1' : obj.profit1,
		'profit2' : obj.profit2,
		'state' : obj.state == '1' ? '0' : '1',
	};

	if(angular.isDefined(obj.id)){
		para['id'] = obj.id;
	}

	$resource('/api/ac/wc/newProductService/createProductProfit', {}, {}).save(para, function (res) {
		console.log(para);
		console.log(res);

		if (res.errcode !== 0) {
			alert("数据获取失败");
			return;
		}

		load();

	});


}

$scope.save = function(attr_id, obj){

	var para = {
		'attr_id' : attr_id,
		'level_code' : obj.code,
		'code' : p.code,
		'profit' : obj.profit,
		'profit1' : obj.profit1,
		'profit2' : obj.profit2,			
		'state' : '1',
	}

	if(angular.isDefined(obj.id)){
		para['id'] = obj.id;
	}

	$resource('/api/ac/wc/newProductService/createProductProfit', {}, {}).save(para, function (res) {
		console.log(para);
		console.log(res);

		if (res.errcode !== 0) {
			alert(res.errmsg);
			return;
		}

		load();

	});

}


$scope.cancel = function () {
    $modalInstance.close();
}



};
