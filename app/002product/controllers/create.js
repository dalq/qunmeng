module.exports = function($scope, $resource, $state, $modal){

    console.log('create');

    $scope.p = {
        'product_attr' : '1',
        'price_type' : '0',
        //'sys_affirm_type' : '1',
        'sale_belong' : 'qunmeng',
        'place_code_array' : [],
        'price' : 0,
        's' : new Date(),
        'e' : new Date((new Date()).getFullYear() + '-12-31'),
        //'sysaffirm_target_goods_back_type' : '0',
        'sysaffirm_target_goods_child_flag' : '0',
        'one_order_limit_num' : '0',
    };

    $scope.sale_belong_arr = [
        {'label' : '自营', 'value' : 'qunmeng'},
        {'label' : '居游', 'value' : 'weshop_juyou'},
        {'label' : '票付通', 'value' : 'weshop_piaofutong'},
        {'label' : '天时同城（爱旅）', 'value' : 'weshop_tstc_al'},
        {'label' : '天时同城（北国）', 'value' : 'weshop_tstc_bg'},
        {'label' : '海洋馆', 'value' : 'weshop_royaloc'},
    ];

    $scope.vm1 = {
        'options' : {
            format: "YYYY-MM-DD",
            locale : 'zh-cn',
            ignoreReadonly : true,
            //showClear: true ,                       
           // clearBtn:true
        }
    };

    //商家列表
    function getPlaceList(){
        $resource('/api/as/wc/productbussiness/storeselectlist', {}, {}).save({}, function(res){
            console.log(res);
            if (res.errcode !== 0) {
                alert(res.errmsg);
                return;
            }
            $scope.view_array = res.data;
        });
    }
    getPlaceList();


    //创建商家
    $scope.createPlace = function(place){

        var para = {};

        if(angular.isUndefined(place)){
            para = {
                'type' : 'M'
            };
        }else{
            para = place;
        }

        console.log('创建商家');

        var modalInstance = $modal.open({
          template: require('../views/pop_createplace.html'),
          controller: 'qmplace',
          //size: 'lg',
          resolve: {
            obj : function(){
                return para;
            }
          }
        });

        modalInstance.result.then(function () {
            getPlaceList();
        }, function () {
            
        });
    };

    $scope.gogo = function(){

        var para = {
            'product_attr' : $scope.p.product_attr,
            'sale_belong' : $scope.p.sale_belong,
            'name' : $scope.p.name,
            'sale_category' : 'W10',
            'sys_affirm_type' : '0',
            'price_type' : $scope.p.price_type,   //价格类型，0：有效期模式；1：价格日历模式

        };

        //=============== 对接 =============================//
        //自营
        if($scope.p.sale_belong === 'qunmeng')
        {
            para['sys_affirm_type'] = '1';
        }
        //票付通
        else if($scope.p.sale_belong === 'weshop_piaofutong')
        {
            para['sysaffirm_target_goods_supply'] = $scope.p.sysaffirm_target_goods_supply;
            para['sysaffirm_target_goods_view'] = $scope.p.sysaffirm_target_goods_view;
            para['sysaffirm_target_goods_code'] = $scope.p.sysaffirm_target_goods_code;
        }
        //居游
        else if($scope.p.sale_belong === 'weshop_juyou')
        {
            para['sysaffirm_target_goods_code'] = $scope.p.sysaffirm_target_goods_code;
        }
        //天时同城（北国）/天时同城（爱旅）
        else if($scope.p.sale_belong === 'weshop_tstc_al'
             || $scope.p.sale_belong === 'weshop_tstc_bg')
        {
            para['sysaffirm_target_goods_code'] = $scope.p.sysaffirm_target_goods_code;
        }
        //海洋馆
        else if($scope.p.sale_belong === 'weshop_royaloc')
        {
            para['sysaffirm_target_goods_code'] = $scope.p.sysaffirm_target_goods_code;
            //para['sysaffirm_target_goods_back_type'] = $scope.p.sysaffirm_target_goods_back_type;
            para['sysaffirm_target_goods_child_flag'] = $scope.p.sysaffirm_target_goods_child_flag;
            para['one_order_limit_num'] = 1;
        }
        //=============== 对接 =============================//

        

        //新建产品
        if($scope.p.product_attr == '1')
        {
            if($scope.p.sale_belong === 'qunmeng'){ //自营
                para['sms_type'] = 1;
                para['tour_date_type'] = '0';
                para['cardno_state'] = '0';
            }else{  //对接
                para['sms_type'] = 0;
                para['tour_date_type'] = '1';
                para['cardno_state'] = '1';
            }

            para['back_type'] = '0';
            para['isshow'] = 0;
            para['sms_template_id'] = 'HDSMS0015'; //写死的短信前面的部分
            para['sms_diy'] = '请刷读群盟设备核销';
            para['flash_state'] = '0';
            
            para['take_effect_type'] = 0;   //生效状态, 0即时生效 -1次日生效  正数数字是几就是几个小时
            para['pay_overtime'] = '15';

            
            para['auth_state'] = 0; //是否需要实名，0：不需要 1：全部实名 2：机构实名
            

            //----- 型号 ----------------------//
            para['attr_name'] = $scope.p.name;
            para['guide_price'] = $scope.p.price;
            para['cost_price'] = 0;
            para['market_price'] = 0;
            para['stock_type'] = '0';
            para['current_stock_num'] = 0;
            para['order_num_state'] = 0;
            para['order_limit_num'] = 1;
            para['order_limit_type'] = 0;
            //----- 型号 ----------------------//


            //----- 票种 ----------------------//
            
            //=============== 商家 =============================//
            console.log($scope.p.place_code_array);
            if(angular.isUndefined($scope.p.place_code_array)
            ||  $scope.p.place_code_array.length == 0){
                alert('请选择一个商家');
                return;
            }
            var pc = $scope.p.place_code_array[0];
            for(var i = 1; i < $scope.p.place_code_array.length; i++){
                pc += ',' + $scope.p.place_code_array[i];
            }
            //=============== 商家 =============================//

            para['place_code'] = pc;
            para['appoint'] = '0';
            para['periodstart'] = $scope.p.s.format('YYYY-MM-DD');
            para['periodend'] = $scope.p.e.format('YYYY-MM-DD');
            //----- 票种 ----------------------//

        }
        //儿童剧
        else if($scope.p.product_attr == '2')
        {

        }
        //实物
        else if($scope.p.product_attr == '3')
        {
            para['sms_type'] = 0;
            para['tour_date_type'] = '0';
            para['cardno_state'] = '0';

            para['back_type'] = '0';
            para['isshow'] = 0;
            para['sms_template_id'] = 'HDSMS0015'; //写死的短信前面的部分
            para['sms_diy'] = '不发短信';
            para['flash_state'] = '0';
            
            para['take_effect_type'] = 0;   //生效状态, 0即时生效 -1次日生效  正数数字是几就是几个小时
            para['pay_overtime'] = '15';

            
            para['auth_state'] = 0; //是否需要实名，0：不需要 1：全部实名 2：机构实名


            //----- 型号 ----------------------//
            para['attr_name'] = $scope.p.name;
            para['guide_price'] = $scope.p.price;
            para['cost_price'] = 0;
            para['market_price'] = 0;
            para['stock_type'] = '0';
            para['current_stock_num'] = 0;
            para['order_num_state'] = 0;
            para['order_limit_num'] = 1;
            para['order_limit_type'] = 0;
            //----- 型号 ----------------------//


        }

        //$resource('/api/ac/wc/productService/createWdProduct', {}, {}).save(para, function(res){
        $resource('/api/ac/wc/newProductService/createProduct', {}, {}).save(para, function(res){
            console.log(res);
            if (res.errcode !== 0) {
                alert(res.errmsg);
                return;
            }
            //新建
            $state.go('app.newproductlist.create');
        });

    };

};
