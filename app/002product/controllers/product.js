module.exports = function($scope, $resource, $q, $http, $stateParams, id, $modalInstance, FileUploader){

	console.log('product');

	//var id = $stateParams.id;
	console.log(id);

	$scope.p = {};

	$scope.close = function(){
		$modalInstance.close();
	};


	$scope.uploader = new FileUploader({
	    //url: 'http://cl.juyouhx.com/oss.php/oss/webuploader1?topdir=aa&selfdir=bb'
	    url : 'https://txy.juyouhx.com/Api/Api/ObjectToOss'
	});

	$scope.uploader.filters.push({
	    name: 'imageFilter',
	    fn: function (item /*{File|FileLikeObject}*/, options) {
	        var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
	        return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
	    }
	});

	$scope.uploader.formData.push({'dir' : 'weshop_poster' + "_" + Date.parse(new Date())});

  // $scope.uploader.onSuccessItem = function (fileItem, response, status, headers) {
  //     console.log(fileItem);
  //     console.log(response);
  //     $scope.posterobj.share_pic = response.url;
  // };


	

	$scope.load = function(callback){

		//$resource('/api/ac/wc/productService/wdProductInfo', {}, {}).save({'id' : id}, function(res){
		$resource('/api/ac/wc/newProductService/wdProductInfo', {}, {}).save({'id' : id}, function(res){
			console.log('产品详情');
			console.log(res);

			if(res.errcode != 0){
				alert(res.errmsg);
				return;
			}

			var obj = res.data;

		    obj['start_time_date'] = obj['start_time'];
        	obj['end_time_date'] = obj['end_time'];

        	obj['tour_date_limit_start_date'] = obj['tour_date_limit_start'];
        	obj['tour_date_limit_end_date'] = obj['tour_date_limit_end'];

        	for(var i = 0; i < obj.ticketlist.length; i++){
				var tmp = obj.ticketlist[i];
				tmp['periodstart_date'] = tmp['periodstart'];
			    tmp['periodend_date'] = tmp['periodend'];

				tmp['opt'] = {
					'name' : '0',
					'period' : '0',
					'appoint' : '0',
				};
			}

			console.log('111');

			for(var i = 0; i < obj.attrlist.length; i++){
				var tmp = obj.attrlist[i];

				tmp['opt'] = {
					'attr_name' : '0',
					'guide_price' : '0',
					'cost_price' : '0',
					'stock' : '0',
					'limit' : '0',
				};

				//价格日历模式
				if(obj.price_type == '1'){
					var now = new Date();
					now.setDate(now.getDate() + 30);  

					tmp['calendar'] = {
			    		's' : new Date(),
			    		'e' : now,
			    		'guide_price' : 0,
			    		'cost_price' : 0,
			    		'current_stock_num' : 0,
			    		'week' : [true, true, true, true, true, true, true],
			    		'flag' : {},
			    	};

			    	//处理价格日历需要显示的数据
			    	for(var j = 0; j < tmp.daypricelist.length; j++){
			    		var d = tmp.daypricelist[j];
			    		d['d'] = d.date.split('-').join('');
			    		tmp.calendar.flag[d.date] = d;
			    	}
				}
		    	
			}

			console.log('222');

			//

			$scope.p = obj;

			console.log(callback);

			if(angular.isDefined(callback)){

				console.log('3333');
				callback();
			}

		});
	};

	$scope.load();
	

};
