

var router = function ($urlRouterProvider, $stateProvider) {


    $stateProvider


        //产品列表
        .state('app.newproductlist', {
            url: "/newproduct/list.html",
            views: {
                'main@': {
                    template: require('./views/list.html'),
                    controller: 'newproductlist',
                }
            },
            resolve: {
                'what': function () {
                    return 'list';
                },
                
            }
        })

        //创建产品跳转的产品列表
        .state('app.newproductlist.create', {
            url: "/newproduct/list.html",
            views: {
                'main@': {
                    template: require('./views/list.html'),
                    controller: 'newproductlist',
                }
            },
            resolve: {
                'what': function () {
                    return 'create';
                }
            }
        })

        .state('app.qmnewproduct', {
            url: "/qmnewproduct/create.html",
            views: {
                'main@': {
                    template: require('./views/create.html'),
                    controller: 'qmcreateproduct',
                }
            },
            resolve: {
                
            }
        })


        ;

};

module.exports = router;