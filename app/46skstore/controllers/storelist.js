module.exports = function($scope, $state, $stateParams,$resource,$modal){
    /* 分页
    * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10         //每页显示几条
	$scope.searchform = {};


	$scope.create = function(){

		$state.go('app.newStore');
	};

    
    $scope.load = function () {     
        var para = {
            pageNo:$scope.bigCurrentPage,
            pageSize:$scope.itemsPerPage,
            'type' : 'M',
        };

        para = angular.extend($scope.searchform, para);
        $resource('/api/as/tc/placeview/list', {}, {}).
        save(para, function(res){
            console.log(para);
         	console.log(res);

         	if(res.errcode !== 0)
         	{
         		alert(res.errmsg);
         		return;
         	}

         	$scope.objs = res.data.results;
            $scope.bigTotalItems = res.data.totalRecord;

        });

        

    };
    $scope.load();


    $scope.edit = function(placeid){
        console.log(placeid);
        $state.go('app.newStore',{'placeid' : placeid});
        // var modalInstance = $modal.open({
        //     template: require('../views/additionalinfo.html'),
        //     controller: 'additionalinfo',
        //     size: 'lg',
        //     resolve: {
        //         placeid: function () {
        //             return placeid;
        //         },
        //     }
        //     });
        //     modalInstance.result.then(function (showResult) {	
        //         $scope.load();
        //     });

    };
    $scope.info = function(id){
        console.log(id);
        var modalInstance = $modal.open({
        template: require('../views/storeinfo.html'),
        controller: 'storeinfo',
        size: 'lg',
        resolve: {
            id: function () {
                return id;
            },
         }
        });
        modalInstance.result.then(function (showResult) {	
            $scope.load();
        });
    	// $state.go('app.newStore', {'placeid' : obj.id});

    };
    $scope.updateinfo = function(code){
        $resource('/api/ac/mc/merplaceserviceimpl/updateSKPlace', {}, {}).                        
        save({'place_code' : code}, function(res){

            console.log(res);

            if(res.errcode === 0)
            {
                $scope.load();
            }
            else
            {
                alert(res.errmsg);
            }

        });
    }


    $scope.asort = function(id, asort){
        $resource('/api/as/mc/merplacedao/updatePlace', {}, {}).                        
        save({'id' : id, 'asort' : asort}, function(res){

            console.log(res);

            if(res.errcode === 0)
            {
                $scope.load();
            }
            else
            {
                alert(res.errmsg);
            }

        });

    };

    $scope.type = function(id){
        console.log('配置票种');
    	$state.go('app.tkttype', {'placeid' : id});
    };


    $scope.createtkttype = function(id){
        $state.go('app.tkttypecreate', {'placeid' : id});
    }

    $scope.device = function(code){
        $state.go('app.devicelist', {'placecode' : code});
    }

    
};