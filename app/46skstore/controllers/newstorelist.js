module.exports = function($scope, $state, $stateParams,$resource,$modal,toaster){
    /* 分页
    * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10         //每页显示几条
	$scope.searchform = {};


	$scope.create = function(){

		$state.go('app.newStore');
	};

    
    
    $scope.load = function () {     
        var para = {
            pageNo:$scope.bigCurrentPage,
            pageSize:$scope.itemsPerPage,
            'type' : 'M',
        };

        // para = angular.extend($scope.searchform, para);
        $resource('/api/as/mc/merplacedao/findPlaceList', {}, {}).
        save(para, function(res){
            console.log(para);
         	console.log(res);

         	if(res.errcode !== 0)
         	{
                toaster.error({title:"",body:res.errmsg});
         		return;
         	}

         	$scope.objs = res.data.results;
            $scope.bigTotalItems = res.data.totalRecord;

        });

        

    };
    $scope.load();

    $scope.search = function () {    
        console.log($scope.searchform); 
        // para = angular.extend($scope.searchform, para);
        $resource('/api/as/mc/merplacedao/findPlaceList', {}, {}).
        save($scope.searchform, function(res){

         	if(res.errcode !== 0)
         	{
                toaster.error({title:"",body:res.errmsg});
         		return;
         	}

         	$scope.objs = res.data.results;
            $scope.bigTotalItems = res.data.totalRecord;

        });

        

    };


    $scope.edit = function(code){
        console.log('编辑义卖说');
        console.log(code);
        var modalInstance = $modal.open({
            template: require('../views/additionalinfo.html'),
            controller: 'additionalinfo',
            size: 'lg',
            resolve: {
                code: function () {
                    return code;
                },
            }
            });
            modalInstance.result.then(function (showResult) {	
                // $scope.load();
            });

    };
    $scope.info = function(id){
        console.log(id);
        var modalInstance = $modal.open({
        template: require('../views/storeinfo.html'),
        controller: 'storeinfo',
        size: 'lg',
        resolve: {
            id: function () {
                return id;
            },
         }
        });
        modalInstance.result.then(function (showResult) {	
            $scope.load();
        });
    	// $state.go('app.newStore', {'placeid' : obj.id});

    };
    $scope.updateinfo = function(code){
        $resource('/api/ac/mc/merplaceserviceimpl/updateSKPlace', {}, {}).                        
        save({'place_code' : code}, function(res){

            console.log(res);

            if(res.errcode === 0)
            {
                toaster.success({title:"", body:"商户信息已同步"});
                $scope.load();
            }
            else
            {
                toaster.error({title:"", body:res.errmsg});
            }

        });
    }


    $scope.asort = function(id, asort){
        $resource('/api/as/tc/placemerchant/info', {}, {}).        
        save({'merchant_id' : id},function(res){
            if(res.errcode == 10003){
                // console.log(res);
                alert('未设置商户信息,不可进行排序操作');
                // toaster.success({title:"",body:"未设置商户信息,不可进行排序操作"});
                
            } else {
                console.log('商户信息');
                console.log(res);
                console.log({'id' : id, 'asort' : asort});
                $resource('/api/as/tc/place/update', {}, {}).                        
                save({'id' : id, 'asort' : asort}, function(res){

                    console.log(res);

                    if(res.errcode === 0)
                    {
                        $scope.load();
                    }
                    else
                    {
                        alert(res.errmsg);
                    }

                });
            }
        

        });
        

    	// console.log({'merchant_id' : id, 'merchant_asort' : asort});
    	// updateplacemerchant.save({'merchant_id' : id, 'merchant_asort' : asort}, function(res){

		// 	console.log(res);

		// 	if(res.errcode === 0)
		// 	{
		// 		$scope.load();
		// 	}
		// 	else
		// 	{
		// 		alert(res.errmsg);
		// 	}

		// });

    };

    $scope.type = function(id){
    	$state.go('app.tkttype', {'placeid' : id});
    };


    $scope.createtkttype = function(id){
        $state.go('app.tkttypecreate', {'placeid' : id});
    }

    $scope.device = function(code){
        $state.go('app.devicelist', {'placecode' : code});
    }

    
};