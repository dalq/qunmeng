module.exports = function($scope, $stateParams,devicetype,$resource,slist){

	//机器id
	var id = $stateParams.id;

	$scope.lock = true;

	//机器类型
	var  devicetype = [
            {
                'name' : '检票机',
                'code' : 1
            },
            {
                'name' : '闸机',
                'code' : 2
            },
            {
                'name' : '手持查消票机',
                'code' : 3
            },
            {
                'name' : '办卡机',
                'code' : 4
            }
        ];
	$scope.typearr = devicetype;

	// 景区下拉
	$resource('/api/as/sa/placeview/jlist', {}, {}).
	save({}, function(res){
		console.log(res);
        if(res.errcode === 0)
        {
        	$scope.viewarr = res.data;
			console.log(res);
        }
        else
        {
            alert(res.errmsg);
        }
	});
	// slist().get(function(res) {
    //     console.log(res);
    //     if(res.errcode === 0)
    //     {
    //     	$scope.viewarr = res.data;
    //     }
    //     else
    //     {
    //         alert(res.errmsg);
    //     }
    // });

	//票机详情
	$resource('/api/as/tc/device/info', {}, {}).
	get({'id' : id}, function(res){

		console.log(res);

		if(res.errcode === 0)
		{
			$scope.obj = res.data;
			$scope.obj.id = id;
		}
		else
		{
			alert(res.errmsg);
		}

	});


	
	$scope.gogo = function(){
		$resource('/api/as/tc/device/update', {}, {}).		
		save($scope.obj, function(res){

			console.log(res);

			if(res.errcode !== 0)
			{
				alert(res.errmsg);
			}
			else
			{
				alert('修改成功');
			}

		});

	};


	
	$scope.remove = function(){

		console.log({'device_code' : $scope.obj.code});
		$resource('/api/as/tc/deviceauth/deleteall', {}, {}).				
		save({'device_code' : $scope.obj.code}, function(res){

			console.log(res);

			if(res.errcode === 0 || res.errcode === 1105)
			{
				$scope.lock = false;
			}
			else
			{
				alert(res.errmsg);
			}

		});
	}

};