module.exports = function($scope, $state, $stateParams,$resource,$modalInstance,$modal,FileUploader,id,$http,$q){
    console.log(id);
    $resource('/api/as/mc/merplacedao/getPlaceInfo', {}, {}).
    save({'id' : id}, function(res){
        if(res.errcode !== 0)
        {
            alert(res.errmsg);
            return;
        }

        console.log(res);
		$scope.placeobj = res.data;
		$scope.province_code = res.data.province;// 省code
		$scope.city_code = res.data.city; //市code
		$scope.area_code = res.data.area;// 区code
		$scope.placeobj.place_info = getSimpleText(res.data.place_info);
		$resource('/api/us/sc/city/arealist', {}, {}).
		save({}, function(res){
			if(res.errcode !== 0)
			{
				alert(res.errmsg);
				return;
			}
			var provincearr = res.data;
			for(var i = 0; i < provincearr.length; i++){
				if($scope.placeobj.province == provincearr[i].code){
				  $scope.placeobj.province_name = provincearr[i].name;
				}
			}
		});
		$resource('/api/us/sc/city/arealist', {}, {}).
		save({'code' : $scope.province_code}, function(res){
			if(res.errcode !== 0)
			{
				alert(res.errmsg);
				return;
			}
			console.log('chenghsi');
			console.log(res);
			var cityearr = res.data;
			for(var i = 0; i < cityearr.length; i++){
				if($scope.placeobj.city == cityearr[i].code){
				  $scope.placeobj.city_name = cityearr[i].name;
				}
			}
		});
		$resource('/api/us/sc/city/arealist', {}, {}).
		save({'code' : $scope.city_code}, function(res){
			if(res.errcode !== 0)
			{
				alert(res.errmsg);
				return;
			}
			console.log('区');
			console.log(res);
			var districtarr = res.data;
			for(var i = 0; i < districtarr.length; i++){
				if($scope.placeobj.area == districtarr[i].code){
				  $scope.placeobj.district_name = districtarr[i].name;
				}
			}
		});
		$resource('/api/us/sc/city/arealist', {}, {}).
		save({'code' : $scope.area_code}, function(res){
			if(res.errcode !== 0)
			{
				alert(res.errmsg);
				return;
			}
			console.log('商圈');
			console.log(res);
			var buss_districtarr = res.data;
			for(var i = 0; i < buss_districtarr.length; i++){
				if($scope.placeobj.business_district == buss_districtarr[i].code){
				  $scope.placeobj.business_district_name = buss_districtarr[i].name;
				}
			}
		});
			

    });

	 function getSimpleText(html){
        var re1 = new RegExp("<.+?>","g");//匹配html标签的正则表达式，"g"是搜索匹配多个符合的内容
        var msg = html.replace(re1,'');//执行替换成空字符
        console.log(msg);
        return msg;
    }

    $scope.page = {};
	var beforedata = {
		//省份列表
		'provincelist' :
		$http({
			'method' : 'GET', 
			'url': '/api/us/sc/city/arealist',
		}),
		//城市列表
		'citylist' :
		$http({
			'method' : 'GET', 
			'url': '/api/us/sc/city/arealist',
			'params' : {'code' : '210000'},
		}),
		//区
		'districtlist' :
		$http({
			'method' : 'GET', 
			'url': '/api/us/sc/city/arealist',
			'params' : {'code' : '210100'},
		}),
		//类型
		'typelist' :
		$http({
			'method' : 'GET', 
			'url': '/api/us/mc/mertradetypedao/findByTypeList',
			'params' : {'type' : 'cheap_menu'},
		}),
	};

			// if(id !== ''){

			// 	beforedata['placeinfo'] = $http({
			//         'method' : 'GET', 
			//         'url': '/api/as/tc/place/info',
			//         'params' : {
			//             'id' : id
			//         }
			//     });
			// }


		$q.all(beforedata).then(function(res){
			console.log('8888888');
			// 地址信息
			console.log(res);
			//分类信息
		    if(res.provincelist.data.errcode === 0){
		        //console.log(res.categorylist.data);

		    }else{
		        alert(res.provincelist.data.errmsg);
		        return ;
		    }

		    if(res.citylist.data.errcode === 0){
		        //console.log(res.categorylist.data);

		    }else{
		        alert(res.citylist.data.errmsg);
		        return ;
		    }

		    if(res.districtlist.data.errcode === 0){
		        //console.log(res.categorylist.data);

		    }else{
		        alert(res.districtlist.data.errmsg);
		        return ;
		    }

		    if(angular.isDefined(res.placeinfo)){
		    	//地点信息
	            if(res.placeinfo.data.errcode === 0){
	            	angular.extend(scope.placeobj, res.placeinfo.data.data);
	            }else{
	            	alert(res.placeinfo.data.errmsg);
	            	return
	            }
	        }

	        if(res.typelist.data.errcode === 0){
		        //console.log(res.categorylist.data);

		    }else{
		        alert(res.districtlist.data.errmsg);
		        return ;
		    }


	        $scope.page = {
	        	'provincelist' : res.provincelist.data.data,
	        	'citylist' : res.citylist.data.data,
	        	'districtlist' : res.districtlist.data.data,
	        	'typelist' : res.typelist.data.data,
	        	'business_districtlist' : [],
	        }

	        if($scope.placeobj.province == ''){
				$scope.placeobj.province = '210000';	//辽宁省
	        	$scope.placeobj.city = '210100';		//沈阳市
	        	$scope.placeobj.district = '';		//铁西区
	        	//scope.placeobj.business_district = ''	//xxx商圈
	        }
            $scope.areaobj = {
                'province' : '',
                'city' : '',
                'district' : '',
                'business_district' : ''
            }
            var provincearr = $scope.page.provincelist;
            var cityarr = $scope.page.citylist;
            var districtarr = $scope.page.districtlist;
            var business_districtarr = $scope.page.business_districtlist;
            
            for(var i = 0; i < provincearr.length; i++){
                if($scope.placeobj.province == provincearr[i].code){
                    $scope.areaobj.province =  provincearr[i].name;
                }
            }
            for(var i = 0; i < cityarr.length; i++){
                if($scope.placeobj.city == cityarr[i].code){
                    $scope.areaobj.city =  cityarr[i].name;
                }
            }
            for(var i = 0; i < districtarr.length; i++){
                if($scope.placeobj.district == districtarr[i].code){
                    $scope.areaobj.district =  districtarr[i].name;
                }
            }


		});


		//省
		$scope.changeprovince = function(code){
			$scope.placeobj.city = '';
			$scope.placeobj.district = '';
			$scope.placeobj.business_district = '';

			$scope.page.citylist = [];
			$scope.page.districtlist = [];
			$scope.page.business_districtlist = [];

			getarea('province', code);
		};
		//市
		$scope.changecity = function(code){
			$scope.placeobj.district = '';
			$scope.placeobj.business_district = '';

			$scope.page.districtlist = [];
			$scope.page.business_districtlist = [];

			getarea('city', code);
		};
		//区
		$scope.changedistrict = function(code){
			$scope.placeobj.business_district = '';
			$scope.page.business_districtlist = [];
			getarea('district', code);
		};



		function getarea(what, code){
			$resource('/api/us/sc/city/arealist', {}, {})
			.get({'code' : code}, function(res){
				console.log(res);
				if(res.errcode !== 0){
					alert(res.errmsg);
					return;
				}

				if(what === 'province'){
					$scope.page.citylist = res.data;
				}else if(what === 'city'){
					$scope.page.districtlist = res.data;
				}else if(what === 'district'){
					$scope.page.business_districtlist = res.data;
				}
			});
		}

};