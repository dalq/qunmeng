var App = angular.module('skstore', []);

App.config(require('./router'));

//service
App.factory('skstoreservice', require('./service'));
// 商户列表
App.controller('skstorelist',require('./controllers/storelist'));
// App.controller('skviewedit',require('./controllers/viewedit'));
App.controller('createplace',require('./controllers/create'));
App.controller('createstore',require('./directives/baseinfo'));
App.controller('additionalinfo',require('./controllers/additionalinfo'));

App.controller('tkttype',require('./controllers/tkttype'));
App.controller('tkttypecreate',require('./controllers/tkttypecreate'));
App.controller('tkttypeedit',require('./controllers/tkttypeedit'));
App.controller('tkttypeattr',require('./controllers/tkttypeattr'));
App.controller('tkttypeattrcreate',require('./controllers/tkttypeattrcreate'));
App.controller('tkttypeattredit',require('./controllers/tkttypeattredit'));
App.controller('list',require('./controllers/list'));
App.controller('tickettypelist',require('./controllers/tickettypelist'));
App.controller('skdevicetktedit',require('./controllers/devicetktedit'));
App.controller('tickettypelist',require('./controllers/tickettypelist'));
App.controller('devicecreate',require('./controllers/devicecreate'));
App.controller('storeinfo',require('./controllers/storeinfo'));

App.controller('newstorelist',require('./controllers/newstorelist'));





module.exports = App;