var service = function($resource, $q, $state, $modal,$http){
    //景区简表
    // var slist = BASEURL38985 + '/api/as/sa/placeview/jlist';
    var create = BASEURL38985 + '/api/as/tc/device/create';
    var model = [
        {
            'title' : '机器码',
            'id' : 'code',
            'type' : 'text',
            'required' : true,
            'placeholder' : '必填',
        },
        {
            'title' : '景区',
            'id' : 'view',
            'type' : 'select',
            'url' : '/api/as/tc/placeview/jlist',
            'lock' : true,
            'n' : 'name',
            'v' : 'code',
        },
         {
            'title' : '解除绑定',
            'type' : 'button',
            'info' : [
                {'label' : '解除' , 'btnclick' : function(form, $event){
                    $resource('/api/as/tc/deviceauth/deleteall', {}, {}).save({'device_code' : form.result.code}, function(res){
                        console.log(res);
                        if(res.errcode === 0 || res.errcode === 1105){
                            for(var i = 0; i < form.elements.length; i++){
                                var tmp = form.elements[i];
                                if(tmp.id == 'view'){
                                    tmp.lock = false;
                                    break;
                                }
                            }
                        }else{
                            alert(res.errmsg);
                        }
                    });
                }}
            ],
        },
        {
            'title' : '密码',
            'id' : 'pas',
            'type' : 'text',
        },
        {
            'title' : '备注',
            'id' : 'remarks',
            'type' : 'text',
        },
        {
            'title' : '票机类型',
            'id' : 'type',
            'type' : 'select',
            'info' : [
                {'name' : '检票机', 'value' : 1},
                {'name' : '闸机', 'value' : 2},
                {'name' : '手持查消票机', 'value' : 3},
                {'name' : '办卡机', 'value' : 4},
            ],
            'value' : 1
        },
        {
            'title' : '设备状态可用',
            'id' : 'state',
            'type' : 'switch',
            'open' : 1,
            'close' : 0,
            'value': 1,
        },
        {
            'title' : '散票-权限状态',
            'id' : 'auth_state',
            'type' : 'switch',
            'open' : 1,
            'close' : 0,
            'value': 1,
        },
        {
            'title' : '散票-是否多销验票',
            'id' : 'many_state',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '单张', 'value' : '1'},
                {'name' : '多张', 'value' : '2'},
            ],
            'value': 1,
        },
        {
            'title' : '团票-权限状态',
            'id' : 'group_auth_state',
            'type' : 'switch',
            'open' : 1,
            'close' : 0,
            'value': 1,
        },
         {
            'title' : '创建时间',
            'id' : 'create_time',
            'type' : 'static',
        },

    ];
    return {
        makeArr : function(str){
            var obj = [];

            if(str === undefined || str.length === 0) return obj;

            var arr = str.split(',');
            for(var i = 0; i < arr.length; i++)
            {
                obj.push({'name' : arr[i]});
            }
            return obj;
        },

        makeStr : function(arr){
            
            if(!angular.isArray(arr)) return '';

            var arr1 = [];
            for(var i = 0; i < arr.length; i++)
            {
                arr1.push(arr[i].name);
            }
            return arr1.join(',');
        },
        slist : function(obj){
            var deferred = $q.defer(); // 声明延后执行，表示要去监控后面的执行  
            $http({method: 'GET', params: obj, url: slist}).then(
                function(data){
                    deferred.resolve(data.data);
                },
                function(data){
                    deferred.reject(data.data);
                });
            // success(function(data, status, headers, config) {  
            //     deferred.resolve(data);  // 声明执行成功，即http请求数据成功，可以返回数据了  
            // }).  
            // error(function(data, status, headers, config) {  
            //     deferred.reject(data);   // 声明执行失败，即服务器返回错误  
            // });  
            return deferred.promise;   // 返回承诺，这里并不是最终数据，而是访问最终数据的API 
        },
        devicetype : [
            {
                'name' : '检票机',
                'code' : 1
            },
            {
                'name' : '闸机',
                'code' : 2
            },
            {
                'name' : '手持查消票机',
                'code' : 3
            },
            {
                'name' : '办卡机',
                'code' : 4
            }
        ],
         model : function(){
            return model;
        },
        create : function(){
            return create;
        },
    };
};
module.exports = service;