module.exports = function($scope, $state, $stateParams,$resource,FileUploader,$http,$q,$modal, toaster){
	var placeid = $stateParams.placeid;
	// alert(placeid);
	$scope.placeobj = {
		'name' : '',
		'subname' : '',
		'address' : '',
		'content' : '',
		'province' : '',	//省
		'city' : '',		//市
		'district' : '',	//区
		'business_district' : '', 	//商圈
		'open_date' : '',	//开放日期
		'open_time' : '',	//营业时间
		'longitude' : '0',	//经度
		'latitude' : '0',	//纬度
		'asort'	: 0,	
		'theme'	: [],	//主题
		'star' : '5',
		'img' : '',		//顶图
		'logo' : '',
		'view_type' : '',	//分类
		'type' : 'M'
	}
	$scope.info = {
		'themearr' : []
	}
	
	// 标头图片
    var uploader1 = $scope.uploader1 = new FileUploader({
        url: 'http://cl.juyouhx.com/oss.php/oss/webuploader1?topdir=aa&selfdir=bb'
    });

    uploader1.filters.push({
        name: 'imageFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });
    
    
    uploader1.onSuccessItem = function(fileItem, response, status, headers) {
        $scope.placeobj.img = response.savename;
    };
	// 标头图片
    var uploader2 = $scope.uploader2 = new FileUploader({
        url: 'http://cl.juyouhx.com/oss.php/oss/webuploader1?topdir=aa&selfdir=bb'
    });

    uploader2.filters.push({
        name: 'imageFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });
    
    
    uploader2.onSuccessItem = function(fileItem, response, status, headers) {
        $scope.placeobj.logo = response.savename;
    };

	// // 		angular.extend($scope.placeobj, obj);
	// // 		console.log('99999999');
	// // 		console.log($scope.placeobj.id);
	$scope.aaa = {
		'provincecode' : '',
		'citycode' : '',
		'districtcode' : ''
	}
	
	if(placeid){
		$resource('/api/as/tc/place/info', {}, {})
		.save({'id' : placeid}, function(res){
			if(res.errcode !== 0){
				toaster.error({title:"", body:res.errmsg});
			} else {
				// 详情的主题数组
				$scope.linshiarr = res.data.theme.split(",");
				console.log('主题');
				console.log($scope.linshiarr);
				// 拿取出来的值跟主题列表进行比较
				$resource('/api/as/sc/dict/dictbytypelist', {}, {})
				.save({'type' : 'place_theme'}, function(res){
					if(res.errcode !== 0){
						alert(res.errmsg);
						return;
					}
					var themeArr = res.data;
					console.log('主题列表');
					console.log(themeArr);
					for(var i = 0; i < themeArr.length; i++){
						for(var j = 0; j < $scope.linshiarr.length; j++){
							if($scope.linshiarr[j] == themeArr[i].label){
								console.log($scope.linshiarr[j]);
								$scope.info.themearr.push($scope.linshiarr[j]);
							} 
							

						}
					}
					console.log('框框里面的');
					console.log($scope.info.themearr);
					
				});

				console.log(res);
				$scope.placeobj = res.data;
				$scope.aaa.provincecode = res.data.province;
				$scope.aaa.citycode = res.data.city;
				$scope.aaa.districtcode = res.data.district;
				console.log('provincecode' + $scope.aaa.provincecode);
				// 城市列表
				$resource('/api/us/sc/city/arealist', {}, {})
				.save({'code' : $scope.aaa.provincecode}, function(res){
					if(res.errcode !== 0){
						alert(res.errmsg);
						return;
					}
					console.log('城市列表');
					console.log(res);
					$scope.page.citylist = res.data;
					
				});

				// 区列表
				$resource('/api/us/sc/city/arealist', {}, {})
				.save({'code' : $scope.aaa.citycode}, function(res){
					if(res.errcode !== 0){
						alert(res.errmsg);
						return;
					}
					console.log('城市列表');
					console.log(res);
					$scope.page.districtlist = res.data;
					
				});
				// 商圈列表
				$resource('/api/us/sc/city/arealist', {}, {})
				.save({'code' : $scope.aaa.districtcode}, function(res){
					if(res.errcode !== 0){
						alert(res.errmsg);
						return;
					}
					console.log('城市列表');
					console.log(res);
					$scope.page.business_districtlist = res.data;
					
				});
			}


		});

	}

	$scope.page = {};
	var beforedata = {
		//省份列表
		'provincelist' :
		$http({
			'method' : 'GET', 
			'url': '/api/us/sc/city/arealist',
		}),
		//城市列表
		'citylist' :
		$http({
			'method' : 'GET', 
			'url': '/api/us/sc/city/arealist',
			'params' : {'code' : '210000'},
		}),
		//区
		'districtlist' :
		$http({
			'method' : 'GET', 
			'url': '/api/us/sc/city/arealist',
			'params' : {'code' : '210100'},
		}),
		//类型
		'typelist' :
		$http({
			'method' : 'GET', 
			'url': '/api/us/mc/mertradetypedao/findByTypeList',
			'params' : {'type' : 'cheap_menu'},
		}),
		//主题
		'themelist' :
		$http({
			'method' : 'GET', 
			'url': '/api/as/sc/dict/dictbytypelist',
			'params' : {'type' : 'place_theme'},
		}),
	};

			if(placeid){
				console.log('oooooo');
				beforedata['placeinfo'] = $http({
			        'method' : 'GET', 
			        'url': '/api/as/tc/place/info',
			        'params' : {
			            'id' : placeid
			        }
			    });
			}




		$q.all(beforedata).then(function(res){
			console.log('8888888');
			// 地址信息
			console.log(res);
			//分类信息
		    if(res.provincelist.data.errcode === 0){
		        //console.log(res.categorylist.data);

		    }else{
		        alert(res.provincelist.data.errmsg);
		        return ;
		    }

		    if(res.citylist.data.errcode === 0){
		        //console.log(res.categorylist.data);

		    }else{
		        alert(res.citylist.data.errmsg);
		        return ;
		    }

		    if(res.districtlist.data.errcode === 0){
		        //console.log(res.categorylist.data);

		    }else{
		        alert(res.districtlist.data.errmsg);
		        return ;
		    }

		    if(angular.isDefined(res.placeinfo)){
		    	//地点信息
	            if(res.placeinfo.data.errcode === 0){
	            	angular.extend($scope.placeobj, res.placeinfo.data.data);
	            }else{
	            	alert(res.placeinfo.data.errmsg);
	            	return
	            }
	        }

	        if(res.typelist.data.errcode === 0){
		    }else{
		        alert(res.typelist.data.errmsg);
		        return ;
		    }

			if(res.themelist.data.errcode === 0){
				console.log('111111');
				console.log(res.themelist.data);
		    }else{
				
				alert(res.themelist.data.errmsg);
		        return ;
		    }

			
	        $scope.page = {
	        	'provincelist' : res.provincelist.data.data,
	        	'citylist' : res.citylist.data.data,
	        	'districtlist' : res.districtlist.data.data,
	        	'typelist' : res.typelist.data.data,
	        	'business_districtlist' : [],
				'themelist' : res.themelist.data.data,
			}
			console.log($scope.page.themelist);

	        if($scope.placeobj.province == ''){
				$scope.placeobj.province = '210000';	//辽宁省
	        	$scope.placeobj.city = '210100';		//沈阳市
	        	$scope.placeobj.district = '';		//铁西区
	        	//scope.placeobj.business_district = ''	//xxx商圈
	        }

		});


		//省
		$scope.changeprovince = function(code){
			$scope.placeobj.city = '';
			$scope.placeobj.district = '';
			$scope.placeobj.business_district = '';

			$scope.page.citylist = [];
			$scope.page.districtlist = [];
			$scope.page.business_districtlist = [];

			getarea('province', code);
		};
		//市
		$scope.changecity = function(code){
			$scope.placeobj.district = '';
			$scope.placeobj.business_district = '';

			$scope.page.districtlist = [];
			$scope.page.business_districtlist = [];

			getarea('city', code);
		};
		//区
		$scope.changedistrict = function(code){
			console.log(code);
			// $scope.placeobj.district = code;
			$scope.placeobj.business_district = '';
			$scope.page.business_districtlist = [];
			getarea('district', code);
		};



		function getarea(what, code){
			$resource('/api/us/sc/city/arealist', {}, {})
			.get({'code' : code}, function(res){
				console.log(res);
				if(res.errcode !== 0){
					alert(res.errmsg);
					return;
				}

				if(what === 'province'){
					$scope.page.citylist = res.data;
				}else if(what === 'city'){
					$scope.page.districtlist = res.data;
				}else if(what === 'district'){
					$scope.page.business_districtlist = res.data;
				}
			});
		}
			
			 
			$scope.save = function(){
				console.log($scope.info.themearr);
				$scope.placeobj.theme = $scope.info.themearr.join(",");
				var url = '';
				if(placeid !== ''){
					var para = {
						'id' : placeid
					}
					// $scope.placeobj.theme = $scope.array.split(",");
					para = angular.extend($scope.placeobj,para);
					url = '/api/as/tc/place/update';
					$resource(url, {}, {}).save(para, function(res){
					console.log(res);
					if(res.errcode != 0){
						alert(res.errmsg);
						return;
					}
					alert('修改成功');
					$state.go('app.skstorelist');
					
					// var modalInstance = $modal.open({
					// template: require('../views/additionalinfo.html'),
					// controller: 'additionalinfo',
					// size: 'lg',
					// resolve: {
					// 	placeid: function () {
					// 		return placeid;
					// 	},
					// }
					// });
					// modalInstance.result.then(function (showResult) {	
					// 	$scope.getlist();
					// });
					
				});
				}else{
					url = '/api/as/tc/place/create';
						if(!$scope.placeobj.longitude){
						$scope.placeobj.longitude = 0;
					}
					if(!$scope.placeobj.latitude){
						$scope.placeobj.latitude = 0;
					}

					$resource(url, {}, {}).save($scope.placeobj, function(res){
						console.log(res);
						if(res.errcode != 0){
							alert(res.errmsg);
							return;
						}
						alert('添加成功');
						console.log(res.data.uuid);
						$state.go('app.skstorelist');
						// var modalInstance = $modal.open({
						// 	template: require('../views/additionalinfo.html'),
						// 	controller: 'additionalinfo',
						// 	size: 'lg',
						// 	resolve: {
						// 		placeid: function () {
						// 			return placeid;
						// 		},
						// 		uuid: function () {
						// 			return res.data.uuid;
						// 		}

						// 	}
						// });
						// modalInstance.result.then(function (showResult) {	
						// 	$scope.getlist();
						// });
					});
				}
				
			};

	
};

