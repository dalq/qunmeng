var router = function($urlRouterProvider, $stateProvider){

 	$stateProvider

     //商户列表
    .state('app.skstorelist', {
        url: "/shangke/storelist.html",
        views: {
            'main@' : {
                template : require('../46skstore/views/storelist.html'),
                controller : 'skstorelist',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
        }
    })
    //创建商户
    .state('app.newStore', {
        url: "/shangke/newStore.html/:placeid",
        views: {
            'main@' : {
                template : require('../46skstore/views/p_baseinfo.html'),
                controller : 'createstore',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
            'type' : function(){
                    return 'M';
            },
            'placeid' : function(){
                return '';
            }
        }
    })
    //票种
    .state('app.tkttype', {
        url: "/shangke/tkttype.html/:placeid",
        views: {
            'main@' : {
                template : require('../46skstore/views/tkttype.html'),
                controller : 'tkttype',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
            
        }
    })



    .state('app.tkttypecreate', {
        url: "/shangke/tkttypecreate.html/:placeid",
        views: {
            'main@' : {
                template : require('../46skstore/views/tkttypemodel.html'),
                controller : 'tkttypecreate',
            }
        },
        resolve:{
            // makeArr: function (skstoreservice) {
            //     return skstoreservice.makeArr;
            // },
            // makeStr: function (skstoreservice) {
            //     return skstoreservice.makeStr;
            // },
            
        }
    })

    .state('app.tkttypeedit', {
        url: "/shangke/tkttypeedit.html/:placeid/:id",
        views: {
            'main@' : {
                template : require('../46skstore/views/tkttypemodel.html'),
                controller : 'tkttypeedit',
            }
        },
        resolve:{
            // tableconfig : function(tableservice){
            //     return tableservice.tableconfig();
            // },
            // makeArr: function (skstoreservice) {
            //     return skstoreservice.makeArr;
            // },
            // makeStr: function (skstoreservice) {
            //     return skstoreservice.makeStr;
            // },
            
        }
    })

    .state('app.devicelist', {
        url: "/shangke/devicelist.html/:placecode",
        views: {
            'main@' : {
                template : require('../46skstore/views/list.html'),
                controller : 'list',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
            
        }
    })

    .state('app.skdevicetktedit', {
        url: "/shangke/devicetktedit.html/:id",
        views: {
            'main@' : {
                template : require('../46skstore/views/module.html'),
                controller : 'skdevicetktedit',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
            slist : function(viewservice){
                return viewservice.slist;
            },
            devicetype : function(deviceservice){
                return deviceservice.devicetype;
            }
            
        }
    })

    .state('app.devicetktcreate', {
        url: "/shangke/devicetkt.html/:id",
        views: {
            'main@' : {
                template : require('../46skstore/views/module.html'),
                controller : 'devicecreate',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
            slist : function(viewservice){
                return viewservice.slist;
            },
            devicetype : function(deviceservice){
                return deviceservice.devicetype;
            }
            
        }
    })

     .state('app.newstorelist', {
        url: "/shangke/newstorelist.html",
        views: {
            'main@' : {
                template : require('../46skstore/views/newstorelist.html'),
                controller : 'newstorelist',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
            slist : function(viewservice){
                return viewservice.slist;
            },
            devicetype : function(deviceservice){
                return deviceservice.devicetype;
            }
            
        }
    })


    // //创建设备
    //   .state('app.devicetktcreate', {
    //     url: '/shangke/devicetkt/:placecode',
    //     controller: 'devicecreate',
    //     template: require('../46skstore/views/module.html'),
    //     resolve:{
    //         // create : function(deviceservice){
    //         //     return deviceservice.create();
    //         // },
    //         slist : function(viewservice){
    //             return viewservice.slist;
    //         },
    //         devicetype : function(deviceservice){
    //             return deviceservice.devicetype;
    //         }
    //         // update : function(deviceservice){
    //         //     return deviceservice.update();
    //         // }
    //     }
        
    //   })
    // //创建设备
	// 	.state('app.deviceCreate', {
	// 		url: "/device/create.html",
	// 		views: {
	// 			'main@': {
	// 				template: require('../996tpl/views/form.html'),
	// 				controller: 'deviceCreate',
	// 			}
	// 		},
	// 		resolve: {
	// 			formconfig: function (formservice) {
	// 				return formservice.formconfig();
	// 			},
	// 			model: function (deviceservice) {
	// 				return deviceservice.model;
	// 			}
	// 		}
	// 	})


   

	
};

module.exports = router;