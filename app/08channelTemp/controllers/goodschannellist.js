module.exports = function($scope, item, $state, $resource, $modalInstance, $q, $http, 
	model, category, date2str, FileUploader){

	$scope.model = model;

	//每次只有一个显示：true，不限制显示数量：false。
	$scope.oneAtATime = true;
	$scope.obj = item;
	console.log(item);


	var goodsChannelObj = {};
    var goodsChannelStateObj = {};

	var objs = [];

	var beforedata = {
        //渠道列表
        'channelTempList' :
        $http({
            'method' : 'GET', 
            'url': '/api/ac/tc/channelTemplete/jlist'
        }),
        //商品信息
        'goodsinfo' :
        $http({
            'method' : 'GET', 
            'url': '/api/ac/tc/ticketGoods/getTicketGoodsInfo',
            'params' : {
                'id' : item.id
            }
        }),
        //商品的渠道状态
        'goodsChannelState' :
        $http({
            'method' : 'GET', 
            'url': '/api/ac/tc/ticketGoods/getGoodsChannelList',
            'params' : {
                'id' : item.id
            }
        }),
    };


    $q.all(beforedata).then(function(res){

        console.log(res.channelTempList.data.data);
        console.log(res.goodsinfo.data.data);
        console.log(res.goodsChannelState.data.data.goodsChannelList);

        //所有的渠道信息
        var channelList = res.channelTempList.data.data;
        //商品信息
        var goodsinfo = res.goodsinfo.data.data;
        //商品的渠道状态
        var goodsChannelState = res.goodsChannelState.data.data.goodsChannelList;

        //填充渠道对象
        for(var i = 0; i < goodsinfo.channelInfoList.length; i++){
            var tt = goodsinfo.channelInfoList[i];
            goodsChannelObj[tt.templete_code] = tt;
        }

        //填充渠道状态对象
        for(var i = 0; i < goodsChannelState.length; i++){
            var tt = goodsChannelState[i];
            goodsChannelStateObj[tt.channel_code] = tt.product_state;
        }

        for(var i = 0; i < channelList.length; i++){
        	var t = channelList[i];
            console.log(t);
        	t['obj'] = {};
        	t['dateshow'] = {};
		    t['timeshow'] = {};
		    t['imageshow'] = {};
		    t['result'] = {};
		    t['data'] = [];
            //状态
            t['state'] = goodsChannelStateObj[t.templete_code];

		    //1.初始化result
		    var tmp = t.templete_check_data_json;
            var tmparr = tmp.split('#');
		    for(var m = 1; m < tmparr.length - 1; m++){
                var tmp1 = tmparr[m];
                if(angular.isUndefined(model[tmp1])){
                    continue;
                }

                if(angular.isDefined(model[tmp1].value)){
                	t.result[tmp1] = t['obj'][tmp1] = model[tmp1].value;
                }else{
                    t.result[tmp1] = t['obj'][tmp1] = '';
                }
            }
		    //2.初始化date，time，image等必要数据
		    for(var m = 0; m < category.length; m++){
                var tmp1 = category[m];
                for(var n = 0; n < tmp1.data.length; n++){
                    var tmp2 = tmp1.data[n];
                    if(angular.isUndefined(t['obj'][tmp2])){
                        continue;
                    }

                    if(model[tmp2].type === 'date1')
                    {
                        t.dateshow[tmp2] = {
                            'label' : date2str(new Date()),
                            'opened' : false,
                        };
                    }

                    if(model[tmp2].type === 'time')
                    {
                        var strh = model[tmp2].value;
                        var hh = strh.split(':')[0];
                        var mm = strh.split(':')[1];
                        var d = new Date();
                        d.setHours(hh);
                        d.setMinutes(mm);
                        t.timeshow[tmp2] = {
                            'label' : d,
                        };
                    }

                    if(model[tmp2].type === 'image')
                    {
                        (function(tmp){

                            var uploader = new FileUploader({
                                url: 'http://cl.juyouhx.com/oss.php/oss/webuploader1?topdir=aa&selfdir=bb'
                            });

                            t.imageshow[tmp] = {
                                'uploader' : uploader
                            };

                            uploader.filters.push({
                                name: 'imageFilter',
                                fn: function(item, options) {
                                    var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                                    return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
                                }
                            }); 

                            uploader.onSuccessItem = function(fileItem, response, status, headers) {
                                t.result[tmp] = response.savename;
                            };

                        })(tmp2);
                    }

                    t.data.push(model[tmp2]);
                }
            }
            //3.如果该商品有这个渠道的信息
            if(angular.isDefined(goodsChannelObj[t.templete_code])){

                var gc = goodsChannelObj[t.templete_code];
                for(var k = 0; k < t.data.length; k++){
                    var kk = t.data[k];

                    if(angular.isDefined(gc[kk.id])){
                        if(kk.type == 'number'){
                            t.result[kk.id] = parseInt(gc[kk.id]);
                        }else if(kk.type == 'float'){
                            t.result[kk.id] = parseFloat(gc[kk.id]);
                        }else{
                            t.result[kk.id] = gc[kk.id];
                        }
                    }
                }
            }
        }
        $scope.objs = channelList;
    });


	//保存渠道
	$scope.savechannel = function(channel){

        var channelcode = channel.templete_code;
        console.log(channelcode);
        console.log(item.goods_code);

        if(angular.isDefined(channel.result['tpsp.period_model_valid_week'])){
            channel.result['tpsp.period_model_valid_week'] = '1,2';
        }

        //将所有时间对象处理成返回的字符串
        angular.forEach(channel.dateshow, function(value, key){
            if(angular.isDate(value.label)){
                channel.result[key] = date2str(value.label);
            }else{
                channel.result[key] = value.label;
            }
        });

        var url = '/api/ac/tc/goodsChannelInfo/create';
        channel.result['goods_id'] = item.goods_code;
        channel.result['templete_id'] = channelcode;

        $resource(url, {}, {}).save(channel.result, function(res){
            console.log(res);
            if(res.errcode === 0){
                alert(res.data.describe);
                
            }else{
                alert(res.errmsg);
            }
        });

    };

	
    //商品上架
	$scope.start = function(channel){

		var para = {
			'goods_code' : item.goods_code,
			'channel_code' : channel.templete_code,
		};
		console.log(para);
		$resource('/api/ac/tc/product/create', {}, {}).save(para, function(res){
            console.log(res);
            if(res.errcode === 0){
                alert('上架成功');
                
            }else{
                alert(res.errmsg);
            }
        });

	};
	
};