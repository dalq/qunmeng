module.exports = function($scope, tableconfig, $state){

	tableconfig.start($scope, {
		'url' : '/api/ac/tc/channelTemplete/list',
		'col' : [
			{'title' : '渠道模版编号', 'col' : 'templete_code'},
			{'title' : '渠道模板名称', 'col' : 'templete_name'},
			{'title' : '操作', 'col' : 'btn'},
		],
		'btn' : [
			{'title' : '查看详情', 'onclick' : 'info'},
			{'title' : '编辑', 'onclick' : 'edit'},
			{'title' : '删除', 'onclick' : 'delete'},
		],
		'search' : [
			{'title' : '名称', 'type' : 'txt', 'name' : 'templete_name', 'show' : true},
			{'title' : '编号', 'type' : 'txt', 'name' : 'templete_code', 'show' : true},
		],
		'title' : '渠道模板列表',
		'info' : {
			'to' : 'app.channelTemp_info',
		},
		'delete' : {
			'url' : '/api/ac/tc/channelTemplete/delete',
		},
		'edit' : {
			'to' : 'app.channelTemp_edit'
		}
	});
	$scope.table = tableconfig;

};