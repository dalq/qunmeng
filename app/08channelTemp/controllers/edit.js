module.exports = function($scope, formconfig, $stateParams, model){

	var id = $stateParams.id;

	formconfig.start({
		'title' : '渠道模板详情',
		'formtitle' : '渠道模板基本信息',
		'elements' : model(),
		'info' : {
			'url' : '/api/ac/tc/channelTemplete/info',
			'para' : {'id' : id}
		},
		'save' : {
			'url' : '/api/ac/tc/channelTemplete/update',
			'to' : 'app.channelTemp_list',
			'para' : {'id' : id}
		}
	}, $scope);

	$scope.form = formconfig;

};