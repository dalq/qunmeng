module.exports = function($scope, formconfig, model){

	formconfig.start({
		'title' : '创建渠道模板',
		'formtitle' : '渠道模版基本信息',
		'elements' : model(),
		'save' : {
			'url' : '/api/ac/tc/channelTemplete/create',
			'to' : 'app.channelTemp_list',
		}
	}, $scope);

};