var App = angular.module('channelTemp', []);

App.config(require('./router'));
App.factory('channelTempservice', require('./service'));

App.controller('channelTemplist',require('./controllers/list'));
App.controller('channelTempcreate',require('./controllers/create'));
App.controller('channelTempinfo',require('./controllers/info'));
App.controller('channelTempedit',require('./controllers/edit'));
//商品的渠道信息
App.controller('goodschannellist',require('./controllers/goodschannellist'));


module.exports = App;