/**
* 子模块路由
* dlq
*/

var router = function ($urlRouterProvider, $stateProvider) {

    $stateProvider

        //渠道列表
        .state('app.channelTemp_list', {
            url: "/channelTemp/list.html",
            views: {
                'main@' : {
                    template : require('../996tpl/views/table.html'),
                    controller : 'channelTemplist',
                }
            },
            resolve:{
                tableconfig : function(tableservice){
                    return tableservice.tableconfig();
                },
            }
        })

        //创建渠道
        .state('app.channelTemp_create', {
            url: "/channelTemp/create.html",
            views: {
                'main@' : {
                    template : require('../996tpl/views/form.html'),
                    controller : 'channelTempcreate',
                }
            },
            resolve:{
                formconfig : function(formservice){
                    return formservice.formconfig();
                },
                model : function(channelTempservice){
                    return channelTempservice.model;
                }
            }
        })
        //渠道详情
        .state('app.channelTemp_info', {
            url: "/channelTemp/info/:id",
            views: {
                'main@' : {
                    template : require('../996tpl/views/form.html'),
                    controller : 'channelTempinfo',
                }
            },
            resolve:{
                formconfig : function(formservice){
                    return formservice.formconfig();
                },
                model : function(channelTempservice){
                    return channelTempservice.model;
                }
            }
        })

        //渠道编辑
        .state('app.channelTemp_edit', {
            url: "/channelTemp/edit/:id",
            views: {
                'main@' : {
                    template : require('../996tpl/views/form.html'),
                    controller : 'channelTempedit',
                }
            },
            resolve:{
                formconfig : function(formservice){
                    return formservice.formconfig();
                },
                model : function(channelTempservice){
                    return channelTempservice.model;
                }
            }
        })

        //商品的渠道信息
        .state('app.goods_channel', {
            url: "/goodschannel/:goodsid",
            template : require('./views/goodschannellist.html'),
            'size' : 'lg',
            controller : 'goodschannellist',
            // resolve : {  

            // }  
        })

        ;

};

module.exports = router;