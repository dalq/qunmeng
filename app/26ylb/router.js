 /**
 * 子模块路由
 * DHpai
 */

var router = function($urlRouterProvider, $stateProvider){


 	$stateProvider


    //分类管理
    .state('app.category', {
        url: "/ylb/category",
        views: {
            'main@' : {
                template : require('../996tpl/views/table.html'),
                controller : 'category',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
        }
    })

     //字典管理
    .state('app.dicc', {
        url: "/ylb/dicc",
        views: {
            'main@' : {
                template : require('../996tpl/views/table.html'),
                controller : 'dicc',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
        }
    })

    //文章管理
    .state('app.artical', {
        url: "/ylb/artical",
        views: {
            'main@' : {
                template : require('../996tpl/views/table.html'),
                controller : 'artical',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
        }
    })



    //添加分类
    .state('app.addYlbCategory', {
        url: "/ylb/addYlbCategory/:id",
        views: {
            'main@' : {
                template : require('./views/addYlbCategory.html'),
                controller : 'addYlbCategory',
            }
        },
        resolve:{
            saveCategory : function(ylbservice){
                return ylbservice.saveCategory();
            },
            findPidList : function(ylbservice){
                return ylbservice.findPidList();
            },
            findDictionaryList : function(ylbservice){
                return ylbservice.findDictionaryList();
            },
            getCategory : function(ylbservice){
                return ylbservice.getCategory();
            }
            
        }
    })

    //添加字典
    .state('app.addYlbDicc', {
        url: "/ylb/addYlbDicc/:id",
        views: {
            'main@' : {
                template : require('./views/addYlbDicc.html'),
                controller : 'addYlbDicc',
            }
        },
        resolve:{
            saveDictionary : function(ylbservice){
                return ylbservice.saveDictionary();
            },
            getDictionary : function(ylbservice){
                return ylbservice.getDictionary();
            }
            
        }
    })

    //添加文章
    .state('app.addYlbArtical', {
        url: "/ylb/addYlbArtical/:id",
        views: {
            'main@' : {
                template : require('./views/addYlbArtical.html'),
                controller : 'addYlbArtical',
            }
        },
        resolve:{
            saveArticle : function(ylbservice){
                return ylbservice.saveArticle();
            },
            getArticle : function(ylbservice){
                return ylbservice.getArticle();
            },
            findPidList : function(ylbservice){
                return ylbservice.findPidList();
            }
            
        }
    })


	;

};

module.exports = router;