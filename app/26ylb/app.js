var App = angular.module('ylb', []);

App.config(require('./router'));

//service
App.factory('ylbservice', require('./service'));

//Controllers
App.controller('category', require('./controllers/category'));
App.controller('artical', require('./controllers/artical'));
App.controller('dicc', require('./controllers/dicc'));
App.controller('addYlbCategory', require('./controllers/addYlbCategory'));
App.controller('addYlbDicc', require('./controllers/addYlbDicc'));
App.controller('addYlbArtical', require('./controllers/addYlbArtical'));




module.exports = App;