module.exports = function($scope, tableconfig, $state, $resource){

	tableconfig.start($scope, {
		'url' : '/api/as/ic/dictionary/findDictionaryList',
		'col' : [
			{'title' : '字典标题', 'col' : 'title'},
			{'title' : 'key值', 'col' : 'key'},
			{'title' : 'type', 'col' : 'type'},
			{'title' : '操作', 'col' : 'btn'},
		],
		'btn' : [
			{'title' : '编辑', 'onclick' : 'edit'},
			{'title' : '删除', 'onclick' : 'delete'},
		],
		// 'btns' : [
		// 	{'name' : '添加分类', 'click' : 'add'},
		// ],
		'search' : [
			{'title' : '名称', 'type' : 'txt', 'name' : 'title', 'show' : true},
			{'title' : '编号', 'type' : 'txt', 'name' : 'code', 'show' : true},
			{'title' : '城市', 'type' : 'date', 'name' : 'city'},
			{'title' : '类型', 'type' : 'txt', 'name' : 'type'},
		],
		'title' : '分类列表',
		// 'info' : {
		// 	'to' : 'app.view_info',
		// },
		'delete' : {
			'url' : '/api/as/ic/dictionary/updateDel',
		},
		'edit' : {
			'to' : 'app.addYlbDicc'
		}
	});
	$scope.table = tableconfig;

	
};






