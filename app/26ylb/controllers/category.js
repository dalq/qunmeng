module.exports = function($scope, tableconfig, $state, $resource){

	tableconfig.start($scope, {
		'url' : '/api/as/ic/category/findCategoryList',
		'col' : [
			{'title' : '分类名称', 'col' : 'title'},
			{'title' : '上级id', 'col' : 'pid'},
			{'title' : '分类属性', 'col' : 'dictionary_title'},
			{'title' : '排序', 'col' : 'sort'},
			{'title' : '状态', 'col' : 'state'},
			{'title' : '操作', 'col' : 'btn'},
		],
		'btn' : [
			{'title' : '编辑', 'onclick' : 'edit'},
			{'title' : '启动', 'onclick' : 'start', 'show' : {'state' : '2'}},
			{'title' : '禁用', 'onclick' : 'disabled1', 'show' : {'state' : '1'}},
		],
		// 'btns' : [
		// 	{'name' : '添加分类', 'click' : 'add'},
		// ],
		'search' : [
			{'title' : '名称', 'type' : 'txt', 'name' : 'title', 'show' : true},
			{'title' : '编号', 'type' : 'txt', 'name' : 'code', 'show' : true},
			{'title' : '城市', 'type' : 'date', 'name' : 'city'},
			{'title' : '类型', 'type' : 'txt', 'name' : 'type'},
		],
		'title' : '分类列表',
		// 'info' : {
		// 	'to' : 'app.view_info',
		// },
		'delete' : {
			'url' : '/api/ac/tc/placeView/delete',
		},
		'edit' : {
			'to' : 'app.addYlbCategory'
		}
	});
	$scope.table = tableconfig;


	$scope.start = function(obj){

		$resource('/api/as/ic/category/updateStartState', {}, {}).save({'id' : obj.id}, function(res){
			if(res.errcode === 0){
				tableconfig.search();
			}else{
				alert(res.errmsg);
			}
		});
	};

	$scope.disabled1 = function(obj){
		$resource('/api/as/ic/category/updateDisableState', {}, {}).save({'id' : obj.id}, function(res){
			if(res.errcode === 0){
				tableconfig.search();
			}else{
				alert(res.errmsg);
			}
		});
	}

};






