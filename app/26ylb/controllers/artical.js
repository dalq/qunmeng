module.exports = function($scope, tableconfig, $state, $resource){

	tableconfig.start($scope, {
		'url' : '/api/as/ic/article/findArticleList',
		'col' : [
			{'title' : '文章标题', 'col' : 'title'},
			{'title' : '排序', 'col' : 'sort'},
			{'title' : '状态', 'col' : 'state'},
			{'title' : '创建时间', 'col' : 'createtime'},
			{'title' : '修改时间', 'col' : 'updatetime'},			
			{'title' : '操作', 'col' : 'btn'},
		], 
		'btn' : [
			{'title' : '编辑', 'onclick' : 'edit'},
			{'title' : '启用', 'onclick' : 'start', 'show' : {'state' : '2'}},
			{'title' : '禁用', 'onclick' : 'disbaled1', 'show' : {'state' : '1'}},			
		], 
		// 'btns' : [
		// 	{'name' : '添加分类', 'click' : 'add'},
		// ],
		'search' : [
			{'title' : '名称', 'type' : 'txt', 'name' : 'title', 'show' : true},
			{'title' : '编号', 'type' : 'txt', 'name' : 'code', 'show' : true},
			{'title' : '城市', 'type' : 'date', 'name' : 'city'},
			{'title' : '类型', 'type' : 'txt', 'name' : 'type'},
		],
		'title' : '文章列表',
		// 'info' : {
		// 	'to' : 'app.view_info',
		// },
		'delete' : {
			'url' : '/api/as/ic/dictionary/updateDel',
		},
		'edit' : {
			'to' : 'app.addYlbArtical'
		}
	});
	$scope.table = tableconfig;
	$scope.start = function(obj){

		$resource('/api/as/ic/article/updateStartState', {}, {}).save({'id' : obj.id}, function(res){
			if(res.errcode === 0){
				tableconfig.search();
			}else{
				alert(res.errmsg);
			}
		});
	};

	$scope.disbaled1 = function(obj){
		alert(obj.id);
		$resource('/api/as/ic/category/updateDisableState', {}, {}).save({'id' : obj.id}, function(res){
			if(res.errcode === 0){
				tableconfig.search();
			}else{
				alert(res.errmsg);
			}
		});
	}

	
};






