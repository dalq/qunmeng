var App = angular.module('admin', []);

App.config(require('./router'));

//service
App.factory('adminservice', require('./service'));

//Controllers
App.controller('adminUse', require('./controllers/adminUse'));


module.exports = App;