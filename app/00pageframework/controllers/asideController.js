module.exports = function($scope, $rootScope, toaster, selectMenu,$cookies,$resource, $state, angularPermission){

	// $scope.menuobj = $rootScope.menudata;

	$scope.noRule = function(){
		// alert("请配置链接地址！");
		toaster.error({ title: "提示", body: "请配置链接地址！" });
	}

	// $scope.isCollapsed = true;

	//切换菜单
	$scope.$on('changeMenu', function () {
		var menu_id = selectMenu.value.id;
		$resource('/api/ac/sc/menuService/newMenulist', {}, {}).get({'menu_id' : menu_id}, function(res){
			if(res.errcode === 0){
				$scope.menuobj = res.data;
				var menudata = res.data;
				var permissions = new Array();
				
				for (var i = 0; i < res.data.list.length; i++) {
					var tmp = res.data.list[i];
					if (angular.isArray(tmp.list)) {
						for (var j = 0; j < tmp.list.length; j++) {
							var tmp1 = tmp.list[j];
							if(tmp1.hasOwnProperty('permission') && tmp1.permission){
								var permissionArr = [];
								var permissionArr = tmp1.permission.split(',');
								for (var index = 0; index < permissionArr.length; index++) {
									var element = permissionArr[index];
									permissions.push(element)
								}
								// permissions.push(tmp1.permission)
							}
						}
					}
				}
				$rootScope.userPermissionList = permissions;
				angularPermission.setPermissions($rootScope.userPermissionList);

			} else if (res.errcode === 1001) {
				if($rootScope.info.company_sys_area == 'LW'){
					//window.location = "/qunmeng.html";
					window.location = "/login.html";
				}else{
					window.location = "/login.html";
				}
			} else if(res.errcode == 1002){
				alert('菜单无权限!');
			} else {
				alert(res.errmsg);
			}
		});


		// $.ajax({
		// 	url: '/api/ac/sc/menuService/newMenulist',
		// 	type: "GET",
		// 	data:{
		// 		'menu_id' : menu_id,
		// 	},
		// 	dataType: 'json'
		// }).then(function (res) {
		// 	if (res.errcode === 0) {
		// 		$scope.menuobj = res.data;
		// 		console.log($scope.menuobj);
		// 	} else if (res.errcode === 1001) {
		// 		window.location = "/login.html";
		// 	} else if (res.errcode === 1002) {
		// 		alert('菜单无权限!');
		// 	} else {
		// 		alert('菜单' + res.errmsg);
		// 	}
		// });
	});


};