/**
 * 子模块路由
 * dlq
 */

var router = function($urlRouterProvider, $stateProvider){

    $stateProvider
        
      .state('app.addcoupon', {
        url: '/addcoupon',
        views: {
            'main@' : {
                controller : 'addcoupon',
                template: require('./views/addcoupon.html'),
            }
        },
        resolve:{
            // namelist : function(linestatisticsservice){
            //     return linestatisticsservice.namelist();
            // },
        }
        
      })
      .state('app.couponprojectlist',{
          url:'/couponprojectlist',
          views:{
              'main@' : {
                  controller : 'couponprojectlist',
                  template : require('./views/couponprojectlist.html'),
              }
          },
          resolve:{
              
          }
      })
      .state('app.addcouponproject',{
          url:'/addcouponproject/:p_code/:status',
          views:{
              'main@' : {
                  controller : 'addcouponproject',
                  template : require('./views/addcouponproject.html'),
              }
          },
          resolve:{
            getDate : function(utilservice){
		             return utilservice.getDate;
		    },
            date2str: function (utilservice) {
                    return utilservice.getDate;
            },
            str2date: function (utilservice) {
                return utilservice.str2date;
            },
              
          }
      })
      //优惠券列表
      .state('app.couponlist', {
        url: '/couponlist/:type',
        views: {
            'main@' : {
                controller : 'couponlist',
                template: require('./views/couponlist.html'),
            }
        },
        resolve:{
            // namelist : function(linestatisticsservice){
            //     return linestatisticsservice.namelist();
            // },
        }
        
      })

       //优惠券订单列表
      .state('app.coupon_orderlist', {
        url: '/coupon_orderlist',
        views: {
            'main@' : {
                controller : 'coupon_orderlist',
                template: require('./views/coupon_orderlist.html'),
            }
        },
        resolve:{
            
        }

      })

       //优惠券规则列表
      .state('app.rulelist', {
        url: '/rulelist',
        views: {
            'main@' : {
                controller : 'rulelist',
                template: require('./views/rulelist.html'),
            }
        },
        resolve:{
            
        }

      })

      //优惠券类别列表
      .state('app.coupontype_list', {
        url: '/coupontype_list',
        views: {
            'main@' : {
                controller : 'coupontype_list',
                template: require('./views/coupontype_list.html'),
            }
        },
        resolve:{
            
        }

      }) 

      

     

};

module.exports = router;