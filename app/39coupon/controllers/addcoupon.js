module.exports = function ($scope, $state, $stateParams,$resource,$modal,$modalInstance,toaster,item) {
    $scope.info = {
        'coupon_code' : '',
        'name' : '',
        'money' : '',
        'description' : ''
    }
    if(item){
        var id = item.id;
        $scope.info.coupon_code = id;
    }

    
    if(id){
        $resource('/api/as/puc/couponproduct/getCouponById', {}, {}).save({'coupon_code' : id}, function(res){
            if(res.errcode === 0){   
                $scope.info.name = res.data.name;
                $scope.info.money = res.data.money / 100;
                $scope.info.description = res.data.description;
                console.log($scope.info.name);
            }else{
                toaster.error({title:"",body:res.errmsg});
            }
        });
    }
    $scope.submit = function(){
        var para = {
            'coupon_code' : $scope.info.coupon_code,
            'name' : $scope.info.name,
            'money' : $scope.info.money * 100,
            'description' : $scope.info.description
        }
        if(id){
            $resource('/api/ac/puc/couponProduct/updateCouponList', {}, {}).save(para, function(res){
                if(res.errcode === 0){   
                    toaster.success({title:"",body:"恭喜你💐,修改优惠券成功"});
                    $modalInstance.close();
                }else{
                    toaster.error({title:"",body:res.errmsg});
                }
            });
        } else {
            $resource('/api/as/puc/couponproduct/create', {}, {}).save(para, function(res){
                if(res.errcode === 0){   
                    toaster.success({title:"",body:"恭喜你💐,创建优惠券成功"});
                    $modalInstance.close();
                }else{
                    toaster.error({title:"",body:res.errmsg});
                }
            });
        }
     
    }



};
