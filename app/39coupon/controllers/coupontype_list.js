module.exports = function ($scope, $state, $stateParams,$resource,$modal, toaster) {
    /* 分页
     * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10         //每页显示几条
    $scope.info = {};
    $scope.getlist=function(){ 
        // var para = {
        //     pageNo:$scope.bigCurrentPage, 
        //     pageSize:$scope.itemsPerPage
        // };
        // para = angular.extend($scope.info, para);
        $resource('/api/as/puc/couponproduct/findTypeList', {}, {}).save($scope.info,function(res){
            if(res.errcode === 0){
                console.log(res);
                $scope.objs=res.data;
                $scope.bigTotalItems = res.data.totalRecord;
                
            }else{
                alert(res.errmsg);
            }
        });
    }
    $scope.getlist();
	
    $scope.add=function(item){
        $scope.item = item;
        $scope.flag = 'type';
        var modalInstance = $modal.open({
            template: require('../views/addruletype.html'),
            controller: 'addruletype',
            size: 'lg',
            resolve: {
                item: function () {
                    return $scope.item;
                },
                flag: function () {
                    return $scope.flag;
                },
                
            }
        });
        modalInstance.result.then(function (showResult) {	
            $scope.getlist();
        });
    }
    $scope.edit=function(item){
        $scope.item = item;
        $scope.flag = 'type';
        var modalInstance = $modal.open({
            template: require('../views/addruletype.html'),
            controller: 'addruletype',
            size: 'lg',
            resolve: {
                item: function () {
                    return $scope.item;
                },
                flag: function () {
                    return $scope.flag;
                },
                
            }
        });
        modalInstance.result.then(function (showResult) {	
            $scope.getlist();
        });
    }
    $scope.delete=function(id){
        console.log(id);
        if(confirm("确认删除吗")){
            $resource('/api/as/puc/couponproduct/updateDelCtype', {}, {}).save({'id':id},function(res){
                if(res.errcode === 0){
                    toaster.success({title:"",body:"删除成功"});
                    $scope.getlist();
                }else{
                    toaster.error({title:"",body:res.errmsg});
                }
            });
        }
        
        
    }
    
};