module.exports = function ($scope, $state, $stateParams,$resource,$modal, toaster) {
	$scope.maxSize = 5;             //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10
    $scope.info={};
    $scope.search=function(){
        var para = {
            pageNo:$scope.bigCurrentPage, 
            pageSize:$scope.itemsPerPage
        };
        para=angular.extend($scope.info,para);
        $resource('/api/as/puc/couponproduct/findCouProductList', {}, {}).save(para,function(res){
            if(res.errcode === 0){
                console.log(res);
                $scope.a=res.data.results;
                $scope.bigTotalItems = res.data.totalRecord;
                
            }else{
                alert(res.errmsg);
            }
        });
    }
    $scope.search();
	
    $scope.add=function(){
        $state.go('app.addcouponproject');
    }
    $scope.edit=function(p_code, status){
        $state.go('app.addcouponproject', {'p_code' : p_code, 'status' : status});
    }
    $scope.del=function(id){
        if(confirm("确认删除吗")){
            $resource('/api/ac/puc/couponProduct/updateCouProductDel', {}, {}).save({'pcode':id},function(res){
                if(res.errcode === 0){
                    $scope.search();
                }else{
                    alert(res.errmsg);
                }
            });
        }
        
        
    }
    $scope.online = function(product_code){
        $resource('/api/as/puc/couponproduct/updateproductTop', {}, {}).save({'product_code':product_code},function(res){
            if(res.errcode === 0){
                toaster.success({title:"",body:"上架成功"});
                $scope.search();
            }else{
                toaster.error({title:"",body:res.errmsg});
            }
        })
    }
    $scope.offline = function(product_code){
        $resource('/api/as/puc/couponproduct/updateproductDown', {}, {}).save({'product_code':product_code},function(res){
            if(res.errcode === 0){
                toaster.success({title:"",body:"上架成功"});
                $scope.search();
            }else{
                toaster.error({title:"",body:res.errmsg});
            }
        })
    }
};