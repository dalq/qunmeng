module.exports = function ($scope, $state, $stateParams,$resource,$modal,$modalInstance,toaster,item, flag) {
    console.log(item);
    console.log(flag);
    $scope.info = {
        'name' : ''
    };
    $scope.type_para = {
        'type_name' : ''
    }
    $scope.rule_para = {
        'rule_name' : ''
    }
    if(item){
        $scope.info = item;
        if(flag == 'type'){
            $scope.info.name = item.type_name;
        } else {
            $scope.info.name = item.rule_name;
        }
            
    }
    $scope.ok = function(){
        if(item){
            var para = {
                'id' : item.id
            }
            var url = '';
            if(flag == 'type'){
                $scope.type_para.type_name = $scope.info.name;
                para = angular.extend($scope.type_para, para);
                url = '/api/as/puc/couponproduct/updateCtype';
            } else if(flag == 'rule'){
                $scope.rule_para.rule_name = $scope.info.name;
                para = angular.extend($scope.rule_para, para);
                url = '/api/as/puc/couponproduct/updateRule';
            }
            $resource(url, {}, {}).save(para, function(res){
                if(res.errcode !== 0){ 
                    toaster.error({title:"",body:res.errmsg});
                }else{
                    console.log(res);
                    toaster.success({title:"",body:"修改成功"});
                    $modalInstance.close();

                }
            });
        } else {
            var para = {};
            var url = '';
            if(flag == 'type'){
                $scope.type_para.type_name = $scope.info.name;
                para = angular.extend($scope.type_para, para);
                url = '/api/as/puc/couponproduct/createCType';
            } else if(flag == 'rule'){
                $scope.rule_para.rule_name = $scope.info.name;
                para = angular.extend($scope.rule_para, para);
                url = '/api/as/puc/couponproduct/createCouRule';
            }
            $resource(url, {}, {}).save(para, function(res){
                if(res.errcode !== 0){ 
                    toaster.error({title:"",body:res.errmsg});
                }else{
                    console.log(res);
                    toaster.success({title:"",body:"创建成功"});
                    $modalInstance.close();
                }
            });
        }
        
    }

    $scope.cancel = function(){
        $modalInstance.close();
    }

}