module.exports = function ($scope, $state, $stateParams,$resource,$modal,toaster, date2str, str2date) {
    var p_code = $stateParams.p_code;
    $scope.status = $stateParams.status;
    console.log(p_code);
    $scope.abc = '';
    $scope.info = {};
    $scope.array = [];
    $scope.para = {
        'product_name' : '',
        'stock_type' : '',
        'stock_num' : 0,
        'max_limit' : '',
        'max_limit_num' : 0
    }
    $scope.product_code = '';
    $scope.rulearr = [];
    // 优惠券产品详情
    $scope.getinfo = function(){
        if(p_code){
            $resource('/api/ac/puc/couponProduct/getCouProductByCode', {}, {}).
            save({'pcode' : p_code},function(res) {
                if (res.errcode !== 0) {
                    toaster.error({title:"",body:res.errmsg});
                    return;
                }
                $scope.product_code = res.data.getcouproduct.CODE;
                // 主表信息
                $scope.para = res.data.getcouproduct;
                //子表信息
                $scope.array = res.data.getcousub;
                
                $scope.array.forEach(function(element) {
                    console.log(element);
                    //获取优惠券名称
                    $resource('/api/as/puc/couponproduct/findCouponnopageList', {}, {}).save({}, function(res){
                        if(res.errcode !== 0){ 
                            toaster.error({title:"",body:res.errmsg});
                        }else{
                            var namearr = res.data;
                            for(var i = 0; i < namearr.length; i++){
                                if(element.coupon_code == namearr[i].id){
                                    element.coupon_name = namearr[i].name;
                                }
                            }
                        }
                    });
                    // 获取优惠券规则
                    $resource('/api/as/puc/couponproduct/findRuleList', {}, {}).save({}, function(res){
                        if(res.errcode !== 0){ 
                            toaster.error({title:"",body:res.errmsg});
                        }else{
                            $scope.rulearr = res.data;
                            for(var i = 0; i < $scope.rulearr.length; i++){
                                if(element.rule == $scope.rulearr[i].id){
                                    element.rule_name = $scope.rulearr[i].rule_name;
                                }
                            }
                        }
                    });
                    // 获取优惠券类别
                    $resource('/api/as/puc/couponproduct/findTypeList', {}, {}).save({}, function(res){
                        if(res.errcode !== 0){ 
                            toaster.error({title:"",body:res.errmsg});
                        }else{
                            var typearr = res.data;
                            for(var i = 0; i < typearr.length; i++){
                                if(element.type == typearr[i].id){
                                    element.type_name = typearr[i].type_name;
                                }
                            }
                        }
                    });

                }, this);

            })
        }
    }

    $scope.getinfo();
    
    
    
    $scope.submit = function(){ 
        // 编辑产品的时候创建新的
        if(p_code){
            $resource('/api/ac/puc/couponProduct/createCouProduct', {}, {}).
            save($scope.para,function(res) {
                console.log($scope.para);
                if (res.errcode !== 0) {
                    toaster.error({title:"",body:res.errmsg});
                    return;
                }
                console.log(res);
                $state.go('app.couponprojectlist');
            })
        } else {
            // 添加的时候创建
            $resource('/api/ac/puc/couponProduct/createCouProduct', {}, {}).
            save($scope.info,function(res) {
                console.log($scope.info);
                if (res.errcode !== 0) {
                    toaster.error({title:"",body:res.errmsg});
                    return;
                }
                console.log(res);
                $state.go('app.couponprojectlist');
            })
        }
            
    }

    // 修改
    $scope.savesubmit = function(){
        if(p_code){
            var param = {
                'pcode' : p_code
            }
            param = angular.extend($scope.para, param);
            $resource('/api/ac/puc/couponProduct/updateConProduct', {}, {}).
            save(param,function(res) {
                console.log(param);
                if (res.errcode !== 0) {
                    toaster.error({title:"",body:res.errmsg});
                    return;
                }
                console.log(res);
                toaster.success({title:"",body:"修改产品成功"});
                // $state.go('app.couponprojectlist');
            })
        } else {
            $resource('/api/ac/puc/couponProduct/createCouProduct', {}, {}).
            save($scope.para,function(res) {
                console.log($scope.para);
                if (res.errcode !== 0) {
                    toaster.error({title:"",body:res.errmsg});
                    return;
                }
                console.log(res);
                $scope.product_code = res.data;
                toaster.success({title:"",body:"创建产品成功"});
                $scope.ceratesuccess = '1';
                // $state.go('app.couponprojectlist');
            })
        }
        
    }

    $scope.add = function(item){
        $scope.item = item;
        $scope.flag = 'add';
        var modalInstance = $modal.open({
            template: require('../views/add_coupon.html'),
            controller: 'add_coupon',
            size: 'lg',
            resolve: {
                date2str: function () {
                    return date2str;
                },
                str2date: function () {
                    return str2date;
                },
                item: function () {
                    return $scope.item;
                },
                p_code: function () {
                    return p_code;
                },
                product_code: function () {
                    return $scope.product_code;
                },
                flag: function () {
                    return $scope.flag;
                },
                
            }
        });
        modalInstance.result.then(function (showResult) {	
            $scope.getinfo();
            //添加回来之后要把值带回来和原来的一起显示在列表
            console.log(showResult);
           // 获取优惠券类别
           $resource('/api/as/puc/couponproduct/findTypeList', {}, {}).save({}, function(res){
                if(res.errcode !== 0){ 
                    toaster.error({title:"",body:res.errmsg});
                }else{
                    var typearr = res.data;
                    console.log(typearr);
                    for(var i = 0; i < typearr.length; i++){
                        if(showResult.type === typearr[i].id){
                            showResult.type = typearr[i].type_name;
                            console.log('name' + showResult.type);
                        }
                    }
                }
            });
             // 获取优惠券规则
             $resource('/api/as/puc/couponproduct/findRuleList', {}, {}).save({}, function(res){
                if(res.errcode !== 0){ 
                    toaster.error({title:"",body:res.errmsg});
                }else{
                    $scope.rulearr = res.data;
                    for(var i = 0; i < $scope.rulearr.length; i++){
                        if(showResult.rule == $scope.rulearr[i].id){
                            showResult.rule = $scope.rulearr[i].rule_name;
                        }
                    }
                }
            });
            $scope.array.push(showResult);
           
        });
    }

    $scope.edit = function(item){
        $scope.item = item;
        $scope.flag = 'edit';
        var modalInstance = $modal.open({
            template: require('../views/add_coupon.html'),
            controller: 'add_coupon',
            size: 'lg',
            resolve: {
                date2str: function () {
                    return date2str;
                },
                str2date: function () {
                    return str2date;
                },
                item: function () {
                    return $scope.item;
                },
                p_code: function () {
                    return p_code;
                },
                product_code: function () {
                    return $scope.product_code;
                },
                flag: function () {
                    return $scope.flag;
                },
                
            }
        });
        modalInstance.result.then(function (showResult) {	
            $scope.getinfo();
            $scope.aaa = 'hide';
            
        });
    }

    $scope.delete = function(id){
        $resource('/api/as/puc/couponproduct/updateCouSubSmileDel', {}, {}).
        save({'id' : id, 'pcode' : $scope.product_code},function(res) {
            if (res.errcode !== 0) {
                toaster.error({title:"",body:res.errmsg});
                return;
            }
            console.log(res);
            toaster.success({title:"",body:"删除成功"});
             $scope.getinfo();

            
        })
    }
    
}