module.exports = function ($scope, $state, $stateParams,$resource,$modal,$modalInstance,toaster,date2str,str2date,item,p_code,product_code, flag) {
    console.log(flag);
    $scope.flag = flag;
    $scope.vm = {
        'date' : '',
        'options' : {
            // format: "YYYY-MM-DD",
            locale : 'zh-cn'
        }
    }
    $scope.vm1 = {
        'date' : '',
        'options' : {
            // format: "YYYY-MM-DD",
            locale : 'zh-cn'
        }
    }

    //获取优惠券名称
    $resource('/api/as/puc/couponproduct/findCouponnopageList', {}, {}).save({}, function(res){
        if(res.errcode !== 0){ 
            toaster.error({title:"",body:res.errmsg});
        }else{
            $scope.namearr = res.data;
            console.log(res);
        }
    });

    // 优惠券规则
    $resource('/api/as/puc/couponproduct/findRuleList', {}, {}).save({}, function(res){
        if(res.errcode !== 0){ 
            toaster.error({title:"",body:res.errmsg});
        }else{
            $scope.rulearr = res.data;
        }
    });

    // 优惠券类型
    $resource('/api/as/puc/couponproduct/findTypeList', {}, {}).save({}, function(res){
        if(res.errcode !== 0){ 
            toaster.error({title:"",body:res.errmsg});
        }else{
            $scope.typearr = res.data;
        }
    });
    $scope.searchform = {
        'name' : '',
        'money' : '',
        'description' : ''
    };
   console.log()
    $scope.info = {
        'product_code' : product_code,
        'coupon_code' : '',
        'name' : '',
        'money' : '',
        'description' : ''
    }
    if(item){
        $scope.info = item;
        $scope.searchform.name = item.coupon_name;
        $scope.vm.date = item.start_date;
        $scope.vm1.date = item.end_date;
        //获取优惠券名称
        $resource('/api/as/puc/couponproduct/findCouponnopageList', {}, {}).save({}, function(res){
            if(res.errcode !== 0){ 
                toaster.error({title:"",body:res.errmsg});
            }else{
                $scope.namearr = res.data;
                for(var i = 0; i < $scope.namearr.length; i++){
                    console.log('名字' + item.coupon_name);
                    if(item.coupon_name === $scope.namearr[i].name){
                        $scope.info.money = $scope.namearr[i].money / 100;
                        $scope.info.description = $scope.namearr[i].description;
                    }
                }
            }
        });

    }
    $scope.change = function(searchform){
        console.log(searchform);
        $scope.info.money = searchform.money / 100;
        $scope.info.description = searchform.description;
        $scope.info.name = searchform.name;
        $scope.info.coupon_code = searchform.id;
    }
    
    $scope.save = function(){
        $scope.info.money = $scope.info.money * 100;
        console.log($scope.info.money);
        if (typeof $scope.vm.date._d === 'string') {
            $scope.vm.date._d = $scope.vm.date._d;
        } else {
            $scope.vm.date._d = time2str($scope.vm.date._d);
        }
        if (typeof $scope.vm1.date._d === 'string') {
            $scope.vm1.date._d = $scope.vm1.date._d;
        } else {
            $scope.vm1.date._d = time2str($scope.vm1.date._d);
        }
        $scope.info.start_date = ($scope.vm.date._d);
        $scope.info.end_date = ($scope.vm1.date._d);
        // $scope.info.money = $scope.info.money * 100;
        if(item){
            var para = {
                'id' : item.id
            }
            para = angular.extend($scope.info,para);
            $resource('/api/as/puc/couponproduct/updateCouSubById', {}, {}).save(para, function(res){
                if(res.errcode !== 0){ 
                    toaster.error({title:"",body:res.errmsg});
                }else{
                    console.log(res);
                    toaster.success({title:"",body:"修改成功"});
                    $modalInstance.close($scope.info);
                }
            });
        } else {
            $resource('/api/as/puc/couponproduct/createConSubProductlist', {}, {}).save($scope.info, function(res){
                if(res.errcode !== 0){ 
                    toaster.error({title:"",body:res.errmsg});
                }else{
                    console.log(res);
                    toaster.success({title:"",body:"保存成功"});
                    $modalInstance.close($scope.info);
                }
            });
        }
        
        
    }


    function time2str(objDate) {
        if(angular.isDate(objDate))
        {
            var y = objDate.getFullYear();
            var m = objDate.getMonth();
            var d = objDate.getDate();
            var h = objDate.getHours();
            var mt = objDate.getMinutes();
            // var s = objDate.getSeconds();
            return y + '-' + (m + 1) + '-' + d + ' ' + h + ':' + mt;
        }
        else
        {
            return '错误格式';
        }
    }

    
    



		


};
