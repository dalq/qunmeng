module.exports = function ($scope, $state, $stateParams,$resource,$modal, toaster) {
    $scope.info = {};
    $scope.getlist=function(){
        $resource('/api/as/puc/couponproduct/findRuleList', {}, {}).save($scope.info,function(res){
            if(res.errcode === 0){
                console.log(res);
                $scope.objs=res.data;
                $scope.bigTotalItems = res.data.totalRecord;
                
            }else{
                toaster.error({title:"",body:res.errmsg});
            }
        });
    }
    $scope.getlist();
	
    $scope.add=function(item){
        $scope.item = item;
        $scope.flag = 'rule';
        var modalInstance = $modal.open({
            template: require('../views/addruletype.html'),
            controller: 'addruletype',
            size: 'lg',
            resolve: {
                item: function () {
                    return $scope.item;
                },
                flag: function () {
                    return $scope.flag;
                },
                
            }
        });
        modalInstance.result.then(function (showResult) {	
            $scope.getlist();
        });
    }
    $scope.edit=function(item){
        $scope.item = item;
        $scope.flag = 'rule';
        var modalInstance = $modal.open({
            template: require('../views/addruletype.html'),
            controller: 'addruletype',
            size: 'lg',
            resolve: {
                item: function () {
                    return $scope.item;
                },
                flag: function () {
                    return $scope.flag;
                },
                
            }
        });
        modalInstance.result.then(function (showResult) {	
            $scope.getlist();
        });
    }
    $scope.delete=function(id){
        if(confirm("确认删除吗")){
            $resource('/api/as/puc/couponproduct/updateRuleDel', {}, {}).save({'id':id},function(res){
                if(res.errcode === 0){
                    toaster.success({title:"",body:"删除成功"});
                    $scope.getlist();
                }else{
                    toaster.error({title:"",body:res.errmsg});
                }
            });
        }
        
        
    }
    // $scope.online = function(product_code){
    //     $resource('/api/as/puc/couponproduct/updateproductTop', {}, {}).save({'product_code':product_code},function(res){
    //         if(res.errcode === 0){
    //             toaster.success({title:"",body:"上架成功"});
    //             $scope.search();
    //         }else{
    //             toaster.error({title:"",body:res.errmsg});
    //         }
    //     })
    // }
    // $scope.offline = function(product_code){
    //     $resource('/api/as/puc/couponproduct/updateproductDown', {}, {}).save({'product_code':product_code},function(res){
    //         if(res.errcode === 0){
    //             toaster.success({title:"",body:"上架成功"});
    //             $scope.search();
    //         }else{
    //             toaster.error({title:"",body:res.errmsg});
    //         }
    //     })
    // }
};