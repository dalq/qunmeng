module.exports = function ($scope, $state, $stateParams,$resource,$modal,$modalInstance,toaster, item) {
    $resource('/api/as/puc/couponorder/findCouponOrderCodeAndClList', {}, {}).save({'order_code' : item.code}, function(res){
        if(res.errcode !== 0){   
            toaster.error({title:"",body:res.errmsg});
        }else{
            console.log(res);
            $scope.couponarr = res.data;
        }
    });
    $scope.cancel = function(){
        $modalInstance.close();
    }
    
}