module.exports = function ($scope, $state, $stateParams,$resource,$modal,toaster) {
 /* 分页
     * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10         //每页显示几条
    $scope.info = {};
    $scope.getlist = function () {
        var para = {
            pageNo:$scope.bigCurrentPage, 
            pageSize:$scope.itemsPerPage
        };
        para = angular.extend($scope.info, para);
        $resource('/api/as/puc/couponproduct/findCouponList', {}, {}).
        save(para,function(res) {
            console.log(para);
            if (res.errcode !== 0) {
                toaster.error({title:"",body:res.errmsg});
                return;
            }
            console.log(res);
            $scope.objs = res.data.results;
            $scope.bigTotalItems = res.data.totalRecord;

            
        })
    };
    
    $scope.getlist(); 

   
    $scope.add=function(item){
        console.log('add');
        $scope.item = item;
        var modalInstance = $modal.open({
            template: require('../views/addcoupon.html'),
            controller: 'addcoupon',
            size: 'lg',
            resolve: {
                item: function () {
                    return $scope.item;
                },
                
            }
        });
        modalInstance.result.then(function (showResult) {	
            $scope.getlist();
        });
    }
    $scope.edit=function(item){
        $scope.item = item;
        var modalInstance = $modal.open({
            template: require('../views/addcoupon.html'),
            controller: 'addcoupon',
            size: 'lg',
            resolve: {
                item: function () {
                    return $scope.item;
                },
                
            }
        });
        modalInstance.result.then(function (showResult) {	
            $scope.getlist();
        });
    }
    $scope.del=function(id){
        if(confirm("确认删除吗?")){
            $resource('/api/ac/puc/couponProduct/updateCouponDel', {}, {}).save({'coupon_code':id},function(res){
            if(res.errcode === 0){
                toaster.success({title:"",body:"删除成功"});
                $scope.getlist();
            }else{
                toaster.error({title:"",body:res.errmsg});
            }
        });
        }

    }
};