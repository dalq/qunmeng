var App = angular.module('coupon', []);

    App.config(require('./router'));
    App.factory('couponservice', require('./service'));

    App.controller('addcoupon',require('./controllers/addcoupon')); //添加/修改 优惠券
    App.controller('couponlist',require('./controllers/couponlist')); //优惠券列表 
    App.controller('addcouponproject',require('./controllers/addcouponproject')); //添加/修改 优惠券产品
    App.controller('couponprojectlist',require('./controllers/couponprojectlist')); //优惠券产品列表
    App.controller('coupon_orderlist',require('./controllers/coupon_orderlist'));
    App.controller('orderinfo',require('./controllers/orderinfo')); // 订单详情 
    App.controller('add_coupon',require('./controllers/add_coupon')); // 子表添加优惠券
    App.controller('couponinfo',require('./controllers/couponinfo'));
    App.controller('rulelist',require('./controllers/rulelist'));
    App.controller('coupontype_list',require('./controllers/coupontype_list'));
    App.controller('addruletype',require('./controllers/addruletype'));

    // 添加优惠券类别
    App.controller('add_coupontype',require('./controllers/add_coupontype'));
    
    
module.exports = App;