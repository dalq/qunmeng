/**
 * 模态框
 */
module.exports = function ($scope, $state, $stateParams,$resource,$modal) {
	/* 分页
     * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.currentPage = 1;      //当前页码
    $scope.itemsPerPage = 10         //每页显示几条
    $scope.redpacketorderlist={};
    $scope.redpacketorderlist.mobile="";
    $scope.redpacketorderlist.gift_code="";
    $scope.redpacketorderlist.rp_status="";
    $scope.redpacketorderlist.rp_send_status="";
    
	$scope.pageChanged = function () {
       var para = {
            pageNo:$scope.currentPage,
            pageSize:$scope.itemsPerPage,            
            mobile:$scope.redpacketorderlist.mobile,
            gift_code:$scope.redpacketorderlist.gift_code,
            rp_status:$scope.redpacketorderlist.rp_status,
            rp_send_status:$scope.redpacketorderlist.rp_send_status,
    
        };
        console.log(para);
        

        $resource('/api/as/puc/redpacketorder/findRedPacketOrderInfoList', {}, {}).
			save(para, function(res){				
                console.log(res);
                if(res.errcode === 0 || res.errcode === 10003){
					$scope.a=res.data.results;
					$scope.totalItems = res.data.totalRecord;
                    //alert($scope.totalItems);
                     for(var i = 0;i < res.data.results.length; i++) {
                         $scope.a[i].rp_fee=res.data.results[i].rp_fee/100;
                         if($scope.a[i].rp_status=="01"){
                            $scope.a[i].rp_status="发放";
                         }
                         if($scope.a[i].rp_status=="02"){
                            $scope.a[i].rp_status="自行领取";
                         }
                         if($scope.a[i].rp_send_status=="01"){
                            $scope.a[i].rp_send_status="已领取";
                         }
                         if($scope.a[i].rp_send_status=="02"){
                            $scope.a[i].rp_send_status="未领取";
                         }
                         $scope.a[i].otime=new Date(res.data.results[i].otime);
                         $scope.a[i].rtime=new Date(res.data.results[i].rtime);
                         //alert();
                         if($scope.a[i].rtime=="Invalid Date"){
                            $scope.a[i].rtime="";
                         }
                     }
                }else{
                    alert(res.errmsg);
                }
            });

    };
	 $scope.pageChanged();
			$scope.redpacketorderlist.search=function(){
            var dic = {
                pageNo:$scope.currentPage,
                pageSize:$scope.itemsPerPage,
                mobile:$scope.redpacketorderlist.mobile,
                gift_code:$scope.redpacketorderlist.gift_code,
                rp_status:$scope.redpacketorderlist.rp_status,
                rp_send_status:$scope.redpacketorderlist.rp_send_status,
			}
            console.log(dic);
				 $resource('/api/as/puc/redpacketorder/findRedPacketOrderInfoList', {}, {}).
                        save(dic, function(res){				
                            console.log(res);
                            if(res.errcode === 0 || res.errcode === 10003){
                                $scope.a=res.data.results;
                                $scope.totalItems = res.data.totalRecord;
                                for(var i = 0;i < res.data.results.length; i++) {
                                    $scope.a[i].rp_fee=res.data.results[i].rp_fee/100;
                                if($scope.a[i].rp_status=="01"){
                                    $scope.a[i].rp_status="发放";
                                }
                                if($scope.a[i].rp_status=="02"){
                                    $scope.a[i].rp_status="自行领取";
                                }
                                if($scope.a[i].rp_send_status=="01"){
                                    $scope.a[i].rp_send_status="已领取";
                                }
                                if($scope.a[i].rp_send_status=="02"){
                                    $scope.a[i].rp_send_status="未领取";
                                }
                                $scope.a[i].otime=new Date(res.data.results[i].otime);
                                $scope.a[i].rtime=new Date(res.data.results[i].rtime);
                                //alert();
                                if($scope.a[i].rtime=="Invalid Date"){
                                    $scope.a[i].rtime="";
                                }
                                }
                            }else{
                                alert(res.errmsg);
                            }
                        });
			}
            $scope.redpacketorderlist.details = function (index) {
                //alert("nihao");
               $scope.item = $scope.a[index];
                var modalInstance = $modal.open({
                    template: require('../views/redpacketOrderDetails.html'),
                    controller: 'redpacketOrederDetails',
                    size: 'lg',
                    resolve: {
                        items: function () {
                            return $scope.item;
                        }
                    }
                });
                modalInstance.opened.then(function () {
                });
                modalInstance.result.then(function (showResult) {
                }, function (reason) {
                    // click，点击取消，则会暑促cancel
                    $log.info('Modal dismissed at: ' + new Date());
                });
            };
            $scope.redpacketorderlist.delete= function (index) {
                if(!confirm("是否确认删除")){
                    return ;
                }
                $scope.itemw = $scope.a[index];
                alert($scope.itemw.code);
                $resource('/api/as/puc/redpacketorder/deleteRedPacketOrder', {}, {}).save({ code: $scope.itemw.code }, function (res) {
                    if (res.errcode === 0) {
                        $scope.pageChanged();
                        alert('删除成功');
                    } else {
                        alert(res.errmsg);
                    }
                });

            };
            
            
};