module.exports = function($scope, $stateParams,$resource,$state){
	var code = $stateParams.code;
	console.log("进来没？");
	$scope.redpacket={};	
	$scope.redpacket.types=[{ code:"00",sys_type:"未选择"}];
	$resource('/api/ac/puc/redPacketProductService/getRedPacketTypes', {}, {}).save({}, function(res){
        console.log(res);
		//console.log(res.data);
		if(res.errcode==0){	
			for(var i = 0;i < res.data.length; i++){
				$scope.redpacket.types.push(res.data[i]);
			}	
			 console.log($scope.redpacket.types);
		}else{
			alert(res.errmsg);
		}
    });
	//console.log($stateParams.code);
	 $resource('/api/ac/puc/redPacketProductService/getRedPacketProduct', {}, {}).save({code:code}, function(res){
	 	console.log(res.data.results);
	 	if(res.errcode==0){
			$scope.redpacket.name=res.data.results[0].name;
			$scope.redpacket.type=res.data.results[0].type; 
			$scope.redpacket.rule=res.data.results[0].rule;
			$scope.redpacket.rp_type=res.data.results[0].rp_type;
			$scope.redpacket.stock_type=res.data.results[0].stock_type;
			$scope.redpacket.max_limit=res.data.results[0].max_limit;
			$scope.redpacket.normal_redmoney=res.data.results[0].normal_redmoney/100;
			$scope.redpacket.random_redmoney_start=res.data.results[0].random_redmoney_start/100;
			$scope.redpacket.random_redmoney_end=res.data.results[0].random_redmoney_end/100;
			$scope.redpacket.consume_redmoney=res.data.results[0].consume_redmoney*100;
			$scope.redpacket.stock_num=res.data.results[0].stock_num;
			$scope.redpacket.max_limit_num=res.data.results[0].max_limit_num;
			$scope.redpacket.remarks=res.data.results[0].remarks;
			if($scope.redpacket.rule=="n01"){
				$scope.redpacket.quotamoney=true;
				$scope.redpacket.randommoney=false;
				$scope.redpacket.percentmoney=false;
				};
				if($scope.redpacket.rule=="n02"){
					$scope.redpacket.quotamoney=false;
					$scope.redpacket.randommoney=true;
					$scope.redpacket.percentmoney=false;
				};
				if($scope.redpacket.rule=="n03"){
					$scope.redpacket.quotamoney=false;
					$scope.redpacket.randommoney=false;
					$scope.redpacket.percentmoney=true;
				};
				if($scope.redpacket.stock_type=="1"){
					$scope.redpacket.stocknumshow=true;
				}else{
					$scope.redpacket.stocknumshow=false;
				}
				if($scope.redpacket.max_limit=="0"){
					$scope.redpacket.showmaxlimit=true;
				}else{
					$scope.redpacket.showmaxlimit=false;
				}

			$scope.redpacket.showrule=function(){
				if($scope.redpacket.rule=="n01"){
				$scope.redpacket.quotamoney=true;
				$scope.redpacket.randommoney=false;
				$scope.redpacket.percentmoney=false;
				};
				if($scope.redpacket.rule=="n02"){
					$scope.redpacket.quotamoney=false;
					$scope.redpacket.randommoney=true;
					$scope.redpacket.percentmoney=false;
				};
				if($scope.redpacket.rule=="n03"){
					$scope.redpacket.quotamoney=false;
					$scope.redpacket.randommoney=false;
					$scope.redpacket.percentmoney=true;
				};	
			}
			$scope.redpacket.showstocknum=function(){
				if($scope.redpacket.stock_type=="1"){
					$scope.redpacket.stocknumshow=true;
				}else{
					$scope.redpacket.stocknumshow=false;
				}
			}
			$scope.redpacket.maxlimitshow=function(){
				if($scope.redpacket.max_limit=="0"){
					$scope.redpacket.showmaxlimit=true;
				}else{
					$scope.redpacket.showmaxlimit=false;
				}
			}
			//用户名非空验证
			$scope.redpacket.nameadd=function(){
			$scope.redpacket.namecheck=false;
			}
			$scope.redpacket.nameblur=function(){
				if ($scope.redpacket.name==undefined || $scope.redpacket.name==""){
							$scope.redpacket.namecheck=true;
						}
			}
			//红包金额非空验证
			$scope.redpacket.normal_redmoneyadd=function(){	
			$scope.redpacket.normal_redmoneycheck=false;
			}
			$scope.redpacket.normal_redmoneyblur=function(){
				if ($scope.redpacket.normal_redmoney==undefined || $scope.redpacket.normal_redmoney==""){
							$scope.redpacket.normal_redmoneycheck=true;
						}
			}
			//随机红包金额非空验证
			$scope.redpacket.random_redmoneyadd=function(){	
			$scope.redpacket.random_redmoneycheck=false;
			}
			$scope.redpacket.random_redmoneyblur=function(){
				if ($scope.redpacket.random_redmoney_start==undefined || $scope.redpacket.random_redmoney_start=="" || $scope.redpacket.random_redmoney_end==undefined || $scope.redpacket.random_redmoney_end==""){
							$scope.redpacket.random_redmoneycheck=true;
						}
			}
			//百分比红包金额非空验证
			$scope.redpacket.consume_redmoneyadd=function(){	
			$scope.redpacket.consume_redmoneycheck=false;
			}
			$scope.redpacket.consume_redmoneyblur=function(){
				if ($scope.redpacket.consume_redmoney==undefined || $scope.redpacket.consume_redmoney==""){
							$scope.redpacket.consume_redmoneycheck=true;
						}
			}
			//库存数量非空验证
			$scope.redpacket.stock_numadd=function(){	
			$scope.redpacket.stock_numcheck=false;
			}
			$scope.redpacket.stock_numblur=function(){
				if ($scope.redpacket.stock_num==undefined || $scope.redpacket.stock_num==""){
							$scope.redpacket.stock_numcheck=true;
						}
			}
			//最大限度非空验证
			$scope.redpacket.max_limit_numadd=function(){	
			$scope.redpacket.max_limit_numcheck=false;
			}
			$scope.redpacket.max_limit_numblur=function(){
				if ($scope.redpacket.max_limit_num==undefined || $scope.redpacket.max_limit_num==""){
							$scope.redpacket.max_limit_numcheck=true;
						}
			}
			
			$scope.submit=function(){
			var redmoney={};
			var redmoney={
				code:code,
				name:$scope.redpacket.name,
				type:$scope.redpacket.type,
				rule:$scope.redpacket.rule,
				rp_type:$scope.redpacket.rp_type,
				stock_type:$scope.redpacket.stock_type,
				max_limit:$scope.redpacket.max_limit,
				remarks:$scope.redpacket.remarks		   
			};
			if($scope.redpacket.type=="00"){
					$scope.redpacket.redtype=true;
					return;
			}
			if($scope.redpacket.name==undefined || $scope.redpacket.name==""){
					$scope.redpacket.namecheck=true;
					return;
			}	   						
				if($scope.redpacket.rule=="n01"){
					if ($scope.redpacket.normal_redmoney==undefined || $scope.redpacket.normal_redmoney==""){
						$scope.redpacket.normal_redmoneycheck=true;
						return;
					}
					redmoney.normal_redmoney=$scope.redpacket.normal_redmoney*100;
				}
				if($scope.redpacket.rule=="n02"){
					if ($scope.redpacket.random_redmoney_start==undefined || $scope.redpacket.random_redmoney_start=="" || $scope.redpacket.random_redmoney_end==undefined || $scope.redpacket.random_redmoney_end==""){
						$scope.redpacket.random_redmoneycheck=true;
						return;
					}
					redmoney.random_redmoney_start=$scope.redpacket.random_redmoney_start*100;
					redmoney.random_redmoney_end=$scope.redpacket.random_redmoney_end*100;
				}
				if($scope.redpacket.rule=="n03"){
					if ($scope.redpacket.consume_redmoney==undefined || $scope.redpacket.consume_redmoney==""){
						$scope.redpacket.consume_redmoneycheck=true;
						return;
					}
					redmoney.consume_redmoney=$scope.redpacket.consume_redmoney/100;			
				}
				if($scope.redpacket.stock_type=="1"){
					if ($scope.redpacket.stock_num==undefined || $scope.redpacket.stock_num==""){
						$scope.redpacket.stock_numcheck=true;
						return;
					}
					redmoney.stock_num=$scope.redpacket.stock_num;			
				}
				if($scope.redpacket.max_limit=="0"){
					if ($scope.redpacket.max_limit_num==undefined || $scope.redpacket.max_limit_num==""){
						$scope.redpacket.max_limit_numcheck=true;
						return;
					}
					redmoney.max_limit_num=$scope.redpacket.max_limit_num;			
				}		
				console.log(redmoney);
				$resource('/api/ac/puc/redPacketProductService/updateRedPacketProduct', {}, {}).save(redmoney, function(res){
					console.log(res);
					if(res.errcode==0){
							$state.go("app.redpacketList",{}, {reload: true});
						// }				
					}else{
						alert(res.errmsg);
					}
				})			   
			//console.log(redmoney);
				};	
	// 		$scope.redpacket=res.data[0];
	 	}else{
			alert(res.errmsg);
	 	}
	 });
};