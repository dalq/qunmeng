/**
 * 模态框
 */
module.exports = function ($scope, $state, $stateParams,$resource,$modal) {
	/* 分页
     * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.currentPage = 1;      //当前页码
    $scope.itemsPerPage = 10 ;        //每页显示几条
    $scope.redpacketlist={};
    $scope.redpacketlist.name="";
    $scope.redpacketlist.rp_type="";
    $scope.redpacketlist.status="";
    $scope.redpacketlist.type="";
    
	$scope.pageChanged = function () {
        $scope.redpacketlist.types=[{ code:"",sys_type:"未选择"}];
        $resource('/api/ac/puc/redPacketProductService/getRedPacketTypes', {}, {}).save({}, function(res){
            console.log(res);
            if(res.errcode==0){	
                for(var i = 0;i < res.data.length; i++){
                    $scope.redpacketlist.types.push(res.data[i]);
                }
                console.log("类型");	
                console.log($scope.redpacketlist.types);
            }else{
                alert(res.errmsg);
            }
        });
       var para = {
        //    ta:1,
            pageNo:$scope.currentPage,
            pageSize:$scope.itemsPerPage,
            del_flag:"0",
            name:$scope.redpacketlist.name,
            rp_type:$scope.redpacketlist.rp_type,
            status:$scope.redpacketlist.status,
            type:$scope.redpacketlist.type
    
        };
        		console.log(para);

        $resource('/api/ac/puc/redPacketProductService/getRedPacketProduct', {}, {}).
			save(para, function(res){				
                console.log(res);
                if(res.errcode === 0 || res.errcode === 10003){
					$scope.a=res.data.results;
					$scope.totalItems = res.data.totalRecord;
                    //alert(res.data.results.length);
                    for(var i = 0;i < res.data.results.length; i++) {
                        if(res.data.results[i].rule=="n01"){
                            $scope.a[i].rule="定额红包";
                        }
                        if(res.data.results[i].rule=="n02"){
                            $scope.a[i].rule="随机红包";
                        }
                        if(res.data.results[i].rule=="n03"){
                            $scope.a[i].rule="百分比红包";
                        }
                        if(res.data.results[i].rp_type=="01"){
                            $scope.a[i].rp_type=" 发放";
                        }
                        if(res.data.results[i].rp_type=="02"){
                            $scope.a[i].rp_type=" 自行领取";
                        }
                        if(res.data.results[i].stock_type=="0"){
                            $scope.a[i].stock_type="不启用";
                        }
                        if(res.data.results[i].stock_type=="1"){
                            $scope.a[i].stock_type=undefined;
                        }
                        if(res.data.results[i].max_limit=="-1"){
                            $scope.a[i].max_limit="不限制";
                        }
                        if(res.data.results[i].max_limit=="0"){
                            $scope.a[i].max_limit=undefined;
                        }
                        if(res.data.results[i].type=="01"){
                            $scope.a[i].type="商客系统";
                        }
                        if(res.data.results[i].type=="02"){
                            $scope.a[i].type="一卡通";
                        }
                        if(res.data.results[i].type=="03"){
                            $scope.a[i].type="票务系统";
                        }
                        if(res.data.results[i].status=="0"){
                            $scope.a[i].status="草稿";
                            $scope.a[i].statusC=true;
                            $scope.a[i].statusS=false;
                        }
                        if(res.data.results[i].status=="1"){
                            $scope.a[i].status="已上架";
                            $scope.a[i].statusC=false;
                            $scope.a[i].statusS=true;                            
                        }
                        if(res.data.results[i].status=="2"){
                            $scope.a[i].status="已下架";
                           $scope.a[i].statusC=true;
                            $scope.a[i].statusS=false;                           
                        }
                    }
                }else{
                    alert(res.errmsg);
                }
            });

    };
	 $scope.pageChanged();

			$scope.redpacketlist.search=function(){
            var dic = {
                pageNo:$scope.currentPage,
                pageSize:$scope.itemsPerPage,
                del_flag:"0",
                name:$scope.redpacketlist.name,
                rp_type:$scope.redpacketlist.rp_type,
                status:$scope.redpacketlist.status,
                type:$scope.redpacketlist.type
			}
            //alert($scope.redpacketlist.status);
            console.log(dic);
				/*console.log('123123');
				console.log(dic);*/
				 $resource('/api/ac/puc/redPacketProductService/getRedPacketProduct', {}, {}).
			save(dic, function(res){				
                console.log(res);
                if(res.errcode === 0 || res.errcode === 10003){
					$scope.a=res.data.results;
					$scope.totalItems = res.data.totalRecord;
                    //alert(res.data.results.length);
                    for(var i = 0;i < res.data.results.length; i++) {
                        if(res.data.results[i].rule=="n01"){
                            $scope.a[i].rule="定额红包";
                        }
                        if(res.data.results[i].rule=="n02"){
                            $scope.a[i].rule="随机红包";
                        }
                        if(res.data.results[i].rule=="n03"){
                            $scope.a[i].rule="百分比红包";
                        }
                        if(res.data.results[i].rp_type=="01"){
                            $scope.a[i].rp_type=" 发放";
                        }
                        if(res.data.results[i].rp_type=="02"){
                            $scope.a[i].rp_type=" 自行领取";
                        }
                        if(res.data.results[i].stock_type=="0"){
                            $scope.a[i].stock_type="不启用";
                        }
                        if(res.data.results[i].stock_type=="1"){
                            $scope.a[i].stock_type=undefined;
                        }
                        if(res.data.results[i].max_limit=="-1"){
                            $scope.a[i].max_limit="不限制";
                        }
                        if(res.data.results[i].max_limit=="0"){
                            $scope.a[i].max_limit=undefined;
                        }
                        if(res.data.results[i].type=="01"){
                            $scope.a[i].type="商客系统";
                        }
                        if(res.data.results[i].type=="02"){
                            $scope.a[i].type="一卡通";
                        }
                        if(res.data.results[i].type=="03"){
                            $scope.a[i].type="票务系统";
                        }
                        if(res.data.results[i].status=="0"){
                            $scope.a[i].status="草稿";
                            $scope.a[i].statusC=true;
                            $scope.a[i].statusS=false;
                        }
                        if(res.data.results[i].status=="1"){
                            $scope.a[i].status="已上架";
                            $scope.a[i].statusC=false;
                            $scope.a[i].statusS=true;                            
                        }
                        if(res.data.results[i].status=="2"){
                            $scope.a[i].status="已下架";
                           $scope.a[i].statusC=true;
                            $scope.a[i].statusS=false;                           
                        }
                    }
                }else{
                    alert(res.errmsg);
                }
            });
			}
            // $scope.redpacketlist.details = function (index) {
            //     alert("nihao");
            //     $scope.item = $scope.a[index];
            //     var modalInstance = $modal.open({
            //         template: require('../views/redpacketDetails.html'),
            //         controller: 'redpacketDetails',
            //         size: 'lg',
            //         resolve: {
            //             items: function () {
            //                 return $scope.item;
            //             }
            //         }
            //     });
            //     modalInstance.result.then(function (showResult) {
            //             $scope.pageChanged();
            //     });

            // }


            $scope.redpacketlist.goup = function (index) {
                $scope.item = $scope.a[index];
                var code = $scope.item.code;
                $resource('/api/ac/puc/redPacketProductService/shelvesRedPacketProduct', {}, {}).save({ code: code,shelves_type:"1" }, function (res) {
                    if (res.errcode === 0) {
                        alert('上架申请成功');
                        $scope.pageChanged();
                        
                    } else {
                        alert(res.errmsg);
                    }
                });

            };

            $scope.redpacketlist.edit = function (index) {
                $scope.item = $scope.a[index];
                console.log($scope.item.code);
                $state.go('app.redpacketEdit', {                   
                     code: $scope.item.code
                    // product_state_name: $scope.item.product_state_name
                });
            }
    //         //我的修改
            $scope.redpacketlist.details = function (index) {
                //alert("nihao");
               $scope.item = $scope.a[index];
                var modalInstance = $modal.open({
                    template: require('../views/redpacketDetails.html'),
                    controller: 'redpacketDetails',
                    size: 'lg',
                    resolve: {
                        items: function () {
                            return $scope.item;
                        }
                    }
                });
                modalInstance.opened.then(function () {
                });
                modalInstance.result.then(function (showResult) {
                }, function (reason) {
                    // click，点击取消，则会暑促cancel
                    $log.info('Modal dismissed at: ' + new Date());
                });
            }

            $scope.redpacketlist.godown = function (index) {
                //alert("nihao1");
                $scope.item = $scope.a[index];
                $resource('/api/ac/puc/redPacketProductService/shelvesRedPacketProduct', {}, {}).save({ code: $scope.item.code,shelves_type:"0" }, function (res) {
                    if (res.errcode === 0) {
                        alert('下架成功');
                        $scope.pageChanged();
                        
                    } else {
                        alert(res.errmsg);
                    }
                });

            };

    //         $scope.copy = function (index) {
    //             $scope.item = $scope.a[index];
    //             var modalInstance = $modal.open({
    //                 template: require('../views/copy.html'),
    //                 controller: 'linecopy',
    //                 size: 'lg',
    //                 resolve: {
    //                     items: function () {
    //                         return $scope.item;
    //                     }
    //                 }
    //             });
    //             modalInstance.result.then(function (showResult) {
    //                 $scope.pageChanged();
    //             });
    //         }

            $scope.redpacketlist.delete= function (index) {
                if(!confirm("是否确认删除")){
                    return ;
                }
                $scope.itemw = $scope.a[index];
                //alert($scope.itemw.code);
                $resource('/api/ac/puc/redPacketProductService/delRedPacketProduct', {}, {}).save({ code: $scope.itemw.code }, function (res) {
                    if (res.errcode === 0) {
                        alert('删除成功');
                        $scope.pageChanged();
                    } else {
                        alert(res.errmsg);
                    }
                });

            };
};