var App = angular.module('redpacket', []);

App.config(require('./router'));

//service
App.factory('redpacketservice', require('./service'));

//Controllers
App.controller('redpacketList', require('./controllers/list'));
App.controller('redpacketEdit', require('./controllers/edit'));
App.controller('redpacketDetails', require('./controllers/details'));
App.controller('redpacketOrederDetails', require('./controllers/orderdetails'));
App.controller('redpacketCreate',require('./controllers/create'));
App.controller('redpacketDelete',require('./controllers/delete'));
App.controller('redpacketOrderList', require('./controllers/orderlist'));
module.exports = App;