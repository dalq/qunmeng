 /**
 * 子模块路由
 * DHpai
 */

var router = function($urlRouterProvider, $stateProvider){
 	$stateProvider
   //红包产品列表
    .state('app.redpacketList', {
        url: "/redpacket/redpacket.html",
        views: {
            'main@' : {
                template : require('../24redpacket/views/RedPacketList.html'),
                controller : 'redpacketList',
            }
        }
    })   
    //编辑红包产品
    .state('app.redpacketEdit', {
        url: "/redpacket/redpacket.html/:code",
        views: {
            'main@' : {
                template : require('../24redpacket/views/updataRedPacket.html'),
                controller : 'redpacketEdit',
            }
        },
        resolve:{
        }
    })
    //创建红包产品
    .state('app.redpacketCreate', {
        url: "/redpacket/redpacketcreate.html",
        views: {
            'main@' : {
                template : require('../24redpacket/views/creatRedPacket.html'),
                controller : 'redpacketCreate',
            }
        }      
    })
    //查看红包详情
    .state('app.redpacketDetails.html', {
        url: "/redpacket/redpacketdetails/:id",
        views: {
            'main@' : {
                template : require('../996tpl/views/form.html'),
                controller : 'redpacketDetails',
            }
        },
        resolve:{
        }
    })
    //红包产品订单列表
    .state('app.redpacketOrderList', {
        url: "/redpacket/redpacketorder.html",
        views: {
            'main@' : {
                template : require('../24redpacket/views/RedPacketOrderList.html'),
                controller : 'redpacketOrderList',
            }
        }
    }) 
	;

};

module.exports = router;