/**
 * 子模块service
 * DHpai
 */
var service = function($resource, $q, $state, $modal){
    
    var model = [
        {
            'title' : '系统编号',
            'id' : 'system_code',
            'type' : 'text',
        },
        {
            'title' : '系统名称',
            'id' : 'system_name',
            'type' : 'text',
        },
        {
            'title' : '备注',
            'id' : 'system_remark',
            'type' : 'text',
        },
        {
            'title' : '启用标志',
            'id' : 'system_start_flag',
            'type' : 'switch',
            'open': 1,
            'close':0,
        },
        {
            'title' : '排序',
            'id' : 'system_sort',
            'type' : 'number',
        },
        {
            'title' : '系统别名',
            'id' : 'system_alias',
            'type' : 'text',

        }
    ];

    return {
        model : function(){
            return model;
        }
    };

};

module.exports = service;