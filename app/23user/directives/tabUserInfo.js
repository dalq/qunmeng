module.exports = function ($resource, $state, $http, $q, toaster, $modal, toaster) {
	return {
		restrict: 'E',
		template: require('../views/tabUserInfo.html'),
		replace: true,
		scope: {
			'baseinfo': '=',
			'tabData': '=',
			'funs': '='
		},
		link: function (scope, elements, attrs){

			//监听adduser状态的变化，为true时添加员工,初始化一些信息
			scope.$watch('tabData.status.adduser', function(newValue, oldValue){
				if(newValue){
					scope.baseinfo = {
						'login_flag': '1',
						'user_type': '3',
						'password': '',
						'passwordc': ''
					};
				}
			}, true);

			//保存用户
			scope.save = function(formValid) {
				scope.baseinfo.rolelist = [{'id': scope.baseinfo.role_ids}];
				// angular.forEach(scope.tabData.roleList, function(item){
				// 	if(item.active){
				// 		scope.baseinfo.rolelist.push(item);
				// 	}
				// });
				if(!checkInfo(formValid)) return;
				var aa = '';
				if(scope.tabData.status.adduser){
					aa = '/api/ac/sc/systemUserService/addUser';
					scope.baseinfo.company_id = scope.tabData.company_id;
				} else {
					aa = '/api/ac/sc/systemUserService/setUserInfo';
				}
				scope.baseinfo.mobile = scope.baseinfo.mobile || null; //临时解决办法
				$resource(aa, {}, {}).save(scope.baseinfo, function(res){
					if (res.errcode === 0) {
						if(scope.tabData.status.adduser){
							alert(res.data.Message + ',用户名:' + res.data.login_name);
						} else {
							toaster.success({title: '', body: '保存成功'});
						}
						scope.funs.reset();
						scope.funs.search(1);
					} else {
						toaster.error({title: '', body: '手机号码重复'});
					}
				});
			};

			scope.addRole = function() {
				$state.go('app.roleManagement', {});
			}

			//取消
			scope.cancel = scope.funs.reset;

			//验证提交的用户信息
			function checkInfo(formValid){
				//添加员工时必填手机号
				if(scope.tabData.status.adduser && !scope.baseinfo.mobile){
					toaster.warning({title: '', body: '添加员工时手机号不能为空'});
					return false;
				}
				//检验手机号格式
				if(scope.baseinfo.mobile && !/^1[3|5|7|8|9]\d{9}$/.test(scope.baseinfo.mobile)){
					toaster.warning({title: '', body: '手机号格式不正确'});
					return false;
				}
				//员工名称一定要有
				if(!scope.baseinfo.name){
					toaster.warning({title: '', body: '名称不能为空'});
					return false;
				}
				//添加员工时密码必填, 否则通过angular的表单验证即可
				if(formValid.password.$invalid || formValid.passwordc.$invalid){
					toaster.warning({title: '', body: '密码格式不正确'});
					return false;
				} else if(scope.tabData.status.adduser && !scope.baseinfo.password && !scope.baseinfo.passwordc){
					toaster.warning({title: '', body: '密码不能为空'});
					return false;
				}
				//angular的表单验证
				if(formValid.email.$invalid){
					toaster.warning({title: '', body: '邮箱格式不正确'});
					return false;
				}
				//最后验证员工拥有的角色(暂时只能选一个角色)
				if(!scope.baseinfo.role_ids){
					toaster.warning({title: '', body: '请选择一个角色, 如果没有角色请添加'});
					return false;
				}
				return true;
			}
			


		}
	};
};

