module.exports = function ($resource, $state, $http, $q, toaster, $modal, toaster) {
	return {
		restrict: 'E',
		template: require('../views/tabUserList.html'),
		replace: true,
		scope: {
			'baseinfo': '=',
			'tabData': '=',
			'funs': '='
		},
		link: function (scope, elements, attrs){
			scope.currentPage = 1;			//当前页码
			scope.itemsPerPage = 10;		//每页显示几条
			scope.searchform = {};
			
			
			//搜索
			scope.search = function(pageNo){
				var para = {
					'pageNo': pageNo,
					'pageSize': scope.itemsPerPage,
					'loginname': scope.searchform.loginname,
					'username': scope.searchform.username
				};
				$resource('/api/ac/sc/systemUserService/getUserInfoList', {}, {}).save(para, function (res) {
					if (res.errcode === 0) {
						scope.userList = res.data.results;
						scope.totalItems = res.data.totalRecord;
						scope.currentPage = pageNo;
					} else {
						toaster.error({title: '', body: res.errmsg});
					}
				});
			};
			scope.search(1);
			scope.funs.search = scope.search;

			//查看用户信息
			scope.getInfo = function(id){
				var para = {
					'id': id,
					'contain_role': '1',
				}
				$resource('/api/ac/sc/systemUserService/getById', {}, {}).save(para, function(res){
					if (res.errcode === 0) {
						var modalInstance = $modal.open({
							template: require('../views/myInfo.html'),
							controller: 'myInfo',
							size: 'lg',
							resolve: {
								info: function (){
									return res.data;
								}
							}
						});
					} else {
						toaster.error({title: '', body: res.errmsg});
					}
				});
			};

			//修改界面
			scope.update = function(info){
				$resource('/api/ac/sc/systemUserService/getById', {}, {}).save({'id': info.id}, function(res){
					if (res.errcode === 0) {
						scope.baseinfo = res.data;
						scope.baseinfo.password = ''; 	//莫名其妙,不置空则会带到下一次查询或添加员工
						scope.baseinfo.passwordc = '';
						scope.baseinfo.role_ids = info.role_ids;
						
						scope.tabData.tabInfo.name = '修改信息';
						scope.tabData.tabInfo.active = true;
					} else {
						toaster.error({title: '', body: res.errmsg});
					}
				});
			};
			
			//删除用户
			scope.delete = function (id, index) {
				if(confirm('确认要删除该用户吗？') == true){
					$resource('/api/ac/sc/systemUserService/deleteUser', {}, {}).save({'id': id}, function(res){
						if (res.errcode === 0) {
							toaster.success({title: '', body: '删除成功'});
							scope.userList.splice(index, 1);
						} else {
							toaster.error({title: '', body: res.errmsg});
						}
					});
				}
			};




		}
	};
};

