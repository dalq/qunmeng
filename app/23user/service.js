/**
 * 子模块service
 * DHpai
 */
var service = function($resource, $q, $state, $modal){
    
    //机构列表
    var officeList = '/api/ac/sc/office/getList';
    //用户列表
    var userList = '/api/ac/sc/systemUserService/getUserList';
    //用户详情
    var userInfo = '/api/ac/sc/systemUserService/getById';
    //添加用户
    var addUser = '/api/ac/sc/systemUserService/addUser';
    //修改用户
    var setUserInfo = '/api/ac/sc/systemUserService/setUserInfo';
    //角色列表-用户选角色用
    var roleList = '/api/as/sc/role/getForUserList';
    

    return {
        officeList : function(){
            return $resource(officeList, {}, {});
        },
        userList : function(){
            return $resource(userList, {}, {});
        },
        userInfo : function(){
            return $resource(userInfo, {}, {});
        },
        delUser : function(){
            return $resource(delUser, {}, {});
        },
        addUser : function(){
            return $resource(addUser, {}, {});
        },
        setUserInfo : function(){
            return $resource(setUserInfo, {}, {});
        },
        roleList : function(){
            return $resource(roleList, {}, {});
        }
    };

};

module.exports = service;