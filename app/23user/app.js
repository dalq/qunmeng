var App = angular.module('user', []);

App.config(require('./router'));

//service
App.factory('userservice', require('./service'));

//Controllers
App.controller('userList1', require('./controllers/userList'));
App.controller('userInfo', require('./controllers/userinfo'));
App.controller('userInfo_new', require('./controllers/userinfo_new'));
App.controller('myInfo', require('./controllers/myInfo'));
App.controller('bindWechat', require('./controllers/bindWechat'));
App.controller('setWechat', require('./controllers/setWechat'));
App.controller('wechatQrcode', require('./controllers/wechatQrcode'));

//Directive
App.directive('tabUserList', require('./directives/tabUserList'));
App.directive('tabUserInfo', require('./directives/tabUserInfo'));


module.exports = App;