module.exports = function ($scope, $resource, $modalInstance, toaster, obj) {


    $scope.list = [
        {'lable': '下单通知', 'value': 'notice_order', 'check': false},
        // {'lable': '下单失败通知', 'value': 'notice_orderfail', 'check': false},
        {'lable': '预存不足提醒', 'value': 'notcie_depositout', 'check': false},
        {'lable': '退单通知', 'value': 'notice_orderback', 'check': false},
        // {'lable': '核销通知', 'value': 'notice_orderused', 'check': false}
    ];
    if(obj.notice_type){
        $scope.list.forEach(element => {
            if(obj.notice_type.indexOf(element.value) >= 0){
                element.check = true;
            }
        });
    }

    $scope.ok = function(){
        var aa = [];
        $scope.list.forEach(element => {
            if(element.check){
                aa.push(element.value);
            }
        });
        var para = {
            'id': obj.id,
            'notice_type': aa.toString()
        }

        $resource('/api/ac/sc/systemUserService/setUserWechat', {}, {}).save(para, function(res){
            if (res.errcode === 0){
                toaster.success({title: '', body: '修改成功'});
                $modalInstance.close(para.notice_type);
            } else {
                toaster.error({title: '', body: '修改失败'});
            }
        });
    }
    
    //取消
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    }


};