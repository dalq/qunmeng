module.exports = function ($scope, $resource, $modal, toaster) {
	
	//取消==重置
	$scope.reset = function(){
		$scope.tabData.status.active = true;
		$scope.tabData.status.adduser = false;
		$scope.tabData.tabInfo.name = '添加员工';
		$scope.tabData.tabInfo.active = false;
	};

	//查询机构内角色
	$scope.loadRoleList = function(){
		$resource('/api/as/sc/role/getCompanyRoleList', {}, {}).save({}, function(res){
			if (res.errcode === 0){
				$scope.tabData.roleList = res.data;
			} else {
				toaster.error({title: '', body: '加载角色列表失败,请重试'});
			}
		});
	};

	init();
	//初始化用户列表
	function init(){
		$scope.tabData = {
			'status': {'active': true, 'adduser': false},
			'tabInfo': {'name': '添加员工', 'active': false},
		};
		$scope.funs = {
			'reset': $scope.reset,
		};
		$scope.baseinfo = {};
		$resource('/api/ac/sc/systemUserService/getMyInfo', {}, {}).save({'contain_role': '1'}, function (res) {
			if (res.errcode === 0) {
				$scope.myInfo = res.data;
				$scope.tabData.company_id = $scope.myInfo.company_id;
				$scope.loadRoleList();
			} else {
				toaster.error({title: '', body: res.errmsg});
			}
		});
	};

	//查看个人信息
	$scope.getInfo = function() {
		var modalInstance = $modal.open({
			template: require('../views/myInfo.html'),
			controller: 'myInfo',
			size: 'lg',
			resolve: {
				info: function() {
					return $scope.myInfo;
				}
			}
		});
	};

	//添加界面
	$scope.add = function() {
		if($scope.tabData.tabInfo.name == '修改信息'){
			return;
		}
		$scope.tabData.status.adduser = true;
	};


};