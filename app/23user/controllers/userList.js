module.exports = function ($scope, $resource, transData, officeList, userList, userInfo, addUser, setUserInfo, roleList) {
	
	//取消==重置
	$scope.cancel = function () {
		$scope.tabname = {'name': '用户添加', 'active': false};
		$scope.status = {'active': true, 'adduser': false, 'nameUsed': '2'};
		$scope.obj = {};
	};

	init();
	//初始化用户树结构
	function init(){
		$scope.currentPage = 1;			//当前页码
		$scope.itemsPerPage = 20;		//每页显示几条
		$scope.cancel();
		officeList.save({}, function (res) {
			if (res.errcode !== 0) {
				alert(res.errmsg);
				return;
			};
			$scope.dataForTheTree = transData(res.data, 'id', 'parent_id', 'children');
			$scope.bakobj = $scope.dataForTheTree[0];
			$scope.loadrolelist($scope.bakobj.id);
		});
	} 

	//加载角色列表-修改用户新建用户时选择权限使用
	$scope.loadrolelist = function (id) {
		roleList.save({'company_id': id, 'flag': '1'}, function (res) {
			if (res.errcode === 0) {
				$scope.role_list = res.data;
			} else {
				alert(res.errmsg);
			}
		});
	};

	//菜单树选中事件
	$scope.showSelected = function (item) {
		$scope.cancel();
		$scope.bakobj = item;
		$scope.search.loginname = '';
		$scope.search.username = '';
		var para = {
			'pageNo': '1',
			'pageSize': $scope.itemsPerPage,
			'id': item.id
		}
		userList.save(para, function (res) {
			if (res.errcode === 0) {
				$scope.user_list = res.data.results;
				$scope.totalItems = res.data.totalRecord;
				$scope.loadrolelist(item.id);
			} else {
				alert(res.errmsg);
			}
		});
	};

	//搜索
	$scope.search = function (pageNo) {
		var para = {
			'id': $scope.bakobj.id,
			'pageNo': pageNo,
			'pageSize': $scope.itemsPerPage,
			'loginname': $scope.search.loginname,
			'mobile': $scope.search.mobile
		};
		userList.save(para, function (res) {
			if (res.errcode === 0) {
				$scope.user_list = res.data.results;
				$scope.totalItems = res.data.totalRecord;
			} else {
				alert(res.errmsg);
			}
		});
	};

	//添加界面
	$scope.add = function () {
		if($scope.tabname.name == '修改用户'){
			return;
		}
		$scope.obj = {'login_flag': '1', 'user_type': '3', 'company_name': $scope.bakobj.name};
		$scope.status = {'adduser': true, 'nameUsed': '2', 'prefix': $scope.bakobj.code};
		for(var i = 0; i < $scope.role_list.length; i++){
			$scope.role_list[i].active = false;
		}
	};

	//添加用户
	$scope.addUser = function () {
		if($scope.status.nameused == '2'){
			alert('登录名已存在');
			return;
		}
		$scope.obj.company_id = $scope.bakobj.id;
		$scope.obj.rolelist = [];
		for(var i = 0; i < $scope.role_list.length; i++){
			if($scope.role_list[i].active){
				$scope.obj.rolelist.push($scope.role_list[i]);
			}
		}
		addUser.save($scope.obj, function (res) {
			if (res.errcode === 0) {
				alert(res.data.Message + ',登录名为:' + res.data.login_name);
				$scope.showSelected($scope.bakobj);
			} else {
				alert(res.errmsg);
			}
		});
	};

	//修改界面
	$scope.update = function (item) {
		$scope.tabname = {'name': '修改用户', 'active': true};
		if(item.flag == 1){
			$scope.bakobj2 = item;
			item.contain_role = '1';
			userInfo.save(item, function (res) {
				if (res.errcode === 0) {
					$scope.obj = res.data;
					$scope.obj.mobile = parseInt($scope.obj.mobile);
					$scope.obj.flag = 1;	//拦截无权限用户使用
					$scope.obj.user_type = $scope.obj.user_type || '3';
					for(var i = 0; i < $scope.role_list.length; i++){
						$scope.role_list[i].active = false;
						for(var j = 0; j < $scope.obj.rolelist.length; j++){
							if($scope.obj.rolelist[j].id == $scope.role_list[i].id){
								$scope.role_list[i].active = true;
							}
						}
					}
				} else {
					alert(res.errmsg);
				}
			});
		}
	};

	//删除用户
	$scope.delete = function (item) {
		if (confirm('确认要删除该用户吗？')==true){
			item.del = '1';
			setUserInfo.save(item, function (res) {
				if (res.errcode === 0) {
					$scope.showSelected($scope.bakobj);
				} else {
					alert(res.errmsg);
				}
			});
		}
	};

	//保存用户
	$scope.save = function () {
		$scope.obj.rolelist = [];
		for(var i = 0; i < $scope.role_list.length; i++){
			if($scope.role_list[i].active){
				$scope.obj.rolelist.push($scope.role_list[i]);
			}
		}
		if($scope.obj.flag == 1){
			setUserInfo.save($scope.obj, function (res) {
				if (res.errcode === 0) {
					$scope.bakobj2.name = $scope.obj.name;
					$scope.bakobj2.phone = $scope.obj.phone;
					$scope.cancel();
				} else {
					alert(res.errmsg);
				}
			});
		}
	};

	//失去焦点时检测用户名是否可用
	$scope.checkname = function () {
		if($scope.obj.login_name){
			var para = {
				'loginName': $scope.obj.login_name,
				'company_id': $scope.bakobj.id
			}
			$resource('/api/ac/sc/systemUserService/checkLoginName', {}, {}).save(para, function (res) {
				if (res.errcode === 0) {
					$scope.status.nameused = res.data.message;
				} else {
					alert(res.errmsg);
				}
			});
		} else {
			$scope.status.nameused = '2';
		}
	}

};