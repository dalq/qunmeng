module.exports = function ($scope, $resource, transData, userInfo, addUser, setUserInfo, roleList) {
	
	//取消==重置
	$scope.cancel = function () {
		$scope.status = {'active': true, 'adduser': false};
		$scope.tabname = {'name': '用户添加', 'active': false};
		$scope.obj = {};
	};

	init();
	//初始化用户树结构
	function init(){
		$scope.currentPage = 1;			//当前页码
		$scope.itemsPerPage = 20;		//每页显示几条
		$scope.searchform = {};
		$scope.cancel();
		$resource('/api/ac/sc/office/getOfficeList', {}, {}).save({}, function(res){
			if (res.errcode !== 0) {
				alert(res.errmsg);
				return;
			};
			$scope.dataForTheTree = transData(res.data, 'id', 'parent_id', 'children');
			$scope.loadrolelist($scope.dataForTheTree[0].id);
		});
	}

	//加载角色列表-修改用户新建用户时选择权限使用
	$scope.loadrolelist = function (id) {
		roleList.save({'company_id': id, 'flag': '1'}, function (res) {
			if (res.errcode === 0) {
				$scope.role_list = res.data;
			} else {
				alert(res.errmsg);
			}
		});
	};

	//菜单树选中事件
	$scope.showSelected = function(item) {
		$scope.cancel();
		$scope.company_id = item.id;
		$scope.searchform.loginname = '';
		$scope.searchform.username = '';
		$scope.search(1);
	};

	//搜索
	$scope.search = function(pageNo){
		var para = {
			'id': $scope.company_id || $scope.dataForTheTree[0].id,
			'pageNo': pageNo,
			'pageSize': $scope.itemsPerPage,
			'loginname': $scope.searchform.loginname,
			'username': $scope.searchform.username
		};
		$resource('/api/ac/sc/systemUserService/getUserInfoList', {}, {}).save(para, function (res) {
			if (res.errcode === 0) {
				$scope.userList = res.data.results;
				$scope.totalItems = res.data.totalRecord;
			} else {
				alert(res.errmsg);
			}
		});
	};

	//添加界面
	$scope.add = function () {
		if($scope.tabname.name == '修改用户'){
			return;
		}
		$scope.obj = {};
		$scope.obj.office_name = $scope.dataForTheTree[0].name;
		$scope.status.adduser = true;
		$scope.role_list.forEach(function(roleInfo) {
            roleInfo.active = false;
        });
	};

	//添加用户
	$scope.addUser = function() {
		$scope.obj.company_id = $scope.dataForTheTree[0].id;
		$scope.obj.rolelist = [];
		angular.forEach($scope.role_list, function(item){
			if(item.active){
				$scope.obj.rolelist.push(item);
			}
		});
		//----------表单验证---------
		if(!checkInfo()) return;
		//--------------------------

		addUser.save($scope.obj, function (res) {
			if (res.errcode === 0) {
				alert(res.data.Message + ',登录名为:' + res.data.login_name);
				$scope.showSelected({'id': $scope.obj.company_id});
			} else {
				alert(res.errmsg);
			}
		});
	};

	//修改界面
	$scope.update = function (item) {
		$scope.tabname.name = '修改用户';
		$scope.tabname.active = true;
		if(item.flag == 1){
			userInfo.save(item, function (res) {
				if (res.errcode === 0) {
					$scope.obj = res.data;
					$scope.obj.mobile = parseInt($scope.obj.mobile);
					$scope.obj.flag = 1;	//拦截无权限用户使用
					if(!$scope.obj.user_type){
						$scope.obj.user_type = '3';
					}
					angular.forEach($scope.role_list, function(item){
						item.active = false;
						for(var j = 0; j < $scope.obj.rolelist.length; j++){
							if($scope.obj.rolelist[j].id == item.id){
								item.active = true;
							}
						}
					});
				} else {
					alert(res.errmsg);
				}
			});
			
		}
	};

	//删除用户
	$scope.delete = function (item) {
		if(confirm('确认要删除该用户吗？') == true){
			item.del = '1';
			setUserInfo.save(item, function (res) {
				if (res.errcode === 0) {
					item.id = item.company_id;
					$scope.showSelected(item);
				} else {
					alert(res.errmsg);
				}
			});
		}
	};

	//保存用户
	$scope.save = function () {
		$scope.obj.rolelist = [];
		angular.forEach($scope.role_list, function(item){
			if(item.active){
				$scope.obj.rolelist.push(item);
			}
		});
		//----------表单验证---------
		if(!checkInfo()) return;
		//--------------------------

		if($scope.obj.flag == 1){
			$scope.obj.user_type = '0'
			setUserInfo.save($scope.obj, function (res) {
				if (res.errcode === 0) {
					$scope.cancel();
					$scope.search(1);
				} else {
					alert(res.errmsg);
				}
			});
		}
	};

	//验证提交的用户信息---暂时只校验手机号
	function checkInfo(){
		if($scope.obj.mobile && !/^1\d{10}$/.test($scope.obj.mobile)){
			alert('手机号格式不正确');
			return false;
		}
		return true;
	}


};