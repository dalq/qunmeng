module.exports = function ($scope, $modal, $resource, toaster) {


	$scope.load = function() {
		$resource('/api/ac/sc/systemUserService/getUserWechatList', {}, {}).save({}, function (res) {
			if (res.errcode === 0) {
				$scope.list = res.data;
			} else {
				toaster.error({title: '', body: '查询失败'});
			}
		});
	}
	$scope.load();

	$scope.edit = function (obj) {
		var modalInstance = $modal.open({
			template: require('../views/setWechat.html'),
			controller: 'setWechat',
			size: 'lg',
			resolve: {
				obj: function () {
					return obj;
				}
			}
		});
		modalInstance.result.then(function(result){
			obj.notice_type = result;
		});
	}


	$scope.addWechat = function(){
		$resource('/api/ac/sc/systemUserService/createWxQrCode', {}, {}).save({}, function(res){
			if (res.errcode === 0){
				var modalInstance = $modal.open({
					template: require('../views/wechatQrcode.html'),
					controller: 'wechatQrcode',
					size: 'sm',
					resolve: {
						qrcode: function () {
							return res.data.result.url;
						}
					}
				});
				modalInstance.result.then(function(){
					$scope.load();
				}, function(){
					$scope.load();
				});
			} else {
				toaster.error({title: '', body: '生成二维码失败'});
			}
		});
		
	}

	$scope.setStatus = function(obj, flag){
		var msg = flag == '1' ? '开启此微信的消息提醒吗？' : '要关闭此微信的消息提醒吗？';
		var para = {'id': obj.id, 'status': flag};
		
		if(confirm(msg)){
			$resource('/api/ac/sc/systemUserService/setUserWechat', {}, {}).save(para, function (res) {
				if (res.errcode === 0) {
					toaster.success({title: '', body: '成功'});
					obj.status = flag;
				} else {
					toaster.error({title: '', body: '失败'});
				}
			});
		}
	}

};