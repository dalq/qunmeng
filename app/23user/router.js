 /**
 * 子模块路由
 * DHpai
 */

var router = function($urlRouterProvider, $stateProvider){


 	$stateProvider


    //用户列表
    .state('app.userList', {
        url: "/user/userlist",
        views: {
            'main@' : {
                template : require('./views/userList.html'),
                controller : 'userList1',
            }
        },
        resolve:{
            transData : function(utilservice){
                return utilservice.transData;
            },
            officeList : function(userservice){
                return userservice.officeList();
            },
            userList : function(userservice){
                return userservice.userList();
            },
            userInfo : function(userservice){
                return userservice.userInfo();
            },
            addUser : function(userservice){
                return userservice.addUser();
            },
            setUserInfo : function(userservice){
                return userservice.setUserInfo();
            },
            roleList : function(userservice){
                return userservice.roleList();
            }
        }
    })

    //用户列表
    .state('app.userInfoList', {
        url: "/user/userinfo",
        views: {
            'main@' : {
                template : require('./views/userinfo.html'),
                controller : 'userInfo',
            }
        },
        resolve:{
            transData : function(utilservice){
                return utilservice.transData;
            },
            userInfo : function(userservice){
                return userservice.userInfo();
            },
            addUser : function(userservice){
                return userservice.addUser();
            },
            setUserInfo : function(userservice){
                return userservice.setUserInfo();
            },
            roleList : function(userservice){
                return userservice.roleList();
            }
        }
    })

    //用户列表
    .state('app.userInfoList_new', {
        url: "/user/userinfonew",
        views: {
            'main@' : {
                template : require('./views/userinfo_new.html'),
                controller : 'userInfo_new',
            }
        },
        resolve:{
            transData : function(utilservice){
                return utilservice.transData;
            },
            userInfo : function(userservice){
                return userservice.userInfo();
            },
            addUser : function(userservice){
                return userservice.addUser();
            },
            setUserInfo : function(userservice){
                return userservice.setUserInfo();
            },
            roleList : function(userservice){
                return userservice.roleList();
            }
        }
    })

    //用户列表
    .state('app.setOfficeNotice', {
        url: "/bindwechat/list",
        views: {
            'main@' : {
                template : require('./views/bindWechat.html'),
                controller : 'bindWechat',
            }
        },
        resolve:{
            
        }
    })


	;

};

module.exports = router;