var App = angular.module('system', []);

App.config(require('./router'));

//service
App.factory('systemservice', require('./service'));

//Controllers
App.controller('systemList', require('./controllers/systemList'));


module.exports = App;