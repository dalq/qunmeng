module.exports = function ($scope, $state, $resource) {
	
	//加载系统列表
	$scope.search = function(pageNo){
		var para = {
            'pageNo': pageNo,
			'pageSize': $scope.itemsPerPage,
			'system_name' : $scope.searchform.label
		};
		$resource('/api/ac/sc/sysSystemService/systemList', {}, {}).save(para, function(res){
			if(res.errcode === 0){
				$scope.system_list = res.data.results;
				$scope.totalItems = res.data.totalRecord;
			}else{
				alert(res.errmsg);
			}
		});
	}

	init();
	function init(){
		$scope.currentPage = 1;			//当前页码
		$scope.itemsPerPage = 10;		//每页显示几条
		$scope.searchform = {};
		$scope.obj = {};
		$scope.status = {'active': true, 'addsystem': false};
		$scope.tabname = {'name': '添加系统', 'active': false};
		$scope.search(1);
	}

	//取消==重置
	$scope.cancel = function (){
		$scope.status.active = true;
		$scope.status.addsystem = false;
		$scope.tabname.name = '添加系统';
		$scope.tabname.active = false;
		
	}

	//添加系统
	$scope.add = function (){
		if($scope.tabname.name == '修改系统'){
			return;
		}
		$scope.tabname.active = true;
		$scope.status.addsystem = true;
		var temp = $scope.system_list[$scope.system_list.length-1].system_sort + 10 || 10;
		$scope.obj = {'system_sort': temp};
	}

	//保存
	$scope.save = function (){
		$resource('/api/ac/sc/sysSystemService/saveSystem', {}, {}).save($scope.obj, function(res){
			if(res.errcode === 0 && res.data == 1){
				$scope.cancel();
				$scope.search(1);
			}else{
				alert(res.errmsg);
			}
		});
	}

	//修改系统
	$scope.update = function (item){
		$scope.tabname.name = '修改系统';
		$scope.tabname.active = true;
		$scope.obj = item;
	}

	//删除系统
	$scope.delete = function (index){
		if(confirm('确认删除此系统吗?\r\n删除后将导致此系统下的菜单不可用')==true){
			$resource('/api/ac/sc/sysSystemService/deleteSys', {}, {}).save($scope.system_list[index], function(res){
				if(res.errcode === 0){
					$scope.system_list.splice(index,1);
				}else{
					alert(res.errmsg);
				}
			});
		}
	}

};