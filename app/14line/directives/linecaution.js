module.exports = function($modal, $state, $resource){

	return {

		restrict : 'AE',
		template : require('../views/line_caution.html'),
		replace:true,
		scope:{
			'model' : '=',
			'util' : '=',
			// 'result' : '=',
			// 'dateshow' : '=',
			// 'timeshow' : '=',
			// 'imageshow' : '=',
			// 'selectobj' : '=',
			// 'model' : '=',
			// 'hstep' : '=',
			// 'mstep' : '=',
			// 'ismeridian' : '=',
		},
		link : function(scope, elements, attrs){

			scope.result = {};
			scope.dateshow = {};
			scope.imageshow = {};
			scope.info = {};
			scope.a='';
			// $resource('/api/as/lc/travelWarning/findinfo', {}, {}).
			// get({'product_id' : scope.util.product_id}, function(res){
			// 	console.log('购买须知');
            //     console.log(res);
            //     if(res.errcode === 0 || res.errcode === 10003){
            //     	scope.util.init({
			// 			'model' : scope.model,
			// 			'result' : scope.result,
			// 			'dateshow' : scope.dateshow,
			// 			'imageshow' : scope.imageshow,
			// 			'info' : res.errcode == 0 ? res.data : {},
			// 		});
            //     }else{
            //         alert(res.errmsg);
            //     }
            // });
			$resource('/api/as/lc/travelWarning/findinfo', {}, {}).
			get({'product_id' : scope.util.product_id}, function(res){
				// console.log('购买须知');
                // console.log(res);
				scope.a=res.data;
				scope.result.bed_material_commit=res.data.bed_material_commit;
				scope.result.payinfo_advance=res.data.payinfo_advance;
				scope.result.payinfo_full=res.data.payinfo_full;
				
				scope.result.hint_visa_attach_prompt1=angular.fromJson(res.data.hint_visa_attach_prompt);

				// console.log(scope.result.hint_visa_attach_prompt1);
                if(res.errcode === 0 || res.errcode === 10003){
                	scope.util.init({
						'model' : scope.model,
						'result' : scope.result,
						'dateshow' : scope.dateshow,
						'imageshow' : scope.imageshow,
						'info' : res.errcode == 0 ? res.data : {},
					});
                }else{
                    alert(res.errmsg);
                }
            });

			scope.util.init({
				'model' : scope.model,
				'result' : scope.result,
				'dateshow' : scope.dateshow,
				'imageshow' : scope.imageshow,
				'info' : scope.info
			});
			

			scope.result['product_id'] = scope.util.product_id;
			scope.gogogo = function(){
				
				if(!scope.result.bed_material_commit  || scope.result.bed_material_commit==''){
					alert('提前收取材料日期不可为空');
					return;
				}
				if(!scope.result.payinfo_full  || scope.result.payinfo_full==''){
					alert('支付类型中时间不可为空');
					return;
				}
				if(!scope.result.payinfo_advance  || scope.result.payinfo_advance==''){
					alert('支付类型中时间不可为空');
					return;
				}
				
				
				if(scope.util.product_state_name=='待上架'||scope.util.product_state_name=='已上架'||scope.util.product_state_name=='已下架'){
					alert('该产品不为草稿状态，只有价格库存中日库存量可更改，其余不可更改!');
					return;
				}
				
				if(scope.result.hint_cancel_trip==0){
					// console.log(scope.result.hint_min_travel_population);
					if(scope.result.hint_min_travel_population<0){
							alert('取消行程最小人数不可为负数');
							return;
					}
					if(!scope.result.hint_min_travel_population  || scope.result.hint_min_travel_population==''){
						if(scope.result.hint_min_travel_population!=0){
							alert('取消行程最小人数不可为空');
							return;
						}
					}
					if(scope.result.hint_ravel_advance_notification<0){
							alert('提前通知天数不可为负数');
							return;
						}
					if(scope.result.hint_ravel_advance_notification=='' || !scope.result.hint_ravel_advance_notification){
						if(scope.result.hint_ravel_advance_notification!=0){
							alert('提前通知天数不可为空');
							return;
						}
					}
				}else{
					scope.result.hint_min_travel_population='';
					scope.result.hint_ravel_advance_notification=null
				}
				if(scope.result.hint_touring_penalty==0){
					if(scope.result.hint_touring_out_money<0){
							alert('违约金额不可为负数');
							return;
						}
					if(scope.result.hint_touring_out_money=='' || !scope.result.hint_touring_out_money){
						if(scope.result.hint_touring_out_money!=0){
							alert('违约金额不可为空');
							return;
						}
					}
				}else{
					scope.result.hint_touring_out_money=null;
				}
				if(scope.result.additional_hainan_payment==0){
					if(scope.result.additional_fee<0){
							alert('海南政府调节金不可为负数');
							return;
						}
					if(scope.result.additional_fee=='' || !scope.result.additional_fee){
						if(scope.result.additional_fee!=0){
							alert('海南政府调节金不可为空');
							return;
						}
					}
				}else{
					scope.result.additional_fee=null;
				}
				if(scope.result.important_spell_group==1){
					if(scope.result.important_spell_group_extras=='' || !scope.result.important_spell_group_extras){
						alert('拼团备注不可为空');
						return;
					}
				}else{
					scope.result.important_spell_group_extras='';
				}
				if(scope.result.important_spell_car==0){
					if(scope.result.important_spell_car_remark=='' || !scope.result.important_spell_car_remark){
						alert('拼车备注不可为空');
						return;
					}
				}else{
					scope.result.important_spell_car_remark='';
				}
				if(scope.result.important_change_car_guide==0){
					if(scope.result.important_change_car_guide_remark=='' || !scope.result.important_change_car_guide_remark){
						alert('换车/导游备注不可为空');
						return;
					}
				}else{
					scope.result.important_change_car_guide_remark='';
				}
				if(scope.result.important_update_travel==0){
					if(scope.result.important_update_travel_remark=='' || !scope.result.important_update_travel_remark){
						alert('升级备注不可为空');
						return;
					}
				}else{
					scope.result.important_update_travel_remark='';
				}
				if(scope.result.spellgroup_count_right==0){
					if(scope.result.spellgroup_estimate_remark=='' || !scope.result.spellgroup_estimate_remark){
						alert('拼团次数备注不可为空');
						return;
					}
				}else{
					scope.result.spellgroup_estimate_remark='';
				}
				if(scope.result.special_order_min_right==0){
					if(scope.result.special_order_min_num<0){
							alert('最少人数不可为负数');
							return;
						}
					if(scope.result.special_order_min_num=='' || !scope.result.special_order_min_num){
						if(scope.result.special_order_min_num!=0){
							alert('最少人数不可为空');
							return;
						}
					}
				}else{
					scope.result.special_order_min_num=null;
				}
				if(scope.result.special_order_max_right==0){
					if(scope.result.special_order_max_num<0){
							alert('最大人数不可为负数');
							return;
						}
					if(scope.result.special_order_max_num=='' || !scope.result.special_order_max_num){
						if(scope.result.special_order_max_num!=0){
							alert('最大人数不可为空');
							return;
						}
					}
				}else{
					scope.result.special_order_max_num=null;
				}
				if(scope.result.special_age_min_right==0){
					if(scope.result.special_age_min_num<0){
							alert('最小年龄不可为负数');
							return;
						}
					if(scope.result.special_age_min_num=='' || !scope.result.special_age_min_num){
						if(scope.result.special_age_min_num!=0){
							alert('最小年龄不可为空');
							return;
						}
					}
				}else{
					scope.result.special_age_min_num=null;
				}
				if(scope.result.special_age_max_right==0){
					if(scope.result.special_age_max_num<0){
							alert('最大年龄不可为负数');
							return;
						}
					if(scope.result.special_age_max_num=='' || !scope.result.special_age_max_num){
						if(scope.result.special_age_max_num!=0){
							alert('最大年龄不可为空');
							return;
						}
					}
				}else{
					scope.result.special_age_max_num=null;
				}
				if(scope.result.special_exceed_age_right==0){
					if(scope.result.special_exceed_age_num<0){
							alert('需要签署健康协议年龄限制不可为负数');
							return;
						}
					if(scope.result.special_exceed_age_num=='' || !scope.result.special_exceed_age_num){
						if(scope.result.special_exceed_age_num!=0){
							alert('需要签署健康协议年龄限制不可为空');
							return;
						}
					}
				}else{
					scope.result.special_exceed_age_num=null;
				}
				if(scope.result.special_age_restriction==0){
					if(scope.result.special_age_restriction_num=='' ||!scope.result.special_age_restriction_num){
						alert('年龄限制范围不可为空');
						return;
					}
				}else{
					scope.result.special_age_restriction_num='';
				}
				if(scope.result.special_region_limit==0){
					if(scope.result.special_region_limit_list=='' || !scope.result.special_region_limit_list){
						alert('地域限制不可为空');
						return;
					}
				}else{
					scope.result.special_region_limit_list='';
				}
				// scope.result.else_arr
				var str= '';
				//遍历多选框对象信息。
				angular.forEach(scope.result.hint_visa_attach_prompt1, function(value, key){
					// alert(123);
					if(value){
						
						str += key + ',';
					}
				});
				scope.result.else_arr=str;
				scope.result['hint_visa_attach_prompt']=angular.toJson(scope.result.hint_visa_attach_prompt1);
				// console.log(scope.result);
				$resource('/api/as/lc/travelWarning/create', {}, {}).save(scope.result, function(res){
                    // console.log(res);
                    if(res.errcode === 0){
						 $state.go('app.line_edit1', {'id' : scope.util.product_id});
						 alert('保存成功');
                    }else{
                        alert(res.errmsg);
                    }
                });
			};
		}

	};

};

