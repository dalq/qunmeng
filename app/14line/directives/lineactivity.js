module.exports = function($modal, $state, $resource){

	return {

		restrict : 'AE',
		template : require('../views/line_activity.html'),
		replace:true,
		scope:{
			'model' : '=',
			'util' : '=',
		},
		link : function(scope, elements, attrs){
            scope.arr = [
				{ firstname: '' ,secondname:'',thirdname :''},
			];
            scope.arr1 = [
                {name : ''},
                {name : ''},
            ]
			scope.result = {};
			scope.dateshow = {};
			scope.imageshow = {};
			scope.info = {};
			scope.a='';

            $resource('/api/as/lc/characteristic/findinfo', {}, {}).
			get({'product_id' : scope.util.product_id}, function(res){
				// console.log('活动特色');
                // console.log(res);
				scope.a=res.data;
				var retarr = [];
				retarr = angular.fromJson(res.data.title_class_json);
				if(retarr.length!=0){
					scope.arr=[];
				}
				
				for (var x=0;x<retarr.length;x++){
					scope.arr.push({firstname : retarr[x].firstname, secondname : retarr[x].secondname, thirdname : retarr[x].thirdname});
				};
				var arrtuijian = res.data.product_manage_ecommend_arr.split("|");
				if(arrtuijian.length!=0){
					scope.arr1 = [];
				}
				
				for (var y=0;y<arrtuijian.length;y++){
					scope.arr1.push({name : arrtuijian[y]});
				};
                if(res.errcode === 0 || res.errcode === 10003){
                	scope.util.init({
						'model' : scope.model,
						'result' : scope.result,
						'dateshow' : scope.dateshow,
						'imageshow' : scope.imageshow,
						'info' : res.errcode == 0 ? res.data : {},
					});
                }else{
                    alert(res.errmsg);
                }
            });
			scope.util.init({
				'model' : scope.model,
				'result' : scope.result,
				'dateshow' : scope.dateshow,
				'imageshow' : scope.imageshow,
				'info' : scope.info
			});
			scope.adda = function () {
				scope.arr.push({ firstname: '' ,secondname:'',thirdname :''});
			};
            scope.add = function(){
                if(scope.arr1.length==4){
                    alert('最多可添加4条');
                    return;
                }
                scope.arr1.push({name : ''});
                
            };
            var arrindex=scope.arr.length+1;
            scope.sub = function(index){
                if(scope.arr.length==1){
                    alert('最少有一条');
                    return;
                }
                scope.arr.splice(index,1);
            };
             scope.sub1 = function(index){
                 if(scope.arr1.length==2){
                    alert('最少有两条');
                    return;
                }
                scope.arr1.splice(index,1);
            };
			scope.gogogo = function(){
				 if(scope.lineactivity.$valid){
					//alert('通过验证可以提交表单');
				}else{
					//alert('表单没有通过验证');
					return;
				}
				if(scope.util.product_state_name=='待上架'||scope.util.product_state_name=='已上架'||scope.util.product_state_name=='已下架'){
					alert('该产品不为草稿状态，只有价格库存中日库存量可更改，其余不可更改!');
					return;
				}
                var names = "";
				for (var i = 0; i < scope.arr1.length; i++) {
					if (i == 0) {
						names = scope.arr1[i].name;
					} else {
						names += "|" + scope.arr1[i].name;
					}
					// names=scope.arr[i]+names+",";
				}

                
				scope.result['product_manage_ecommend_arr']=names;
				scope.result['title_class_json']=angular.toJson(scope.arr);
                scope.result['product_id'] = scope.util.product_id;
				$resource('/api/as/lc/characteristic/create', {}, {}).save(scope.result, function(res){
                    // console.log(res);
                    if(res.errcode === 0){
                        alert('保存成功');
						
                    }else{
                        alert(res.errmsg);
                    }
                });
			};
		}

	};

};

