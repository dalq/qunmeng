module.exports = function ($modal, $state, $resource) {

	return {

		restrict: 'AE',
		template: require('../views/line_explain.html'),
		replace: true,
		scope: {
			'model': '=',
			'util': '=',
			// 'result' : '=',
			// 'dateshow' : '=',
			// 'timeshow' : '=',
			// 'imageshow' : '=',
			// 'selectobj' : '=',
			// 'model' : '=',
			// 'hstep' : '=',
			// 'mstep' : '=',
			// 'ismeridian' : '=',
		},
		link: function (scope, elements, attrs) {
			scope.arr = [
				{ name: '' }
			];
			
			scope.result = {};
			scope.dateshow = {};
			scope.imageshow = {};
			scope.a='';

			$resource('/api/as/lc/backExplain/findinfo', {}, {}).
			get({'product_id' : scope.util.product_id}, function(res){
				// console.log('退款说明');
                // console.log(res);
				scope.a=res.data;
				scope.result.else_terms = res.data.else_terms;
				scope.result.terms_type =res.data.terms_type;

				scope.result.terms_type1 = angular.fromJson(res.data.terms_type);


				

				if(res.data.terms_type==0){
					scope.result.else_terms='';
				}else if(res.data.terms_type==1){
					scope.result.with_group_terms='';
				};
                if(res.errcode === 0 || res.errcode === 10003){
                	scope.util.init({
						'model' : scope.model,
						'result' : scope.result,
						'dateshow' : scope.dateshow,
						'imageshow' : scope.imageshow,
						'info' : res.errcode == 0 ? res.data : {},
					});
                }else{
                    alert(res.errmsg);
                }
            });

			scope.adda = function () {
				scope.arr.push({ name: '' });
			};
			scope.result['product_id'] = scope.util.product_id;
			scope.gogogo = function () {
				if(scope.util.product_state_name=='待上架'||scope.util.product_state_name=='已上架'||scope.util.product_state_name=='已下架'){
					alert('该产品不为草稿状态，只有价格库存中日库存量可更改，其余不可更改!');
					return;
				}
				// if(scope.result.terms_type==0){
				// 	scope.result.else_terms='';
				// 	if(scope.result.with_group_terms=='' || !scope.result.with_group_terms){
				// 		alert('条款说明不可为空1');
				// 		return;
				// 	}
				// }else if(scope.result.terms_type==1){
				// 	scope.result.with_group_terms='';
				// 	if(scope.result.else_terms=='' || !scope.result.else_terms){
				// 		alert('条款说明不可为空2');
				// 		return;
				// 	}
				// };
				// console.log(scope.result);
				//生成调用接口需要传的数据。
				var para = {
					'with_group_terms' : '',
					'product_id' : scope.util.product_id,
					'else_terms' : '',
				};
				var str='';
				scope.result.with_group_terms='';
				// 遍历多选框对象信息。
				angular.forEach(scope.result.terms_type1, function(value, key){
					if(value){
						str += key + ',';
					}
				});
				scope.result.with_group_terms=str;
				scope.result['terms_type']=angular.toJson(scope.result.terms_type1);
				// 遍历'其他'文本框的数据，写在这里。
				
				// console.log(para);
				var names = "";
				// var shuzi = "";
				for (var i = 0; i < scope.arr.length; i++) {
					if (i == 0) {
						names = scope.arr[i].name;
					} else {
						names += "," + scope.arr[i].name;
					}
					// names=scope.arr[i]+names+",";
				}
				scope.result.else_terms = names;
				// para.else_terms = names;
				
				scope.result['product_id'] = scope.util.product_id;
				$resource('/api/as/lc/backExplain/create', {}, {}).save(scope.result, function (res) {
					// console.log(res);
					
					if (res.errcode === 0) {
						alert('保存成功');
					} else {
						alert(res.errmsg);
					}
				});
			};
		}

	};

};

