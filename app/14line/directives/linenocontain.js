module.exports = function($modal, $state, $resource){
//费用包含
	return {

		restrict : 'AE',
		template : require('../views/line_nocontain.html'),
		replace:true,
		scope:{
			'model' : '=',
			'util' : '=',

		},
		link : function(scope, elements, attrs){
			scope.arr = [
				{ name: '' }
			];
			scope.arrfirst = [
				{first : '',second : ''}
			]
			scope.result = {};
			scope.dateshow = {};
			scope.imageshow = {};
			scope.info = {};
			scope.a='';

			$resource('/api/as/lc/notincludePrice/findinfo', {}, {}).
			save({'product_id' : scope.util.product_id}, function(res){
				// console.log('费用不包含');
                // console.log(res);
				scope.a=res.data;
				var retarr = [];
				retarr = angular.fromJson(res.data.single_room_json);
				scope.arrfirst=[];
				for (var x=0;x<retarr.length;x++){
					scope.arrfirst.push({first : retarr[x].first, second : retarr[x].second});
				};
				var arrtuijian = res.data.other_arr.split("|");
				scope.arr = [];
				for (var y=0;y<arrtuijian.length;y++){
					scope.arr.push({name : arrtuijian[y]});
				};
				scope.result.single_room_margin_custom=res.data.single_room_margin_custom;
				if(res.data.single_room_trpe==0){
					scope.result.single_room_margin_custom='';
				}else if(res.data.single_room_trpe==1){
					scope.result.single_room_margin_custom='';
					scope.arrfirst=[{first : '',second : ''}];
				}else if(res.data.single_room_trpe==2){
					scope.arrfirst=[{first : '',second : ''}];
				};
                if(res.errcode === 0 || res.errcode === 10003){
                	scope.util.init({
						'model' : scope.model,
						'result' : scope.result,
						'dateshow' : scope.dateshow,
						'imageshow' : scope.imageshow,
						'info' : res.errcode == 0 ? res.data : {},
					});
                }else{
                    alert(res.errmsg);
                }
            });

			scope.util.init({
				'model' : scope.model,
				'result' : scope.result,
				'dateshow' : scope.dateshow,
				'imageshow' : scope.imageshow,
				'info' : scope.info
			});


			scope.result['trip_id'] = '1000';
			
			scope.result['product_id'] = scope.util.product_id;
			scope.adda = function () {
				scope.arr.push({ name: '' });
			};
			scope.sub1 = function(index){
                 if(scope.arr.length==1){
                    alert('最少有一条');
                    return;
                }
                scope.arr.splice(index,1);
            };
			scope.sub = function(index){
                 if(scope.arrfirst.length==1){
                    alert('最少有一条');
                    return;
                }
                scope.arrfirst.splice(index,1);
            };
			scope.addfirst = function () {
				scope.arrfirst.push({ first: '' ,second : ''});
			};
			scope.gogogo = function(){
				var names = "";
				for (var i = 0; i < scope.arr.length; i++) {
					if (i == 0) {
						names = scope.arr[i].name;
					} else {
						names += "|" + scope.arr[i].name;
					}
					// names=scope.arr[i]+names+",";
				}
				if(scope.util.product_state_name=='待上架'||scope.util.product_state_name=='已上架'||scope.util.product_state_name=='已下架'){
					alert('该产品不为草稿状态，只有价格库存中日库存量可更改，其余不可更改!');
					return;
				}
				var jsonstr = '';
				for (var i = 0; i < scope.arrfirst.length; i++) {
					jsonstr+=scope.arrfirst[i];
				}
				scope.result['single_room_json']=angular.toJson(scope.arrfirst);
				scope.result['other_arr']=names;
				// scope.result['single_room_json']=jsonstr;
				$resource('/api/as/lc/notincludePrice/create', {}, {}).save(scope.result, function(res){
                    if(res.errcode === 0){
                        alert('保存成功');
                    }else{
                        alert(res.errmsg);
                    }
                });
			};
		}

	};

};

