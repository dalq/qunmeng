module.exports = function($modal, $state, $resource){
//费用包含
	return {

		restrict : 'AE',
		template : require('../views/line_contain.html'),
		replace:true,
		scope:{
			'model' : '=',
			'util' : '=',

		},
		link : function(scope, elements, attrs){
			scope.arr = [
				{ name: '' }
			];
			scope.result = {};
			scope.dateshow = {};
			scope.imageshow = {};

			$resource('/api/as/lc/includePrice/findinfo', {}, {}).
			save({'product_id' : scope.util.product_id}, function(res){
				 console.log('费用包含返回值');			
                 console.log(res);
				
				





                if(res.errcode === 0 || res.errcode === 10003){
                	scope.util.init({
						'model' : scope.model,
						'result' : scope.result,
						'dateshow' : scope.dateshow,
						'imageshow' : scope.imageshow,
						'info' : res.errcode == 0 ? res.data : {},
					});
					//lc
					var c = document.getElementById('big_traffic_ticket_type');
					
					if(res.data.big_traffic_economy_ticket==0){

					}
					var arrtuijian = res.data.else_messages_arr.split("|");
					scope.arr = [];
					for (var y=0;y<arrtuijian.length;y++){
						scope.arr.push({name : arrtuijian[y]});
					};
					
					scope.result.dining_early = res.data.dining_early;
					scope.result.dining_dinner = res.data.dining_dinner;
					scope.result.dining_spend = res.data.dining_spend;
					scope.result.dining_people_num = res.data.dining_people_num;
					scope.result.dining_table_num = res.data.dining_table_num;
					scope.result.dining_food_num = res.data.dining_food_num;
					scope.result.dining_soup_num = res.data.dining_soup_num;
					scope.result.dining_custom=res.data.dining_custom;


					scope.result.child_fee_age_start = res.data.child_fee_age_start;
					scope.result.child_fee_age_end = res.data.child_fee_age_end;
					scope.result.child_fee_height_start = res.data.child_fee_height_start;
					scope.result.child_fee_height_end = res.data.child_fee_height_end;
					scope.result.child_fee_exist = res.data.child_fee_exist;

					if(res.data.dining_type==0){
						scope.result.dining_custom='';
					}else if(res.data.dining_type==1||res.data.dining_type==2){
						scope.result.dining_early = '';
						scope.result.dining_dinner = '';
						scope.result.dining_spend = '';
						scope.result.dining_people_num = '';
						scope.result.dining_table_num = '';
						scope.result.dining_food_num = '';
						scope.result.dining_soup_num = '';

						scope.result.dining_custom='';
					}else if(res.data.dining_type==3){
						scope.result.dining_early = '';
						scope.result.dining_dinner = '';
						scope.result.dining_spend = '';
						scope.result.dining_people_num = '';
						scope.result.dining_table_num = '';
						scope.result.dining_food_num = '';
						scope.result.dining_soup_num = '';
					}

					if(res.data.child_fee_exist==1){
						scope.result.child_fee_age_start = '';
						scope.result.child_fee_age_end = '';
						scope.result.child_fee_height_start = '';
						scope.result.child_fee_height_end = '';
					}
					
                }else{
                    alert(res.errmsg);
                }
            });



			scope.adda = function () {
				scope.arr.push({ name: '' });
			};
			scope.sub1 = function(index){
                 if(scope.arr.length==1){
                    alert('最少有一条');
                    return;
                }
                scope.arr.splice(index,1);
            };
			// scope.result['dining_type'] = '2';
			// scope.result['trip_id'] = '1000';
			
			scope.result['product_id'] = scope.util.product_id;
			// scope.result['trip_id'] = scope.util.trip_id;

			scope.gogogo = function(){
				if(scope.util.product_state_name=='待上架'||scope.util.product_state_name=='已上架'||scope.util.product_state_name=='已下架'){
					alert('该产品不为草稿状态，只有价格库存中日库存量可更改，其余不可更改!');
					return;
				}

				if(scope.result.safe_contain==0){
					if(scope.result.safe_content=='' || !scope.result.safe_content){
						alert('保险备注不可为空');
						return;
					}
				}else{
					scope.result.safe_content='';
				}
				if(scope.result.stay_type==1){
					if(scope.result.stay_hotel_star=='' || !scope.result.stay_hotel_star){
						if(scope.result.stay_hotel_star!=0){
							alert('酒店星级不可为空');
							return;
						}
					}
					if(scope.result.stay_room_capacity=='' || !scope.result.stay_room_capacity){
						if(scope.result.stay_room_capacity!=0){
							alert('标准几人间不可为空');
							return;
						}
					}
					scope.result.dest_hotel_messages_describe=''
					scope.result.dest_check_in=null
					scope.result.dest_room_capacity=null
					scope.result.dest_hotel_destination=''

					scope.result.stay_hotel_num=null;
				}else if(scope.result.stay_type==2){
					if(scope.result.stay_hotel_num=='' || !scope.result.stay_hotel_num){
						if(scope.result.stay_hotel_num!=0){
							alert('标准几人间不可为空');
							return;
						}
					}
					scope.result.dest_hotel_messages_describe=''
					scope.result.dest_check_in=null
					scope.result.dest_room_capacity=null
					scope.result.dest_hotel_destination=''

					scope.result.stay_room_capacity=null;
					scope.result.stay_hotel_star=null ;
					
				}else if(scope.result.stay_type==3){
					if(scope.result.dest_hotel_destination=='' || !scope.result.dest_hotel_destination){
						alert('酒店类型不可为空');
						return;
					}
					if(scope.result.dest_room_capacity=='' || !scope.result.dest_room_capacity){
						if(scope.result.dest_room_capacity!=0){
							alert('标准几人间不可为空');
							return;
						}
					}
					if(scope.result.dest_check_in=='' || !scope.result.dest_check_in){
						if(scope.result.dest_check_in!=0){
							alert('入住几晚不可为空');
							return;
						}
					}
					if(scope.result.dest_hotel_messages_describe=='' || !scope.result.dest_hotel_messages_describe){
						alert('其他说明不可为空');
						return;
					}

					scope.result.stay_hotel_num=null;

					scope.result.stay_room_capacity=null
					scope.result.stay_hotel_star=null
				}else {
					scope.result.dest_hotel_messages_describe=''
					scope.result.dest_check_in=null
					scope.result.dest_room_capacity=null
					scope.result.dest_hotel_destination=''

					scope.result.stay_hotel_num=null

					scope.result.stay_room_capacity=null
					scope.result.stay_hotel_star=null
				}
				if(scope.result.cruises_dest_messages==0){
					if(scope.result.cruises_dest__room_capacity=='' || !scope.result.cruises_dest__room_capacity){
						if(scope.result.cruises_dest__room_capacity!=0){
							alert('游轮标准间容量不可为空');
							return;
						}
					}
					if(scope.result.cruises_dest_check_in=='' || !scope.result.cruises_dest_check_in){
						if(scope.result.cruises_dest_check_in!=0){
							alert('入住几晚不可为空');
							return;
						}
					}
					if(scope.result.cruises_dest_messages_describe=='' || !scope.result.cruises_dest_messages_describe){
						alert('游轮其他说明不可为空');
						return;
					}
				}else{
					scope.result.cruises_dest__room_capacity=null;
					scope.result.cruises_dest_check_in=null;
					scope.result.cruises_dest_messages_describe='';					
				}
				if(scope.result.admission_ticket==0){
					if(scope.result.admission_ticket_attractions=='' || !scope.result.admission_ticket_attractions){
						alert('首道景点门票不可为空');
						return;
					}
					if(scope.result.admission_ticket_coupon_attractions=='' || !scope.result.admission_ticket_coupon_attractions){
						alert('联票包含门票不可为空');
						return;
					}
					if(scope.result.admission_ticket_free_attractions=='' || !scope.result.admission_ticket_free_attractions){
						alert('赠送项目门票不可为空');
						return;
					}
				}else{
					scope.result.admission_ticket_attractions='';
					scope.result.admission_ticket_coupon_attractions='';
					scope.result.admission_ticket_free_attractions='';					
				}

				if(scope.result.dining_type==0){
					if(scope.result.dining_early=='' || !scope.result.dining_early){
						if(scope.result.dining_early!=0){
							alert('含餐信息不可为空');
							return;
						}
						
					}
					// console.log(scope.result.dining_spend);
					if(scope.result.dining_dinner=='' || !scope.result.dining_dinner){
						if(scope.result.dining_dinner!=0){
							alert('含餐信息不可为空');
							return;
						}
					}
					if(scope.result.dining_spend=='' || !scope.result.dining_spend){
						alert('含餐信息不可为空');
							return;
					}
					if(scope.result.dining_people_num=='' || !scope.result.dining_people_num){
						if(scope.result.dining_people_num!=0){
							alert('含餐信息不可为空');
							return;
						}
					}
					if(scope.result.dining_table_num=='' || !scope.result.dining_table_num){
						if(scope.result.dining_table_num!=0){
							alert('含餐信息不可为空');
							return;
						}
					}
					if(scope.result.dining_food_num=='' || !scope.result.dining_food_num){
						if(scope.result.dining_food_num!=0){
							alert('含餐信息不可为空');
							return;
						}
					}
					if(scope.result.dining_soup_num=='' || !scope.result.dining_soup_num){
						if(scope.result.dining_soup_num!=0){
							alert('含餐信息不可为空');
							return;
						}
					}
					scope.result.dining_custom=''
				}else if(scope.result.dining_type==3){
					if(scope.result.dining_custom=='' || !scope.result.dining_custom){
						if(scope.result.dining_custom!=0){
							alert('含餐信息不可为空');
							return;
						}
					}
					scope.result.dining_early=null;
					scope.result.dining_dinner=null;
					scope.result.dining_spend='';
					scope.result.dining_people_num=null ;
					scope.result.dining_table_num=null;
					scope.result.dining_food_num=null;
					scope.result.dining_soup_num=null;
				}
				else{
					scope.result.dining_early=null;
					scope.result.dining_dinner=null;
					scope.result.dining_spend='';
					scope.result.dining_people_num=null ;
					scope.result.dining_table_num=null;
					scope.result.dining_food_num=null;
					scope.result.dining_soup_num=null;

					scope.result.dining_custom=''
				}

				if(scope.result.guide_service_driver_tip==0){
					if(scope.result.guide_service_driver_price=='' || !scope.result.guide_service_driver_price){
						if(scope.result.guide_service_driver_price!=0){
							alert('小费价格不可为空');
							return;
						}
					}
					if(scope.result.guide_service_driver_explain=='' || !scope.result.guide_service_driver_explain){
						alert('小费说明不可为空');
						return;
					}
				}else{
					scope.result.guide_service_driver_price=null;
					scope.result.guide_service_driver_explain='';
				}

				if(scope.result.child_fee_exist==0){
					if(scope.result.child_fee_age_end=='' || !scope.result.child_fee_age_end){
						if(scope.result.child_fee_age_end!=0){
							alert('儿童最大年龄不可为空');
							return;
						}
					}
					if(scope.result.child_fee_age_start=='' || !scope.result.child_fee_age_start){
						if(scope.result.child_fee_age_start!=0){
							alert('儿童最小年龄不可为空');
							return;
						}
					}
					if(scope.result.child_fee_height_start=='' || !scope.result.child_fee_height_start){
						if(scope.result.child_fee_height_start!=0){
							alert('儿童最低身高不可为空');
							return;
						}
					}
					if(scope.result.child_fee_height_end=='' || !scope.result.child_fee_height_end){
						if(scope.result.child_fee_height_end!=0){
							alert('儿童最高身高不可为空');
							return;
						}
					}
					if(scope.result.child_fee_age_start>scope.result.child_fee_age_start){
						if(scope.result.child_fee_age_start!=0){
							alert('儿童最小年龄不可大于最大年龄');
							return;
						}
					}
					if(scope.result.child_fee_height_start>scope.result.child_fee_height_end){
						if(scope.result.child_fee_height_start!=0){
							alert('儿童最小身高不可大于最大身高');
							return;
						}
					}
				}else{
					scope.result.child_fee_age_end=null;
					scope.result.child_fee_age_start=null;
					scope.result.child_fee_height_start=null;
					scope.result.child_fee_height_end=null;
				}

				if(scope.result.ticket_tax_round_trip==0){
					if(scope.result.ticket_tax_fee=='' || !scope.result.ticket_tax_fee){
						if(scope.result.ticket_tax_fee!=0){
							alert('机票税价格不可为空');
							return;
						}
					}
				}else{
					scope.result.ticket_tax_fee=null;
				}
				
				var other = "";
				for (var i = 0; i < scope.arr.length; i++) {
					if (i == 0) {
						other = scope.arr[i].name;
					} else {
						other += "|" + scope.arr[i].name;
					}
					// names=scope.arr[i]+names+",";
				}
				// console.log(scope.result);
				scope.result['else_messages_arr']=other;
				console.log(scope.result);
				console.log('上面是提交的result')
				$resource('/api/as/lc/includePrice/create', {}, {}).save(scope.result, function(res){
                    // console.log(res);
                    if(res.errcode === 0){
                        alert('保存成功');
                    }else{
                        alert(res.errmsg);
                    }
                });
			};
		}

	};

};

