module.exports = function($modal, $state, $resource){
//费用包含
	return {

		restrict : 'AE',
		template : require('../views/line_picture.html'),
		replace:true,
		scope:{
			'model' : '=',
			'util' : '=',
		},
		link : function(scope, elements, attrs){

			scope.result = {};
			scope.dateshow = {};
			scope.imageshow = {};
			scope.info = {};
			scope.a='';

			scope.util.initother({
				'model' : scope.model,
				'result' : scope.result,
				'dateshow' : scope.dateshow,
				'imageshow' : scope.imageshow,
			});

			$resource('/api/as/lc/image/findinfo', {}, {}).
			get({'product_id' : scope.util.product_id}, function(res){
				// console.log('购买须知');
                // console.log(res);
				scope.a=res.data;
                if(res.errcode === 0 || res.errcode === 10003){
                	scope.util.init({
						'model' : scope.model,
						'result' : scope.result,
						'dateshow' : scope.dateshow,
						'imageshow' : scope.imageshow,
						'info' : res.errcode == 0 ? res.data : {},
					});
                }else{
                    alert(res.errmsg);
                }
            });
			



			scope.result['product_id'] = scope.util.product_id;
			// console.log(scope.result);
			scope.gogogo = function(){
				if(scope.util.product_state_name=='待上架'||scope.util.product_state_name=='已上架'||scope.util.product_state_name=='已下架'){
					alert('该产品不为草稿状态，只有价格库存中日库存量可更改，其余不可更改!');
					return;
				}
				if(scope.result == undefined ||scope.result == ''||scope.result == null){
					alert('至少添加一张图片')
				}
				$resource('/api/as/lc/image/create', {}, {}).save(scope.result, function(res){
                    // console.log(res);
                    if(res.errcode === 0){
                        alert('保存成功');
                    }else{
                        alert(res.errmsg);
                    }
                });
			};
		}

	};

};

