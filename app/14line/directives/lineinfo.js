module.exports = function($resource, $state){

	return {

		restrict : 'AE',
		template : require('../views/line_info.html'),
		replace:true,
		scope:{
			'model' : '=',
			'util' : '=',
			'open' : '=',
		},
		link : function(scope, elements, attrs){

			scope.result = {};
			scope.dateshow = {};
			scope.imageshow = {};
			scope.info = {};
			scope.a='';
			scope.arr=[];
			scope.codearr=[];
			scope.arr1=[];
			scope.codearr1=[];
			scope.emarr=[];
			scope.b={};
			scope.b.employee='1';
			scope.codearr3=[];
			scope.dparr=[];
			scope.dparr2=[];
			scope.dparr3=[];

			var product_id = scope.util.product_id;


			if(product_id != ''){

				$resource('/api/ac/lc/productService/findProductDetail', {}, {}).
				save({'product_id' : scope.util.product_id}, function(res){
	                 console.log('下面是修改返回值');
	                 console.log(res);					
					 scope.a=res.data;
										
					
	                if(res.errcode === 0){
						scope.result['codelist']=scope.codearr;
						scope.result['endcodelist']=scope.codearr1;
	                	scope.util.init({
							'model' : scope.model,
							'result' : scope.result,
							'dateshow' : scope.dateshow,
							'imageshow' : scope.imageshow,
							'info' : res.data,
						});
						var arrtuijian = res.data.start_city_arr.split(",");
						var arrtuijian1 = res.data.end_city.split(",");
																
						if(arrtuijian.length!=0){
							scope.arr = [];
						}
						if(arrtuijian1.length!=0){
							scope.arr1 = [];
						}
						scope.codearr=res.data.codelist;
						scope.codearr1=res.data.endcodelist;

						var lcarr2 = res.data.supplier_name_new.split(",");
						if(lcarr2.length!=0){
							scope.emarr = [];
						}
						for (var y=0;y<lcarr2.length;y++){
							scope.emarr.push({name : lcarr2[y]});
							
						};
						var lcarr4 = res.data.supplier_name.split(",");
						if(lcarr4.length!=0){
							scope.codearr3 = [];
						}
						for (var y=0;y<lcarr4.length;y++){
							scope.codearr3.push(lcarr4[y]);
							
						};

						var lcarr5 = res.data.docking_port_name.split(",");
						if(lcarr5.length!=0){
							scope.dparr = [];
						}
						for (var y=0;y<lcarr5.length;y++){
							scope.dparr.push({name : lcarr5[y]});
							
						};

						var lcarr6 = res.data.docking_port_arr.split(",");
						if(lcarr6.length!=0){
							scope.dparr2 = [];
						}
						for (var y=0;y<lcarr6.length;y++){
							scope.dparr2.push(lcarr6[y]);
							
						};
						
							
							
								//沈阳市
						
						for (var y=0;y<arrtuijian.length;y++){
							scope.arr.push({one : arrtuijian[y]});
						};
						for (var y=0;y<arrtuijian1.length;y++){
							scope.arr1.push({one : arrtuijian1[y]});
						};
	                }else{
	                    alert(res.errmsg);
	                }
	            });

			}else{

				scope.util.init({
					'model' : scope.model,
					'result' : scope.result,
					'dateshow' : scope.dateshow,
					'imageshow' : scope.imageshow,
					'info' : scope.info
				});

			}
			scope.page = {};
			scope.placeobj={};
			scope.page1 = {};
			scope.placeobj1={};
			
			getarea('a', '');
			getarea1('a', '');
			scope.changeprovince = function(code,item){
				scope.placeobj.city = '';
				scope.placeobj.district = '';
				scope.placeobj.business_district = '';
				scope.city='';
				scope.province='';

				scope.page.citylist = [];
				scope.page.districtlist = [];
				scope.page.business_districtlist = [];
				scope.province_code=code;
				scope.province=item;
				getarea('province', code);
			};
			//市
			scope.changecity = function(code,item){
				// scope.placeobj.district = '';
				// scope.placeobj.business_district = '';

				scope.page.districtlist = [];
				scope.page.business_districtlist = [];
				scope.city=item;
				scope.city_code=code;
			};

			function getarea(what, code){
				$resource('/api/us/sc/city/arealist', {}, {})
				.get({'code' : code}, function(res){
					// console.log(res);
					if(res.errcode !== 0){
						alert(res.errmsg);
						return;
					}

					if(what === 'province'){
						scope.page.citylist = res.data;
					}else if(what === 'city'){
						scope.page.districtlist = res.data;
					}else if(what === 'a'){
						scope.page.provincelist = res.data;
					}
				});
			}

			//目的地城市
			scope.changeprovince1 = function(code,item){
				scope.placeobj.city1 = '';
				scope.placeobj.province1 = '';
				scope.return_city='';
				scope.return_province='';
				scope.return_city_code='';
				scope.return_province_code='';
				
				scope.page1.citylist = [];
				scope.page1.provincelist = [];
				scope.return_guojia=item;
				scope.return_guojia_code=code;
				getarea1('guojia', code);
			};
			//市
			scope.changeprovince2 = function(code,item){
				scope.placeobj.city1 = '';
				scope.return_city='';
				scope.return_city_code='';

				scope.page1.citylist = [];
				scope.return_province=item;
				scope.return_province_code=code;
				
				getarea1('province', code);
			};
			scope.changecity1 = function(code,item){
				scope.return_city=item;
				scope.return_city_code=code;
				// getarea1('province', code);
			};


			function getarea1(what, code){
				$resource('/api/as/lc/product/findTravelLocationList', {}, {})
				.get({'pid' : code}, function(res){					 
					if(res.errcode !== 0){
						alert(res.errmsg);
						return;
					}

					if(what === 'province'){
						scope.page1.citylist = res.data;
					}else if(what === 'city'){
						scope.page1.districtlist = res.data;
					}else if(what === 'guojia'){
						scope.page1.provincelist = res.data;
					}else {
						scope.page1.guojialist = res.data;
					}
				});
			}

			function getemployee(){	
				$resource('/api/as/lc/employee/findEmList', {}, {})
				.save({}, function(res){
					console.log(res);
					scope.lclist = res.data;					 
					if(res.errcode !== 0){
						alert(res.errmsg);
						return;
					}
					
				});
			}
			getemployee();

			scope.addemployee = function(){
				scope.codearr3=[];
							
				for(var i=0;i<scope.emarr.length;i++){
					if(scope.b.employee==scope.emarr[i].name){
						alert('不可重复添加姓名');
						return;
					}
					
				}
				
				if(scope.b.employee!='1'){
					scope.emarr.push({ name: scope.b.employee});					
				}
				console.log(scope.emarr);
				for(var z=0;z<scope.lclist.length;z++){
					for(var p=0;p<scope.emarr.length;p++){						
						if(scope.emarr[p].name==scope.lclist[z].name){
							scope.codearr3.push(scope.lclist[z].employee_code);
						}
					}
				}
				

			};

			scope.removeEmployee = function(index){
				scope.emarr.splice(index,1);
				scope.codearr3.splice(index,1);
				
			}

			scope.add = function(){
				for(var i=0;i<scope.arr.length;i++){
					if(scope.city==scope.arr[i].one){
						alert('不可重复添加城市');
						return;
					}
					if(scope.province==scope.arr[i].one){
						alert('不可重复添加城市');
						return;
					}
				}
				if(scope.city){
					scope.arr.push({one : scope.city});
				}else if(scope.province == '上海市' || scope.province == '澳门特别行政区' || scope.province == '香港特别行政区' || scope.province == '台湾省' || scope.province == '天津市' || scope.province == '重庆市' ){
					// console.log(scope.province);
					scope.arr.push({one : scope.province});
				}
				else{
					alert('请选择城市');
				}
				scope.codearr.push({province_code : scope.province_code,city_code :scope.city_code, type : '0'});
			}
			scope.removeSelectDate = function(index){
				 scope.arr.splice(index,1);
				 scope.codearr.splice(index,1);
			}

			scope.add1 = function(){
				for(var i=0;i<scope.arr1.length;i++){
					if(scope.return_city==scope.arr1[i].one){
						alert('不可重复添加城市');
						return;
					}
					if(scope.return_province==scope.arr1[i].one){
						alert('不可重复添加城市');
						return;
					}
					if(scope.return_guojia==scope.arr1[i].one){
						alert('不可重复添加城市');
						return;
					}
				}
				if(scope.return_city){
					scope.arr1.push({one : scope.return_city});					
				}else if(scope.return_province){
					scope.arr1.push({one : scope.return_province});					
				}else if(scope.return_guojia){
					scope.arr1.push({one : scope.return_guojia});										
				}else{
					alert('请选择城市');					
				}
				// if(scope.city){
					// scope.arr1.push({one : scope.city});
				// }else if(scope.province == '上海市' || scope.province == '澳门特别行政区' || scope.province == '香港特别行政区' || scope.province == '台湾省' || scope.province == '天津市' || scope.province == '重庆市' ){
				// 	console.log(scope.province);
				// 	scope.arr.push({one : scope.province});
				// }
				// else{
				// 	alert('请选择城市');
				// }
				scope.codearr1.push({country_code : scope.return_guojia_code,province_code : scope.return_province_code,city_code :scope.return_city_code, type : '1'});
			}
			scope.removeSelectDate1 = function(index){
				 scope.arr1.splice(index,1);
				 scope.codearr1.splice(index,1);
			}


			scope.result['product_id'] = scope.util.product_id;
			// scope.result['product_state_name'] = scope.util.product_state_name;
			scope.dp = [
				{name:'商客',value:'S10'},
				{name:'居游',value:'J10'},
			]
			
			scope.adddp = function(){
				console.log(scope.b.docking_port);
				scope.dparr2=[];
							
				for(var i=0;i<scope.dparr.length;i++){
					if(scope.b.docking_port==scope.dparr[i].name){
						alert('不能重复添加');
						return;
					}
					
				}
				
				if(scope.b.docking_port){
					scope.dparr.push({ name: scope.b.docking_port});					
				}
				for(var i=0;i<scope.dparr.length;i++){
					if(scope.dparr[i].name=='商客'){
						scope.dparr2.push('S10');
					}
					if(scope.dparr[i].name=='居游'){
						scope.dparr2.push('J10');
					}
					
				}
				

			};
			scope.removedp = function(index){
				scope.dparr.splice(index,1);
				scope.dparr2.splice(index,1);
								
			}
			scope.gogogo = function(){
				scope.dparr3=[];
				for(var i=0;i<scope.dparr.length;i++){
					scope.dparr3.push(scope.dparr[i].name);
				}

				
				scope.result.start_city_arr='';
				scope.result.end_city='';
				if(scope.lineinfo.$valid){
					//alert('通过验证可以提交表单');
				}else{
					//alert('表单没有通过验证');undefined
					return;
				}
				for(var x=0;x<scope.arr.length;x++){
					if(x==(scope.arr.length-1)){
						scope.result.start_city_arr+=scope.arr[x].one;	
					}else{
						scope.result.start_city_arr+=scope.arr[x].one+',';	
					}
				}
				for(var x=0;x<scope.arr1.length;x++){
					if(x==(scope.arr1.length-1)){
						scope.result.end_city+=scope.arr1[x].one;	
					}else{
						scope.result.end_city+=scope.arr1[x].one+',';	
					}
				}
				// if(!scope.return_city_code){
				// 	scope.return_city_code='';
				// }
				// if(!scope.return_province_code){
				// 	scope.return_province_code='';
				// }

				// scope.result['endcodelist']=[{country_code : scope.return_guojia_code,province_code : scope.return_province_code,city_code :scope.return_city_code, type : '1'}];	
				// if(scope.return_city_code!=''){				
				// 	scope.result['end_city']=scope.return_city;
				// }else if(scope.return_province_code!=''){				
				// 	scope.result['end_city']=scope.return_province;
				// }else if(scope.return_guojia_code!=''){
				// 	scope.result['end_city']=scope.return_guojia;	
				// }else{
				// 	alert('目的地城市不可为空');
				// 	return;
				// }
				scope.codearr3=[];
				for(var z=0;z<scope.lclist.length;z++){
					for(var p=0;p<scope.emarr.length;p++){						
						if(scope.emarr[p].name==scope.lclist[z].name){
							scope.codearr3.push(scope.lclist[z].employee_code);
						}
					}
				}
				scope.result.supplier_name='';
				for(var x=0;x<scope.codearr3.length;x++){
					if(x==(scope.codearr3.length-1)){
						scope.result.supplier_name+=scope.codearr3[x];	
					}else{
						scope.result.supplier_name+=scope.codearr3[x]+',';	
					}
				}
				
				scope.result.supplier_name_new='';
				for(var x=0;x<scope.emarr.length;x++){
					if(x==(scope.emarr.length-1)){
						scope.result.supplier_name_new+=scope.emarr[x].name;	
					}else{
						scope.result.supplier_name_new+=scope.emarr[x].name+',';	
					}
				}

				scope.result.docking_port_arr='';
				for(var x=0;x<scope.dparr2.length;x++){
					if(x==(scope.dparr2.length-1)){
						scope.result.docking_port_arr+=scope.dparr2[x];	
					}else{
						scope.result.docking_port_arr+=scope.dparr2[x]+',';	
					}
				}

				scope.result.docking_port_name='';
				for(var x=0;x<scope.dparr.length;x++){
					if(x==(scope.dparr.length-1)){
						scope.result.docking_port_name+=scope.dparr[x].name;	
					}else{
						scope.result.docking_port_name+=scope.dparr[x].name+',';	
					}
				}
				
				if(scope.arr.length==0){
					alert('出发城市不可为空');
					return;
				}
				if(scope.util.product_state_name=='待上架'||scope.util.product_state_name=='已上架'||scope.util.product_state_name=='已下架'){
					alert('该产品不为草稿状态，只有价格库存中日库存量可更改，其余不可更改!');
					return;
				}
				if(scope.result.supplier_name==''){
					alert('请添加供应商产品负责人');
					return;
				}
				if(scope.result.docking_port_name==''){
					alert('请添加上架端');
					return;
				}
				scope.result['codelist']=scope.codearr;
				scope.result['endcodelist']=scope.codearr1;
				
				console.log(scope.result);
				console.log('上面是scope.result');
				
				// console.log(scope.result);
				$resource('/api/ac/lc/productService/create', {}, {}).save(scope.result, function(res){
                    if(res.errcode === 0){
                    	alert('保存成功')
                    	if(scope.util.product_id == ''){
                    		scope.util.product_id = res.data.product_id;
	                        $state.go('app.line_edit1', {'id' : res.data.product_id});
                    	}
                    }else{
                        alert(res.errmsg);
                    }
                });
			};
		}

	};

};

