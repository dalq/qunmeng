module.exports = function($resource, $state,$modal){


	function addDay(arr){
		arr.push({
			'title' : {
				'start' : '',
				'by' : '0',
				'end' : '',
			},
			'trip' : [],
			'triptype' : '0',
		});
	}

	

	//上移动一天,index=3  [1,2,3,4,5]=>[1,2,4,3,5]
	function arrup(arr, index){
		arr[index - 1] = arr.splice(index, 1, arr[index - 1])[0];
	}
	//下移动一天,index=3  [1,2,3,4,5]=>[1,2,3,5,4]
	function arrdown(arr, index){
		arr[index + 1] = arr.splice(index, 1, arr[index + 1])[0];
	}

	return {

		restrict : 'AE',
		template : require('../views/line_trip.html'),
		replace:true,
		scope:{
			'model' : '=',
			'util' : '=',
		},
		link : function(scope, elements, attrs){

			scope.oneAtATime = false;
			scope.triptype = '0';
			scope.a='';
			scope.imgarr=[];

			scope.showinfo = {
				'day' : [
					{'name' : '1', 'value' : 1},
	                {'name' : '2', 'value' : 2},
	                {'name' : '3', 'value' : 3},
	                {'name' : '4', 'value' : 4},
	                {'name' : '5', 'value' : 5},
	                {'name' : '6', 'value' : 6},
	                {'name' : '7', 'value' : 7},
	                {'name' : '8', 'value' : 8},
	                {'name' : '9', 'value' : 9},
	                {'name' : '10', 'value' : 10},
					{'name' : '11', 'value' : 11},
	                {'name' : '12', 'value' : 12},
	                {'name' : '13', 'value' : 13},
	                {'name' : '14', 'value' : 14},
	                {'name' : '15', 'value' : 15},
	                {'name' : '16', 'value' : 16},
	                {'name' : '17', 'value' : 17},
	                {'name' : '18', 'value' : 18},
	                {'name' : '19', 'value' : 19},
	                {'name' : '20', 'value' : 20},
	            ],
				'evening' : [
					{'name' : '0', 'value' : 0},
					{'name' : '1', 'value' : 1},
	                {'name' : '2', 'value' : 2},
	                {'name' : '3', 'value' : 3},
	                {'name' : '4', 'value' : 4},
	                {'name' : '5', 'value' : 5},
	                {'name' : '6', 'value' : 6},
	                {'name' : '7', 'value' : 7},
	                {'name' : '8', 'value' : 8},
	                {'name' : '9', 'value' : 9},
	                {'name' : '10', 'value' : 10},
					{'name' : '11', 'value' : 11},
	                {'name' : '12', 'value' : 12},
	                {'name' : '13', 'value' : 13},
	                {'name' : '14', 'value' : 14},
	                {'name' : '15', 'value' : 15},
	                {'name' : '16', 'value' : 16},
	                {'name' : '17', 'value' : 17},
	                {'name' : '18', 'value' : 18},
	                {'name' : '19', 'value' : 19},
	                {'name' : '20', 'value' : 20},
	            ],
	            'flight' : [
	            	{'name' : '-2', 'value' : -2},
	            	{'name' : '-1', 'value' : -1},
	            	{'name' : '0', 'value' : 0},
	            	{'name' : '1', 'value' : 1},
	            	{'name' : '2', 'value' : 2},
	            ],
	            //往，交通
	            'to' : [
	            	{'name' : '交通方式', 'value' : '0'},
	                {'name' : '飞机', 'value' : '1'},
	                {'name' : '火车', 'value' : '2'},
	                {'name' : '大客', 'value' : '3'},
	                {'name' : '轮船', 'value' : '4'},
					{'name' : '自定义', 'value' : '5'},
	            ],
	            'triptype' : [
	            	{'name' : '交通', 'value' : '0'},
	                {'name' : '景点', 'value' : '1'},
	                {'name' : '住宿', 'value' : '2'},
	                {'name' : '用餐', 'value' : '3'},
	                {'name' : '购物', 'value' : '4'},
	                {'name' : '活动', 'value' : '5'},
	                {'name' : '推荐项目', 'value' : '6'},
	                {'name' : '温馨提示', 'value' : '7'},
	            ]
			};


			var types = [
				{
					'id' : 't0',
					'name' : '交通',
					'start' : '',
					'by' : '0',
					'end' : '',
					'time' : '0',
				},
				{
					'id' : 't1',
					'name' : '景点',
					'start' : '',
					'time' : '0'
				},
				{
					'id' : 't2',
					'name' : '住宿',
					'start' : '',
					'time' : '0'
				},
				{
					'id' : 't3',
					'name' : '用餐',
					'start' : '',
					'time' : '0'
				},
				{
					'id' : 't4',
					'name' : '购物',
					'start' : '',
					'time' : '0'
				},
				{
					'id' : 't5',
					'name' : '活动',
					'start' : '',
					'time' : '0'
				},
				{
					'id' : 't6',
					'name' : '推荐项目',
					'start' : '',
					'time' : '0'
				},
				{
					'id' : 't7',
					'name' : '温馨提示',
					'start' : '',
				},
			];

			scope.result = {
				'day' : 1,
				'night' : 1,
				'characteristic_words_arr' : '',
				'flight_reason' : 0,
				'days' : [
					{
						'title' : {
							'start' : '',
							'by' : '0',
							'end' : '',
						},
						'trip' : [],
						'triptype' : '0',
					},
				],
			};



			$resource('/api/as/lc/trip/findinfo', {}, {}).get({'product_id' : scope.util.product_id}, function(res){
                // console.log('行程数据');
                // console.log(res);
				scope.a=res.data;
                if(res.errcode === 0){
                    //alert(res.data.msg);
                    scope.result.day = res.data.day;
                    scope.result.night = res.data.night;
                    scope.result.characteristic_words_arr = res.data.characteristic_words_arr;
                    scope.result.flight_reason = res.data.flight_reason;
                    scope.result.days = angular.fromJson(res.data.trip_info_json);
					for(var x=0;x<scope.result.days.length;x++){
						scope.imgarr.push([[]]);
						for(var y=0;y<scope.result.days[x].trip.length;y++){
							scope.imgarr[x][y]=scope.result.days[x].trip[y].imgstr;
						}
					}
                }else if(res.errcode === 10003) {	//没找到

                }else{
                    alert(res.errmsg);
                }
            });

			scope.addtrip = function(day){
				day.trip.push(angular.copy(types[parseInt(day.triptype)]));
			}

			//天数修改方法
			scope.change = function(what){
				var l = scope.result.days.length;
				if(what > l){
					var step = what - l;
					for(var i = 0; i < step; i++){
						addDay(scope.result.days);
					}
				}
				else if(what < l){
					if(!confirm("确定要删除之前的数据吗？")){
						scope.result.day = l;
	                    return ;
	                }
	                scope.result.days.splice(0,scope.result.days.length);
	                for(var i = 0; i < what; i++){
	                	addDay(scope.result.days);
						//scope.result.days.push({});
					}
				}
				// console.log(scope.result);
			};

			//上移动一天,index=3  0,1,2,3,4=>0,1,3,2,4
			scope.upday = function(index){
				arrup(scope.result.days, index);
			}

			//下移动一天
			scope.downday = function(index){
				arrdown(scope.result.days, index);
			}

			//删除一天
			scope.removeday = function(index){
				if(!confirm("确定要删除这一天的数据吗？")){
                    return ;
                }
				scope.result.days.splice(index, 1);
				scope.result.day = scope.result.day - 1;
			}

			//上移
			scope.up = function(index,day){
				if(index==0){
					alert('已经是第一项无法上移');
					return;
				}else{
					arrup(day.trip, index);
				}
				
			}
			//下移
			scope.down = function(index,day){
				if(index==(day.trip.length-1)){
					alert('已经是最后一项无法下移');
					return;
				}else{
					arrdown(day.trip, index);
				}
				
			}
			//删除一项
			scope.remove = function(index,day){
				if(!confirm("确定要删除这一项的数据吗？")){
                    return ;
                }
				day.trip.splice(index, 1);
			}
			scope.gogogo = function(){
				if(scope.util.product_state_name=='待上架'||scope.util.product_state_name=='已上架'||scope.util.product_state_name=='已下架'){
					alert('该产品不为草稿状态，只有价格库存中日库存量可更改，其余不可更改!');
					return;
				}
				scope.result['trip_info_json'] = angular.toJson(scope.result.days);
				scope.result['product_id'] = scope.util.product_id;
				scope.result['trip_type'] = 'A';
				// console.log(scope.result);
				// console.log(scope.util.product_id);
				$resource('/api/ac/lc/productTripService/create', {}, {}).save(scope.result, function(res){
                    // console.log(scope.util.product_id);
                    if(res.errcode === 0){
						$state.go('app.line_aaaaa', {'id' : scope.util.product_id});
                        alert(res.data.msg);
                    }else{
                        alert(res.errmsg);
                    }
                });

			}
			 scope.remove1 = function(ppindex,pindex,index){

				scope.imgarr[ppindex][pindex].splice(index,1);
				scope.result.days[ppindex].trip[pindex].imgstr = scope.imgarr[ppindex][pindex];

            };


			scope.image = function(pindex,index){
				var para = $state.get('app.imageupload');
				var modalInstance = $modal.open(para);
				modalInstance.opened.then(function() {// 模态窗口打开之后执行的函数  
					// console.log('modal is opened');  
				});  
				modalInstance.result.then(function(date) {   
					scope.result.days[pindex].trip[index].imgstr = date;
					if(pindex>(scope.imgarr.length-1)){
						scope.imgarr[pindex]=[[]];
					}
					if(scope.imgarr[pindex][index]){
						for(var i=0;i<date.length;i++){
							scope.imgarr[pindex][index].push(date[i]);					
						}
					}else{
						scope.imgarr[pindex][index]=date;
					}
					
					// console.log(scope.imgarr); 
					$scope.$apply();
					// form.result['templete_lock_data_json'] = JSON.stringify(result.lock);
					// form.result['templete_check_data_json'] = result.unlock;

				}, function(reason) {  
					// console.log(reason);// 点击空白区域，总会输出backdrop  
					// click，点击取消，则会暑促cancel  
					$log.info('Modal dismissed at: ' + new Date());  
				}); 
			};

			

		}

	};

};

