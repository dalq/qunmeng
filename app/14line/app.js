var App = angular.module('line', []);

App.config(require('./router'));
App.factory('lineservice', require('./service'));


App.controller('linesimplelist',require('./controllers/simplelist'));
App.controller('lineinformation',require('./controllers/information'));
//我的修改
App.controller('setgroupstate',require('./controllers/setgroupstate'));
App.controller('lineedit',require('./controllers/edit'));
App.controller('linecontain',require('./controllers/contain'));
App.controller('linecaution',require('./controllers/caution'));
App.controller('lineitinerary',require('./controllers/itinerary'));
App.controller('lineorderinfo',require('./controllers/orderinfo'));

App.controller('lineconfirminfo',require('./controllers/confirminfo'));
App.controller('lineconfirminfotwo',require('./controllers/confirminfotwo'));
App.controller('linelineallinformationtwo',require('./controllers/lineallinformationtwo'));
App.controller('linechecklistreturn',require('./controllers/checklistreturn'));
App.controller('linechecklistinfo',require('./controllers/checklistinfo'));
App.controller('linecopy',require('./controllers/copy'));
App.controller('linemessageinfo',require('./controllers/messageinfo'));
App.controller('linemessagelist',require('./controllers/messagelist'));

App.controller('linereject',require('./controllers/reject'));

App.controller('linecheckinfo',require('./controllers/checkinfo'));
App.controller('linecreate',require('./controllers/create1'));
App.controller('addpricecalendar',require('./controllers/addpricecalendar'));
App.controller('updatepricecalendar',require('./controllers/updatepricecalendar'));
App.controller('touristMessage',require('./controllers/touristMessage'));
App.controller('linechecklist1',require('./controllers/checklist1'));
App.controller('linereturnmoreylist1',require('./controllers/returnmoreylist1'));
App.controller('linemessagelist1',require('./controllers/messagelist1'));
App.controller('lineconfirm1',require('./controllers/confirm1'));
App.controller('linelist1',require('./controllers/list1'));
App.controller('imageupload',require('./controllers/imageupload'));
App.controller('lineshowimg',require('./controllers/showimg'));
App.controller('linedistributorlist',require('./controllers/distributorlist'));
//lc分销商产品列表
App.controller('linedistributor_list',require('./controllers/distributor_list'));
App.controller('linetoorder',require('./controllers/toorder'));
App.controller('linedistributororderlist',require('./controllers/distributororderlist'));
App.controller('linedistributororderpay',require('./controllers/distributororderpay'));
App.controller('lineorderlist1',require('./controllers/orderlist1'));
App.controller('lineprestore',require('./controllers/prestore'));
App.controller('lineprestoreedit',require('./controllers/prestoreedit'));
App.controller('lineprestoresteup',require('./controllers/prestoresteup'));
App.controller('lineprestorereturn',require('./controllers/prestorereturn'));
App.controller('linebonus',require('./controllers/linebonus'));
//lc退款列表
App.controller('returnmoneylist',require('./controllers/returnmoneylist'));
App.controller('linegroupnotice',require('./controllers/linegroupnotice'));
//lc出团通知单
App.controller('gotournoticeinfo',require('./controllers/gotournoticeinfo'));
//lc产品经理人
App.controller('productmanager',require('./controllers/productmanager'));
//lc产品经理人添加
App.controller('productmanageradd',require('./controllers/productmanageradd'));
//lc产品经理人添加
App.controller('productmanagermodify',require('./controllers/productmanagermodify'));
App.controller('fastselectcalendar',require('./controllers/fastselectcalendar'));
App.directive('lineinfo',require('./directives/lineinfo'));
App.directive('linecontain',require('./directives/linecontain'));
App.directive('linecaution',require('./directives/linecaution'));
App.directive('lineexplain',require('./directives/lineexplain'));
App.directive('linegoreturntraffic',require('./directives/linegoreturntraffic'));
App.directive('linetrip',require('./directives/linetrip'));
App.directive('linepicture',require('./directives/linepicture'));
App.directive('linenocontain',require('./directives/linenocontain'));
App.directive('lineactivity',require('./directives/lineactivity'));
App.directive('linepricecalendar',require('./directives/pricecalendar'));
App.directive('lineallinformation',require('./directives/lineallinformation'));

// App.controller('skprofit',require('./29product/controllers/skprofit'));


module.exports = App;