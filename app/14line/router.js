 /**
 * 子模块路由
 * dlq
 */

var router = function($urlRouterProvider, $stateProvider){


 	$stateProvider


    //景区列表
    .state('app.linelist', {
        url: "/line/linelist1.html",
        views: {
            'main@' : {
                template : require('../14line/views/list1.html'),
                controller : 'linelist1',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
        }
    })

     //供应商产品列表
    .state('app.distributorlist', {
        url: "/line/linedistributorlist.html",
        views: {
            'main@' : {
                template : require('../14line/views/distributorlist.html'),
                controller : 'linedistributorlist',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
        }
    })

    //分销商产品列表
    .state('app.distributor_list', {
        url: "/line/distributor_list.html",
        views: {
            'main@' : {
                template : require('../14line/views/distributor_list.html'),
                controller : 'linedistributor_list',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
        }
    })

    //占位通知
    .state('app.line_create', {
        url: "/line/lineinformation.html",
        views: {
            'main@' : {
                template : require('../14line/views/occupy.html'),
                controller : 'lineinformation',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },

        }
    })
    //我的修改

    /*.state('app.line_setgroupstate', {
        url: "/line/linesetgroupstate.html/:id",
        views: {
            'main@' : {
                template : require('../14line/views/setgroupstate.html'),
                controller : 'setgroupstate',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },

        }
    })*/
    //确认单
    .state('app.line_confirm', {
        url: "/line/lineconfirm1.html",
        views: {
            'main@' : {
                template : require('../14line/views/confirm1.html'),
                controller : 'lineconfirm1',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
        }
    })
    //发送短信 prestore
     .state('app.line_message', {
        url: "/line/linemessagelist1.html",
        views: {
            'main@' : {
                template : require('../14line/views/messagelist1.html'),
                controller : 'linemessagelist1',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
        }
    })
     //预存管理 prestore
     .state('app.line_prestore', {
        url: "/line/prestore.html",
        views: {
            'main@' : {
                template : require('../14line/views/prestore.html'),
                controller : 'lineprestore',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
        }
    })
     //退款列表
     .state('app.line_returnmorey', {
        url: "/line/linereturnmoreylist1.html",
        views: {
            'main@' : {
                template : require('../14line/views/returnmoreylist1.html'),
                controller : 'linereturnmoreylist1',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
        }
    })
    //lc退款列表
         .state('app.returnmoney', {
        url: "/line/returnmoneylist.html",
        views: {
            'main@' : {
                template : require('../14line/views/returnmoneylist.html'),
                controller : 'returnmoneylist',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
        }
    })

        //lc产品经理人
         .state('app.productmanager', {
        url: "/line/productmanager.html",
        views: {
            'main@' : {
                template : require('../14line/views/productmanager.html'),
                controller : 'productmanager',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
        }
    })

 //分销商订单列表
    .state('app.line_distributororderlist', {
        url: "/line/linedistributorcorderlist.html",
        views: {
            'main@' : {
                template : require('../14line/views/distributor_orderlist.html'),
                controller : 'linedistributororderlist',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
        }
    })
     //订单列表
    .state('app.line_orderlist', {
        url: "/line/linecorderlist.html",
        views: {
            'main@' : {
                template : require('../14line/views/orderlist1.html'),
                controller : 'lineorderlist1',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
        }
    })

    //平台审核上架
    .state('app.line_checklist', {
        url: "/line/linechecklist1.html",
        views: {
            'main@' : {
                template : require('../14line/views/checklist1.html'),
                controller : 'linechecklist1',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
        }
    })

    //费用包含
    .state('app.line_contain', {
        url: "/sys/linecontain/:id",
        views: {
            'main@' : {
                template : require('../996tpl/views/form.html'),
                controller : 'linecontain',
            }
        },
        resolve:{
            formconfig : function(formservice){
                return formservice.formconfig();
            },
            containmodel : function(lineservice){
                return lineservice.containmodel;
            }
        }
    })

    //购买须知
    .state('app.line_caution', {
        url: "/sys/linecaution/:id",
        views: {
            'main@' : {
                template : require('../996tpl/views/form.html'),
                controller : 'linecaution',
            }
        },
        resolve:{
            formconfig : function(formservice){
                return formservice.formconfig();
            },
            cautionmodel : function(lineservice){
                return lineservice.cautionmodel;
            }
        }
    })

    //行程管理
    .state('app.line_itinerary', {
        url: "/sys/lineitinerary/:id",
        views: {
            'main@' : {
                template : require('../996tpl/views/form.html'),
                controller : 'lineitinerary',
            }
        },
        resolve:{
            formconfig : function(formservice){
                return formservice.formconfig();
            },
            itinerarymodel : function(lineservice){
                return lineservice.itinerarymodel;
            }
        }
    })

    //景区详情
    .state('app.line_info', {
        url: "/sys/lineinfo/:id",
        views: {
            'main@' : {
                template : require('../996tpl/views/form.html'),
                controller : 'lineinfo',
            }
        },
        resolve:{
            formconfig : function(formservice){
                return formservice.formconfig();
            },
            viewmodel : function(lineservice){
                return lineservice.viewmodel;
            }
        }
    })



    //景区编辑
    .state('app.line_edit', {
        url: "/sys/lineedit/:id",
        views: {
            'main@' : {
                template : require('../996tpl/views/form.html'),
                controller : 'lineedit',
            }
        },
        resolve:{
            formconfig : function(formservice){
                return formservice.formconfig();
            },
            viewmodel : function(lineservice){
                return lineservice.viewmodel;
            }
        }
    })


    .state('app.line_editto', {
        url: "/sys/lineedit/:id/:aaa",
        views: {
            'main@' : {
                template : require('../996tpl/views/form.html'),
                controller : 'lineedit',
            }
        },
        resolve:{
            formconfig : function(formservice){
                return formservice.formconfig();
            },
            viewmodel : function(lineservice){
                return lineservice.viewmodel;
            }
        }
    })



    //平台上架审核
    .state('app.line_create2', {
        url: "/line/checkinfo.html",
        views: {
            'main@' : {
                template : require('./views/checkinfo.html'),
                controller : 'linecheckinfo',
            }
        },
        resolve:{
            // formconfig : function(formservice){
            //     return formservice.formconfig();
            // },
            lineinfomodel : function(lineservice){
                return lineservice.viewmodel;
            },
            linecontainmodel : function(lineservice){
                return lineservice.containmodel;
            },
            linecautionmodel : function(lineservice){
                return lineservice.cautionmodel;
            },
             lineexplainmodel : function(lineservice){
                return lineservice.explainmodel;
            },
            linetripmodel : function(lineservice){
                return lineservice.tripmodel;
            },
             linepicturemodel : function(lineservice){
                return lineservice.picturemodel;
            },
            linenocontainmodel : function(lineservice){
                return lineservice.nocontainmodel;
            },
            lineallinformationmodel : function(lineservice){
                return lineservice.allinformationmodel;
            },
        }
    })



    //创建线路
    .state('app.line_create1', {
        url: "/line/create.html",
        views: {
            'main@' : {
                template : require('./views/create.html'),
                controller : 'linecreate',
            }
        },
        resolve:{
            // formconfig : function(formservice){
            //     return formservice.formconfig();
            // },
            lineinfomodel : function(lineservice){
                return lineservice.viewmodel;
            },
            linecontainmodel : function(lineservice){
                return lineservice.containmodel;
            },
            linecautionmodel : function(lineservice){
                return lineservice.cautionmodel;
            },
             lineexplainmodel : function(lineservice){
                return lineservice.explainmodel;
            },
            linetripmodel : function(lineservice){
                return lineservice.tripmodel;
            },
             linepicturemodel : function(lineservice){
                return lineservice.picturemodel;
            },
            linenocontainmodel : function(lineservice){
                return lineservice.nocontainmodel;
            },
            lineactivitymodel : function(lineservice){
                return lineservice.activitymodel;
            },
            lineallinformationmodel : function(lineservice){
                return lineservice.allinformationmodel;
            },
        }
    })

     .state('app.imageupload1', {
        url: "/line/imageupload.html",
        template : require('./views/imageupload.html'),
        'size' : 'lg',
        controller : 'imageupload',
        // resolve : {  
            
        // }  
    })



    //创建线路
    .state('app.line_edit1', {
        url: "/line/edit/:id/:product_state_name",
        views: {
            'main@' : {
                template : require('./views/create.html'),
                controller : 'linecreate',
            }
        },
        resolve:{
            // formconfig : function(formservice){
            //     return formservice.formconfig();
            // },
            lineinfomodel : function(lineservice){
                return lineservice.viewmodel;
            },
            linecontainmodel : function(lineservice){
                return lineservice.containmodel;
            },
            linecautionmodel : function(lineservice){
                return lineservice.cautionmodel;
            },
             lineexplainmodel : function(lineservice){
                return lineservice.explainmodel;
            },
            linetripmodel : function(lineservice){
                return lineservice.tripmodel;
            },
             linepicturemodel : function(lineservice){
                return lineservice.picturemodel;
            },
            linenocontainmodel : function(lineservice){
                return lineservice.nocontainmodel;
            },
             lineactivitymodel : function(lineservice){
                return lineservice.activitymodel;
            },
            lineallinformationmodel : function(lineservice){
                return lineservice.allinformationmodel;
            },
            
        }
    })
     //出团通知列表
    .state('app.group_notice', {
        url: "/line/linegroupnotice.html",
        views: {
            'main@' : {
                template : require('../14line/views/group_notice.html'),
                controller : 'linegroupnotice',
            }
        },
        resolve:{
            tableconfig : function(tableservice){
                return tableservice.tableconfig();
            },
        }
    })
    //创建线路2
    .state('app.line_aaaaa', {
        url: "/line/edit/:id/:product_state_name",
        views: {
            'main@' : {
                template : require('./views/create.html'),
                controller : 'linecreate',
            }
        },
        resolve:{
            // formconfig : function(formservice){
            //     return formservice.formconfig();
            // },
            lineinfomodel : function(lineservice){
                return lineservice.viewmodel;
            },
            linecontainmodel : function(lineservice){
                return lineservice.containmodel;
            },
            linecautionmodel : function(lineservice){
                return lineservice.cautionmodel;
            },
             lineexplainmodel : function(lineservice){
                return lineservice.explainmodel;
            },
            linetripmodel : function(lineservice){
                return lineservice.tripmodel;
            },
             linepicturemodel : function(lineservice){
                return lineservice.picturemodel;
            },
            linenocontainmodel : function(lineservice){
                return lineservice.nocontainmodel;
            },
             lineactivitymodel : function(lineservice){
                return lineservice.activitymodel;
            },
            lineallinformationmodel : function(lineservice){
                return lineservice.allinformationmodel;
            },
           
        }
    });

};

module.exports = router;