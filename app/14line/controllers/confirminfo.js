/**
 * 模态框
 */
module.exports = function($scope, $state, $stateParams, $modalInstance, items, $resource,$modal){
    if(items.a=='a'){
        $scope.wjq=1;
    }else{
        $scope.wjq=0;
    }
    
    $resource('/api/ac/lc/lineOrderListService/findSupplierInfo', {}, {}).
			get({order_code : items.order_code}, function(res){
				console.log('详情返回值');
                console.log(res);
                if(res.errcode === 0 || res.errcode === 10003){
					$scope.obj=res.data;
                    $scope.str = '';
                    $scope.smallstr = '';
                    $scope.sleepstr = '';
                    $scope.eatstr = '';
                    $scope.orderstr = '';
                    $scope.cautionstr = '';
                    $scope.addstr = '';
                    $scope.importantstr = '';
                    $scope.cautionsleepstr = '';
                    $scope.speelstr = '';
                    $scope.paystr = '';
                    $scope.peoplestr = '';
                    $scope.days = angular.fromJson(res.data.trip_info_json);
                    $scope.coursestr = '';
                    $scope.coursestrinfo = '';
                    $scope.biaotiarr= [];
                    $scope.secondarr = [];
                    $scope.shuzu = [];
                    $scope.t0 = '';
                    $scope.t1 = '';
                    $scope.t2 = '';
                    $scope.t3 = '';
                    $scope.t4 = '';
                    $scope.t5 = '';
                    $scope.t6 = '';
                    for(var x=0; x<$scope.days.length;x++){
                        $scope.secondarr = [];
                        $scope.coursestrinfo = '';
                        $scope.t0 = '';
                        $scope.t1 = '';
                        $scope.t2 = '';
                        $scope.t3 = '';
                        $scope.t4 = '';
                        $scope.t5 = '';
                        $scope.t6 = '';
                    //联系人信息
                        if($scope.obj.from_app_id=='juyou_yc'){
                            $scope.obj.from_app_id='分销商端'
                        }
                        if($scope.obj.from_app_id=='juyouWX'){
                            $scope.obj.from_app_id='居游微信'
                        }
                        if($scope.obj.from_app_id=='shangke'){
                            $scope.obj.from_app_id='商客'
                        }
                        if($scope.obj.from_app_id=='juyouAPP'){
                            $scope.obj.from_app_id='居游APP'
                        }
                           
                        if($scope.days[x].title.by=='0'){
                            $scope.coursestrinfo+='第'+(x+1)+'天:'+$scope.days[x].title.start+$scope.days[x].title.end;
                           for(var y=0; y<$scope.days[x].trip.length;y++){
                                if(!$scope.days[x].trip[y].info){
                                   $scope.days[x].trip[y].info='';
                               }
                                if($scope.days[x].trip[y].id=='t0'){
                                    if($scope.days[x].trip[y].by=='1'){
                                        $scope.t0+='交通：从'+$scope.days[x].trip[y].start+'坐飞机到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                    }else if($scope.days[x].trip[y].by=='2'){
                                        $scope.t0+='交通：从'+$scope.days[x].trip[y].start+'坐火车到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                    }else if($scope.days[x].trip[y].by=='3'){
                                        $scope.t0='交通：从'+$scope.days[x].trip[y].start+'坐大客到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                    }else if($scope.days[x].trip[y].by=='4'){
                                        $scope.t0+='交通：从'+$scope.days[x].trip[y].start+'坐轮船到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                    }else if($scope.days[x].trip[y].by==''){
                                        $scope.t0+='交通：从'+$scope.days[x].trip[y].start+'到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                    }
                                    $scope.secondarr.push($scope.t0);
                                }else if($scope.days[x].trip[y].id=='t1'){
                                        $scope.t1+='景点：'+$scope.days[x].trip[y].start+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        $scope.secondarr.push($scope.t1);
                                }else if($scope.days[x].trip[y].id=='t2'){
                                       $scope.t2+='住宿：在'+$scope.days[x].trip[y].start+'住宿。'+$scope.days[x].trip[y].info;
                                       $scope.secondarr.push($scope.t2);
                                }else if($scope.days[x].trip[y].id=='t3'){
                                       $scope.t3+='用餐：在'+$scope.days[x].trip[y].start+'用餐，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                       $scope.secondarr.push($scope.t3);
                                }else if($scope.days[x].trip[y].id=='t4'){
                                       $scope.t4+='购物：在'+$scope.days[x].trip[y].start+'购物，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                       $scope.secondarr.push($scope.t4);
                                }else if($scope.days[x].trip[y].id=='t5'){
                                     $scope.coursestr='第'+(x+1)+'天:'+$scope.days.start+$scope.days.end;
                                       $scope.t5+='活动：在'+$scope.days[x].trip[y].start+'活动，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                       $scope.secondarr.push($scope.t5);
                                }else if($scope.days[x].trip[y].id=='t6'){
                                     $scope.coursestr='第'+(x+1)+'天:'+$scope.days.start+$scope.days.end;
                                       $scope.t6+='推荐：在'+$scope.days[x].trip[y].start+'比较好，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                       $scope.secondarr.push($scope.t6);
                                }
                            }
                            
                        }else if($scope.days[x].title.by=='1'){
                            $scope.coursestrinfo+='第'+(x+1)+'天:'+$scope.days[x].title.start+'坐飞机到'+$scope.days[x].title.end;
                            for(var y=0; y<$scope.days[x].trip.length;y++){
                                 if(!$scope.days[x].trip[y].info){
                                   $scope.days[x].trip[y].info='';
                               }
                                if($scope.days[x].trip[y].id=='t0'){
                                    if($scope.days[x].trip[y].by=='1'){
                                        $scope.t0+='交通：从'+$scope.days[x].trip[y].start+'坐飞机到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                    }else if($scope.days[x].trip[y].by=='2'){
                                        $scope.t0+='交通：从'+$scope.days[x].trip[y].start+'坐火车到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                    }else if($scope.days[x].trip[y].by=='3'){
                                        $scope.t0='交通：从'+$scope.days[x].trip[y].start+'坐大客到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                    }else if($scope.days[x].trip[y].by=='4'){
                                        $scope.t0+='交通：从'+$scope.days[x].trip[y].start+'坐轮船到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                    }else if($scope.days[x].trip[y].by==''){
                                        $scope.t0+='交通：从'+$scope.days[x].trip[y].start+'到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                    }
                                    $scope.secondarr.push($scope.t0);
                                }else if($scope.days[x].trip[y].id=='t1'){
                                        $scope.t1+='景点：'+$scope.days[x].trip[y].start+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        $scope.secondarr.push($scope.t1);
                                }else if($scope.days[x].trip[y].id=='t2'){
                                       $scope.t2+='住宿：在'+$scope.days[x].trip[y].start+'住宿。'+$scope.days[x].trip[y].info;
                                       $scope.secondarr.push($scope.t2);
                                }else if($scope.days[x].trip[y].id=='t3'){
                                       $scope.t3+='用餐：在'+$scope.days[x].trip[y].start+'用餐，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                       $scope.secondarr.push($scope.t3);
                                }else if($scope.days[x].trip[y].id=='t4'){
                                       $scope.t4+='购物：在'+$scope.days[x].trip[y].start+'购物，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                       $scope.secondarr.push($scope.t4);
                                }else if($scope.days[x].trip[y].id=='t5'){
                                     $scope.coursestr='第'+(x+1)+'天:'+$scope.days.start+$scope.days.end;
                                       $scope.t5+='活动：在'+$scope.days[x].trip[y].start+'活动，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                       $scope.secondarr.push($scope.t5);
                                }else if($scope.days[x].trip[y].id=='t6'){
                                     $scope.coursestr='第'+(x+1)+'天:'+$scope.days.start+$scope.days.end;
                                       $scope.t6+='推荐：在'+$scope.days[x].trip[y].start+'比较好，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                       $scope.secondarr.push($scope.t6);
                                }
                            }
                        }else if($scope.days[x].title.by=='2'){
                            $scope.coursestrinfo+='第'+(x+1)+'天:'+$scope.days[x].title.start+'坐火车到'+$scope.days[x].title.end;
                           for(var y=0; y<$scope.days[x].trip.length;y++){
                                if(!$scope.days[x].trip[y].info){
                                   $scope.days[x].trip[y].info='';
                               }
                                if($scope.days[x].trip[y].id=='t0'){
                                    if($scope.days[x].trip[y].by=='1'){
                                        $scope.t0+='交通：从'+$scope.days[x].trip[y].start+'坐飞机到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                    }else if($scope.days[x].trip[y].by=='2'){
                                        $scope.t0+='交通：从'+$scope.days[x].trip[y].start+'坐火车到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                    }else if($scope.days[x].trip[y].by=='3'){
                                        $scope.t0='交通：从'+$scope.days[x].trip[y].start+'坐大客到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                    }else if($scope.days[x].trip[y].by=='4'){
                                        $scope.t0+='交通：从'+$scope.days[x].trip[y].start+'坐轮船到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                    }else if($scope.days[x].trip[y].by==''){
                                        $scope.t0+='交通：从'+$scope.days[x].trip[y].start+'到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                    }
                                    $scope.secondarr.push($scope.t0);
                                }else if($scope.days[x].trip[y].id=='t1'){
                                        $scope.t1+='景点：'+$scope.days[x].trip[y].start+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        $scope.secondarr.push($scope.t1);
                                }else if($scope.days[x].trip[y].id=='t2'){
                                       $scope.t2+='住宿：在'+$scope.days[x].trip[y].start+'住宿。'+$scope.days[x].trip[y].info;
                                       $scope.secondarr.push($scope.t2);
                                }else if($scope.days[x].trip[y].id=='t3'){
                                       $scope.t3+='用餐：在'+$scope.days[x].trip[y].start+'用餐，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                       $scope.secondarr.push($scope.t3);
                                }else if($scope.days[x].trip[y].id=='t4'){
                                       $scope.t4+='购物：在'+$scope.days[x].trip[y].start+'购物，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                       $scope.secondarr.push($scope.t4);
                                }else if($scope.days[x].trip[y].id=='t5'){
                                     $scope.coursestr='第'+(x+1)+'天:'+$scope.days.start+$scope.days.end;
                                       $scope.t5+='活动：在'+$scope.days[x].trip[y].start+'活动，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                       $scope.secondarr.push($scope.t5);
                                }else if($scope.days[x].trip[y].id=='t6'){
                                     $scope.coursestr='第'+(x+1)+'天:'+$scope.days.start+$scope.days.end;
                                       $scope.t6+='推荐：在'+$scope.days[x].trip[y].start+'比较好，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                       $scope.secondarr.push($scope.t6);
                                }
                            }
                        }else if($scope.days[x].title.by=='3'){
                            $scope.coursestrinfo+='第'+(x+1)+'天:'+$scope.days[x].title.start+'做大客到'+$scope.days[x].title.end;
                             for(var y=0; y<$scope.days[x].trip.length;y++){
                                  if(!$scope.days[x].trip[y].info){
                                   $scope.days[x].trip[y].info='';
                               }
                                if($scope.days[x].trip[y].id=='t0'){
                                    if($scope.days[x].trip[y].by=='1'){
                                        $scope.t0+='交通：从'+$scope.days[x].trip[y].start+'坐飞机到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                    }else if($scope.days[x].trip[y].by=='2'){
                                        $scope.t0+='交通：从'+$scope.days[x].trip[y].start+'坐火车到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                    }else if($scope.days[x].trip[y].by=='3'){
                                        $scope.t0='交通：从'+$scope.days[x].trip[y].start+'坐大客到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                    }else if($scope.days[x].trip[y].by=='4'){
                                        $scope.t0+='交通：从'+$scope.days[x].trip[y].start+'坐轮船到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                    }else if($scope.days[x].trip[y].by==''){
                                        $scope.t0+='交通：从'+$scope.days[x].trip[y].start+'到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                    }
                                    $scope.secondarr.push($scope.t0);
                                }else if($scope.days[x].trip[y].id=='t1'){
                                        $scope.t1+='景点：'+$scope.days[x].trip[y].start+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        $scope.secondarr.push($scope.t1);
                                }else if($scope.days[x].trip[y].id=='t2'){
                                       $scope.t2+='住宿：在'+$scope.days[x].trip[y].start+'住宿。'+$scope.days[x].trip[y].info;
                                       $scope.secondarr.push($scope.t2);
                                }else if($scope.days[x].trip[y].id=='t3'){
                                       $scope.t3+='用餐：在'+$scope.days[x].trip[y].start+'用餐，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                       $scope.secondarr.push($scope.t3);
                                }else if($scope.days[x].trip[y].id=='t4'){
                                       $scope.t4+='购物：在'+$scope.days[x].trip[y].start+'购物，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                       $scope.secondarr.push($scope.t4);
                                }else if($scope.days[x].trip[y].id=='t5'){
                                     $scope.coursestr='第'+(x+1)+'天:'+$scope.days.start+$scope.days.end;
                                       $scope.t5+='活动：在'+$scope.days[x].trip[y].start+'活动，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                       $scope.secondarr.push($scope.t5);
                                }else if($scope.days[x].trip[y].id=='t6'){
                                     $scope.coursestr='第'+(x+1)+'天:'+$scope.days.start+$scope.days.end;
                                       $scope.t6+='推荐：在'+$scope.days[x].trip[y].start+'比较好，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                       $scope.secondarr.push($scope.t6);
                                }
                            }
                        }else if($scope.days[x].title.by=='4'){
                            $scope.coursestrinfo+='第'+(x+1)+'天:'+$scope.days[x].title.start+'做轮船到'+$scope.days[x].title.end;
                             for(var y=0; y<$scope.days[x].trip.length;y++){
                                  if(!$scope.days[x].trip[y].info){
                                   $scope.days[x].trip[y].info='';
                               }
                                if($scope.days[x].trip[y].id=='t0'){
                                    if($scope.days[x].trip[y].by=='1'){
                                        $scope.t0+='交通：从'+$scope.days[x].trip[y].start+'坐飞机到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                    }else if($scope.days[x].trip[y].by=='2'){
                                        $scope.t0+='交通：从'+$scope.days[x].trip[y].start+'坐火车到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                    }else if($scope.days[x].trip[y].by=='3'){
                                        $scope.t0='交通：从'+$scope.days[x].trip[y].start+'坐大客到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                    }else if($scope.days[x].trip[y].by=='4'){
                                        $scope.t0+='交通：从'+$scope.days[x].trip[y].start+'坐轮船到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                    }else if($scope.days[x].trip[y].by==''){
                                        $scope.t0+='交通：从'+$scope.days[x].trip[y].start+'到'+$scope.days[x].trip[y].end+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                    }
                                    $scope.secondarr.push($scope.t0);
                                }else if($scope.days[x].trip[y].id=='t1'){
                                        $scope.t1+='景点：'+$scope.days[x].trip[y].start+'，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                        $scope.secondarr.push($scope.t1);
                                }else if($scope.days[x].trip[y].id=='t2'){
                                       $scope.t2+='住宿：在'+$scope.days[x].trip[y].start+'住宿。'+$scope.days[x].trip[y].info;
                                       $scope.secondarr.push($scope.t2);
                                }else if($scope.days[x].trip[y].id=='t3'){
                                       $scope.t3+='用餐：在'+$scope.days[x].trip[y].start+'用餐，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                       $scope.secondarr.push($scope.t3);
                                }else if($scope.days[x].trip[y].id=='t4'){
                                       $scope.t4+='购物：在'+$scope.days[x].trip[y].start+'购物，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                       $scope.secondarr.push($scope.t4);
                                }else if($scope.days[x].trip[y].id=='t5'){
                                     $scope.coursestr='第'+(x+1)+'天:'+$scope.days.start+$scope.days.end;
                                       $scope.t5+='活动：在'+$scope.days[x].trip[y].start+'活动，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                       $scope.secondarr.push($scope.t5);
                                }else if($scope.days[x].trip[y].id=='t6'){
                                     $scope.coursestr='第'+(x+1)+'天:'+$scope.days.start+$scope.days.end;
                                       $scope.t6+='推荐：在'+$scope.days[x].trip[y].start+'比较好，大约'+$scope.days[x].trip[y].time+'分钟。'+$scope.days[x].trip[y].info;
                                       $scope.secondarr.push($scope.t6);
                                }
                            }
                        }
                        $scope.shuzu.push($scope.secondarr);
                        $scope.biaotiarr.push($scope.coursestrinfo);
                    }
                	if(res.data.big_traffic_economy_ticket=='0'){
                        if(res.data.big_traffic_tax=='0'){
                            $scope.str+='含税经济舱机票、';
                        }else{
                            $scope.str+='不含税经济舱机票、';
                        }
                    }
                    if(res.data.big_traffic_train_ticket=='0'){
                        if(res.data.big_traffic_train_ticket_type=='0'){
                            $scope.str+='往返火车票、';
                        }else if(res.data.big_traffic_train_ticket_type=='1'){
                            $scope.str+='去程火车票、';
                        }else if(res.data.big_traffic_train_ticket_type=='2'){
                            $scope.str+='返程火车票、';
                        }else if(res.data.big_traffic_train_ticket_type=='3'){
                            $scope.str+='中间段火车票、';
                        }
                    }
                    if(res.data.big_traffic_tourist_bus=='0'){
                        if(res.data.big_traffic_bus_type=='0'){
                            $scope.str+='往返交通旅游大巴、';
                        }else if(res.data.big_traffic_bus_type=='1'){
                            $scope.str+='去程交通旅游大巴、';
                        }else if(res.data.big_traffic_bus_type=='2'){
                            $scope.str+='返程交通旅游大巴、';
                        }else if(res.data.big_traffic_bus_type=='3'){
                            $scope.str+='全程交通旅游大巴、';
                        }
                    }
                    if(res.data.big_traffic_local_tourist_bus=='0'){
                        $scope.str+='当地交通旅游大巴、';
                    }
                    if(res.data.big_traffic_return_ticket=='0'){
                        $scope.str+='往返车票、';
                    }
                    if(res.data.big_traffic_steamer_ticket=='0'){
                         if(res.data.big_traffic_steamer_ticket_type =='0'){
                            $scope.str+='往返船票、';
                        }else if(res.data.big_traffic_steamer_ticket_type =='1'){
                            $scope.str+='去程船票、';
                        }else if(res.data.big_traffic_steamer_ticket_type =='2'){
                            $scope.str+='返程船票、';
                        }else if(res.data.big_traffic_steamer_ticket_type =='3'){
                            $scope.str+='全程船票、';
                        }
                    }
                     $scope.str = $scope.str.substring(0,$scope.str.length-1);
                     if($scope.str!=''){
                        $scope.str+='。';
                     }
                    if(res.data.little_traffic_traffic =='0'){
                        if(res.data.little_traffic_traffic_type  =='0'){
                            $scope.smallstr+='往返小交通';
                        }else if(res.data.little_traffic_traffic_type  =='1'){
                            $scope.smallstr+='去程小交通、';
                        }else if(res.data.little_traffic_traffic_type  =='2'){
                            $scope.smallstr+='返程小交通、';
                        }
                    }
                    if(res.data.little_traffic_scenic_car=='0'){
                        $scope.smallstr+='景区内用车、';
                    }
                    if(res.data.safe_contain=='0'){
                        $scope.smallstr+='保险、';
                    }
                    $scope.smallstr = $scope.smallstr.substring(0,$scope.smallstr.length-1);
                    if($scope.smallstr!=''){
                        $scope.smallstr+='。';                        
                    }

                    if(res.data.stay_type=='0'){
                        $scope.sleepstr+='行程所列酒店、';
                    }else if(res.data.stay_type=='1'){
                        $scope.sleepstr+=res.data.stay_hotel_star+'星级标准'+res.data.stay_room_capacity+'人间、';
                    }else if(res.data.stay_type=='2'){
                        $scope.sleepstr+='普通酒店标准'+res.data.stay_hotel_num+'人间、';
                    }else if(res.data.stay_type=='3'){
                        $scope.sleepstr+=res.data.dest_hotel_destination+'(星)目的地酒店标准'+res.data.dest_room_capacity+'人间、';
                    }
                    if(res.data.cruises_dest_messages=='0'){
                        $scope.sleepstr+='目的地游轮、';
                    }
                     $scope.sleepstr = $scope.sleepstr.substring(0,$scope.sleepstr.length-1);
                     if($scope.sleepstr!=''){
                        $scope.sleepstr+='。';
                     }

                    if(res.data.dining_type=='0'){
                        $scope.eatstr+=res.data.dining_early+'早'+res.data.dining_dinner+'正、';
                    }else if(res.data.dining_type=='1'){

                    }else if(res.data.dining_type=='2'){
                        $scope.eatstr+='酒店含早正餐自理、';
                    }else if(res.data.dining_type=='3'){
                        $scope.eatstr+='其他：'+res.data.dining_custom+'、';
                    }
                    $scope.eatstr = $scope.eatstr.substring(0,$scope.eatstr.length-1);
                    if($scope.eatstr!=''){
                        $scope.eatstr+='。';
                    }

                    if(res.data.admission_ticket=='1'){
                        $scope.orderstr+='门票';
                    }
                    if(res.data.guide_service_type=='0'){
                        $scope.orderstr+='当地中文导游、';
                    }else if(res.data.guide_service_type=='1'){
                        $scope.orderstr+='专职中文领队兼导游(境外)、';
                    }else if(res.data.guide_service_type=='2'){
                        $scope.orderstr+='全程陪同中文导游(境内)、';
                    }else if(res.data.guide_service_type=='3'){
                        $scope.orderstr+='专职领队和当地中文导游(境外)、';
                    }else if(res.data.guide_service_type=='4'){
                        $scope.orderstr+='全陪和当地中文导游(境内)、';
                    }
                    if(res.data.guide_service_driver_tip=='0'){
                        $scope.orderstr+=res.data.guide_service_driver_price+'元司机小费、';
                    }
                    if(res.data.child_fee_exist=='0'){
                        $scope.orderstr+='儿童标准价、';
                    }
                    if(res.data.ticket_tax_round_trip=='0'){
                        $scope.orderstr+=res.data.ticket_tax_fee+'元往返机票税、';
                    }
                    var arrtuijian = res.data.else_messages_arr.split("|");
                    if(arrtuijian.length!=0){
                        // $scope.smallstr+=res.data.ticket_tax_fee+'元往返机票税、';
                        for(var x=0; x<arrtuijian.length; x++){
                            if(arrtuijian[x]=''){
                                $scope.orderstr+=arrtuijian[x]+'、';                                
                            }
                        }
                    }
                     $scope.orderstr = $scope.orderstr.substring(0,$scope.orderstr.length-1);
                     if($scope.orderstr!=''){
                        $scope.orderstr+='。';
                     }
                    //购买须知
                    if(res.data.hint_cancel_trip=='0'){
                        $scope.cautionstr+='此团在收容人数不足'+res.data.hint_min_travel_population+'人时提前'+res.data.hint_ravel_advance_notification+'天通知、';
                    }
                    if(res.data.hint_touring_drop_out=='0'){
                        $scope.cautionstr+='团队游览允许擅自离队、';
                    }else{
                        $scope.cautionstr+='团队游览不允许擅自离队、';
                    }
                    if(res.data.hint_touring_penalty=='0'){
                        $scope.cautionstr+='违约需要支付'+res.data.hint_touring_out_money+'元违约金、';
                    }
                    if(res.data.hint_not_outing_explain=='0'){
                        $scope.cautionstr+='甲方原因无法出游乙方负责说明。';
                    }else if(res.data.hint_not_outing_explain=='0'){
                        $scope.cautionstr+='不存在甲方原因无法出游乙方负责说明。';
                    }
                     $scope.cautionstr = $scope.cautionstr.substring(0,$scope.cautionstr.length-1);
                     if($scope.cautionstr!=''){
                        $scope.cautionstr+='。';                         
                     }

                    if(res.data.additional_lijiang_upkeep=='0'){
                        $scope.addstr+='丽江维护费80元每人、';
                    }
                    if(res.data.additional_hainan_payment=='0'){
                        $scope.addstr+='海南政府调节金'+res.data.additional_fee+'每人、';
                    }
                    if(res.data.additional_extra_fee=='0'){
                        $scope.addstr+='存在因交通延阻、战争、政变、罢工、天气、飞机机器故障、航班取消等导致额外费用、';
                    }
                    if(res.data.additional_personal_fee=='0'){
                        $scope.addstr+='酒店内洗漱、理发、电话、传真、收费电视、饮品、烟酒等个人消费。';
                    }
                     $scope.addstr = $scope.addstr.substring(0,$scope.addstr.length-1);
                     if($scope.addstr!=''){
                        $scope.addstr+='。';
                     }

                    $scope.importantstr+='最晚收材料提前'+res.data.bed_material_commit+'天、'
                    if(res.data.important_hair_regiment_type=='0'){
                        $scope.importantstr+='出发地成团、';
                    }else if(res.data.important_hair_regiment_type=='1'){
                        $scope.importantstr+='目的地成团、';
                    }else if(res.data.important_hair_regiment_type=='2'){
                        $scope.importantstr+='中转地联运、';
                    }
                    if(res.data.important_group_type=='0'){
                        $scope.importantstr+='独家发团、';
                    }else if(res.data.important_group_type=='1'){
                        $scope.importantstr+='联合发团、';
                    }
                    if(res.data.important_spell_group=='0'){
                        $scope.importantstr+='非行程中拼团、';
                    }else if(res.data.important_spell_group=='1'){
                        $scope.importantstr+='行程中拼团、';
                    }
                    if(res.data.important_local_join=='0'){
                        $scope.importantstr+='存在当地人员参团、';
                    }
                    if(res.data.important_spell_car=='0'){
                        $scope.importantstr+='存在拼车、';
                    }
                    if(res.data.important_change_car_guide=='0'){
                        $scope.importantstr+='存在当地换车或导游、';
                    }
                     $scope.importantstr = $scope.importantstr.substring(0,$scope.importantstr.length-1);
                     if($scope.importantstr!=''){
                        $scope.importantstr+='。';
                     }

                    if(res.data.stay_spell_room=='0'){
                        $scope.cautionsleepstr+='不可拼房、';
                    }else if(res.data.stay_spell_room=='1'){
                        $scope.cautionsleepstr+='可亲友加床、';
                    }
                    if(res.data.stay_toilet_articles=='0'){
                        $scope.cautionsleepstr+='提供洗漱用品。';
                    }else if(res.data.stay_toilet_articles=='1'){
                        $scope.cautionsleepstr+='不提供洗漱用品。';
                    }
                     $scope.cautionsleepstr = $scope.cautionsleepstr.substring(0,$scope.cautionsleepstr.length-1);
                     if($scope.cautionsleepstr!=''){
                        $scope.cautionsleepstr+='。';                         
                     }

                    if(res.data.spellgroup_count_right=='0'){
                        $scope.speelstr+='共拼团'+res.data.spellgroup_count+'次（准确）、';
                    }else if(res.data.spellgroup_count_right=='1'){
                         $scope.speelstr+='共拼团'+res.data.spellgroup_count+'次（估算）、';
                    }
                    $scope.speelstr = $scope.speelstr.substring(0,$scope.speelstr.length-1);
                    if($scope.speelstr!=''){
                        $scope.speelstr+='。';
                    }


                    $scope.paystr='1、为确保您能够按时出行，产品确认后请在'+res.data.payinfo_full+'小时内付款，同时按要求尽快提供出游所需要的材料并签订出游合同。'+'2、为确保您能够按时出行，产品确认后请在'+res.data.payinfo_advance+'小时内付预付款，同时按要求尽快提供出游所需要的材料，并于出团前5个工作日交齐尾款并签订出游合同。'+'3、预定时请告知您的出游人数，出发日期、住宿、用餐标准、以及您的特殊要求。';

                    if(res.data.special_order_min_right=='0'){
                        $scope.peoplestr+='订单少于'+res.data.special_order_min_num+'人时需要确认、';
                    }
                    if(res.data.special_order_max_right=='0'){
                        $scope.peoplestr+='订单多于'+res.data.special_order_max_num+'人时需要确认、';
                    }
                    if(res.data.special_age_min_right=='0'){
                        $scope.peoplestr+='出游人年龄小于'+res.data.special_age_min_num+'岁不接收、';
                    }
                    if(res.data.special_age_max_right=='0'){
                        $scope.peoplestr+='出游人年龄大于'+res.data.special_age_max_num+'岁不接收、';
                    }
                    if(res.data.special_exceed_age_right=='0'){
                        $scope.peoplestr+='出游人年龄大于'+res.data.special_exceed_age_num+'岁需要签署健康协议、';
                    }
                    if(res.data.special_receive_foreignness=='0'){
                        $scope.peoplestr+='接收外籍游客、';
                    }else if(res.data.special_receive_foreignness=='0'){
                        $scope.peoplestr+='不接收外籍游客、';
                    }
                    if(res.data.special_region_limit=='0'){
                        $scope.peoplestr+='存在地域限制。';
                    }
                     $scope.peoplestr = $scope.peoplestr.substring(0,$scope.peoplestr.length-1);
                     if($scope.peoplestr!=''){
                        $scope.peoplestr+='。';
                     }


                    



                }else{
                    alert(res.errmsg);
                }
            });
            $scope.cancel = function () {  
                $modalInstance.dismiss('cancel'); 
            };
           $scope.toinfo = function (item) {

                $scope.item = item;
                var modalInstance = $modal.open({
                    template: require('../views/confirminfotwo.html'),
                    controller: 'lineconfirminfotwo',
                    size: 'lg',
                    resolve: {
                        items: function () {
                            // $modalInstance.close($scope.result);
                            return $scope.obj;
                        }
                    }
                });
                 modalInstance.result.then(function (showResult) {
                    $modalInstance.close($scope.result);
		        });
            };
            $scope.reject = function () {
                
                var modalInstance = $modal.open({
                    template: require('../views/reject.html'),
                    controller: 'linereject',
                    size: 'lg',
                    resolve: {
                        items: function () {
                            // $modalInstance.close($scope.result);
                            return $scope.obj;
                        }
                    }
                });
                 modalInstance.result.then(function (showResult) {
                    $modalInstance.close($scope.result);
		        });
            }

            $scope.reject1 = function () {
                $scope.obj.wjq='wjq1';
                var modalInstance = $modal.open({
                    template: require('../views/reject.html'),
                    controller: 'linereject',
                    size: 'lg',
                    resolve: {
                        items: function () {
                            // $modalInstance.close($scope.result);
                            return $scope.obj;
                        }
                    }
                });
                 modalInstance.result.then(function (showResult) {
                    $modalInstance.close($scope.result);
		        });
            }

            $scope.toinfo1 = function () {

                var modalInstance = $modal.open({
                    template: require('../views/reject.html'),
                    controller: 'linereject',
                    size: 'lg',
                    resolve: {
                        items: function () {
                            // $modalInstance.close($scope.result);
                            return $scope.obj;
                        }
                    }
                });
                 modalInstance.result.then(function (showResult) {
                    $modalInstance.close($scope.result);
		        });
            }
    // $scope.obj = items;
};