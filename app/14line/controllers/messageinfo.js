module.exports = function($scope, $state, $stateParams, $modalInstance, items, $resource){
   
    $scope.item=items;
    $scope.obj='';
     $resource('/api/as/lc/sendmessage/findGuide', {}, {}).
			get({'product_id': $scope.item.product_id , 'tour_date' : $scope.item.tour_date}, function(res){
                if(res.errcode === 0 || res.errcode === 10003){
					$scope.obj=res.data;
                    // $scope.touristList=res.data.touristList;
                	
                }else{
                    alert(res.errmsg);
                }
            });
    $scope.cancel = function () {  
        $modalInstance.dismiss('cancel'); 
    };
    $scope.gogogo = function () {
        if($scope.messageinfo.$valid){
        }else{
            return;
        }
        $scope.obj['tour_date']=items.tour_date;
        $scope.obj['product_id']=items.product_id;
        
        
       
        $resource('/api/as/lc/sendmessage/create', {}, {}).save($scope.obj, function(res){
            if(res.errcode === 0){
                $modalInstance.close($scope.result);
                alert('保存成功');
            }else{
                alert(res.errmsg);
            }
        });
    };
};