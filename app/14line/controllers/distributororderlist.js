/**
 * 模态框
 */
module.exports = function ($scope, $state, $stateParams,$resource,$modal) {
	/* 分页
     * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.currentPage1 = 1;      //当前页码
    $scope.itemsPerPage = 10         //每页显示几条
    $scope.a=[];
	 

	$scope.pageChanged1 = function () {

       var para = {
        //    ta:1, 
            pageNo:$scope.currentPage1, 
            pageSize:$scope.itemsPerPage,
			order_code : $scope.order_code,
			order_name : $scope.order_name,
			product_id : $scope.product_id,
			supplier_product_name : $scope.supplier_product_name,
			order_state : $scope.a.order_state,
			pay_state : $scope.a.pay_state,
			upplier_check_state : $scope.a.upplier_check_state,
			tour_date :  $scope.tour_date,
			tour_date_two :  $scope.tour_date_two,
            cancel_state :  $scope.a.cancel_state,
        };
        console.log('下面是pagechanged传参');
        console.log(para);
        
        $resource('/api/ac/lc/lineDistributorOrderService/findOrderList', {}, {}).
			save(para, function(res){
                console.log('下面是orderlist的res');
                 console.log(res);
                // console.log('上面是orderlist的res');
                if(res.errcode === 0 || res.errcode === 10003){
                    // console.log(res);
					$scope.a=res.data.results;                    
					$scope.totalItems = res.data.totalRecord;
                	// $scope.b='0';
                    for(var p = 0 ;p<$scope.a.length;p++){                        
                        if($scope.a[p].cancel_state =="1"){                           
                            $scope.a[p].color= true;
                        }
                    }
                    for(var z = 0;z<$scope.a.length;z++){                        
                        var temp  = $scope.a[z].touristlist;
                        for(var y = 0;y<temp.length;y++) {
                          if(temp[y].card_type_name =='身份证'){                            
                              if(temp[y].cardno == undefined ||temp[y].mobile == undefined || temp[y].name == undefined ||temp[y].cardno == '' ||temp[y].mobile == '' || temp[y].name == ''){
                                  $scope.a[z].b='0';
                               break;
                            
                              } else {
                                  $scope.a[z].b='1';
                              }
                          } else if(temp[y].card_type_name =='护照'||temp[y].card_type_name =='港澳通行证'){
                              if(temp[y].cardno == undefined ||temp[y].mobile == undefined || temp[y].name == undefined ||temp[y].certificate_yesr == undefined ||temp[y].certificate_month == undefined ||temp[y].certificate_day == undefined ||temp[y].birthday_day == undefined ||temp[y].birthday_mouth == undefined ||temp[y].birthday_year == undefined||temp[y].certificate_yesr == '' ||temp[y].certificate_month == '' ||temp[y].certificate_day == ''||temp[y].birthday_day == '' ||temp[y].birthday_mouth == '' ||temp[y].birthday_year == ''||temp[y].cardno == '' ||temp[y].mobile == '' || temp[y].name == ''){
                                  $scope.a[z].b='0';
                              break;
                              } else {
                                  $scope.a[z].b='1';
                              }
                          } else if(temp[y].card_type_name =='台胞证'||temp[y].card_type_name =='军官证'){
                              if(temp[y].cardno == undefined ||temp[y].mobile == undefined || temp[y].name == undefined ||temp[y].birthday_day == undefined ||temp[y].birthday_mouth == undefined ||temp[y].birthday_year == undefined||temp[y].birthday_day == '' ||temp[y].birthday_mouth == '' ||temp[y].birthday_year == '' ||temp[y].cardno == '' ||temp[y].mobile == '' || temp[y].name == ''){
                                  $scope.a[z].b='0';
                              break;
                            }
                          } else {
                                if(temp[y].cardno == undefined ||temp[y].mobile == undefined || temp[y].name == undefined ||temp[y].certificate_yesr == undefined ||temp[y].certificate_month == undefined ||temp[y].certificate_day == undefined ||temp[y].birthday_day == undefined ||temp[y].birthday_mouth == undefined ||temp[y].birthday_year == undefined||temp[y].certificate_yesr == '' ||temp[y].certificate_month == '' ||temp[y].certificate_day == ''||temp[y].birthday_day == '' ||temp[y].birthday_mouth == '' ||temp[y].birthday_year == ''||temp[y].cardno == '' ||temp[y].mobile == '' || temp[y].name == ''){
                                  $scope.a[z].b='0';
                              break;
                              } else {
                                  $scope.a[z].b='1';
                              }                                 
                              }
                        
                        }
                    
                    }
                }else{
                    alert(res.errmsg);
                }
                 
            });

    };
	$scope.pageChanged1();

			$scope.search=function(){

				if($scope.date.lable){
                    $scope.tour_date=date2str2($scope.date.lable);
                }else{
                    $scope.tour_date=''
                }
                if($scope.date1.lable){
                    $scope.tour_date_two=date2str2($scope.date1.lable);
                }else{
                    $scope.tour_date_two=''
                }

                if($scope.date.lable!=null && $scope.date1.lable!=null){
                    if($scope.tour_date>$scope.tour_date_two){
                        alert('开始日期不能大于结束日期');
                        return;
                    }
                }
                if($scope.date.lable){
                    if(!$scope.date1.lable){
                        alert('开始和结束日期必须同时输入进行搜索')
                        return;
                    }
                }
                if($scope.date1.lable){
                    if(!$scope.date.lable){
                        alert('开始和结束日期必须同时输入进行搜索')
                        return;
                    }
                }
			var dic = {
				order_code : $scope.order_code,
				order_name : $scope.order_name,
				product_id : $scope.product_id,
				supplier_product_name : $scope.supplier_product_name,
				order_state : $scope.a.order_state,
				pay_state : $scope.a.pay_state,
				upplier_check_state : $scope.a.upplier_check_state,
				tour_date :  $scope.tour_date,
				tour_date_two :  $scope.tour_date_two,
                cancel_state :  $scope.a.cancel_state,
			}
            console.log('下面是搜索传参')
            console.log(dic);
				$resource('/api/ac/lc/lineDistributorOrderService/findOrderList', {}, {}).save(dic, function(res){
                if(res.errcode === 0 || res.errcode === 10003){               
				     $scope.a=res.data.results;
                      for(var p = 0 ;p<$scope.a.length;p++){                        
                        if($scope.a[p].cancel_state =="1"){                           
                            $scope.a[p].color= true;
                        }
                    }
                    
                	 $scope.totalItems = res.data.totalRecord;
                     for(var z = 0;z<$scope.a.length;z++){
                        var temp  = $scope.a[z].touristlist;
                        for(var y = 0;y<temp.length;y++) {
                          if(temp[y].card_type_name =='身份证'){                            
                              if(temp[y].cardno == undefined ||temp[y].mobile == undefined || temp[y].name == undefined ||temp[y].cardno == '' ||temp[y].mobile == '' || temp[y].name == ''){
                                  $scope.a[z].b='0';
                               break;
                            
                              } else {
                                  $scope.a[z].b='1';
                              }
                          } else if(temp[y].card_type_name =='护照'||temp[y].card_type_name =='港澳通行证'){
                              if(temp[y].cardno == undefined ||temp[y].mobile == undefined || temp[y].name == undefined ||temp[y].certificate_yesr == undefined ||temp[y].certificate_month == undefined ||temp[y].certificate_day == undefined ||temp[y].birthday_day == undefined ||temp[y].birthday_mouth == undefined ||temp[y].birthday_year == undefined||temp[y].certificate_yesr == '' ||temp[y].certificate_month == '' ||temp[y].certificate_day == ''||temp[y].birthday_day == '' ||temp[y].birthday_mouth == '' ||temp[y].birthday_year == ''||temp[y].cardno == '' ||temp[y].mobile == '' || temp[y].name == ''){
                                  $scope.a[z].b='0';
                              break;
                              } else {
                                  $scope.a[z].b='1';
                              }
                          } else if(temp[y].card_type_name =='台胞证'||temp[y].card_type_name =='军官证'){
                              if(temp[y].cardno == undefined ||temp[y].mobile == undefined || temp[y].name == undefined ||temp[y].birthday_day == undefined ||temp[y].birthday_mouth == undefined ||temp[y].birthday_year == undefined||temp[y].birthday_day == '' ||temp[y].birthday_mouth == '' ||temp[y].birthday_year == '' ||temp[y].cardno == '' ||temp[y].mobile == '' || temp[y].name == ''){
                                  $scope.a[z].b='0';
                              break;
                            }
                          } else {
                                if(temp[y].cardno == undefined ||temp[y].mobile == undefined || temp[y].name == undefined ||temp[y].certificate_yesr == undefined ||temp[y].certificate_month == undefined ||temp[y].certificate_day == undefined ||temp[y].birthday_day == undefined ||temp[y].birthday_mouth == undefined ||temp[y].birthday_year == undefined||temp[y].certificate_yesr == '' ||temp[y].certificate_month == '' ||temp[y].certificate_day == ''||temp[y].birthday_day == '' ||temp[y].birthday_mouth == '' ||temp[y].birthday_year == ''||temp[y].cardno == '' ||temp[y].mobile == '' || temp[y].name == ''){
                                  $scope.a[z].b='0';
                              break;
                              } else {
                                  $scope.a[z].b='1';
                              }                                 
                              }
                        
                        }
                    
                    }
                }else{
                    alert(res.errmsg);
                }
            });
			}
            //更改订单状态
            $scope.changestate = function(info){
                var order_code = info.order_code;
                console.log(info.order_code);
                $resource('/api/as/lc/distributor/updateByCode', {}, {}).save({'order_code':order_code}, function(res){    
                    if(res.errcode === 0 || res.errcode === 10003){
                        $scope.pageChanged1();
                }else{
                    alert(res.errmsg);
                }
            });          
                
            }
            //详情
            $scope.distributor_toinfo = function (info) {
                //$scope.item = $scope.a[index];
                var modalInstance = $modal.open({
                    template: require('../views/orderinfo.html'),
                    controller: 'lineorderinfo',
                    size: 'lg',
                    resolve: {
                        items: function () {
                            return info;
                        }
                    }
                });
                modalInstance.result.then(function (showResult) {	
                        $scope.pageChanged1();
                });
                
            }
            //支付
             $scope.distributor_pay = function (info) {
                //  console.log(info);
               // $scope.item = $scope.a[index];
               info.a='1';
                var modalInstance = $modal.open({
                    template: require('../views/distributor_orderpay.html'),
                    controller: 'linedistributororderpay',
                    size: 'lg',
                    resolve: {
                        items: function () {
                            return info;
                        }
                    }
                });
                modalInstance.result.then(function (showResult) {	
                        $scope.pageChanged1();
                });
                
            }
            //人员信息
            $scope.distributor_tourinfo = function (info) {

                 info.a='0';
                var modalInstance = $modal.open({
                    template: require('../views/distributor_orderpay.html'),
                    controller: 'linedistributororderpay',
                    size: 'lg',
                    resolve: {
                        items: function () {
                            return info;
                        }
                    }
                });
                modalInstance.result.then(function (showResult) {	
                        $scope.pageChanged1();
                });
                
            }
            //退款

            $scope.toinfo1 = function (info) {
                $scope.item = info;
                $scope.item.wjq='wjq';
                var modalInstance = $modal.open({
                    template: require('../views/reject.html'),
                    controller: 'linereject',
                    size: 'lg',
                    resolve: {
                        items: function () {
                            // $modalInstance.close($scope.result);
                            return $scope.item;
                        }
                    }
                });
                 modalInstance.result.then(function (showResult) {
                    // $modalInstance.close($scope.result);
                     $scope.pageChanged1();
		        });
            }

			$scope.date = {
                // 'lable': date2str2(new Date()),
                'value': date2str(new Date()),
                'opened': false
            }


             $scope.dateOpen = function ($event, item) {
                $event.preventDefault();
                $event.stopPropagation();
                item.opened = true;
            };

            $scope.date1 = {
                // 'lable': date2str2(new Date()),
                'value': date2str(new Date()),
                'opened': false
            }


             $scope.dateOpen1 = function ($event, item) {
                $event.preventDefault();
                $event.stopPropagation();
                item.opened = true;
            };

            function date2str2(d) {
                if (d === undefined) {
                    return "";
                }
                var month = (d.getMonth() + 1).toString();
                var day = d.getDate().toString();
                if (month.length == 1) month = '0' + month;
                if (day.length == 1) day = '0' + day;
                return d.getFullYear() + "-" + month + "-" + day;
            }

            function date2str(d) {
                if (d === undefined) {
                    return "";
                }
                var month = (d.getMonth() + 1).toString();
                var day = d.getDate().toString();
                if (month.length == 1) month = '0' + month;
                if (day.length == 1) day = '0' + day;
                return d.getFullYear() + month + day;
            }
            


 $scope.zhifu=[{name:'未选择',value:''},
                {name:'待支付',value:'0'},
                {name:'已支付',value:'1'}
              
            ]
$scope.quxiao=[{name:'未选择',value:''},
                {name:'正常',value:'0'},
                {name:'取消',value:'1'}
              
            ]
$scope.shenhe=[{name:'未选择',value:''},
                {name:'未审核',value:'0'},
                {name:'已审核',value:'1'},
                {name:'已驳回',value:'2'}    
              
            ]
$scope.dingdan=[{name:'未选择',value:''},
                {name:'订单提交中',value:'0'},
                {name:'占位中',value:'1'},
                {name:'待签约付款',value:'2'},
                {name:'已付款,待确认',value:'3'},
                {name:'开心出游',value:'4'} ,
                {name:'归来点评',value:'5'},
                {name:'已完成',value:'6'},                                 
                {name:'已退款',value:'7'},
                {name:'申请退款',value:'8'},
                {name:'申请退款失败',value:'9'},
               {name:'退款中',value:'10'},
               {name:'超时未付款，已取消订单',value:'11'}
            ]
	$scope.start = {
		'date' : '',
		'opened' : false,
	};

	$scope.end = {
		'date' : '',
		'opened' : false,
	};

	$scope.open = function(obj) {
		obj.opened = true;
	}
};
