module.exports = function($scope, formconfig, $stateParams, viewmodel){

	var id = $stateParams.id;

	formconfig.start({
		'title' : '我的详情',
		'formtitle' : '我的基本信息',
		'elements' : viewmodel(),
		'info' : {
			'url' : '/api/ac/tc/placeView/info',
			'para' : {'id' : id}
		}
	}, $scope);

	$scope.form = formconfig;

};