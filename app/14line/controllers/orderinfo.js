/**
 * 模态框
 */
module.exports = function($scope, $state, $stateParams, $modalInstance, items, $resource,$modal){

    $scope.wjq=0;
    if(items.pay_state_name=='已支付' && items.upplier_check_state_name=='已审核' && items.order_state_name=='开心出游'){
        $scope.wjq=1;
    }
    
    $resource('/api/ac/lc/lineOrderListService/findOrderInfo', {}, {}).
			get({order_code : items.order_code,product_id : items.product_id}, function(res){

                if(res.errcode === 0 || res.errcode === 10003){
					$scope.obj=res.data;
                    console.log($scope.obj);
                    $scope.touristList=res.data.touristList;
                    $scope.touristList2=[];
                    for(var z=0;z<$scope.touristList.length;z++){
                        if($scope.touristList[z].lot_name!=null&&$scope.touristList[z].lot_name!=undefined&&$scope.touristList[z].lot_name!=''){
                            $scope.touristList2.push($scope.touristList[z]);
                        }
                    }
                    //联系人信息
                        if($scope.obj.from_app_id=='juyou_yc'){
                            $scope.obj.from_app_id='分销商端'
                        }
                        if($scope.obj.from_app_id=='juyouWX'){
                            $scope.obj.from_app_id='居游微信'
                        }
                        if($scope.obj.from_app_id=='shangke'){
                            $scope.obj.from_app_id='商客'
                        }
                        if($scope.obj.from_app_id=='juyouAPP'){
                            $scope.obj.from_app_id='居游APP'
                        }

                }else{
                    alert(res.errmsg);
                }
            });
            $scope.cancel = function () {  
                $modalInstance.dismiss('cancel'); 
            };

            $scope.reject = function () {
                var modalInstance = $modal.open({
                    template: require('../views/reject.html'),
                    controller: 'linereject',
                    size: 'lg',
                    resolve: {
                        items: function () {
                            // $modalInstance.close($scope.result);
                            return $scope.obj;
                        }
                    }
                });
                 modalInstance.result.then(function (showResult) {
                    $modalInstance.close($scope.result);
		        });
            }
    // $scope.obj = items;
};