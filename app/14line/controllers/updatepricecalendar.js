/**
 * 模态框
 */
module.exports = function ($scope, $state, $resource, $stateParams, $modalInstance, $http, items) {

    $scope.product_id = $scope.$parent.$stateParams.id;

    $scope.product_state_name_flag = items.product_state_name != '草稿';
    $scope.product_state_name_flag_2 = items.product_state_name != '已上架';
    //数据模板
    $scope.result = {
        stock_surplus: '',
        selectedDate: [],
        // close_group : '0',
        adult_call_price: 0,
        adult_sale_price: 0,
        children_call_price: 0,
        children_sale_price: 0,
        single_room_call_price: 0,
        single_room_sale_price: 0
    }

    //提交结果集
    $scope.lastResult = [];

    $scope.date = {
        'label': date2str2(new Date()),
        'value': date2str(new Date()),
        'opened': true
    }

    $scope.dateOpen = function ($event, item) {
        $event.preventDefault();
        $event.stopPropagation();
        item.opened = true;
    };

    function date2str(d) {
        if (d === undefined) {
            return "";
        }
        var month = (d.getMonth() + 1).toString();
        var day = d.getDate().toString();
        if (month.length == 1) month = '0' + month;
        if (day.length == 1) day = '0' + day;
        return d.getFullYear() + month + day;
    }

    function date2str2(d) {
        if (d === undefined) {
            return "";
        }
        var month = (d.getMonth() + 1).toString();
        var day = d.getDate().toString();
        if (month.length == 1) month = '0' + month;
        if (day.length == 1) day = '0' + day;
        return d.getFullYear() + "-" + month + "-" + day;
    }

    $scope.toggleMode = function () {
        $scope.ismeridian = !$scope.ismeridian;
    };

    //天
    $scope.day = [];
    for (var index = 0; index < 51; index++) {
        $scope.day.push({ value: index })
    }

    //时
    $scope.hour = [];
    for (var index = 0; index < 24; index++) {
        $scope.hour.push({ value: index })
    }

    //分
    $scope.minute = [];
    for (var index = 0; index < 60; index++) {
        $scope.minute.push({ value: index })
    }

    //添加选择日期
    $scope.addSelectDate = function () {
        //已有数据日期
        for (var index = 0; index < items.dateArray.length; index++) {

            //查找是否有对应日期
            if ((typeof ($scope.date.label) == 'string' ? $scope.date.label.replace('-', '').replace('-', '') : date2str($scope.date.label)) == items.dateArray[index]) {
                //所有已有数据
                var tempDate = typeof ($scope.date.label) == 'string' ? $scope.date.label.replace('-', '').replace('-', '') : date2str($scope.date.label);
                //已选日期
                for (var index = 0; index < $scope.result.selectedDate.length; index++) {
                    if (tempDate == $scope.result.selectedDate[index].date) {
                        alert('不可添加重复日期');
                        return;
                    }
                }
                $scope.result.selectedDate.push({ date: typeof ($scope.date.label) == 'string' ? $scope.date.label.replace('-', '').replace('-', '') : date2str($scope.date.label) });
                return false;
            }
        }
        alert('此日期无信息,不可更改!');
    }

    //删除选择日期
    $scope.removeSelectDate = function (index) {
        $scope.result.selectedDate.splice(index, 1);
    }

    $scope.ok = function () {
        if ($scope.result.selectedDate.length < 1) {
            alert('请选择日期');
            return;
        }
        if (!$scope.updatePriceCalendar.$valid) {
            alert('请填写页面内所有可输入的输入框');
            return;
        }
        //共有库存
        if ($scope.result.stock_type == 1) {
            //页面选择日期
            for (var indexDateArray = 0; indexDateArray < $scope.result.selectedDate.length; indexDateArray++) {
                //所有日期与数据
                for (var indexItemsData = 0; indexItemsData < items.data.length; indexItemsData++) {
                    //页面日期与数据库日期对应
                    if (items.data[indexItemsData].d == $scope.result.selectedDate[indexDateArray].date) {
                        //库存不能小
                        if (items.data[indexItemsData].true_stock > $scope.result.stock_totals) {
                            alert('日库存不可小于已卖出库存');
                            return;
                        }
                    }
                }
            }
        }
        var temp = angular.copy($scope.result);

        temp.children_ban = temp.children_ban ? '1' : '0';
        if(temp.children_ban == '1'){
            temp.children_call_price = 0;
            temp.children_sale_price = 0;
        }

        temp.adult_ban = temp.adult_ban ? '1' : '0';
        if(temp.adult_ban == '1'){
            temp.adult_call_price = 0;
            temp.adult_sale_price = 0;
        }

        temp.single_room_ban = temp.single_room_ban ? '1' : '0';
        if(temp.single_room_ban == '1'){
            temp.single_room_call_price = 0;
            temp.single_room_sale_price = 0;
        }

        if( temp.stock_type=='0'){
            temp.stock_totals = 0;
            temp.stock= 0;
        }
        
        for (var index = 0; index < temp.selectedDate.length; index++) {
            var temp_2 = angular.copy(temp);
            temp_2.tour_date = temp.selectedDate[index].date.substring(0, 4) + '-' + temp.selectedDate[index].date.substring(4, 6) +
                '-' + temp.selectedDate[index].date.substring(6, 8);
            delete temp_2.selectedDate;
            temp_2.trip_id = 'A103021489303125738';
            $scope.lastResult.push(temp_2);
        }
        var para = {
            // product_id: 'A103011489302762689',
            product_id: $scope.product_id,
            list: $scope.lastResult
        }
        // console.log(para);
        $resource('/api/ac/lc/groupDatesService/create', {}, {}).save(para, function (res) {
            if (res.errcode === 0) {
                alert('保存成功');
                $modalInstance.close($scope.result);
            } else {
                alert(res.errmsg);
            }
        });

    };
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

};