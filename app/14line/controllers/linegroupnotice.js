/**
 * 模态框
 */
module.exports = function ($scope, $state, $stateParams,$resource,$modal) {
	/* 分页
     * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.currentPage = 1;      //当前页码
    $scope.itemsPerPage = 10         //每页显示几条
    $scope.d={};
	 

	$scope.pageChanged = function () {
       var para = {
        //    ta:1, 
            pageNo:$scope.currentPage, 
            pageSize:$scope.itemsPerPage,
			order_code : $scope.order_code,
			order_name : $scope.order_name,
			product_id : $scope.product_id,
			supplier_product_name : $scope.supplier_product_name,
			order_state : $scope.d.order_state,
			pay_state : $scope.d.pay_state,
			upplier_check_state : $scope.d.upplier_check_state,
			tour_date :  $scope.tour_date,
			tour_date_two :  $scope.tour_date_two,
            cancel_state :  $scope.d.cancel_state,
        };
        $resource('/api/as/lc/gotourorderlist/findGotourOrderList', {}, {}).
			save(para, function(res){
                if(res.errcode === 0 || res.errcode === 10003){
					$scope.a=res.data.results;
					$scope.totalItems = res.data.totalRecord;
                	
                }else{
                    alert(res.errmsg);
                }
            });

    };
	$scope.pageChanged();

			$scope.b= '0';
			$scope.c = '';
			$scope.show = function(index){
				if($scope.b==0){
					$scope.b='1',
					$scope.c=index
				}else if($scope.b==1){
					if($scope.c!=index){
						$scope.c=index
					}else{
						$scope.b='0',
						$scope.c=index
					}	
				}	
			};

            $scope.psarr=[
                {name:'请选择',value:''},
                {name:'待支付',value:'0'},
                {name:'已支付',value:'1'},
            ]
            $scope.osarr=[
                {name:'请选择',value:''},
                {name:'订单提交中',value:'0'},
                {name:'占位中',value:'1'},
                {name:'待确认付款',value:'2'},
                {name:'已付款,待确认',value:'3'},
                {name:'开心出游',value:'4'},
                {name:'归来点评',value:'5'},
                {name:'已完成',value:'6'},
                {name:'已退款',value:'7'},
                {name:'申请退款',value:'8'},
                {name:'申请退款失败',value:'9'},
            ]
            $scope.csarr=[
                {name:'请选择',value:''},
                {name:'正常',value:'0'},
                {name:'取消',value:'1'},
            ]
            $scope.ucsarr=[
                {name:'请选择',value:''},
                {name:'未审核',value:'0'},
                {name:'已审核',value:'1'},
                {name:'已驳回',value:'2'},
            ]

            
			
			$scope.search=function(){

				if($scope.date.lable){
                    $scope.tour_date=date2str2($scope.date.lable);
                }else{
                    $scope.tour_date=''
                }
                if($scope.date1.lable){
                    $scope.tour_date_two=date2str2($scope.date1.lable);
                }else{
                    $scope.tour_date_two=''
                }

                if($scope.date.lable!=null && $scope.date1.lable!=null){
                    if($scope.tour_date>$scope.tour_date_two){
                        alert('开始日期不能大于结束日期');
                        return;
                    }
                }
                if($scope.date.lable){
                    if(!$scope.date1.lable){
                        alert('开始和结束日期必须同时输入进行搜索')
                        return;
                    }
                }
                if($scope.date1.lable){
                    if(!$scope.date.lable){
                        alert('开始和结束日期必须同时输入进行搜索')
                        return;
                    }
                }
			var dic = {
				order_code : $scope.order_code,
				order_name : $scope.order_name,
				product_id : $scope.product_id,
				supplier_product_name : $scope.supplier_product_name,
				order_state : $scope.d.order_state,
				pay_state : $scope.d.pay_state,
				upplier_check_state : $scope.d.upplier_check_state,
				tour_date :  $scope.tour_date,
				tour_date_two :  $scope.tour_date_two,
                cancel_state :  $scope.d.cancel_state,
			}
				$resource('/api/as/lc/gotourorderlist/findGotourOrderList', {}, {}).save(dic, function(res){
                if(res.errcode === 0 || res.errcode === 10003){
					$scope.a=res.data.results;
                	$scope.totalItems = res.data.totalRecord;
                }else{
                    alert(res.errmsg);
                }
            });
			}
            $scope.toinfo = function (index) {
                $scope.item = $scope.a[index];
                var modalInstance = $modal.open({
                    template: require('../views/gotournoticeinfo.html'),
                    controller: 'gotournoticeinfo',
                    size: 'lg',
                    resolve: {
                        items: function () {
                            return $scope.item;
                        }
                    }
                });
                modalInstance.result.then(function (showResult) {	
                        $scope.pageChanged();
                });
                
            }
          

			$scope.date = {
                // 'lable': date2str2(new Date()),
                'value': date2str(new Date()),
                'opened': false
            }


             $scope.dateOpen = function (item) {
                
                item.opened = true;
            };

            $scope.date1 = {
                // 'lable': date2str2(new Date()),
                'value': date2str(new Date()),
                'opened': false
            }


             $scope.dateOpen1 = function (item) {
                
                item.opened = true;
            };

            function date2str2(d) {
                if (d === undefined) {
                    return "";
                }
                var month = (d.getMonth() + 1).toString();
                var day = d.getDate().toString();
                if (month.length == 1) month = '0' + month;
                if (day.length == 1) day = '0' + day;
                return d.getFullYear() + "-" + month + "-" + day;
            }

            function date2str(d) {
                if (d === undefined) {
                    return "";
                }
                var month = (d.getMonth() + 1).toString();
                var day = d.getDate().toString();
                if (month.length == 1) month = '0' + month;
                if (day.length == 1) day = '0' + day;
                return d.getFullYear() + month + day;
            }
            
};
