/**
 * 模态框
 */
module.exports = function ($scope, $state, $stateParams,$resource,$modal) {
	/* 分页预存管理列表
     * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.currentPage = 1;      //当前页码
    $scope.itemsPerPage = 10         //每页显示几条
    $scope.item={};
    $scope.wjq=1;
    $scope.strpt='';
	$scope.pageChanged = function () {
         $resource('/api/ac/lc/employeeService/findEmployeeList', {}, {})
        .save({}, function (res) {          
            if(res.errcode === 0 || res.errcode === 10003){
                console.log('调员工接口');
                console.log(res);
					$scope.a=res.data.results;
                    $scope.totalItems = res.data.totalRecord;
                    for(var i = 0;i<$scope.a.length;i++){                       
                        for(var j = 0;j<$scope.a[i].product_type.length;j++){
                            if($scope.a[i].product_type[j] =='0'){
                                $scope.a[i].product_type[j] = '周边游';
                            }
                            if($scope.a[i].product_type[j] =='1'){
                                $scope.a[i].product_type[j] = '国内长线';
                            }
                            if($scope.a[i].product_type[j] =='2'){
                                $scope.a[i].product_type[j] = '国内当地参团';
                            }
                            if($scope.a[i].product_type[j] =='3'){
                                $scope.a[i].product_type[j] = '出境当地参团';
                            }
                            if($scope.a[i].product_type[j] =='4'){
                                $scope.a[i].product_type[j] = '出境短线';
                            }
                            if($scope.a[i].product_type[j] =='5'){
                                $scope.a[i].product_type[j] = '出境长线';
                            }
                            
                        }
                        $scope.a[i].product_type = $scope.a[i].product_type.toString();
                    }
                    
                }else{
                    alert(res.errmsg);
                }
        });



    };
	$scope.pageChanged();
	
            $scope.add = function () {
                // console.log('adsad');
                var modalInstance = $modal.open({
                    template: require('../views/productmanageradd.html'),
                    controller: 'productmanageradd',
                    size: 'lg',
                    resolve: {
                        items: function () {
                            return $scope.item;
                        }
                    }
                });
                 modalInstance.result.then(function (showResult) {
                    $scope.pageChanged();
                });
            }
            $scope.modify = function (index) {
                 $scope.item = $scope.a[index];                
                var modalInstance = $modal.open({
                    template: require('../views/productmanagermodify.html'),
                    controller: 'productmanagermodify',
                    size: 'lg',
                    resolve: {
                        items: function () {
                            return $scope.item;
                        }
                    }
                });
                 modalInstance.result.then(function (showResult) {
                    $scope.pageChanged();
                });
            }
            $scope.delete = function (index) {
                  if(!confirm("是否确认删除")){
                    return ;
                }
                $scope.item = $scope.a[index];
                console.log($scope.item.id);
                
                $resource('/api/as/lc/employee/del', {}, {}).save({ id: $scope.item.id }, function (res) {
                    if (res.errcode === 0) {
                        $scope.pageChanged();
                        alert('删除成功');
                    } else {
                        alert(res.errmsg);
                    }
                });
                 
            }
 

            


};
