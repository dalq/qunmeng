/**
 * 模态框
 */
module.exports = function ($scope, $state, $stateParams,$resource,$modal) {
	/* 分页
     * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.currentPage = 1;      //当前页码
    $scope.itemsPerPage = 10         //每页显示几条
    $scope.a=[];
	 

	$scope.pageChanged = function () {
       var para = {
        //    ta:1, 
            pageNo:$scope.currentPage, 
            pageSize:$scope.itemsPerPage,
			order_code : $scope.id,
        };
        $resource('/api/as/lc/distributorBackOrder/findDistributorBackOrderList', {}, {}).
			save(para, function(res){
				// console.log('下面是res');
                // console.log(res);
                if(res.errcode === 0 || res.errcode === 10003){
					$scope.a=res.data.results;
					$scope.totalItems = res.data.totalRecord;
					for(var i = 0;i<$scope.a.length;i++){
						if($scope.a[i].children_call_price_name == '0'){
							$scope.a[i].children_call_price_name = '儿童禁售'
					}
					}
					
                	
                }else{
                    alert(res.errmsg);
                }
            });

    };
	$scope.pageChanged();


			$scope.search=function(){
				
			var dic = {
				// pay_time : $scope.pay_time,
				supplier_product_name : $scope.supplier_product_name,
				pay_state : $scope.a.pay_state,				
				
                order_code : $scope.id,
			}
				$resource('/api/as/lc/distributorBackOrder/findDistributorBackOrderList', {}, {}).save(dic, function(res){
                if(res.errcode === 0 || res.errcode === 10003){
					$scope.a=res.data.results;
                	$scope.totalItems = res.data.totalRecord;
                    for(var i = 0;i<$scope.a.length;i++){
						if($scope.a[i].children_call_price_name == '0'){
							$scope.a[i].children_call_price_name = '儿童禁售'
					}
					}
                }else{
                    alert(res.errmsg);
                }
            });
			}
            $scope.toinfo = function (info) {
                if(!confirm("是否确认已退款！")){
                    return ;
                }
                $scope.item = $scope.a[index];
                $resource('/api/as/lc/orderactual/updateState', {}, {}).
                    get({ 'order_code': $scope.item.id }, function (res) {
                        if (res.errcode === 0 || res.errcode === 10003) {
                            alert('退款成功');
							$scope.pageChanged();
                        } else {
                            alert(res.errmsg);
                        }
                    });
            }


            $scope.toinfo2 = function (index) {
               
                $scope.item = $scope.a[index];
                $scope.item.a='a';
                var modalInstance = $modal.open({
                    template: require('../views/confirminfo.html'),
                    controller: 'lineconfirminfo',
                    size: 'lg',
                    resolve: {
                        items: function () {
                            return $scope.item;
                        }
                    }
                });
                modalInstance.result.then(function (showResult) {	
                        $scope.pageChanged();
                });
            }

            
$scope.zhuangtai=[{name:'未选择',value:''},
                {name:'未支付',value:'0'},
                {name:'已支付',value:'1'}
]

};
