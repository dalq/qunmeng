module.exports = function($scope, formconfig, $stateParams, itinerarymodel){
	var id = $stateParams.id;
	formconfig.start({
		'title' : '创建信息',
		'formtitle' : '行程管理',
		'elements' : itinerarymodel(),
		'save' : {
			'url' : '/api/ac/tc/placeView/create',
			'to' : 'app.linelist',
			'para' : {'type' : 'J'}
		}
	}, $scope);

};