module.exports = function($scope, formconfig, $stateParams, containmodel){
	var id = $stateParams.id;
	formconfig.start({
		'title' : '创建信息',
		'formtitle' : '费用包含',
		'elements' : containmodel(),
		'save' : {
			'url' : '/api/as/lc/includePrice/create',
			'to' : 'app.linelist',
			'para' : {'dining_type' : '2','trip_id' : '111'}
		}
	}, $scope);

};