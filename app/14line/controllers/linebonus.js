module.exports = function($scope, $state, $stateParams, $modalInstance, items, $resource){
   
    $scope.item=items;
	$scope.result={};
	$scope.util={};
	$scope.util.bonusData={};
	$scope.util.bonusData.profit_margin=0;
	$resource('/api/ac/lc/productProfitService/findInfo', {}, {}).
		save({'product_id' : items.id}, function(res){
			if(res.errcode === 0 || res.errcode === 10003){
				$scope.util.bonusData = res.data;
				$scope.jisuan();
			}else{
				
				alert(res.errmsg);
			}
		});
		$scope.clearpeopleNoNum = function(obj,attr){
				console.log(obj[attr]);
				if(obj[attr]>100){
					alert('不可大于100');
					obj[attr]=100;
					$scope.jisuan();
					return;
				}
				if(!obj[attr]){
					$scope.jisuan();
					return;
				}
				//先把非数字的都替换掉，除了数字和.
				obj[attr] = obj[attr].replace(/[^\d.]/g,"");
				//必须保证第一个为数字而不是.
				obj[attr] = obj[attr].replace(/^\./g,"");
				//保证只有出现一个.而没有多个.
				obj[attr] = obj[attr].replace(/\.{2,}/g,"");
				//保证.只出现一次，而不能出现两次以上
				obj[attr] = obj[attr].replace(".","$#$").replace(/\./g,"").replace("$#$",".");
				$scope.jisuan();
			}
			$scope.clearpeopleNoNum1 = function(obj,attr){
				
				//先把非数字的都替换掉，除了数字和.
				obj[attr] = obj[attr].replace(/[^\d.]/g,"");
				//必须保证第一个为数字而不是.
				obj[attr] = obj[attr].replace(/^\./g,"");
				//保证只有出现一个.而没有多个.
				obj[attr] = obj[attr].replace(/\.{2,}/g,"");
				//保证.只出现一次，而不能出现两次以上
				obj[attr] = obj[attr].replace(".","$#$").replace(/\./g,"").replace("$#$",".");
			}
			$scope.jisuan=function(){
				var a=$scope.util.bonusData.profit_margin;
				if(!$scope.util.bonusData.profit_margin){
					// $scope.util.bonusData.profit_margin=0;
					a=0;
				}
				if($scope.util.bonusData.max_adult_profit){
					$scope.util.peoplemax = ((1-a/100)*$scope.util.bonusData.max_adult_profit).toFixed(2)+'元';	
				}else{
					$scope.util.peoplemax = (0).toFixed(2)+'元';
				}
				if($scope.util.bonusData.min_adult_profit){
					$scope.util.peoplemin = ((1-a/100)*$scope.util.bonusData.min_adult_profit).toFixed(2)+'元';				
				}else{
					$scope.util.peoplemin = (0).toFixed(2)+'元';
				}
				if($scope.util.bonusData.max_children_profit){
					$scope.util.childmax = ((1-a/100)*$scope.util.bonusData.max_children_profit).toFixed(2)+'元';				
				}else{
					$scope.util.childmax = (0).toFixed(2)+'元';
				}
				if($scope.util.bonusData.min_children_profit){
					$scope.util.childmin = ((1-a/100)*$scope.util.bonusData.min_children_profit).toFixed(2)+'元';				
				}else{
					$scope.util.childmin = (0).toFixed(2)+'元';
				}
			}
    $scope.cancel = function () {  
        $modalInstance.dismiss('cancel'); 
    };
    $scope.gogogo = function(){
		$scope.result['product_id'] = items.id;
		$scope.result['profit_margin'] = $scope.util.bonusData.profit_margin;
		$scope.result['integral_start'] = $scope.util.bonusData.integral_start;
		$scope.result['integral_end'] = $scope.util.bonusData.integral_end;
		$scope.result['share_title'] = $scope.util.bonusData.share_title;
		$scope.result['label'] = $scope.util.bonusData.label;
		if(!$scope.util.bonusData.share_title || $scope.util.bonusData.share_title==''){
			alert('分享标题不可为空');
			return;
		}
		if(!$scope.util.bonusData.label || $scope.util.bonusData.label==''){
			alert('标签不可为空');
			return;
		}
		if(!$scope.util.bonusData.profit_margin || $scope.util.bonusData.profit_margin==''){
			alert('利润率不可为空');
			return;
		}
		if(!$scope.util.bonusData.integral_start || $scope.util.bonusData.integral_start==''){
			alert('红包下限不可为空');
			return;
		}
		if(!$scope.util.bonusData.integral_end || $scope.util.bonusData.integral_end==''){
			alert('红包上限不可为空');
			return;
		}
		
		if(parseFloat($scope.util.bonusData.integral_start)>parseFloat($scope.util.bonusData.integral_end)){

			alert('红包上限不可小于于红包下限');
			return;
		}
		
		if(parseFloat($scope.util.peoplemax)>=parseFloat($scope.util.childmax)){
			if(parseFloat($scope.util.bonusData.integral_end)>parseFloat($scope.util.peoplemax)){
				alert('红包上限不可大于利润最大值');
			return;						
			}
		}else{
			if(parseFloat($scope.util.bonusData.integral_end)>parseFloat($scope.util.childmax)){
				alert('红包上限不可大于利润最大值');
				return;						
			}
		}
		$resource('/api/ac/lc/productProfitService/save', {}, {}).save($scope.result, function(res){
			if(res.errcode === 0){
				alert('保存成功');
				$modalInstance.dismiss('cancel'); 
			}else{
				alert(res.errmsg);
			}
		});
	};
};

