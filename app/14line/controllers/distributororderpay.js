/**
 * 模态框
 */
 module.exports = function($scope, $state, $stateParams, $modalInstance, items, $resource){
   
           
            console.log(items);
            if(items.pay_state_name=='已支付'){
                $scope.show=1;
            }else{
                $scope.show=0;
            }
            // console.log('上面是items');
            $scope.a=items.a;
            $scope.product_type = items.product_type;
            $scope.distributor_order={};
            var  savestock;
            var d = new Date();
            var dy=d.getFullYear();
            var dm=d.getMonth()+1;
            var dd=d.getDate();
            $scope.distributor_order.adults=[];
            $scope.distributor_order.childs=[];
            $scope.distributor_order.savedata=[];
            $scope.showdata={};
            $scope.distributor_order.numberAdult=items.adult_number;
            //正则判断身份证
            var aCity={11:"北京",12:"天津",13:"河北",14:"山西",15:"内蒙古",21:"辽宁",22:"吉林",23:"黑龙江",31:"上海",32:"江苏",33:"浙江",34:"安徽",35:"福建",36:"江西",37:"山东",41:"河南",42:"湖北",43:"湖南",44:"广东",45:"广西",46:"海南",50:"重庆",51:"四川",52:"贵州",53:"云南",54:"西藏",61:"陕西",62:"甘肃",63:"青海",64:"宁夏",65:"新疆",71:"台湾",81:"香港",82:"澳门",91:"国外"}; 
            var checkidcardno=function(sId){
                    var iSum=0 ;
                    var info="" ;
                    if(!/^\d{17}(\d|x)$/i.test(sId)) return "您输入的身份证长度或格式错误";
                    sId=sId.replace(/x$/i,"a");
                    if(aCity[parseInt(sId.substr(0,2))]==null) return "您的身份证地区非法";
                    sBirthday=sId.substr(6,4)+"-"+Number(sId.substr(10,2))+"-"+Number(sId.substr(12,2));
                    var d=new Date(sBirthday.replace(/-/g,"/")) ;
                    if(sBirthday!=(d.getFullYear()+"-"+ (d.getMonth()+1) + "-" + d.getDate()))return "身份证上的出生日期非法";
                    for(var i = 17;i>=0;i --) iSum += (Math.pow(2,i) % 11) * parseInt(sId.charAt(17 - i),11) ;
                    if(iSum%11!=1) return "您输入的身份证号非法";
                    //aCity[parseInt(sId.substr(0,2))]+","+sBirthday+","+(sId.substr(16,1)%2?"男":"女");//此次还可以判断出输入的身份证号的人性别
                    return true;
            }
            //正则护照
            var checkpassport=function(x){
                if(!/^(P\d{7})|(G\d{8})$/i.test(x)) return "您输入的护照号长度或格式错误";
            }
            //正则判断军官
            var checkofficer=function(x){
                if(!/^[a-zA-Z0-9]{7,21}$/i.test(x)) return "您输入的军官证号长度或格式错误";
            }
            //正则判断港澳台
            var checkgat=function(x){
                if(!/^[HMhm]{1}([0-9]{10}|[0-9]{8})$/i.test(x)) return "您输入的证件号长度或格式错误";
            }
            //正则判断电话号
            var checkphone=function(x){
                if(!/^(\(\d{3,4}\)|\d{3,4}-|\s)?\d{7,14}$/i.test(x)) return "您输入的电话号长度或格式错误";
            }
            //正则判断台胞证
            var checkt=function(x){
                if(!/^[a-zA-Z0-9]{8}$/i.test(x) || /^[a-zA-Z0-9]{10}$/i.test(x)) return "您输入的台胞证号长度或格式错误";
            }


			     $resource('/api/ac/lc/lineOrderListService/findOrderInfo', {}, {}).
                			get({order_code : items.order_code,product_id : items.product_id}, function(res){
                                // console.log(res.data.touristList);                               
                                if(res.errcode === 0 || res.errcode === 10003){
                                    savestock={main_mobile:res.data.mobile,
                                               order_code:items.order_code
                                              };          
                                    var count=0;
                                    var ty=true;                                   
                                    //使用计数器判断成人和儿童
                                    for(var i=0;i<res.data.touristList.length;i++){
                                        if(res.data.touristList[i].tourist_type==undefined && count<items.adult_number){
                                            count++;
                                            ty=false;                                            
                                            var json={}
                                            json.name="";
                                            if($scope.product_type=='4'||$scope.product_type=='5'||$scope.product_type=='3'){
                                                json.card_type="2";
                                            } else{
                                                json.card_type="1";
                                            }                                          
                                            
                                            json.cardno="";
                                            json.mobile="";
                                            json.id=res.data.touristList[i].id;
                                            json.tourist_type="1";
                                            json.certificate_yesr=dy;
                                            json.certificate_month=dm;
                                            json.certificate_day=dd;
                                            json.birthday_year=dy;
                                            json.birthday_mouth=dm;
                                            json.birthday_day=dd;
                                            json.sex="1";
                                            json.cardinfo="";
                                            json.phoneinfo="";
                                            json.showphoneinfo=function(x,index){
                                                 if(x==undefined){
                                                    return;
                                                }
                                                if(x==''){
                                                    return;
                                                }
                                                //alert(x);
                                                if(checkphone(x)=="您输入的电话号长度或格式错误"){
                                                    $scope.distributor_order.adults[index].phoneinfo="您输入的电话号长度或格式错误"
                                                }else{
                                                    $scope.distributor_order.adults[index].phoneinfo="";
                                                }
                                            }
                                            json.showcardno=function(x,index){
                                                 if(x==undefined){
                                                    return;
                                                }
                                                if(x==''){
                                                    return;
                                                }
                                                
                                               
                                                if($scope.distributor_order.adults[index].card_type=="1" && $scope.distributor_order.adults[index].tourist_type=="1"){                                                    
                                                    if(checkidcardno(x)=="您输入的身份证长度或格式错误"){
                                                        $scope.distributor_order.adults[index].cardinfo="您输入的身份证长度或格式错误";
                                                    };
                                                    if(checkidcardno(x)=="您的身份证地区非法"){
                                                        $scope.distributor_order.adults[index].cardinfo="您的身份证地区非法";
                                                    };
                                                    if(checkidcardno(x)=="身份证上的出生日期非法"){
                                                        $scope.distributor_order.adults[index].cardinfo="身份证上的出生日期非法";
                                                    };
                                                    if(checkidcardno(x)=="您输入的身份证号非法"){
                                                        $scope.distributor_order.adults[index].cardinfo="您输入的身份证号非法";
                                                    };
                                                    if(checkidcardno(x)==true){
                                                        $scope.distributor_order.adults[index].cardinfo="";
                                                    };
                                                }
                                            //alert($scope.distributor_order.adults[index].card_type);
                                            if($scope.distributor_order.adults[index].card_type=="2" && $scope.distributor_order.adults[index].tourist_type=="1"){
                                                if(checkpassport(x)=="您输入的护照号长度或格式错误"){
                                                    $scope.distributor_order.adults[index].cardinfo="您输入的护照号长度或格式错误";
                                                }else{
                                                    $scope.distributor_order.adults[index].cardinfo="";
                                                }
                                            }
                                            if($scope.distributor_order.adults[index].card_type=="3" && $scope.distributor_order.adults[index].tourist_type=="1"){
                                                if(checkgat(x)=="您输入的证件号长度或格式错误"){
                                                    $scope.distributor_order.adults[index].cardinfo="您输入的证件号长度或格式错误";
                                                }else{
                                                    $scope.distributor_order.adults[index].cardinfo="";
                                                }
                                            }
                                            if($scope.distributor_order.adults[index].card_type=="5" && $scope.distributor_order.adults[index].tourist_type=="1"){
                                                if(checkt(x)=="您输入的台胞证号长度或格式错误"){
                                                    $scope.distributor_order.adults[index].cardinfo="您输入的台胞证号长度或格式错误";
                                                }else{
                                                    $scope.distributor_order.adults[index].cardinfo="";
                                                }
                                            }
                                            if($scope.distributor_order.adults[index].card_type=="6" && $scope.distributor_order.adults[index].tourist_type=="1"){
                                                if(checkofficer(x)=="您输入的军官证号长度或格式错误"){
                                                    $scope.distributor_order.adults[index].cardinfo="您输入的军官证号长度或格式错误";
                                                }else{
                                                    $scope.distributor_order.adults[index].cardinfo="";
                                                }
                                            }    
                                            }
                                            json.clear=function(index){
                                                if($scope.show==1){
                                                    return;
                                                }  
                                                $scope.distributor_order.adults[index].name="";                                            
                                                $scope.distributor_order.adults[index].card_type="1";
                                                $scope.distributor_order.adults[index].cardno="";
                                                $scope.distributor_order.adults[index].mobile="";
                                                //json.id=res.data.touristList[i].id;
                                                $scope.distributor_order.adults[index].tourist_type="1";
                                                $scope.distributor_order.adults[index].certificate_yesr=dy;
                                                $scope.distributor_order.adults[index].certificate_month=dm;
                                                $scope.distributor_order.adults[index].certificate_day=dd;
                                                $scope.distributor_order.adults[index].birthday_year=dy;
                                                $scope.distributor_order.adults[index].birthday_mouth=dm;
                                                $scope.distributor_order.adults[index].birthday_day=dd;
                                                $scope.distributor_order.adults[index].sex="1";
                                                $scope.distributor_order.adults[index].cardinfo="";
                                                $scope.distributor_order.adults[index].phoneinfo="";
                                                cadayS(json.birthday_day,json.birthday_mouth);
                                                cadayS(json.certificate_yesr,json.certificate_mouth);
                                            }
                                            cadayS(json.birthday_day,json.birthday_mouth);
                                            cadayS(json.certificate_yesr,json.certificate_mouth);
                                            $scope.distributor_order.adults.push(json);
                                            $scope.distributor_order.savedata.push(json);
                                        }
                                        if(res.data.touristList[i].tourist_type==undefined && count==items.adult_number && ty==true){
                                            var json={}
                                            json.name="";                                            
                                            if($scope.product_type=='4'||$scope.product_type=='5'||$scope.product_type=='3'){
                                                json.card_type="2";
                                            } else{
                                                json.card_type="1";
                                            } 
                                            json.cardno="";
                                            json.mobile="";
                                            json.id=res.data.touristList[i].id;
                                            json.tourist_type="0";
                                            json.certificate_yesr=dy;
                                            json.certificate_month=dm;
                                            json.certificate_day=dd;
                                            json.birthday_year=dy;
                                            json.birthday_mouth=dm;
                                            json.birthday_day=dd;
                                            json.sex="1";
                                            json.cardinfo="";
                                            json.phoneinfo="";
                                            json.showcardno=function(x,index){
                                                 if(x==undefined){
                                                    return;
                                                }
                                                if(x==''){
                                                    return;
                                                }
                                                
                                                //alert($scope.distributor_order.childs[index].card_type);
                                                if($scope.distributor_order.childs[index].card_type=="1" && $scope.distributor_order.childs[index].tourist_type=="0"){
                                                    if(checkidcardno(x)=="您输入的身份证长度或格式错误"){
                                                        $scope.distributor_order.childs[index].cardinfo="您输入的身份证长度或格式错误";
                                                        alert($scope.distributor_order.childs[index].cardinfo);
                                                    };
                                                    if(checkidcardno(x)=="您的身份证地区非法"){
                                                        $scope.distributor_order.childs[index].cardinfo="您的身份证地区非法";
                                                    };
                                                    if(checkidcardno(x)=="身份证上的出生日期非法"){
                                                        $scope.distributor_order.childs[index].cardinfo="身份证上的出生日期非法";
                                                    };
                                                    if(checkidcardno(x)=="您输入的身份证号非法"){
                                                        $scope.distributor_order.childs[index].cardinfo="您输入的身份证号非法";
                                                    };
                                                    if(checkidcardno(x)==true){
                                                        var age=dy-x.substring(6, 10);
                                                        //alert(age);
                                                        if(age<12 && age>2){
                                                            $scope.distributor_order.childs[index].cardinfo="";
                                                        }else{
                                                            $scope.distributor_order.childs[index].cardinfo="你的年龄不在在2~12岁之间，不属于儿童";
                                                        }
                                                    };
                                                 };
                                                 if($scope.distributor_order.childs[index].card_type=="2" && $scope.distributor_order.childs[index].tourist_type=="0"){
                                                    if(checkpassport(x)=="您输入的护照号长度或格式错误"){
                                                        $scope.distributor_order.childs[index].cardinfo="您输入的护照号长度或格式错误";
                                                    }else{
                                                    $scope.distributor_order.childs[index].cardinfo="";
                                                }
                                                }
                                                if($scope.distributor_order.childs[index].card_type=="3" && $scope.distributor_order.childs[index].tourist_type=="0"){
                                                    if(checkgat(x)=="您输入的证件号长度或格式错误"){
                                                        $scope.distributor_order.childs[index].cardinfo="您输入的证件号长度或格式错误";
                                                    }else{
                                                    $scope.distributor_order.childs[index].cardinfo="";
                                                }
                                                }
                                                if($scope.distributor_order.childs[index].card_type=="5" && $scope.distributor_order.childs[index].tourist_type=="0"){
                                                    if(checkt(x)=="您输入的台胞证号长度或格式错误"){
                                                    $scope.distributor_order.childs[index].cardinfo="您输入的台胞证号长度或格式错误";
                                                    }else{
                                                        $scope.distributor_order.childs[index].cardinfo="";
                                                    }
                                                }
                                                if($scope.distributor_order.childs[index].card_type=="6" && $scope.distributor_order.childs[index].tourist_type=="0"){
                                                    if(checkofficer(x)=="您输入的军官证号长度或格式错误"){
                                                        $scope.distributor_order.childs[index].cardinfo="您输入的军官证号长度或格式错误";
                                                    }else{
                                                    $scope.distributor_order.adults[index].cardinfo="";
                                                }
                                                } 
                                                                                                                                                   
                                            }
                                            
                                            json.clear=function(index){
                                                if($scope.show==1){
                                                    return;
                                                }  
                                                $scope.distributor_order.childs[index].name="";                                            
                                                $scope.distributor_order.childs[index].card_type="1";
                                                $scope.distributor_order.childs[index].cardno="";
                                                $scope.distributor_order.childs[index].mobile="";
                                                //json.id=res.data.touristList[i].id;
                                                $scope.distributor_order.childs[index].tourist_type="0";
                                                $scope.distributor_order.childs[index].certificate_yesr=dy;
                                                $scope.distributor_order.childs[index].certificate_month=dm;
                                                $scope.distributor_order.childs[index].certificate_day=dd;
                                                $scope.distributor_order.childs[index].birthday_year=dy;
                                                $scope.distributor_order.childs[index].birthday_mouth=dm;
                                                $scope.distributor_order.childs[index].birthday_day=dd;
                                                $scope.distributor_order.childs[index].sex="1";
                                                $scope.distributor_order.childs[index].cardinfo="";
                                                $scope.distributor_order.childs[index].phoneinfo="";
                                                cadayS(json.birthday_day,json.birthday_mouth);
                                                cadayS(json.certificate_yesr,json.certificate_mouth);
                                            }
                                            cadayS(json.birthday_day,json.birthday_mouth);
                                            cadayS(json.certificate_yesr,json.certificate_mouth);
                                            $scope.distributor_order.childs.push(json);
                                            $scope.distributor_order.savedata.push(json);
                                        } 
                                        ty=true;
                                        if(res.data.touristList[i].tourist_type=='成人' || res.data.touristList[i].tourist_type=="1"){ 
                                            //alert(res.data.touristList[i].tourist_type); 
                                               res.data.touristList[i].tourist_type="1";                        
                                             if(res.data.touristList[i].card_type_name=='身份证'){res.data.touristList[i].card_type_name="1"}
                                             if(res.data.touristList[i].card_type_name=='护照'){res.data.touristList[i].card_type_name="2"}
                                             if(res.data.touristList[i].card_type_name=='港澳通行证'){res.data.touristList[i].card_type_name="3"}
                                             if(res.data.touristList[i].card_type_name=='军官证'){res.data.touristList[i].card_type_name="6"}
                                             if(res.data.touristList[i].card_type_name=='台胞证'){res.data.touristList[i].card_type_name="5"}
                                             if(res.data.touristList[i].sex_name=='男'){res.data.touristList[i].sex_name="1"} 
                                             if(res.data.touristList[i].sex_name=='女'){res.data.touristList[i].sex_name="2"}                                             
                                             var json={};
                                            json.name=res.data.touristList[i].lot_name;                                            
                                            json.card_type=res.data.touristList[i].card_type_name;
                                            json.cardno=res.data.touristList[i].lot_cardno;
                                            json.mobile=res.data.touristList[i].lot_mobile;
                                            json.id=res.data.touristList[i].id;
                                            json.tourist_type=res.data.touristList[i].tourist_type;
                                            json.certificate_yesr=res.data.touristList[i].certificate_yesr;
                                            json.certificate_month=res.data.touristList[i].certificate_month;
                                            json.certificate_day=res.data.touristList[i].certificate_day;
                                            json.birthday_year=res.data.touristList[i].birthday_year;
                                            json.birthday_mouth=res.data.touristList[i].birthday_mouth;
                                            json.birthday_day=res.data.touristList[i].birthday_day;
                                            
                                            json.sex=res.data.touristList[i].sex_name;
                                            json.cardinfo="";
                                            json.phoneinfo="";                                           
                                            if(json.certificate_yesr==undefined){
                                                json.certificate_yesr=dy;
                                            }
                                            if(json.certificate_month==undefined){
                                                json.certificate_month=dm;
                                            }
                                            if(json.certificate_day==undefined){
                                                json.certificate_day=dd;
                                            }
                                            if(json.birthday_year==undefined){
                                                json.birthday_year=dy;
                                            }
                                            if(json.birthday_mouth==undefined){
                                                json.birthday_mouth=dm;
                                            }
                                            if(json.birthday_day==undefined){
                                                json.birthday_day=dd;
                                            }
                                            if(json.sex==undefined){
                                                json.sex="1";
                                            }
                                            
                                            json.showphoneinfo=function(x,index){
                                               
                                                if(x==undefined){
                                                    return;
                                                }
                                                if(x==''){
                                                    return;
                                                }
                                                if(checkphone(x)=="您输入的电话号长度或格式错误"){
                                                    $scope.distributor_order.adults[index].phoneinfo="您输入的电话号长度或格式错误"
                                                }else{
                                                    $scope.distributor_order.adults[index].phoneinfo="";
                                                }
                                            }
                                            json.showcardno=function(x,index){
                                                 if(x==undefined){
                                                    return;
                                                }
                                                if(x==''){
                                                    return;
                                                }
                                                
                                                // alert(x);
                                                //alert($scope.distributor_order.adults[index].card_type);
                                                if($scope.distributor_order.adults[index].card_type=="1" && $scope.distributor_order.adults[index].tourist_type=="1"){                                                    
                                                    if(checkidcardno(x)=="您输入的身份证长度或格式错误"){
                                                        $scope.distributor_order.adults[index].cardinfo="您输入的身份证长度或格式错误";
                                                    };
                                                    if(checkidcardno(x)=="您的身份证地区非法"){
                                                        $scope.distributor_order.adults[index].cardinfo="您的身份证地区非法";
                                                    };
                                                    if(checkidcardno(x)=="身份证上的出生日期非法"){
                                                        $scope.distributor_order.adults[index].cardinfo="身份证上的出生日期非法";
                                                    };
                                                    if(checkidcardno(x)=="您输入的身份证号非法"){
                                                        $scope.distributor_order.adults[index].cardinfo="您输入的身份证号非法";
                                                    };
                                                    if(checkidcardno(x)==true){
                                                        $scope.distributor_order.adults[index].cardinfo="";
                                                    };
                                                }
                                            //alert($scope.distributor_order.adults[index].card_type);
                                            if($scope.distributor_order.adults[index].card_type=="2" && $scope.distributor_order.adults[index].tourist_type=="1"){
                                                if(checkpassport(x)=="您输入的护照号长度或格式错误"){
                                                    $scope.distributor_order.adults[index].cardinfo="您输入的护照号长度或格式错误";
                                                }else{
                                                    $scope.distributor_order.adults[index].cardinfo="";
                                                }
                                            }
                                            if($scope.distributor_order.adults[index].card_type=="3" && $scope.distributor_order.adults[index].tourist_type=="1"){
                                                if(checkgat(x)=="您输入的证件号长度或格式错误"){
                                                    $scope.distributor_order.adults[index].cardinfo="您输入的证件号长度或格式错误";
                                                }else{
                                                    $scope.distributor_order.adults[index].cardinfo="";
                                                }
                                            }
                                            if($scope.distributor_order.adults[index].card_type=="5" && $scope.distributor_order.adults[index].tourist_type=="1"){
                                                if(checkt(x)=="您输入的台胞证号长度或格式错误"){
                                                    $scope.distributor_order.adults[index].cardinfo="您输入的台胞证号长度或格式错误";
                                                }else{
                                                    $scope.distributor_order.adults[index].cardinfo="";
                                                }
                                            }
                                            if($scope.distributor_order.adults[index].card_type=="6" && $scope.distributor_order.adults[index].tourist_type=="1"){
                                                if(checkofficer(x)=="您输入的军官证号长度或格式错误"){
                                                    $scope.distributor_order.adults[index].cardinfo="您输入的军官证号长度或格式错误";
                                                }else{
                                                    $scope.distributor_order.adults[index].cardinfo="";
                                                }
                                            }    
                                            }
                                            json.clear=function(index){
                                                if($scope.show==1){
                                                    return;
                                                }  
                                                //alert("chengren");                                               
                                                $scope.distributor_order.adults[index].name="";                                            
                                                $scope.distributor_order.adults[index].card_type="1";
                                                $scope.distributor_order.adults[index].cardno="";
                                                $scope.distributor_order.adults[index].mobile="";
                                                //json.id=res.data.touristList[i].id;
                                                $scope.distributor_order.adults[index].tourist_type="1";
                                                $scope.distributor_order.adults[index].certificate_yesr=dy;
                                                $scope.distributor_order.adults[index].certificate_month=dm;
                                                $scope.distributor_order.adults[index].certificate_day=dd;
                                                $scope.distributor_order.adults[index].birthday_year=dy;
                                                $scope.distributor_order.adults[index].birthday_mouth=dm;
                                                $scope.distributor_order.adults[index].birthday_day=dd;
                                                $scope.distributor_order.adults[index].sex="1"; 
                                                $scope.distributor_order.adults[index].cardinfo="";
                                                $scope.distributor_order.adults[index].phoneinfo="";                                              
                                                cadayS(json.birthday_year,json.birthday_mouth);
                                                cadayS(json.certificate_yesr,json.certificate_mouth);
                                            }
                                            cadayS(json.birthday_day,json.birthday_mouth);
                                            cadayS(json.certificate_yesr,json.certificate_mouth);
                                            $scope.distributor_order.adults.push(json);
                                            $scope.distributor_order.savedata.push(json);
                                         };
                                            // console.log("adults");
                                            // console.log($scope.distributor_order.adults);
                                    };

                                    
                                    for(var i=0;i<res.data.touristList.length;i++){
                                        if(res.data.touristList[i].tourist_type=='儿童' || res.data.touristList[i].tourist_type=="0"){ 
                                            res.data.touristList[i].tourist_type="0";
                                            
                                            if(res.data.touristList[i].card_type_name=='身份证'){res.data.touristList[i].card_type_name="1"}
                                             if(res.data.touristList[i].card_type_name=='护照'){res.data.touristList[i].card_type_name="2"}
                                             if(res.data.touristList[i].card_type_name=='港澳通行证'){res.data.touristList[i].card_type_name="3"}
                                             if(res.data.touristList[i].card_type_name=='军官证'){res.data.touristList[i].card_type_name="6"}
                                             if(res.data.touristList[i].card_type_name=='台胞证'){res.data.touristList[i].card_type_name="5"}
                                             if(res.data.touristList[i].sex_name=='男'){res.data.touristList[i].sex_name="1"} 
                                             if(res.data.touristList[i].sex_name=='女'){res.data.touristList[i].sex_name="2"}                                               
                                             var json={};
                                            json.name=res.data.touristList[i].lot_name;                                            
                                            json.card_type=res.data.touristList[i].card_type_name;
                                            
                                            json.cardno=res.data.touristList[i].lot_cardno;
                                            json.mobile=res.data.touristList[i].lot_mobile;
                                            json.id=res.data.touristList[i].id;
                                            json.tourist_type=res.data.touristList[i].tourist_type;
                                            json.certificate_yesr=res.data.touristList[i].certificate_yesr;
                                            json.certificate_month=res.data.touristList[i].certificate_month;
                                            json.certificate_day=res.data.touristList[i].certificate_day;
                                            json.birthday_year=res.data.touristList[i].birthday_year;
                                            json.birthday_mouth=res.data.touristList[i].birthday_mouth;
                                            json.birthday_day=res.data.touristList[i].birthday_day;
                                            json.sex=res.data.touristList[i].sex_name;
                                            json.cardinfo="";
                                            json.phoneinfo="";
                                            if(json.certificate_yesr==undefined){
                                                json.certificate_yesr=dy;
                                            }
                                            if(json.certificate_month==undefined){
                                                json.certificate_month=dm;
                                            }
                                            if(json.certificate_day==undefined){
                                                json.certificate_day=dd;
                                            }
                                            if(json.birthday_year==undefined){
                                                json.birthday_year=dy;
                                            }
                                            if(json.birthday_mouth==undefined){
                                                json.birthday_mouth=dm;
                                            }
                                            if(json.birthday_day==undefined){
                                                json.birthday_day=dd;
                                            }
                                            if(json.sex==undefined){
                                                json.sex="1";
                                            }
                                            json.clear=function(index){   
                                                if($scope.show==1){
                                                    return;
                                                }                                            
                                                $scope.distributor_order.childs[index].name="";                                            
                                                $scope.distributor_order.childs[index].card_type="1";
                                                $scope.distributor_order.childs[index].cardno="";
                                                $scope.distributor_order.childs[index].mobile="";
                                                //json.id=res.data.touristList[i].id;
                                                $scope.distributor_order.childs[index].tourist_type="0";
                                                $scope.distributor_order.childs[index].certificate_yesr=dy;
                                                $scope.distributor_order.childs[index].certificate_month=dm;
                                                $scope.distributor_order.childs[index].certificate_day=dd;
                                                $scope.distributor_order.childs[index].birthday_year=dy;
                                                $scope.distributor_order.childs[index].birthday_mouth=dm;
                                                $scope.distributor_order.childs[index].birthday_day=dd;
                                                $scope.distributor_order.childs[index].sex="1";  
                                                $scope.distributor_order.childs[index].cardinfo="";
                                                $scope.distributor_order.childs[index].phoneinfo="";                                            
                                                cadayS(json.birthday_day,json.birthday_mouth);
                                                cadayS(json.certificate_yesr,json.certificate_mouth);
                                            }
                                            json.showcardno=function(x,index){
                                                 if(x==undefined){
                                                    return;
                                                }
                                                if(x==''){
                                                    return;
                                                }
                                                
                                                //alert($scope.distributor_order.childs[index].card_type);
                                                if($scope.distributor_order.childs[index].card_type=="1" && $scope.distributor_order.childs[index].tourist_type=="0"){
                                                    if(checkidcardno(x)=="您输入的身份证长度或格式错误"){
                                                        $scope.distributor_order.childs[index].cardinfo="您输入的身份证长度或格式错误";
                                                        alert($scope.distributor_order.childs[index].cardinfo);
                                                    };
                                                    if(checkidcardno(x)=="您的身份证地区非法"){
                                                        $scope.distributor_order.childs[index].cardinfo="您的身份证地区非法";
                                                    };
                                                    if(checkidcardno(x)=="身份证上的出生日期非法"){
                                                        $scope.distributor_order.childs[index].cardinfo="身份证上的出生日期非法";
                                                    };
                                                    if(checkidcardno(x)=="您输入的身份证号非法"){
                                                        $scope.distributor_order.childs[index].cardinfo="您输入的身份证号非法";
                                                    };
                                                    if(checkidcardno(x)==true){
                                                        var age=dy-x.substring(6, 10);
                                                        //alert(age);
                                                        if(age<12 && age>2){
                                                            $scope.distributor_order.childs[index].cardinfo="";
                                                        }else{
                                                            $scope.distributor_order.childs[index].cardinfo="你的年龄不在在2~12岁之间，不属于儿童";
                                                        }
                                                    };
                                                 };
                                                 if($scope.distributor_order.childs[index].card_type=="2" && $scope.distributor_order.childs[index].tourist_type=="0"){
                                                    if(checkpassport(x)=="您输入的护照号长度或格式错误"){
                                                        $scope.distributor_order.childs[index].cardinfo="您输入的护照号长度或格式错误";
                                                    }else{
                                                    $scope.distributor_order.childs[index].cardinfo="";
                                                }
                                                }
                                                if($scope.distributor_order.childs[index].card_type=="3" && $scope.distributor_order.childs[index].tourist_type=="0"){
                                                    if(checkgat(x)=="您输入的证件号长度或格式错误"){
                                                        $scope.distributor_order.childs[index].cardinfo="您输入的证件号长度或格式错误";
                                                    }else{
                                                    $scope.distributor_order.childs[index].cardinfo="";
                                                }
                                                }
                                                if($scope.distributor_order.childs[index].card_type=="5" && $scope.distributor_order.childs[index].tourist_type=="0"){
                                                    if(checkt(x)=="您输入的台胞证号长度或格式错误"){
                                                    $scope.distributor_order.childs[index].cardinfo="您输入的台胞证号长度或格式错误";
                                                    }else{
                                                        $scope.distributor_order.childs[index].cardinfo="";
                                                    }
                                                }
                                                if($scope.distributor_order.childs[index].card_type=="6" && $scope.distributor_order.childs[index].tourist_type=="0"){
                                                    if(checkofficer(x)=="您输入的军官证号长度或格式错误"){
                                                        $scope.distributor_order.childs[index].cardinfo="您输入的军官证号长度或格式错误";
                                                    }else{
                                                    $scope.distributor_order.adults[index].cardinfo="";
                                                }
                                                } 
                                                                                                                                                   
                                            }
                                            cadayS(json.birthday_day,json.birthday_mouth);
                                            cadayS(json.certificate_yesr,json.certificate_mouth);
                                            $scope.distributor_order.childs.push(json);                                          
                                            $scope.distributor_order.savedata.push(json);                                            
                                        };
                                        // console.log("childs");
                                        // console.log( $scope.distributor_order.childs);
                                    };                                   
                                    savestock.list=$scope.distributor_order.savedata;       	
                                }else{
                                    alert(res.errmsg);
                                }
                            });
            $scope.change_ad=function(index){
                $scope.distributor_order.adults[index].cardno="";
                $scope.distributor_order.adults[index].cardinfo="";
            }
            $scope.change_ch=function(index){
                $scope.distributor_order.childs[index].cardno="";
                $scope.distributor_order.adults[index].cardinfo="";
            }
            //证件类型                			
            
            if($scope.product_type=='4'||$scope.product_type=='5'||$scope.product_type=='3') {
                
                 $scope.showinfo = {'card_type' : [					                            
                                                {'name' : '护照', 'value' : '2'},                                                
	                                        ],}
            } else{
                $scope.showinfo = {'card_type' : [
					                            {'name' : '身份证', 'value' : '1'},
                                                {'name' : '护照', 'value' : '2'},
                                                {'name' : '港澳通行证', 'value' : '3'},
                                                {'name' : '军官证', 'value' : "6"},
                                                {'name' : '台胞证', 'value' : '5'},
	                                        ],}
            }                            
           
            //出生日期和证件有效期日期下拉框
            $scope.showdata.cyears=[];
            $scope.showdata.byears=[];
            $scope.showdata.cmonths=[];
            $scope.showdata.cdays=[];
            var cm;
            var cadayS=function(cay,cam){
                if( (cay % 4 == 0 && cay % 100 != 0 )|| (cay % 400 == 0)){
                        if(cam==2){
                             $scope.showdata.cdays=[];
                            for(var i=1;i<30;i++){                                                                      
                                $scope.showdata.cdays.push({code:i,value:i});
                            }
                        }
                        if(cam==1 || cam==3 || cam==5 || cam== 7 || cam==8 || cam==10 || cam==12){
                             $scope.showdata.cdays=[];
                            for(var i=1;i<32;i++){                                                                      
                                $scope.showdata.cdays.push({code:i,value:i});
                            }
                        }
                        if(cam==4 || cam==6 || cam==9 || cam== 11 ){
                            $scope.showdata.cdays=[]; 
                            for(var i=1;i<31;i++){                                                                      
                                $scope.showdata.cdays.push({code:i,value:i});
                            }
                        }
                    }else{
                        if(cam==2){
                           $scope.showdata.cdays=[];
                            for(var i=1;i<29;i++){                                                                      
                                $scope.showdata.cdays.push({code:i,value:i});
                            }
                        }
                        if(cam==1 || cam==3 || cam==5 || cam== 7 || cam==8 || cam==10 || cam==12){
                            $scope.showdata.cdays=[]; 
                            for(var i=1;i<32;i++){                                                                      
                                $scope.showdata.cdays.push({code:i,value:i});
                            }
                        }
                        if(cam==4 || cam==6 || cam==9 || cam== 11 ){
                            $scope.showdata.cdays=[];
                            for(var i=1;i<31;i++){                                                                       
                                $scope.showdata.cdays.push({code:i,value:i});
                            }
                        }
                    }
            }           
            for(var i=0;i<=20;i++){
                var cy=dy-10+i;               
                $scope.showdata.cyears.push({code:cy,value:cy})
            }
            for(var i=0;i<=90;i++){
                var cy=dy-90+i;               
                $scope.showdata.byears.push({code:cy,value:cy})
            } 
            for(var i=1;i<13;i++){                                  
                    $scope.showdata.cmonths.push({code:i,value:i})
            }          
            $scope.distributor_order.cy=function(cy){
                $scope.showdata.cmonths=[];
                for(var i=1;i<13;i++){                                  
                    $scope.showdata.cmonths.push({code:i,value:i})
                }
                cadayS(cy,cm);
                $scope.distributor_order.cm=function(cm){
                    cadayS(cy,cm);                                       
                }
            }
        //取消
            $scope.cancel = function () {
                // if(confirm("是否确认关闭？")){
                    $modalInstance.dismiss('cancel');
                // }  
                 
            };
            //保存
            $scope.save = function () {
                if(confirm("是否确认保存？")){                
                
                    for(var i=0;i<$scope.distributor_order.adults.length;i++){                      
                            if($scope.distributor_order.adults[i].cardinfo!=""){
                                alert("证件格式不正确");
                                return;
                            }
                            
                       
                    }
                    for(var i=0;i<$scope.distributor_order.childs.length;i++){
                        if($scope.distributor_order.childs[i].cardinfo!=""){
                                alert("证件格式不正确");
                                return;
                            }
                    }
                    for(var i=0;i<$scope.distributor_order.adults.length;i++){
                       // if($scope.distributor_order.adults[i].card_type=="1"){
                            if($scope.distributor_order.adults[i].phoneinfo!=""){
                                alert("手机号格式不正确");
                                return;
                            }                            
                        //}
                    }
                    // console.log(savestock);
                    for(var i=0;i<savestock.list.length;i++){
                        var json={};

                          
                        if((savestock.list[i].name==''||savestock.list[i].name==undefined||savestock.list[i].name=='null')&&(savestock.list[i].cardno==''||savestock.list[i].cardno==undefined||savestock.list[i].cardno=='null')&&(savestock.list[i].mobile==''||savestock.list[i].mobile==undefined||savestock.list[i].mobile=='null')){
                        }
                      else{
                                
                        
                            if(savestock.list[i].card_type=="1"){
                            

                                if(savestock.list[i].tourist_type=='1'){
                            
                                    
                                delete savestock.list[i].certificate_yesr;
                                delete savestock.list[i].certificate_month;
                                delete savestock.list[i].certificate_day;
                                delete savestock.list[i].birthday_year;
                                delete savestock.list[i].birthday_mouth;
                                delete savestock.list[i].birthday_day;
                                delete savestock.list[i].sex;
                                if((savestock.list[i].name == ''||savestock.list[i].name == undefined )|| (savestock.list[i].cardno == ''||savestock.list[i].cardno == undefined )|| (savestock.list[i].mobile == ''||savestock.list[i].mobile == undefined )){
                                    alert('第'+(i+1)+'条人员信息未填写完整');
                                    return;
                                }
                                }
                                if(savestock.list[i].tourist_type=='0'){
                                delete savestock.list[i].certificate_yesr;
                                delete savestock.list[i].certificate_month;
                                delete savestock.list[i].certificate_day;
                                delete savestock.list[i].birthday_year;
                                delete savestock.list[i].birthday_mouth;
                                delete savestock.list[i].birthday_day;
                                delete savestock.list[i].sex;
                                delete savestock.list[i].mobile;
                                if((savestock.list[i].name == ''||savestock.list[i].name == undefined ) || (savestock.list[i].cardno == ''||savestock.list[i].cardno == undefined ) ){
                                    alert('第'+(i+1)+'条人员信息未填写完整');
                                    return;
                                }    
                                }
                            }
                            if(savestock.list[i].card_type=="5" || savestock.list[i].card_type=="6"){
                                if(savestock.list[i].tourist_type=='1'){
                                delete savestock.list[i].certificate_yesr;
                                delete savestock.list[i].certificate_month;
                                delete savestock.list[i].certificate_day;
                                if((savestock.list[i].name == ''||savestock.list[i].name == undefined )|| (savestock.list[i].cardno == ''||savestock.list[i].cardno == undefined )){
                                    alert('第'+(i+1)+'条人员信息未填写完整');
                                    return;
                                }
                            }
                                if(savestock.list[i].tourist_type=='0'){
                                delete savestock.list[i].certificate_yesr;
                                delete savestock.list[i].certificate_month;
                                delete savestock.list[i].certificate_day;
                                delete savestock.list[i].mobile;
                                if((savestock.list[i].name == ''||savestock.list[i].name == undefined )|| (savestock.list[i].cardno == ''||savestock.list[i].cardno == undefined ) ){
                                    alert('第'+(i+1)+'条人员信息未填写完整');
                                    return;
                                }
                            }
                        }
                        if(savestock.list[i].card_type=="3"||savestock.list[i].card_type=="2" ){
                                if(savestock.list[i].tourist_type=='1'){
 
                                if((savestock.list[i].name == ''||savestock.list[i].name == undefined )|| (savestock.list[i].cardno == ''||savestock.list[i].cardno == undefined ) ){
                                    alert('第'+(i+1)+'条人员信息未填写完整');
                                    return;
                                }
                            }
                                if(savestock.list[i].tourist_type=='0'){
                               
                                if((savestock.list[i].name == ''||savestock.list[i].name == undefined ) || (savestock.list[i].cardno == ''||savestock.list[i].cardno == undefined ) ){
                                    alert('第'+(i+1)+'条人员信息未填写完整');
                                    return;
                                }
                            }
                        
                        }
                            }
                        
                    }
                   
                    for(var zz = 0;zz<savestock.list.length;zz++){
                        if(savestock.list[zz].name==undefined){
                           savestock.list[zz].name='';
                        }
                         if(savestock.list[zz].cardno==undefined){
                           savestock.list[zz].cardno='';
                        }
                         if(savestock.list[zz].mobile==undefined){
                           savestock.list[zz].mobile='';
                        }
                       
                    }
                    
                    
                      if(items.pay_state_name=='已支付'){
                          alert('该订单已支付，无法更改人员信息');
                          return;
                      }                    
                     $resource('/api/ac/lc/disTouristService/update', {}, {}).save(savestock, function(res){
                        // console.log(res);
                        if(res.errcode === 0){
                         
                                $modalInstance.close();
                                    alert('保存成功');   
                        }else{
                            alert(res.errmsg);
                        }
                    });
             
              };
            };
            //支付
            $scope.gogogo = function () {
                if(confirm("是否确认支付？")){                
                if($scope.messageinfo.$valid){
                   
                    for(var i=0;i<$scope.distributor_order.adults.length;i++){
                       
                            if($scope.distributor_order.adults[i].cardinfo!=""){
                                alert("证件格式不正确");
                                return;
                            }
                            
                       
                    }
                    for(var i=0;i<$scope.distributor_order.childs.length;i++){
                        if($scope.distributor_order.childs[i].cardinfo!=""){
                                alert("证件格式不正确");
                                return;
                            }
                    }
                    for(var i=0;i<$scope.distributor_order.adults.length;i++){
                       // if($scope.distributor_order.adults[i].card_type=="1"){
                            if($scope.distributor_order.adults[i].phoneinfo!=""){
                                alert("手机号格式不正确");
                                return;
                            }                            
                        //}
                    }
                   
                    for(var i=0;i<savestock.list.length;i++){
                        var json={};
                        if(savestock.list[i].card_type=="1"){
                            delete savestock.list[i].certificate_yesr;
                            delete savestock.list[i].certificate_month;
                            delete savestock.list[i].certificate_day;
                            delete savestock.list[i].birthday_yesr;
                            delete savestock.list[i].birthday_month;
                            delete savestock.list[i].birthday_day;
                            delete savestock.list[i].sex;
                        }
                        if(savestock.list[i].card_type=="5" || savestock.list[i].card_type=="6"){
                            delete savestock.list[i].certificate_yesr;
                            delete savestock.list[i].certificate_month;
                            delete savestock.list[i].certificate_day;
                        }
                    }
                   
                           $resource('/api/ac/lc/lineDistributorOrderService/createOrderByDistributor', {}, {}).save(savestock, function(res){
                               if(res.errcode === 0){
                                    $modalInstance.close();
                                    alert('支付成功');
                               }else{
                                   alert(res.errmsg);
                               }
                           });
                           
                    
                }else{
                    alert('请将信息添加完整');
                    return;
                }   
              };
              };
        };
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
