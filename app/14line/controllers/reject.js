module.exports = function($scope, $state, $stateParams, $modalInstance, items, $resource){
    $scope.back = {};
    $scope.item=items;
    if($scope.item.wjq=='wjq'){
        $scope.url='/api/ac/lc/lineRejectDistributorOrderService/updatePayBack';
        $scope.wjq1=1;
    }else if($scope.item.wjq=='wjq1'){
        $scope.url='/api/as/lc/distributorBackOrder/updateRejectPayBack';
        $scope.wjq1=1;
    }
    else{
         $scope.url='/api/ac/lc/lineRejectOrderService/updateOrderBack';
        $scope.wjq1=0;
    }
    $scope.cancel = function () {  
        $modalInstance.dismiss('cancel'); 
    };
    $scope.gogogo = function () {
        if($scope.linereject.$valid){
            //alert('通过验证可以提交表单');
        }else{
            //alert('表单没有通过验证');
            return;
        }
        if(items.from_app_id=='juyou_yc'){
            if(items.call_money<$scope.back.money){
            alert('退款金额不能大于实际支付金额');
            return;
        }
        }
        if(items.money<$scope.back.money){
            alert('退款金额不能大于实际支付金额');
            return;
        }
        
        items['cancel_reason']=$scope.qwe;
        items['pay_back_fee']=$scope.back.money;
        items['pay_fee']=items.money;
        items['back_remarks']=$scope.qwe;
        items['reject_remark']=$scope.qwe;
        console.log($scope.back.money);
        console.log(items);
        $resource($scope.url, {}, {}).save(items, function(res){
            // console.log(res);
            if(res.errcode === 0){
                $modalInstance.close($scope.result);
                alert('驳回/退款成功');
            }else{
                alert(res.errmsg);
                $modalInstance.close($scope.result);
            }
        });
    };
};