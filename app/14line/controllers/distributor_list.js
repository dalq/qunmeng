/**
 * 模态框
 */
module.exports = function ($scope, $state, $stateParams,$resource,$modal) {
	/* 分页
     * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.currentPage = 1;      //当前页码
    $scope.itemsPerPage = 10         //每页显示几条
    $scope.a=[];


	$scope.pageChanged = function () {
       var para = {
        //    ta:1,
            pageNo:$scope.currentPage,
            pageSize:$scope.itemsPerPage,
            product_id : $scope.id,
            product_name : $scope.product_name,
            product_type : $scope.a.product_type,
            supplier_product_name : $scope.supplier_product_name,
            start_city_arr : $scope.start_city_arr,
            product_state : '2'
            
        };
        		// console.log(para);

        $resource('/api/as/lc/product/findinfolist', {}, {}).
			save(para, function(res){
			
                if(res.errcode === 0 || res.errcode === 10003){
					$scope.a=res.data.results;
					$scope.totalItems = res.data.totalRecord;

                }else{
                    alert(res.errmsg);
                }
            });

    };
	$scope.pageChanged();


			$scope.b= '0';
			$scope.c = '';
			$scope.show = function(index){
				if($scope.b==0){
					$scope.b='1',
					$scope.c=index
				}else if($scope.b==1){
					if($scope.c!=index){
						$scope.c=index
					}else{
						$scope.b='0',
						$scope.c=index
					}
				}
			};



			$scope.search=function(){
			var dic = {

				product_id : $scope.id,
				product_name : $scope.product_name,
				product_type : $scope.a.product_type,
				supplier_product_name : $scope.supplier_product_name,
				start_city_arr : $scope.start_city_arr,
                product_state : '2',
				
			}
						 console.log(dic);
				$resource('/api/as/lc/product/findinfolist', {}, {}).save(dic, function(res){
                if(res.errcode === 0 || res.errcode === 10003){
                    // console.log(res);
					$scope.a=res.data.results;
                    $scope.totalItems = res.data.totalRecord;
                }else{
                    alert(res.errmsg);
                }
            });
			}






                //到详情
            	$scope.toinfo = function (index) {
				$scope.item = $scope.a[index];
                $scope.item['cc'] = '1';
				var modalInstance = $modal.open({
					template: require('../views/checklistinfo.html'),
					controller: 'linechecklistinfo',
					size: 'lg',
					resolve: {
						items: function () {
							return $scope.item;
						}
					}
				});
				modalInstance.result.then(function (showResult) {	
						$scope.pageChanged();
				});
			}

         


            $scope.toorder = function (index) {
                $scope.item = $scope.a[index];
				$scope.item['wjq'] = 'wjq';
				var modalInstance = $modal.open({
					template: require('../views/toorder.html'),
					controller: 'linetoorder',
					size: 'lg',
					resolve: {
						items: function () {
							return $scope.item;
						}
					}
				});
				modalInstance.result.then(function (showResult) {	
						$scope.pageChanged();
				});
			}

              //申请退款
			 $scope.backmoney = function () {                
                var modalInstance = $modal.open({
                    template: require('../views/reject.html'),
                    controller: 'linereject',
                    size: 'lg',
                    resolve: {
                        items: function () {
                            // $modalInstance.close($scope.result);
                            return $scope.obj;
                        }
                    }
                });
                 modalInstance.result.then(function (showResult) {
                    $modalInstance.close($scope.result);
		        });
            }

$scope.leixing=[{name:'未选择',value:''}, 
                {name:'周边',value:'0'},
                {name:'国内长线',value:'1'},
                {name:'当地',value:'2'}  ,
                {name:'出境当地参团',value:'3'} ,    
                {name:'出境短线',value:'4'},   
                {name:'出境长线',value:'5'}   
              
            ]





};

