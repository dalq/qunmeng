module.exports = function ($scope, tableconfig, $state, $resource, $modal) {

	tableconfig.start($scope, {
		'url': '/api/ac/lc/sendMessageService/findSendMessageList',
		'col': [
			{ 'title': '产品编号', 'col': 'product_id' },
			{ 'title': '产品名称', 'col': 'product_name' },
			{ 'title': '供应商产品名称', 'col': 'supplier_product_name' },
			{ 'title': '团期', 'col': 'tour_date' },
			{ 'title': '发送状态', 'col': 'send_state_name' },
			{ 'title': '操作', 'col': 'btn' },
		],
		'btn': [
			{ 'title': '导游信息', 'onclick': 'reject' },
			{ 'title': '发送短信', 'onclick': 'gomessage' },
			{ 'title': '再次发送短信', 'onclick': 'gotwomessage' },
			{ 'title': '人员信息', 'onclick': 'touristMessage' },
		],
		'search': [
			{ 'title': '产品编号', 'type': 'txt', 'name': 'product_id', 'show': true },
			{ 'title': '主名称', 'type': 'txt', 'name': 'product_name', 'show': true },
			{ 'title': '副名称', 'type': 'txt', 'name': 'product_sub_name' },
			{ 'title': '出发城市', 'type': 'txt', 'name': 'start_city_arr' },
			{ 'title': '目的城市', 'type': 'txt', 'name': 'end_city' },
			{ 'title': '产品状态', 'type': 'txt', 'name': 'product_state_name' },


		],
		'title': '发送短信列表',

	});
	$scope.gomessage = function (item) {
		if(!confirm("确认发送短信？")){
			return ;
		}
		$resource('/api/ac/lc/sendMessageService/updateSendState', {}, {}).
			get({ 'product_id': item.product_id , 'tour_date' : item.tour_date}, function (res) {
				if (res.errcode === 0 || res.errcode === 10003) {
					tableconfig.search();

					alert('短信发送成功！');
					scope.util.init({
						'model': scope.model,
						'result': scope.result,
						'dateshow': scope.dateshow,
						'imageshow': scope.imageshow,
						'info': res.errcode == 0 ? res.data : {},
					});
				} else {
					alert(res.errmsg);
				}
			});
	}

	$scope.gotwomessage = function (item) {
		if(!confirm("确认再次发送短信？")){
			return ;
		}
		$resource('/api/as/lc/sendmessage/updateAgainSendState', {}, {}).
			get({ 'product_id': item.product_id , 'tour_date' : item.tour_date}, function (res) {
				if (res.errcode === 0 || res.errcode === 10003) {
					tableconfig.search();

					alert('短信发送成功！');
					scope.util.init({
						'model': scope.model,
						'result': scope.result,
						'dateshow': scope.dateshow,
						'imageshow': scope.imageshow,
						'info': res.errcode == 0 ? res.data : {},
					});
				} else {
					alert(res.errmsg);
				}
			});
	}

	$scope.touristMessage = function (item) {
		$scope.item = item;
		var modalInstance = $modal.open({
			template: require('../views/touristMessage.html'),
			controller: 'touristMessage',
			size: 'lg',
			resolve: {
				items: function () {
					return $scope.item;
				}
			}
		});
		modalInstance.opened.then(function () { 
		});
		modalInstance.result.then(function (showResult) {
		}, function (reason) {
			// click，点击取消，则会暑促cancel  
			$log.info('Modal dismissed at: ' + new Date());
		});
	}
	$scope.reject = function (item) {
		$scope.item = item;
		var modalInstance = $modal.open({
			template: require('../views/messageinfo.html'),
			controller: 'linemessageinfo',
			size: 'lg',
			resolve: {
				items: function () {
					return $scope.item;
				}
			}
		});
		modalInstance.result.then(function (showResult) {
			tableconfig.search();
		});
	}



	$scope.table = tableconfig;

};