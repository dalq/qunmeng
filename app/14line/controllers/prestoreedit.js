module.exports = function($scope, $state, $stateParams, $modalInstance, items, $resource){
   
    $scope.item=items;
    $scope.obj={};
     $resource('/api/as/pc/bmaccount/getbalanceinfo', {}, {}).
			save({'seller_code': $scope.item.seller_code ,'save_type': items.save_type}, function(res){
                if(res.errcode === 0 || res.errcode === 10003){
					$scope.obj=res.data;
                    // $scope.touristList=res.data.touristList;
                	
                }else{
                    alert(res.errmsg);
                }
            });
    $scope.cancel = function () {  
        $modalInstance.dismiss('cancel'); 
    };
    $scope.gogogo = function () {
        if($scope.messageinfo.$valid){
            //alert('通过验证可以提交表单');
        }else{
            //alert('表单没有通过验证');
            return;
        }
        $scope.obj['seller_code']=items.seller_code;
        $resource('/api/as/pc/bmaccount/updatebalance', {}, {}).save($scope.obj, function(res){
            // console.log(res);
            if(res.errcode === 0){
                $modalInstance.close($scope.result);
                alert('保存成功');
            }else{
                alert(res.errmsg);
            }
        });
    };
};