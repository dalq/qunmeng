/**
 * 模态框
 */
module.exports = function ($scope, $state, $stateParams,$resource,$modal,items,$modalInstance,$interval) {
	// console.log('下面是items');
	// console.log(items);
	// console.log('上面是items');
			$scope.data = [];
            $scope.item={};
            $scope.a={};
			$scope.linkman = {};
		    $scope.shuju={};
			$scope.bb='暂不填写';


 //正则判断身份证
            var aCity={11:"北京",12:"天津",13:"河北",14:"山西",15:"内蒙古",21:"辽宁",22:"吉林",23:"黑龙江",31:"上海",32:"江苏",33:"浙江",34:"安徽",35:"福建",36:"江西",37:"山东",41:"河南",42:"湖北",43:"湖南",44:"广东",45:"广西",46:"海南",50:"重庆",51:"四川",52:"贵州",53:"云南",54:"西藏",61:"陕西",62:"甘肃",63:"青海",64:"宁夏",65:"新疆",71:"台湾",81:"香港",82:"澳门",91:"国外"}; 
            var checkidcardno=function(sId){
                    var iSum=0 ;
                    var info="" ;
                    if(!/^\d{17}(\d|x)$/i.test(sId)) return "您输入的身份证长度或格式错误";
                    sId=sId.replace(/x$/i,"a");
                    if(aCity[parseInt(sId.substr(0,2))]==null) return "您的身份证地区非法";
                    sBirthday=sId.substr(6,4)+"-"+Number(sId.substr(10,2))+"-"+Number(sId.substr(12,2));
                    var d=new Date(sBirthday.replace(/-/g,"/")) ;
                    if(sBirthday!=(d.getFullYear()+"-"+ (d.getMonth()+1) + "-" + d.getDate()))return "身份证上的出生日期非法";
                    for(var i = 17;i>=0;i --) iSum += (Math.pow(2,i) % 11) * parseInt(sId.charAt(17 - i),11) ;
                    if(iSum%11!=1) return "您输入的身份证号非法";
                    //aCity[parseInt(sId.substr(0,2))]+","+sBirthday+","+(sId.substr(16,1)%2?"男":"女");//此次还可以判断出输入的身份证号的人性别
                    return true;
            }
            //正则护照
            var checkpassport=function(x){
                if(!/^(P\d{7})|(G\d{8})$/i.test(x)) return "您输入的护照号长度或格式错误";
            }
            //正则判断军官
            var checkofficer=function(x){
                if(!/^[a-zA-Z0-9]{7,21}$/i.test(x)) return "您输入的军官证号长度或格式错误";
            }
            //正则判断港澳台
            var checkgat=function(x){
                if(!/^[HMhm]{1}([0-9]{10}|[0-9]{8})$/i.test(x)) return "您输入的证件号长度或格式错误";
            }
            //正则判断手机号码
            var checkphone=function(x){
                if(!/^1[34578]\d{9}$/.test(x)) return "您输入的手机号码长度或格式错误";
            }
            //正则判断台胞证
            var checkt=function(x){
                if(!/^[a-zA-Z0-9]{8}$/i.test(x) || /^[a-zA-Z0-9]{10}$/i.test(x)) return "您输入的台胞证号长度或格式错误";
            }
			//正则邮箱
			var checkemail=function(x){
                if(!/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/i.test(x) )return "您输入的邮箱格式错误"
            }
			

			//出生日期和证件有效期日期下拉框
			var d = new Date();
            var dy=d.getFullYear();
            var dm=d.getMonth()+1;
            var dd=d.getDate();
			$scope.showdata={};
            $scope.showdata.cyears=[];
            $scope.showdata.byears=[];
            $scope.showdata.cmonths=[];
            $scope.showdata.cdays=[];
            var cm;
            var cadayS=function(cay,cam){
                if( (cay % 4 == 0 && cay % 100 != 0 )|| (cay % 400 == 0)){
                        if(cam==2){
                             $scope.showdata.cdays=[];
                            for(var i=1;i<30;i++){                                                                      
                                $scope.showdata.cdays.push({code:i,value:i});
                            }
                        }
                        if(cam==1 || cam==3 || cam==5 || cam== 7 || cam==8 || cam==10 || cam==12){
                             $scope.showdata.cdays=[];
                            for(var i=1;i<32;i++){                                                                      
                                $scope.showdata.cdays.push({code:i,value:i});
                            }
                        }
                        if(cam==4 || cam==6 || cam==9 || cam== 11 ){
                            $scope.showdata.cdays=[]; 
                            for(var i=1;i<31;i++){                                                                      
                                $scope.showdata.cdays.push({code:i,value:i});
                            }
                        }
                    }else{
                        if(cam==2){
                           $scope.showdata.cdays=[];
                            for(var i=1;i<29;i++){                                                                      
                                $scope.showdata.cdays.push({code:i,value:i});
                            }
                        }
                        if(cam==1 || cam==3 || cam==5 || cam== 7 || cam==8 || cam==10 || cam==12){
                            $scope.showdata.cdays=[]; 
                            for(var i=1;i<32;i++){                                                                      
                                $scope.showdata.cdays.push({code:i,value:i});
                            }
                        }
                        if(cam==4 || cam==6 || cam==9 || cam== 11 ){
                            $scope.showdata.cdays=[];
                            for(var i=1;i<31;i++){                                                                       
                                $scope.showdata.cdays.push({code:i,value:i});
                            }
                        }
                    }
            }           
            for(var i=0;i<=20;i++){
                var cy=dy-10+i;               
                $scope.showdata.cyears.push({code:cy,value:cy})
            }
            for(var i=0;i<=90;i++){
                var cy=dy-90+i;               
                $scope.showdata.byears.push({code:cy,value:cy})
            } 
            for(var i=1;i<13;i++){                                  
                    $scope.showdata.cmonths.push({code:i,value:i})
            }  
			 $scope.distributor_order={};       
             $scope.distributor_order.cy=function(cy){
                $scope.showdata.cmonths=[];
                for(var i=1;i<13;i++){                                  
                    $scope.showdata.cmonths.push({code:i,value:i})
                }
                cadayS(cy,cm);
                $scope.distributor_order.cm=function(cm){
                    cadayS(cy,cm);                                       
                }
             }
			
			//提交时人员信息list成人
			$scope.resultarr=[
				{
					name : '',
					card_type : '',
					cardno : '',
					tourist_type : '1',
					certificate_yesr : dy,
					certificate_month : dm,
					certificate_day : dd,
					birthday_year : dy,
					birthday_mouth : dm,
					birthday_day : dd,
					sex : '1',
					mobile : '',
				},
			];
			cadayS(dy,dm);  


			// for(var x=0;x<$scope.resultarr.length;x++){
			// 	$scope.resultarr[x].card_type='1';
			// }
			for(var x=0;x<$scope.resultarr.length;x++){
					if(items.product_type_name=='国内长线'||items.product_type_name=='周边游'||items.product_type_name=='国内当地参团'){
						$scope.resultarr[x].card_type='1';
					}else{
						$scope.resultarr[x].card_type='2';
					}
			}
			$scope.resultarrlength=$scope.resultarr.length;

			$scope.resulchildtarr=[

			];
			for(var x=0;x<$scope.resulchildtarr.length;x++){
				$scope.resulchildtarr[x].card_type='1';
			}
			
			if(items.product_type_name=='国内长线'||items.product_type_name=='周边游'||items.product_type_name=='国内当地参团'){
				 $scope.showinfo = {
				'day' : [
					{'name' : '身份证', 'value' : '1'},
	                {'name' : '护照', 'value' : '2'},
	                {'name' : '港澳通行证', 'value' : '3'},
	                {'name' : '军官证', 'value' : "6"},
	                {'name' : '台胞证', 'value' : '5'},
	            ],
            }
			}else{
				 $scope.showinfo = {
				'day' : [					
	                {'name' : '护照', 'value' : '2'},	               
	            ],
            }
			}
           
			$scope.sex = {
				'day' : [
					{'name' : '男', 'value' : '1'},
	                {'name' : '女', 'value' : '2'},
	            ],
            }
             $scope.a.product_name=items.product_name;
			$scope.a.city=items.start_city_arr;
            //日历要显示的数据
            $scope.wjq=0;
			$scope.result = [];

			$scope.dateArray = [];

			$scope.day='';

			$scope.product_state_name = '';
            
            //日历显示信息的顺序和样式
			$scope.showattrarr = [
				
				{
					key: 'adult_sale_price',
					position: 'left',
					before: ' ¥'
				},
				
				{
					key: 'stock',
					position: 'left',
					before: ''
				},
				
			];


            $resource('/api/ac/lc/groupDatesService/findinfoList', {}, {}).
			get({ product_id: items.id }, function(res){
                if (res.errcode === 0 || res.errcode === 10003) {
                         console.log(res);
						 console.log('上面是res');
						
						$scope.data = [];
						$scope.dateArray = [];
						$scope.day=res.data.day
						if (angular.isDefined(res.data)) {
							$scope.product_state_name = res.data.product_state_name;
							for (var dateNumber = 0; dateNumber < res.data.list.length; dateNumber++) {
								var element = angular.copy(res.data.list[dateNumber]);
								if(element.stock_type==0){
                                    element.stock = '不限';
									
                                }else{
                                    if (element.stock>9 ) {
                                        element.stock = '库存充足';
                                    }else if(element.stock==0){
                                        element.stock = '售罄';
                                    }
                                }
								

								element.d = element.tour_date.replace('-', '').replace('-', '');
								$scope.dateArray.push(element.d);
								delete element.tour_date;
								delete element.id;
								delete element.product_id;
								delete element.trip_id;
								element.materials_advance_reserve = element.materials_advance_reserve_day + '天' + 
																(element.materials_advance_reserve_hour ? element.materials_advance_reserve_hour : 0) + '时' + (element.materials_advance_reserve_minute ? element.materials_advance_reserve_minute : 0) + '分';

								$scope.data[dateNumber] = element;
                            }
                        }
                        

                }else{
                    alert(res.errmsg);
                }
                $scope.loadupdate();
            });


            /**
			 * 下方是初始化方法
			 */


            $scope.loadupdate = function () {
				var dataobj = {};
				if (angular.isDefined($scope.data) && angular.isArray($scope.data)) {
					for (var i = 0; i < $scope.data.length; i++) {
						var tmp = $scope.data[i];
						dataobj[tmp.d] = tmp;
					}
				}

				var showattrarr = [];
				if (angular.isDefined($scope.showattrarr) && angular.isArray($scope.showattrarr)) {
					showattrarr = $scope.showattrarr;
				}

				$scope.weekarr = ["日", "一", "二", "三", "四", "五", "六"];

				var obj = {};
				var date = new Date();
				var year = date.getFullYear();
				var month = date.getMonth();


				$scope.obj = makedata(year, month, dataobj, showattrarr);

				$scope.pre = function () {
					
					var dd = getYM($scope.obj.y, $scope.obj.m, -1);
					$scope.mmm =dd.m+1;
					// console.log($scope.mmm);
					// console.log($scope.mmmmm);
					$scope.obj = makedata(dd.y, dd.m, dataobj, showattrarr);
					if(dd.m+1==$scope.mmmmm){						
						setTimeout(function(){						
						$("#xiaokuai"+$scope.pindex1+$scope.index1 ).css("border","2px solid  #f80");
						},10)
						
						
					}
				};

				$scope.back = function () {
					
					var dd = getYM($scope.obj.y, $scope.obj.m, 1);
					$scope.mmm =dd.m-1;
					$scope.obj = makedata(dd.y, dd.m, dataobj, showattrarr);
					if(dd.m+1==$scope.mmmmm){						
                        setTimeout(function(){
						$("#xiaokuai"+$scope.pindex1+$scope.index1 ).css("border","2px solid  #f80")
						},10)					
					}
				};

				function makedata(year, month, dataobj, showattrarr) {
					var montharray = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
					var obj = {
						'y': year,
						'm': month,
						'data': [],
					};
					if ((obj.m === 1) && (obj.y % 4 === 0)
						&& ((obj.y % 100 !== 0) || (obj.y % 400 === 0))) {
						montharray[1] = 29;
					}
					//1号
					var firstdate = new Date(obj.y, obj.m, 1);
					//最后一号
					var lastdate = new Date(obj.y, obj.m, montharray[obj.m]);

					//1号星期几
					var fxingqi = firstdate.getDay();
					//最后一号星期几
					var lxingqi = lastdate.getDay();

					var dataarr = [];
					//日期之前的空位
					for (var f = 0; f < fxingqi; f++) {
						var dayobj = {
							'label': '',
							'd': '0',
						};
						dataarr.push(dayobj);
					}
					//日期
					for (var j = 0; j < montharray[obj.m]; j++) {
						var dayobj = {
							'label': j + 1,
							'd': obj.y + '',
							'labelarr': [],	//具体显示的信息
						};

						if (obj.m < 9) {
							dayobj.d += '0' + (obj.m + 1);
						} else {
							dayobj.d += obj.m + 1;
						}

						if (j < 9) {
							dayobj.d += '0' + (j + 1);
						} else {
							dayobj.d += (j + 1);
						}

						//有数据要显示。
						var show = dataobj[dayobj.d];
						if (angular.isDefined(dataobj[dayobj.d])) {
							for (var x = 0; x < showattrarr.length; x++) {
								var xx = showattrarr[x];
								var ooo = {
									'show': ((xx.before && show[xx.key]) ? xx.before : '') + (show[xx.key] ? show[xx.key] : '') + ((xx.after && show[xx.key]) ? xx.after : ''),
									'position': xx.position
								};
								dayobj.labelarr.push(ooo);
								dayobj['data'] = show;
							}
						}
						dataarr.push(dayobj);
					}
					//日期之后的空位
					for (var l = lxingqi; l < 6; l++) {
						var dayobj = {
							'label': '',
							'd': '0',
						};
						dataarr.push(dayobj);
					}

					for (var i = 0; i < dataarr.length; i++) {
						//每七个重新组装一个数组。
						var x = i % 7;
						if (x === 0) {
							obj.data.push(new Array());
						}
						obj.data[obj.data.length - 1].push(dataarr[i]);
					}
					return obj;
				}

				//step 负数，之前几个月，-1：表示之前一个月
				//     正数，之后几个月，1 ：表示之后一个月
				function getYM(y, m, step) {
					var yy = y;
					var mm = m;

					if (mm + step < 0) {
						yy -= 1;
						mm = 12 + mm + step;
					} else if (mm + step > 11) {
						yy += 1;
						mm = mm + step - 12;
					} else {
						mm += step;
					}

					return {
						'y': yy,
						'm': mm,
					}
				}
                $scope.item.chargeid2=1;
                $scope.item.chargeid1=0;

                //必须为数字成人
                $scope.clearpeopleNoNum = function(obj,attr){
					if(((!obj[attr])) || obj[attr]==0){
						obj[attr]=1;
						
					}
					if(obj[attr]>99){
						obj[attr]=99;
					}
                    //先把非数字的都替换掉，除了数字和.
                    obj[attr] = obj[attr].replace(/[^\d]/g,"");
                    $scope.a.peopleprice=$scope.a.adult_sale_price1*$scope.item.chargeid2;
                    
                    $scope.a.childprice=$scope.item.chargeid1*$scope.a.children_sale_price1; 
                    $scope.a.allprice=($scope.a.peopleprice+$scope.a.childprice).toFixed(2); 
					if($scope.children_ban=='1'||$scope.children_ban == 1){
							$scope.a.allprice=$scope.a.peopleprice;
						}
					if(obj[attr]>$scope.resultarr.length){
						var len=$scope.resultarr.length
						// console.log(obj[attr]-$scope.resultarr.length);
						for(var x=0;x<(obj[attr]-len);x++){
							$scope.resultarr.push({
								name : '',
								card_type : '',
								cardno : '',
								tourist_type : '1',
								certificate_yesr : '',
								certificate_month : '',
								certificate_day : '',
								birthday_year : '',
								birthday_mouth : '',
								birthday_day : '',
								sex : '1',
								mobile : '',

							});
						}
						for(var x=0;x<$scope.resultarr.length;x++){
							$scope.resultarr[x].card_type='1';
						}
					}else if(obj[attr]<$scope.resultarr.length){
						var len=$scope.resultarr.length
						for(var y=0;y<(len-obj[attr]);y++){
							$scope.resultarr.pop();
						}
					} 
					$scope.resultarrlength=$scope.resultarr.length;  
                    
                }
				//必须为数字儿童
                $scope.clearchildNoNum = function(obj,attr){
					if(obj[attr]>99){
						obj[attr]=99;
					}
                    //先把非数字的都替换掉，除了数字和.
                    obj[attr] = obj[attr].replace(/[^\d]/g,"");
                    $scope.a.peopleprice=$scope.a.adult_sale_price1*$scope.item.chargeid2;
                    
                    $scope.a.childprice=$scope.item.chargeid1*$scope.a.children_sale_price1; 
                    $scope.a.allprice=($scope.a.peopleprice+$scope.a.childprice).toFixed(2); 
					if(obj[attr]>$scope.resulchildtarr.length){
						var len=$scope.resulchildtarr.length
						// console.log(obj[attr]-$scope.resulchildtarr.length);
						for(var x=0;x<(obj[attr]-len);x++){
							$scope.resulchildtarr.push({
								name : '',
								card_type : '',
								cardno : '',
								tourist_type : '0',
								certificate_yesr : '',
								certificate_month : '',
								certificate_day : '',
								birthday_year : '',
								birthday_mouth : '',
								birthday_day : '',
								sex : '1',
							});
						}
						for(var a=0;a<$scope.resulchildtarr.length;a++){
							// console.log(a);
							$scope.resulchildtarr[a].card_type='1';
						}
					}else if(obj[attr]<$scope.resulchildtarr.length){
						var len=$scope.resulchildtarr.length
						for(var y=0;y<(len-obj[attr]);y++){
							$scope.resulchildtarr.pop();
						}
					}      
                    
                }
                $scope.input=function(){
                    $scope.a.peopleprice=$scope.a.adult_sale_price1*$scope.item.chargeid2;                    
                    $scope.a.childprice=$scope.item.chargeid1*$scope.a.children_sale_price1; 					
                    $scope.a.allprice=($scope.a.peopleprice+$scope.a.childprice).toFixed(2);    
                }

                //成人加
                $scope.peopleadd = function(){

                     $scope.item.chargeid2=parseInt($scope.item.chargeid2)+1;
                     $scope.a.peopleprice=$scope.a.adult_sale_price1*$scope.item.chargeid2;
					 $scope.a.allprice=$scope.a.peopleprice;
					 if($scope.children_ban=='1'||$scope.children_ban == 1){
							$scope.a.allprice=$scope.a.peopleprice;
						}
                    
					 $scope.resultarr.push({
						name : '',
						card_type : '',
						cardno : '',
						tourist_type : '1',
						certificate_yesr : dy,
						certificate_month : dm,
						certificate_day : dd,
						birthday_year : dy,
						birthday_mouth : dm,
						birthday_day : dd,
						sex : '1',
						mobile : '',

					});
					cadayS(dy,dm);
					
				for(var x=1;x<$scope.resultarr.length;x++){
					if(items.product_type_name=='国内长线'||items.product_type_name=='周边游'||items.product_type_name=='国内当地参团'){
						$scope.resultarr[x].card_type='1';
					}else{
						$scope.resultarr[x].card_type='2';
					}
			}
					
					$scope.resultarrlength=$scope.resultarr.length;
                }
                //成人减
                $scope.peoplesub = function(){
                    if($scope.item.chargeid2<2){
                        return;
                    }else{
                        $scope.item.chargeid2-=1;
                        $scope.a.peopleprice=$scope.a.adult_sale_price1*$scope.item.chargeid2;
                        $scope.a.allprice=($scope.a.peopleprice+$scope.a.childprice).toFixed(2);  
						if($scope.children_ban=='1'||$scope.children_ban == 1){
							$scope.a.allprice=$scope.a.peopleprice;
						}
                    }
					$scope.resultarr.pop();
					// for(var x=0;x<$scope.resultarr.length;x++){
					// 	$scope.resultarr[x].card_type='1';
					// }
					
					for(var x=0;x<$scope.resultarr.length;x++){
						// console.log(items.product_type_name);
					if(items.product_type_name=='国内长线'||items.product_type_name=='周边游'||items.product_type_name=='国内当地参团'){
						$scope.resultarr[x].card_type='1';
					}else{
						$scope.resultarr[x].card_type='2';
					}
			}
					$scope.resultarrlength=$scope.resultarr.length; 
                }
                 //儿童加
                $scope.childadd = function(){

                    $scope.item.chargeid1=parseInt($scope.item.chargeid1)+1;
                    $scope.a.childprice=$scope.item.chargeid1*$scope.a.children_sale_price1; 
                    $scope.a.allprice=($scope.a.peopleprice+$scope.a.childprice).toFixed(2); 
					$scope.resulchildtarr.push({
						name : '',
						card_type : '',
						cardno : '',
						tourist_type : '0',
						certificate_yesr : dy,
						certificate_month : dm,
						certificate_day : dd,
						birthday_year : dy,
						birthday_mouth : dm,
						birthday_day : dd,
						sex : '1',
					}); 
					cadayS(dy,dm);

					for(var x=0;x<$scope.resulchildtarr.length;x++){
					if(items.product_type_name=='国内长线'||items.product_type_name=='周边游'||items.product_type_name=='国内当地参团'){
						$scope.resulchildtarr[x].card_type='1';
					}else{
						$scope.resulchildtarr[x].card_type='2';
					}
			}
					
                }

                //儿童减
                $scope.childsub = function(){
                    if($scope.item.chargeid1<1){
                        return;
                    }else{
                        $scope.item.chargeid1-=1;
                        $scope.a.childprice=$scope.item.chargeid1*$scope.a.children_sale_price1; 
                        $scope.a.allprice=($scope.a.peopleprice+$scope.a.childprice).toFixed(2);                    
                    }
					$scope.resulchildtarr.pop();
					
                }
				//上车地点默认值
				$scope.linkman.bording_locations = [{ code:"0",sys_type:"未选择"}];
				$scope.linkman.bording_location='0';
                //日历每一天点击事件
                $scope.clickday = function (item1,index,pindex) {
				var today=date2str(new Date());				
				// console.log(item1);
				if(!item1.data){
                        return;
                    }
				if(item1.d<today){
					alert('出游日期不能不能小于当天日期');
					return;
				}
				if(item1.data.close_group=='1'){
					alert('当前团期已封团，无法下单');
					return;
				}
				
					if($scope.pindex1==pindex&&$scope.index1==index){
						return;
					}
					$scope.item.chargeid1=0;
					$scope.item.chargeid2=1;
					$scope.resultarr=[
				{
					name : '',
					card_type : '1',
					cardno : '',
					tourist_type : '1',
					certificate_yesr : dy,
					certificate_month : dm,
					certificate_day : dd,
					birthday_year : dy,
					birthday_mouth : dm,
					birthday_day : dd,
					sex : '1',
					mobile : '',
				},
			];
			cadayS(dy,dm);  
			for(var x=0;x<$scope.resultarr.length;x++){
					if(items.product_type_name=='国内长线'||items.product_type_name=='周边游'||items.product_type_name=='国内当地参团'){
						$scope.resultarr[x].card_type='1';
					}else{
						$scope.resultarr[x].card_type='2';
					}
			}
			$scope.resulchildtarr=[

			];

					
					$("#xiaokuai"+$scope.pindex1+$scope.index1).removeAttr("style");
					// $("#xiaokuai"+pindex+index).html(41532131);
				var cloneDiv = $("#xiaokuai"+pindex+index).clone(true);
				var cloneDiv2 = $("#dakuai");
				$("#dakuai").css({
					 "border":"2px solid  #f80",
					//  "background-color":" #f80",
					 //  "box-shadow": " 0 0 3px #f80",
				 })
				// cloneDiv.css({
				// 	"border":"1px solid red",
				// 	//  "box-shadow": " 0 0 3px #f80",
				// })
				 $("#xiaokuai"+pindex+index).css({
					 "border":"2px solid  #f80",
					//  "background-color":" #f80",
					 //  "box-shadow": " 0 0 3px #f80",
				 })
				var proOffsettop=	getTop( document.getElementById('xiaokuai'+pindex+index));
				var proOffsetleft=	getLeft(document.getElementById('xiaokuai'+pindex+index));
				var shopOffsettop=	getTop( document.getElementById('shop'));
				var shopOffsetleft=	getLeft( document.getElementById('shop'));
				var tempttop=	getTop( document.getElementById('temp'));
				var templeft=	getLeft( document.getElementById('temp'));
				var headtop=	getTop( document.getElementById('head'));
				var headleft=	getLeft( document.getElementById('head'));
				// console.log(proOffsettop);
				// console.log(proOffsetleft);
				// console.log(shopOffsettop);
				// console.log(shopOffsetleft);
				$scope.index1=index;
				$scope.pindex1=pindex;
					// var proOffset = document.getElementById('xiaokuai'+pindex+index).getBoundingClientRect();
					// var shopOffset =  document.getElementById('shop').getBoundingClientRect();

					// console.log(cloneDiv.offset().left);
					// console.log(shopOffset);
					cloneDiv.css({ "position": "absolute", "top": proOffsettop - tempttop, "left": proOffsetleft- templeft,"border":"2px solid #f80"
					});
                    $("#xiaokuai"+pindex+index).append(cloneDiv);
 
                    cloneDiv.animate({	
					
					height:0,
					width:0,
                    left:0- templeft+shopOffsetleft,
                    top: 0-tempttop+shopOffsettop		
				},'slow',function(){
					
					  $scope.godate = item1.d.substring(0, 4) + '-' + item1.d.substring(4, 6) +
                        '-' + item1.d.substring(6, 8);
						$(cloneDiv).remove();
						$scope.$apply();
				});

				
				// var interval = setInterval(show,1000);
                // function show(){

				// 	var num =0;
				// 	num=num+1;
				// 	console.log(num);
				// 	if(num==2){
				// 		clearInterval(interval);
				// 	}
					
                // }
				var timesRun = 0;
                var interval = setInterval(function(){
                 timesRun += 1;
                 if(timesRun ===4){
                 clearInterval(interval);
				 $("#shop").removeAttr("style");

                 }
                   //do whatever here..
				   if(timesRun%2==0){
					$("#shop").removeAttr("style");
					$("#shop").css({
					 "border":"1px solid grey"
				   });
				   
				   }else{
					   $("#shop").css({
					 "border":"1px solid #f80",
					 "box-shadow": " 0 0 3px #f80",
					 
				   });
	
				   }
				   
				  
                   }, 400);
				
				
				
				//获取元素的纵坐标 
                function getTop(e){
                var offset=e.offsetTop; 
                if(e.offsetParent!=null) offset+=getTop(e.offsetParent); 
                return offset; 
                } 
                //获取元素的横坐标 
                function getLeft(e){ 
                var offset=e.offsetLeft; 
                if(e.offsetParent!=null) offset+=getLeft(e.offsetParent); 
                return offset; 
                }                
					
				
			        // console.log('下面是item1内容');	
                    // console.log(item1);
					// console.log('上面是item1内容');
                    $scope.mmmm = item1.d.substring(4, 6);
					$scope.mmmmm=parseInt($scope.mmmm);
					// console.log($scope.mmmmm);
                    $scope.wjq=1;
                    // var today=date2str2(new Date());
                    // $scope.godate=date2str2(item1.d);

					if(item1.data.children_ban=='1'||item1.data.children_ban==1){
						    $scope.cc='儿童禁售'
							$scope.dd='儿童禁售'
							$scope.a.childprice=0;
							$scope.a.children_sale_price1=0;
						}else{
							$scope.cc='儿童价：'+item1.data.children_sale_price+'元'
							$scope.dd='儿童结算价：'+item1.data.children_call_price+'元'

						}
						                    
                        $scope.a.adult_call_price=item1.data.adult_call_price;
                        $scope.a.adult_sale_price=item1.data.adult_sale_price;
                        $scope.a.children_call_price=item1.data.children_call_price;
                        $scope.a.children_sale_price=item1.data.children_sale_price;
                        				
                        $scope.a.stock=item1.data.stock;
						if($scope.a.stock=='不限'){
							$scope.a.stock_totals='不限';
						}else{
                        $scope.a.stock_totals=item1.data.stock_totals;		
                       		
						}

                        $scope.a.adult_sale_price1=item1.data.adult_sale_price; 
                        $scope.a.children_sale_price1=item1.data.children_sale_price;                                               
                        
						$scope.children_ban=item1.data.children_ban;
							
						

                        $scope.a.peopleprice=$scope.a.adult_sale_price1*$scope.item.chargeid2;
                        $scope.a.childprice=$scope.item.chargeid1*$scope.a.children_sale_price1;
                        $scope.a.allprice=($scope.a.peopleprice+$scope.a.childprice).toFixed(2);  

                        if($scope.children_ban=='1'||$scope.children_ban==1){
							$scope.a.allprice=$scope.a.peopleprice;
						}
					
						 

					
            //     调上车地点接口
			
        $resource('/api/ac/lc/sendMessageService/findTourstPlace', {}, {})
		.save({ product_id: items.id }, function (res) {
		// console.log('下面是上车地点');
        // console.log(res);
		// console.log('上面是上车地点');
		
        if (res.errcode === 0) {
		$scope.linkman.bording_locations = [{ code:"0",sys_type:"未选择"}];
        var arrtuijian = res.data.assembling_place.split("%"); 
		for(i=0;i<arrtuijian.length;i++){		
			$scope.linkman.bording_locations.push({code :arrtuijian[i] , sys_type: arrtuijian[i]});			
		}
			

        } else {
            alert(res.errmsg);
        }
        });

			    };

				//提交
				//联系人信息验证
				$scope.toinfo = function(){
					if($scope.linkman.name==undefined){
							alert('联系人姓名不可为空！');
							return;
						} else
					if($scope.linkman.tel==undefined){
							alert('联系人电话不可为空！');
							return;
						} 
						else
					if($scope.phoneinfo!=''){
							alert('请填写正确的联系人电话');
							return;
						}else
					if($scope.linkman.bording_location=='0'){
							alert('请选择联系人上车地点');
							return;
						}
					

				
						//二次确认
						if($scope.b == '0'){
							alert('此订单暂无人员信息，请尽快填写人员信息');
							$scope.result=[];
						}else{
					
						//游客信息验证
					for(var j=0;j<$scope.resultarr.length;j++){
						if($scope.resultarr[j].name==undefined || $scope.resultarr[j].name==''){
							alert('请填写游客姓名');
							return;
						}
					}
					for(var m=0;m<$scope.resulchildtarr.length;m++){
						if($scope.resulchildtarr[m].name==undefined || $scope.resulchildtarr[m].name==''){
							alert('请填写游客姓名');
							return;
						}
					}
					
					for(var k=0;k<$scope.resultarr.length;k++){
						// console.log( $scope.resultarr[k].cardinfo);
						if( $scope.resultarr[k].cardinfo!=''){
							alert('请填写正确的游客证件号码');
						return;
					    }
					}

					for(var o=0;o<$scope.resulchildtarr.length;o++){
						// console.log( $scope.resulchildtarr[o].cardinfo);
						if( $scope.resulchildtarr[o].cardinfo!=''){
							alert('请填写正确的游客证件号码');
						return;
					    }
					}
					
					
					for(var l=0;l<$scope.resultarr.length;l++){
						if( $scope.resultarr[l].phoneinfo!=''){
							alert('请填写正确的游客手机号码');
						return;
					    }
					}


					$scope.result = [];
					for(var p=0;p<$scope.resultarr.length;p++){
						$scope.result.push($scope.resultarr[p]);
					}
					for(var m=0;m<$scope.resulchildtarr.length;m++){
						$scope.result.push($scope.resulchildtarr[m]);
					}
					
				
					//清空其他证件类型信息
					for(var z = 0;z<$scope.result.length;z++){
						if ($scope.result[z].card_type == '1'){
							 $scope.result[z].certificate_yesr = null;
							 $scope.result[z].certificate_month = null;
							 $scope.result[z].certificate_day = null;
							 $scope.result[z].birthday_year = null;
							 $scope.result[z].birthday_mouth = null;
							 $scope.result[z].birthday_day = null;

						};
						if ($scope.result[z].card_type == '5' || $scope.result[z].card_type == '6'){
							 $scope.result[z].certificate_yesr = null;
							 $scope.result[z].certificate_month = null;
							 $scope.result[z].certificate_day = null;
						};
					}
						
					if(!confirm("人员信息已填写完整，是否下单并扣除预存？")){
                    return ;
               
						}
					  
					};
					// console.log($scope.resultarr);
					$scope.shuju['order_code'] = '1';
					$scope.shuju['cardno'] = '1';
					$scope.shuju['invoice'] = '0';
				    $scope.shuju['noinfo'] = $scope.b;
					$scope.shuju['tour_date'] = $scope.godate;
					$scope.shuju['tourist'] = $scope.result;
					$scope.shuju['product_id'] = items.id;
					$scope.shuju['name'] = $scope.linkman.name;
					$scope.shuju['mobile'] = $scope.linkman.tel ;
					$scope.shuju['email'] = $scope.linkman.email;
					$scope.shuju['assembling_place'] = $scope.linkman.bording_location;
					$scope.shuju['adult_number'] =$scope.item.chargeid2;
					$scope.shuju['children_number'] =$scope.item.chargeid1;
					$scope.shuju['actual_adult_sale_price'] = $scope.a.adult_sale_price1;
					$scope.shuju['actual_child_sale_price'] = $scope.a.children_sale_price1;

					// console.log($scope.shuju);
					
					 $resource('/api/ac/lc/lineDistributorOrderService/createOrderByDistributor', {}, {}).
						save($scope.shuju, function(res){
							if (res.errcode === 0 || res.errcode === 10003) {
									alert('提交成功');
                            $modalInstance.dismiss('cancel'); 
							}else{
								alert(res.errmsg);
							}
						});
					
				}
            //取消
			$scope.cancel = function () {
                if(confirm("是否确认关闭？")){
                     $modalInstance.dismiss('cancel');
                }  
                 
            };
			//日期转换
			function date2str(d) {
                if (d === undefined) {
                    return "";
                }
                var month = (d.getMonth() + 1).toString();
                var day = d.getDate().toString();
                if (month.length == 1) month = '0' + month;
                if (day.length == 1) day = '0' + day;
                return d.getFullYear() + month + day;
            }
            function date2str2(d) {
                if (d === undefined) {
                    return "";
                }
                var month = (d.getMonth() + 1).toString();
                var day = d.getDate().toString();
                if (month.length == 1) month = '0' + month;
                if (day.length == 1) day = '0' + day;
                return d.getFullYear() + "-" + month + "-" + day;
            }
        }
			//暂不填写
			$scope.b= '1';
			
			$scope.show = function(){
				// console.log($scope.b);
				if($scope.b=='1'){
					if(!confirm("暂不填写人员信息？")){
                    return ;
                }
					$scope.b='0';					
					$scope.bb='展开';

				}else if($scope.b=='0'){
					
					$scope.b='1';
					$scope.bb='暂不填写';
					
				}	
			};


			 $scope.clear1 = function(index){
			 	  $scope.resultarr[index].name='';
				  //$scope.resultarr[index].card_type='1';
				  $scope.resultarr[index].cardno='';
				  $scope.resultarr[index].certificate_yesr=dy;
				  $scope.resultarr[index].certificate_month=dm;
				  $scope.resultarr[index].certificate_day=dd;
				  $scope.resultarr[index].birthday_year=dy;
				  $scope.resultarr[index].birthday_mouth=dm;
				  $scope.resultarr[index].birthday_day=dd;
				  $scope.resultarr[index].sex='1';
				  $scope.resultarr[index].mobile='';
				  cadayS(dy,dm);
				  $scope.resultarr[index].phoneinfo='';
				  $scope.resultarr[index].cardinfo='';
			 }

			 $scope.clear2 = function(index){
			 	  $scope.resulchildtarr[index].name='';
				  //$scope.resulchildtarr[index].card_type='1';
				  $scope.resulchildtarr[index].cardno='';
				  $scope.resulchildtarr[index].certificate_yesr=dy;
				  $scope.resulchildtarr[index].certificate_month=dm;
				  $scope.resulchildtarr[index].certificate_day=dd;
				  $scope.resulchildtarr[index].birthday_year=dy;
				  $scope.resulchildtarr[index].birthday_mouth=dm;
				  $scope.resulchildtarr[index].birthday_day=dd;
				  $scope.resulchildtarr[index].sex='1';
				  cadayS(dy,dm);		
				  $scope.resulchildtarr[index].cardinfo=''
				  
			 }
			 $scope.atypechange = function(index){
				 $scope.resultarr[index].cardno=null;
				 $scope.resultarr[index].cardinfo="";
				 
			 }
			 $scope.ctypechange = function(index){
				 $scope.resulchildtarr[index].cardno=null;
				 $scope.resulchildtarr[index].cardinfo="";
			 }
       
			                                    //正则成人
			                                    $scope.showphoneinfo=function(x,index){
													if(x==undefined){
                                                    return;
                                                }
                                                if(x==''){
                                                    return;
                                                }
												if(x==null){
                                                    return;
                                                }
												if(x==""){
                                                    return;
                                                }
                                                if(checkphone(x)=="您输入的手机号码长度或格式错误"){
                                                    $scope.resultarr[index].phoneinfo="您输入的手机号码长度或格式错误"
                                                }else{
                                                    $scope.resultarr[index].phoneinfo="";
                                                }
                                            }

											  $scope.showphoneinfo4=function(index){
                             
                                                    $scope.resultarr[index].phoneinfo="";
                                                
                                            }

                                            $scope.showcardno=function(x,index){
												if(x==undefined){
                                                    return;
                                                }
                                                if(x==''){
                                                    return;
                                                }
												if(x==null){
                                                    return;
                                                }
												if(x==""){
                                                    return;
                                                }
                                                
                                                if($scope.resultarr[index].card_type=="1" && $scope.resultarr[index].tourist_type=="1"){                                                    
                                                    if(checkidcardno(x)=="您输入的身份证长度或格式错误"){
                                                        $scope.resultarr[index].cardinfo="您输入的身份证长度或格式错误";
                                                    };
                                                    if(checkidcardno(x)=="您的身份证地区非法"){
                                                        $scope.resultarr[index].cardinfo="您的身份证地区非法";
                                                    };
                                                    if(checkidcardno(x)=="身份证上的出生日期非法"){
                                                        $scope.resultarr[index].cardinfo="身份证上的出生日期非法";
                                                    };
                                                    if(checkidcardno(x)=="您输入的身份证号非法"){
                                                        $scope.resultarr[index].cardinfo="您输入的身份证号非法";
                                                    };
                                                    if(checkidcardno(x)==true){
                                                        $scope.resultarr[index].cardinfo="";
                                                    };
                                                }
                                                    if($scope.resultarr[index].card_type=="2" && $scope.resultarr[index].tourist_type=="1"){
                                                        if(checkpassport(x)=="您输入的护照号长度或格式错误"){
                                                            $scope.resultarr[index].cardinfo="您输入的护照号长度或格式错误";
                                                        }else{
                                                            $scope.resultarr[index].cardinfo="";
                                                        }
                                                    }
                                                    if($scope.resultarr[index].card_type=="3" && $scope.resultarr[index].tourist_type=="1"){
                                                        if(checkgat(x)=="您输入的证件号长度或格式错误"){
                                                            $scope.resultarr[index].cardinfo="您输入的证件号长度或格式错误";
                                                        }else{
                                                    $scope.resultarr[index].cardinfo="";
                                                }
                                                    }
                                                    if($scope.resultarr[index].card_type=="5" && $scope.resultarr[index].tourist_type=="1"){
                                                        if(checkt(x)=="您输入的台胞证号长度或格式错误"){
                                                            $scope.resultarr[index].cardinfo="您输入的台胞证号长度或格式错误";
                                                        }else{
                                                            $scope.resultarr[index].cardinfo="";
                                                        }
                                                    }
                                                    if($scope.resultarr[index].card_type=="6" && $scope.resultarr[index].tourist_type=="1"){
                                                        if(checkofficer(x)=="您输入的军官证号长度或格式错误"){
                                                            $scope.resultarr[index].cardinfo="您输入的军官证号长度或格式错误";
                                                        }else{
                                                    $scope.resultarr[index].cardinfo="";
                                                }
                                                    }    
												}
												
												 $scope.showcardno4=function(index){
                                                
                                                if($scope.resultarr[index].card_type=="1" && $scope.resultarr[index].tourist_type=="1"){                                                    
                                              
                                                        $scope.resultarr[index].cardinfo="";
                                                  
                                                }
                                                    if($scope.resultarr[index].card_type=="2" && $scope.resultarr[index].tourist_type=="1"){
                                                       
                                                            $scope.resultarr[index].cardinfo="";
                                                        
                                                    }
                                                    if($scope.resultarr[index].card_type=="3" && $scope.resultarr[index].tourist_type=="1"){
                                                     
                                                    $scope.resultarr[index].cardinfo="";
                                                
                                                    }
                                                    if($scope.resultarr[index].card_type=="5" && $scope.resultarr[index].tourist_type=="1"){
                                                      
                                                            $scope.resultarr[index].cardinfo="";
                                                        
                                                    }
                                                    if($scope.resultarr[index].card_type=="6" && $scope.resultarr[index].tourist_type=="1"){
                                                        
                                                    $scope.resultarr[index].cardinfo="";
                                                
                                                    }    
                                                    }


                                                //正则儿童
			                                    $scope.showphoneinfo2=function(x,index){
												if(x==undefined){
                                                    return;
                                                }
                                                if(x==''){
                                                    return;
                                                }
												if(x==null){
                                                    return;
                                                }
												if(x==""){
                                                    return;
                                                }
                                                if(checkphone(x)=="您输入的手机号码长度或格式错误"){
                                                    $scope.resulchildtarr[index].phoneinfo="您输入的手机号码长度或格式错误"
                                                }else{
                                                    $scope.resulchildtarr[index].phoneinfo="";
                                                }
                                            }

												$scope.showphoneinfo3=function(index){
													
                                                    $scope.resulchildtarr[index].phoneinfo="";
                                                }

                                            $scope.showcardno2=function(x,index){
												if(x==undefined){
                                                    return;
                                                }
                                                if(x==''){
                                                    return;
                                                }
												if(x==null){
                                                    return;
                                                }
												if(x==""){
                                                    return;
                                                }
                                                
                                                if($scope.resulchildtarr[index].card_type=="1" && $scope.resulchildtarr[index].tourist_type=="0"){                                                    
                                                    if(checkidcardno(x)=="您输入的身份证长度或格式错误"){
                                                        $scope.resulchildtarr[index].cardinfo="您输入的身份证长度或格式错误";
                                                    };
                                                    if(checkidcardno(x)=="您的身份证地区非法"){
                                                        $scope.resulchildtarr[index].cardinfo="您的身份证地区非法";
                                                    };
                                                    if(checkidcardno(x)=="身份证上的出生日期非法"){
                                                        $scope.resulchildtarr[index].cardinfo="身份证上的出生日期非法";
                                                    };
                                                    if(checkidcardno(x)=="您输入的身份证号非法"){
                                                        $scope.resulchildtarr[index].cardinfo="您输入的身份证号非法";
                                                    };
                                                    if(checkidcardno(x)==true){
                                                        var age=dy-x.substring(6, 10);
                                                        //alert(age);
                                                        if(age<12 && age>2){
                                                            $scope.resulchildtarr[index].cardinfo="";
                                                        }else{
                                                            $scope.resulchildtarr[index].cardinfo="你的年龄不在在2~12岁之间，不属于儿童";
                                                        }
                                                    };
                                                }
                                                    if($scope.resulchildtarr[index].card_type=="2" && $scope.resulchildtarr[index].tourist_type=="0"){
                                                        if(checkpassport(x)=="您输入的护照号长度或格式错误"){
                                                            $scope.resulchildtarr[index].cardinfo="您输入的护照号长度或格式错误";
                                                        }else{
                                                            $scope.resulchildtarr[index].cardinfo="";
                                                        }
                                                    }
                                                    if($scope.resulchildtarr[index].card_type=="3" && $scope.resulchildtarr[index].tourist_type=="0"){
                                                        if(checkgat(x)=="您输入的证件号长度或格式错误"){
                                                            $scope.resulchildtarr[index].cardinfo="您输入的证件号长度或格式错误";
                                                        }else{
                                                    $scope.resulchildtarr[index].cardinfo="";
                                                }
                                                    }
                                                    if($scope.resulchildtarr[index].card_type=="5" && $scope.resulchildtarr[index].tourist_type=="0"){
                                                        if(checkt(x)=="您输入的台胞证号长度或格式错误"){
                                                            $scope.resulchildtarr[index].cardinfo="您输入的台胞证号长度或格式错误";
                                                        }else{
                                                            $scope.resulchildtarr[index].cardinfo="";
                                                        }
                                                    }
                                                    if($scope.resulchildtarr[index].card_type=="6" && $scope.resulchildtarr[index].tourist_type=="0"){
                                                        if(checkofficer(x)=="您输入的军官证号长度或格式错误"){
                                                            $scope.resulchildtarr[index].cardinfo="您输入的军官证号长度或格式错误";
                                                        }else{
                                                    $scope.resulchildtarr[index].cardinfo="";
                                                }
                                                    }    
												}

												 $scope.showcardno3=function(index){
                                                
                                                if($scope.resulchildtarr[index].card_type=="1" && $scope.resulchildtarr[index].tourist_type=="0"){                                                    
 
                                                        $scope.resulchildtarr[index].cardinfo="";
                                                    
                                                }
                                                    if($scope.resulchildtarr[index].card_type=="2" && $scope.resulchildtarr[index].tourist_type=="0"){
  
                                                    $scope.resulchildtarr[index].cardinfo="";
                                                
                                                    }
                                                    if($scope.resulchildtarr[index].card_type=="5" && $scope.resulchildtarr[index].tourist_type=="0"){
                                                     
                                                            $scope.resulchildtarr[index].cardinfo="";
                                                       
                                                    }
                                                    if($scope.resulchildtarr[index].card_type=="6" && $scope.resulchildtarr[index].tourist_type=="0"){
                                                       
                                                    $scope.resulchildtarr[index].cardinfo="";
                                                
                                                    }    
												}
												//联系人电话正则
												$scope.showphoneinfol=function(x){
													if(x==undefined){
                                                    return;
                                                }
                                                if(x==''){
                                                    return;
                                                }
												if(x==null){
                                                    return;
                                                }
												if(x==""){
                                                    return;
                                                }
                                                if(checkphone(x)=="您输入的手机号码长度或格式错误"){
                                                    $scope.phoneinfo="您输入的手机号码长度或格式错误"
                                                }else{
                                                    $scope.phoneinfo="";
                                                }
                                            }
											//联系人电话focus
												$scope.showphoneinfol2=function(){
                                                    $scope.phoneinfo="";
                                                
                                            }
											//联系人电子邮箱正则
												$scope.showemailinfol=function(x){
													if(x==undefined){
                                                    return;
                                                }
                                                if(x==''){
                                                    return;
                                                }
												if(x==null){
                                                    return;
                                                }
												if(x==""){
                                                    return;
                                                }
                                                if(checkemail(x)=="您输入的邮箱格式错误"){
                                                    $scope.emailinfo="您输入的邮箱格式错误"
                                                }else{
                                                    $scope.emailinfo="";
                                                }
                                            }
											//联系人电话focus
												$scope.showemailinfol2=function(){
                                                    $scope.emailinfo="";
                                                
                                            }
					
					//成人、儿童销售价
				    $scope.positive=function(obj,attr){
								
							if((!obj[attr])||obj[attr]==undefined){
						    obj[attr]=0;
						
							}
					
					$scope.a.peopleprice=$scope.a.adult_sale_price1*$scope.item.chargeid2;                    
                    $scope.a.childprice=$scope.item.chargeid1*$scope.a.children_sale_price1; 
                    $scope.a.allprice=($scope.a.peopleprice+$scope.a.childprice).toFixed(2); 
					if($scope.children_ban=='1'||$scope.children_ban == 1){
							$scope.a.allprice=$scope.a.peopleprice;
						}
					// console.log($scope.children_ban);	
					obj[attr] = obj[attr].replace(/[^\d]/g,"");
							
					}
};
