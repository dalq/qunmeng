/**
 * 模态框
 */
module.exports = function ($scope, $state, $stateParams,$resource,$modal) {
	/* 分页
     * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.currentPage = 1;      //当前页码
    $scope.itemsPerPage = 10         //每页显示几条
    $scope.a=[];
	 

	$scope.pageChanged = function () {
       var para = {
        //    ta:1, 
            pageNo:$scope.currentPage, 
            pageSize:$scope.itemsPerPage,
			pay_time : $scope.pay_time,
			trade_no : $scope.trade_no,
			refund_state : $scope.a.refund_state,
			refund_code : $scope.refund_code,
			order_code : $scope.id,
        };
        $resource('/api/as/lc/orderactual/findinfolist', {}, {}).
			save(para, function(res){
				// console.log('购买须知');
                // console.log(res);
                if(res.errcode === 0 || res.errcode === 10003){
					$scope.a=res.data.results;
					$scope.totalItems = res.data.totalRecord;
                	
                }else{
                    alert(res.errmsg);
                }
            });

    };
	$scope.pageChanged();


			$scope.search=function(){
			var dic = {
				pay_time : $scope.pay_time,
				trade_no : $scope.trade_no,
				refund_state : $scope.a.refund_state,
				refund_code : $scope.refund_code,
				order_code : $scope.id,
			}
				$resource('/api/as/lc/orderactual/findinfolist', {}, {}).save(dic, function(res){
                if(res.errcode === 0 || res.errcode === 10003){
					$scope.a=res.data.results;
                	$scope.totalItems = res.data.totalRecord;
                }else{
                    alert(res.errmsg);
                }
            });
			}
            $scope.toinfo = function (index) {
                if(!confirm("是否确认已退款！")){
                    return ;
                }
                $scope.item = $scope.a[index];
                $resource('/api/as/lc/orderactual/updateState', {}, {}).
                    get({ 'order_code': $scope.item.id }, function (res) {
                        if (res.errcode === 0 || res.errcode === 10003) {
                            alert('退款成功');
							$scope.pageChanged();
                        } else {
                            alert(res.errmsg);
                        }
                    });
            }
            $scope.tuikuan=[{name:'未选择',value:''},
                {name:'未退款',value:'0'},
                {name:'已退款',value:'1'}
              
            ]
};
