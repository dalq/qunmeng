module.exports = function($scope, $state, $stateParams, $modalInstance, items, $resource){
   

    $scope.item=items;
    $scope.obj={};
    $scope.bording_locations = [{ CODE:"0",NAME:"未选择"}];
    $scope.bording_location = '0';
    $scope.bording_location1 = '0';
    $scope.arr = [];
    $scope.product_type = "0";
    $scope.employee = {};
    $scope.result = [];
    $scope.ptarr=[];  
    $scope.ptarr2=[];
    $scope.showinfo = {
				'product_type' : [
					{'name' : '周边', 'value' : '0'},
	                {'name' : '国内长线', 'value' : '1'},
	                {'name' : '国内当地参团', 'value' : '2'},
	                {'name' : '出境当地参团', 'value' : '3'},
	                {'name' : '出境短线', 'value' : '4'},
                    {'name' : '出境长线', 'value' : '5'},
	            ],
            }
    $scope.pt = {
        'info':[
                    {'name' : '周边', 'value' : '0'},
	                {'name' : '国内长线', 'value' : '1'},
	                {'name' : '国内当地参团', 'value' : '2'},
	                {'name' : '出境当地参团', 'value' : '3'},
	                {'name' : '出境短线', 'value' : '4'},
                    {'name' : '出境长线', 'value' : '5'},

        ]
    }
            //正则判断电话号
            $scope.checkphone=function(x){
                if(!/^1[34578]\d{9}$/.test(x)) return "您输入的电话号长度或格式错误";
            }
            $scope.showphoneinfo=function(x){
                console.log(x);
                if(x==undefined){
                    $scope.employee.phoneinfo="";
                   return;
                  }
                if(x==''){
                    $scope.employee.phoneinfo="";
                   return;
                  }
                                         
            if($scope.checkphone(x)=="您输入的电话号长度或格式错误"){                
                $scope.employee.phoneinfo="您输入的电话号长度或格式错误"
            }else{
                     $scope.employee.phoneinfo="";
                    }
            }


     			$scope.addemployee = function(){
                    if($scope.product_type=='0'){
                       $scope.product_type1='周边游'
                    };
                    if($scope.product_type=='1'){
                        $scope.product_type1='国内长线'
                    };
                    if($scope.product_type=='2'){
                        $scope.product_type1='国内当地参团'
                    };
                    if($scope.product_type=='3'){
                        $scope.product_type1='出境当地参团'
                    };
                    if($scope.product_type=='4'){
                        $scope.product_type1='出境短线'
                    };
                    if($scope.product_type=='5'){
                        $scope.product_type1='出境长线'
                    };  
                        
				 for(var i = 0;i<$scope.ptarr.length;i++){
                     if($scope.product_type==$scope.ptarr[i].name){
                         alert('不可重复添加数据！');
                         return;
                     }
                 }        
                $scope.ptarr.push({name:$scope.product_type1});
                $scope.ptarr2.push({name:$scope.product_type});    
                console.log($scope.ptarr);
                console.log($scope.ptarr2);
							

				

			};       
       
    $scope.cancel = function () {  
        $modalInstance.dismiss('cancel'); 
    };
    $scope.gogogo = function () {
       console.log($scope.ptarr2);
        $scope.stptarr2='';
        for(var i = 0;i<$scope.ptarr2.length;i++){
            if(i<$scope.ptarr2.length-1){
                $scope.stptarr2+=$scope.ptarr2[i].name+',';
            }else{
                $scope.stptarr2+=$scope.ptarr2[i].name;
            }
        }
        console.log($scope.stptarr2);
        console.log('上面是$scope.stptarr2')

        $scope.obj['name']=$scope.name;
        $scope.obj['mobile']=$scope.mobile;
        $scope.obj['product_type']=$scope.stptarr2;
        
        
        if($scope.name==undefined){
            alert('请输入联系人姓名');
            return;
        }
        if($scope.mobile==undefined){
            alert('请输入联系人电话号码');
            return;
        }
        if($scope.employee.phoneinfo!=''){
            alert('请输入正确的电话号码！');
            return;
        }
        
       
        $resource('/api/as/lc/employee/create', {}, {}).save($scope.obj, function(res){
            // console.log(res);
            if(res.errcode === 0){
                $modalInstance.close($scope.result);
                alert('保存成功');
            }else{
                alert(res.errmsg);
            }
        });
    };
};