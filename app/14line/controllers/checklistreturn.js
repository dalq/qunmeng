module.exports = function($scope, $state, $stateParams, $modalInstance, items, $resource){

    $scope.cancel = function () {  
        $modalInstance.dismiss('cancel'); 
    };
    $scope.gogogo = function () {
        if($scope.linereject.$valid){
            //alert('通过验证可以提交表单');

        }else{
            //alert('表单没有通过验证');

            return;
        }

        items['back_setup_reason']=$scope.qwe;
        items['product_id']=items.id;
        $resource('/api/ac/lc/productState/setUpBack', {}, {}).save(items, function(res){
            if(res.errcode === 0){
                $modalInstance.close($scope.result);
                
                $state.go('app.line_checklist',{});
                alert('驳回成功');
            }else{
                alert(res.errmsg);
            }
        });
    };
};