module.exports = function($scope, formconfig, $stateParams, cautionmodel){
	// var id = $stateParams.id;
	formconfig.start({
		'title' : '创建信息',
		'formtitle' : '购买须知',
		'elements' : cautionmodel(),
		'save' : {
			'url' : '/api/ac/tc/placeView/create',
			'to' : 'app.linelist',
			'para' : {'type' : 'J'}
		}
	}, $scope);

};