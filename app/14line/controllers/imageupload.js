module.exports = function($scope, FileUploader, $modalInstance){

	

	var uploader = $scope.uploader = new FileUploader({
        url: 'http://cl.juyouhx.com/oss.php/oss/webuploader1?topdir=aa1&selfdir=bb1'
    });

    uploader.filters.push({
        name: 'imageFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });
    
    $scope.ok = function () {  
        var imagearr = [];
        for(var i = 0; i < uploader.queue.length; i++){
            var tmp = uploader.queue[i];
            if(tmp.isSuccess){
                var txt = tmp._xhr.responseText;
                var json = angular.fromJson(txt);
                imagearr.push(json.savename);
            }
        }
        $modalInstance.close(imagearr);
    };  
    $scope.cancel = function () {  
        $modalInstance.dismiss('cancel');  
    };  

};
