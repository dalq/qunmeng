/**
 * 模态框
 */
module.exports = function ($scope, $state, $stateParams,$resource,$modalInstance,items) {
	/* 分页
     * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.currentPage = 1;      //当前页码
    $scope.itemsPerPage = 6;
           //每页显示几条
    $scope.setgroupdate=null;
	$scope.pageChanged = function () {
       var aa=$scope.setgroupdate;
       (aa == '') ? aa=null : aa=$scope.setgroupdate;
       var para = {
            pageNo:$scope.currentPage,
            pageSize:6,
            product_id:items.id,
            close_group : aa,
        };
        // console.log(para);
        $resource('/api/as/lc/groupDatesLog/findinfoList', {}, {}).
			get(para, function(res){
				// console.log('我的数据');
                // console.log(res);
                if(res.errcode === 0 || res.errcode === 10003){
                    $scope.a={};
                    $scope.a=res.data.results;
                    $scope.totalItems = res.data.totalRecord;
                    for(var i = 0;i < res.data.results.length; i++) {
                        $scope.a[i].salled=res.data.results[i].stock_totals-res.data.results[i].stock;
                        if(res.data.results[i].adult_ban==1){
                            $scope.a[i].adultbanno=false;
                            $scope.a[i].adultbanyes=true;
                            $scope.a[i].adult_ban="停售"
                        }else{
                            $scope.a[i].adultbanno=true;
                            $scope.a[i].adultbanyes=false;
                        }
                        if(res.data.results[i].children_ban==1){
                            $scope.a[i].childrenbanno=false;
                            $scope.a[i].childrenbanyes=true;
                            $scope.a[i].children_ban="停售"
                        }else{
                            $scope.a[i].childrenbanno=true;
                            $scope.a[i].childrenbanyes=false;
                        }
                        if(res.data.results[i].single_room_ban==1){
                            $scope.a[i].single_roombanno=false;
                            $scope.a[i].single_roombanyes=true;
                            $scope.a[i].single_room_ban="停售"
                        }else{
                            $scope.a[i].single_roombanno=true;
                            $scope.a[i].single_roombanyes=false;
                        }
                        if(res.data.results[i].stock_type==0){
                            $scope.a[i].stocknoshowzero=false;
                        }else{
                            $scope.a[i].stocknoshowzero=true;
                        }
                        if(res.data.results[i].stock_type==0){
                            $scope.a[i].stock_type="未知"
                        };
                        if(res.data.results[i].stock_type==1){
                            $scope.a[i].stock_type="共有"
                        };
                        if(res.data.results[i].stock_type==2){
                            $scope.a[i].stock_type="对接"
                        };
                        if(res.data.results[i].back_rule_type==0){
                            $scope.a[i].back_rule_type="不退不改"
                        }else{
                            $scope.a[i].back_rule_type="人工退改"
                        };
                        if(res.data.results[i].stock_type=="未知"){
                            $scope.a[i].stockstt=false;
                        }else{
                            $scope.a[i].stockstt=true;
                        };
                        if(res.data.results[i].close_group==1){
                            $scope.a[i].close_group="已停售"
                            $scope.a[i].btncolor="btn m-b-xs w-xs btn-danger"
                        }else{
                            $scope.a[i].close_group="正在销售"
                            $scope.a[i].btncolor="btn m-b-xs w-xs btn-success"
                        }
                    }
                }else{
                    alert(res.errmsg);
                }
            });
    };
	$scope.pageChanged();
    $scope.setgroup01=function(item){
        // console.log(item.product_state_name);
        if(item.product_state_name=="草稿"){
            if(item.close_group=="正在销售"){
            if(confirm("您现在的状态为草稿状态，确定要停售吗？")){
                var para1 = {
            close_group:'0',
            tour_date:item.tour_date,
            product_id:items.id
            };
             $resource('/api/as/lc/groupDatesLog/setGroupStop', {}, {}).
                save(para1, function(res){
                    // console.log(res);
                    if(res.errcode=='0'){

                        $scope.pageChanged();
                    }else{
                    alert(res.errmsg);
                }
            });
            }
        }
        if(item.close_group=="已停售"){
            if(confirm("您现在的状态为草稿状态，确定要开始销售吗？")){
                var para2 = {
            close_group:'1',
            tour_date:item.tour_date,
            product_id:items.id
           };
             $resource('/api/as/lc/groupDatesLog/setGroupOnSale', {}, {}).
                get(para2, function(res){
                    if(res.errcode=='0'){

                       $scope.pageChanged();
                    }else{
                    alert(res.errmsg);
                }

            });
            }

        }
        };
        //~~~~~~~~~~~~~~~~~~~~~
        if(item.product_state_name=="待上架"){
            alert("您现在的状态为待上架，不可修改销售状态！");
        };
        //~~~~~~~~~~~~~~~~~~~~~
        if(item.product_state_name=="已上架"){
            if(item.close_group=="正在销售"){
            if(confirm("您现在的状态为已上架状态，确定要停售吗？")){
                var para3 = {
            close_group:'0',
            tour_date:item.tour_date,
            product_id:items.id
            };
             $resource('/api/as/lc/groupDatesLog/setGroupStop', {}, {}).
                save(para3, function(res){
                    // console.log(res);
                    if(res.errcode=='0'){
                        $scope.pageChanged();
                    }else{
                    alert(res.errmsg);
                }
            });
            }
        }
        if(item.close_group=="已停售"){
            if(confirm("您现在的状态为上架状态，确定要开始销售吗？")){
                var para2 = {
            close_group:'1',
            tour_date:item.tour_date,
            product_id:items.id
           };
             $resource('/api/as/lc/groupDatesLog/setGroupOnSale', {}, {}).
                get(para2, function(res){
                    if(res.errcode=='0'){
                       $scope.pageChanged();
                    }else{
                    alert(res.errmsg);
                }

            });
            }

        }
        };
        //~~~~~~~~~~~~~~~~~~~~~~
        if(item.product_state_name=="已下架"){
            if(item.close_group=="正在销售"){
            if(confirm("您现在的状态为已下架状态，确定要停售吗？")){
                var para1 = {
            close_group:'0',
            tour_date:item.tour_date,
            product_id:items.id
            };
             $resource('/api/as/lc/groupDatesLog/setGroupStop', {}, {}).
                save(para1, function(res){
                    // console.log(res);
                    if(res.errcode=='0'){
                        $scope.pageChanged();
                    }else{
                    alert(res.errmsg);
                }
            });
            }


        }
        if(item.close_group=="已停售"){
            if(confirm("您现在的状态为下架状态，确定要开始销售吗？")){
                var para2 = {
            close_group:'1',
            tour_date:item.tour_date,
            product_id:items.id
           };
             $resource('/api/as/lc/groupDatesLog/setGroupOnSale', {}, {}).
                get(para2, function(res){
                    if(res.errcode=='0'){
                       $scope.pageChanged();
                    }else{
                    alert(res.errmsg);
                }

            });
            }
        }
        };
        //~~~~~~~~~~~~~~~~~~
    }
            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        //保存方法
            $scope.ok=function(){
                //alert(items.id);
                //alert($scope.a[2].stock_totals);
                if(confirm("是否确定保存")){
                     var savestock={product_id:items.id};
                var list=[];
                 for(var i = 0;i < $scope.a.length; i++) {
                    if($scope.a[i].stock_type=="共有" && $scope.a[i].stock_totals>=$scope.a[i].salled){
                        list.push({tour_date:$scope.a[i].tour_date,stock_totals:$scope.a[i].stock_totals})                                                   
                    }else{
                    };
                    if($scope.a[i].stock_type=="共有" && $scope.a[i].stock_totals<$scope.a[i].salled){
                        // else{
                         alert("库存总量不可小于销售量");
                         return;
                     };

                 };
                 savestock.list=list;
                 if(list=="" && $scope.a==""){                 
                      alert("价格库存信息未录入");     
                 }else{
                    //   console.log(savestock);
                            $resource('/api/ac/lc/groupDatesService/create', {}, {}).
                                save(savestock, function(res){
                                    // console.log(res);
                                    if(res.errcode=='0'){
                                        $scope.pageChanged();
                                    }else{
                                        alert(res.errmsg);
                                }
                        });
                 }
                  
                }
               
            }
			$scope.search=function(){
			var dic = {
                pageNo:$scope.currentPage,
                pageSize:$scope.itemsPerPage,
                product_id:items.id,
				close_group : $scope.setgroupdate
			}
				/*console.log('123123');
				console.log(dic);*/
				$resource('/api/as/lc/groupDatesLog/findinfoList', {}, {}).save(dic, function(res){
                if(res.errcode === 0 || res.errcode === 10003){
                    $scope.a={};
                    $scope.a=res.data.results;
                    $scope.totalItems = res.data.totalRecord;
                    for(var i = 0;i < res.data.results.length; i++) {
                        if(res.data.results[i].adult_ban==1){
                            $scope.a[i].adultbanno=false;
                            $scope.a[i].adultbanyes=true;
                            $scope.a[i].adult_ban="停售"
                        }else{
                            $scope.a[i].adultbanno=true;
                            $scope.a[i].adultbanyes=false;
                        }
                        if(res.data.results[i].children_ban==1){
                            $scope.a[i].childrenbanno=false;
                            $scope.a[i].childrenbanyes=true;
                            $scope.a[i].children_ban="停售"
                        }else{
                            $scope.a[i].childrenbanno=true;
                            $scope.a[i].childrenbanyes=false;
                        }
                        if(res.data.results[i].single_room_ban==1){
                            $scope.a[i].single_roombanno=false;
                            $scope.a[i].single_roombanyes=true;
                            $scope.a[i].single_room_ban="停售"
                        }else{
                            $scope.a[i].single_roombanno=true;
                            $scope.a[i].single_roombanyes=false;
                        }
                         if(res.data.results[i].stock_type==0){
                            $scope.a[i].stock_type="未知"
                        };
                        if(res.data.results[i].stock_type==1){
                            $scope.a[i].stock_type="共有"
                        };
                        if(res.data.results[i].stock_type==2){
                            $scope.a[i].stock_type="对接"
                        };
                        if(res.data.results[i].stock_type=="未知"){
                            $scope.a[i].stockstt=false;
                        }else{
                            $scope.a[i].stockstt=true;
                        };
                        if(res.data.results[i].back_rule_type==0){
                            $scope.a[i].back_rule_type="不退不改"
                        }else{
                            $scope.a[i].back_rule_type="人工退改"
                        };
                        if(res.data.results[i].close_group==1){
                            $scope.a[i].close_group="已停售"
                            $scope.a[i].btncolor="btn m-b-xs w-xs btn-danger"
                        }else{
                            $scope.a[i].close_group="正在销售"
                            $scope.a[i].btncolor="btn m-b-xs w-xs btn-success"
                        }

                }
                }else{
                    alert(res.errmsg);
                }
            })
            }
}
