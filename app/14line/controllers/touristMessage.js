/**
 * 模态框
 */
module.exports = function ($scope, $state, $resource, $stateParams, $modalInstance, $http, items,Excel,$timeout) {

    /* 分页
     * ========================================= */
    $scope.maxSize = 5;            //最多显示多少个按钮
    $scope.currentPage = 1;      //当前页码
    $scope.itemsPerPage = 10;         //每页显示几条
    // $scope.bigTotalItems = 10;
    $scope.firstitem=items;
     $scope.didianarr=[];
     $scope.show=1;
    // console.log( $scope.firstitem);
    $scope.load = function () {
        var para = {
            pageNo: $scope.currentPage,
            pageSize: 10,
            product_id: items.product_id,
            tour_date: items.tour_date
        };
        $resource('/api/ac/lc/sendMessageService/findTourstInfoList', {}, {}).save(para, function (res) {
            // console.log(res);
            if (res.errcode === 0) {
                $scope.objs = res.data.results;
                
                $scope.totalItems = res.data.totalRecord;
                
            } else {
                alert(res.errmsg);
            }
        });

        $resource('/api/ac/lc/sendMessageService/findTourstInfosList', {}, {}).save({'product_id': $scope.firstitem.product_id , 'tour_date' : $scope.firstitem.tour_date}, function (res) {
            // console.log(res);
            if (res.errcode === 0) {
                $scope.objs1 = res.data;
                $scope.allpeople=$scope.objs1.length;
                if(!$scope.objs1.order_remark || $scope.objs1.order_remark=='' || $scope.objs1.order_remark=='无'){
                    $scope.show=0;
                }
                $scope.totalItems1 = res.data.totalRecord;
                
            } else {
                alert(res.errmsg);
            }
        });
         $resource('/api/as/lc/sendmessage/findGuide', {}, {}).
			get({'product_id': $scope.firstitem.product_id , 'tour_date' : $scope.firstitem.tour_date}, function(res){
                if(res.errcode === 0 || res.errcode === 10003){
					$scope.wjq=res.data;
                    // console.log($scope.wjq);
                	
                }else{
                    alert(res.errmsg);
                }
            });
    }
    $scope.load();

    $resource('/api/ac/lc/sendMessageService/findTourstPlace', {}, {}).save({ product_id: items.product_id }, function (res) {
        // console.log(res);
        if (res.errcode === 0) {
            $scope.selectArray = res.data.assembling_place.split('%');
            
        } else {
            alert(res.errmsg);
        }
    });

    $scope.ok = function () {

        var results = [];
        for (var index = 0; index < $scope.objs.length; index++) {
            results.push({});
            results[index].lot_id = $scope.objs[index].lot_id;
            results[index].assembling_place = $scope.objs[index].assembling_place;
            $scope.timearr = $scope.objs[index].assembling_place.split('^');
            $scope.didianarr.push($scope.timearr);
        }

        var para = {
            list: results
        }
        $resource('/api/ac/lc/sendMessageService/updateTourstPlace', {}, {}).save(para, function (res) {
            if (res.errcode === 0) {
                alert('保存成功');
                $modalInstance.close($scope.result);
            } else {
                alert(res.errmsg);
            }
        });
    };
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };


    $scope.exportToExcel = function (tableId) {
         for (var index = 0; index < $scope.objs.length; index++) {
           
            $scope.timearr = $scope.objs[index].assembling_place.split('^');
            $scope.didianarr.push($scope.timearr);
        }
		// console.log($scope.wjq);
        if($scope.wjq){
            if($scope.wjq.contacts_name==undefined ||$scope.wjq.contacts_name=='' ){
                alert('导游信息未填写完整，无法导出');
                return;
            }
        }else{
            alert('导游信息未填写完整，无法导出');
            return;
        }
        
		$timeout(function () {
            
             document.getElementById("dlink").href = Excel.tableToExcel(tableId, 'sheet1');
            //document.getElementById("dlink").download = "1122b.xls";//这里是关键所在,当点击之后,设置a标签的属性,这样就可以更改标签的标题了
            document.getElementById("dlink").click();
        
        }, 100); // trigger download

       
	}
    

   

};