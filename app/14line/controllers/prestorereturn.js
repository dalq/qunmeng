module.exports = function($scope, $state, $stateParams, $modalInstance, items, $resource){
   
    $scope.item=items;
    // console.log(items);

    $scope.money=$scope.item.money;
    $scope.obj={};
    $scope.cancel = function () {  
        $modalInstance.dismiss('cancel'); 
    };

    $scope.clearpeopleNoNum = function(obj,attr){
        if(obj[attr]>$scope.money){
            alert('充值金额不能大于账户余额');
            obj[attr]=$scope.money;
        }
        if(obj[attr]){
            $scope.item.money=$scope.money-obj[attr]; 
        }else{
            $scope.item.money=$scope.money;
        }
        // console.log(obj[attr]);
    }

    $scope.gogogo = function () {
        if($scope.messageinfo.$valid){
            //alert('通过验证可以提交表单');
        }else{
            //alert('表单没有通过验证');
            return;
        }
        $scope.obj['company_code']=items.company_code;
        $scope.obj['parent_code']=items.parent_code;

        $resource('/api/ac/pc/payBmAccountService/updateRechargeBySellerCode', {}, {}).save($scope.obj, function(res){
            // console.log(res);
            if(res.errcode === 0){
                $modalInstance.close($scope.result);
                alert('保存成功');
            }else{
                alert(res.errmsg);
            }
        });
    };
};