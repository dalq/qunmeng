module.exports = function($scope, formconfig, $stateParams, viewmodel){

	var id = $stateParams.id;
	
	alert($stateParams.aaa);
	formconfig.start({
		'title' : '我的详情',
		'formtitle' : '我的基本信息',
		'elements' : viewmodel(),
		'info' : {
			'url' : '/api/ac/tc/placeView/info',
			'para' : {'id' : id}
		},
		'save' : {
			'url' : '/api/ac/tc/placeView/update',
			'to' : 'app.line_list',
			'para' : {'id' : id, 'type' : 'J'}
		}
	}, $scope);

	$scope.form = formconfig;

};