/**
 * 模态框
 */
module.exports = function ($scope, $state, $stateParams,$resource,$modal) {
	/* 分页预存管理列表
     * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.currentPage = 1;      //当前页码
    $scope.itemsPerPage = 10         //每页显示几条
    $scope.item={};
    $scope.wjq=1;

	$scope.pageChanged = function () {
         $resource('/api/as/pc/bmaccount/getSellerInfoByCode', {}, {})
        .save({}, function (res) {
            // console.log('下面是上车地点');
            // console.log(res);
            // console.log('上面是上车地点');
            $scope.item['parent_code']=res.data.parent_code;
            $scope.balance_price=res.data.balance_price;
            if(res.data.seller_code){
                if(res.data.seller_code=='LA'){
                    $scope.wjq=0;
                }
            }
           
            if (res.errcode == 0) {
                
            } else {
                alert(res.errmsg);
            }
        });

        $scope.b= '0';
        $scope.c = '';
        $scope.show = function(index){
            if($scope.b==0){
                $scope.b='1',
                $scope.c=index
            }else if($scope.b==1){
                if($scope.c!=index){
                    $scope.c=index
                }else{
                    $scope.b='0',
                    $scope.c=index
                }	
            }	
        };
       var para = {
        //    ta:1,
            pageNo:$scope.currentPage,
            pageSize:$scope.itemsPerPage,
            product_id : $scope.product_id,
            product_name : $scope.product_name,
            supplier_product_name : $scope.supplier_product_name,
            tour_date :  $scope.tour_date,
			tour_date_two :  $scope.tour_date_two,
            send_state : $scope.send_state,
        };
        $resource('/api/ac/pc/payBmAccountService/sellerInfoList', {}, {}).
			save(para, function(res){
				// console.log('购买须知');
                // console.log(res);
                if(res.errcode === 0 || res.errcode === 10003){
					$scope.a=res.data.results;
                    // console.log($scope.a);
					$scope.totalItems = res.data.totalRecord;

                }else{
                    alert(res.errmsg);
                }
            });

    };
	$scope.pageChanged();

	
			

		
			$scope.search=function(){
               
                
                                      
			var dic = {
				seller_code : $scope.product_id,
				
			}
            
            // console.log(dic);
				$resource('/api/ac/pc/payBmAccountService/sellerInfoList', {}, {}).save(dic, function(res){
                if(res.errcode === 0 || res.errcode === 10003){
					$scope.a=res.data.results;
                    $scope.totalItems = res.data.totalRecord;
                }else{
                    alert(res.errmsg);
                }
            });
			}
           
            

            $scope.edit = function (index) {
               $scope.item = $scope.a[index];
            //    $scope.item.save_type=$scope.a[pindex].sellerlist[index].save_type;
                var modalInstance = $modal.open({
                    template: require('../views/prestoreedit.html'),
                    controller: 'lineprestoreedit',
                    size: 'lg',
                    resolve: {
                        items: function () {
                            return $scope.item;
                        }
                    }
                });
                modalInstance.result.then(function (showResult) {
                    $scope.pageChanged();
                });
            }
            $scope.chuangjian = function () {
                // console.log('adsad');
                var modalInstance = $modal.open({
                    template: require('../views/prestoresteup.html'),
                    controller: 'lineprestoresteup',
                    size: 'lg',
                    resolve: {
                        items: function () {
                            return $scope.item;
                        }
                    }
                });
                 modalInstance.result.then(function (showResult) {
                    $scope.pageChanged();
                });
            }
            $scope.returnmonery = function () {
                 $scope.item['company_code'] = 'tuihui'; 
                 $scope.item['money'] =$scope.balance_price;                  
                var modalInstance = $modal.open({
                    template: require('../views/prestorereturn.html'),
                    controller: 'lineprestorereturn',
                    size: 'lg',
                    resolve: {
                        items: function () {
                            return $scope.item;
                        }
                    }
                });
                 modalInstance.result.then(function (showResult) {
                    $scope.pageChanged();
                });
            }
            $scope.returnmonery1 = function (index) {
                 $scope.item = $scope.a[index];
                //  console.log($scope.a[pindex]);
                  $scope.item['company_code'] = $scope.a[index].seller_code; 
                //  $scope.item.save_type=$scope.a[pindex].sellerlist[index].save_type;
                 $scope.item['money'] = $scope.balance_price;   
                var modalInstance = $modal.open({
                    template: require('../views/prestorereturn.html'),
                    controller: 'lineprestorereturn',
                    size: 'lg',
                    resolve: {
                        items: function () {
                            return $scope.item;
                        }
                    }
                });
                 modalInstance.result.then(function (showResult) {
                    $scope.pageChanged();
                });
            }

            $scope.reject = function (index) {
                $scope.item = $scope.a[index];
                var modalInstance = $modal.open({
                    template: require('../views/messageinfo.html'),
                    controller: 'linemessageinfo',
                    size: 'lg',
                    resolve: {
                        items: function () {
                            return $scope.item;
                        }
                    }
                });
                modalInstance.result.then(function (showResult) {
                    $scope.pageChanged();
                });
            }

            


};
