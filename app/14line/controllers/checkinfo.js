
module.exports = function($scope,items, $stateParams, $http, $q, FileUploader, lineinfomodel, linecontainmodel,linecautionmodel,lineexplainmodel, linetripmodel, linepicturemodel, linenocontainmodel, lineactivitymodel,lineallinformationmodel){

	var product_id = $stateParams.id ? $stateParams.id : '';
	

	$scope.lineallinformationmodel = lineallinformationmodel();

    $scope.util = {

		'product_id' : items.id,//保存完产品基本信息后有值。

		//初始化图片和日期。
		'initother' : function(config){
			var model = config.model;
			var result = config.result;
			var dateshow = config.dateshow;
			var imageshow = config.imageshow;

			for(var i = 0; i < model.length; i++){
				var tmp = model[i];

				makedate(dateshow, tmp);
				makeimg(imageshow, tmp, result);
			}
		},

		//子页的初始化方法。
		'init' : function(config){
			var model = config.model;
			var result = config.result;
			var dateshow = config.dateshow;
			var imageshow = config.imageshow;
			var info = config.info;

			for(var i = 0; i < model.length; i++){
				var tmp = model[i];

				if(tmp.type === 'checkbox'){
					result[tmp.id] = {};
					for(var j = 0; j < tmp.info.length; j++)
                    {
                        result[tmp.id][tmp.info[j].value] = tmp.info[j].check ? true : false;
                    }

                    if(angular.isDefined(info[tmp.id])){
                    	var dd = info[tmp.id].split(',');
	    				for(var m = 0; m < dd.length; m++){
	    					if(dd[m] != ''){
	    						result[tmp.id][dd[m]] = true;
	    					}
	    				}
                    }
    			}else{

					if(angular.isDefined(info[tmp.id])){
						result[tmp.id] = info[tmp.id];
					} else if(angular.isDefined(tmp.value)){
						result[tmp.id] = tmp.value;
					}
				}
			}
		},

	};
	

	function date2str(objDate){
		if(angular.isDate(objDate))
		{
			var y = objDate.getFullYear();
			var m = objDate.getMonth();
			var d = objDate.getDate();

			return y + '-' + (m + 1) + '-' + d;
		}
		else
		{
			return '错误格式';
		}
	}

	function makedate(dateshow, tmp){
		if(tmp.type !== 'date1'){
			return ;
		}

		var dddd = '';

        // if(angular.isDefined(goodsinfo)){
        //     dddd = goodsinfo[tmp2];
        // } else {
            dddd = date2str(new Date());
        //}

        dateshow[tmp.id] = {
            'label' : dddd,
            'opened' : false,
        };
	}

	function makeimg(imageshow, tmp, result){
		if(tmp.type !== 'image'){
			return;
		}

		(function(tmp){

            var uploader = new FileUploader({
                url: 'http://cl.juyouhx.com/oss.php/oss/webuploader1?topdir=aa&selfdir=bb'
            });

            imageshow[tmp] = {
                'uploader' : uploader
            };

            uploader.filters.push({
                name: 'imageFilter',
                fn: function(item, options) {
                    var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                    return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
                }
            }); 

            uploader.onSuccessItem = function(fileItem, response, status, headers) {
                result[tmp] = response.savename;
            };

        })(tmp.id);

	}

};