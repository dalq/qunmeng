/**
 * 模态框
 */
module.exports = function ($scope, $state, $stateParams,$resource,$modal) {
	/* 分页
     * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.currentPage = 1;      //当前页码
    $scope.itemsPerPage = 10;        //每页显示几条
	// $scope.product_state='1'
	 
	 $scope.d={};

	$scope.pageChanged = function () {
       var para = {
        //    ta:1, 
            pageNo:$scope.currentPage, 
            pageSize:$scope.itemsPerPage,
			// product_state:  $scope.product_state,
			product_id : $scope.id,
			product_name : $scope.product_name,
			product_sub_name : $scope.product_sub_name,
			start_city_arr : $scope.start_city_arr,
			end_city : $scope.end_city,
			product_state : $scope.d.product_state,
			product_type : $scope.product_type,


        };
        $resource('/api/as/lc/product/findSuppliersinfolist', {}, {}).
			save(para, function(res){
                if(res.errcode === 0 || res.errcode === 10003){
					// console.log(res);
					$scope.a=res.data.results;
					 $scope.totalItems = res.data.totalRecord;
					//$scope.aa = [];
                	//for(var i = 0;i<$scope.a.length;i++){
					//	if($scope.a[i].product_state_name =='待上架'){
					//		$scope.aa.push($scope.a[i]);
					//	}
					//}
					//$scope.totalItems = $scope.aa.length;
                }else{
                    alert(res.errmsg);
                }
            });

    };
	$scope.pageChanged();

			$scope.b= '0';
			$scope.c = '';
			$scope.show = function(index){
				if($scope.b==0){
					$scope.b='1',
					$scope.c=index
				}else if($scope.b==1){
					if($scope.c!=index){
						$scope.c=index
					}else{
						$scope.b='0',
						$scope.c=index
					}	
				}	
			};

			$scope.psarr=[
				{name:'请选择',value:''},
                {name:'草稿',value:'0'},
                {name:'待上架',value:'1'},
                {name:'已上架',value:'2'},
                {name:'已下架',value:'3'},
                {name:'已驳回',value:'4'} 
            ]

			
			$scope.search=function(){
			var dic = {
				product_id : $scope.id,
				product_name : $scope.product_name,
				product_sub_name : $scope.product_sub_name,
				start_city_arr : $scope.start_city_arr,
				end_city : $scope.end_city,
				product_state : $scope.d.product_state,
				product_type : $scope.product_type,
				
			}
		
				$resource('/api/as/lc/product/findSuppliersinfolist', {}, {}).save(dic, function(res){
                if(res.errcode === 0 || res.errcode === 10003){
					$scope.a=res.data.results;
                	$scope.totalItems = res.data.totalRecord;
                }else{
                    alert(res.errmsg);
                }
            });
			}

			$scope.bonus = function (index) {
				$scope.item = $scope.a[index];
				var modalInstance = $modal.open({
					template: require('../views/line_bonus.html'),
					controller: 'linebonus',
					size: 'lg',
					resolve: {
						items: function () {
							return $scope.item;
						}
					}
				});
				modalInstance.result.then(function (showResult) {	
						$scope.pageChanged();
				});
			}
           	$scope.toinfo = function (index) {
				$scope.item = $scope.a[index];
				$scope.item.cc = "0";
				var modalInstance = $modal.open({
					template: require('../views/checklistinfo.html'),
					controller: 'linechecklistinfo',
					size: 'lg',
					resolve: {
						items: function () {
							return $scope.item;
						}
					}
				});
				modalInstance.result.then(function (showResult) {	
						$scope.pageChanged();
				});
			} 
            
};
