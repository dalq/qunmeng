/**
 * 模态框
 */
module.exports = function ($scope, $state, $stateParams,$resource,$modal) {
	/* 分页
     * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.currentPage = 1;      //当前页码
    $scope.itemsPerPage = 10         //每页显示几条
    $scope.d={};


	$scope.pageChanged = function () {
       var para = {
        //    ta:1,
            pageNo:$scope.currentPage,
            pageSize:$scope.itemsPerPage,
            product_id : $scope.id,
            product_name : $scope.product_name,
            product_type : $scope.d.product_type,
            supplier_product_name : $scope.supplier_product_name,
            start_city_arr : $scope.start_city_arr,
            product_state : $scope.d.product_state,
            
        };
        		// console.log(para);

        $resource('/api/as/lc/product/findinfolist', {}, {}).
			save(para, function(res){
				
                if(res.errcode === 0 || res.errcode === 10003){
					$scope.a=res.data.results;
					$scope.totalItems = res.data.totalRecord;

                }else{
                    alert(res.errmsg);
                }
            });

    };
	$scope.pageChanged();

			$scope.b= '0';
			$scope.c = '';
			$scope.show = function(index){
				if($scope.b==0){
					$scope.b='1',
					$scope.c=index
				}else if($scope.b==1){
					if($scope.c!=index){
						$scope.c=index
					}else{
						$scope.b='0',
						$scope.c=index
					}
				}
			};

            $scope.ptarr=[
                {name:'请选择',value:''},
                {name:'周边游',value:'0'},
                {name:'国内长线',value:'1'},
                {name:'国内当地参团',value:'2'},
                {name:'出境当地参团',value:'3'},
                {name:'出境短线',value:'4'},
                {name:'出境长线',value:'5'}          
            ]

            $scope.psarr=[
				{name:'请选择',value:''},
                {name:'草稿',value:'0'},
                {name:'待上架',value:'1'},
                {name:'已上架',value:'2'},
                {name:'已下架',value:'3'},
                {name:'已驳回',value:'4'} 
            ]


			$scope.search=function(){
			var dic = {

				product_id : $scope.id,
				product_name : $scope.product_name,
				product_type : $scope.d.product_type,
				supplier_product_name : $scope.supplier_product_name,
				start_city_arr : $scope.start_city_arr,
                product_state : $scope.d.product_state,
			}
						// console.log(dic);
				$resource('/api/as/lc/product/findinfolist', {}, {}).save(dic, function(res){
                if(res.errcode === 0 || res.errcode === 10003){
                    // console.log(res);
					$scope.a=res.data.results;
                    $scope.totalItems = res.data.totalRecord;
                }else{
                    alert(res.errmsg);
                }
            });
			}


            $scope.goup = function (index) {
                $scope.item = $scope.a[index];
                var product_id = $scope.item.id;
                $resource('/api/ac/lc/productState/setApplyUp', {}, {}).save({ product_id: product_id }, function (res) {
                    if (res.errcode === 0) {
                        $scope.pageChanged();
                        alert('上架申请成功');
                    } else {
                        alert(res.errmsg);
                    }
                });

            };

            $scope.edit = function (index) {
                $scope.item = $scope.a[index];
                $state.go('app.line_edit1', {
                    id: $scope.item.id,
                    product_state_name: $scope.item.product_state_name
                });
            }
            //我的修改
            $scope.setgroupstate = function (index) {
               $scope.item = $scope.a[index];
                var modalInstance = $modal.open({
                    template: require('../views/setgroupstate.html'),
                    controller: 'setgroupstate',
                    size: 'lg',
                    resolve: {
                        items: function () {
                            return $scope.item;
                        }
                    }
                });
                modalInstance.opened.then(function () {
                });
                modalInstance.result.then(function (showResult) {
                }, function (reason) {
                    // click，点击取消，则会暑促cancel
                    $log.info('Modal dismissed at: ' + new Date());
                });
            }

            $scope.godown = function (index) {
                $scope.item = $scope.a[index];
                $resource('/api/ac/lc/productState/setDown', {}, {}).save({ product_id: $scope.item.id }, function (res) {
                    if (res.errcode === 0) {
                        $scope.pageChanged();
                        alert('下架成功');
                    } else {
                        alert(res.errmsg);
                    }
                });

            };

            $scope.copy = function (index) {
                $scope.item = $scope.a[index];
                var modalInstance = $modal.open({
                    template: require('../views/copy.html'),
                    controller: 'linecopy',
                    size: 'lg',
                    resolve: {
                        items: function () {
                            return $scope.item;
                        }
                    }
                });
                modalInstance.result.then(function (showResult) {
                    $scope.pageChanged();
                }, function (reason) {
				// click，点击取消，则会暑促cancel  
                    $scope.pageChanged();
				});
                
            }

            $scope.delegate = function (index) {
                if(!confirm("是否确认删除")){
                    return ;
                }
                $scope.item = $scope.a[index];
                $resource('/api/ac/lc/productService/deleteProduct', {}, {}).save({ product_id: $scope.item.id }, function (res) {
                    if (res.errcode === 0) {
                        $scope.pageChanged();
                        alert('删除成功');
                    } else {
                        alert(res.errmsg);
                    }
                });

            };

            $scope.toinfo = function (index) {
				$scope.item = $scope.a[index];
                $scope.item.cc = '1';
				var modalInstance = $modal.open({
					template: require('../views/checklistinfo.html'),
					controller: 'linechecklistinfo',
					size: 'lg',
					resolve: {
						items: function () {
							return $scope.item;
						}
					}
				});
				modalInstance.result.then(function (showResult) {	
						$scope.pageChanged();
				});
			}

            $scope.skset = function(index,id,code,name,guide_price,cost_price,productflag){
				$scope.item = $scope.a[index];                
                console.log($scope.item);
                console.log('ssssss');
                var modalInstance = $modal.open({
					template: require('../../29product/views/skprofit.html'),
					controller: 'skprofit',
					size: 'lg',
					resolve: {
                        item: function () {
                            return $scope.item;
                        },
                        id: function () {
                            return id;
                        },
                        code: function () {
                            return code;
                        },
                        name: function () {
                            return name;
                        },
                        guide_price: function () {
                            return guide_price;
                        },
                        cost_price: function () {
                            return cost_price;
                        },
                        productflag: function () {
                            return productflag;
                        },
                        what: function () {
					return 'edit';
				},
					}
				});
				modalInstance.result.then(function (showResult) {	
						$scope.pageChanged();
				});
            }
};

