/**
 * 模态框
 */
module.exports = function ($scope, $state, $stateParams,$resource,$modal) {
	/* 分页
     * ========================================= */
    $scope.maxSize = 5;             //最多显示多少个按钮
    $scope.currentPage = 1;      //当前页码
    $scope.itemsPerPage = 10         //每页显示几条
   $scope.a = {};


	$scope.pageChanged = function () {
       var para = {
        //    ta:1,
            pageNo:$scope.currentPage,
            pageSize:$scope.itemsPerPage,
            product_id : $scope.product_id,
            product_name : $scope.product_name,
            supplier_product_name : $scope.supplier_product_name,
            tour_date :  $scope.tour_date,
			tour_date_two :  $scope.tour_date_two,
            send_state : $scope.a.send_state,
        };
        console.log(para);
        $resource('/api/ac/lc/sendMessageService/findSendMessageList', {}, {}).
			save(para, function(res){
				// console.log('购买须知');
                // console.log(res);
                if(res.errcode === 0 || res.errcode === 10003){
					$scope.a=res.data.results;                      
                    for(var i=0;i<$scope.a.length;i++){
                        if($scope.a[i].touristlist.length==0){
                            $scope.a.splice(i,1);
                        }
                    }
                    console.log($scope.a);
                    $scope.totalItems = res.data.totalRecord;                  
                    for(var i=0;i<$scope.a.length;i++){
                        var guideslist=$scope.a[i].guideslist[0];
                        var touristlist=$scope.a[i].touristlist;
                        if(guideslist==undefined){
                            $scope.a[i].sgg=true;
                        }else{
                            // console.log(guideslist);
                            if(guideslist.car_num==undefined || guideslist.phone_num==undefined  ||  guideslist.contacts_name==undefined){
                                $scope.a[i].sgg=true;
                            }else{
                                $scope.a[i].sgs=true;
                            }                           //$scope.a[i].smsshow=true;
                        }
                        if(touristlist.length==0){
                            $scope.a[i].sng=true;                     
                        }else{
                            for(var j=0;j<touristlist.length;j++){
                                if(touristlist[j].assembling_place==undefined || touristlist[j].cardno==undefined 
                                    || touristlist[j].id==undefined || touristlist[j].lot_id==undefined 
                                    || touristlist[j].mobile==undefined || touristlist[j].name==undefined
                                    || touristlist[j].order_code==undefined || touristlist[j].sex_name==undefined
                                    || touristlist[j].tourist_type_name==undefined
                                ){
                                   $scope.a[i].sng=true;
                                }else{
                                   $scope.a[i].sns=true;
                                }                               
                            }
                        }
                        if($scope.a[i].sgs==true && $scope.a[i].sns==true){
                            $scope.a[i].smsshow=true;
                        }else{
                            $scope.a[i].smsnoshow=true;
                        }
                    }					
                }else{
                    alert(res.errmsg);
                }
            });

    };
	$scope.pageChanged();


			$scope.b= '0';
			$scope.c = '';
			$scope.show = function(index){
				if($scope.b==0){
					$scope.b='1',
					$scope.c=index
				}else if($scope.b==1){
					if($scope.c!=index){
						$scope.c=index
					}else{
						$scope.b='0',
						$scope.c=index
					}
				}
			};


			$scope.search=function(){
                if($scope.date.lable){
                    $scope.tour_date=date2str2($scope.date.lable);
                }else{
                    $scope.tour_date=''
                }
                if($scope.date1.lable){
                    $scope.tour_date_two=date2str2($scope.date1.lable);
                }else{
                    $scope.tour_date_two=''
                }
                
                if($scope.date.lable!=null && $scope.date1.lable!=null){
                    if($scope.tour_date>$scope.tour_date_two){
                        alert('开始日期不能大于结束日期');
                        return;
                    }
                }
                
                if($scope.date.lable){
                    if(!$scope.date1.lable){
                        alert('开始和结束日期必须同时输入进行搜索')
                        return;
                    }
                }
                if($scope.date1.lable){
                    if(!$scope.date.lable){
                        alert('开始和结束日期必须同时输入进行搜索')
                        return;
                    }
                }
                if($scope.tour_date==''){
                    $scope.tour_date=undefined;
                }
                if($scope.tour_date_two==''){
                    $scope.tour_date_two=undefined;
                }
                                      
			var dic = {
				product_id : $scope.product_id,
				product_name : $scope.product_name,
				supplier_product_name : $scope.supplier_product_name,
				tour_date :  $scope.tour_date,
				tour_date_two :  $scope.tour_date_two,
				send_state : $scope.a.send_state,
			}
            
             console.log(dic);
				$resource('/api/ac/lc/sendMessageService/findSendMessageList', {}, {}).save(dic, function(res){
                if(res.errcode === 0 || res.errcode === 10003){
					$scope.a=res.data.results;
                    $scope.totalItems = res.data.totalRecord;
                    for(var i=0;i<$scope.a.length;i++){
                        if($scope.a[i].touristlist.length==0){
                            $scope.a.splice(i,1);
                        }
                    }
                    for(var i=0;i<$scope.a.length;i++){
                        var guideslist=$scope.a[i].guideslist[0];
                        var touristlist=$scope.a[i].touristlist;
                        if(guideslist==undefined){
                            $scope.a[i].sgg=true;
                        }else{
                            // console.log(guideslist);
                            if(guideslist.car_num==undefined || guideslist.phone_num==undefined  ||  guideslist.contacts_name==undefined){
                                $scope.a[i].sgg=true;
                            }else{
                                $scope.a[i].sgs=true;
                            }                           //$scope.a[i].smsshow=true;
                        }
                        if(touristlist.length==0){
                            $scope.a[i].sng=true;                     
                        }else{
                            for(var j=0;j<touristlist.length;j++){
                                if(touristlist[j].assembling_place==undefined || touristlist[j].cardno==undefined 
                                    || touristlist[j].id==undefined || touristlist[j].lot_id==undefined 
                                    || touristlist[j].mobile==undefined || touristlist[j].name==undefined
                                    || touristlist[j].order_code==undefined || touristlist[j].sex_name==undefined
                                    || touristlist[j].tourist_type_name==undefined
                                ){
                                   $scope.a[i].sng=true;
                                }else{
                                   $scope.a[i].sns=true;
                                }                               
                            }
                        }
                        if($scope.a[i].sgs==true && $scope.a[i].sns==true){
                            $scope.a[i].smsshow=true;
                        }else{
                            $scope.a[i].smsnoshow=true;
                        }
                    }
                }else{
                    alert(res.errmsg);
                }
            });
			}
            $scope.gomessage = function (index) {
                if(!confirm("确认发送短信？")){
                    return ;
                }
                $scope.item = $scope.a[index];
                $resource('/api/ac/lc/sendMessageService/updateSendState', {}, {}).
                    get({ 'product_id': $scope.item.product_id , 'tour_date' : $scope.item.tour_date}, function (res) {
                        if (res.errcode === 0 || res.errcode === 10003) {
                            tableconfig.search();

                            alert('短信发送成功！');
                            $scope.pageChanged();
                            scope.util.init({
                                'model': scope.model,
                                'result': scope.result,
                                'dateshow': scope.dateshow,
                                'imageshow': scope.imageshow,
                                'info': res.errcode == 0 ? res.data : {},
                            });
                        } else {
                            alert(res.errmsg);
                        }
                    });
            }

            $scope.gotwomessage = function (index) {
                if(!confirm("确认再次发送短信？")){
                    return ;
                }
                 $scope.item = $scope.a[index];
                $resource('/api/as/lc/sendmessage/updateAgainSendState', {}, {}).
                    get({ 'product_id': $scope.item.product_id , 'tour_date' : $scope.item.tour_date}, function (res) {
                        if (res.errcode === 0 || res.errcode === 10003) {
                            tableconfig.search();

                            alert('短信发送成功！');
                            $scope.pageChanged();
                            scope.util.init({
                                'model': scope.model,
                                'result': scope.result,
                                'dateshow': scope.dateshow,
                                'imageshow': scope.imageshow,
                                'info': res.errcode == 0 ? res.data : {},
                            });
                        } else {
                            alert(res.errmsg);
                        }
                    });
            }

            $scope.touristMessage = function (index) {
               $scope.item = $scope.a[index];
                var modalInstance = $modal.open({
                    template: require('../views/touristMessage.html'),
                    controller: 'touristMessage',
                    size: 'lg',
                    resolve: {
                        items: function () {
                            return $scope.item;
                        }
                    }
                });
                modalInstance.opened.then(function () {
                });
                modalInstance.result.then(function (showResult) {
                }, function (reason) {
                    // click，点击取消，则会暑促cancel
                    $log.info('Modal dismissed at: ' + new Date());
                });
            }

            $scope.reject = function (index) {
                $scope.item = $scope.a[index];
                var modalInstance = $modal.open({
                    template: require('../views/messageinfo.html'),
                    controller: 'linemessageinfo',
                    size: 'lg',
                    resolve: {
                        items: function () {
                            return $scope.item;
                        }
                    }
                });
                modalInstance.result.then(function (showResult) {
                    $scope.pageChanged();
                });
            }

             $scope.date = {
                // 'lable': date2str2(new Date()),
                'value': date2str(new Date()),
                'opened': false
            }


             $scope.dateOpen = function ($event, item) {
                $event.preventDefault();
                $event.stopPropagation();
                item.opened = true;
            };

            $scope.date1 = {
                // 'lable': date2str2(new Date()),
                'value': date2str(new Date()),
                'opened': false
            }


             $scope.dateOpen1 = function ($event, item) {
                $event.preventDefault();
                $event.stopPropagation();
                item.opened = true;
            };

            function date2str2(d) {
                if (d === undefined) {
                    return "";
                }
                var month = (d.getMonth() + 1).toString();
                var day = d.getDate().toString();
                if (month.length == 1) month = '0' + month;
                if (day.length == 1) day = '0' + day;
                return d.getFullYear() + "-" + month + "-" + day;
            }

            function date2str(d) {
                if (d === undefined) {
                    return "";
                }
                var month = (d.getMonth() + 1).toString();
                var day = d.getDate().toString();
                if (month.length == 1) month = '0' + month;
                if (day.length == 1) day = '0' + day;
                return d.getFullYear() + month + day;
            }
         $scope.start = {
		'date' : '',
		'opened' : false,
	};

	$scope.end = {
		'date' : '',
		'opened' : false,
	};

	$scope.open = function(obj) {
		obj.opened = true;
	}
    $scope.fasong=[{name:'未选择',value:''},
                {name:'未发送',value:'0'},
                {name:'待发送',value:'1'},
                {name:'已发送',value:'2'}
              
            ]

};
