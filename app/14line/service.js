/**
 * 子模块service
 * dlq
 */
var service = function($resource, $q, $modal, $state){

    //view模型
    var viewmodel = [
        // {
        //     'title' : '*产品ID',
        //     'id' : 'product_code',
        //     'type' : 'text',
        //     'placeholder' : '',
        //     // 'lock' : true,
        //     'required' : true,
        //     'show' : 'no',
        // },
        {
            'title' : '*产品类型',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '周边', 'value' : '0'},
                {'name' : '国内长线', 'value' : '1'},
                {'name' : '国内当地参团', 'value' : '2'},
                {'name' : '出境当地参团', 'value' : '3'},
                {'name' : '出境短线', 'value' : '4'},
                {'name' : '出境长线', 'value' : '5'},
            ],
            'id' : 'product_type',
            'value' : '0'
        },
         {
            'title' : '*是否为节日',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '否', 'value' : '0'},
                {'name' : '元旦', 'value' : '1'},
                {'name' : '春节', 'value' : '2'},
                {'name' : '元宵', 'value' : '3'},
                {'name' : '清明', 'value' : '4'},
                {'name' : '五一', 'value' : '5'},
                {'name' : '端午', 'value' : '6'},
                {'name' : '中秋', 'value' : '7'},
                {'name' : '国庆', 'value' : '8'},
                {'name' : '圣诞', 'value' : '9'},
            ],
            'id' : 'holiday',
            'value' : '0'
        },
        {
            'title' : '*主名称',
            'id' : 'product_name',
            'type' : 'text',
            'placeholder' : '产品名，限70字以内',
            'required' : true,
        },
        {
            'title' : '*副名称',
            'id' : 'product_sub_name',
            'type' : 'text',
            'placeholder' : '产品名后缀，限70字以内',
            'required' : true,
        },
        {
            'title' : '*供应商产品名称',
            'id' : 'supplier_product_name',
            'type' : 'text',
            'tip' : '供应商为自己维护后台 例如：商品名A1',
            'required' : true,
        },
         {
            'title' : '*采购方式',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '打包产品', 'value' : '0'},
                {'name' : '自由行', 'value' : '1'},
                /*{'name' : '机票+地接', 'value' : '1'},
                {'name' : '火车票+地接', 'value' : '2'},*/
            ],
            'id' : 'procurement_mode',
            'value' : '0'
        },
         {
            'title' : '*出发城市',
            'id' : 'start_city_arr',
            'type' : 'text',
            'show' : 'no',
        },
         {
            'title' : '*目的地城市',
            'id' : 'end_city',
            'type' : 'text',
            'show' : 'no',
        },
         {
            'title' : '*供应商产品负责人',
            'id' : 'supplier_name',
            'type' : 'text',
            'tip' : '供应商为实际负责此业务的对接人，如操作、计调',
            'required' : true,
            'show' : 'no',
        },
        {
            'title' : '平台产品经理ID',
            'id' : 'product_manager_id',
            'type' : 'text',
        },
        {
            'title' : '平台产品经理名称',
            'id' : 'product_managger_name',
            'type' : 'text',
        },
         {
            'title' : '是否默认行程',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '默认行程', 'value' : '0'},
                {'name' : '多行程', 'value' : '1'},
            ],
            'id' : 'trip_default',
            'value' : '0'
        },
         {
            'title' : '*上架端',
            'id' : 'docking_port_arr',
            'type' : 'text',            
            'required' : true,
            'show' : 'no',
        },
        {
            'title' : '占位时间',
            'id' : 'cancel_order_time',
            'type' : 'select',
            'value':30,
            'info' : [
                {'name' : '30分钟', 'value' : 30},
                {'name' : '40分钟', 'value' : 40},
                {'name' : '50分钟', 'value' : 50},
                {'name' : '60分钟', 'value' : 60},
                {'name' : '2小时', 'value' : 120},
                {'name' : '4小时', 'value' : 240},
                {'name' : '8小时', 'value' : 480},
                {'name' : '16小时', 'value' : 960},
                {'name' : '24小时', 'value' : 1920},
                {'name' : '2天', 'value' : 3840},
                {'name' : '3天', 'value' : 5760},
            ],
            
        },

    ];
    var containmodel = [
         {
            // 'title' : '*产品ID',
            // 'id' : 'id',
            'type' : 'text',
            'placeholder' : '温馨提示：默认勾选项请点击保存才会生效',
            'lock' : true,
        },
        {
            'id' : 'aa',
            'title' : '大交通：',
             'type' : 'title',

            //  'value' : '',
        },
        {
            'title' : '*交通包含团队经济舱机票',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '包含', 'value' : '0'},
                {'name' : '不包含', 'value' : '1'},
            ],
            'id' : 'big_traffic_economy_ticket',
            'value' : '0',
        },
         {
            'title' : '*机票类型',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '往返', 'value' : '0'},
                {'name' : '去程', 'value' : '1'},
                {'name' : '返程', 'value' : '2'},
                {'name' : '中间段', 'value' : '3'},
            ],
            'id' : 'big_traffic_ticket_type',
            'value' : '0',
            'show' : 'no',
        },
         {
            'title' : '*是否含税',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '包含', 'value' : '0'},
                {'name' : '不包含', 'value' : '1'},
                {'name' : '其他', 'value' : '2'},
            ],
            'id' : 'big_traffic_tax',
            'value' : '0',
            'show' : 'no',
        },
         {
            'title' : '*交通包含火车票',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '包含', 'value' : '0'},
                {'name' : '不包含', 'value' : '1'},
            ],
            'id' : 'big_traffic_train_ticket',
            'value' : '0',

        },
         {
            'title' : '*火车票类型',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '往返', 'value' : '0'},
                {'name' : '去程', 'value' : '1'},
                {'name' : '返程', 'value' : '2'},
                {'name' : '中间段', 'value' : '3'},
            ],
            'id' : 'big_traffic_train_ticket_type',
            'value' : '0',
            'show' : 'no',
        },
         {
            'title' : '*火车坐席类型',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '动车一等座', 'value' : '0'},
                {'name' : '动车二等座', 'value' : '1'},
                {'name' : '硬座', 'value' : '2'},
                {'name' : '软座', 'value' : '3'},
                {'name' : '硬卧', 'value' : '4'},
                {'name' : '软卧', 'value' : '5'},
                {'name' : '高铁', 'value' : '6'},
                {'name' : '城际列车一等座', 'value' : '7'},
                {'name' : '城际列车二等座', 'value' : '8'},

            ],
            'id' : 'big_traffic_train_seats_type',
            'value' : '0',
            'show' : 'no',
        },
         {
            'title' : '*交通包含旅游巴士',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '包含', 'value' : '0'},
                {'name' : '不包含', 'value' : '1'},
            ],
            'id' : 'big_traffic_tourist_bus',
            'value' : '0'
        },
         {
            'title' : '*汽车票类型',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '往返', 'value' : '0'},
                {'name' : '去程', 'value' : '1'},
                {'name' : '返程', 'value' : '2'},
                {'name' : '中间段', 'value' : '3'},
            ],
            'id' : 'big_traffic_bus_type',
            'value' : '0',
            'show' : 'no',
        },
         {
            'title' : '*交通包含当地旅游巴士',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '包含', 'value' : '0'},
                {'name' : '不包含', 'value' : '1'},
            ],
            'id' : 'big_traffic_local_tourist_bus',
            'value' : '0'
        },
         {
            'title' : '*交通包含往返车票',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '包含', 'value' : '0'},
                {'name' : '不包含', 'value' : '1'},
            ],
            'id' : 'big_traffic_return_ticket',
            'value' : '0'
        },
        {
            'title' : '*交通包含船票',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '包含', 'value' : '0'},
                {'name' : '不包含', 'value' : '1'},
            ],
            'id' : 'big_traffic_steamer_ticket',
            'value' : '0'
        },
        {
            'title' : '*船票类型',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '往返', 'value' : '0'},
                {'name' : '去程', 'value' : '1'},
                {'name' : '回程', 'value' : '2'},
                {'name' : '全程', 'value' : '3'},
            ],
            'id' : 'big_traffic_steamer_ticket_type',
            'value' : '0',
            'show' : 'no',
        },
        {
            'title' : '船票备注信息',
            'id' : 'big_traffic_ticket_remark',
            'type' : 'text',
            'show' : 'no',
        },
        {
            'title' : '小交通：',
            'type' : 'title',
            'id' : 'little_traffic',

        },
         {
            'title' : '*是否包含小交通',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '包含', 'value' : '0'},
                {'name' : '不包含', 'value' : '1'},
            ],
            'id' : 'little_traffic_traffic',
            'value' : '0'
        },
        {
            'title' : '*小交通类型',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '往返', 'value' : '0'},
                {'name' : '去程', 'value' : '1'},
                {'name' : '回程', 'value' : '2'},
            ],
            'id' : 'little_traffic_traffic_type',
            'value' : '0',
            'show' : 'no',
        },
         {
            'title' : '小交通描述',
            'id' : 'little_traffic_describe',
            'type' : 'text',
            'show' : 'no',
        },
         {
            'title' : '*是否存在景区内用车',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '存在', 'value' : '0'},
                {'name' : '不存在', 'value' : '1'},
            ],
            'id' : 'little_traffic_scenic_car',
            'value' : '0'
        },
        
        {
            'title' : '保险：',
            'type' : 'title',
        },
        {
            'title' : '是否含有保险',
            'id' : 'safe_contain',
            'value' : '0',
            'type' : 'radiobutton',
            'info' : [

                {'name' : '有', 'value' : '0'},
                {'name' : '没有', 'value' : '1'},
            ],
        },
        {
            'title' : '保险说明',
            'id' : 'safe_content',
            'type' : 'text',
            'show' : 'no',
        },
        {
            'title' : '费用包含住宿信息：',
            'type' : 'title',
        },
        {
            'title' : '住宿',
            'id' : 'stay_includes',
            'value' : '0',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '是', 'value' : '0'},
                {'name' : '否', 'value' : '1'},
            ],
        },
        {
            'title' : '*住宿类型',
            'id' : 'stay_type',
            'value' : '0',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '行程所列酒店', 'value' : '0'},
                {'name' : '星级酒店', 'value' : '1'},
                {'name' : '酒店', 'value' : '2'},
                // {'name' : '当地酒店', 'value' : '3'},
                {'name' : '目的地酒店 ', 'value' : '3'},
            ],
            'show':'no',
        },
        {
            'title' : '酒店星级',
            'id' : 'stay_hotel_star',
            'type' : 'text',
            'show' : 'no',
        },
        {
            'title' : '房间标准容量',
            'id' : 'stay_room_capacity',
            'type' : 'number',
             'show' : 'no',
        },
        {
            'title' : '酒店信息描述',
            'id' : 'stay_hotel_describe',
            'type' : 'textarea',
            'state' : 'normal',
             'show' : 'no',
        },
        // {
        //     'title' : '目的地酒店信息：',
        //     'type' : 'title',
        //      'show' : 'no',
        // },
        {
            'title' : '*目的地酒店信息是否存在',
            'id' : 'dest_hotel_messages',
            'value' : '0',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '存在', 'value' : '0'},
                {'name' : '不存在', 'value' : '1'},
            ],
            'show' : 'no',
        },
        {
            'title' : '*目的地',
            'id' : 'dest_hotel_destination',
            'type' : 'text',
            'show' : 'no',
        },
        {
            'title' : '*目的地酒店星级',
            'id' : 'dest_hotel_star',
            'value' : '0',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '普通', 'value' : '0'},
                {'name' : '3星', 'value' : '1'},
                {'name' : '4星', 'value' : '2'},
                {'name' : '5星', 'value' : '3'},
                {'name' : '未知', 'value' : '4'},
            ],
            'show' : 'no',
        },
        {
            'title' : '房间标准容量',
            'id' : 'dest_room_capacity',
            'type' : 'number',
            'show' : 'no',
        },
        {
            'title' : '入住',
            'id' : 'dest_check_in',
            'type' : 'number',
            'show' : 'no',
        },
        {
            'title' : '酒店入住天数',
            'id' : 'stay_hotel_num',
            'type' : 'number',
            'state' : 'normal',
            'show' : 'no',
        },
        {
            'title' : '目的地酒店信息描述',
            'id' : 'dest_hotel_messages_describe',
            'type' : 'textarea',
            'state' : 'normal',
            'show' : 'no',
        },
        {
            'title' : '目的地游轮信息：',
            'type' : 'title',
            'id':'cruises_dest_destination_info',
            
        },
        {
            'title' : '目的地游轮信息是否存在',
            'id' : 'cruises_dest_messages',
            'value' : '0',
            'type' : 'radiobutton',
            'info' : [

                {'name' : '不存在', 'value' : '1'},
                {'name' : '存在', 'value' : '0'},
            ],
        },
         {
            'title' : '*目的地',
            'id' : 'cruises_dest_destination',
            'type' : 'text',
            'show' : 'no',

        },
        {
            'title' : '游轮房间标准容量',
            'id' : 'cruises_dest__room_capacity',
            'type' : 'number',
            'show' : 'no',
        },
         {
            'title' : '入住',
            'id' : 'cruises_dest_check_in',
            'type' : 'number',
            'show' : 'no',
        },
        {
            'title' : '目的地游轮信息描述',
            'id' : 'cruises_dest_messages_describe',
            'type' : 'textarea',
            'state' : 'normal',
            'show' : 'no',
        },
        {
            'title' : '费用包含的用餐信息',
            'id' : 'fee_dining_information',
            'type' : 'text',
            'show' : 'no',
            
        },
        {
            'title' : '门票信息：',
            'type' : 'title',
                        
        },
         {
            'title' : '门票',
            'id' : 'admission_ticket',
            'value' : '0',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '无', 'value' : '0'},
                {'name' : '有', 'value' : '1'},
            ],
            'show' : 'no',
        },
        {
            'title' : '包含',
            'id' : 'admission_ticket_attractions',
            'type' : 'text',
            'tip' : '首道大门票',
            'placeholder' : '输入景点',
            'show' : 'no',
        },
         {
            'title' : '其中',
            'id' : 'admission_ticket_coupon_attractions',
            'type' : 'text',
            'tip' : '为联票，放弃其中任何一个景点费用不退',
            'placeholder' : '输入景点',
            'show' : 'no',
        },
        {
            'title' : '其中',
            'id' : 'admission_ticket_free_attractions',
            'type' : 'text',
            'tip' : '为赠送项目，如不参加或不能游览，不退任何费用',
            'placeholder' : '输入景点',
            'show' : 'no',
        },
         {
            'title' : '含餐信息：',
            'type' : 'title',
        },
        {
            'title' : '含餐信息',
            'id' : 'dining_type',
            'value' : '0',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '含', 'value' : '0'},
                {'name' : '全程用餐自理', 'value' : '1'},
                {'name' : '酒店含早正餐自理', 'value' : '2'},
                {'name' : '其他', 'value' : '3'},
            ],
        },
        {
            'title' : '行程特色',
            'id' : 'zhusu',
            'type' : 'text',
            'placeholder' : '6个词以内',
            'show' : 'no'
        },
        {
            'title' : '导游：',
            'type' : 'title',
        },
        {
            'title' : '*导服类型',
            'id' : 'guide_service_type',
            'value' : '0',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '当地中文导游', 'value' : '0'},
                {'name' : '专职中文领队兼导游（境外）', 'value' : '1'},
                {'name' : '全程陪同中文导游（境内）', 'value' : '2'},
                {'name' : '专职领队和当地中文导游（境外）', 'value' : '3'},
                {'name' : '全陪和当地导游（境内）', 'value' : '4'},
            ],
        },
        {
            'title' : '导服说明',
            'id' : 'guide_service_explain',
            'type' : 'text',
        },
        {
            'title' : '费用包含的消费信息：',
            'type' : 'title',
        },
        {
            'title' : '是否存在小费',
            'id' : 'guide_service_driver_tip',
            'value' : '0',
            'type' : 'radiobutton',
            'info' : [

                {'name' : '不存在', 'value' : '1'},
                {'name' : '存在', 'value' : '0'},
            ],
        },
        {
            'title' : '消费价格',
            'id' : 'guide_service_driver_price',
            'type' : 'number',
            'tip' : '元/人',
            'show' : 'no',
        },
        {
            'title' : '说明',
            'id' : 'guide_service_driver_explain',
            'type' : 'text',
            'show' :'no',
        },
        //儿童价格标准没写、
        {
            'title' : '儿童价标准',
            'id' : 'child_fee_exist',
            'value' : '0',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '创建儿童费用标准', 'value' : '0'},
                {'name' : '无儿童价', 'value' : '1'},
            ],
        },
        {
            'title' : '机票税信息：',
            'type' : 'title',
        },
        {
            'title' : '往返机票税是否存在',
            'id' : 'ticket_tax_round_trip',
            'value' : '0',
            'type' : 'radiobutton',
            'info' : [

                {'name' : '不存在', 'value' : '1'},
                {'name' : '存在', 'value' : '0'},
            ],
        },
        {
            'title' : '*机票税的价格为',
            'id' : 'ticket_tax_fee',
            'type' : 'number',
            'tip' : '元/人',
            'show' : 'no'
        },
        // {
        //     'title' : '境外机场回程机场税',
        //     'id' : 'ticket_tax_outside',
        //     'value' : '0',
        //     'type' : 'radiobutton',
        //     'info' : [

        //         {'name' : '不存在', 'value' : '1'},
        //         {'name' : '存在', 'value' : '0'},
        //     ],
        // },
        // {
        //     'title' : '*境外机场回程机票税的价格为',
        //     'id' : 'ticket_tax_outside_fee',
        //     'type' : 'text',
        //     'tip' : '元/人',
        //      'show' : 'no'
        // },
        {
            'title' : '其他',
            'id' : 'else_messages_arr',
            'type' : 'text',
        },
    ];
    var cautionmodel = [
        {
            // 'title' : '*产品ID',
            // 'id' : 'id',
            'type' : 'text',
            'placeholder' : '温馨提示：默认勾选项请点击保存才会生效',
            'lock' : true,
        },
        {
            'title' : '提示信息：',
             'type' : 'title',
        },
         {
            'title' : '是否自驾游手动编写内容',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '是', 'value' : '0'},
                {'name' : '否', 'value' : '1'},
            ],
            'id' : 'hint_self_driving_writing',
            'value' : '0'
        },
        {
            'title' : '自驾游手动编写内容',
            'id' : 'hint_self_driving_writing_content',
            'type' : 'text',
            'show' : 'no',
        },
        {
            'title' : '付款的预定须知',
            'id' : 'hint_payment_attention',
            'type' : 'text',
            
        },
        {
            'title' : '是否在此团收容人数不足时取消行程',
            'type' : 'radiobutton',
            'info' : [

                {'name' : '否', 'value' : '1'},
                {'name' : '是', 'value' : '0'},
            ],
            'id' : 'hint_cancel_trip',
            'value' : '0'
        },
        {
            'title' : '取消最小行程人数',
            'id' : 'hint_min_travel_population',
            'type' : 'text',
            'show' : 'no',
        },
        {
            'title' : '取消行程的通知提前天数',
            'id' : 'hint_ravel_advance_notification',
            'type' : 'number',
            'show' : 'no',
        },
        {
            'title' : '团队游览中是否允许擅自离队',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '是', 'value' : '0'},
                {'name' : '否', 'value' : '1'},
            ],
            'id' : 'hint_touring_drop_out',
            'value' : '0'
        },
        {
            'title' : '是否需要支付违约金',
            'type' : 'radiobutton',
            'info' : [

                {'name' : '否', 'value' : '1'},
                {'name' : '是', 'value' : '0'},
            ],
            'id' : 'hint_touring_penalty',
            'value' : '0'
        },
        {
            'title' : '违约金额',
            'id' : 'hint_touring_out_money',
            'type' : 'number',
            'show' : 'no',
        },
        // {
        //     'title' : '（出境）是否包含销票说明',
        //     'type' : 'radiobutton',
        //     'info' : [
        //         {'name' : '是', 'value' : '0'},
        //         {'name' : '否', 'value' : '1'},
        //     ],
        //     'id' : 'hint_logoff_visa_explain'
        // },
        // {
        //     'title' : '（出境）允许人群',
        //     'type' : 'radiobutton',
        //     'info' : [
        //         {'name' : '全部', 'value' : '0'},
        //         {'name' : '只接受持各地签发的中国大陆因私护照客人，不接受港澳台及外籍客人', 'value' : '1'},
        //         {'name' : '只接受持各地签发的港澳通行证客人，不接受港澳台及外籍客人', 'value' : '2'},
        //     ],
        //     'id' : 'hint_allow_crowd'
        // },
        // {
        //     'title' : '自备签证说明',
        //     'type' : 'radiobutton',
        //     'info' : [
        //         {'name' : '存在', 'value' : '0'},
        //         {'name' : '不存在', 'value' : '1'},
        //     ],
        //     'id' : 'hint_owned_visa_explain'
        // },
        // {
        //     'title' : '附加提示',
        //     'id' : 'hint_visa_attach_prompt',
        //     'type' : 'text',
        // },
        // {
        //     'title' : '因甲方原因发生拒签、延时交签造成本次出境行程变更或取消的，乙方不承担违约责任，甲方需承担签证费用，是否存在',
        //     'type' : 'radiobutton',
        //     'info' : [
        //         {'name' : '存在', 'value' : '0'},
        //         {'name' : '不存在', 'value' : '1'},
        //     ],
        //     'id' : 'hint_bear_visa'
        // },
        // {
        //     'title' : '甲方需承担申办签证费用',
        //     'id' : 'hint_bear_visa_fee',
        //     'type' : 'text',
        //     'tip' : '元/人'
        // },
        {
            'title' : '机票说明是否存在',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '存在', 'value' : '0'},
                {'name' : '不存在', 'value' : '1'},
            ],
            'id' : 'hint_airline_ticket_explain',
            'value' : '0'
        },
        {
            'title' : '甲方原因无法出游乙方负责说明是否存在',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '存在', 'value' : '0'},
                {'name' : '不存在', 'value' : '1'},
            ],
            'id' : 'hint_not_outing_explain',
            'value' : '0'
        },
        {
            'title' : '特别提醒说明是否存在',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '存在', 'value' : '0'},
                {'name' : '不存在', 'value' : '1'},
            ],
            'id' : 'hint_special_remind_explain',
            'value' : '0'
        },
         {
            'title' : '补充信息：',
             'type' : 'title',
        },
        {
            'title' : '是否存在丽江古城维护费80元',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '存在', 'value' : '0'},
                {'name' : '不存在', 'value' : '1'},
            ],
            'id' : 'additional_lijiang_upkeep',
            'value' : '0'
        },
        {
            'title' : '是否存在海南政府调节金',
            'type' : 'radiobutton',
            'info' : [

                {'name' : '不存在', 'value' : '1'},
                {'name' : '存在', 'value' : '0'},
            ],
            'id' : 'additional_hainan_payment',
            'value' : '0'
        },
        {
            'title' : '海南政府调节金为',
            'id' : 'additional_fee',
            'type' : 'number',
            'tip' : '元/人',
            'show' : 'no',
        },
        {
            'title' : '因交通延阻、战争、政变、罢工、天气、飞机机器故障，航班取消或更改时间等不可抗力因素所导致的额外费用是否存在',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '存在', 'value' : '0'},
                {'name' : '不存在', 'value' : '1'},
            ],
            'id' : 'additional_extra_fee',
            'value' : '0'
        },
        {
            'title' : '酒店内洗漱、理发、电话、传真、收费电视饮品、烟酒等个人消费',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '存在', 'value' : '0'},
                {'name' : '不存在', 'value' : '1'},
            ],
            'id' : 'additional_personal_fee',
            'value' : '0'
        },
        {
            'title' : '当地参加自费及以上“费用包含”中不包含的其他项目',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '存在', 'value' : '0'},
                {'name' : '不存在', 'value' : '1'},
            ],
            'id' : 'additional_self_pay',
            'value' : '0'
        },
        {
            'title' : '加床信息：',
             'type' : 'title',
        },
         {
            'title' : '单人房差',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '可以补房差或加床', 'value' : '0'},
                {'name' : '无3人间切不能加床', 'value' : '1'},
                {'name' : '标间或大床房不可指定', 'value' : '2'},
            ],
            'id' : 'bed_single_room_difference',
            'value' : '0'
        },
        {
            'title' : '最晚收材料日',
            'id' : 'cailiao',
            'type' : 'text',
            'placeholder' : '',
            'show' : 'no'
        },
         {
            'title' : '重要信息：',
             'type' : 'title',
        },
        {
            'title' : '发团形式',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '出发地成团', 'value' : '0'},
                {'name' : '目的地成团', 'value' : '1'},
                {'name' : '中转地联运', 'value' : '2'},
                {'name' : '无', 'value' : '3'},
            ],
            'id' : 'important_hair_regiment_type',
            'value' : '0'
        },
        {
            'title' : '组团形式',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '平台独家发团', 'value' : '0'},
                {'name' : '联合发团', 'value' : '1'},
                {'name' : '无', 'value' : '2'},
            ],
            'id' : 'important_group_type',
            'value' : '0'
        },
        {
            'title' : '是否拼团',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '非行程中拼团', 'value' : '0'},
                {'name' : '行程中拼团', 'value' : '1'},
            ],
            'id' : 'important_spell_group',
            'value' : '1'
        },
         {
            'title' : '拼团附注信息',
            'id' : 'important_spell_group_extras',
            'type' : 'text',
            'show' : 'no',
        },
        {
            'title' : '是否存在当地参团人员',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '存在', 'value' : '0'},
                {'name' : '不存在', 'value' : '1'},
            ],
            'id' : 'important_local_join',
            'value' : '0'
        },
        {
            'title' : '是否拼车',
            'type' : 'radiobutton',
            'info' : [

                {'name' : '不存在', 'value' : '1'},
                 {'name' : '存在', 'value' : '0'},
            ],
            'id' : 'important_spell_car',
            'value' : '0'
        },
        {
            'title' : '拼车备注',
            'id' : 'important_spell_car_remark',
            'type' : 'text',
            'show' : 'no',
        },
        {
            'title' : '是否更换当地车或导游',
            'type' : 'radiobutton',
            'info' : [

                {'name' : '不存在', 'value' : '1'},
                {'name' : '存在', 'value' : '0'},
            ],
            'id' : 'important_change_car_guide',
            'value' : '0'
        },
        {
            'title' : '换车/导游备注',
            'id' : 'important_change_car_guide_remark',
            'type' : 'text',
            'show' : 'no',
        },
        {
            'title' : '是否升级行程',
            'type' : 'radiobutton',
            'info' : [

                {'name' : '不升级', 'value' : '1'},
                {'name' : '升级', 'value' : '0'},
            ],
            'id' : 'important_updaoyoute_travel',
            'value' : '0',
        },
        {
            'title' : '升级行程备注',
            'id' : 'important_update_travel_remark',
            'type' : 'text',
            'show' : 'no',
        },
        {
            'title' : '住宿提示信息：',
             'type' : 'title',
        },
        {
            'title' : '拼房选择',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '不拼房', 'value' : '0'},
                {'name' : '与亲友加床', 'value' : '1'},
            ],
            'id' : 'stay_spell_room',
            'value' : '0'
        },
        {
            'title' : '是否提供洗漱用品',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '提供', 'value' : '0'},
                {'name' : '不提供', 'value' : '1'},
            ],
            'id' : 'stay_toilet_articles',
            'value' : '0'
        },
        {
            'title' : '拼团附注信息：',
             'type' : 'title',
        },
        {
            'title' : '是否可以确认拼团次数',
            'type' : 'radiobutton',
            'info' : [

                {'name' : '估算', 'value' : '1'},
                 {'name' : '准确', 'value' : '0'},
            ],
            'id' : 'spellgroup_count_right',
            'value' : '0'
        },
        {
            'title' : '拼团次数',
            'id' : 'spellgroup_count',
            'type' : 'number',
            'value' : ' ',
        },
        {
            'title' : '如非估算值拼团备注',
            'id' : 'spellgroup_estimate_remark',
            'type' : 'text',
            'show' : 'no',
        },
        {
            'title' : '支付信息：',
             'type' : 'title',
        },
        {
            'title' : '行程特色',
            'id' : 'pay',
            'type' : 'text',
            'placeholder' : '',
            'show' : 'no'
        },
        {
            'title' : '其他：',
             'type' : 'title',
        },
        {
            'title' : '多选',
            'type' : 'textarea',
            'info' : [
                {'name' : '酒店不提供一次性用品', 'value' : '1'},
                {'name' : '山上酒店潮湿', 'value' : '2'},
                {'name' : '酒店二次确认', 'value' : '3'},
                {'name' : '火车票不能提前预售', 'value' : '4'},
                {'name' : '拼车', 'value' : '5'},
                {'name' : '伊斯兰教国家', 'value' : '6'},
                {'name' : '车况较差', 'value' : '7'},
                {'name' : '拼号', 'value' : '8'},
                {'name' : '道路拥堵', 'value' : '9'},
                {'name' : '路况较差、路程长', 'value' : '10'},
            ],
            'id' : 'hint_visa_attach_prompt1',
        },
        //点击添加一条没写
        {
            'title' : '线路条款：',
             'type' : 'title',
        },
        {
            'title' : '团队酒店描述：',
             'type' : 'title',
        },
        {
            'title' : '特殊人群限制：',
             'type' : 'title',
        },
        //特殊人群限制没写
        {
            'title' : '是否存在订单人数少于多少人时，需现询确认',
            'type' : 'radiobutton',
            'info' : [

                {'name' : '不存在', 'value' : '1'},
                {'name' : '存在', 'value' : '0'},
            ],
            'id' : 'special_order_min_right',
            'value' : '0'
        },
         {
            'title' : '最少人数',
            'id' : 'special_order_min_num',
            'type' : 'number',
            'show' : 'no',
        },
        {
            'title' : '是否存在订单人数大于多少人时，需现询确认',
            'type' : 'radiobutton',
            'info' : [

                {'name' : '不存在', 'value' : '1'},
                {'name' : '存在', 'value' : '0'},
            ],
            'id' : 'special_order_max_right',
            'value' : '0'
        },
         {
            'title' : '最大人数',
            'id' : 'special_order_max_num',
            'type' : 'number',
            'show' : 'no',
        },
        {
            'title' : '是否存在出行人数小于多少岁，不接收',
            'type' : 'radiobutton',
            'info' : [

                {'name' : '不存在', 'value' : '1'},
                {'name' : '存在', 'value' : '0'},
            ],
            'id' : 'special_age_min_right',
            'value' : '0'
        },
         {
            'title' : '最小年龄',
            'id' : 'special_age_min_num',
            'type' : 'number',
            'show' : 'no',
        },
        {
            'title' : '是否存在出行人数大于多少岁，不接收',
            'type' : 'radiobutton',
            'info' : [

                {'name' : '不存在', 'value' : '1'},
                 {'name' : '存在', 'value' : '0'},
            ],
            'id' : 'special_age_max_right',
            'value' : '0'
        },
         {
            'title' : '最大年龄',
            'id' : 'special_age_max_num',
            'type' : 'number',
            'show' : 'no',
        },
        {
            'title' : '是否存在出行人数大于多少岁，需要签署健康协议',
            'type' : 'radiobutton',
            'info' : [

                {'name' : '不存在', 'value' : '1'},
                 {'name' : '存在', 'value' : '0'},
            ],
            'id' : 'special_exceed_age_right',
            'value' : '0'
        },
         {
            'title' : '年龄',
            'id' : 'special_exceed_age_num',
            'type' : 'number',
            'show' : 'no',
        },
        {
            'title' : '年龄限制',
            'type' : 'radiobutton',
            'info' : [

                {'name' : '无限制', 'value' : '1'},
                 {'name' : '存在限制', 'value' : '0'},
            ],
            'id' : 'special_age_restriction',
            'value' : '0'
        },
        {
            'title' : '年龄限制范围',
            'id' : 'special_age_restriction_num',
            'type' : 'text',
            'placeholder' : '岁',
            'show' : 'no',
        },
        {
            'title' : '是否接收外籍游客',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '是', 'value' : '0'},
                {'name' : '否', 'value' : '1'},
            ],
            'id' : 'special_receive_foreignness',
            'value' : '0'
        },
        {
            'title' : '是否存在地域限制',
            'type' : 'radiobutton',
            'info' : [

                {'name' : '无限制', 'value' : '1'},
                 {'name' : '存在限制', 'value' : '0'},
            ],
            'id' : 'special_region_limit',
            'value' : '0'
        },
        {
            'title' : '地域限制列表',
            'id' : 'special_region_limit_list',
            'type' : 'textarea',
            'state' : 'normal',
            'show' : 'no',
        },
        {
            'title' : '时间限制',
            'id' : 'special_time_limit',
            'type' : 'number',
            'tip' : '（支付类型不为两小时显示）',
        },
    ];
    var explainmodel = [
        {
            'title' : '其他条款',
            'type' : 'textarea',
            'info' : [
                {'name' : '旅游者在出发前30日内提出解除合同的，应当按下列标准向组团社支付业务损失费', 'value' : '1'},
                {'name' : '行程开始前29日至15日，按旅游费用总额的5%', 'value' : '2'},
                {'name' : '行程开始前14日至7日，按旅游费用总额的20%', 'value' : '3'},
                {'name' : '行程开始前6日至4日，按旅游费用总额的50%', 'value' : '4'},
                {'name' : '行程开始前3日至1日，按旅游费用总额的60%', 'value' : '5'},
                {'name' : '行程开始当日，按旅游费用总额的70%', 'value' : '6'},
                {'name' : '软上述比例支付业务损失费不足以赔偿组社团的实际损失，旅游者当按实际损失对组团社予以赔偿，但最高不应当超过旅游费用总额', 'value' : '7'},
                {'name' : '游客转让：出行前，在符合办理团队签证或签注期限或其他条件许可情况下，旅游者可以向组团社书面提出将其自身在本合同中的权利和义务转让给符合出游条件的第三人；并且由第三人与组团社重新签订合同；因此增加的费用由旅游者或第三人承担，减少的费用组团社退还旅游者。', 'value' : '8'},
            ],
            'id' : 'terms_type1',

        },
        {
            'title' : '其他',
            'type' : 'text',
            'id' : 'contracting_plane_terms',
            'state' : 'normal',
        },
    ];
    var noticemodel = [
        {
            'title' : '订单序列号',
            'id' : 'product_name',
            'type' : 'text',
        },
        {
            'title' : '供应商产品编码',
            'id' : 'product_name',
            'type' : 'text',
        },
        {
            'title' : '出游日期',
            'id' : 'product_name',
            'type' : 'text',
        },
        {
            'title' : '出游方案',
            'id' : 'product_name',
            'type' : 'text',
        },
        {
            'title' : '订单号',
            'id' : 'product_name',
            'type' : 'text',
        },
        {
            'title' : '联系人信息',
            'id' : 'product_name',
            'type' : 'title',
        },
        {
            'title' : '姓名',
            'id' : 'product_name',
            'type' : 'text',
        },
        {
            'title' : '电话',
            'id' : 'product_name',
            'type' : 'text',
        },
        {
            'title' : '订单成人数',
            'id' : 'product_name',
            'type' : 'text',
        },
        {
            'title' : '订单儿童数',
            'id' : 'product_name',
            'type' : 'text',
        },
        {
            'title' : '订单婴儿数',
            'id' : 'product_name',
            'type' : 'text',
        },
        {
            'title' : '成人成本价',
            'id' : 'product_name',
            'type' : 'text',
        },
        {
            'title' : '儿童成本价',
            'id' : 'product_name',
            'type' : 'text',
        },
        {
            'title' : '婴儿成本价',
            'id' : 'product_name',
            'type' : 'text',
        },
        {
            'title' : '单房差数',
            'id' : 'product_name',
            'type' : 'text',
        },
        {
            'title' : '单房差价格',
            'id' : 'product_name',
            'type' : 'text',
        },
        {
            'title' : '剩余库存',
            'id' : 'product_name',
            'type' : 'text',
        },
    ];
    var picturemodel = [
        {
            'title' : '添加封面',
            'id' : 'image_top',
            'type' : 'image',
        },
        {
            'title' : '添加图片',
            'id' : 'image_arr',
            'type' : 'image',
        },
    ];
    var nocontainmodel = [
         {
            'title' : '单人房差',
            'id' : 'single_room_trpe',
            'value' : '0',
            'type' : 'radiobutton',
            'info' : [
                {'name' : '单人房差（房差说明详见：预定须知--购买须知及说明第一条）', 'value' : '0'},
                {'name' : '本产品报价是按照2人入住1间房计算的价格，如您的订单产生单房，将按排您与其他客人拼房入住。要求享受单房，请在后续附加产品页面中选择单人房差选项', 'value' : '1'},
                {'name' : '自定义', 'value' : '2'},
            ],
        },
         {
            'title' : '其他',
            'type' : 'text',
            'id' : 'contracting_plane_terms1',
            'state' : 'normal',
        },
    ];
    var activitymodel = [
        {
            'title' : '小标题',
            'type' : 'text',
            'id' : 'add',
            'show' : 'no'
        },
    ];
    var allinformationmodel = [
        {
            'title' : '小标题',
            'type' : 'text',
            'id' : 'wjq',
            'show' : 'no'
        },
    ];


    var tripmodel = [
        {
            'title' : '天',
            'id' : 'day',
            'type' : 'select',
            'info' : [
                {'name' : '1', 'value' : '1'},
                {'name' : '2', 'value' : '2'},
                {'name' : '3', 'value' : '3'},
                {'name' : '4', 'value' : '4'},
                {'name' : '5', 'value' : '5'},
                {'name' : '6', 'value' : '6'},
                {'name' : '7', 'value' : '7'},
                {'name' : '8', 'value' : '8'},
            ],
            'value' : '1',
        },
        {
            'title' : '晚',
            'id' : 'night',
            'type' : 'select',
            'info' : [
                {'name' : '1', 'value' : '1'},
                {'name' : '2', 'value' : '2'},
                {'name' : '3', 'value' : '3'},
                {'name' : '4', 'value' : '4'},
                {'name' : '5', 'value' : '5'},
                {'name' : '6', 'value' : '6'},
                {'name' : '7', 'value' : '7'},
                {'name' : '8', 'value' : '8'},
            ],
            'value' : '1',
        },
    ];
    
    return {
        allinformationmodel : function(){
            return allinformationmodel;
        },
        viewmodel : function(){
            return viewmodel;
        },
        containmodel : function(){
            return containmodel;
        },
        cautionmodel : function(){
           return cautionmodel;
       },
       explainmodel : function(){
           return explainmodel
       },
       noticemodel : function(){
           return noticemodel
       },
       tripmodel : function(){
           return tripmodel;
       },
       picturemodel : function(){
           return picturemodel;
       },
       nocontainmodel :function(){
           return nocontainmodel;
       },
       activitymodel :function(){
           return activitymodel;
       },
       
    };

};

module.exports = service;