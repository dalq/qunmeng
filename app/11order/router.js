/**
* 子模块路由
* ml
*/

var router = function ($urlRouterProvider, $stateProvider) {

    $stateProvider
        //新商品列表
        .state('app.order_list', {
            url: "/order/list.html",
            views: {
                'main@': {
                    template: require('./views/orderlist1.html'),
                    controller: 'orderlist1',
                }
            },
            resolve: {
                list: function (orderservice) {
                    return orderservice.list();
                },
                getDate: function (utilservice) {
                    return utilservice.getDate;
                },
                ticketlist: function (orderservice) {
                    return orderservice.ticketlist();
                },
                createBackOrder: function (orderservice) {
                    return orderservice.createBackOrder();
                },
                resend: function (orderservice) {
                    return orderservice.resend();
                },
                getRedCorridorOrderList: function (orderservice) {
                    return orderservice.getRedCorridorOrderList();
                },
                getRedCorridorResentMsg: function (orderservice) {
                    return orderservice.getRedCorridorResentMsg();
                },
                orderbacklist: function (orderservice) {
                    return orderservice.orderbacklist();
                },
                relay: function (orderservice) {
                    return orderservice.relay();
                },
                getRedCorridorTrSendSms: function (orderservice) {
                    return orderservice.getRedCorridorTrSendSms();
                },
                getOrderSimInfo: function (orderservice) {
                    return orderservice.getOrderSimInfo();
                },
                agencyOrderRepeatECode: function (orderservice) {
                    return orderservice.agencyOrderRepeatECode();
                },
                updateTicketEffectTime: function (orderservice) {
                    return orderservice.updateTicketEffectTime();
                },
                getroyalocOrdersState: function (orderservice) {
                    return orderservice.getroyalocOrdersState();
                },
                testCreateBackOrder: function (orderservice) {
                    return orderservice.testCreateBackOrder();
                },
                str2date: function (utilservice) {
                    return utilservice.str2date;
                },
                categorylist: function (orderservice) {
                    return orderservice.categorylist();
                },
                date2str: function (utilservice) {
                    return utilservice.date2str;
                }


            }


        })



        .state('app.grouporderlist', {
            url: '/grouporderlist',
            views: {
                'main@': {
                    controller: '13grouplist',
                    template: require('./views/grouplist.html'),
                }
            },
            resolve: {
                grouplist: function (orderservice) {
                    return orderservice.grouplist();
                },
                getDate: function (utilservice) {
                    return utilservice.getDate;
                },
                //报名详情
                infolist: function (orderservice) {
                    return orderservice.infolist();
                }
            }

        })

        .state('app.grouporderjqlist', {
            url: '/grouporderjqlist',
            views: {
                'main@': {
                    controller: 'groupjqlist',
                    template: require('./views/grouplist.html'),
                }
            },
            resolve: {
                grouporderlist: function (orderservice) {
                    return orderservice.grouporderlist();
                },
                getDate: function (utilservice) {
                    return utilservice.getDate;
                },
                //报名详情
                infolist: function (orderservice) {
                    return orderservice.infolist();
                }
            }

        })

        .state('app.supplyorderList', {
            url: '/supplyOrderList',
            views: {
                'main@': {
                    controller: 'supplyOrderList',
                    template: require('./views/supplyOrderList.html'),
                }
            },
            resolve: {
                supplyOrderList: function (orderservice) {
                    return orderservice.supplyOrderList();
                },
                ticketlist: function (orderservice) {
                    return orderservice.ticketlist();
                },
                getDate: function (utilservice) {
                    return utilservice.getDate;
                },
                createBackOrder: function (orderservice) {
                    return orderservice.createBackOrder();
                },
                resend: function (orderservice) {
                    return orderservice.resend();
                },
                getRedCorridorOrderList: function (orderservice) {
                    return orderservice.getRedCorridorOrderList();
                },
                getRedCorridorResentMsg: function (orderservice) {
                    return orderservice.getRedCorridorResentMsg();
                },
                orderbacklist: function (orderservice) {
                    return orderservice.orderbacklist();
                },
                relay: function (orderservice) {
                    return orderservice.relay();
                },
                getRedCorridorTrSendSms: function (orderservice) {
                    return orderservice.getRedCorridorTrSendSms();
                },
                getOrderSimInfo: function (orderservice) {
                    return orderservice.getOrderSimInfo();
                },
                agencyOrderRepeatECode: function (orderservice) {
                    return orderservice.agencyOrderRepeatECode();
                },
                updateTicketEffectTime: function (orderservice) {
                    return orderservice.updateTicketEffectTime();
                },
                getroyalocOrdersState: function (orderservice) {
                    return orderservice.getroyalocOrdersState();
                },
                str2date: function (utilservice) {
                    return utilservice.str2date;
                }
            }

        })



        .state('app.areaorderList', {
            url: '/areaOrderList',
            views: {
                'main@': {
                    controller: 'areaOrderList',
                    template: require('./views/areaOrderList.html'),
                }
            },
            resolve: {
                areaOrderList: function (orderservice) {
                    return orderservice.areaOrderList();
                },
                getDate: function (utilservice) {
                    return utilservice.getDate;
                },
                ticketlist: function (orderservice) {
                    return orderservice.ticketlist();
                },

                createBackOrder: function (orderservice) {
                    return orderservice.createBackOrder();
                },
                resend: function (orderservice) {
                    return orderservice.resend();
                },
                getRedCorridorOrderList: function (orderservice) {
                    return orderservice.getRedCorridorOrderList();
                },
                getRedCorridorResentMsg: function (orderservice) {
                    return orderservice.getRedCorridorResentMsg();
                },
                orderbacklist: function (orderservice) {
                    return orderservice.orderbacklist();
                },
                relay: function (orderservice) {
                    return orderservice.relay();
                },
                getRedCorridorTrSendSms: function (orderservice) {
                    return orderservice.getRedCorridorTrSendSms();
                },
                getOrderSimInfo: function (orderservice) {
                    return orderservice.getOrderSimInfo();
                },
                agencyOrderRepeatECode: function (orderservice) {
                    return orderservice.agencyOrderRepeatECode();
                },
                updateTicketEffectTime: function (orderservice) {
                    return orderservice.updateTicketEffectTime();
                },
                getroyalocOrdersState: function (orderservice) {
                    return orderservice.getroyalocOrdersState();
                },
                str2date: function (utilservice) {
                    return utilservice.str2date;
                }
            }

        })

        .state('app.marketorderList', {
            url: '/marketOrderList',
            views: {
                'main@': {
                    controller: 'marketOrderList',
                    template: require('./views/marketOrderList.html'),
                }
            },
            resolve: {
                marketOrderList: function (orderservice) {
                    return orderservice.marketOrderList();
                },
                getDate: function (utilservice) {
                    return utilservice.getDate;
                },
                ticketlist: function (orderservice) {
                    return orderservice.ticketlist();
                },

                createBackOrder: function (orderservice) {
                    return orderservice.createBackOrder();
                },
                resend: function (orderservice) {
                    return orderservice.resend();
                },
                getRedCorridorOrderList: function (orderservice) {
                    return orderservice.getRedCorridorOrderList();
                },
                getRedCorridorResentMsg: function (orderservice) {
                    return orderservice.getRedCorridorResentMsg();
                },
                orderbacklist: function (orderservice) {
                    return orderservice.orderbacklist();
                },
                relay: function (orderservice) {
                    return orderservice.relay();
                },
                getRedCorridorTrSendSms: function (orderservice) {
                    return orderservice.getRedCorridorTrSendSms();
                },
                getOrderSimInfo: function (orderservice) {
                    return orderservice.getOrderSimInfo();
                },
                agencyOrderRepeatECode: function (orderservice) {
                    return orderservice.agencyOrderRepeatECode();
                },
                updateTicketEffectTime: function (orderservice) {
                    return orderservice.updateTicketEffectTime();
                },
                getroyalocOrdersState: function (orderservice) {
                    return orderservice.getroyalocOrdersState();
                },
                str2date: function (utilservice) {
                    return utilservice.str2date;
                }
            }

        })

        //购物车订单列表
        .state('app.orderCartList', {
            url: '/orderCartList',
            views: {
                'main@': {
                    controller: 'orderCartList',
                    template: require('./views/orderCartList.html'),
                }
            },
            resolve: {
                getDate: function (utilservice) {
                    return utilservice.getDate;
                },
                ticketlist: function (orderservice) {
                    return orderservice.ticketlist();
                },
                createBackOrder: function (orderservice) {
                    return orderservice.createBackOrder();
                },
                resend: function (orderservice) {
                    return orderservice.resend();
                },
                getRedCorridorOrderList: function (orderservice) {
                    return orderservice.getRedCorridorOrderList();
                },
                getRedCorridorResentMsg: function (orderservice) {
                    return orderservice.getRedCorridorResentMsg();
                },
                orderbacklist: function (orderservice) {
                    return orderservice.orderbacklist();
                },
                relay: function (orderservice) {
                    return orderservice.relay();
                },
                getRedCorridorTrSendSms: function (orderservice) {
                    return orderservice.getRedCorridorTrSendSms();
                },
                getOrderSimInfo: function (orderservice) {
                    return orderservice.getOrderSimInfo();
                },
                agencyOrderRepeatECode: function (orderservice) {
                    return orderservice.agencyOrderRepeatECode();
                },
                updateTicketEffectTime: function (orderservice) {
                    return orderservice.updateTicketEffectTime();
                },
                getroyalocOrdersState: function (orderservice) {
                    return orderservice.getroyalocOrdersState();
                },
                testCreateBackOrder: function (orderservice) {
                    return orderservice.testCreateBackOrder();
                },
                str2date: function (utilservice) {
                    return utilservice.str2date;
                },
                categorylist: function (orderservice) {
                    return orderservice.categorylist();
                },
                date2str: function (utilservice) {
                    return utilservice.date2str;
                }
            }

        })


};

module.exports = router;