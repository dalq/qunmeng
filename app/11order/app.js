var App = angular.module('order', []);

App.config(require('./router'));
App.factory('orderservice', require('./service'));

App.controller('userMsg',require('./controllers/userMsg'));
App.controller('backnum',require('./controllers/backnum'));
App.controller('orderinfo',require('./controllers/orderinfo'));
App.controller('relaymessage',require('./controllers/relaymessage'));
App.controller('backticket',require('./controllers/backticket'));
 App.controller('orderinfo11',require('./controllers/orderinfo11'));
App.controller('orderlist1',require('./controllers/orderlist1'));
App.controller('orderticketlist',require('./controllers/orderticketlist'));
App.controller('13grouplist',require('./controllers/grouplist'));
App.controller('grouplistinfo',require('./controllers/grouplistinfo'));
App.controller('groupjqlist',require('./controllers/groupjqlist'));
App.controller('supplyOrderList',require('./controllers/supplyOrderList'));
App.controller('areaOrderList',require('./controllers/areaOrderList'));
App.controller('areaOrderList',require('./controllers/areaOrderList'));
App.controller('marketOrderList',require('./controllers/marketOrderList'));
App.controller('orderCartList',require('./controllers/orderCartList'));

module.exports = App;