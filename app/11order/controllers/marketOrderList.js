module.exports = function ($scope, $state, marketOrderList, getDate,toaster, 
	$modal, ticketlist, createBackOrder, resend, getRedCorridorOrderList,
    getRedCorridorResentMsg, getRedCorridorTrSendSms, orderbacklist, relay,
    getOrderSimInfo, agencyOrderRepeatECode, updateTicketEffectTime, str2date,
    getroyalocOrdersState) {

    var ticketstate_arr=[
        {
        'label':'查询全部',
        'value':'',
       },
       {
        'label':'未出票',
        'value':'0',
       }, 
       {
        'label':'等待出票',
        'value':'1',
       },
        {
        'label':'出票完成',
        'value':'2',
       },
        {
        'label':'无需出票',
        'value':'8',
       },
        {
        'label':'出票失败',
        'value':'9',
       },
       
   ];
   $scope.tstate=ticketstate_arr;

	$scope.searchform = {};

	  $scope.date = {
               
                'opened': false
            }


             $scope.dateOpen = function ($event, item) {
                $event.preventDefault();
                $event.stopPropagation();
                item.opened = true;
            };

            $scope.date1 = {
                 // 'lable': date2str2(new Date()),
                //'value': date2str(new Date()),
                'opened': false
            }


             $scope.dateOpen1 = function ($event, item) {
                $event.preventDefault();
                $event.stopPropagation();
                item.opened = true;
            };
             function date2str2(d) {
                if (d === undefined) {
                    return "";
                }
                var month = (d.getMonth() + 1).toString();
                var day = d.getDate().toString();
                if (month.length == 1) month = '0' + month;
                if (day.length == 1) day = '0' + day;
                return d.getFullYear() + "-" + month + "-" + day;
            }
	

    /* 分页
     * ========================================= */
	$scope.maxSize = 5;            //最多显示多少个按钮
	$scope.bigCurrentPage = 1;      //当前页码
	$scope.itemsPerPage = 10;         //每页显示几条
    

	$scope.myKeyup = function (e) {

		//IE 编码包含在window.event.keyCode中，Firefox或Safari 包含在event.which中
		var keycode = window.event ? e.keyCode : e.which;
		if (keycode == 13) {
			$scope.load();
		}
	};

	$scope.load = function () {
		 if($scope.date.lable){
                    $scope.tour_date=date2str2($scope.date.lable);;
                }else{
                    $scope.tour_date=''
                }
                if($scope.date1.lable){
                    $scope.tour_date_two=date2str2($scope.date1.lable);;
                }else{
                    $scope.tour_date_two=''
                }
                if($scope.date.lable!=null && $scope.date1.lable!=null){
                    if($scope.tour_date>$scope.tour_date_two){
                        alert('开始日期不能大于结束日期');
                        return;
                    }
                }
                if($scope.date.lable){
                    if(!$scope.date1.lable){
                        alert('开始和结束日期必须同时输入进行搜索')
                        return;
                    }
                }
                if($scope.date1.lable){
                    if(!$scope.date.lable){
                        alert('开始和结束日期必须同时输入进行搜索')
                        return;
                    }
                }

        
        if( $scope.tour_date==='' ||  $scope.tour_date===undefined){
             var para = {
                pageNo:$scope.bigCurrentPage, 
                pageSize:$scope.itemsPerPage,
                start_time : '',
                end_time : '',
           };
        }else{

            var para = {
                pageNo:$scope.bigCurrentPage, 
                pageSize:$scope.itemsPerPage,
                start_time : $scope.tour_date+ " 00:00:00",
                end_time : $scope.tour_date_two+ " 23:59:59",
            };

        }
		para = angular.extend($scope.searchform, para);
		marketOrderList.save(para, function (res) {
			if (res.errcode === 0) {
				$scope.objs = res.data.results;
				$scope.bigTotalItems = res.data.totalRecord;
			}
			else {
				toaster.error({title: "提示", body:res.errmsg});
			}
		});

	};
	$scope.load();


    $scope.ticketlist = function(obj){

        //$state.go('app.orderticketlist', {'code' : code});

        var modalInstance = $modal.open({
          template: require('../views/orderticketlist.html'),
          controller: 'orderticketlist',
          size: 'lg',
          resolve: {
            obj : function(){
                return obj;
            },
            ticketlist : function(){
                return ticketlist;
            },
            createBackOrder : function(){
                return createBackOrder;
            },
            //红海滩
            getRedCorridorOrderList : function(){
                return getRedCorridorOrderList;
            },
            //退票历史
            orderbacklist : function(){
                return orderbacklist;
            },
            //北京票联  红海滩廊道
            getOrderSimInfo : function(){
                return getOrderSimInfo;
            },
            //修改生效时间
            updateTicketEffectTime : function(){
                return updateTicketEffectTime;
            },
            getDate : function(){
                return getDate;
            },
            str2date : function(){
                return str2date;
            },
            getroyalocOrdersState : function(){
                return getroyalocOrdersState;
            }
          }
        });

        modalInstance.result.then(function () {
          //load();
        }, function () {
          //$log.info('Modal dismissed at: ' + new Date());
        });

    };

	

    //打开模态框
    $scope.orderinfo = function(obj){

        var modalInstance = $modal.open({
          template: require('../views/orderinfo.html'),
          controller: 'orderinfo11',
          size: 'lg',
          resolve: {
            obj : function(){
                return obj;
            }
          }
        });

        modalInstance.result.then(function () {
          //load();
        }, function () {
          //$log.info('Modal dismissed at: ' + new Date());
        });
    }


    $scope.resend = function(obj){
        var fun;

        var para = {};
        if(obj.sale_belong === 'juyou' || obj.sale_belong.indexOf('supply_piaofutong') == 0 ||  obj.sale_belong.indexOf('supply_tstc') == 0 ||  obj.sale_belong === 'supply_tongchenglvyou' || obj.sale_belong === 'supply_zhiyoubao' || obj.sale_belong === 'supply_xiaojing' || obj.sale_belong === 'supply_ziwoyou')
        {
            fun = resend;
            para['code'] = obj.code;
        }
        else if(obj.sale_belong == 'langdao')
        {
            fun = getRedCorridorResentMsg;
            para['code'] = obj.code;
        }
        else if(obj.sale_belong == 'huaxiapiaolian')
        {
            fun = agencyOrderRepeatECode;
            para['order_code'] = obj.code;
            para['ownerMobile'] = obj.mobile;
        }

        fun.save(para, function(res){
            if(res.errcode === 0)
            {
                alert('发送成功');
            }
            else
            {
                alert(res.errmsg);
            }
        });
    }

    $scope.relay = function(obj){
        
    	var modalInstance = $modal.open({
          template: require('../views/relaymessage.html'),
          controller: 'relaymessage',
          size: 'xs',
          resolve: {
            code : function(){
                return obj.code;
            },
            relay : function(){
                return relay;
            },
            getRedCorridorTrSendSms : function(){
                return getRedCorridorTrSendSms;
            },
            sale_belong : function(){
                return obj.sale_belong;
            },
            agencyOrderRepeatECode : function(){
                return agencyOrderRepeatECode;
            }
          }
        });

        modalInstance.result.then(function () {
            $scope.load();
        }, function () {
            
        });
    }

    $scope.back = function(obj){
        
    	var modalInstance = $modal.open({
          template: require('../views/backticket.html'),
          controller: 'backticket',
          size: 'xs',
          resolve: {
            code : function(){
                return obj.code;
            },
            num : function(){
                return obj.num;
            },
            createBackOrder : function(){
                return createBackOrder;
            }
          }
        });

        modalInstance.result.then(function () {
            $scope.load();
        }, function () {
            
        });
    }

};