/**
 * 模态框
 */
module.exports = function ($scope, $state, $stateParams, $modalInstance, items) {

	$scope.obj = items;
	$scope.objs = {};
	$scope.authority = true;
	$scope.middle;
	$scope.order_unique_code = $scope.obj.order_unique_code;
	$scope.ticketUrl = '';
	$scope.UrlList = {
		'ticketlist': '/api/as/tc/ticket2/orderticketlist',
		//Red 廊道
		'getRedCorridorOrderList': '/api/ac/tc/ticketRedCorridorService/getRedCorridorOrderList',
		//北京票联  红海滩廊道
		'getOrderSimInfo': '/api/ac/dc/huaxiapl/getOrderSimInfo',
		//皇家极地海洋馆信息
		'getroyalocOrdersState': '/api/ac/dc/royaloc/getOrdersState'
	}

	if ($scope.obj.sale_belong === 'juyou' || $scope.obj.sale_belong.indexOf('supply_piaofutong') == 0 ||  $scope.obj.sale_belong.indexOf('supply_tstc') == 0 ||  $scope.obj.sale_belong === 'supply_tongchenglvyou' || $scope.obj.sale_belong === 'supply_zhiyoubao' || $scope.obj.sale_belong === 'supply_xiaojing' || obj.sale_belong === 'supply_ziwoyou') {
		$scope.ticketUrl = $scope.UrlList.ticketlist;
	}
	else if ($scope.obj.sale_belong === 'langdao') {
		viewname = '红海滩廊道';
		$scope.ticketUrl = $scope.UrlList.getRedCorridorOrderList;
	}
	else if ($scope.obj.sale_belong === 'huaxiapiaolian') {
		viewname = '红海滩廊道';
		$scope.ticketUrl = $scope.UrlList.getOrderSimInfo;
	}
	else if ($scope.obj.sale_belong === 'royaloc') {
		viewname = '皇家极地海洋馆';
		$scope.ticketUrl = $scope.UrlList.getroyalocOrdersState;
	}

	$.ajax({
		'type': 'POST',
		'contentType': 'application/json',
		'url': $scope.ticketUrl,
		'dataType': 'json',
		'data': JSON.stringify({ order_code : $scope.obj.code }),
		'success': function (res) {

			if (res.errcode != 0) {
				alert('获取数据失败 :\n'+res.errmsg);
				return false;
			}

			for (var i = res.data.length - 1; i >= 0; i--) {
				res.data[i].take_effect_time = str2date(res.data[i].take_effect_time);
			}

			/* 门票存储结构
			* ========================================= */
			var tkt = new Object();
			var restkt = new Array();

			// if (res.errcode !== 0) {
			// 	alert("数据获取失败");
			// 	return;
			// }

			var arr = [];
			if ($scope.obj.sale_belong === 'juyou' || $scope.obj.sale_belong.indexOf('supply_piaofutong') == 0 ||  $scope.obj.sale_belong.indexOf('supply_tstc') == 0 ||  $scope.obj.sale_belong === 'supply_tongchenglvyou' || $scope.obj.sale_belong === 'supply_zhiyoubao' || $scope.obj.sale_belong === 'supply_xiaojing') {
				arr = res.data;
			}
			else if ($scope.obj.sale_belong === 'huaxiapiaolian') {
				arr = change_huaxiapiaolian(res.data.order);
			}
			else if ($scope.obj.sale_belong === 'langdao') {
				arr = change(res.data);
			}
			else if ($scope.obj.sale_belong === 'royaloc') {
				arr = change_royaloc(res.data);
			}

			for (var i = 0, j = arr.length; i < j; i++) {
				var tt = arr[i];
				var v = tt.sequence;

				if (!tkt.hasOwnProperty(v)) {
					tkt[v] = new Object();
					tkt[v].ticketarr = new Array();
					tkt[v].sequence = tt.sequence;
					tkt[v].name = tt.order_name;
					tkt[v].newdate = tt.take_effect_time;
				}
				tkt[v].ticketarr.push(tt);
			}

			for (var key in tkt) {
				var o = tkt[key];
				restkt.push(o);
			}

			$scope.objs = restkt;
		}
	});
};