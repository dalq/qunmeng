module.exports = function($scope, $resource, uid, $modalInstance, toaster){

    init();
    function init(){
        $scope.isWXUser = false;
        var url = '/api/ac/uc/userService/getUserInfoById';
        var para = {'userid': uid}
        if(uid.indexOf('wx') == 0){
            url = '/api/us/uc/userwxsk/getUserInfoByUserid';
            para.user_base = uid;
            $scope.isWXUser = true;
            delete para.userid;
        }
        $resource(url, {}, {}).save(para, function(res){
			if(res.errcode === 0){
                $scope.userInfo = res.data;
                if($scope.isWXUser){
                    $scope.userInfo.city = res.data.country + '-' + res.data.province + '-' + res.data.city;
                }
			} else {
				toaster.error({title: '', body: res.errmsg});
			}
		});
    }

    $scope.cancel = function (){
    	$modalInstance.close();
    }

};