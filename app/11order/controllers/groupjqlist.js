module.exports = function($scope, grouporderlist,  $modal, getDate, infolist){


       
     $scope.date = {
                 // 'lable': date2str2(new Date()),
                //'value': date2str(new Date()),
                'opened': false
            }


             $scope.dateOpen = function ($event, item) {
                $event.preventDefault();
                $event.stopPropagation();
                item.opened = true;
            };
             function date2str2(d) {
                if (d === undefined) {
                    return "";
                }
                var month = (d.getMonth() + 1).toString();
                var day = d.getDate().toString();
                if (month.length == 1) month = '0' + month;
                if (day.length == 1) day = '0' + day;
                return d.getFullYear() + "-" + month + "-" + day;
            }

    $scope.searchform = {};

    //有效区间
    $scope.section = {};
    $scope.section.start = {};
    // $scope.section.start.date = new Date();

    // $scope.open = function(obj) {
    //     obj.opened = true;
    // };

    /* 分页
     * ========================================= */
    $scope.maxSize = 5;            //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10;         //每页显示几条
    

	$scope.myKeyup = function (e) {

		//IE 编码包含在window.event.keyCode中，Firefox或Safari 包含在event.which中
		var keycode = window.event ? e.keyCode : e.which;
		if (keycode == 13) {
			$scope.load();
		}
	};
    
    $scope.load = function () {

        if($scope.date.lable){
                    $scope.tour_date=date2str2($scope.date.lable);;
                }else{
                    $scope.tour_date=''
                }

    	var para = {
            pageNo:$scope.bigCurrentPage, 
            pageSize:$scope.itemsPerPage,
            arrival_date :  $scope.tour_date,
            del_flg : 0,
            outstate : 1
        };
        
        para = angular.extend($scope.searchform, para);

        grouporderlist.save(para, function(res){

            if(res.errcode === 0)
            {
                $scope.objs = res.data.results;
                $scope.bigTotalItems = res.data.totalRecord;
            }
            else
            {
                alert(res.errmsg);
            }

        });

    };
    $scope.load();

    $scope.info = function (code) {
        var modalInstance = $modal.open({
          template: require('../views/grouplistinfo.html'),
          controller: 'grouplistinfo',
          url:'groupjq_info',
          size: 'lg',
          resolve: {
            code : function(){
                return code;
            },
            infolist : function(){
                return infolist;
            }
          }
        });

        modalInstance.result.then(function () {
            $scope.load();
        }, function () {
            //$scope.load();
          //$log.info('Modal dismissed at: ' + new Date());
        });


    	//$state.go('app.infosellinggroup', {'code' : code});
    };

    $scope.print = function () {
    	window.print();
    }

};