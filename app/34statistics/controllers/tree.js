module.exports = function ($scope, $modalInstance, $resource, $modal, data, toaster) {
    $scope.data = data;
    $scope.back = function () {
        $modalInstance.dismiss('cancel');
    }
    $scope.ok = function (obj) {
        $scope.obj = obj;
        $modalInstance.close(obj);
    }
    $scope.select = function (obj) {
        $scope.selectedCompany = obj.$modelValue.name
        $scope.obj = obj;
    }
    $scope.save = function () {
        if (!$scope.obj) {
            toaster.warning({ title:"", body:"请选择机构" });
            return false;
        }
        $modalInstance.close($scope.obj);
    }
};