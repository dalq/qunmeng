module.exports = function($scope, staticonline, getDate){
	
	/* 分页
     * ========================================= */
    $scope.maxSize = 5;            //最多显示多少个按钮
    $scope.bigCurrentPage = 1;      //当前页码
    $scope.itemsPerPage = 10;         //每页显示几条
	
	
	$scope.searchform = {};
    //有效区间
    // $scope.section = {};
    // $scope.section.start = {};
    // $scope.section.start.date = new Date();

    // $scope.section.end = {};
    // $scope.section.end.date = new Date();

     $scope.date = {
                 'lable': date2str2(new Date()),
                //'lable': new Date(),
                //'value': date2str(new Date()),
                'opened': false
            }


             $scope.dateOpen = function ($event, item) {
                $event.preventDefault();
                $event.stopPropagation();
                item.opened = true;
            };

            $scope.date1 = {
                  'lable': date2str2(new Date()),
                //'value': date2str(new Date()),
                'opened': false
            }


             $scope.dateOpen1 = function ($event, item) {
                $event.preventDefault();
                $event.stopPropagation();
                item.opened = true;
            };
             function date2str2(d) {
                if (d === undefined) {
                    return "";
                }
                var month = (d.getMonth() + 1).toString();
                var day = d.getDate().toString();
                if (month.length == 1) month = '0' + month;
                if (day.length == 1) day = '0' + day;
                return d.getFullYear() + "-" + month + "-" + day;
            }


    $scope.card_sum = 0;
    
    //初始化
    $scope.load = function() {

            if($scope.date.lable){
                    if(typeof $scope.date.lable === 'string'){
                        $scope.tour_date=$scope.date.lable;
                    }else{
                            $scope.tour_date=date2str2($scope.date.lable);
                    }
                }else{
                    $scope.tour_date='';
                }
                if($scope.date1.lable){
                    if(typeof $scope.date1.lable === 'string'){
                        $scope.tour_date_two=$scope.date1.lable;
                    }else{
                            $scope.tour_date_two=date2str2($scope.date1.lable);
                    }
            
                }else{
                    $scope.tour_date_two='';
                }
                if($scope.tour_date ===''){
                    $scope.searchform.starttime='';
                }else{
                    $scope.searchform.starttime = $scope.tour_date + ' 00:00:00';
                }
                 if($scope.tour_date_two ===''){
                   $scope.searchform.endtime ='';
                }else{
                   $scope.searchform.endtime = $scope.tour_date_two + ' 23:59:59';
                }
        	
        var para = {
        pageNo:$scope.bigCurrentPage, 
        pageSize:$scope.itemsPerPage
        };
        para = angular.extend($scope.searchform, para);
    	staticonline.save(para, function(res){


            if(res.errcode === 0) {
        		$scope.objs = res.data.results;
        		$scope.bigTotalItems = res.data.totalRecord;
                if (res.data.results.length) {
                     $scope.card_sum = res.data.results[0].card_sum;
                }
            } else {
                alert(res.errmsg);
            }

        });
    }
    $scope.load();
    
    //日期控件开关
    // $scope.open = function(obj) {
    //     obj.opened = true;
    // };
}