module.exports = function($scope, getDate, companySaleStatistic, companySaleStatisticHistory, DateDiff,
    $modal, orderstatisticsusedinfolist){

    init();
    function init(){
        $scope.chaxunarr = [
            {'label':'实时查询', 'value':'0'},
            {'label':'历史查询', 'value':'1'}, 
        ];
        $scope.ticket_arr = [
            {'label':'电子票', 'value': '01', 'active': false, 'status': false},
            {'label':'套票', 'value': '02', 'active': false, 'status': false},
            {'label':'礼品', 'value': '03', 'active': false, 'status': false},
            {'label':'居游套票', 'value': '04', 'active': false, 'status': false},
            {'label':'补贴票', 'value': '05', 'active': false, 'status': false},
            {'label':'限时购', 'value': '06', 'active': false, 'status': false},
            {'label':'月票', 'value': '10', 'active': false, 'status': false},
            {'label':'补贴团票', 'value':'98', 'active': false, 'status': false},
            {'label':'团票', 'value':'99', 'active': false, 'status': false}
        ];
        $scope.searchform = {'seltype': '0'};
        $scope.total = {};
        $scope.date = {
            'lable': date2str2(new Date()),
            'opened': false
        };
        $scope.date1 = {
            'lable': date2str2(new Date()),
            'opened': false
        };
    }

    //打开日历空间,选择时间
    $scope.dateOpen = function ($event, item) {
        $event.preventDefault();
        $event.stopPropagation();
        item.opened = true;
    };

    //日期转字符串
    function date2str2(d) {
        if (d === undefined) {
            return "";
        }
        if(typeof d === 'string'){
            return d;
        }
        var month = (d.getMonth() + 1).toString();
        var day = d.getDate().toString();
        if (month.length == 1) month = '0' + month;
        if (day.length == 1) day = '0' + day;
        return d.getFullYear() + "-" + month + "-" + day;
    }

    //查询数据
    $scope.load = function () {
        var fun, para;
        if($scope.searchform.seltype == '0'){
            //实时查询
            var iDays = date2str2(new Date());
            fun = companySaleStatistic;
            para = {
                'start_time' : iDays + " 00:00:00",
                'end_time' : iDays + " 23:59:59",
            };
        } else {
            //历史查询
            fun = companySaleStatisticHistory;
            para = {
                'start_time' : date2str2($scope.date.lable),
                'end_time' : date2str2($scope.date1.lable),
            };
        }
        angular.extend(para, $scope.searchform);

        fun.save(para, function(res){
            $scope.total.buy = 0;
            $scope.total.used = 0;
            $scope.total.back = 0;
            $scope.total.total = 0;
            $scope.total.purchase_total = 0;
            var ticket_arr_str = '';
            if(res.errcode === 0) {
        		$scope.objs = res.data;
                angular.forEach($scope.objs, function(item){
                    item.show = true;
                    $scope.total.buy += parseInt(item.buy);
                    $scope.total.used += parseInt(item.used);
                    $scope.total.back += parseInt(item.back);
                    $scope.total.total += parseInt(item.total_buy - item.total_back);
                    $scope.total.purchase_total += parseInt(item.total_purchase_price);
                    if(item.ticket_type_attr.indexOf(',') > 0){
                        var temp = item.ticket_type_attr.split(',');
                        angular.forEach(temp, function(attr){
                            if(ticket_arr_str.indexOf(attr) == -1){
                                ticket_arr_str += attr + ','
                            }
                        });
                    } else {
                        if(ticket_arr_str.indexOf(item.ticket_type_attr) == -1){
                            ticket_arr_str += item.ticket_type_attr + ','
                        }
                    }
                });
                angular.forEach($scope.ticket_arr, function(item){
                    if(ticket_arr_str.indexOf(item.value) != -1){
                        item.active = true;
                        item.status = true;
                    } else {
                        item.active = false;
                        item.status = false;
                    }
                });
            } else {
                alert(res.errmsg);
            }
        });
    };

    //选择显示的票类型
    $scope.checkattr = function(attr){
        $scope.total.buy = 0;
        $scope.total.used = 0;
        $scope.total.back = 0;
        $scope.total.total = 0;
        $scope.total.purchase_total = 0;
        angular.forEach($scope.objs, function(item){
            if(item.ticket_type_attr.indexOf(',') == -1){
                if(item.ticket_type_attr == attr.value){
                    item.show = attr.status;
                }
            } else {
                item.show = false;
                var temp;
                for(var i = 0, j = $scope.ticket_arr.length; i < j; i++){
                    temp = $scope.ticket_arr[i];
                    if(temp.active && temp.status && item.ticket_type_attr.indexOf(temp.value) != -1){
                        item.show = true;
                        break;
                    }
                }
            }
            if(item.show){
                $scope.total.buy += parseInt(item.buy);
                $scope.total.used += parseInt(item.used);
                $scope.total.back += parseInt(item.back);
                $scope.total.total += parseInt(item.total_buy - item.total_back);
                $scope.total.purchase_total += parseInt(item.total_purchase_price);
            }
        });
    }

    $scope.detail = function (obj) {
        var modalInstance = $modal.open({
          template: require('../views/statisticsdetail.html'),
          controller: 'statisticsdetail',
          size: 'lg',
          resolve: {
            day : function(){
                return obj.date;
            },
            sale_code : function(){
                return obj.sale_code;
            },
            sale_name : function(){
                return obj.name;
            },
            company_code : function(){
                return obj.company_code;
            },
            operation_login_name : function(){
                return undefined;
            },
            orderstatisticsusedinfolist : function(){
                return orderstatisticsusedinfolist;
            }
          }
        });

        modalInstance.result.then(function () {
            $scope.load();
        }, function () {
            
        });
    };
    

};