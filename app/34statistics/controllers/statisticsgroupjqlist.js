module.exports = function($scope, getDate, groupcountjqlist){

    $scope.searchform = {};

    $scope.total = {
        'book_count' : 0,
        'total' : 0
    };

    //有效区间
    // $scope.section = {};
    // $scope.section.start = {};
    // $scope.section.start.date = new Date();

    // $scope.section.end = {};
    // $scope.section.end.date = new Date();

    // $scope.open = function(obj) {
    //     obj.opened = true;
    // };

    $scope.date = {
                 'lable': date2str2(new Date()),
                //'lable': new Date(),
                //'value': date2str(new Date()),
                'opened': false
            }


             $scope.dateOpen = function ($event, item) {
                $event.preventDefault();
                $event.stopPropagation();
                item.opened = true;
            };

            $scope.date1 = {
                  'lable': date2str2(new Date()),
                //'value': date2str(new Date()),
                'opened': false
            }


             $scope.dateOpen1 = function ($event, item) {
                $event.preventDefault();
                $event.stopPropagation();
                item.opened = true;
            };
             function date2str2(d) {
                if (d === undefined) {
                    return "";
                }
                var month = (d.getMonth() + 1).toString();
                var day = d.getDate().toString();
                if (month.length == 1) month = '0' + month;
                if (day.length == 1) day = '0' + day;
                return d.getFullYear() + "-" + month + "-" + day;
            }
    $scope.load = function () {

         if($scope.date.lable){
                    if(typeof $scope.date.lable === 'string'){
                        $scope.tour_date=$scope.date.lable;
                    }else{
                            $scope.tour_date=date2str2($scope.date.lable);
                    }
                }else{
                    $scope.tour_date='';
                }
                if($scope.date1.lable){
                    if(typeof $scope.date1.lable === 'string'){
                        $scope.tour_date_two=$scope.date1.lable;
                    }else{
                            $scope.tour_date_two=date2str2($scope.date1.lable);
                    }
            
                }else{
                    $scope.tour_date_two='';
                }
        
        
        var para = {
            start :  $scope.tour_date,
            end :  $scope.tour_date_two
        };

        para = angular.extend($scope.searchform, para);

        
        groupcountjqlist.save(para, function(res){


            $scope.total = {
		        'book_count' : 0,
                'actual_count' : 0,
		        'total' : 0
		    };

            if(res.errcode === 0)
            {
                for(var i = 0, j = res.data.length; i < j; i++)
                {
                    $scope.total.actual_count += parseInt(res.data[i].actual_count);
                    $scope.total.total += parseInt(res.data[i].actual_count * res.data[i].cost_price);
                }
                $scope.objs = res.data;
            }
            else
            {
                alert(res.errmsg);
            }

        });

    };
    $scope.load();

    
    

};