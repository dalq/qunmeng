module.exports = function($scope, getDate, saleStatisticAllPlace, saleStatisticAllPlaceHistory, DateDiff,
    $modal, orderstatisticsusedinfolist){

    init();
    function init(){
        $scope.chaxunarr = [
            {'label':'实时查询', 'value':'0'},
            {'label':'历史查询', 'value':'1'}, 
        ];
        $scope.ticket_arr = [
            {'label':'全部', 'value': undefined},
            {'label':'电子票', 'value': '01'},
            {'label':'套票', 'value': '02'},
            {'label':'居游套票', 'value': '04'},
            {'label':'限时购', 'value': '06'},
            {'label':'团票', 'value':'99'}
        ];
        $scope.searchform = {'seltype': '0'};
        $scope.total = {};
        $scope.date = {
            'lable': date2str2(new Date()),
            'opened': false
        };
        $scope.date1 = {
            'lable': date2str2(new Date()),
            'opened': false
        };
    }
    
    //打开日历空间,选择时间
    $scope.dateOpen = function ($event, item) {
        $event.preventDefault();
        $event.stopPropagation();
        item.opened = true;
    };

    //日期转字符串
    function date2str2(d){
        if (d === undefined) {
            return "";
        }
        var month = (d.getMonth() + 1).toString();
        var day = d.getDate().toString();
        if (month.length == 1) month = '0' + month;
        if (day.length == 1) day = '0' + day;
        return d.getFullYear() + "-" + month + "-" + day;
    }

    /* 分页
     * ========================================= */
    // $scope.maxSize = 5;            //最多显示多少个按钮
    // $scope.bigCurrentPage = 1;      //当前页码
    // $scope.itemsPerPage = ITEMS_PERPAGE;         //每页显示几条

    $scope.load = function(){
        var fun, para;
        
        if(typeof $scope.date.lable === 'string'){
            $scope.tour_date = $scope.date.lable;
        } else {
            $scope.tour_date = date2str2($scope.date.lable);
        }
        if(typeof $scope.date1.lable === 'string'){
            $scope.tour_date_two = $scope.date1.lable;
        } else {
            $scope.tour_date_two = date2str2($scope.date1.lable);
        }

    	if($scope.searchform.seltype == '0'){
            //实时查询
    		var iDays = date2str2(new Date());
    		fun = saleStatisticAllPlace;
    		para = {
	            'start_time' : iDays + " 00:00:00",
	            'end_time' : iDays + " 23:59:59",
            };
            delete $scope.searchform.ticket_type_attr;
    	} else {
            //历史查询
    		fun = saleStatisticAllPlaceHistory;
    		para = {
                'start_time' : $scope.tour_date,
	            'end_time' : $scope.tour_date_two,
	        };
        }
        angular.extend(para, $scope.searchform);
        fun.save(para, function(res){
            $scope.total.buy = 0;
            $scope.total.used = 0;
            $scope.total.back = 0;
            $scope.total.total = 0;
            if(res.errcode === 0) {
        		$scope.objs = res.data;
                for(var i = 0, j = res.data.length; i < j; i++){
                	if($scope.searchform.seltype == 1){
            			$scope.objs[i].name = res.data[i].sale_name;
            		}
                    $scope.total.buy += parseInt(res.data[i].buy);
                    $scope.total.used += parseInt(res.data[i].used);
                    $scope.total.back += parseInt(res.data[i].back);
                    $scope.total.total += parseInt(res.data[i].total_buy - res.data[i].total_back);
                }
            } else {
                alert(res.errmsg);
            }
        });
    };

    $scope.detail = function (obj) {
        var modalInstance = $modal.open({
          template: require('../views/statisticsdetail.html'),
          controller: 'statisticsdetail',
          size: 'lg',
          resolve: {
            day : function(){
                return obj.date;
            },
            sale_code : function(){
                return obj.sale_code;
            },
            sale_name : function(){
                return obj.name;
            },
            company_code : function(){
                return obj.company_code;
            },
            operation_login_name : function(){
                return undefined;
            },
            orderstatisticsusedinfolist : function(){
                return orderstatisticsusedinfolist;
            }
          }
        });

        modalInstance.result.then(function () {
            $scope.load();
        }, function () {
            
        });
    };
    

};