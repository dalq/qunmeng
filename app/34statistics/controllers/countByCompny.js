module.exports = function ($scope, $state, mechanism, $modal, countByCompnycode, toaster, getDate, Excel, $timeout, date2str) {


var company_arr=[
      {
          'label':'本级',
          'value':'1',
      },
      {
          'label':'下级',
          'value':'2',
      },
      {
          'label':'本级+下级',
          'value':'3',
      }
  ];
  $scope.companyarr=company_arr;
  var ticket_arr=[
      {
          'label':'电子票',
          'value':'1',
      },
      {
          'label':'团票',
          'value':'2',
      },
  
  ];
  $scope.tarr=ticket_arr;


	$scope.objs = [];
	$scope.ExcelName = '';

	$scope.searchform = {};
	$scope.searchform.company_type = '1';
	$scope.searchform.ticket_type = '1';
	$scope.tempData = {};
	$scope.tempSearchData = {};
	$scope.selectedCompany = '';

	$scope.ticket_type_flag = $scope.searchform.ticket_type;

	//有效区间
	$scope.section = {};
	$scope.section.start = {
		'date': {
		}
	};
	$scope.section.start.date = {
		'label': date2str(new Date(new Date().getTime() - 7 * 24 * 3600 * 1000)),
		'value': date2str(new Date(new Date().getTime() - 7 * 24 * 3600 * 1000)),
		'opened': false
	};
	$scope.section.end = {
		'date': {
		}
	};
	$scope.section.end.date = {
		'label': date2str(new Date()),
		'value': date2str(new Date()),
		'opened': false
	};


	$scope.open = function ($event, item) {
		$event.preventDefault();
		$event.stopPropagation();
		item.opened = true;
	}

	$scope.clearDate = function (obj) {
		delete obj.date;
	};

	$scope.code = '';
	$scope.officeid = '';

	//当前的id
	$scope.currentid = '';
	$scope.currentcode = '';
	$scope.currentname = '';

	$scope.editshow = false;

	//机构树
	function mechanismtree() {
		mechanism.save({'suboffice': '1'}, function (res) {
			$scope.tempData = angular.copy(res.data);
			$scope.data = transData(res.data, 'id', 'parent_id', 'nodes');
			// $scope.currentid = $scope.data[0].id;
			// $scope.currentcode = $scope.data[0].code;
			// $scope.currentname = $scope.data[0].name;
			// $scope.load($scope.data[0].id, $scope.data[0].name);
		});
	}
	mechanismtree();

	function transData(a, idStr, pidStr, chindrenStr) {
		var r = [],
			hash = {},
			id = idStr,
			pid = pidStr,
			children = chindrenStr,
			i = 0,
			j = 0,
			len = a.length;

		for (; i < len; i++) {
			hash[a[i][id]] = a[i];
		}

		for (; j < len; j++) {
			var aVal = a[j],
				hashVP = hash[aVal[pid]];

			if (hashVP) {
				!hashVP[children] && (hashVP[children] = []);
				hashVP[children].push(aVal);
			} else {
				r.push(aVal);
			}

		}
		return r;
	}

    /* 分页
     * ========================================= */
	$scope.maxSize = 5;            //最多显示多少个按钮
	$scope.bigCurrentPage = 1;      //当前页码
	$scope.itemsPerPage = 10;         //每页显示几条


	$scope.myKeyup = function (e) {

		//IE 编码包含在window.event.keyCode中，Firefox或Safari 包含在event.which中
		var keycode = window.event ? e.keyCode : e.which;
		if (keycode == 13) {
			$scope.load();
		}
	};

	$scope.load = function () {

		if($scope.section.start.date.label == null){
			toaster.warning({title:'',body:'请选择开始日期'});
			return false;
		}else if (typeof $scope.section.start.date.label === 'string') {
			var start = $scope.section.start.date.label;
		} else {
			var start = date2str($scope.section.start.date.label);
		}
		if($scope.section.end.date.label == null){
			toaster.warning({title:'',body:'请选择结束日期'});
			return false;
		}else if (typeof $scope.section.end.date.label === 'string') {
			var end = $scope.section.end.date.label;
		} else {
			var end = date2str($scope.section.end.date.label);
		}
		// if (!($scope.section.start.date && $scope.section.end.date)) {
		// 	alert('请选择统计时间');
		// 	return false;
		// }
		if (!$scope.selectedCompany) {
			alert('请选择机构');
			return false;
		}
		$scope.ExcelName = start + '至' + end;

		var para = {
			pageNo: $scope.bigCurrentPage,
			pageSize: $scope.itemsPerPage,
			start_date: start,
			end_date: end
		};

		para = angular.extend($scope.searchform, para);

		countByCompnycode.save(para, function (res) {
			if (res.errcode != 0) {
				toaster.error({ body: res.errmsg })
			}
			$scope.ticket_type_flag = $scope.searchform.ticket_type;
			if (res.data[0]) {
				$scope.objs = res.data;
			} else {
				$scope.objs = [];
			}
			$scope.bigTotalItems = res.count;
		});

	};
	// $scope.load();

	$scope.getit = function (obj) {
		$scope.tempSearchData = obj.$modelValue.id;
		$scope.tempSearchData = {
			id: obj.$modelValue.id,
			name: obj.$modelValue.name,
			code: obj.$modelValue.code
		};
		$scope.selectedCompany = obj.$modelValue.name;
	};

	$scope.orderSearch = function () {
		$scope.searchform.code = $scope.tempSearchData.code;
		$scope.searchform.list = [];

		for (var index = 0; index < $scope.tempData.length; index++) {
			var element = $scope.tempData[index];
			if (element.parent_id == $scope.tempSearchData.id) {
				$scope.searchform.list.push({ code: element.code, list: [], id: element.id });
			}
		}

		for (var index = 0; index < $scope.searchform.list.length; index++) {
			var element = $scope.searchform.list[index];
			for (var index_temDat = 0; index_temDat < $scope.tempData.length; index_temDat++) {
				var tempArray = [];
				tempArray = $scope.tempData[index_temDat].parent_ids.split(',');
				for (var index_temArr = 0; index_temArr < tempArray.length; index_temArr++) {
					if (tempArray[index_temArr] == element.id) {
						$scope.searchform.list[index].list.push($scope.tempData[index_temDat].code);
						break;
					}
				}
			}
		}

		$scope.load();
	}

	$scope.infringementNotices = [
		{
			IsPay: '扣款',
			Operate: '通知'
		}
	];

	$scope.exportToExcel = function (tableId) {
		$timeout(function () {
			document.getElementById("dlink").href = Excel.tableToExcel(tableId, 'sheet1');
			//document.getElementById("dlink").download = "1122b.xls";//这里是关键所在,当点击之后,设置a标签的属性,这样就可以更改标签的标题了
			document.getElementById("dlink").click();
		}, 100); // trigger download

		
		// $scope.exportHref = Excel.tableToExcel(tableId, '哈哈哈哈哈sheet1');
		// $timeout(function () { location.href = $scope.exportHref; }, 100); // trigger download
	}

};