module.exports = function($scope, getDate, orderstatisticscompanylist, 
 orderstatisticscompanyhistorylist, DateDiff,
    $modal, orderstatisticsusedinfolist){

    var chaxun=[
      {
          'label':'实时查询',
          'value':'0',
      },
      {
          'label':'历史查询',
          'value':'1',
      },
     
  ];
  $scope.chaxunarr=chaxun;
    $scope.searchform = {};

    $scope.searchform.seltype = '0';

    $scope.total = {
        'buy' : 0,
        'used' : 0,
        'back' : 0,
        'total' : 0
    };

    $scope.showtype = '0';

    //有效区间
    // $scope.section = {};
    // $scope.section.start = {};
    // $scope.section.start.date = new Date();

    // $scope.section.end = {};
    // $scope.section.end.date = new Date();

    // $scope.open = function(obj) {
    //     obj.opened = true;
    // };

     $scope.date = {
                 'lable': date2str2(new Date()),
                //'lable': new Date(),
                //'value': date2str(new Date()),
                'opened': false
            }


             $scope.dateOpen = function ($event, item) {
                $event.preventDefault();
                $event.stopPropagation();
                item.opened = true;
            };

            $scope.date1 = {
                  'lable': date2str2(new Date()),
                //'value': date2str(new Date()),
                'opened': false
            }


             $scope.dateOpen1 = function ($event, item) {
                $event.preventDefault();
                $event.stopPropagation();
                item.opened = true;
            };
             function date2str2(d) {
                if (d === undefined) {
                    return "";
                }
                var month = (d.getMonth() + 1).toString();
                var day = d.getDate().toString();
                if (month.length == 1) month = '0' + month;
                if (day.length == 1) day = '0' + day;
                return d.getFullYear() + "-" + month + "-" + day;
            }


    $scope.changetype = function () {
    	if($scope.searchform.seltype == '0'){
    		$scope.showtype = '0';
    		$scope.date.lable = new Date();
    		$scope.date1.lable = new Date();
    	}else{
    		$scope.showtype = '1';
    	    $scope.date.lable =  date2str2(new Date());
    		$scope.date1.lable =  date2str2(new Date());
    	}
    }

    /* 分页
     * ========================================= */
    // $scope.maxSize = 5;            //最多显示多少个按钮
    // $scope.bigCurrentPage = 1;      //当前页码
    // $scope.itemsPerPage = ITEMS_PERPAGE;         //每页显示几条

    $scope.load = function () {

    	var fun;
    	var para;
    	var iDays;

        if($scope.date.lable){
                    if(typeof $scope.date.lable === 'string'){
                        $scope.tour_date=$scope.date.lable;
                    }else{
                            $scope.tour_date=date2str2($scope.date.lable);
                    }
                }else{
                    $scope.tour_date='';
                }
                if($scope.date1.lable){
                    if(typeof $scope.date1.lable === 'string'){
                        $scope.tour_date_two=$scope.date1.lable;
                    }else{
                            $scope.tour_date_two=date2str2($scope.date1.lable);
                    }
            
                }else{
                    $scope.tour_date_two='';
                }


    	if($scope.searchform.seltype == 0){
    		iDays = DateDiff($scope.tour_date, $scope.tour_date_two);
			if(iDays > 14){
				alert("不能选择超过两周的日期哦\n如有需求请选择历史查询");
				return;
				//$scope.section.start.date = new Date();
				//$scope.section.end.date = new Date();
			}
    		fun = orderstatisticscompanylist;
    		para = {
	            start_time : $scope.tour_date + " 00:00:00",
	            end_time : $scope.tour_date_two + " 23:59:59",
	        };
    	}else{
    		fun = orderstatisticscompanyhistorylist;
    		para = {
	             start_time : $scope.tour_date,
	            end_time : $scope.tour_date_two,
	        };
    	}

        para = angular.extend($scope.searchform, para);

        fun.save(para, function(res){


            $scope.total = {
                'buy' : 0,
                'used' : 0,
                'back' : 0,
                'total' : 0
            };

            if(res.errcode === 0)
            {
        		$scope.objs = res.data;
                for(var i = 0, j = res.data.length; i < j; i++)
                {
                	if($scope.searchform.seltype == 1){
            			$scope.objs[i].name = res.data[i].sale_name;
            		}
                    $scope.total.buy += parseInt(res.data[i].buy);
                    $scope.total.used += parseInt(res.data[i].used);
                    $scope.total.back += parseInt(res.data[i].back);
                    $scope.total.total += parseInt(res.data[i].total_buy - res.data[i].total_back);
                }
                //$scope.objs = res.data;
                //$scope.bigTotalItems = res.data.totalRecord;
            }
            else
            {
                alert(res.errmsg);
            }

        });

    };
    $scope.load();

    $scope.detail = function (obj) {
        var modalInstance = $modal.open({
          template: require('../views/statisticsdetail.html'),
          controller: 'statisticsdetail',
          size: 'lg',
          resolve: {
            day : function(){
                return obj.date;
            },
            sale_code : function(){
                return obj.sale_code;
            },
            sale_name : function(){
                return obj.name;
            },
            company_code : function(){
                return obj.company_code;
            },
            operation_login_name : function(){
                return undefined;
            },
            orderstatisticsusedinfolist : function(){
                return orderstatisticsusedinfolist;
            }
          }
        });

        modalInstance.result.then(function () {
            $scope.load();
        }, function () {
            
        });
    };
    

};