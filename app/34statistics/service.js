/**
 * 子模块service
 * dlq
 */
var service = function($resource, $q, $http){
    var BASEURL38985='';

    var uselist = BASEURL38985 + '/api/as/tc/ticketorder/orderlist';

    var destoryDetail = BASEURL38985 + '/api/as/tc/ticket2/destorydetaillist';

    var viewdestorystatisticlist = BASEURL38985 + '/api/as/tc/ticket2/viewdestorystatisticlist';

    var orderstatisticslist = BASEURL38985 + '/api/as/tc/ticketorder/orderstatisticslist';

    var orderstatisticscompanylist = BASEURL38985 + '/api/as/tc/ticketorder/orderstatisticscompanylist';
    //供应商销售统计---实时统计
    var companySaleStatistic = '/api/as/tc/ticketorder/companySaleStatisticList';
    //景区产品统计---实时统计
    var saleStatisticAllPlace = '/api/as/tc/ticketorder/saleStatisticAllPlaceList';
    //景区产品历史统计
    var saleStatisticAllPlaceHistory = '/api/as/tc/ticketorder/saleStatisticAllPlaceHistoryList';

    var govsubsidygoodscodelist = BASEURL38985 + '/api/as/tc/salegovsubsidy/govsubsidygoodscodelist';

    var groupcountlist = BASEURL38985 + '/api/as/tc/grouporder/groupcountlist';

    var groupcountjqlist = BASEURL38985 + '/api/as/tc/grouporder/groupcountjqlist';

    var useddetaillist = BASEURL38985 + '/api/as/tc/ticket2/useddetaillist';

    var grouplxslist = BASEURL38985 + '/api/as/tc/ticket2/grouplxslist';

    var grouplxslist = BASEURL38985 + '/api/as/tc/ticket2/grouplxslist';
    //销售统计 月查询
    var orderstatisticscompanyhistorylist = BASEURL38985 + '/api/as/tc/ticketorder/orderstatisticscompanyhistorylist';
    //供应商销售统计 历史查询
    var companySaleStatisticHistory = '/api/as/tc/ticketorder/orderstatisticssupplycompanyhistorylist';
	//同业销售统计 月查询
    var orderstatisticshistorylist = BASEURL38985 + '/api/as/tc/ticketorder/orderstatisticshistorylist';
	//商品统计 月查询
    var viewdestorystatistichistorylist = BASEURL38985 + '/api/as/tc/ticket2/viewdestorystatistichistorylist';

    //销售统计－使用详情
    var orderstatisticsusedinfolist = BASEURL38985 + '/api/as/tc/ticketorder/orderstatisticsusedinfolist';

    //在线套票统计
    var staticonline = BASEURL38985 + '/api/as/cdc/cardorder/getList';

    //各级分销统计
    var countByCompnycode = BASEURL38985 + '/api/ac/tc/ticketCountService/countByCompnycode';

    //商户列表
	var talist = BASEURL38985 + '/api/as/sc/office/getofficelist';
    //机构
    var mechanism =  '/api/as/sc/office/myofficeList';
    // var mechanism =  '/a/sys/office/treeData';

    return {

        mechanism : function(){
            return $resource(mechanism, {}, {} );
        },
         talist : function(obj){
            var deferred = $q.defer(); // 声明延后执行，表示要去监控后面的执行  
            // $http({method: 'GET', params: obj, url: talist}).  
            // success(function(data, status, headers, config) {  
            //     deferred.resolve(data);  // 声明执行成功，即http请求数据成功，可以返回数据了  
            // }).  
            // error(function(data, status, headers, config) {  
            //     deferred.reject(data);   // 声明执行失败，即服务器返回错误  
            // });  
            $http({method: 'GET', params: obj, url: talist}).then(
                function(data){
                    deferred.resolve(data.data);
                },
                function(data){
                    deferred.reject(data.data);
                });
            return deferred.promise;   // 返回承诺，这里并不是最终数据，而是访问最终数据的API 
        },
        recharge : function(){
            return $resource(recharge, {}, {});
        },
        uselist : function(){
            return $resource(uselist, {}, {});
        },
        destoryDetail : function(){
            return $resource(destoryDetail, {}, {});
        },
        viewdestorystatisticlist : function(){
            return $resource(viewdestorystatisticlist, {}, {});
        },
        orderstatisticslist : function(){
            return $resource(orderstatisticslist, {}, {});
        },
        orderstatisticscompanylist : function(){
            return $resource(orderstatisticscompanylist, {}, {});
        },
        govsubsidygoodscodelist : function(){
            return $resource(govsubsidygoodscodelist, {}, {});
        },
        groupcountlist : function(){
            return $resource(groupcountlist, {}, {});
        },
        groupcountjqlist : function(){
            return $resource(groupcountjqlist, {}, {});
        },
        useddetaillist : function(){
            return $resource(useddetaillist, {}, {});
        },
        grouplxslist : function(){
            return $resource(grouplxslist, {}, {});
        },
        orderstatisticscompanyhistorylist : function(){
            return $resource(orderstatisticscompanyhistorylist, {}, {});
        },
        orderstatisticshistorylist : function(){
            return $resource(orderstatisticshistorylist, {}, {});
        },
        viewdestorystatistichistorylist : function(){
            return $resource(viewdestorystatistichistorylist, {}, {});
        },
        orderstatisticsusedinfolist : function(){
            return $resource(orderstatisticsusedinfolist, {}, {});
        },
        staticonline : function(){
            return $resource(staticonline, {}, {});
        },
        countByCompnycode : function(){
            return $resource(countByCompnycode, {}, {});
        },
        companySaleStatisticHistory : function(){
            return $resource(companySaleStatisticHistory, {}, {});
        },
        companySaleStatistic : function(){
            return $resource(companySaleStatistic, {}, {});
        },
        saleStatisticAllPlace : function(){
            return $resource(saleStatisticAllPlace, {}, {});
        },
        saleStatisticAllPlaceHistory : function(){
            return $resource(saleStatisticAllPlaceHistory, {}, {});
        }
    };



};

module.exports = service;