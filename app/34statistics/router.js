
var router = function ($urlRouterProvider, $stateProvider) {

	$stateProvider

	
        .state('app.statisticsviewlist', {
			url: '/statisticsviewlist',
             views:{
                'main@':{
                        controller: 'statisticsviewlist',
                        template: require('./views/statisticsviewlist.html'),
                }
             },
			//template: require('../99common/views/table.html'),
			resolve: {
				viewdestorystatisticlist: function (statisticsservice) {
					return statisticsservice.viewdestorystatisticlist();
				},
				viewdestorystatistichistorylist: function (statisticsservice) {
					return statisticsservice.viewdestorystatistichistorylist();
				},
				getDate: function (utilservice) {
					return utilservice.getDate;
				},
				govsubsidygoodscodelist: function (statisticsservice) {
					return statisticsservice.govsubsidygoodscodelist();
				},
				useddetaillist: function (statisticsservice) {
					return statisticsservice.useddetaillist();
				},
				grouplxslist: function (statisticsservice) {
					return statisticsservice.grouplxslist();
				},
				DateDiff: function (utilservice) {
					return utilservice.DateDiff;
				}
			}

		})

        .state('app.statisticssale', {
			url: '/statisticssale',
              views:{
                'main@':{
                        controller: 'statisticssale',
                        template: require('./views/statisticssale.html'),
                }
              },
			resolve: {
				orderstatisticslist: function (statisticsservice) {
					return statisticsservice.orderstatisticslist();
				},
				orderstatisticshistorylist: function (statisticsservice) {
					return statisticsservice.orderstatisticshistorylist();
				},
				orderstatisticsusedinfolist: function (statisticsservice) {
					return statisticsservice.orderstatisticsusedinfolist();
				},
				getDate: function (utilservice) {
					return utilservice.getDate;
				},
				DateDiff: function (utilservice) {
					return utilservice.DateDiff;
				}
			}

		})


		.state('app.statisticscompanylist', {
			url: '/statisticscompanylist',
			 views:{
                'main@':{
					controller: 'statisticscompanylist',
					template: require('./views/statisticscompanylist.html'),
				}
			 },
			resolve: {
				orderstatisticscompanylist: function (statisticsservice) {
					return statisticsservice.orderstatisticscompanylist();
				},
				orderstatisticscompanyhistorylist: function (statisticsservice) {
					return statisticsservice.orderstatisticscompanyhistorylist();
				},
				orderstatisticsusedinfolist: function (statisticsservice) {
					return statisticsservice.orderstatisticsusedinfolist();
				},
				getDate: function (utilservice) {
					return utilservice.getDate;
				},
				DateDiff: function (utilservice) {
					return utilservice.DateDiff;
				}
			}

		})

		//供应商销售统计 2017年11月2日11:38:24
		.state('app.companySaleStatistic', {
			url: '/saleStatistic',
			 views:{
                'main@':{
					controller: 'statisticCompanySale',
					template: require('./views/statisticCompanySale.html'),
				}
			 },
			resolve: {
				companySaleStatistic: function (statisticsservice) {
					return statisticsservice.companySaleStatistic();
				},
				companySaleStatisticHistory: function (statisticsservice) {
					return statisticsservice.companySaleStatisticHistory();
				},
				orderstatisticsusedinfolist: function (statisticsservice) {
					return statisticsservice.orderstatisticsusedinfolist();
				},
				getDate: function (utilservice) {
					return utilservice.getDate;
				},
				DateDiff: function (utilservice) {
					return utilservice.DateDiff;
				}
			}

		})

		//景区产品统计 2017年11月10日15:28:58
		.state('app.saleStatisticAllPlace', {
			url: '/saleStatisticAllPlace',
			 views:{
                'main@':{
					controller: 'saleStatisticAllPlace',
					template: require('./views/saleStatisticAllPlace.html'),
				}
			 },
			resolve: {
				saleStatisticAllPlace: function (statisticsservice) {
					return statisticsservice.saleStatisticAllPlace();
				},
				saleStatisticAllPlaceHistory: function (statisticsservice) {
					return statisticsservice.saleStatisticAllPlaceHistory();
				},
				orderstatisticsusedinfolist: function (statisticsservice) {
					return statisticsservice.orderstatisticsusedinfolist();
				},
				getDate: function (utilservice) {
					return utilservice.getDate;
				},
				DateDiff: function (utilservice) {
					return utilservice.DateDiff;
				}
			}

		})
  
		.state('app.statisticsgrouplist', {
			url: '/statisticsgrouplist',
		    views:{
                'main@':{
						controller: 'statisticsgrouplist',
						template: require('./views/statisticsgrouplist.html'),
				}
			},
			resolve: {
				getDate: function (utilservice) {
					return utilservice.getDate;
				},
				groupcountlist: function (statisticsservice) {
					return statisticsservice.groupcountlist();
				}
			}

		})

		.state('app.statisticsgroupjqlist', {
			url: '/statisticsgroupjqlist',
			 views:{
                'main@':{
						controller: 'statisticsgroupjqlist',
						template: require('./views/statisticsgroupjqlist.html'),
				}
			 },
			resolve: {
				getDate: function (utilservice) {
					return utilservice.getDate;
				},
				groupcountjqlist: function (statisticsservice) {
					return statisticsservice.groupcountjqlist();
				}
			}

		})

		.state('app.distributor', {
					url: '/distributor',
					 views:{
                       'main@':{
							controller: 'distributor',
							template: require('./views/distributor.html'),
					   }
					 },
					resolve: {
						getDate: function (utilservice) {
							return utilservice.getDate;
						},
						orderstatisticscompanyhistorylist: function (statisticsservice) {
							return statisticsservice.orderstatisticscompanyhistorylist();
						},
						talist: function (statisticsservice) {
							return statisticsservice.talist;
						}
					}

				})

       //在线套票统计
		.state('app.cardorder', {
			url: '/cardorder',
			 views:{
                'main@':{
						controller: 'staticonline',
						template: require('./views/staticonline.html'),
				}
			 },
			resolve: {
				getDate: function (utilservice) {
					return utilservice.getDate;
				},
				staticonline: function (statisticsservice) {
					return statisticsservice.staticonline();
				}
			}

		})


		.state('app.countByCompny', {
			url: '/countByCompny',
			views:{
                'main@':{
						controller: 'countByCompny',
						template: require('./views/countByCompny.html'),
				}
			},
			resolve: {
				getDate: function (utilservice) {
					return utilservice.getDate;
				},
				// countByCompny: function (statisticsservice) {
				// 	return statisticsservice.orderstatisticscompanyhistorylist();
				// },
				mechanism: function (statisticsservice) {
					return statisticsservice.mechanism();
				},
				countByCompnycode: function (statisticsservice) {
					return statisticsservice.countByCompnycode();
				},
                date2str : function(utilservice){
                    return utilservice.date2str;
                }
			}

		})

		.state('app.salechart', {
			url: '/salechart',
			views:{
                'main@':{
						controller: 'salechart',
						template: require('./views/salechart.html'),
				}
			},
			resolve: {
                orderstatisticslist : function(statisticsservice){
                    return statisticsservice.orderstatisticslist();
                },
                getDate : function(utilservice){
                    return utilservice.getDate;
                },
                dataScope : function(utilservice){
                    return utilservice.dataScope;
                },
                salelist : function(productservice){
                    return productservice.salelist();
                },
                date2str : function(utilservice){
                    return utilservice.date2str;
                }
			}

		})

		//使用统计
		.state('app.usedStatistics', {
			url: '/usedStatistics',
			views:{
                'main@':{
						controller: 'usedStatistics',
						template: require('./views/usedStatistics.html'),
				}
			},
			resolve: {
                getDate : function(utilservice){
                    return utilservice.getDate;
                },
                date2str : function(utilservice){
                    return utilservice.date2str;
                },
				str2date: function (utilservice) {
					return utilservice.str2date;
				}
			}
		})

		//?统计
		.state('app.count', {
			url: '/count',
			views:{
                'main@':{
						controller: 'count',
						template: require('./views/count.html'),
				}
			},
			resolve: {
                getDate : function(utilservice){
                    return utilservice.getDate;
                },
                date2str : function(utilservice){
                    return utilservice.date2str;
                },
				str2date: function (utilservice) {
					return utilservice.str2date;
				},
				mechanism: function (statisticsservice) {
					return statisticsservice.mechanism();
				}
			}
		})

};
module.exports = router;