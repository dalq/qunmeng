module.exports = function ($) {

	var dlq = angular.element(document).ready(function () {


		//angular.bootstrap(document, ['app']);

		//return ;

		//获取用户信息
		$.ajax({
            url : '/api/as/info',
            type : "GET",
            dataType : 'json'
        }).then(function (res1) {

        //获取用户的拥有的菜单列表
		$.ajax({
			url : '/api/as/sc/menu/userMenuList',
			type : "GET",
			dataType : 'json'
		}).then(function (res) {

			


			if (res.errcode === 0) {

				//按钮显示权限
				var permissions = new Array();

				angular.module('app').run(['$rootScope', '$location', 'angularPermission', function ($rootScope, $location, angularPermission) {

					$rootScope.sys_list = res.data;

					$rootScope.userPermissionList = permissions;

					$rootScope.info = res1;

					angularPermission.setPermissions($rootScope.userPermissionList);
					

				}]);


				angular.module('app').config(function($provide, $qProvider, $urlRouterProvider){

					$provide.service('dlq', function(){
				        this.title = res1.company_name;
				        this.obj = res1;
	    			})

	    			$qProvider.errorOnUnhandledRejections(false);

	    			var dashboard = '';
	    			for(var i = 0; i < res.data.length; i++){
	    				var tmp = res.data[i];

	    				if(tmp.href && tmp.href != ''){
	    					dashboard = tmp.href;
	    					break;
	    				}
	    			}

	    			if(dashboard == ''){
	    				$urlRouterProvider.otherwise('/app/supplier_dashboard');
	    			}else{
	    				$urlRouterProvider.otherwise(dashboard);
	    			}
	    			

				})

				angular.bootstrap(document, ['app']);			

			} else {

				if(res1.company_sys_area == 'LW'){
					//window.location = "/qunmeng.html";
					window.location = "/login.html";
				}else{
					window.location = "/login.html";
				}
				

			}
			
		});

		});


		return;

	// 	$.ajax({
	// 		// url: '/api/ac/sc/menuService/menulist',
	// 		url: '/api/ac/sc/menuService/newMenulist',
	// 		type: "GET",
	// 		dataType: 'json'
	// 	}).then(function (res) {

	// 		//console.log(res);

	// 		//按钮显示权限
	// 		var permissions = new Array();
	// 		//菜单
	// 		var menudata = {};

	// 		if (res.errcode === 0) {


	// 			menudata = res.data;

	// 			console.log('menudata');
	// 			console.log(res);


	// 			if(res.data.list){
	// 				for (var i = 0; i < res.data.list.length; i++) {
	// 					var tmp = res.data.list[i];
	// 					if (angular.isArray(tmp.list)) {
	// 						for (var j = 0; j < tmp.list.length; j++) {
	// 							var tmp1 = tmp.list[j];
	// 							if (tmp1.hasOwnProperty('permission')) {
	// 								permissions.push(tmp1.permission)
	// 							}
	// 						}
	// 					}
	// 				}
	// 			}
				

	// 			angular.module('app').run(['$rootScope', '$location', 'angularPermission', function ($rootScope, $location, angularPermission) {

	// 				$rootScope.menudata = menudata;

	// 				$rootScope.userPermissionList = permissions;

	// 				angularPermission.setPermissions($rootScope.userPermissionList);

	// 			}]);

	// 			angular.bootstrap(document, ['app']);

	// 			//  console.log(menudata);
	// 		}
	// 		else if (res.errcode === 1001) {
	// 			window.location = "/login.html";
	// 		}
	// 		else if (res.errcode === 1002) {
	// 			alert('菜单无权限!');
	// 			window.location = "/login.html";
	// 		}
	// 		else {
	// 			alert('菜单' + res.errmsg);
	// 			//window.location = "/manager/login";
	// 		}

	// 	});
	});

};