var webpack = require('webpack');
var path = require("path");

var ROOT_PATH = path.resolve(__dirname);
var APP_PATH = path.resolve(ROOT_PATH, 'app');
var BUILD_PATH = path.resolve(ROOT_PATH, 'build');

module.exports = {
  entry : {
        bundle: path.resolve(APP_PATH, 'index.js'),
        vendor:[
        "angular",
        "angular-animate",
        "angular-bootstrap",
        "angular-chart.js",
        "angular-cookies",
        "angular-datatables",
        "angular-eonasdan-datetimepicker",
        "angular-file-upload",
        "angular-resource",
        "angular-sanitize",
        "angular-storage",
        "angular-touch",
        "angular-translate",
        "angular-translate-loader-static-files",
        "angular-tree-control",
        // "angular-ueditor",
        "angular-ui-router",
        // "angular-ui-select",
        "angular-ui-tree",
        "angularjs-toaster",
        "bootstrap",
        "eonasdan-bootstrap-datetimepicker",
        // "font-awesome",
        "jquery",
        "moment",
        "ngstorage",
        "oclazyload",
        // "simple-line-icons",
        "textangular",
        "ui-select",
        ]
  },
  //输出的文件名 合并以后的js会命名为bundle.js
  output: {
    path: __dirname + '/build',
    filename: '[name].js'
  },

  module: {
      //加载器配置
      loaders: [
          { test: /\.css$/, loader: 'style-loader!css-loader' },
          { test: /\.scss$/, loader: 'style!css!sass?sourceMap'},
          { test: /\.(gif|jpg|png|woff|svg|eot|ttf)\??.*$/, loader: 'url-loader?limit=50000&name=[path][name].[ext]'},
          { test: /\.html$/,loader: 'raw'}
      ]
  },
    plugins: [
     //这个使用uglifyJs压缩你的js代码
    new webpack.optimize.UglifyJsPlugin({
      minimize: true,
      beautify : false,
      mangle:false, // 关闭混淆
      compress: {
        warnings: false,
      },
    }),
    new webpack.optimize.CommonsChunkPlugin({
      'name':'vendor',
      'filename':'vendor.js'
    })
  ]

};
