var webpack = require('webpack');
var path = require("path");

var ROOT_PATH = path.resolve(__dirname,'../');
var APP_PATH = path.resolve(ROOT_PATH, 'app');
var BUILD_PATH = path.resolve(ROOT_PATH, 'build');

module.exports = {
  entry : {
        vendor:[
        "angular",
        "angular-animate",
        "angular-bootstrap",
        "angular-chart.js",
        "angular-cookies",
        "angular-datatables",
        "angular-eonasdan-datetimepicker",
        "angular-file-upload",
        "angular-resource",
        "angular-sanitize",
        "angular-storage",
        "angular-touch",
        "angular-translate",
        "angular-translate-loader-static-files",
        "angular-tree-control",
        // "angular-ueditor",
        "angular-ui-router",
        // "angular-ui-select",
        "angular-ui-tree",
        "angularjs-toaster",
        "bootstrap",
        "eonasdan-bootstrap-datetimepicker",
        // "font-awesome",
        "jquery",
        "moment",
        "ngstorage",
        "oclazyload",
        // "simple-line-icons",
        "textangular",
        "ui-select",
        ]
  },
  //输出的文件名 合并以后的js会命名为bundle.js
  output: {
    path: BUILD_PATH,
    filename: '[name].js',
    library: '[name]',
  },
  plugins: [
    //这个使用uglifyJs压缩你的js代码
    new webpack.optimize.UglifyJsPlugin({
      minimize: true,
      beautify : false,
      mangle:false, // 关闭混淆
      compress: {
        warnings: false,
      },
    }),
    new webpack.DllPlugin({
      /**
       * path
       * 定义 manifest 文件生成的位置
       * [name]的部分由entry的名字替换
       */
      path: path.resolve(BUILD_PATH, '[name]-manifest.json'),
      /**
       * name
       * dll bundle 输出到那个全局变量上
       * 和 output.library 一样即可。 
       */
    //   name: '[name]_[chunkhash]',
      name: '[name]',
      context: ROOT_PATH,
    })
  ]

};
