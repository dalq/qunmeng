var webpack = require('webpack');
var path = require("path");

var ROOT_PATH = path.resolve(__dirname,'../');
var APP_PATH = path.resolve(ROOT_PATH, 'app');
var BUILD_PATH = path.resolve(ROOT_PATH, 'build');
module.exports = {
  entry : {
        bundle: path.resolve(APP_PATH, 'index.js'),
  },
  //输出的文件名 合并以后的js会命名为bundle.js
  output: {
    path: BUILD_PATH,
    filename: '[name].js'
  },

  module: {
      //加载器配置
      loaders: [
          { test: /\.css$/, loader: 'style-loader!css-loader' },
          { test: /\.scss$/, loader: 'style!css!sass?sourceMap'},
          { test: /\.(gif|jpg|png|woff|svg|eot|ttf)\??.*$/, loader: 'url-loader?limit=50000&name=[path][name].[ext]'},
          //{ test: /\.(png|jpg)$/, loader: 'url-loader?limit=8192'},
          { test: /\.html$/,loader: 'raw'}
      ]
  },
  devServer: {
    // contentBase:'./build',
    historyApiFallback: true,
    hot: true,
    inline: true,
    progress: true
       ,
    proxy: {
      '/api': 'http://dlqt.tripln.com',
      '/wdapi': 'http://dlqt.tripln.com',
      '/manager': 'http://dlqt.tripln.com',
      '/a/': 'http://dlqt.tripln.com',
      '/tktapi': 'http://dlqt.tripln.com'


      // '/api': 'http://192.168.0.114:38985',
      // '/wdapi': 'http://192.168.0.114:38985',
      // '/manager': 'http://192.168.0.114:38985',
      // '/a/': 'http://192.168.0.114:38985',
      // '/tktapi': 'http://192.168.0.114:38985'

      //
    }

  }

};
