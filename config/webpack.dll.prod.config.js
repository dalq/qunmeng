var webpack = require('webpack');
var path = require("path");
var fs = require("fs");

var ROOT_PATH = path.resolve(__dirname,'../');
var APP_PATH = path.resolve(ROOT_PATH, 'app');
var BUILD_PATH = path.resolve(ROOT_PATH, 'build');

module.exports = {
  entry: {
    bundle: path.resolve(APP_PATH, 'index.js'),
  },
  output: {
    path: BUILD_PATH,
    filename: '[name].js',
  },
  module: {
      //加载器配置
      loaders: [
          { test: /\.css$/, loader: 'style-loader!css-loader' },
          { test: /\.scss$/, loader: 'style!css!sass?sourceMap'},
          { test: /\.(gif|jpg|png|woff|svg|eot|ttf)\??.*$/, loader: 'url-loader?limit=50000&name=[path][name].[ext]'},
          { test: /\.html$/,loader: 'raw'}
      ]
  },
  plugins: [
    //这个使用uglifyJs压缩你的js代码
    new webpack.optimize.UglifyJsPlugin({
      minimize: true,
      beautify : false,
      mangle:true, // 关闭混淆
      compress: {
        warnings: false,
      },
    }),
    new webpack.DllReferencePlugin({
      context: ROOT_PATH,
      manifest: require('./build/vendor-manifest.json'),
    })
    // ,
    // function(){
    //     this.plugin('done', stats => {
    //         fs.readFile('./index.html',function(err, data) {
    //             const $ = cheerio.load(data.toString());
    //             $('script[src*=dest]').attr('src', 'build/bundle.'+stats.hash+'.js');
    //             fs.write('./index.html', $.html(), err => {
    //                 !err && console.log('Set has success: '+stats.hash)
    //             })
    //         })
    //     })
    // }
  ],
};